# WerBeo Service and CMS


## Build and Run

  1. Start keycloak and indica with docker (get the current versions from `.gitlab.yml`):
     - ```docker run -p 8080:8080 infinitenature/keycloak-werbeo:4.3.0-4920```
     - ```docker run -p80:80 -p5432:5432 infinitenature/indicia-warehouse-werbeo:1.55.0-5021```
     - ```docker run -p8983:8983 infinitenature/solr-werbeo:7.5.0-3495```
  1. Build the software
     - ```./mvnw-all clean install``` on OS-X or linux
     - on windows do `mvnw.bat clean install` in `werbeo-common`, `werbeo-service` and `werbeo-cms`
  1. Run the software
     - run the maven goal `spring-boot:run` in `werbeo-service/rest/impl` with the spring boot profile `test` to start the service
     - run the maven goal `spring-boot:run` in `werbeo-cms/cms` with the spring boot profile `test` to start the cms
     
The services should be available now:

| Service | Port | Description |
| ------- | ---- | ----------- |
| Postgre SQL | 5432 | the indicia database |
| Indicia warehouse | 80 | [the indicia admin interface](http://localhost/indicia/index.php/login) |
| Keycloak | 8080 | keycloak [admin](http://localhost:8080/auth/admin/master/console/#/realms/test.infinitenature.org) and [user](http://localhost:8080/auth/realms/test.infinitenature.org/account/) interface |
| WerBeo Service | 8087 | [the WerBeo service](http://localhost:8087/doc/index.html) |
| WerBeo CMS | 8085 | [Flora MV](http://localhost:8085), [Flora BB](http://127.0.0.1:8085) |
| Solr | 8983 | solr search index |
