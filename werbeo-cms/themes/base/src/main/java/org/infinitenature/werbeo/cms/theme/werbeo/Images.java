package org.infinitenature.werbeo.cms.theme.werbeo;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.enerccio.vaadin.fontawesome.FontAwesome;
import com.vaadin.server.Resource;

public class Images
{
   private static final String BASE_DIR = "/VAADIN/themes/base";

   private static final Logger LOGGER = LoggerFactory.getLogger(Images.class);

   private Images()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static Resource getIcon(ValidationStatus status)
   {
      if (status == null)
      {
         return null;
      }

      switch (status)
      {
      case VALID:
         return FontAwesome.CHECK_DOUBLE;
      case PROBABLE:
         return FontAwesome.CHECK;
      case UNKNOWN:
         return FontAwesome.QUESTION;
      case IMPROBALE:
         return FontAwesome.TIMES;
      case INVALID:
         return FontAwesome.TIMES_CIRCLE;

      default:
         LOGGER.error("Unknown ValidationStatus {}", status);
         return FontAwesome.QUESTION;
      }
   }
   public static String getIconSVG(ValidationStatus status)
   {

      if (status == null)
      {
         return BASE_DIR + "/img/validation/notvalidated.svg";
      }

      switch (status)
      {
      case VALID:
         return BASE_DIR + "/img/validation/valid.svg";
      case PROBABLE:
         return BASE_DIR + "/img/validation/probable.svg";
      case UNKNOWN:
         return BASE_DIR + "/img/validation/unknown.svg";
      case IMPROBALE:
         return BASE_DIR + "/img/validation/improbale.svg";
      case INVALID:
         return BASE_DIR + "/img/validation/invalid.svg";

      default:
         LOGGER.error("Unknown ValidationStatus {}", status);
         return BASE_DIR + "/img/validation/unknown.svg";
      }
   }
}
