package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.filter.CSVUploadPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.importtable.ImportPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.OccurrenceTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class ImportPageDescription extends AbstractWerbeoPageDescription
{
   public ImportPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("imports");
      setUrlPrefix(PageRepositoryMock.URL_IMPORT);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_EINGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.IMPORT.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, csvUploadComponent());
      addComponent(SimpleLayout.AREA_CENTER, importsTableComponent(), true);
      setAnonymousAllowed(false);
   }

   private VCMSComponentDescriptionXML csvUploadComponent()
   {
      return new VCMSComponentDescriptionXML(
            CSVUploadPlugin.class.getName());
   }

   private VCMSComponentDescriptionXML importsTableComponent()
   {
      return new VCMSComponentDescriptionXML(
            ImportPlugin.class.getName());
   }
}
