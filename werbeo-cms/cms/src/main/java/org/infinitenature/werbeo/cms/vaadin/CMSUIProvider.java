package org.infinitenature.werbeo.cms.vaadin;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.infinitenature.vct.theme.VCMSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.server.UICreateEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.server.SpringUIProvider;

public class CMSUIProvider extends SpringUIProvider
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CMSUIProvider.class);

   private final AppRepositoryQueries appRepository;

   public CMSUIProvider(VaadinSession vaadinSession,
         AppRepositoryQueries appRepositoryQueries)
   {
      super(vaadinSession);
      this.appRepository = appRepositoryQueries;
   }

   @Override
   public String getTheme(UICreateEvent event)
   {
      String hostName = VCMSUtil.getHostName(event.getRequest());
      VCMSApp app = appRepository.getApp(hostName);
      String theme = getThemeName(app);
      createThemeDirectory(theme);
      LOGGER.debug("Set theme to: {}", theme);
      return theme;
   }

   public String getThemeName(VCMSApp app)
   {
      LOGGER.debug("Getting theme for app {}", app);
      String theme;

      switch (app.getName())
      {
      case "Flora-MV":
         theme = "flora-mv";
         break;
      case "Heuschrecken Deutschland":
         theme = "heuschrecken-portal";
         break;
      case "Heuschrecken RLP":
         theme = "heuschrecken-rlp";
         break;
      case "Heuschrecken NRW":
         theme = "heuschrecken-nrw";
         break;
      case "Dermaptera Deutschland":
         theme = "dermaptera-portal";
         break;
      case "Flora-SN":
         theme = "flora-sn";
         break;
      case "Flora-ST":
         theme = "flora-st";
         break;
      default:
         theme = "flora-bb";
         break;
      }
      return theme;
   }

}
