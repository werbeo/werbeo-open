package org.infinitenature.werbeo.cms.vaadin;

import javax.annotation.PostConstruct;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.VCMSContextInitializer;
import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.vaadin.server.SessionInitEvent;

public class ContextIntializer extends VCMSContextInitializer
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(ContextIntializer.class);

   @Autowired
   private AppRepositoryQueries appRepository;

   @PostConstruct
   public void initialize()
   {
      setAppRepository(appRepository);
   }

   @Override
   protected VCMSContext createContextInstance(SessionInitEvent event)
   {
      LOGGER.debug("Creating context instance for event {}", event);
      return new Context();
   }
}
