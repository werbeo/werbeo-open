package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class FloraMVMainPageDescription extends AbstractWerbeoPageDescription
{

   public FloraMVMainPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("main");
      setUrlPrefix(PageRepositoryMock.URL_MAIN);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .htmlComponent("# Willkommen auf Flora-MV!"
                  + "\nFloristische Datenbank in Mecklenburg Vorpommern."),
            true);
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.imageComponent(
                  "werbeo-website.png"),
            true);
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .htmlComponent("## Unsere freundlichen Unterstützer"), true);
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.imageComponent(
                  "werbeo.png#UniRostock.png#UniOsnabrueck.png#DUENE.png#BfN.png#Bund.png#MeckPom.png#Brandenburg.png#Berlin.png#Sachsen-Anhalt.png#Sachsen.png#"),
            true);
      addComponent(SimpleLayout.AREA_FOOTER,
            ComponentDescriptionFactory.htmlComponent("copyright WerBeo\n"
                  + "Prof. Dr. Florian Jansen\n" + "Universität Rostock\n"
                  + "Agrar- und Umweltwissenschaftliche Fakultät\n"
                  + "Justus-von-Liebig-Weg 6\n" + "18059 Rostock"),
            true);
   }

}
