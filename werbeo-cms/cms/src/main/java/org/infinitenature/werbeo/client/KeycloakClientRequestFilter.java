package org.infinitenature.werbeo.client;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class KeycloakClientRequestFilter implements ClientRequestFilter
{
   public static final String AUTHORIZATION_HEADER = "Authorization";

   @Override
   public void filter(ClientRequestContext requestContext) throws IOException
   {
      KeycloakSecurityContext context = this.getKeycloakSecurityContext();
      if (context != null)
      {
         requestContext.getHeaders().add(AUTHORIZATION_HEADER,
               "Bearer " + context.getTokenString());
      }
   }

   /**
    * Returns the {@link KeycloakSecurityContext} from the Spring
    * {@link SecurityContextHolder}'s {@link Authentication}.
    *
    * @return the current <code>KeycloakSecurityContext</code>
    */
   protected KeycloakSecurityContext getKeycloakSecurityContext()
   {
      Authentication authentication = Context.getCurrent()
            .getSpringAuthentication();
      KeycloakAuthenticationToken token;
      KeycloakSecurityContext context;

      if (authentication == null)
      {
         // No authentication means not authenticated
         return null;
      }

      if (org.springframework.security.authentication.AnonymousAuthenticationToken.class
            .isAssignableFrom(authentication.getClass()))
      {
         return null;
      }
      if (!KeycloakAuthenticationToken.class
            .isAssignableFrom(authentication.getClass()))
      {
         throw new IllegalStateException(String.format(
               "Cannot set authorization header because Authentication is of type %s but %s is required",
               authentication.getClass(), KeycloakAuthenticationToken.class));
      }

      token = (KeycloakAuthenticationToken) authentication;
      context = token.getAccount().getKeycloakSecurityContext();

      return context;
   }
}
