package org.infinitenature.werbeo.cms.vaadin;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.PeopleFilter;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.service.v1.types.response.PersonSliceResponse;
import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.vct.repository.AppTemplateFactory;
import org.infinitenature.vct.repository.URLRepository;
import org.infinitenature.vct.vaadin.navigator.PageProvider;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.error.WerbeoErrorHandler;
import org.infinitenature.werbeo.cms.vaadin.piwik.Piwik;
import org.infinitenature.werbeo.cms.vaadin.plugin.tc.AcceptancePlugin;
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceResolver;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.StyleSheet;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@PushStateNavigation
@SpringUI
@Push(transport = Transport.WEBSOCKET_XHR)
@StyleSheet("https://fonts.googleapis.com/css?family=Karla")
public class CMSUI extends UI
{
   private static final Logger LOGGER = LoggerFactory.getLogger(CMSUI.class);

   @Autowired
   private PageProvider pageProvider;
   @Autowired
   private URLRepository urlRepository;
   @Autowired
   private DeviceResolver deviceResolver;
   @Autowired
   private AppTemplateFactory appTemplateFactory;
   @Autowired
   private Piwik piwik;
   @Autowired
   private WerbeoErrorHandler werbeoErrorHandler;
   @Autowired
   private Werbeo werbeo;

   @Override
   protected void init(VaadinRequest request)
   {
      setErrorHandler(werbeoErrorHandler);
      detectDevice(request);

      detectLoginState();
      VCMSAppTemplate appTemplate = getAppTemplate();

      initNavigator(appTemplate);
      addPiwikTracker(request);

      restoreNavigationState();
      if (Context.getCurrent().isAcceptanceNecessary())
      {
         pageProvider.setForceViewName(Optional.ofNullable(
               urlRepository.getFragment(Context.getCurrent().getApp(),
                     AcceptancePlugin.class)));
      }else
      {
         pageProvider.setForceViewName(Optional.empty());
      }
      Component vaadinComponent = appTemplate.getVaadinComponent();
      setContent(vaadinComponent);
   }

   private void restoreNavigationState()
   {
      String navigationStateBeforeLogin = NavigationStateCookie
            .readNavigationState();
      try
      {
         if (StringUtils.isNotBlank(navigationStateBeforeLogin))
         {
            getNavigator()
                  .navigateTo(navigationStateBeforeLogin.replace(" ", "%20"));
            NavigationStateCookie.deleteNavigationState();
         }
      } catch (Exception e)
      {
         LOGGER.error("Failing restoring navigation state {}",
               navigationStateBeforeLogin, e);
         getNavigator().navigateTo("");
      }
   }

   private void addPiwikTracker(VaadinRequest request)
   {
      piwik.init(request);
      piwik.setSiteId(Context.getCurrent().getApp().getPiwikSiteId());
      Optional<String> email = Context.getCurrent().getAuthentication()
            .getEmail();
      if (email.isPresent())
      {
         piwik.setUserId(email.get());
      }

      getNavigator().addViewChangeListener(new ViewChangeListener()
      {
         @Override
         public void afterViewChange(ViewChangeEvent event)
         {
            piwik.track(getPage().getLocation());
         }

         @Override
         public boolean beforeViewChange(ViewChangeEvent event)
         {

            return true;
         }
      });
   }

   private void detectLoginState()
   {
      LOGGER.info("Create context for user {}",
            SecurityContextHolder.getContext().getAuthentication());
      Context.getCurrent().setSpringAuthentication(
            SecurityContextHolder.getContext().getAuthentication());
      if (SecurityContextHolder.getContext().getAuthentication()
            .isAuthenticated()
            && !"anonymousUser".equals(SecurityContextHolder.getContext()
                  .getAuthentication().getPrincipal().toString()))
      {
         if (LOGGER.isInfoEnabled())
         {
            LOGGER.info("User is authenticated with principal {}",
                  SecurityContextHolder.getContext().getAuthentication()
                        .getPrincipal().toString());
         }
         org.springframework.security.core.Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         String email = auth
               .getPrincipal().toString();
         Context.getCurrent().setAuthentication(new Authentication()
         {
            @Override
            public Optional<String> getEmail()
            {
               return Optional.of(email);
            }

            @Override
            public boolean isAnonymous()
            {
               return false;
            }

            @Override
            public Optional<String> getFamilyName()
            {
               return Optional.of(((SimpleKeycloakAccount) auth.getDetails())
                     .getKeycloakSecurityContext().getToken().getFamilyName());
            }

            @Override
            public Optional<String> getGivenName()
            {
               return Optional.of(((SimpleKeycloakAccount) auth.getDetails())
                     .getKeycloakSecurityContext().getToken().getGivenName());
            }

         });
         createPersonIfNotExists();
      } else
      {
         LOGGER.info("User is not authenticated");
         Context.getCurrent().setAuthentication(Authentication.ANONYMOUS);
      }

   }

   private void createPersonIfNotExists()
   {
      try
      {
         Authentication authentication = Context.getCurrent()
               .getAuthentication();

         Person person = new Person();
         person.setEmail(authentication.getEmail().get());
         person.setFirstName(authentication.getGivenName().orElse(""));
         person.setLastName(authentication.getFamilyName().orElse(""));

         PersonSliceResponse response = werbeo.people().find(
               0, 1, PersonField.ID,
               SortOrder.DESC, new PeopleFilter(Context.getCurrent().getPortal().getPortalId(), "", person.getEmail(), null));
         if (response.getContent().isEmpty())
         {
            LOGGER.warn("Person not found in Werbeo, create it:" + person);
            werbeo.people().save(Context.getCurrent().getPortal().getPortalId(),
                  person);
         }
      } catch (Exception e)
      {
         LOGGER.error("Errer finding or creating Person", e);
      }
   }

   private void initNavigator(VCMSAppTemplate appTemplate)
   {
      Navigator navigator = new Navigator(this, appTemplate);
      navigator.addProvider(pageProvider);
      navigator.setErrorProvider(pageProvider.getErrorViewProvider());
   }

   private VCMSAppTemplate getAppTemplate()
   {
      VCMSContext currentContext = Context.getCurrent();
      return appTemplateFactory.getVCMSAppTemplate(currentContext.getApp(),
            currentContext.getViewMode());
   }

   private void detectDevice(VaadinRequest request)
   {
      Device device = deviceResolver
            .resolveDevice((VaadinServletRequest) request);
      LOGGER.info("User uses: {}", device);
      if (!device.isNormal())
      {
         Context.getCurrent().setViewMode(ViewMode.MOBILE);
      } else
      {
         Context.getCurrent().setViewMode(ViewMode.DESKTOP);
      }
   }

   @Override
   public boolean equals(Object obj)
   {
      return super.equals(obj);
   }

   @Override
   public int hashCode()
   {
      return super.hashCode();
   }
}
