package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.TocPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class TocPageDescription extends AbstractWerbeoPageDescription
{
   public TocPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("toc");
      setUrlPrefix(PageRepositoryMock.URL_TOC);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.NUTZUNGSBEDINGUNGEN.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .component(TocPlugin.class), true);
   }
}
