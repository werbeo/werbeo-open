package org.infinitenature.werbeo.cms.config;

import java.io.IOException;
import java.util.UUID;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.infinitenature.service.support.xml.converter.LocalDateConverterProvider;
import org.infinitenature.service.v1.resources.Header;
import org.infinitenature.werbeo.client.KeycloakClientRequestFilter;
import org.infinitenature.werbeo.client.Werbeo;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
@EnableWebSecurity
public class WerbeoClientConfig
{

   @Value("${werbeo.client.url}")
   private String url;

   @Bean
   public KeycloakClientRequestFilter clientRequestFilter()
   {
      return new KeycloakClientRequestFilter();
   }

   @Bean
   public LocalDateConverterProvider localDateConverterProvider()
   {
      return new LocalDateConverterProvider();
   }

   @Bean
   @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
   public Werbeo werbeo(KeycloakClientRequestFilter filter)
   {
      Werbeo werbeo = new Werbeo(url, filter);
      werbeo.getClient().register(new ClientRequestFilter()
      {

         @Override
         public void filter(ClientRequestContext requestContext)
               throws IOException
         {
            requestContext.getHeaders().add(
                  Header.WERBEO_REQUEST_SOURCE_ID_NAME,
                  "werbeo-cms-" + UUID.randomUUID());
         }
      });
      return werbeo;
   }
}
