package org.infinitenature.werbeo.cms.vaadin;

import javax.servlet.ServletException;

import org.infinitenature.vct.VCMSContextInitializer;
import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.infinitenature.vct.vaadin.SeoBootstrapListener;
import org.infinitenature.vct.vaadin.navigator.PageProvider;

import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.server.SpringVaadinServlet;

public class CMSServlet extends SpringVaadinServlet
{
   private final VCMSContextInitializer contextInitializer;
   private final AppRepositoryQueries appRepositoryQueries;
   private final PageProvider seoPageProvider;

   public CMSServlet(VCMSContextInitializer contextInitializer,
         AppRepositoryQueries appRepositoryQueries,
         PageProvider seoPageProvider)
   {
      super();
      this.contextInitializer = contextInitializer;
      this.appRepositoryQueries = appRepositoryQueries;
      this.seoPageProvider = seoPageProvider;
   }

   @Override
   protected void servletInitialized() throws ServletException
   {
      super.servletInitialized();

      CustomizedSystemMessages messages = new CustomizedSystemMessages();
      messages.setSessionExpiredCaption("Sitzung abgelaufen");
      messages.setSessionExpiredMessage("Sie müssen sich neu anmelden. Bitte hier klicken oder ESC drücken.");

      getService().setSystemMessagesProvider(e -> messages);

      getService()
            .addSessionInitListener(event -> event.getSession().addUIProvider(
                  new CMSUIProvider(event.getSession(), appRepositoryQueries)));

      getService().addSessionInitListener(contextInitializer);

      VaadinService.getCurrent()
            .addSessionInitListener((com.vaadin.server.SessionInitEvent e) -> e
                  .getSession().addBootstrapListener(new ViewportBootstrapListener()));
      VaadinService.getCurrent()
            .addSessionInitListener((com.vaadin.server.SessionInitEvent e) -> e
                  .getSession().addBootstrapListener(
                        new SeoBootstrapListener(seoPageProvider)));
   }
}
