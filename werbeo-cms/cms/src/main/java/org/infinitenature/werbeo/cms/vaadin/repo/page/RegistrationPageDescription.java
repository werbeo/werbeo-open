package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class RegistrationPageDescription extends AbstractWerbeoPageDescription
{
   public RegistrationPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("registration");
      setUrlPrefix(PageRepositoryMock.URL_IMPRINT);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.REGISTRIERUNG.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .htmlComponentFromContentFile("registrierung"), true);
   }
}
