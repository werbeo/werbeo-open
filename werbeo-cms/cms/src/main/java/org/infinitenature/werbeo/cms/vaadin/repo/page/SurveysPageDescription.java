package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.surveytable.SurveyTablePlugin;

public class SurveysPageDescription extends AbstractWerbeoPageDescription
{

   public SurveysPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("surveys");
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.PROJEKTE.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER,
            new VCMSComponentDescriptionXML(SurveyTablePlugin.class.getName()),
            true);
   }

}
