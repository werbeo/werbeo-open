package org.infinitenature.werbeo.cms.vaadin;

import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSLayout;
import org.infinitenature.vct.repository.ComponentFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
public class SpringComponentFactory
      implements ComponentFactory, ApplicationContextAware
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(SpringComponentFactory.class);
   private ApplicationContext applicationContext;

   @SuppressWarnings("unchecked")
   private <T> T internalGet(String className)
   {
      try
      {
         Class<? extends T> componentClass = (Class<? extends T>) Class
               .forName(className);
         return applicationContext.getBean(componentClass);
      } catch (ClassNotFoundException | BeansException e)
      {
         LOGGER.error("No bean found for class: {}", className, e);
         return null;
      }
   }

   @Override
   public <T extends VCMSComponent> T getComponent(Class<T> clazz)
   {
      return applicationContext.getBean(clazz);
   }

   @Override
   public VCMSComponent get(String implementationClass)
   {
      return internalGet(implementationClass);
   }

   @Override
   public VCMSLayout getLayout(String layoutclass)
   {
      return internalGet(layoutclass);
   }

   @Override
   public VCMSAppTemplate getAppTemplate(String appTemplateImplementationClass)
   {
      return internalGet(appTemplateImplementationClass);
   }


   @Override
   public void setApplicationContext(ApplicationContext applicationContext)
   {
      this.applicationContext = applicationContext;
   }
}
