package org.infinitenature.werbeo.cms.vaadin.piwik;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.piwik.java.tracking.PiwikRequest;
import org.piwik.java.tracking.PiwikTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServletRequest;

public class Piwik
{
   private static final Logger LOGGER = LoggerFactory.getLogger(Piwik.class);
   private PiwikTracker tracker;
   private String ua;
   private String url;
   private String remoteAddr;
   private String userId = null;
   private final String authToken;
   private final String hostUrl;
   private int siteId;
   private String visitorId;

   public Piwik(String authToken, String hostUrl, int siteId)
   {
      super();
      this.authToken = authToken;
      this.hostUrl = hostUrl;
      this.siteId = siteId;
   }

   public void init(VaadinRequest vaadinRequest)
   {
      tracker = new PiwikTracker(hostUrl);
      VaadinServletRequest vaadinServletRequest = (VaadinServletRequest) vaadinRequest;
      HttpServletRequest servletRequest = vaadinServletRequest
            .getHttpServletRequest();
      ua = servletRequest.getHeader("User-Agent");
      url = servletRequest.getRequestURL().toString();
      visitorId = PiwikRequest.getRandomHexString(PiwikRequest.ID_LENGTH);
      remoteAddr = getClientIpAddress(servletRequest);
   }

   // from https://stackoverflow.com/a/21884642/2602034
   private String getClientIpAddress(HttpServletRequest request)
   {
      String xForwardedForHeader = request.getHeader("X-Forwarded-For");
      if (xForwardedForHeader == null)
      {
         return request.getRemoteAddr();
      } else
      {
         // As of https://en.wikipedia.org/wiki/X-Forwarded-For
         // The general format of the field is: X-Forwarded-For: client, proxy1,
         // proxy2 ...
         // we only want the client
         return new StringTokenizer(xForwardedForHeader, ",").nextToken()
               .trim();
      }
   }

   public CompletableFuture<HttpResponse> track(ViewChangeEvent event)
   {
      return CompletableFuture.supplyAsync(() -> doTrack(event));
   }

   public CompletableFuture<HttpResponse> track(URI url)
   {
      return CompletableFuture.supplyAsync(() -> doTrack(url));
   }

   private HttpResponse doTrack(URI url2)
   {
      PiwikRequest request;
      HttpResponse response = null;
      try
      {
         request = createPiwikRequest(url2);
         response = tracker.sendRequest(request);
         LOGGER.info("Response: {}", response);
         return response;
      } catch (Exception e)
      {
         LOGGER.error("Failure doing pwiki tracking request.", e);
         return response;
      }
   }

   private HttpResponse doTrack(ViewChangeEvent event)
   {
      PiwikRequest request;
      HttpResponse response = null;
      try
      {
         request = createPiwikRequest(event);
         response = tracker.sendRequest(request);
         LOGGER.info("Response: {}", response);
         return response;
      } catch (Exception e)
      {
         LOGGER.error("Failure doing pwiki tracking request.", e);
         return response;
      }

   }

   private PiwikRequest createPiwikRequest(ViewChangeEvent event)
         throws MalformedURLException
   {
      // url = UI.getCurrent().getPage().getLocation().toString();
      PiwikRequest request = new PiwikRequest(siteId, new URL(url));
      request.setHeaderUserAgent(ua);
      request.setAuthToken(authToken);
      if (userId != null)
      {
         request.setUserId(userId);
      }
      request.setVisitorId(visitorId);
      request.setVisitorIp(remoteAddr);
      String actionUrl = url + "#" + event.getViewName() + "/"
            + event.getParameters();
      LOGGER.info("Send piwik request for actionURL: {}", actionUrl);
      request.setActionUrlWithString(actionUrl);
      return request;
   }

   private PiwikRequest createPiwikRequest(URI url2)
         throws MalformedURLException
   {
      PiwikRequest request = new PiwikRequest(siteId, new URL(url2.toString()));
      request.setHeaderUserAgent(ua);
      request.setAuthToken(authToken);
      if (userId != null)
      {
         request.setUserId(userId);
      }
      request.setVisitorId(visitorId);
      request.setVisitorIp(remoteAddr);
      LOGGER.info("Send piwik request for actionURL: {}", url2);
      return request;
   }

   public void setUserId(String userId)
   {
      this.userId = userId;
   }

   public void setSiteId(int siteId)
   {
      this.siteId = siteId;
   }
}
