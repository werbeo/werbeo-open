package org.infinitenature.werbeo.cms.vaadin.repo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractPortalRepository
{
   public static final String PORTAL_NAME_FLORA_BB = "Flora-BB";
   public static final String PORTAL_NAME_FLORA_SN = "Flora-SN";
   public static final String PORTAL_NAME_FLORA_ST = "Flora-ST";
   Portal floraSN = createFloraSN();
   Portal floraST = createFloraST();
   Portal floraMV = createFloraMV();
   Portal floraBB = createFloraBB();
   Portal heuschrecken = createHeuschrecken();
   Portal dermaptera = createDermaptera();
   Portal heuschreckenRlp = createHeuschreckenRlp();
   Portal heuschreckenNrw = createHeuschreckenNrw();
   
   List<Portal> portals = new ArrayList<>();

   @Autowired
   private MessagesResource messages;

   @PostConstruct
   public void init()
   {
      Arrays.stream(new Portal[] { floraMV, floraBB, heuschrecken, dermaptera })
            .forEach(this::addSeoProperties);
      portals.addAll(Arrays.asList(floraBB, floraMV, floraSN, floraST,
            heuschrecken, heuschreckenNrw, heuschreckenRlp, dermaptera));
   }

   private void addSeoProperties(Portal portal)
   {
      String seoDescription = messages.getMessage(portal, "seo.description",
            Locale.GERMANY);
      if (!seoDescription.startsWith("!"))
      {
         portal.setDescription(seoDescription);
      }
      String seoImageUrl = messages.getMessage(portal, "seo.imageURL",
            Locale.GERMANY);
      if (!seoImageUrl.startsWith("!"))
      {
         portal.setImageURL(Optional.of(seoImageUrl));
      }
   }

   abstract Portal createDermaptera();

   abstract Portal createFloraBB();

   abstract Portal createFloraMV();
   
   abstract Portal createFloraSN();
   
   abstract Portal createFloraST();

   abstract Portal createHeuschrecken();
   
   abstract Portal createHeuschreckenRlp();
   
   abstract Portal createHeuschreckenNrw();

}