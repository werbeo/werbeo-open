package org.infinitenature.werbeo.client;

import org.infinitenature.werbeo.cms.vaadin.BaererTokenProvider;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class BearerTokenProviderImpl implements BaererTokenProvider
{

   @Override
   public String getCurrentToken()
   {
      KeycloakSecurityContext keycloakSecurityContext = getKeycloakSecurityContext();
      return keycloakSecurityContext != null
            ? keycloakSecurityContext.getTokenString()
            : null;
   }

   protected KeycloakSecurityContext getKeycloakSecurityContext()
   {
      Authentication authentication = Context.getCurrent()
            .getSpringAuthentication();
      KeycloakAuthenticationToken token;
      KeycloakSecurityContext context;

      if (authentication == null)
      {
         // No authentication means not authenticated
         return null;
      }

      if (org.springframework.security.authentication.AnonymousAuthenticationToken.class
            .isAssignableFrom(authentication.getClass()))
      {
         return null;
      }
      if (!KeycloakAuthenticationToken.class
            .isAssignableFrom(authentication.getClass()))
      {
         throw new IllegalStateException(String.format(
               "Cannot set authorization header because Authentication is of type %s but %s is required",
               authentication.getClass(), KeycloakAuthenticationToken.class));
      }

      token = (KeycloakAuthenticationToken) authentication;
      context = token.getAccount().getKeycloakSecurityContext();

      return context;
   }
}
