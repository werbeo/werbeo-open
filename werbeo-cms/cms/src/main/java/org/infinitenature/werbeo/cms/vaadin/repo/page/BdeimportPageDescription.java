package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrence.BdeimportPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class BdeimportPageDescription extends AbstractWerbeoPageDescription
{
   public BdeimportPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("bdeimport");
      setUrlPrefix(PageRepositoryMock.URL_DATA_ENTRY);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .breadcrumbComponent("Fundeingabe#BDE Import"), false);
      addComponent(SimpleLayout.AREA_CENTER, bdeimportComponent(), true);
      setAnonymousAllowed(false);
   }

   private VCMSComponentDescriptionXML bdeimportComponent()
   {
      return new VCMSComponentDescriptionXML(
            BdeimportPlugin.class.getName());
   }

}
