package org.infinitenature.werbeo.cms.vaadin.repo.page;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.plugin.breadcrump.BreadcrumpPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.HtmlPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.image.ImagePlugin;

public class ComponentDescriptionFactory
{
   private ComponentDescriptionFactory()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static VCMSComponentDescriptionXML htmlComponent(String content)
   {
      VCMSComponentDescriptionXML vcmsComponentDescription = new VCMSComponentDescriptionXML(
            HtmlPlugin.class.getName());

      vcmsComponentDescription.getParameter().put(HtmlPlugin.PARAMETER_CONTENT,
            content);
      return vcmsComponentDescription;
   }

   public static VCMSComponentDescriptionXML htmlComponentFromContentFile(
         String contentFileName)
   {
      String content;

      try (InputStream is = ComponentDescriptionFactory.class
            .getResourceAsStream("/" + contentFileName + "_"
                  + Context.getCurrent().getPortal().getPortalId() + ".MD");)
      {
         content = IOUtils.toString(is, Charset.forName("utf8"));

      } catch (IOException e)
      {
         content = contentFileName + " missing";
      }

      VCMSComponentDescriptionXML vcmsComponentDescription = new VCMSComponentDescriptionXML(
            HtmlPlugin.class.getName());

      vcmsComponentDescription.getParameter().put(HtmlPlugin.PARAMETER_CONTENT,
            content);
      return vcmsComponentDescription;
   }

   public static VCMSComponentDescriptionXML component(
         Class<? extends VCMSComponent> componentClass)
   {
      return new VCMSComponentDescriptionXML(componentClass.getName());
   }

   public static VCMSComponentDescriptionXML breadcrumbComponent(String content)
   {
      VCMSComponentDescriptionXML vcmsComponentDescription = new VCMSComponentDescriptionXML(
            BreadcrumpPlugin.class.getName());

      vcmsComponentDescription.getParameter().put(HtmlPlugin.PARAMETER_CONTENT,
            content);
      return vcmsComponentDescription;
   }

   public static VCMSComponentDescriptionXML imageComponent(String image)
   {
      VCMSComponentDescriptionXML vcmsComponentDescription = new VCMSComponentDescriptionXML(
            ImagePlugin.class.getName());

      vcmsComponentDescription.getParameter().put(ImagePlugin.PARAMETER_IMAGE,
            image);
      return vcmsComponentDescription;
   }

}
