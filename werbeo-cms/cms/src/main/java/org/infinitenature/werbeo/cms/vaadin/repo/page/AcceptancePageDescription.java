package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.breadcrump.BreadcrumpPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.HtmlPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.tc.AcceptancePlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class AcceptancePageDescription extends AbstractWerbeoPageDescription
{
   public AcceptancePageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("acceptance");
      setUrlPrefix(PageRepositoryMock.URL_ACCEPTANCE);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            breadcrumbComponent(NavigationConfig.BEDINGUNGEN.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .component(AcceptancePlugin.class), true);
   }

   private VCMSComponentDescriptionXML breadcrumbComponent(String content)
   {
      VCMSComponentDescriptionXML vcmsComponentDescription = new VCMSComponentDescriptionXML(
            BreadcrumpPlugin.class.getName());

      vcmsComponentDescription.getParameter().put(HtmlPlugin.PARAMETER_CONTENT,
            content);
      return vcmsComponentDescription;
   }
}
