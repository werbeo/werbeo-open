package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.iamap.IAMapPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class IAMapPageDescription extends AbstractWerbeoPageDescription
{

   public IAMapPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitle("Karte");
      setUrlPrefix(PageRepositoryMock.URL_IA_MAP);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.VERBREITUNG.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, speciesComponent(), true);
   }

   private VCMSComponentDescriptionXML speciesComponent()
   {
      return new VCMSComponentDescriptionXML(IAMapPlugin.class.getName());
   }

}
