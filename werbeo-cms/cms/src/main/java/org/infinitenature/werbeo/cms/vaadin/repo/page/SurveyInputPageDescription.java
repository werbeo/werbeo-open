package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.survey.SurveyInputPlugin;

public class SurveyInputPageDescription extends AbstractWerbeoPageDescription
{

   public SurveyInputPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("survey");
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.PROJEKT.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER,
            new VCMSComponentDescriptionXML(
                  SurveyInputPlugin.class.getName()),
            true);
      setAnonymousAllowed(false);
   }

}
