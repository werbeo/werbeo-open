package org.infinitenature.werbeo.cms.config;

import org.infinitenature.vct.VCMSContextInitializer;
import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.infinitenature.vct.vaadin.navigator.PageProvider;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.NavigationResolverImpl;
import org.infinitenature.werbeo.cms.vaadin.CMSServlet;
import org.infinitenature.werbeo.cms.vaadin.piwik.Piwik;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mobile.device.DeviceResolver;
import org.springframework.mobile.device.LiteDeviceResolver;

import com.vaadin.spring.annotation.UIScope;

@Configuration
public class WebConfig
{

   @Bean(name = "vaadinServlet")
   public CMSServlet cmsServlet(VCMSContextInitializer contextInitializer,
         AppRepositoryQueries appRepositoryQueries,
         @Qualifier("seoPageProvider") PageProvider pageProvider)
   {
      return new CMSServlet(contextInitializer, appRepositoryQueries,
            pageProvider);
   }

   @Bean
   public DeviceResolver deviceResolver()
   {
      return new LiteDeviceResolver();
   }

   @Bean
   public NavigationResolver navigationResolver()
   {
      return new NavigationResolverImpl();
   }

   @Bean
   @UIScope
   public Piwik piwik(@Value("${piwik.authToken}") String authToken,
         @Value("${piwik.hostUrl}") String hostUrl,
         @Value("${piwik.siteId}") int siteId)
   {
      return new Piwik(authToken, hostUrl, siteId);
   }
}
