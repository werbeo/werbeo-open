package org.infinitenature.werbeo.cms.vaadin.repo;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.description.VCMSPageDescription;
import org.infinitenature.vct.repository.PageRepository;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.plugin.debug.DebugPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.exportable.ExportTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.PrivacyPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.TocPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.iamap.IAMapPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.importtable.ImportPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrence.OccurrenceInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.MyOccurrenceTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.sample.SampleInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.species.SpeciesDetailPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.survey.SurveyInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.surveytable.SurveyTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.tc.AcceptancePlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.usertable.UserTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.page.AcceptancePageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.BdeimportPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.DebugPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.ExportPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.FloraBBMainPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.IAMapPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.ImportPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.ImprintPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.MyOccurrencesPageDesciption;
import org.infinitenature.werbeo.cms.vaadin.repo.page.OccurrencePageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.OccurrencesInputPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.OccurrencesPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.PrivacyPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.RegistrationPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.SampleInputPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.SpeciesDetailPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.SurveyInputPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.SurveysPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.TocPageDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.UsersPageDescription;

public class PageRepositoryMock implements PageRepository
{
   public static final String URL_MAIN = "main";
   public static final String URL_DEBUG = "debug";
   public static final String URL_OCCURRENCES = "funde";
   public static final String URL_MY_OCCURRENCES = "meineFunde";
   public static final String URL_DATA_ENTRY = "eingabe";
   public static final String URL_SAMPLE_ENTRY = "kartierliste";
   public static final String URL_SPECIES_DETAILS = "species";
   public static final String URL_OCCURRENCE_DETAILS = "fund";
   public static final String URL_IMPRINT = "imprint";
   public static final String URL_REGISTRATION = "registrierung";
   public static final String URL_BDEIMPORT = "bdeimport";
   public static final String URL_EXPORT = "exporte";
   public static final String URL_TOC = "nutzungsbedingungen";
   public static final String URL_PRIVACY = "datenschutzbedingungen";
   public static final String URL_ACCEPTANCE = "bedingungen";
   public static final String URL_SURVEYS = "projekte";
   public static final String URL_SURVEY = "projekt";
   public static final String URL_IMPORT = "importe";
   public static final String URL_USERS = "benutzer";
   public static final String URL_IA_MAP = "karte";

   private final MessagesResource messages;

   public PageRepositoryMock(MessagesResource messages)
   {
      super();
      this.messages = messages;
   }

   @Override
   public String getFragment(VCMSApp app,
         Class<? extends VCMSComponent> mainComponentClass)
   {
      if (DebugPlugin.class.equals(mainComponentClass))
      {
         return URL_DEBUG;
      }
      else if (SpeciesDetailPlugin.class.equals(mainComponentClass))
      {
         return URL_SPECIES_DETAILS;
      }
      else if(OccurrenceInputPlugin.class.equals(mainComponentClass))
      {
         return URL_DATA_ENTRY;
      }
      else if(SampleInputPlugin.class.equals(mainComponentClass))
      {
         return URL_SAMPLE_ENTRY;
      }
      else if (ExportTablePlugin.class.equals(mainComponentClass))
      {
         return URL_EXPORT;
      }
      else if (MyOccurrenceTablePlugin.class.equals(mainComponentClass))
      {
         return URL_MY_OCCURRENCES;
      }
      else if (AcceptancePlugin.class.equals(mainComponentClass))
      {
         return URL_ACCEPTANCE;
      }
      else if (TocPlugin.class.equals(mainComponentClass))
      {
         return URL_TOC;
      }
      else if (PrivacyPlugin.class.equals(mainComponentClass))
      {
         return URL_PRIVACY;
      }
      else if (SurveyTablePlugin.class.equals(mainComponentClass))
      {
         return URL_SURVEYS;
      } else if (SurveyInputPlugin.class.equals(mainComponentClass))
      {
         return URL_SURVEY;
      }
      else if (ImportPlugin.class.equals(mainComponentClass))
      {
         return URL_IMPORT;
      }
      else if (UserTablePlugin.class.equals(mainComponentClass))
      {
         return URL_USERS;
      }
      else if (IAMapPlugin.class.equals(mainComponentClass))
      {
         return URL_IA_MAP;
      }
      else
      {
         return URL_MAIN;
      }
   }

   @Override
   public VCMSPageDescription getDescription(VCMSApp app, String viewName)
         throws IllegalArgumentException
   {
      switch (viewName)
      {
      case URL_MAIN:
         if (AbstractPortalRepository.PORTAL_NAME_FLORA_BB
               .equals(app.getName()))
         {
            return new FloraBBMainPageDescription(messages);
         }else if(AbstractPortalRepository.PORTAL_NAME_FLORA_SN.equals(app.getName()))
         {
            return new OccurrencesPageDescription(messages);
         }
         else
         {
            return new MyOccurrencesPageDesciption(messages);
         }
      case URL_DEBUG:
         return new DebugPageDescription(messages);
      case URL_OCCURRENCES:
         return new OccurrencesPageDescription(messages);
      case URL_DATA_ENTRY:
         return new OccurrencesInputPageDescription(messages);
      case URL_SAMPLE_ENTRY:
         return new SampleInputPageDescription(messages);
      case URL_OCCURRENCE_DETAILS:
         return new OccurrencePageDescription(messages);
      case URL_SPECIES_DETAILS:
         return new SpeciesDetailPageDescription(messages);
      case URL_IMPRINT:
         return new ImprintPageDescription(messages);
      case URL_REGISTRATION:
         return new RegistrationPageDescription(messages);
      case URL_MY_OCCURRENCES:
         return new MyOccurrencesPageDesciption(messages);
      case URL_BDEIMPORT:
         return new BdeimportPageDescription(messages);
      case URL_EXPORT:
         return new ExportPageDescription(messages);
      case URL_ACCEPTANCE:
         return new AcceptancePageDescription(messages);
      case URL_TOC:
         return new TocPageDescription(messages);
      case URL_PRIVACY:
         return new PrivacyPageDescription(messages);
      case URL_SURVEYS:
         return new SurveysPageDescription(messages);
      case URL_SURVEY:
         return new SurveyInputPageDescription(messages);
      case URL_IMPORT:
         return new ImportPageDescription(messages);
      case URL_USERS:
         return new UsersPageDescription(messages);
      case URL_IA_MAP:
         return new IAMapPageDescription(messages);
      default:
         throw new IllegalArgumentException(
               "No page for " + viewName + " found in app " + app.getName());
      }

   }

   @Override
   public String getViewName(VCMSApp app, String viewAndParameters)
   {
      if (viewAndParameters.startsWith(URL_MAIN))
      {
         return URL_MAIN;
      } else if (viewAndParameters.isEmpty())
      {
         return URL_MAIN;
      } else if (viewAndParameters.startsWith(URL_DEBUG))
      {
         return URL_DEBUG;
      } else if (viewAndParameters.startsWith(URL_SPECIES_DETAILS))
      {
         return URL_SPECIES_DETAILS;
      } else if (viewAndParameters.startsWith(URL_DATA_ENTRY))
      {
         return URL_DATA_ENTRY;
      }else if (viewAndParameters.startsWith(URL_SAMPLE_ENTRY))
      {
         return URL_SAMPLE_ENTRY;
      } else if (viewAndParameters.startsWith(URL_OCCURRENCES))
      {
         return URL_OCCURRENCES;
      } else if (viewAndParameters.startsWith(URL_OCCURRENCE_DETAILS))
      {
         return URL_OCCURRENCE_DETAILS;
      } else if (viewAndParameters.startsWith(URL_IMPRINT))
      {
         return URL_IMPRINT;
      }
      else if (viewAndParameters.startsWith(URL_REGISTRATION))
      {
         return URL_REGISTRATION;
      } else if (viewAndParameters.startsWith(URL_MY_OCCURRENCES))
      {
         return URL_MY_OCCURRENCES;
      } else if (viewAndParameters.startsWith(URL_BDEIMPORT))
      {
         return URL_BDEIMPORT;
      } else if (viewAndParameters.startsWith(URL_EXPORT))
      {
         return URL_EXPORT;
      } else if (viewAndParameters.startsWith(URL_ACCEPTANCE))
      {
         return URL_ACCEPTANCE;
      }else if (viewAndParameters.startsWith(URL_TOC))
      {
         return URL_TOC;
      }else if (viewAndParameters.startsWith(URL_PRIVACY))
      {
         return URL_PRIVACY;
      } else if (viewAndParameters.startsWith(URL_SURVEYS))
      {
         return URL_SURVEYS;
      }else if (viewAndParameters.startsWith(URL_USERS))
      {
         return URL_USERS;
      } else if (viewAndParameters.startsWith(URL_SURVEY))
      {
         return URL_SURVEY;
      }
      else if (viewAndParameters.startsWith(URL_IMPORT))
      {
         return URL_IMPORT;
      }
      else if (viewAndParameters.startsWith(URL_IA_MAP))
      {
         return URL_IA_MAP;
      }
      else
      {
         return "";
      }
   }

}
