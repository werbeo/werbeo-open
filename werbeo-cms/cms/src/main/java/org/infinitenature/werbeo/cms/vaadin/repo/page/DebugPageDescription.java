package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.debug.DebugPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class DebugPageDescription extends AbstractWerbeoPageDescription
{

   public DebugPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("debug");
      setUrlPrefix(PageRepositoryMock.URL_MAIN);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.htmlComponent("<h1>DEBUG</h1>"), false);
      addComponent(SimpleLayout.AREA_CENTER, debugComponent(), true);
   }

   private VCMSComponentDescriptionXML debugComponent()
   {
      return new VCMSComponentDescriptionXML(DebugPlugin.class.getName());
   }

}
