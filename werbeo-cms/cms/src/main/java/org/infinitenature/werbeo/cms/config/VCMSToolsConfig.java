package org.infinitenature.werbeo.cms.config;

import org.infinitenature.vct.VCMSContextInitializer;
import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.infinitenature.vct.repository.AppTemplateFactory;
import org.infinitenature.vct.repository.AppTemplateRepositoryQueries;
import org.infinitenature.vct.repository.ComponentFactory;
import org.infinitenature.vct.repository.PageRepository;
import org.infinitenature.vct.vaadin.navigator.PageProvider;
import org.infinitenature.werbeo.cms.vaadin.ContextIntializer;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.SpringComponentFactory;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;
import org.infinitenature.werbeo.cms.vaadin.repo.PortalRepositoryImpl;
import org.infinitenature.werbeo.cms.vaadin.repo.PortalRepositoryMock;
import org.infinitenature.werbeo.cms.vaadin.repo.PortalTemplateRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.UIScope;

@Configuration
public class VCMSToolsConfig
{

   @Bean
   public PageRepository pageRepository(MessagesResource messagesResource)
   {
      return new PageRepositoryMock(messagesResource);
   }

   @Bean
   @Scope(org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON)
   public ComponentFactory componentFactory()
   {
      return new SpringComponentFactory();
   }

   @Profile("!test")
   @Bean
   public AppRepositoryQueries appRepo()
   {
      return new PortalRepositoryImpl();
   }

   @Bean
   public VCMSContextInitializer contextInitializer()
   {
      return new ContextIntializer();
   }

   @Profile("test")
   @Bean
   public AppRepositoryQueries appRepoMcok()
   {
      return new PortalRepositoryMock();
   }

   @Bean
   public AppTemplateRepositoryQueries appTemplateRepository()
   {
      return new PortalTemplateRepository();
   }

   @Bean
   public AppTemplateFactory appTemplateFactory(
         AppTemplateRepositoryQueries appTemplateRepositoryQueries,
         ComponentFactory componentFactory)
   {
      return new AppTemplateFactory(appTemplateRepositoryQueries,
            componentFactory);
   }

   @Bean
   @UIScope
   @Primary
   public PageProvider pageProvider(PageRepository pageRepository,
         ComponentFactory componentFactory)
   {
      PageProvider viewProvider = new PageProvider(pageRepository);
      viewProvider.setComponentFactory(componentFactory);
      return viewProvider;
   }

   @Bean("seoPageProvider")
   public PageProvider seoPageProvider(PageRepository pageRepository,
         ComponentFactory componentFactory)
   {
      PageProvider viewProvider = new PageProvider(pageRepository);
      viewProvider.setComponentFactory(componentFactory);
      return viewProvider;
   }
}
