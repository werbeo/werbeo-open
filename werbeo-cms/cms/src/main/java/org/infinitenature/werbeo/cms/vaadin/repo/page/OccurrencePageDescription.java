package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrence.BdeimportPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;


/**
 * Seems like, this is not used. To be deleted
 * @author hogen
 *
 */

public class OccurrencePageDescription extends AbstractWerbeoPageDescription
{
   public OccurrencePageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("occurrence");
      setUrlPrefix(PageRepositoryMock.URL_OCCURRENCE_DETAILS);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText()
                        + "#Beobachtung"),
            false);
      addComponent(SimpleLayout.AREA_CENTER, occurrenceDetailComponent(), true);
      setAnonymousAllowed(false);
   }

   private VCMSComponentDescriptionXML occurrenceDetailComponent()
   {
      return new VCMSComponentDescriptionXML(BdeimportPlugin.class.getName());
   }
}
