package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.MyOccurrenceTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class MyOccurrencesPageDesciption extends AbstractWerbeoPageDescription
{

   public MyOccurrencesPageDesciption(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("myoccurrences");
      setUrlPrefix(PageRepositoryMock.URL_OCCURRENCES);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.MEINE_FUNDE.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, occurrenceTableComponent(), true);
      setAnonymousAllowed(false);
   }

   private VCMSComponentDescriptionXML occurrenceTableComponent()
   {
      return new VCMSComponentDescriptionXML(
            MyOccurrenceTablePlugin.class.getName());
   }
}
