package org.infinitenature.werbeo.cms.config;

import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.herbariorum.client.HerbariorumGermanClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class IndexHerbariorumConfig
{

   @Bean
   @Scope(org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON)
   public HerbariorumClient herbariorumClient()
   {
      return new HerbariorumGermanClient();
   }
}
