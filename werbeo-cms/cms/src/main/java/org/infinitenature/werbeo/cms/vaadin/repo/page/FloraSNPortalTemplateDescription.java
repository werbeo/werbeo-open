package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSAppTemplateDescriptionXML;
import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.vaadin.app.FloraSNAppTemplate;
import org.infinitenature.werbeo.cms.vaadin.app.HeuschreckenAppTemplate;
import org.infinitenature.werbeo.cms.vaadin.plugin.login.LoginPlugin;
import org.infinitenature.werbeo.cms.vaadin.style.InfiniteNatureConfirmDialogFactory;
import org.vaadin.dialogs.ConfirmDialog;

public class FloraSNPortalTemplateDescription
      extends VCMSAppTemplateDescriptionXML
{
   public FloraSNPortalTemplateDescription()
   {
      ConfirmDialog.setFactory(new InfiniteNatureConfirmDialogFactory());
      addStaticComponent("LOGIN", loginCompnent());
      setImplementationClass(FloraSNAppTemplate.class.getName());
   }

   private VCMSComponentDescriptionXML loginCompnent()
   {
      return new VCMSComponentDescriptionXML(LoginPlugin.class.getName());
   }

}
