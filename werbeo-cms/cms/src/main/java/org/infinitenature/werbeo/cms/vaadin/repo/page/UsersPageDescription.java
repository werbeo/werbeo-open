package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.usertable.UserTablePlugin;

public class UsersPageDescription extends AbstractWerbeoPageDescription
{

   public UsersPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("users");
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.BENUTZER
                              .getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER,
            new VCMSComponentDescriptionXML(UserTablePlugin.class.getName()),
            true);
      setAnonymousAllowed(false);
   }

}
