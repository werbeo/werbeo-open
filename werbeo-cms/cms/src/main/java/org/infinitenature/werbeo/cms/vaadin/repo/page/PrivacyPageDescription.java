package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.PrivacyPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class PrivacyPageDescription extends AbstractWerbeoPageDescription
{
   public PrivacyPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("privacy");
      setUrlPrefix(PageRepositoryMock.URL_PRIVACY);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATENSCHUTZ.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .component(PrivacyPlugin.class), true);
   }
}
