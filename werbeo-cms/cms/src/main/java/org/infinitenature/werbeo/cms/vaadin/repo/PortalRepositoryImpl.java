package org.infinitenature.werbeo.cms.vaadin.repo;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortalRepositoryImpl extends AbstractPortalRepository
      implements AppRepositoryQueries
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PortalRepositoryImpl.class);

   @Override
   Portal createDermaptera()
   {
      return new Portal("Dermaptera Deutschland", 6, 7, ".dermaptera-portal.de");
   }

   @Override
   Portal createFloraBB()
   {
      return new Portal(PORTAL_NAME_FLORA_BB, 4, 2, ".flora-bb.de");
   }

   @Override
   Portal createFloraMV()
   {
      return new Portal("Flora-MV", 3, 5, ".flora-mv.de");
   }
   
   @Override
   Portal createFloraSN()
   {
      return new Portal(PORTAL_NAME_FLORA_SN, 9, 10, "sachsen-portal.");
   }
   
   @Override
   Portal createFloraST()
   {
      return new Portal(PORTAL_NAME_FLORA_ST, 10, 11, "flora-st.");
   }

   @Override
   Portal createHeuschrecken()
   {
      return new Portal("Heuschrecken Deutschland", 5, 6,
            "daten.heuschrecken-portal.de",
            "daten.test.heuschrecken-portal.de");
   }

   @Override
   Portal createHeuschreckenRlp()
   {
      return new Portal("Heuschrecken RLP", 7, 8,
            "daten.rlp.test.heuschrecken-portal.de",
            "daten.rlp.heuschrecken-portal.de");
   }

   @Override
   Portal createHeuschreckenNrw()
   {
      return new Portal("Heuschrecken NRW", 8, 9,
            "daten.nrw.heuschrecken-portal.de",
            "daten.nrw.test.heuschrecken-portal.de");
   }

   @Override
   public Portal getApp(String hostName)
   {
      if (StringUtils.isNotBlank(hostName))
      {
         return getPortal(hostName);
      }

      LOGGER.warn("No VCMSApp found for hostname: {}", hostName);
      return null;

   }

   private Portal getPortal(String hostName)
   {
      for(Portal portal : portals)
      {
         if(portal.matchUrlPattern(hostName))
         {
            return portal;
         }
      }
      
      return null;
   }

}
