package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.*;
import org.infinitenature.werbeo.cms.vaadin.app.FloraBBAppTemplate;
import org.infinitenature.werbeo.cms.vaadin.plugin.login.LoginPlugin;
import org.infinitenature.werbeo.cms.vaadin.style.InfiniteNatureConfirmDialogFactory;
import org.vaadin.dialogs.ConfirmDialog;

public class FloraBBPortalTemplateDescription
      extends VCMSAppTemplateDescriptionXML
{
   public FloraBBPortalTemplateDescription()
   {
      ConfirmDialog.setFactory(new InfiniteNatureConfirmDialogFactory());
      addStaticComponent("LOGIN", loginCompnent());

      setImplementationClass(FloraBBAppTemplate.class.getName());
   }

   private VCMSComponentDescriptionXML loginCompnent()
   {
      return new VCMSComponentDescriptionXML(LoginPlugin.class.getName());
   }
}
