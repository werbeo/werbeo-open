package org.infinitenature.werbeo.cms.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.context.SecurityContextImpl;
import com.vaadin.server.VaadinSession;

/**
 * A custom {@link SecurityContextHolderStrategy} that stores the
 * {@link SecurityContext} in the Vaadin Session.
 */
public class VaadinSessionSecurityContextHolderStrategy
      implements SecurityContextHolderStrategy
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(VaadinSessionSecurityContextHolderStrategy.class);

   @Override
   public void clearContext()
   {
      try
      {
         getSession().setAttribute(SecurityContext.class, null);
      } catch (IllegalStateException e)
      {
         LOGGER.warn("No vaadin session", e);
      }
   }

   @Override
   public SecurityContext getContext()
   {
      VaadinSession session = getSession();
      SecurityContext context = session.getAttribute(SecurityContext.class);
      if (context == null)
      {
         context = createEmptyContext();
         session.setAttribute(SecurityContext.class, context);
      }
      return context;
   }

   @Override
   public void setContext(SecurityContext context)
   {
      getSession().setAttribute(SecurityContext.class, context);
   }

   @Override
   public SecurityContext createEmptyContext()
   {
      return new SecurityContextImpl();
   }

   private static VaadinSession getSession()
   {
      VaadinSession session = VaadinSession.getCurrent();
      if (session == null)
      {
         throw new IllegalStateException(
               "No VaadinSession bound to current thread");
      }
      return session;
   }
}
