package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class FloraBBMainPageDescription extends AbstractWerbeoPageDescription
{

   public FloraBBMainPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("main");
      setUrlPrefix(PageRepositoryMock.URL_MAIN);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER, ComponentDescriptionFactory
            .htmlComponent("# Herzlich Willkommen auf Flora-BB,\n"
                  + "der Seite mit Verbreitungsdaten der Flora von Brandenburg\n\n"
                  + "Die Grundlage der vorliegenden Informationen zur Verbreitung der Gefäßpflanzen in Brandenburg stammt aus der Biotopkartierung BB, die mit Hilfe der Unterstützung des Landesamtes für Umwelt Brandenburg aufbereitet werden konnten, sowie einigen Daten aus der Sammlung des Botanischen Institutes der Universität Greifswald, die das Gebiet des Landes Brandenburg betreffen."),
            true);
   }

}
