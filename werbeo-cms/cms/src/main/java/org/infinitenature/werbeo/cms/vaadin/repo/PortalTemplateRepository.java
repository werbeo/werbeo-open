package org.infinitenature.werbeo.cms.vaadin.repo;

import org.infinitenature.vct.VCMSApp;
import org.infinitenature.vct.ViewMode;
import org.infinitenature.vct.description.VCMSAppTemplateDescription;
import org.infinitenature.vct.repository.AppTemplateRepositoryQueries;
import org.infinitenature.werbeo.cms.vaadin.repo.page.DermapteraPortalTemplateDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.FloraBBPortalTemplateDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.FloraMVPortalTemplateDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.FloraSNPortalTemplateDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.FloraSTPortalTemplateDescription;
import org.infinitenature.werbeo.cms.vaadin.repo.page.HeuschreckenPortalTemplateDescription;

public class PortalTemplateRepository implements AppTemplateRepositoryQueries
{

   @Override
   public VCMSAppTemplateDescription getAppTemplate(VCMSApp app,
         ViewMode viewMode)
   {
      if (app.getName().equals("Flora-MV"))
      {
         return new FloraMVPortalTemplateDescription();
      } 
      else if (app.getName().equals("Flora-BB"))
      {
         return new FloraBBPortalTemplateDescription();
      } 
      else if (app.getName().equals("Heuschrecken Deutschland"))
      {
         return new HeuschreckenPortalTemplateDescription();
      }
      else if (app.getName().equals("Heuschrecken RLP"))
      {
         return new HeuschreckenPortalTemplateDescription();
      }
      else if (app.getName().equals("Heuschrecken NRW"))
      {
         return new HeuschreckenPortalTemplateDescription();
      }
      else if (app.getName().equals("Dermaptera Deutschland"))
      {
         return new DermapteraPortalTemplateDescription();
      }
      else if (app.getName().equals("Flora-SN"))
      {
         return new FloraSNPortalTemplateDescription();
      } 
      else if (app.getName().equals("Flora-ST"))
      {
         return new FloraSTPortalTemplateDescription();
      } 
      else
      {
         return null;
      }
   }

}
