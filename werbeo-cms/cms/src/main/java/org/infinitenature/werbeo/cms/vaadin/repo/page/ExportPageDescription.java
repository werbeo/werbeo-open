package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.exportable.ExportTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class ExportPageDescription extends AbstractWerbeoPageDescription
{
   public ExportPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("export");
      setUrlPrefix(PageRepositoryMock.URL_MAIN);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.MEINE_EXPORTE.getBreadcrumbText()),
            false);
      addComponent(
            SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.component(ExportTablePlugin.class),
            true);
   }
}
