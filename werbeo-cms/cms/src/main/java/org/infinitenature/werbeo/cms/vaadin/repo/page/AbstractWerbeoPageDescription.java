package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSPageDescriptionXML;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

import com.vaadin.ui.UI;

public class AbstractWerbeoPageDescription extends VCMSPageDescriptionXML
{

   public AbstractWerbeoPageDescription(MessagesResource messagesResource)
   {
      super();
      this.messagesResource = messagesResource;
   }

   private final MessagesResource messagesResource;

   public void setTitleKey(String titleKey)
   {
      setTitle(messagesResource.getMessage(Context.getCurrent().getPortal(),
            "page.title." + titleKey, UI.getCurrent().getLocale()));
   }

   @Override
   public void setTitle(String title)
   {
      super.setTitle(title + " | ${APP_NAME}");
   }

   @Override
   public boolean equals(Object obj)
   {
      return super.equals(obj);
   }

   @Override
   public int hashCode()
   {
      return super.hashCode();
   }
}
