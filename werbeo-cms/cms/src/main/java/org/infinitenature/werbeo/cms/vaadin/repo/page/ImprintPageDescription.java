package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class ImprintPageDescription extends AbstractWerbeoPageDescription
{
   public ImprintPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("imprint");
      setUrlPrefix(PageRepositoryMock.URL_IMPRINT);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory
                  .breadcrumbComponent(NavigationConfig.IMPRESSUM
                        .getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, htmlComponent(), true);
   }

   private VCMSComponentDescriptionXML htmlComponent()
   {
      String contentFileName = "imprint";
      return ComponentDescriptionFactory
            .htmlComponentFromContentFile(contentFileName);
   }


}
