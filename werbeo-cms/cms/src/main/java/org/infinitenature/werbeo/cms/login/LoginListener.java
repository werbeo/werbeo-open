package org.infinitenature.werbeo.cms.login;

import org.infinitenature.service.v1.types.Person;
import org.infinitenature.vct.Authentication;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.context.SecurityContextHolder;
import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
public class LoginListener
      implements ApplicationListener<InteractiveAuthenticationSuccessEvent>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(LoginListener.class);

   @Override
   public void onApplicationEvent(InteractiveAuthenticationSuccessEvent event)
   {
      LOGGER.info("Principal: {}", SecurityContextHolder.getContext()
            .getAuthentication().getPrincipal());
      LOGGER.info("Authorities: {}", SecurityContextHolder.getContext()
            .getAuthentication().getAuthorities());
   }

}
