package org.infinitenature.werbeo.cms.vaadin.repo;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vct.repository.AppRepositoryQueries;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortalRepositoryMock extends AbstractPortalRepository
      implements AppRepositoryQueries
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PortalRepositoryMock.class);

   @Override
   Portal createFloraMV()
   {
      return new Portal("Flora-MV", 2, 4);
   }

   @Override
   Portal createFloraBB()
   {
      return new Portal(PORTAL_NAME_FLORA_BB, 3, 3);
   }

   @Override
   Portal createHeuschrecken()
   {
      return new Portal("Heuschrecken Deutschland", 6, 6);
   }

   @Override
   Portal createDermaptera()
   {
      return new Portal("Dermaptera Deutschland", 7, 7);
   }

   @Override
   Portal createHeuschreckenRlp()
   {
      return new Portal("Heuschrecken RLP", 7, 8);
   }

   @Override
   Portal createHeuschreckenNrw()
   {
      return new Portal("Heuschrecken NRW", 8, 9);
   }

   @Override
   public Portal getApp(String hostName)
   {
      if (StringUtils.isNotBlank(hostName))
      {
         if (hostName.toLowerCase().endsWith("localhost"))
         {
            return floraMV;
         } else if (hostName.toLowerCase().endsWith("127.0.0.1"))
         {
            return heuschrecken;
         } else
         {
            return floraST;
         }
      }
      LOGGER.warn("No VCMSApp found for hostname: {}", hostName);
      return null;

   }

   @Override
   Portal createFloraSN()
   {
      return new Portal(PORTAL_NAME_FLORA_SN,9, 10);
   }
   
   @Override
   Portal createFloraST()
   {
      return new Portal(PORTAL_NAME_FLORA_ST,10, 11);
   }
}
