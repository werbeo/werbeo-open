package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.sample.SampleInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class SampleInputPageDescription extends AbstractWerbeoPageDescription
{
   public SampleInputPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("sampleinput");
      setUrlPrefix(PageRepositoryMock.URL_SAMPLE_ENTRY);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_EINGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.KARTIERLISTE.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, occurrenceTableComponent(), true);
      setAnonymousAllowed(false);
   }

   private VCMSComponentDescriptionXML occurrenceTableComponent()
   {
      return new VCMSComponentDescriptionXML(SampleInputPlugin.class.getName());
   }
}
