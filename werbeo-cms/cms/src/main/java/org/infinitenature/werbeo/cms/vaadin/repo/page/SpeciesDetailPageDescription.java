package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.species.SpeciesDetailPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class SpeciesDetailPageDescription extends AbstractWerbeoPageDescription
{
   public SpeciesDetailPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitle("${TAXON}");
      setUrlPrefix(PageRepositoryMock.URL_SPECIES_DETAILS);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.VERBREITUNG.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, speciesComponent(), true);
   }

   private VCMSComponentDescriptionXML speciesComponent()
   {
      return new VCMSComponentDescriptionXML(
            SpeciesDetailPlugin.class.getName());
   }
}
