package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.filter.FilterPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.OccurrenceTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class OccurrencesPageDescription extends AbstractWerbeoPageDescription
{
   public OccurrencesPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("occurrences");
      setUrlPrefix(PageRepositoryMock.URL_OCCURRENCES);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_AUSGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.FUNDE.getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, filterComponent());
      addComponent(SimpleLayout.AREA_CENTER, occurrenceTableComponent(), true);
   }

   private VCMSComponentDescriptionXML filterComponent()
   {
      return new VCMSComponentDescriptionXML(
            FilterPlugin.class.getName());
   }

   private VCMSComponentDescriptionXML occurrenceTableComponent()
   {
      return new VCMSComponentDescriptionXML(
            OccurrenceTablePlugin.class.getName());
   }
}
