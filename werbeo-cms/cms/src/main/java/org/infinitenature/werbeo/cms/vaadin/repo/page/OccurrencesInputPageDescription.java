package org.infinitenature.werbeo.cms.vaadin.repo.page;

import org.infinitenature.vct.description.VCMSComponentDescriptionXML;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.component.SimpleLayout;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrence.OccurrenceInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.repo.PageRepositoryMock;

public class OccurrencesInputPageDescription
      extends AbstractWerbeoPageDescription
{
   public OccurrencesInputPageDescription(MessagesResource messagesResource)
   {
      super(messagesResource);
      setTitleKey("occurrenceinput");
      setUrlPrefix(PageRepositoryMock.URL_DATA_ENTRY);
      setLayout(SimpleLayout.class.getName());
      addComponent(SimpleLayout.AREA_CENTER,
            ComponentDescriptionFactory.breadcrumbComponent(
                  NavigationConfig.DATEN_EINGABE.getBreadcrumbText() + "#"
                        + NavigationConfig.EINZELFUND_EINGABE
                              .getBreadcrumbText()),
            false);
      addComponent(SimpleLayout.AREA_CENTER, occurrenceTableComponent(), true);
      setAnonymousAllowed(false);
   }

   private VCMSComponentDescriptionXML occurrenceTableComponent()
   {
      return new VCMSComponentDescriptionXML(
            OccurrenceInputPlugin.class.getName());
   }
}
