package org.infinitenature.werbeo.cms.navigation;

import org.infinitenature.vct.repository.PageRepository;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.plugin.exportable.ExportTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrence.OccurrenceInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.MyOccurrenceTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.OccurrenceTablePlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.sample.SampleInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.species.SpeciesDetailPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.survey.SurveyInputPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.surveytable.SurveyTablePlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.ui.UI;

public class NavigationResolverImpl implements NavigationResolver
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(NavigationResolverImpl.class);
   @Autowired
   private PageRepository pageRepository;

   @Override
   public void navigateTo(NavigationTarget target)
   {
      String fragment = null;
      if (target instanceof OccurrenceEdit)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               OccurrenceInputPlugin.class);
         fragment += "/" + ((OccurrenceEdit) target).getOccurrenceUUID()
               ;

      }
      if (target instanceof SampleEdit)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               SampleInputPlugin.class);
         fragment += "/" + ((SampleEdit) target).getSampleUUID()
               ;

      }
      if (target instanceof OccurrenceNew)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               OccurrenceInputPlugin.class);
      }
      if (target instanceof SampleNew)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               SampleInputPlugin.class);
      }
      if (target instanceof TaxonDetail)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               SpeciesDetailPlugin.class);
         fragment += "/" + ((TaxonDetail) target).getTaxon().replace(" ", "%20");

      }
      if (target instanceof OccurrenceExport)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               ExportTablePlugin.class);
      }
      if (target instanceof Occurrences)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               OccurrenceTablePlugin.class);
      }
      if (target instanceof MyOccurrences)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               MyOccurrenceTablePlugin.class);
      }
      if (target instanceof Surveys)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               SurveyTablePlugin.class);
      }
      if (target instanceof SurveyEdit)
      {
         fragment = pageRepository.getFragment(Context.getCurrent().getApp(),
               SurveyInputPlugin.class);
         fragment += "/" + ((SurveyEdit) target).getSurveyId();
      }
      LOGGER.info("Navigating to {}", fragment);
      UI.getCurrent().getNavigator().navigateTo(fragment);
   }

}
