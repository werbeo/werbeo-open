package org.infinitenature.werbeo.testing.cms.utils;

import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.NavigationTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NavigationResolverMock implements NavigationResolver
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(NavigationResolverMock.class);

   @Override
   public void navigateTo(NavigationTarget target)
   {
      LOGGER.info("NavigationTarget: {}", target);
   }
}