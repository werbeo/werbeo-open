package org.infinitenature.werbeo.testing.client;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.infinitenature.service.v1.resources.mock.MockFactory;
import org.infinitenature.werbeo.client.Werbeo;

public class WerbeoMockFactory
{
   private MockFactory mockFacotry = new MockFactory();

   public Werbeo get()
   {
      Werbeo mock = mock(Werbeo.class);

      when(mock.occurrences()).thenReturn(mockFacotry.getOccurrenceResource());
      when(mock.taxa()).thenReturn(mockFacotry.getTaxaResource());
      when(mock.samples()).thenReturn(mockFacotry.getSamplesResource());
      when(mock.surveys()).thenReturn(mockFacotry.getSurveysResource());
      when(mock.occurrenceExports())
            .thenReturn(mockFacotry.getOccurrenceExportResource());
      when(mock.portals()).thenReturn(mockFacotry.getPortalResource());
      when(mock.people()).thenReturn(mockFacotry.getPeopleResource());
      return mock;
   }
}
