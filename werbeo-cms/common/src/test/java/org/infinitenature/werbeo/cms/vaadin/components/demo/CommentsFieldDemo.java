package org.infinitenature.werbeo.cms.vaadin.components.demo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.types.AbstractComment;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.comment.CommentsField;
import org.vaadin.addonhelpers.AbstractTest;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

public class CommentsFieldDemo extends AbstractTest
{

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      CommentsField commentsField = new CommentsField(
            new MessagesResourceMock());
      commentsField.setCaption("Kommentare");
      // commentsField.setValue(createComments(10));
      commentsField.setHeight("500px");
      commentsField.setWidth("800px");
      return new MVerticalLayout(commentsField, new Label("Bla"));
   }

   private List<AbstractComment> createComments(int i)
   {
      List<AbstractComment> comments = new ArrayList<>();
      for (int c = 0; c < i; c++)
      {
         AbstractComment comment = new OccurrenceComment();
         comment.setCreatedBy("xxx@xxxx.net");
         comment.setCreationDate(LocalDateTime.now());
         comment.setComment("Das ist **ziemlich** wichtig\n\n<sup>klein</sub>");
         comments.add(comment);
      }
      return comments;
   }

}
