package org.infinitenature.werbeo.cms.vaadin.components.demo;

// in the Browser (for IntelliJ):
// localhost:9998/org.infinitenature.werbeo.cms.vaadin.components.demo.HerbariorumDemoNoConnection

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import org.infinitenature.herbariorum.client.Herbariorum;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.herbariorum.entities.Institution;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.indexherbariorum.HerbariorumField;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.addonhelpers.AbstractTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class HerbariorumDemoNoConnection extends AbstractTest
{
   @Autowired
   HerbariorumClient client;

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      VerticalLayout verticalLayout = new VerticalLayout();

      HerbariorumField herbariorumField = new HerbariorumField(new MessagesResourceMock());
      herbariorumField.init(new DemoClient());
      verticalLayout.addComponent(herbariorumField);

      return verticalLayout;
   }

   class DemoClient implements HerbariorumClient
   {
      List<Institution> institutions = new ArrayList<>();
      List<String> countries = new ArrayList<>();
      Institution institutionTest = new Institution();

      DemoClient()
      {
         institutionTest.setOrganization("TestOrganisation");
         institutions.add(institutionTest);
         countries.add("Musterland");
      }

      @Override
      public List<Institution> findInstitutionsByCountryAndName(String s,
            String s1)
      {
         return Collections.EMPTY_LIST;
      }

      @Override
      public int countInstitutionsByCountryAndName(String s, String s1)
      {
         return 0;
      }

      @Override
      public List<String> findCountriesByName(String s)
      {
         return Collections.EMPTY_LIST;
      }

      @Override
      public int countCountriesByName(String s)
      {
         return 0;
      }

      @Override
      public Institution getByCode(String s)
      {
         return null;
      }

      @Override
      public boolean isValidCountry(String s)
      {
         return true;
      }

      @Override
      public boolean isServiceAvailable()
      {
         return false;
      }

      @Override
      public boolean initCache()
      {
         return false;
      }

      @Override
      public void setHerbariorum(Herbariorum herbariorum)
      {

      }

      @Override
      public void setInstitutions(List<Institution> list)
      {

      }

      @Override
      public void setCountries(Collection<String> collection)
      {

      }

      @Override
      public void setGermanCountries()
      {

      }
   }
}
