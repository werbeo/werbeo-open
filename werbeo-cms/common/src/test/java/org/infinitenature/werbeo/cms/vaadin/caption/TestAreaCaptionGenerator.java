package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.Area;
import org.infinitenature.werbe.test.support.vaadin.SessionUtil;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class TestAreaCaptionGenerator
{

   private AreaCaptionGenerator generatorUT;

   @BeforeEach
   void setUp() throws Exception
   {
      generatorUT = new AreaCaptionGenerator(new MessagesResourceMock());
      Context context = new Context();
      context.setApp(new Portal("test", 2, 2));
      SessionUtil.mockVaadin(Locale.GERMANY, context);
   }

   @Test
   void test()
   {
      assertThat(generatorUT.apply(Area.LESS_ONE), containsString("1m"));
   }

   @Test
   void testNull()
   {
      assertThat(generatorUT.apply(null), containsString("keine Angabe"));
   }

}
