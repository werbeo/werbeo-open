package org.infinitenature.werbeo.cms.vaadin;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

class TestVaadinUtils
{
   private TextField a = new TextField("A");
   private Button b = new Button("B");
   private TextField c = new TextField("C");

   private HasComponents container = new VerticalLayout(a, b, c);

   @Test
   void testComponentStreamHasComponents()
   {

      Stream<Component> stream = VaadinUtils.componentStream(container);

      List<Component> list = stream.collect(Collectors.toList());

      assertAll(() -> assertThat(list, hasSize(3)),
            () -> assertThat(list.get(0), is(a)),
            () -> assertThat(list.get(1), is(b)),
            () -> assertThat(list.get(2), is(c)));
   }

   @Test
   void testComponentStreamHasComponentsClassOfT()
   {
      Stream<Button> stream = VaadinUtils.componentStream(container,
            Button.class);

      List<Button> list = stream.collect(Collectors.toList());

      assertAll(() -> assertThat(list, hasSize(1)),
            () -> assertThat(list.get(0), is(b)));
   }

}
