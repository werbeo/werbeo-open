package org.infinitenature.werbeo.cms.vaadin.components.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentField;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentUploadField;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsField;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsUploadField;
import org.vaadin.addonhelpers.AbstractTest;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MFormLayout;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

public class DocumentFieldDemo extends AbstractTest
{
   private final MessagesResource messages = new MessagesResourceMock();

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
      List<Document> documents = new ArrayList<>();

      Document image = new Document(DocumentType.IMAGE,
            new Link("self", "https://picsum.photos/200/300"), "Lorem picsum");
      DocumentField withImage = new DocumentField(messages);
      HashSet<Operation> allowedOperations = new HashSet<>();
      allowedOperations.add(Operation.UPDATE);
      withImage.setAllowedOperations(allowedOperations);
      documents.add(image);
      Document audio = new Document(DocumentType.AUDIO, new Link("self",
            "http://ichbinsprecher.de/wp-content/uploads/2011/04/Lorem-Ipsum-Commercial.mp3?_=1"),
            "Lorem audio, aber ganz schön lang und hoffentlich bricht das auch um.");
      DocumentField withAudio = new DocumentField(messages);
      withAudio.setAllowedOperations(allowedOperations);
      documents.add(audio);

      withAudio.setValue(audio);
      withImage.setValue(image);

      DocumentsField documentsField = new DocumentsField(messages);
      LabelField<String> valid = new LabelField<>("Valid");

      documentsField.setValue(documents);
      DocumentsUploadField documentsUploadField = new DocumentsUploadField(
            messages);
      documentsUploadField.addValueChangeListener(e ->
      {
         if (documentsUploadField.isValid())
         {
            valid.setValue("VALID");
         } else
         {
            valid.setValue("INVALID");
         }
      });

      DocumentUploadField documentUploadField = new DocumentUploadField(
            messages);
      documentUploadField.setCaption("With 1MB upload limit");
      documentUploadField.setMaxUploadSize(1);
      return new MFormLayout(new DocumentField(messages), withImage,
            withAudio, documentsField, new DocumentsField(messages),
            documentUploadField, new DocumentUploadField(messages),
            documentsUploadField, valid).withWidth("500px");
   }

}
