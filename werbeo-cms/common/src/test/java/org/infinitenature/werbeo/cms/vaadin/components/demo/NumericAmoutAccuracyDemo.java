package org.infinitenature.werbeo.cms.vaadin.components.demo;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.numericamount.NumericAmountWithAccuracy;
import org.infinitenature.werbeo.cms.vaadin.components.numericamount.NumericAmountWithAccuracyField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

public class NumericAmoutAccuracyDemo extends AbstractTest
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(NumericAmoutAccuracyDemo.class);
   private final MessagesResource messages = new MessagesResourceMock();

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
      NumericAmountWithAccuracyField numericAmountWithAccuracyField = new NumericAmountWithAccuracyField(
            messages);
      numericAmountWithAccuracyField
            .addValueChangeListener(this::onValueChange);
      return numericAmountWithAccuracyField;
   }

   private void onValueChange(ValueChangeEvent<NumericAmountWithAccuracy> event)
   {
      LOGGER.info("New value {}, old value {}", event.getValue(),
            event.getOldValue());
   }
}
