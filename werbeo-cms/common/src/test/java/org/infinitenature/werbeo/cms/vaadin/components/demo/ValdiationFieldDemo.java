package org.infinitenature.werbeo.cms.vaadin.components.demo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.ValidationField;
import org.vaadin.addonhelpers.AbstractTest;
import org.vaadin.viritin.layouts.MFormLayout;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

@Theme("flora-mv")
public class ValdiationFieldDemo extends AbstractTest
{

   private static final MessagesResourceMock messages = new MessagesResourceMock();

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
      return new MFormLayout(createComponents());
   }

   public Component[] createComponents()
   {
      List<Component> components = new ArrayList<>();
      for (boolean readOnly : new boolean[] { true, false })
      {
         ValidationField emptyField = new ValidationField(messages);
         emptyField.setCaption("readOnly: " + readOnly + " - status: " + null);
         emptyField.setValue(null);
         emptyField.setReadOnly(readOnly);
         components.add(emptyField);
         for (ValidationStatus validationStatus : ValidationStatus.values())
         {
            ValidationField field = new ValidationField(messages);
            Validation validation = new Validation();
            validation.setStatus(validationStatus);
            validation.setValidationTime(LocalDateTime.now());
            validation.setValidator(new Person(11, "Hans", "Validator",
                  "abc@validation-service.de"));
            field.setValue(validation);
            field.setReadOnly(readOnly);
            field.setCaption(
                  "readOnly: " + readOnly + " - status: " + validationStatus);
            components.add(field);
         }
      }
      return components.toArray(new Component[] {});
   }
}
