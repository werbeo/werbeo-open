package org.infinitenature.werbeo.cms.vaadin.components.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsField;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.ui.Component;

public class DocumentsFieldDemo extends AbstractTest
{
   private final MessagesResource messages = new MessagesResourceMock();

   @Override
   public Component getTestComponent()
   {

      DocumentsField field = new DocumentsField(messages);
      HashSet<Operation> allowedOperations = new HashSet<>();
      allowedOperations.add(Operation.UPDATE);

      field.setAllowedOperations(allowedOperations);
      List<Document> documents = new ArrayList<>();
      documents.add(new Document(DocumentType.IMAGE,
            new Link("self", "https://picsum.photos/id/107/200/300"),
            "Lorem picsum"));
      documents.add(new Document(DocumentType.IMAGE_THUMB,
            new Link("self", "https://picsum.photos/id/107/100/150"),
            "Lorem picsum"));
      documents.add(new Document(DocumentType.AUDIO, new Link("self",
            "http://ichbinsprecher.de/wp-content/uploads/2011/04/Lorem-Ipsum-Commercial.mp3?_=1"),
            "Lorem audio, aber ganz schön lang und hoffentlich bricht das auch um."));
      field.setValue(documents);
      return field;
   }
}
