package org.infinitenature.werbeo.cms.vaadin.components.numericamount;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.infinitenature.service.v1.types.Occurrence.NumericAmountAccuracy;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.vaadin.data.HasValue;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.ui.Button.ClickEvent;

class TestNumericAmountWithAccuracyField
{
   private NumericAmountWithAccuracyField fieldUT;

   @BeforeEach
   void setUp()
   {
      fieldUT = new NumericAmountWithAccuracyField(new MessagesResourceMock());
   }

   @Test
   @DisplayName("Test setting and getting value")
   void test001()
   {
      NumericAmountWithAccuracy value = NumericAmountWithAccuracy.of(33,
            NumericAmountAccuracy.APPROXIMATE);

      fieldUT.setValue(value);

      assertThat(fieldUT.getValue(), is(value));
   }

   @Test
   @DisplayName("Test change numeric value")
   void test002()
   {
      fieldUT.setValue(NumericAmountWithAccuracy.of(33,
            NumericAmountAccuracy.APPROXIMATE));

      ValueChangeListener<NumericAmountWithAccuracy> listener = mock(
            ValueChangeListener.class);
      fieldUT.addValueChangeListener(listener);
      HasValue<Integer> hasValue = mock(HasValue.class);
      when(hasValue.getValue()).thenReturn(34);
      fieldUT.onNumericAmountChange(
            new ValueChangeEvent<Integer>(null, hasValue, 33, false));

      verify(listener, times(1)).valueChange(any());
   }

   @Test
   @DisplayName("Test set numeric value")
   void test003()
   {
      fieldUT.setValue(null);

      ValueChangeListener<NumericAmountWithAccuracy> listener = mock(
            ValueChangeListener.class);
      fieldUT.addValueChangeListener(listener);
      HasValue<Integer> hasValue = mock(HasValue.class);
      when(hasValue.getValue()).thenReturn(34);
      fieldUT.onNumericAmountChange(
            new ValueChangeEvent<Integer>(null, hasValue, null, false));

      verify(listener, times(1)).valueChange(any());
   }

   @Test
   @DisplayName("Test set accuracy to approx")
   void test004()
   {
      fieldUT.setValue(
            NumericAmountWithAccuracy.of(33, NumericAmountAccuracy.MORETHAN));

      ValueChangeListener<NumericAmountWithAccuracy> listener = mock(
            ValueChangeListener.class);
      fieldUT.addValueChangeListener(listener);
      fieldUT.onApproximateClick(new ClickEvent(fieldUT));

      verify(listener, times(1)).valueChange(any());
   }
}
