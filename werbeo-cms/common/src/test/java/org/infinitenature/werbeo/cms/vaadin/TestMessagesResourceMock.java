package org.infinitenature.werbeo.cms.vaadin;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestMessagesResourceMock
{
   private MessagesResource messagesResourceUT = new MessagesResourceMock();

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   void testGetMessage()
   {
      String message = messagesResourceUT.getMessage(new Portal("test", 1, 1),
            "export.csv", Locale.GERMANY);
      assertThat(message, is("csv Export"));
   }

   @Test
   void testGetEntityField()
   {
      String message = messagesResourceUT.getEntityField(
            new Portal("test", Integer.MAX_VALUE, Integer.MAX_VALUE),
            "TaxonComboBox", "MANUAL", Locale.GERMANY);
      assertThat(message, is(
            "Geben Sie Buchstaben des Gattungsnamens oder des Artepithetons ein. Ein Leerzeichen sucht im nächsten Wort."));
   }

   @Test
   void testGetEntityField_portalSepecific()
   {
      String message = messagesResourceUT.getEntityField(
            new Portal("test", 4, 4), "TaxonComboBox", "MANUAL", Locale.GERMANY);
      assertThat(message, is(
            "Geben Sie Buchstaben des Gattungsnamens oder des Artepithetons ein. Ein Leerzeichen sucht im nächsten Wort. Beispiel: ge b für Geranium bohemicum oder ac su ca für Acer campestre subsp. campestre, aber auch glycy für Astragalus glycyphyllos."));
   }

   @Test
   void testGetEntityField_portalSepecificMissingKey()
   {
      String message = messagesResourceUT.getEntityField(
            new Portal("test", 4, 4), "TaxonComboBoxXX", "MANUAL",
            Locale.GERMANY);
      assertThat(message, is("!TaxonComboBoxXX.MANUAL!"));
   }
}