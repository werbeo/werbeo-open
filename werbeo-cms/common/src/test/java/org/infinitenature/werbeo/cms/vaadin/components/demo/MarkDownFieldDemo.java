package org.infinitenature.werbeo.cms.vaadin.components.demo;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.MarkDownField;
import org.vaadin.addonhelpers.AbstractTest;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

public class MarkDownFieldDemo extends AbstractTest
{

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
      MarkDownField markDownField = new MarkDownField(
            new MessagesResourceMock());
      markDownField.setCaption("A MarkDownField");
      markDownField.setHeight("600px");
      return new MVerticalLayout(markDownField, new MButton("ReadOnly",
            e -> markDownField.setReadOnly(!markDownField.isReadOnly())));
   }
}
