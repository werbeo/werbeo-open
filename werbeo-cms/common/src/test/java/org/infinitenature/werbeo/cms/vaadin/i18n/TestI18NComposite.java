package org.infinitenature.werbeo.cms.vaadin.i18n;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.text.DecimalFormat;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestI18NComposite
{
   private I18NComposite i18NCompositeUT = new I18NComposite(null)
   {

      @Override
      public void applyLocalizations()
      {
      }
   };

   @BeforeEach
   void setUp() throws Exception
   {
      i18NCompositeUT.setLocale(Locale.GERMAN);
   }


   @Test
   void test_getDecimalFormat_millions()
   {
      DecimalFormat format = i18NCompositeUT.getDecimalFormat();

      assertThat(format.format(2100123), is("2.100.123"));
   }

   @Test
   void test_getDecimalFormat_thousends()
   {
      DecimalFormat format = i18NCompositeUT.getDecimalFormat();

      assertThat(format.format(2100), is("2.100"));
   }
}
