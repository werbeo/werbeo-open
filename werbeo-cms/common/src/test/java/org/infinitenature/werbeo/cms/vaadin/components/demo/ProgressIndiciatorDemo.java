package org.infinitenature.werbeo.cms.vaadin.components.demo;

import java.util.concurrent.CompletableFuture;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.ProgressIndicator;
import org.vaadin.addonhelpers.AbstractTest;
import org.vaadin.viritin.button.MButton;

import com.vaadin.annotations.Push;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

@Push(transport = Transport.LONG_POLLING)
public class ProgressIndiciatorDemo extends AbstractTest
{

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      return new MButton(VaadinIcons.START_COG, "Start", e -> start());
   }

   private void start()
   {
      UI ui = UI.getCurrent();

      ProgressIndicator pi = new ProgressIndicator(new MessagesResourceMock());

      pi.setSteps(5);
      Window window = new Window("Fortschritt", pi);
      window.setWidth("30%");
      window.center();
      window.setModal(true);
      window.setClosable(false);
      ui.addWindow(window);

      CompletableFuture.runAsync(() ->
      {
         for (int i = 1; i <= 5; i++)
         {
            try
            {

               Thread.sleep(3 * 1000);
               ui.access(() -> pi.success("Save"));
               System.out.println("SUCCESS");

            } catch (InterruptedException e)
            {
               e.printStackTrace();
            }
         }
         ui.access(() -> window.setClosable(true));
      });
   }

}
