package org.infinitenature.werbeo.cms.formatter;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class TestPublicationFormatter
{
   @Test
   public void testFormatShort_null()
   {
      assertThat(PublicationFormatter.formatShort(null), is(""));
   }
}
