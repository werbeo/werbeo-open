package org.infinitenature.werbeo.cms.formatter;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.service.v1.types.Person;
import org.junit.Test;

public class TestPersonFormatter
{

   @Test
   public void testFormat_null()
   {
      assertThat(PersonFormatter.format(null), is(""));
   }

   @Test
   public void testFormat_fullName()
   {
      Person person = new Person();
      person.setFirstName("Hans");
      person.setLastName("Wurst");

      assertThat(PersonFormatter.format(person), is("Hans Wurst"));
   }

   @Test
   public void testFormat_onlyLastName()
   {
      Person person = new Person();
      person.setLastName("Wurst");

      assertThat(PersonFormatter.format(person), is("Wurst"));
   }
}
