package org.infinitenature.werbeo.cms.vaadin;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

class TestContext
{
   private Context contextUT;
   private List<GrantedAuthority> grantedAuthorities;

   @BeforeEach
   void setUp() throws Exception
   {
      grantedAuthorities = new ArrayList<>();
      contextUT = new Context();
      contextUT.setApp(new Portal("Test-Portal", 10, 11));
      contextUT.setSpringAuthentication(new Authentication()
      {

         @Override
         public String getName()
         {
            return null;
         }

         @Override
         public void setAuthenticated(boolean isAuthenticated)
               throws IllegalArgumentException
         {
         }

         @Override
         public boolean isAuthenticated()
         {
            return false;
         }

         @Override
         public Object getPrincipal()
         {
            return null;
         }

         @Override
         public Object getDetails()
         {
            return null;
         }

         @Override
         public Object getCredentials()
         {
            return null;
         }

         @Override
         public Collection<GrantedAuthority> getAuthorities()
         {
            return grantedAuthorities;
         }
      });
   }

   @Test
   void test001()
   {
      assertThat(contextUT.isAccepted(), is(false));
   }

   @Test
   void test002()
   {
      assertThat(contextUT.isUserApproved(), is(false));
   }

   @Test
   void test003()
   {
      grantedAuthorities.add(new SimpleGrantedAuthority(
            RoleNameFactory.getKeycloakRoleName(10, Roles.APPROVED)));

      assertThat(contextUT.isUserApproved(), is(false));
   }

   @Test
   void test004()
   {
      grantedAuthorities.add(new SimpleGrantedAuthority(
            RoleNameFactory.getKeycloakRoleName(11, Roles.ACCEPTED)));

      assertThat(contextUT.isAccepted(), is(true));

   }

   @Test
   void test005()
   {
      grantedAuthorities.add(new SimpleGrantedAuthority(
            RoleNameFactory.getKeycloakRoleName(11, Roles.ACCEPTED)));

      assertThat(contextUT.isUserApproved(), is(false));
   }
}
