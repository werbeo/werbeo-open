//package org.infinitenature.werbeo.cms.geometry;
//
//import static org.hamcrest.Matchers.is;
//import static org.junit.Assert.assertThat;
//
//import java.util.Locale;
//
//import org.infinitenature.werbe.test.support.vaadin.SessionUtil;
//import org.infinitenature.werbeo.cms.vaadin.Context;
//import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
//import org.infinitenature.werbeo.cms.vaadin.Portal;
//import org.infinitenature.werbeo.cms.vaadin.components.MapPointField;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//
//import com.vaadin.server.Sizeable.Unit;
//
//class TestMapField
//{
//   private MapPointField mapFieldUT;
//
//   @BeforeEach
//   void setUp() throws Exception
//   {
//      Locale locale = Locale.GERMANY;
//      Context context = new Context();
//      context.setApp(new Portal("test", 2, 2));
//      SessionUtil.mockVaadin(locale, context);
//      mapFieldUT = new MapPointField(new MessagesResourceMock(),
//            MapPointField.MapType.POINT,
//            "https://wms.test.infinitenature.org/geoserver/werbeo/wms");
//   }
//
//
//
//   @Test
//   @DisplayName("Test readonly")
//   void test_001()
//   {
//      mapFieldUT.setReadOnly(true);
//
//      assertThat(mapFieldUT.isReadOnly(), is(true));
//   }
//
//   @Test
//   @DisplayName("Test set witdh")
//   void test_002()
//   {
//      mapFieldUT.setWidth("3000px");
//
//      assertThat(mapFieldUT.getWidth(), is(3000f));
//      assertThat(mapFieldUT.getWidthUnits(), is(Unit.PIXELS));
//   }
//}
