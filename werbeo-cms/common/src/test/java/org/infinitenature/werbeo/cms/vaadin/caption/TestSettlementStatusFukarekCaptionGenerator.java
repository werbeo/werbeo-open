package org.infinitenature.werbeo.cms.vaadin.caption;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import java.util.Locale;

import org.infinitenature.service.v1.types.Occurrence.SettlementStatusFukarek;
import org.infinitenature.werbe.test.support.vaadin.SessionUtil;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestSettlementStatusFukarekCaptionGenerator
{
   private SettlementStatusFukarekCaptionGenerator generatorUT;

   @BeforeEach
   void setUp() throws Exception
   {
      generatorUT = new SettlementStatusFukarekCaptionGenerator(
            new MessagesResourceMock());
      Context context = new Context();
      context.setApp(new Portal("test", 2, 2));
      SessionUtil.mockVaadin(Locale.GERMANY, context);
   }

   @Test
   void test()
   {
      assertThat(generatorUT.apply(SettlementStatusFukarek.ANGESALBT_KULTIVIERT),
            containsString("angesalbt"));
   }

   @Test
   void testNull()
   {
      assertThat(generatorUT.apply(null), containsString("keine Angabe"));
   }
}

