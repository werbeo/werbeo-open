package org.infinitenature.werbeo.cms.vaadin.components.demo;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.herbariorum.client.HerbariorumGermanClient;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.indexherbariorum.HerbariorumField;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.addonhelpers.AbstractTest;

// in the Browser (for IntelliJ):
// localhost:9998/org.infinitenature.werbeo.cms.vaadin.components.demo.HerbariorumDemo

public class HerbariorumDemo extends AbstractTest
{
   @Autowired
   HerbariorumClient client;

   @Override
   public Component getTestComponent()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      VerticalLayout verticalLayout = new VerticalLayout();

      HerbariorumField herbariorumField = new HerbariorumField(new MessagesResourceMock());
      herbariorumField.init(new HerbariorumGermanClient());
      verticalLayout.addComponent(herbariorumField);

      return verticalLayout;
   }
}
