package org.infinitenature.werbeo.cms.vaadin.components.demo;

import org.infinitenature.werbeo.cms.vaadin.components.PositiveIntegerField;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.ui.Component;

public class PositiveIntegerDemo extends AbstractTest
{

   @Override
   public Component getTestComponent()
   {
      return new PositiveIntegerField();
   }

}
