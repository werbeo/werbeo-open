package org.infinitenature.werbeo.cms.vaadin.components.demo;

import java.util.*;

import org.infinitenature.service.v1.types.*;
import org.infinitenature.service.v1.types.enums.LayerName;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.map.Values;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.Layers;
import org.infinitenature.werbeo.cms.vaadin.components.maps.*;
import org.infinitenature.werbeo.cms.vaadin.notification.NotificationUtils;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;

public class MapViewComponentDemo extends AbstractTest
{

   @Override
   public Component getTestComponent()
   {
      VerticalLayout demoLayout = new VerticalLayout();
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      PortalConfiguration configuration = new PortalConfiguration();
      configuration.getMapOverlayLayers().add(LayerName.MTB);
      configuration.getMapOverlayLayers().add(LayerName.ORTHOFOTO_MV);
      configuration.getMapOverlayLayers().add(LayerName.TOPO_MV);
      configuration.setMapInitialLatitude(53d);
      configuration.setMapInitialLongitude(12d);
      configuration.setMapInitialZoom(6d);
      

      MapViewComponent component = new MapViewComponent("https://wms.test.infinitenature.org/geoserver/werbeo/wms", configuration);


      demoLayout.addComponent(new Button("Klick", click ->
      {
         component.setValues(createLocalities(), true);
      }));
      demoLayout.addComponent(component);
      demoLayout.setSizeFull();
      demoLayout.setExpandRatio(component, 1F);
      return demoLayout;
   }

   private List<ComponentLocality> createLocalities()
   {
      List<ComponentLocality> list = new ArrayList<ComponentLocality>();
      
      list.add(new ComponentLocality(point(), new Button("Klick", click ->
      {
         NotificationUtils.showSaveSuccess("Klick auf Punkt", "Auf Punkt geklickt");
      })));
      list.add(new ComponentLocality(mtb(),  new Button("Klick", click ->
      {
         NotificationUtils.showSaveSuccess("Klick auf MTB", "Auf MTB geklickt");
      })));
      list.add(new ComponentLocality(polygon(),  new Button("Klick", click ->
      {
         NotificationUtils.showSaveSuccess("Klick auf Polygon", "Auf Polygon geklickt");
      })));
      
      return list;
   }
   
   private Locality point()
   {
      Locality locality = new Locality();
      
      Position position = new Position();
      position.setWkt("POINT(13.060821531107 54.308532719734)");
      position.setWktEpsg(4326);
      position.setEpsg(4326);
      position.setType(Position.PositionType.POINT);
      
      locality.setBlur(216);
      locality.setPosition(position);
      return locality;
   }
   
   private Locality mtb()
   {
      Locality locality = new Locality();
      MTB mtb = MTBHelper.toMTB("1946");
      Position position = new Position();
      org.infinitenature.service.v1.types.MTB serviceMTB = new org.infinitenature.service.v1.types.MTB();
      serviceMTB.setMtb(mtb.getMtb());
      position.setMtb(serviceMTB);
      position.setWkt(mtb.toWkt());
      position.setWktEpsg(Values.MTB_EPSG);
      position.setEpsg(Values.MTB_EPSG);
      position.setType(Position.PositionType.MTB);
      locality.setPosition(position);
      return locality;
   }
   
   private Locality polygon()
   {
      Locality locality = new Locality();
      Position position = new Position();
      position.setWkt(
            "POLYGON((13.1903076171875 54.18725496239297,13.6737060546875 54.1164807621956,13.5198974609375 53.92284467253592,13.2177734375 53.95194756311168,13.1903076171875 54.18725496239297))");
      position.setWktEpsg(4326);
      position.setEpsg(4326);
      position.setType(Position.PositionType.SHAPE);
      locality.setPosition(position);
      return locality;
   }
}
