package org.infinitenature.werbeo.cms.vaadin.components.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;

public class GridDemo extends AbstractTest
{
   public class Bean
   {
      private final int id;

      public Bean(int id)
      {
         super();
         this.id = id;
      }

      public int getId()
      {
         return id;
      }

   }

   @Override
   public Component getTestComponent()
   {

      Grid<Bean> grid = new Grid<>(Bean.class);

      grid.setDataProvider(new AbstractBackEndDataProvider<Bean, String>()
      {

         @Override
         protected Stream<Bean> fetchFromBackEnd(Query<Bean, String> query)
         {
            List<Bean> beans = new ArrayList<>();
            for (int i = query.getOffset(); i < query.getOffset()
                  + query.getLimit(); i++)
            {
               beans.add(new Bean(i));
            }
            return beans.stream();
         }

         @Override
         protected int sizeInBackEnd(Query<Bean, String> query)
         {
            return 520000;
         }
      });

      return grid;
   }
}
