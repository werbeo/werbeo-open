package org.infinitenature.werbeo.cms.vaadin.components.demo;

import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.enums.LayerName;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.IAMapFilter;
import org.infinitenature.werbeo.cms.vaadin.components.maps.interactive.InteractiveMap;
import org.vaadin.addonhelpers.AbstractTest;
import org.vaadin.viritin.label.MLabel;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class InteractiveMapComponentDemo extends AbstractTest
{

   @Override
   public Component getTestComponent()
   {
      VerticalLayout demoLayout = new VerticalLayout();
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      PortalConfiguration configuration = new PortalConfiguration();
      configuration.getMapOverlayLayers().add(LayerName.MTB);
      configuration.getMapOverlayLayers().add(LayerName.ORTHOFOTO_MV);
      configuration.getMapOverlayLayers().add(LayerName.TOPO_MV);
      configuration.setMapInitialLatitude(53d);
      configuration.setMapInitialLongitude(12d);
      configuration.setMapInitialZoom(10d);

      InteractiveMap component = new InteractiveMap(new MessagesResourceMock(),
            configuration, new InstanceConfig());
      TaxonBase abiesAlba = new TaxonBase();
      abiesAlba.setEntityId(54870);
      IAMapFilter iaMapFilter = new IAMapFilter();
      iaMapFilter.setTaxon(abiesAlba);
      component.setFilter(iaMapFilter);
      demoLayout.addComponent(new MLabel("TEST"));
      demoLayout.addComponent(component);
      demoLayout.setSizeFull();
      demoLayout.setExpandRatio(component, 1F);
      return demoLayout;
   }
}
