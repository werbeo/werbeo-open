package org.infinitenature.werbeo.cms.vaadin.components;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.stream.Stream;

import javax.validation.constraints.Min;

import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.MTBResponse;
import org.infinitenature.service.v1.types.response.TaxonBaseSliceResponse;
import org.infinitenature.service.v1.types.response.TaxonFieldConfigResponse;
import org.infinitenature.service.v1.types.response.TaxonResponse;
import org.infinitenature.service.v1.types.response.TaxonSliceResponse;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.vaadin.data.provider.Query;
import com.vaadin.server.VaadinSession;

class TestTaxonComboBoxDataProvider
{
   private TaxonComboBoxDataProvider taxonComboBoxDataProviderUT;

   @BeforeEach
   void setUp() throws Exception
   {
      Context context = new Context();
      context.setApp(new Portal("test", 2, 2));
      VaadinSession session = mock(VaadinSession.class);
      when(session.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(context);
      VaadinSession.setCurrent(session);

      Werbeo werbeo = mock(Werbeo.class);
      when(werbeo.taxa()).thenReturn(new TaxaResource()
      {

         @Override
         public TaxonResponse getTaxon(int portalId, int id)
         {
            return null;
         }

         @Override
         public TaxonBaseSliceResponse find(int offset, int limit,
               TaxaFilter parameterObject)
         {
            return new TaxonBaseSliceResponse(new ArrayList<>(), 0);
         }

         @Override
         public CountResponse count(TaxaFilter parameterObject)
         {
            return new CountResponse(0, "mock-data");
         }

         @Override
         public TaxonSliceResponse findAllSynonyms(@Min(1) int portalId, int id)
         {
            return new TaxonSliceResponse();
         }

         @Override
         public MTBResponse getCoveredMTB(@Min(1) int portalId, int id,
               boolean inclusiveChildTaxa)
         {
            return null;
         }

         @Override
         public TaxonFieldConfigResponse getFieldConfig(@Min(1) int portalId)
         {
            return null;
         }

         @Override
         public TaxonFieldConfigResponse getFieldConfig(@Min(1) int portalId,
               @Min(1) int surveyId)
         {
            return null;
         }

      });
      taxonComboBoxDataProviderUT = new TaxonComboBoxDataProvider(werbeo, new InstanceConfig(), false, false, false);
   }

   @Test
   void testFetchFromBackEndQueryOfTaxonBaseString()
   {
      Query<TaxonBase, String> query = new Query<>("abies");
      Stream<TaxonBase> data = taxonComboBoxDataProviderUT
            .fetchFromBackEnd(query);

      assertThat(data.count(), is(0l));
   }

   @Test
   void testSizeInBackEndQueryOfTaxonBaseString()
   {
      Query<TaxonBase, String> query = new Query<>("abies");

      assertThat(taxonComboBoxDataProviderUT.sizeInBackEnd(query), is(0));
   }

}
