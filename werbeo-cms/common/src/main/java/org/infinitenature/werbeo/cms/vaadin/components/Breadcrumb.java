package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;
import org.infinitenature.werbeo.cms.NavigationConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.vaadin.viritin.button.MButton;

public class Breadcrumb extends CustomComponent
{
   private HorizontalLayout layout;
   private String externalCmsUrl;

   public Breadcrumb(String externalCmsUrl)
   {
      this.externalCmsUrl = externalCmsUrl;
      layout = new HorizontalLayout();
      layout.setSpacing(true);
      MButton homeLabel = new MButton(VaadinIcons.HOME_O);
      homeLabel.addStyleName(ValoTheme.BUTTON_LINK);
      homeLabel.addStyleName("breadcrumb");
      homeLabel.addClickListener(clickEvent -> UI.getCurrent().getPage()
            .setLocation(externalCmsUrl));
      layout.addComponent(homeLabel);
      setStyleName("breadcrumb");

      setCompositionRoot(layout);
   }

   public void setHierarchy(String... content)
   {
      for (int i = 0; i < content.length; i++)
      {
         layout.addComponent(new Label(">"));

         if (i == content.length - 1)
         {
            layout.addComponent(new Label(content[i]));
         }
         else
         {
            NavigationConfig navigationConfig = NavigationConfig
                  .getEnum(content[i],
                        Context.getCurrent().getPortal().getPortalId());
            if (navigationConfig == null)
            {
               // missing breadcrumb values, should not prevent the rest of the breadcrumb to build
               // this makes is easier to find and add missing values
               continue;
            }
            addBreadcrumbEntry(navigationConfig, content[i]);
         }
      }
   }

   private void addBreadcrumbEntry(NavigationConfig navigationConfig,
         String caption)
   {
      if (navigationConfig.getNavigationTarget().isEmpty())
      {
         layout.addComponent(new Label(caption));
      }
      else
      {
         addBreadcrumbButton(caption, navigationConfig);
      }
   }

   private void addBreadcrumbButton(String caption,
         NavigationConfig navigationConfig)
   {
      MButton link = new MButton(caption);
      link.addStyleName(ValoTheme.BUTTON_LINK);
      link.addStyleName("breadcrumb");
      layout.addComponent(link);

      if (navigationConfig.isExternalCmsLink())
      {
         link.addClickListener(clickEvent -> UI.getCurrent().getPage()
               .setLocation(
                     externalCmsUrl + navigationConfig.getNavigationTarget()));
      }
      else
      {
         link.addClickListener(clickEvent -> UI.getCurrent().getNavigator()
               .navigateTo(navigationConfig.getNavigationTarget()));
      }
   }
}
