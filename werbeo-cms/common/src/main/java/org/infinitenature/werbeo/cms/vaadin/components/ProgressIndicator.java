package org.infinitenature.werbeo.cms.vaadin.components;

import org.apache.commons.lang3.Validate;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.vaadin.viritin.components.DisclosurePanel;
import org.vaadin.viritin.label.RichText;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.ui.ProgressBar;

public class ProgressIndicator extends I18NComposite
{
   private int steps = 0;
   private int successfullSteps = 0;
   private int failureSteps = 0;
   private final ProgressBar progressBar = new ProgressBar();
   private final RichText logViewer = new RichText();
   private final RichText lastMessage = new RichText();
   private final DisclosurePanel disclosurePanel = new DisclosurePanel();
   private StringBuilder log = new StringBuilder();

   public ProgressIndicator(MessagesResource messages)
   {
      super(messages);
      progressBar.setWidth("100%");
      disclosurePanel.setContent(logViewer);
      setCompositionRoot(
            new MVerticalLayout(progressBar, lastMessage, disclosurePanel));
   }

   @Override
   public void applyLocalizations()
   {
      disclosurePanel.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "general.details", getLocale()));
   }

   public void success()
   {
      successfullSteps++;
      updateBar();
   }

   public void success(String message)
   {
      addSuccess(message);
      success();

   }

   private void addSuccess(String message)
   {
      lastMessage.withMarkDown(message);
      log.append("  * ");
      log.append(message);
      log.append("\n");
   }

   public void failure()
   {
      failureSteps++;
      updateBar();
   }

   public void failure(String message)
   {
      addFailure(message);
      failure();
   }

   private void addFailure(String message)
   {
      lastMessage.withMarkDown(message);
      log.append("  * ");
      log.append(message);
      log.append("\n");
   }

   private void updateBar()
   {
      progressBar.setValue(steps / (failureSteps + successfullSteps));
      lastMessage.setValue(getMessages().getMessage(
            Context.getCurrent().getPortal(), "progressIndiciator.summary",
            getLocale(), successfullSteps, failureSteps));
      logViewer.withMarkDown(log.toString());
   }

   public int getSteps()
   {
      return steps;
   }

   /**
    * Sets the amount of steps and resets the bar.
    *
    * @param steps
    */
   public void setSteps(int steps)
   {
      Validate.isTrue(steps > 0, "There must be a least 1 step.");
      this.steps = steps;
      this.failureSteps = 0;
      this.successfullSteps = 0;
   }

   public boolean hasFailure()
   {
      return failureSteps != 0;
   }

}
