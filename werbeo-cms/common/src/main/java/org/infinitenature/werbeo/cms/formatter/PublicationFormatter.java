package org.infinitenature.werbeo.cms.formatter;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.Publication;

public class PublicationFormatter
{
   private PublicationFormatter()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static String formatShort(Publication publication)
   {

      StringBuilder sb = new StringBuilder();

      if (publication != null)
      {
         sb.append(publication.getPrimAuthorLastName());
         if (StringUtils.isNotBlank(publication.getPrimAuthorFirstName()))
         {
            sb.append(" ");
            sb.append(publication.getPrimAuthorFirstName().substring(0, 1));
            sb.append(".");
         }
         sb.append(" (");
         sb.append(publication.getYear());
         sb.append(") ");
         if (StringUtils.isNotBlank(publication.getTitle())
               && publication.getTitle().length() > 30)
         {
            sb.append(publication.getTitle().substring(0, 30));
         } else if (StringUtils.isNotBlank(publication.getTitle()))
         {
            sb.append(publication.getTitle());
         }
      }
      return sb.toString();
   }
}
