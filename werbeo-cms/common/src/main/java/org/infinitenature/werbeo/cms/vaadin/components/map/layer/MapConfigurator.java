package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LTileLayer;
import org.vaadin.addon.leaflet.LWmsLayer;
import org.vaadin.addon.leaflet.control.LScale;
import org.vaadin.addon.leaflet.util.AbstractJTSField;
import org.vaadin.addon.leaflet.util.AbstractJTSField.Configurator;

public class MapConfigurator implements Configurator
{
   private Layers layers;
   private boolean collapseLayerControl = false;

   public MapConfigurator(Layers layers)
   {
      super();
      this.layers = layers;
   }
   
   public MapConfigurator(Layers layers, boolean collapseLayerControl)
   {
      super();
      this.collapseLayerControl = collapseLayerControl;
      this.layers = layers;
   }

   @Override
   public void configure(AbstractJTSField<?> field)
   {
      field.getMap().setScrollWheelZoomEnabled(true);
      LScale scaleControl = new LScale();
      scaleControl.setImperial(false);
      scaleControl.setMetric(true);
      field.getMap().addControl(scaleControl);
      // hardcoded support for Leaflet.Editable
      field.getMap().setCustomInitOption("editable", true);
      field.getMap().getLayersControl().setCollapsed(collapseLayerControl);

      for(Layer layer : layers.getBaselayers())
      {
         field.getMap().addBaseLayer((LTileLayer)layer, layer.getCaption());
      }

      for(Layer layer : layers.getOverlays())
      {
         field.getMap().addOverlay((LWmsLayer)layer, layer.getCaption());
      }
   }

   public Layers getLayers()
   {
      return layers;
   }
}