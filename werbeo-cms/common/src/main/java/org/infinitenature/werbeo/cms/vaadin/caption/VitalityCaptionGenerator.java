package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.Vitality;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class VitalityCaptionGenerator extends I18NCaptionGenerator<Vitality>
{

   public VitalityCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
