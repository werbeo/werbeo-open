package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.werbeo.cms.formatter.VagueDateFormater;

public class VagueDateCaptionGenerator
      implements WerbeoCaptionGenerator<VagueDate>
{

   @Override
   public String apply(VagueDate option)
   {
      if (option == null)
      {
         return "";
      } else
      {
         return VagueDateFormater.format(option);
      }
   }

}
