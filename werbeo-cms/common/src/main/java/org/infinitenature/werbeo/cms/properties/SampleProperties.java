package org.infinitenature.werbeo.cms.properties;

public enum SampleProperties implements EntityEnum
{
   RECORDER,
   ENTITY_ID,
   DATE,
   POSITION,
   LOCATION_COMMENT,
   TIME,
   LOCATION,
   BLUR,
   SURVEY,
   LAST_CHANGE,
   LATITUDE,
   LONGITUDE,
   MTB, SAMPLE_METHOD;

   @Override
   public String getEntityName()
   {
      return "Sample";
   }

}
