package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class AmountItemCaptionGenerator
      extends I18NCaptionGenerator<Occurrence.Amount>
{
   public AmountItemCaptionGenerator(MessagesResource messages)
   {
      super(messages);

   }
}