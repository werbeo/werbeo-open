package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Person;
import org.infinitenature.werbeo.cms.formatter.PersonFormatter;

public class PersonCaptionGenerator implements WerbeoCaptionGenerator<Person>
{

   @Override
   public String apply(Person option)
   {
      if (option == null)
      {
         return "";
      } else
      {
         return PersonFormatter.format(option);
      }
   }

}
