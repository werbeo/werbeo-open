package org.infinitenature.werbeo.cms.vaadin.notification;

import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class NotificationUtils
{
   public static void showSaveSuccess(String header, String message)
   {
      Notification notif = new Notification(header,
            message,
            Type.WARNING_MESSAGE);

      // Customize it
      notif.setDelayMsec(5000);
      notif.setStyleName("save-notification");
      notif.show(Page.getCurrent());
   }

   public static void showError(String header, String message)
   {
      Notification notif = new Notification(header,
            message,
            Type.ERROR_MESSAGE);

      // Customize it
      notif.setDelayMsec(5000);
      notif.setStyleName("error-notification");
      notif.show(Page.getCurrent());
   }
}
