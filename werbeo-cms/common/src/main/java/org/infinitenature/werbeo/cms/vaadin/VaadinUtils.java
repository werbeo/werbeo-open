package org.infinitenature.werbeo.cms.vaadin;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;

public class VaadinUtils
{
   private VaadinUtils()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static Stream<Component> componentStream(HasComponents parent)
   {
      Iterable<Component> iterable = () -> parent.iterator();
      return StreamSupport.stream(iterable.spliterator(), false);
   }

   public static <T> Stream<T> componentStream(HasComponents parent,
         Class<T> ofType)
   {
      return componentStream(parent).filter(c -> ofType.isInstance(c))
            .map(c -> (T) c);
   }
}
