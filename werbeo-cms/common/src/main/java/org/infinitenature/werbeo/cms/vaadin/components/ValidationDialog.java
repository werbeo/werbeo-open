package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.button.DismissButton;
import org.infinitenature.werbeo.cms.vaadin.components.button.SaveButton;
import org.infinitenature.werbeo.cms.vaadin.components.comment.ListenerSupport;
import org.vaadin.viritin.layouts.MFormLayout;

import com.vaadin.data.Binder;
import com.vaadin.shared.Registration;

public class ValidationDialog extends PopUpDialog<ValidationDialog>
{
   public class ValidationEvent
   {
      public final Validation validation;

      public ValidationEvent(Validation validation)
      {
         this.validation = validation;
      }

   }

   @FunctionalInterface
   public interface ValidationListener
   {
      public void valdationDone(ValidationEvent event);
   }

   private final ValidationStatusComboBox validationStatusComboBox;
   private final MarkDownField validationComment;
   private final ListenerSupport<ValidationListener> validationListers = new ListenerSupport<>();
   private final Binder<Validation> binder = new Binder<>(Validation.class);

   public ValidationDialog(MessagesResource messages)
   {
      super(messages);
      this.validationStatusComboBox = new ValidationStatusComboBox(messages);
      this.validationComment = new MarkDownField(messages);
      this.validationComment.setHeight("10em");
      this.setSizeUndefined();
      MFormLayout formLayout = new MFormLayout(validationStatusComboBox,
            validationComment);
      withContent(formLayout);
      setModal(true);
      withCloseButton(new DismissButton(messages));
      withButton(new SaveButton(messages, event -> onSaveClick()));
      binder.bind(validationStatusComboBox, "status");
      binder.bind(validationComment, "comment");
   }

   public void setBean(Validation validation)
   {
      binder.readBean(validation);
   }

   private void onSaveClick()
   {
      validationListers.fire(listener ->
      {
         Validation newValue = new Validation();
         binder.writeBeanIfValid(newValue);
         listener.valdationDone(new ValidationEvent(newValue));
      });
      this.close();
   }

   public Registration addValidationListener(ValidationListener listener)
   {
      return validationListers.addListener(listener);
   }

   @Override
   public void applyLocalizations()
   {
      Portal portal = Context.getCurrent().getPortal();
      setCaption(getMessages().getMessage(portal,
            "occurrence.validation.header", getLocale()));
      validationComment.setCaption(
            getMessages().getMessage(portal,
                  "occurrence.validation.comment", getLocale()));
      validationStatusComboBox.setCaption(getMessages().getMessage(portal,
            "occurrence.validation.status", getLocale()));
   }

}
