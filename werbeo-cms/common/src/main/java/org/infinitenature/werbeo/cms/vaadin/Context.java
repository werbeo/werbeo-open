package org.infinitenature.werbeo.cms.vaadin;

import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.LoginChangeListener;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.common.commons.roles.Roles;

import com.vaadin.server.VaadinSession;

public class Context extends VCMSContext
{

   private org.springframework.security.core.Authentication springAuthentication;

   private Filters filters = new Filters();
   private Filters myFilters = new Filters();

   private final TablesConfig tablesConfig = new TablesConfig();

   private final MapPositionConfig mapPositionConfig = new MapPositionConfig();

   private Preselection preselection;

   public static Context getCurrent()
   {
      return (Context) VaadinSession.getCurrent().getAttribute(SESSION_KEY);
   }

   @Override
   public void setAuthentication(Authentication authentication)
   {
      super.setAuthentication(authentication);
      notifyLoginChanged();
   }

   public synchronized void notifyLoginChanged()
   {
      loginChangeListeners.forEach(LoginChangeListener::loginChanged);
   }

   @Override
   public Portal getApp()
   {
      return (Portal) super.getApp();
   }

   public Portal getPortal()
   {
      return getApp();
   }

   public void setSpringAuthentication(
         org.springframework.security.core.Authentication authentication)
   {
      this.springAuthentication = authentication;
   }

   public org.springframework.security.core.Authentication getSpringAuthentication()
   {
      return springAuthentication;
   }

   public boolean isAccepted()
   {
      return hasRole(Roles.ACCEPTED);
   }

   public boolean isValdiator()
   {
      return hasRole(Roles.VALIDATOR);
   }

   /**
    * Checks if terms and conditions need to be accepted.
    *
    * @return true if a user is logged in and terms and conditions are not
    *         accepted yet
    */
   public boolean isAcceptanceNecessary()
   {
      return !isAccepted() && !getAuthentication().isAnonymous();
   }

   private boolean hasRole(Roles role)
   {
      return springAuthentication != null
            && springAuthentication.getAuthorities().stream()
                  .map(auth -> auth.getAuthority()).filter(auth ->
                  {

                     return auth.equals(RoleNameFactory.getKeycloakRoleName(
                           getPortal().getPortalId(), role));
                  }).count() > 0;
   }

   public boolean isUserApproved()
   {
      return hasRole(Roles.APPROVED);
   }

   @Override
   public org.infinitenature.werbeo.cms.vaadin.ViewMode getViewMode()
   {
      return (org.infinitenature.werbeo.cms.vaadin.ViewMode) super.getViewMode();
   }

   public Filters getMyFilters()
   {
      return myFilters;
   }

   public void setMyFilters(Filters myFilters)
   {
      this.myFilters = myFilters;
   }

   public Filters getFilters()
   {
      return filters;
   }

   public void setFilters(Filters filters)
   {
      this.filters = filters;
   }

   public TablesConfig getTablesConfig()
   {
      return tablesConfig;
   }

   public boolean isAdmin()
   {
      return hasRole(Roles.ADMIN);
   }

   public MapPositionConfig getMapPositionConfig()
   {
      return mapPositionConfig;
   }

   public Preselection getPreselection()
   {
      return preselection;
   }

   public void setPreselection(Preselection preselection)
   {
      this.preselection = preselection;
   }

}
