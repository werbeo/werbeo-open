package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.werbeo.cms.vaadin.components.map.*;

import org.locationtech.jts.geom.*;
import org.locationtech.jts.util.GeometricShapeFactory;

public class CircleCreator
{
   public static Polygon createCircle(Point center, int radiusInMeters)
   {
      MapCRSTranslator centerTranslator = new MapCRSTranslator(CoordinateSystem.EPSG4326.getEpsg(), center.getSRID());
      center = (Point)centerTranslator.toPresentation(center);
      double diameter = radiusInMeters * 2;
      GeometricShapeFactory factory =  new GeometricShapeFactory();
      factory.setNumPoints(64);
      factory.setCentre(center.getCoordinate());
      factory.setWidth(diameter/111320d * 1.8);
      factory.setHeight(diameter/111320d);
      Polygon circle = factory.createEllipse();
      circle.setSRID(CoordinateSystem.EPSG4326.getEpsg());
      return circle;
   }
}
