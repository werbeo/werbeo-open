package org.infinitenature.werbeo.cms.vaadin.components;

import java.io.*;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.infinitenature.vct.Authentication;
import org.infinitenature.werbeo.cms.vaadin.Context;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.*;
import com.vaadin.ui.*;
import com.vaadin.ui.Upload.Receiver;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;

public class PictureBox extends I18NComposite implements Receiver
{
   private Upload uploadTaxon;

   private String currentFileName;
   private HorizontalLayout pictureLayout = new HorizontalLayout();
   private String caption;
   private String buttonCaption;

   public PictureBox(MessagesResource messages, String caption,
         String buttonCaption)
   {
      super(messages);
      this.caption = caption;
      this.buttonCaption = buttonCaption;
      VerticalLayout mainLayout = new VerticalLayout();

      uploadTaxon = new Upload("", this);

      uploadTaxon.addFinishedListener(event -> {
         File file = new File(getPath() + currentFileName);
         pictureLayout.addComponent(
               new Picture(new FileResource(file), pictureLayout));
      });
      mainLayout.addComponent(uploadTaxon);
      mainLayout.addComponent(pictureLayout);
      setCompositionRoot(mainLayout);
   }

   private String getPath()
   {
      return FileUtils.getTempDirectory() + "/" + Context.getCurrent()
            .getPortal().getPortalId() + "/" + createUserDir(
            Context.getCurrent().getAuthentication()) + "/";
   }

   @Override
   public OutputStream receiveUpload(String filename, String mimeType)
   {
      currentFileName = UUID.randomUUID().toString();
      File dir = new File(getPath());
      dir.mkdirs();
      File file = new File(dir, currentFileName);

      try
      {
         return new FileOutputStream(file);
      } catch (FileNotFoundException e)
      {
         e.printStackTrace();
         return null;
      }
   }

   private String createUserDir(Authentication authentication)
   {
      return authentication.getEmail().get().replace("@", "").replace(".", "");
   }

   @Override
   public void applyLocalizations()
   {
      uploadTaxon.setCaption(caption);
      uploadTaxon.setButtonCaption(buttonCaption);
   }
}

class Picture extends CustomComponent
{

   public Picture(FileResource fileResource, HorizontalLayout pictureLayout)
   {
      Panel panel = new Panel();
      HorizontalLayout imageLayout = new HorizontalLayout();
      imageLayout.setMargin(true);
      imageLayout.setSpacing(true);
      Image image = new Image("", fileResource);
      image.setWidth(200, Unit.PIXELS);
      image.setHeight(200, Unit.PIXELS);
      imageLayout.addComponent(image);
      Button closeButton = new Button();
      closeButton.setCaption("");
      closeButton.setIcon(VaadinIcons.CLOSE);
      closeButton
            .addClickListener(cevent -> pictureLayout.removeComponent(this));
      imageLayout.addComponent(closeButton);
      panel.setContent(imageLayout);
      setCompositionRoot(panel);
   }

}
