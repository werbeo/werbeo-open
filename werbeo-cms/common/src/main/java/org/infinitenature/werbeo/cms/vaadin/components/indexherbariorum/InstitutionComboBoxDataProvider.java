package org.infinitenature.werbeo.cms.vaadin.components.indexherbariorum;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.ui.ComboBox;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.herbariorum.entities.Institution;

import java.util.stream.Stream;

public class InstitutionComboBoxDataProvider
      extends AbstractBackEndDataProvider<Institution, String>
{

   private HerbariorumClient client;
   private ComboBox<String> countryComboBox;
   private String lastSelection;

   InstitutionComboBoxDataProvider (HerbariorumClient client, String presetCountry, ComboBox<String> countryComboBox)
   {
         this.client = client;
         lastSelection = presetCountry;
         this.countryComboBox = countryComboBox;
   }

   @Override
   protected Stream<Institution> fetchFromBackEnd(
         Query<Institution, String> query)
   {
      String country = countryComboBox.getSelectedItem().get();
      if(client.isValidCountry(country))
      {
         lastSelection = country;
      }
      return client.findInstitutionsByCountryAndName(
            lastSelection, query.getFilter().orElse(""))
            .stream().skip(query.getOffset()).limit(query.getLimit());
   }

   @Override
   protected int sizeInBackEnd(Query<Institution, String> query)
   {
      String country = countryComboBox.getSelectedItem().get();
      if(client.isValidCountry(country))
      {
         lastSelection = country;
      }
      return client.countInstitutionsByCountryAndName(
            lastSelection,
            query.getFilter().orElse(""));
   }


}
