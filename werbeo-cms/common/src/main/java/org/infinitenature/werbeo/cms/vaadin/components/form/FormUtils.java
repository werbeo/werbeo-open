package org.infinitenature.werbeo.cms.vaadin.components.form;

import org.vaadin.viritin.layouts.MVerticalLayout;

import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.jarektoro.responsivelayout.ResponsiveRow.SpacingSize;
import com.vaadin.shared.ui.MarginInfo;

public class FormUtils
{
   private FormUtils()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static MVerticalLayout createMainLayout()
   {
      return new MVerticalLayout().withMargin(new MarginInfo(false, true))
            .withFullSize();
   }

   public static ResponsiveLayout createFromLayout()
   {
      return new ResponsiveLayout().withDefaultRules(12, 12, 12, 6)
            .withSpacing();
   }

   public static ResponsiveRow createRow(ResponsiveLayout layout)
   {
      return layout.addRow().withSpacing(SpacingSize.SMALL, true);
   }

   public static ResponsiveRow createWideRow(ResponsiveLayout layout)
   {
      return layout.addRow(new ResponsiveRow().withDefaultRules(12, 12, 12, 12)
            .withSpacing(SpacingSize.SMALL, true));
   }
}
