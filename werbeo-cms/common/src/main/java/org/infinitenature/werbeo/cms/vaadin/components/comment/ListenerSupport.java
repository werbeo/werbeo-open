package org.infinitenature.werbeo.cms.vaadin.components.comment;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Consumer;

import com.vaadin.shared.Registration;

public class ListenerSupport<T>
{
   public interface HasListener<T>
   {
      public Registration addListner(T listener);

      public boolean removeListener(T listener);
   }

   private final Set<T> listeners = new LinkedHashSet<>();

   public Registration addListener(T listener)
   {
      listeners.add(listener);
      return () -> listeners.remove(listener);
   }

   public boolean removeListener(T listener)
   {
      return listeners.remove(listener);
   }

   public void fire(Consumer<T> func)
   {
      for (T listner : listeners)
      {
         func.accept(listner);
      }
   }
}
