package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.UI;

public class TimeComboBox extends ComboBox<String>
{
   private static final long serialVersionUID = -1613534535679692804L;
   private MessagesResource messages;

   public TimeComboBox(MessagesResource messages)
   {
      this(messages, null);
   }
   
   public TimeComboBox(MessagesResource messages, String caption)
   {
      super(caption);
      this.messages = messages;
      this.setPlaceholder(translate("HOUR_MINUTES"));
      
      List<String> timedata = new ArrayList<>();
      for (int h = 0; h < 24; h++)
      {
         for (int m = 0; m < 60; m++)
         {
            timedata.add(String.format("%02d:%02d", h, m));
         }
      }

      ListDataProvider<String> dataProvider = DataProvider
            .ofCollection(timedata);
      this.setDataProvider(dataProvider);
   }
   
   public TimeComboBox withSelected(String time)
   {
      this.setValue(time);
      return this;
   }
   
   public TimeComboBox withOnChange(ValueChangeListener<String> listener)
   {
      this.addValueChangeListener(listener);
      return this;
   }

   public TimeComboBox withSizeFull()
   {
      setSizeFull();
      return this;
   }
   
   private String translate(String field)
   {
      return messages.getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }
}