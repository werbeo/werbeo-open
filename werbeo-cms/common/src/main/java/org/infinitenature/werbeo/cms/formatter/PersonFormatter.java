package org.infinitenature.werbeo.cms.formatter;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.Person;

public class PersonFormatter
{
   private PersonFormatter()
   {
      throw new IllegalArgumentException("Utility class");
   }

   public static String format(Person person)
   {
      if (person == null)
      {
         return "";
      }
      
      String firstName = person.getFirstName() != null ? person.getFirstName() : "";
      String lastName = person.getLastName() != null ? person.getLastName() : "";
      
      return firstName + (StringUtils.isNotBlank(firstName) ? " " : "") + lastName;
   }
}
