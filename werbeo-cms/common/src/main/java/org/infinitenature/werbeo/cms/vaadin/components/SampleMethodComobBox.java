package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.infinitenature.service.v1.types.SampleBase.SampleMethod;
import org.infinitenature.service.v1.types.Value;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.response.SampleFieldConfigResponse;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.SampleMethodCaptionGenrator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class SampleMethodComobBox extends EnumComboBox<SampleMethod>
{

   public SampleMethodComobBox(MessagesResource messages,
         SampleFieldConfigResponse sampleFieldConfig)
   {
      super(messages, SampleMethod.class,
            new SampleMethodCaptionGenrator(messages));
      Optional<SampleFieldConfig> fieldConfig = sampleFieldConfig
            .getFieldConfigs().stream()
            .filter(field -> field.getField().equals(SampleField.SAMPLE_METHOD))
            .findFirst();
      if (fieldConfig.isPresent())
      {
         this.setEmptySelectionAllowed(!fieldConfig.get().isMandantory());
      }
      List<SampleMethod> sampleMethods = new ArrayList<>();
      for(SampleFieldConfig config : sampleFieldConfig.getFieldConfigs())
      {
         if(config.getField().equals(SampleField.SAMPLE_METHOD))
         {
            for(Value value : config.getValues())
            {
               sampleMethods.add(SampleMethod.valueOf(value.getValue()));
            }
         }
      }
      setItems(sampleMethods);
   }
}
