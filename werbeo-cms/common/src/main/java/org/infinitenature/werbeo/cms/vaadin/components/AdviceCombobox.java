package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class AdviceCombobox<T extends Enum<T>> extends I18NField<T>
{

   private EnumComboBox<T> combobox;
   private Label label;
   private Button adviceButton;
   private VerticalLayout adviceLayout;
   private Window adviceWindow;

   public AdviceCombobox(MessagesResource messages, EnumComboBox<T> combobox)
   {
      super(messages);
      this.combobox = combobox;
      label = new Label();
      label.addStyleName("v-caption");
      adviceButton = new Button(VaadinIcons.INFO_CIRCLE);
      adviceButton.addClickListener(click -> showAdviceWindow());
      adviceButton.addStyleName("info-button");
      adviceLayout = new VerticalLayout();
      adviceLayout.setWidth("750px");
      adviceWindow = new Window("Hinweise");
      adviceWindow.center();
      adviceWindow.setHeight("750px");
      adviceWindow.setWidth("800px");
      adviceWindow.setContent(adviceLayout);
   }


   private void showAdviceWindow()
   {
      try{adviceWindow.close();}catch(Exception e){};
      UI.getCurrent().addWindow(adviceWindow);
   }


   @Override
   public void applyLocalizations()
   {
      // TODO Auto-generated method stub

   }


   @Override
   public T getValue()
   {
      return combobox.getValue();
   }

   @Override
   public Registration addValueChangeListener(ValueChangeListener<T> listener)
   {
      return combobox.addValueChangeListener(listener);
   }

   @Override
   public void setCaption(String caption)
   {
      label.setValue(caption);
      combobox.setCaption(null);
   }

   @Override
   protected Component initContent()
   {
      HorizontalLayout captionLayout = new HorizontalLayout(label, adviceButton);
      captionLayout.setMargin(false);
      captionLayout.setSpacing(false);
      captionLayout.setStyleName("padding-bottom-01");
      VerticalLayout mainlayout = new VerticalLayout(captionLayout, combobox);
      mainlayout.setMargin(false);
      mainlayout.setSpacing(false);
      return mainlayout;
   }


   @Override
   protected void doSetValue(T value)
   {
      combobox.setValue(value);
   }

   public void setAdviceHtmlText(String adviceText)
   {
      adviceLayout.removeAllComponents();
      // lines are splitted by double-pipes, first line is header
      String[] lines = adviceText.split("\\|\\|");

      boolean first = true;
      for(String line : lines)
      {
         // skip header line
         if(first)
         {
            addLine(adviceLayout, line.split("\\|"), calculateMaxWidths(lines), true);
            first = false;
            continue;
         }
         else
         {
            addLine(adviceLayout, line.split("\\|"), calculateMaxWidths(lines), false);
         }
      }

      adviceWindow.setContent(adviceLayout);
   }


   private int[] calculateMaxWidths(String[] lines)
   {
      int[] widths = new int[lines[0].split("\\|").length];
      int column = 0;
      for (String columnText : lines[0].split("\\|"))
      {
         int columnLength = columnText.length() * 22;
         widths[column] = columnLength;
         column++;
      }
      return widths;
   }

   private void addLine(VerticalLayout layout, String[] split, int[] widths, boolean header)
   {
      HorizontalLayout row = new HorizontalLayout();
      int column = 0;
      for (String cellValue : split)
      {
         Label cellLabel = new Label(header ? ("<h2>" + cellValue + "</h2>") : cellValue);
         cellLabel.setContentMode(ContentMode.HTML);
         cellLabel.setWidth(widths[column], Unit.PIXELS);
         row.addComponent(cellLabel);
         column++;
      }
      layout.addComponent(row);
   }


   public void setEmptySelectionCaption(String entityField)
   {
      combobox.setEmptySelectionCaption(entityField);
   }


   public void setComboboxStyleName(String styleName)
   {
      combobox.setStyleName(styleName);
   }

}
