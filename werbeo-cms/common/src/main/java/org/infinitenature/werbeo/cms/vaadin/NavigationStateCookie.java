package org.infinitenature.werbeo.cms.vaadin;

import javax.servlet.http.Cookie;

import com.vaadin.server.VaadinService;
import com.vaadin.ui.UI;

public class NavigationStateCookie
{
   private static final String COOKIE_PATH = "/";
   private static final String NAVIGATION_STATE = "navigationState";

   private NavigationStateCookie()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static void deleteNavigationState()
   {
      Cookie cookie = new Cookie(NAVIGATION_STATE, "");
      cookie.setPath(COOKIE_PATH);
      cookie.setMaxAge(0); // Delete
      VaadinService.getCurrentResponse().addCookie(cookie);

   }

   public static String readNavigationState()
   {
      Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
      if(cookies == null)
      {
         return null;
      }
      for (Cookie cookie : cookies)
      {
         if (NAVIGATION_STATE.equals(cookie.getName()))
         {
            return cookie.getValue();
         }
      }
      return null;
   }

   public static void saveNavigationState()
   {
      Cookie cookie = new Cookie(NAVIGATION_STATE,
            UI.getCurrent().getNavigator().getState());
      cookie.setMaxAge(600); // 5 minutes, must be long enough to login
      cookie.setPath(COOKIE_PATH);
      VaadinService.getCurrentResponse().addCookie(cookie);
   }

}
