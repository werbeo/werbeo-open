package org.infinitenature.werbeo.cms.vaadin.components.maps;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ShapeFileProcessor
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(ShapeFileProcessor.class);

   public String process(File uploadingFile) throws IOException
   {

      String dir = Files.createTempDirectory("userShapeUnzip").toString();

      String fileName = "";
      // find file name
      try (ZipInputStream zipIn = new ZipInputStream(
            new FileInputStream(uploadingFile.getPath())))
      {

         ZipEntry entry = zipIn.getNextEntry();
         while (entry != null)
         {
            // skip system directories
            if (entry.getName().startsWith("__"))
            {
               zipIn.closeEntry();
               entry = zipIn.getNextEntry();
               continue;
            }

            if (entry.getName().endsWith("shp"))
            {
               fileName = entry.getName();
            }

            if (entry.isDirectory())
            {
               new File(dir, entry.getName()).mkdirs();
            } else
            {
               extractFile(zipIn, dir + "/" + entry.getName());
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
         }
         zipIn.close();
      }
      return ShapeUtils.shapeToWkt(new File(dir, fileName));
   }

   private void extractFile(ZipInputStream zipIn, final String path)
   {
      try (BufferedOutputStream bos = new BufferedOutputStream(
            new FileOutputStream(path)))
      {
         byte[] bytesIn = new byte[4096];
         int read = 0;
         while ((read = zipIn.read(bytesIn)) != -1)
         {
            bos.write(bytesIn, 0, read);
         }
         bos.close();

      } catch (Exception e)
      {
         LOGGER.error("Error extractib file from zip", e);
      }
   }

}
