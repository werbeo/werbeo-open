package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.*;

public class CollapsiblePanel extends Panel
{
   private HorizontalLayout contentLayout = new HorizontalLayout();
   
   private boolean showContent; 
   
   public CollapsiblePanel(String caption, boolean openByDefault)
   {
      VerticalLayout mainLayout = new VerticalLayout();
      mainLayout.setSizeFull();
      Button link = new Button(caption);
      link.setStyleName("link");
      link.addClickListener(event->{
         showContent = !showContent;
         contentLayout.setVisible(showContent);
      });
      mainLayout.addComponent(link);
      showContent = openByDefault;
      contentLayout.setVisible(showContent);
      contentLayout.setSizeFull();
      mainLayout.addComponent(contentLayout);
      setContent(mainLayout);
   }
   
   public void addContent(Component content)
   {
      contentLayout.removeAllComponents();
      contentLayout.addComponent(content);
   }
}
