package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class SettlementStatusCaptionGenerator
      extends I18NCaptionGenerator<SettlementStatus>
{

   public SettlementStatusCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
