package org.infinitenature.werbeo.cms.vaadin.components;

import org.vaadin.viritin.fields.IntegerField;

public class PositiveIntegerField extends IntegerField
{
   private static final long serialVersionUID = -690739371772120642L;

   @Override
   protected void configureHtmlElement()
   {
      super.configureHtmlElement();
      // prevent all but numbers with a simple js
      s.setJavaScriptEventHandler("keypress",
            "function(e) {if(e.metaKey || e.ctrlKey) return true; var c = viritin.getChar(e); return c==null || /^[\\d\\n\\t\\r]+$/.test(c);}");
      s.setProperty("min", "0");
   }

   @Override
   public PositiveIntegerField withValueChangeListener(
         ValueChangeListener<Integer> listener)
   {
      return (PositiveIntegerField) super.withValueChangeListener(listener);
   }
}
