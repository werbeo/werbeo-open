package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.werbeo.cms.vaadin.components.map.*;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent.Selection;
import org.infintenature.mtb.MTBHelper;

import org.locationtech.jts.geom.*;
import org.locationtech.jts.io.WKTWriter;

public class PositionCreator
{
   MapCRSTranslator mtbTranslator = new MapCRSTranslator(Values.PPRESENTATION_EPSG, Values.MTB_EPSG);
   
   public Position createFromGeometry(Geometry geometry, PositionType positionType, Selection mtbQuadrant)
   {
      if(geometry instanceof Point)
      {
         Point pointModel = (Point)geometry;
         Position position = new Position();
         WKTWriter wktWriter = new WKTWriter();
         position.setWkt(wktWriter.write(pointModel));
         position.setWktEpsg(pointModel.getSRID());
         position.setEpsg(pointModel.getSRID());
         position.setPosCenterLatitude(pointModel.getY());
         position.setPosCenterLongitude(pointModel.getX());
         position.setType(Position.PositionType.POINT);
         return position;
      }
      else if(geometry instanceof Polygon)
      {
         Polygon polygon = (Polygon)geometry;
         Position position = new Position();
         WKTWriter wktWriter = new WKTWriter();
         position.setWkt(wktWriter.write(polygon));
         position.setWktEpsg(polygon.getSRID());
         position.setEpsg(polygon.getSRID());
         position.setPosCenterLatitude(polygon.getCentroid().getY());
         position.setPosCenterLongitude(polygon.getCentroid().getX());
         if(PositionType.MTB.equals(positionType))
         {
            position.setType(Position.PositionType.MTB);
            
            
            String mtbString = MTBHelper.fromCentroid(
                  mtbTranslator.toModel(polygon).getCentroid()
                  .getCoordinates()[0].x,
            mtbTranslator.toModel(polygon).getCentroid()
                  .getCoordinates()[0].y);
            mtbString = MTBUtils.processQuadrantSelectiom(mtbString, mtbQuadrant);
            position.setMtb(new org.infinitenature.service.v1.types.MTB(mtbString));
            
         }
         else
         {
            position.setType(Position.PositionType.SHAPE);
         }
         return position;
      }
      else if(geometry instanceof MultiPolygon)
      {
         MultiPolygon polygon = (MultiPolygon)geometry;
         Position position = new Position();
         WKTWriter wktWriter = new WKTWriter();
         position.setWkt(wktWriter.write(polygon));
         position.setWktEpsg(polygon.getSRID());
         position.setEpsg(polygon.getSRID());
         position.setPosCenterLatitude(polygon.getCentroid().getY());
         position.setPosCenterLongitude(polygon.getCentroid().getX());
         position.setType(Position.PositionType.SHAPE);
         return position;
      }
      return null;
   }
}
