package org.infinitenature.werbeo.cms.vaadin;

import java.time.LocalDate;

import com.vaadin.data.HasValue;
import com.vaadin.shared.Registration;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

public class TimeSpanFilter extends Composite implements HasValue<LocalDate>

{

   private static final String FROM = "Von"; // TODO use property Service ->
                                             // i18n
   private static final String TO = "Bis"; // TODO use property Service -> i18n

   private enum DateFilter
   {
      FROM, TO
   }

   private DateField from;
   private DateField to;

   public TimeSpanFilter()
   {
      final HorizontalLayout dateFieldLayout = new HorizontalLayout();
      from = new DateField();
      from.setDateFormat("dd.MM.yyyy");
      to = new DateField();
      to.setDateFormat("dd.MM.yyyy");
      createAndConfigureDateFields();
      layoutDateFields();
      dateFieldLayout.addComponents(from, to);
      dateFieldLayout.setMargin(false);
      configureCompositionRoot(dateFieldLayout);
      from.setRangeEnd(LocalDate.now());
      from.setDateOutOfRangeMessage("Datum muss in der Vergangenheit liegen");
      to.setRangeEnd(LocalDate.now());
      to.setDateOutOfRangeMessage("Datum muss in der Vergangenheit liegen");
   }

   private void createAndConfigureDateFields()
   {
      createAndConfigureDateField(from, DateFilter.FROM, FROM);
      createAndConfigureDateField(to, DateFilter.TO, TO);
   }

   private void createAndConfigureDateField(final DateField dateField,
         final DateFilter dateFilter, final String placeHolder)
   {
      dateField.setData(dateFilter);
      dateField.setPlaceholder(placeHolder);
   }

   private void layoutDateFields()
   {
      layoutDateField(from);
      layoutDateField(to);
   }

   public void layoutDateField(final DateField field)
   {
      // field.setWidth("90px");
      field.addStyleName(ValoTheme.DATEFIELD_ALIGN_CENTER);
   }

   private void configureCompositionRoot(
         final AbstractOrderedLayout compositionRoot)
   {
      setCompositionRoot(compositionRoot);
      compositionRoot.setComponentAlignment(from, Alignment.MIDDLE_LEFT);
      compositionRoot.setComponentAlignment(to, Alignment.MIDDLE_LEFT);
   }

   @Override
   public Registration addValueChangeListener(
         ValueChangeListener<LocalDate> listener)
   {
      from.addValueChangeListener(listener);
      to.addValueChangeListener(listener);
      return null;
   }

   @Override
   public void setReadOnly(boolean readOnly)
   {
      super.setReadOnly(readOnly);
      from.setReadOnly(readOnly);
      to.setReadOnly(readOnly);
   }

   @Override
   public boolean isReadOnly()
   {

      return super.isReadOnly();
   }

   public LocalDate getFrom()
   {
      return from.getValue();
   }

   public LocalDate getTo()
   {
      return to.getValue();
   }

   public void setFrom(LocalDate date)
   {
      from.setValue(date);
   }

   public void setTo(LocalDate date)
   {
      to.setValue(date);
   }

   @Override
   public void setRequiredIndicatorVisible(boolean visible)
   {
      super.setRequiredIndicatorVisible(visible);
   }

   @Override
   public boolean isRequiredIndicatorVisible()
   {
      return super.isRequiredIndicatorVisible();
   }

   @Override
   public void setValue(LocalDate value)
   {
      to.setValue(value);
      from.setValue(value);
   }

   @Override
   public LocalDate getValue()
   {
      // TODO Auto-generated method stub
      return null;
   }
}
