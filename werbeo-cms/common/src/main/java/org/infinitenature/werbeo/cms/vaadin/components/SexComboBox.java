package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence.Sex;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.SexCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class SexComboBox extends EnumComboBox<Sex>
{

   public SexComboBox(MessagesResource messages)
   {
      super(messages, Sex.class, new SexCaptionGenerator(messages));
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "SEX", getLocale()));
   }
}
