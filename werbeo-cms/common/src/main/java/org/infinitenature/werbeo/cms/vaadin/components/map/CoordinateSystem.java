package org.infinitenature.werbeo.cms.vaadin.components.map;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum CoordinateSystem
{
   EPSG4326(4326, "WGS84 - World Geodetic System 1984: EPSG 4326"),
   EPSG4745(4745, "RD/83: EPSG 4745"),
   EPSG5678(5678, "DHDN / 3-degree Gauss-Kruger zone 4: EPSG 5678"),
   EPSG31467(31467, "DHDN / 3-degree Gauss-Kruger zone 3: EPSG 31467 (Potsdam)"),
   EPSG31468(31468, "DHDN / 3-degree Gauss-Kruger zone 4: EPSG 31468 (Potsdam)"),
   EPSG31469(31469, "DHDN / 3-degree Gauss-Kruger zone 5: EPSG 31469 (Potsdam)"),
   EPSG4258(4258, "ETRS89 Ellipsoid GRS 1980: EPSG 4258"),

   EPSG25832(25832, "ETRS89 / UTM zone 32N: EPSG 25832"),
   EPSG25833(25833, "ETRS89 / UTM zone 33N: EPSG 25833"),
   EPSG5650(5650, "ETRS89 / UTM zone 33N (zE-N): EPSG 5650 "),
   EPSG258337(258337, "ETRS89 UTM Zone33 BB 7: EPSG 258337"),
   EPSG900913(900913, "Google Maps Global Spherical Mercator: EPSG 900913"),
   EPSG3857(3857, "WGS 84 / Pseudo-Mercator (Google maps, OpenStreetMap, etc)"),

   UNSUPPORTED(-1, "unsupported coordinate system");


   
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CoordinateSystem.class);

   private int epsg;
   private String name;

   CoordinateSystem(int epsg, String name)
   {
      this.epsg = epsg;
      this.name = name;
   }

   
   public static List<CoordinateSystem> getGaussKruegerSystems()
   {
      List<CoordinateSystem> systems = new ArrayList<CoordinateSystem>();
      systems.add(EPSG5678);
      systems.add(EPSG31467);
      systems.add(EPSG31468);
      systems.add(EPSG31469);
      return systems;
   }
   
   public static CoordinateSystem get(
         org.infinitenature.service.v1.types.enums.CoordinateSystem coordinateSystem)
   {
      switch (coordinateSystem)
      {
      case EPSG4326:
         return EPSG4326;
      case EPSG4745:
         return EPSG4745;
      case EPSG5678:
         return EPSG5678;
      case EPSG31467:
         return EPSG31467;
      case EPSG31468:
         return EPSG31468;
      case EPSG31469:
         return EPSG31469;
      case EPSG4258:
         return EPSG4258;
      case EPSG25832:
         return EPSG25832;
      case EPSG25833:
         return EPSG25833;
      case EPSG258337:
         return EPSG258337;
      case EPSG900913:
         return EPSG900913;
      case EPSG5650:
         return EPSG5650;
      case EPSG3857:
         return EPSG3857;
      default:
         LOGGER.error(
               "Coordinate System not supported: " + coordinateSystem.name());
         return UNSUPPORTED;
      }
   }

   
   public static CoordinateSystem get(int epsg)
   {
      switch (epsg)
      {
      case 3857:
         return EPSG3857;
      case 4326:
         return EPSG4326;
      case 4745:
         return EPSG4745;
      case 5678:
         return EPSG5678;
      case 31467:
         return EPSG31467;
      case 31468:
         return EPSG31468;
      case 31469:
         return EPSG31469;
      case 25832:
         return EPSG25832;
      case 25833:
         return EPSG25833;
      case 258337:
         return EPSG258337;
      case 900913:
         return EPSG900913;
      case 4258:
         return EPSG4258;
      case 5650:
         return EPSG5650;
      default:
         LOGGER.error("Coordinate System not supported: " + epsg);
         return UNSUPPORTED;
      }
   }

   public int getEpsg()
   {
      return epsg;
   }

   public String getName()
   {
      return name;
   }
}
