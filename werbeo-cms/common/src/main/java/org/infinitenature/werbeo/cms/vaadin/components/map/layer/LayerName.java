package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

public enum LayerName
{
   MTB("Messtischblatt"),
   ORTHOFOTO_MV("Orthofoto (bitte einzoomen)"),
   TOPO_MV("Topographische Karte MV"),
   SATELLITE("Satelliten-Ansicht"),
   OPEN_TOPO_MAP("OpenTopoMap"),
   OPEN_STREET_MAP("OpenStreetMap"), OCCURRENCES("Funde");

   private String caption;

   LayerName(String caption)
   {
      this.caption = caption;
   }

   public static LayerName get(String caption)
   {
      switch (caption)
      {
      case "Messtischblatt": return LayerName.MTB;
      case "Orthofoto (bitte einzoomen)": return LayerName.ORTHOFOTO_MV;
      case "Topographische Karte MV": return  LayerName.TOPO_MV;
      case "OpenTopoMap": return LayerName.OPEN_TOPO_MAP;
      case "Satellitensicht": return LayerName.SATELLITE;
      case "OpenStreetMap": return LayerName.OPEN_STREET_MAP;
      default:
         throw new IllegalStateException("Unexpected value: " + caption);
      }
   }

   public String getCaption()
   {
      return caption;
   }
}
