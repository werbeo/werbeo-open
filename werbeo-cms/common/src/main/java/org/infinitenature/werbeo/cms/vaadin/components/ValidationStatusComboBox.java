package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.ValidationStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class ValidationStatusComboBox extends EnumComboBox<ValidationStatus>
{

   public ValidationStatusComboBox(MessagesResource messages)
   {
      super(messages, ValidationStatus.class,
            new ValidationStatusCaptionGenerator(messages));
   }

   public ValidationStatusComboBox(MessagesResource messages,
         String nullSelectionCaption)
   {
      super(messages, ValidationStatus.class,
            new ValidationStatusCaptionGenerator(messages,
                  nullSelectionCaption));
   }
}
