package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.UI;

public class TaxonComboBox extends ComboBox<TaxonBase>
{
   private MessagesResource messages;

   public TaxonComboBox(MessagesResource messages, Werbeo werbeo,
         InstanceConfig instanceConfig, boolean onlyUsedTaxa,
         boolean onlyTaxaAvailableForInput, boolean withSynonyms)
   {
      this(messages, true);
      setDataProvider(new TaxonComboBoxDataProvider(werbeo, instanceConfig,
            onlyUsedTaxa, onlyTaxaAvailableForInput, withSynonyms));
   }

   public TaxonComboBox(MessagesResource messages, boolean useCaption)
   {
      this.messages = messages;
      setProperties(useCaption);
   }

   private void setProperties(boolean useCaption)
   {
      setItemCaptionGenerator(TaxonBase::getName);
      if(useCaption)
      {
         setCaption(translate("SPECIES"));
      }
      setDescription(translate("MANUAL"));
      setPopupWidth("auto");
   }

   private String translate(String field)
   {
      return messages.getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   public TaxonComboBox withSelected(TaxonBase taxon)
   {
      this.setValue(taxon);
      return this;
   }

   public TaxonComboBox withOnChange(ValueChangeListener<TaxonBase> listener)
   {
      this.addValueChangeListener(listener);
      return this;
   }

   public TaxonComboBox withSizeFull()
   {
      setSizeFull();
      return this;
   }
}
