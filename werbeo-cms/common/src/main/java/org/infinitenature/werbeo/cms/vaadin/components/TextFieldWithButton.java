package org.infinitenature.werbeo.cms.vaadin.components;

import org.w3c.dom.ls.LSInput;

import com.vaadin.data.HasValue;
import com.vaadin.server.Resource;
import com.vaadin.shared.Registration;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

public class TextFieldWithButton extends CssLayout implements HasValue<String>
{

   /**
    * 
    */
   private static final long serialVersionUID = 1L;
   private final TextField textField;
   private final Button button;
   private final HasValue buttonValueHolder;

   public TextFieldWithButton(String caption, String placeholder, Resource icon,
         HasValueAndClickListener listener)
   {
      setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

      textField = new TextField();

      setCaption(caption);
      textField.setPlaceholder(placeholder);
      textField.addStyleName("no-border");
      textField.addStyleName("icon-text-field");

      textField.addFocusListener(e -> addStyleName("focus-border-shadow"));

      textField.addBlurListener(e -> removeStyleName("focus-border-shadow"));

      button = new Button(icon);
      button.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
      button.addStyleName("inner-icon");
      button.addClickListener(listener);
      this.buttonValueHolder = listener;
      addComponents(textField, button);
   }

   public TextField getTextField()
   {
      return textField;
   }

   public Button getButton()
   {
      return button;
   }

   @Override
   public void setValue(String value)
   {
      // TODO Auto-generated method stub
      if(value == null)
      {
         textField.setValue("");
         buttonValueHolder.setValue(null);
      }
      else
      {
         textField.setValue(value);
      }
   }
   
   @Override
   public boolean isReadOnly()
   {
      // TODO Auto-generated method stub
      return super.isReadOnly();
   }
   
   @Override
   public boolean isRequiredIndicatorVisible()
   {
      // TODO Auto-generated method stub
      return super.isRequiredIndicatorVisible();
   }

   @Override
   public void setReadOnly(boolean readOnly)
   {
      // TODO Auto-generated method stub
      super.setReadOnly(readOnly);
   }
   
   @Override
   public String getValue()
   {
      return textField.getValue();
   }
   
   @Override
   public void setRequiredIndicatorVisible(boolean visible)
   {
      // TODO Auto-generated method stub
      super.setRequiredIndicatorVisible(visible);
   }

   @Override
   public Registration addValueChangeListener(
         ValueChangeListener<String> listener)
   {
      return buttonValueHolder.addValueChangeListener(listener);
   }
   
   public void setPlaceholder(String placeholder)
   {
      textField.setPlaceholder(placeholder);
   }
}

