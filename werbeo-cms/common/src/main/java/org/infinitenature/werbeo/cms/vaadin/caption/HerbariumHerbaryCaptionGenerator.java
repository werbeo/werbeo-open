package org.infinitenature.werbeo.cms.vaadin.caption;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.Herbarium;

public class HerbariumHerbaryCaptionGenerator implements WerbeoCaptionGenerator<Herbarium>
{
   @Override
   public String apply(Herbarium herbarium)
   {
      if(herbarium == null)
      {
         return "";
      }
      if(StringUtils.isBlank(herbarium.getHerbary()))
      {
         return "";
      }
      return herbarium.getHerbary();
   }
}
