package org.infinitenature.werbeo.cms.vaadin.components;

public interface FormListener<T>
{
   public void onSave(T source);

   public void onCancel(T source);
}
