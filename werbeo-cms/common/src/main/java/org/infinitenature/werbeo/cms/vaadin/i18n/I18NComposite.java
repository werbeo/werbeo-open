package org.infinitenature.werbeo.cms.vaadin.i18n;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

import com.vaadin.ui.Composite;

public abstract class I18NComposite extends Composite implements I18N
{
   private final MessagesResource messages;

   public I18NComposite(MessagesResource messages)
   {
      this.messages = messages;
   }

   @Override
   public MessagesResource getMessages()
   {
      return messages;
   }

   @Override
   public void attach()
   {
      super.attach();
      applyLocalizations();
   }
}
