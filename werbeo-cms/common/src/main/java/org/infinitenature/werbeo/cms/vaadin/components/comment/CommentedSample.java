package org.infinitenature.werbeo.cms.vaadin.components.comment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.collections4.keyvalue.DefaultMapEntry;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SampleComment;

public class CommentedSample
{
   private final Sample sample;
   private List<OccurrenceComment> occurrenceComments = new ArrayList<>();
   private List<SampleComment> sampleComments = new ArrayList<>();

   public CommentedSample()
   {
      sample = new Sample();
      occurrenceComments = Collections.emptyList();
      sampleComments = Collections.emptyList();
   }

   public CommentedSample(Sample sample,
         List<OccurrenceComment> occurrenceComments,
         List<SampleComment> sampleComments)
   {
      super();
      this.sample = sample;
      this.occurrenceComments = occurrenceComments;
      this.sampleComments = sampleComments;
   }

   public List<SampleComment> getSampleComments()
   {
      return sampleComments;
   }

   public Map<Occurrence, List<OccurrenceComment>> getOccurrenceComments()
   {
      return occurrenceComments
            .stream()
            .collect(Collectors.groupingBy(OccurrenceComment::getTargetId))
            .entrySet().stream()
            .map(entry -> new DefaultMapEntry<>(findOccurrence(entry.getKey()),
                  entry.getValue()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
   }

   private Occurrence findOccurrence(UUID key)
   {
      return sample.getOccurrences().stream()
            .filter(occurrence -> occurrence.getEntityId().equals(key))
            .findFirst().orElseThrow(IllegalArgumentException::new);
   }
   
   public boolean hasComments()
   {
      return (occurrenceComments.size() + sampleComments.size()) > 0;
   }

   public Sample getSample()
   {
      return sample;
   }

}
