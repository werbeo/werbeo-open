package org.infinitenature.werbeo.cms.vaadin.plugin.tables;

public interface TableObserver<FILTER_TYPE>
{
   public void onFilterChange(FILTER_TYPE filter);

}
