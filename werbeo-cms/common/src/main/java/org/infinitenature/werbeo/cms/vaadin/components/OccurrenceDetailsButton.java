package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.*;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.formatter.PersonFormatter;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

public class OccurrenceDetailsButton extends I18NButton implements
       Button.ClickListener
{


   private Window window;
   private Button selectButton;
   private ComboBox<Person> personComboBox = new ComboBox<>();
   private RecordStatusComboBox recordStatusComboBox;
   private String caption;

   public OccurrenceDetailsButton(MessagesResource messages, String caption, ClickListener listener, Werbeo werbeo) {
      super(messages);
      this.caption = caption;
      setStyleName("link");
      window = new Window();
      window.setWidth("500px");
      window.setHeight("200px");
      window.center();
      VerticalLayout windowLayout = new VerticalLayout();
      HorizontalLayout inputLayout = new HorizontalLayout();
      recordStatusComboBox = new RecordStatusComboBox(messages);
      personComboBox.setDataProvider(new PersonComboBoxDataProvider(werbeo));
      personComboBox.setItemCaptionGenerator(PersonFormatter::format);
      selectButton = new Button();
      inputLayout.addComponent(personComboBox);
      inputLayout.addComponent(recordStatusComboBox);
      windowLayout.addComponent(inputLayout);
      windowLayout.addComponent(selectButton);
      window.setContent(windowLayout);
      selectButton.addClickListener(listener);
      selectButton.addClickListener(event -> {UI.getCurrent().removeWindow(window);});
      addClickListener(this);
   }

   public Person getDeterminer()
   {
      return personComboBox.getValue();
   }

   public Occurrence.RecordStatus getRecordStatus()
   {
      return recordStatusComboBox.getValue();
   }

   @Override
   public void buttonClick(ClickEvent clickEvent)
   {
      UI.getCurrent().addWindow(window);
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   @Override
   protected void init()
   {

   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      personComboBox.setCaption(translate("DETERMINER"));
      recordStatusComboBox.applyLocalizations();
      recordStatusComboBox.setCaption(translate("STATUS"));
      selectButton.setCaption(translate("CHOOSE"));
   }


   @Override
   protected String getKey()
   {
      return caption;
   }
}
