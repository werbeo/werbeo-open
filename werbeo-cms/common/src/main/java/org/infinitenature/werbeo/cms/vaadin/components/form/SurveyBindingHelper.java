package org.infinitenature.werbeo.cms.vaadin.components.form;

import java.util.HashMap;
import java.util.Set;

import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.werbeo.cms.vaadin.fieldconfig.FieldConfigUtils;

public class SurveyBindingHelper extends BindingHelper<SurveyField>
{
   public SurveyBindingHelper(Set<SurveyFieldConfig> surveyFieldConfigs)
   {
      this.fieldConfigs = FieldConfigUtils
            .toSurveyFieldConfigMap(surveyFieldConfigs);
      this.fieldMapping = new HashMap<>();
   }

   @Override
   protected String getEntityKey()
   {
      return "Survey";
   }
}
