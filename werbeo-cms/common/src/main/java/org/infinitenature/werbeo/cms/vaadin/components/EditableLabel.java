package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class EditableLabel extends CustomComponent
{

   private Label label;
   private Button edit;

   public EditableLabel(String caption, Button.ClickListener listener)
   {
      label = new Label(caption);
      edit = new LinkButton("", listener);
      edit.setIcon(VaadinIcons.EDIT);
      HorizontalLayout layout = new HorizontalLayout();
      layout.addComponent(edit);
      layout.addComponent(label);
      setCompositionRoot(layout);
   }

   public void addEditListener(Button.ClickListener listener)
   {
      edit.addClickListener(listener);
   }

}
