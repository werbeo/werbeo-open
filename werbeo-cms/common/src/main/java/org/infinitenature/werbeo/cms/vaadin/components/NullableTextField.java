package org.infinitenature.werbeo.cms.vaadin.components;

import org.vaadin.viritin.fields.MTextField;

public class NullableTextField extends MTextField
{

   @Override
   public void setValue(String value)
   {
      super.setValue(value == null ? "" : value);
   }
}
