package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent.Selection;

public interface MTBQuadrantSelector
{
   public Selection getSelection();
   public void setMTBString(String mtbString);
}
