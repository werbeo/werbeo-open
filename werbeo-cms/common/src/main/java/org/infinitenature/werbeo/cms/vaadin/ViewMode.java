package org.infinitenature.werbeo.cms.vaadin;

public class ViewMode extends org.infinitenature.vct.ViewMode
{
   public static final ViewMode DESKTOP = new ViewMode();
   public static final ViewMode MOBILE = new ViewMode();

   @Override
   public String toString()
   {
      if (this == DESKTOP)
      {
         return "DESKTOP";
      } else
      {
         return "MOBILE";
      }
   }
}
