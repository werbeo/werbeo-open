package org.infinitenature.werbeo.cms.navigation;

import java.util.UUID;

public class SampleEdit implements NavigationTarget
{
   private final UUID sampleUUID;

   public SampleEdit(UUID sampleUUID)
   {
      super();
      this.sampleUUID = sampleUUID;
   }

   public UUID getSampleUUID()
   {
      return sampleUUID;
   }
}
