package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.*;

public class LayerListener implements LeafletBaseLayerChangeListener,
      LeafletOverlayAddListener, LeafletOverlayRemoveListener
{
   private Layers layers;

   public LayerListener(Layers layers)
   {
      this.layers = layers;
   }

   @Override
   public void onBaseLayerChange(LeafletBaseLayerChangeEvent event)
   {
      for (Layer layer : layers.getBaselayers())
      {
         if (layer.getCaption().equals(event.getName()))
         {
            layer.setActiveIndicator(true);
         } else
         {
            layer.setActiveIndicator(false);
         }
      }
   }

   @Override
   public void onOverlayAdd(LeafletOverlayAddEvent event)
   {
      for (Layer layer : layers.getOverlays())
      {
         if (layer.getCaption().equals(event.getName()))
         {
            layer.setActiveIndicator(true);
         }
      }
   }

   @Override
   public void onOverlayRemove(LeafletOverlayRemoveEvent event)
   {
      for (Layer layer : layers.getOverlays())
      {
         if (layer.getCaption().equals(event.getName()))
         {
            layer.setActiveIndicator(false);
         }
      }
   }

}
