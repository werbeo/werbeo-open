package org.infinitenature.werbeo.cms.vaadin;

import java.util.Locale;

public interface MessagesResource
{

   public String getMessage(Portal portal, String key, Locale locale,
         Object... parameter);

   public String getEntity(Portal portal, String entity, Locale locale);

   public String getEntityField(Portal portal, String entity, String field,
         Locale locale);

   public String getErrorMessage(Portal portal, Locale locale, Throwable e,
         Object... parameter);
}
