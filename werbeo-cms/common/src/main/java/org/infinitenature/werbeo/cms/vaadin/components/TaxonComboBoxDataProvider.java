package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.types.Language;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

public class TaxonComboBoxDataProvider
      extends AbstractBackEndDataProvider<TaxonBase, String>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TaxonComboBoxDataProvider.class);
   private final Werbeo werbeo;
   private final InstanceConfig instanceConfig;

   private static final Set<Language> latinOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.LAT)));

   private static final Set<Language> germanOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.DEU)));

   private final boolean onlyUsedTaxa;
   private final boolean onlyTaxaForInput;
   private final boolean includeSynonyms;

   public TaxonComboBoxDataProvider(Werbeo werbeo,
         InstanceConfig instanceConfig, boolean onlyUsedTaxa,
         boolean onlyTaxaForInput, boolean includeSynonyms)
   {
      super();
      this.werbeo = werbeo;
      this.instanceConfig = instanceConfig;
      this.onlyUsedTaxa = onlyUsedTaxa;
      this.onlyTaxaForInput = onlyTaxaForInput;
      this.includeSynonyms = includeSynonyms;
   }

   @Override
   protected Stream<TaxonBase> fetchFromBackEnd(Query<TaxonBase, String> query)
   {
      LOGGER.debug("Fetch - Offset: {}, Limit: {}, Filter: {}",
            query.getOffset(), query.getLimit(), query.getFilter());
      String nameContains = query.getFilter().orElse("");
      List<TaxonBase> content = werbeo.taxa().find(query.getOffset(),
            query.getLimit(),
            new TaxaFilter(getPortalId(), nameContains, latinOnly,
                  includeSynonyms, null, onlyUsedTaxa, null, onlyTaxaForInput))
            .getContent();
      LOGGER.debug("Fetch size: {}", content.size());
      if(instanceConfig.isTranslateTaxa())
      {
         TaxaFilter filter = new TaxaFilter(getPortalId(), "", germanOnly, true, null, onlyUsedTaxa, null, onlyTaxaForInput);
         filter.setExternalKeys(getExternalIds(content));
         List<TaxonBase> germanTaxa = werbeo.taxa().find(0, Integer.MAX_VALUE, filter).getContent();
         addTranslations(content, germanTaxa);
      }
      return content.stream();
   }

   private void addTranslations(List<TaxonBase> content,
         List<TaxonBase> germanTaxa)
   {
      for(TaxonBase taxon: content)
      {
         TaxonBase germanTaxon = findByExternalKey(germanTaxa, taxon.getExternalKey());
         if(germanTaxon != null)
         {
            taxon.setName(taxon.getName() + " (" + germanTaxon.getName() + ")");
         }
      }
   }

   private TaxonBase findByExternalKey(List<TaxonBase> taxa,
         String externalKey)
   {
     for(TaxonBase taxonBase : taxa)
     {
        if(StringUtils.equals(taxonBase.getExternalKey(), externalKey))
        {
           return taxonBase;
        }
     }
     return null;
   }

   private Set<String> getExternalIds(List<TaxonBase> content)
   {
     Set<String> externalKeys = new HashSet<>();

     for(TaxonBase taxon : content)
     {
        externalKeys.add(taxon.getExternalKey());
     }
     return externalKeys;
   }

   @Override
   protected int sizeInBackEnd(Query<TaxonBase, String> query)
   {
      String nameContains = query.getFilter().orElse("");
      int amount = (int) werbeo.taxa().count(
            new TaxaFilter(getPortalId(), nameContains, latinOnly, includeSynonyms, null,
                  onlyUsedTaxa, null, onlyTaxaForInput))
            .getAmount();
      return amount;
   }

   protected int getPortalId()
   {
      return Context.getCurrent().getApp().getPortalId();
   }

   public boolean isOnlyUsedTaxa()
   {
      return onlyUsedTaxa;
   }
}
