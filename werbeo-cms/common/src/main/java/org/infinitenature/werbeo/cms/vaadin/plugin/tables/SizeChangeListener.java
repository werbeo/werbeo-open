package org.infinitenature.werbeo.cms.vaadin.plugin.tables;

@FunctionalInterface
public interface SizeChangeListener
{
   public void onSizeChange(int size);
}
