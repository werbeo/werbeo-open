package org.infinitenature.werbeo.cms.navigation;

public class SurveyEdit implements NavigationTarget
{
   private final int surveyId;

   public SurveyEdit(int surveyId)
   {
      super();
      this.surveyId = surveyId;
   }

   public int getSurveyId()
   {
      return surveyId;
   }
}
