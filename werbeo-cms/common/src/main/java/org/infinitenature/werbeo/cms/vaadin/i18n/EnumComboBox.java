package org.infinitenature.werbeo.cms.vaadin.i18n;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

public abstract class EnumComboBox<T extends Enum<T>> extends I18NComboBox<T>
{
   private final I18NCaptionGenerator<T> captionGenerator;

   public EnumComboBox(MessagesResource messages, Class<T> enumClass,
         I18NCaptionGenerator<T> captionGenerator)
   {
      super(messages);
      this.captionGenerator = captionGenerator;
      setValue(null);
      setItems(enumClass.getEnumConstants());
      setItemCaptionGenerator(captionGenerator);
   }

   @Override
   public void applyLocalizations()
   {
      setEmptySelectionCaption(captionGenerator.getCaption(null));
   }
}
