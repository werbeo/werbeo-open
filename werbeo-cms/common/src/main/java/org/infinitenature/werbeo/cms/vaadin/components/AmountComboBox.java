package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence.Amount;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.AmountItemCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class AmountComboBox extends EnumComboBox<Amount>
{
   public AmountComboBox(MessagesResource messages)
   {
      super(messages, Amount.class, new AmountItemCaptionGenerator(messages));
   }

}
