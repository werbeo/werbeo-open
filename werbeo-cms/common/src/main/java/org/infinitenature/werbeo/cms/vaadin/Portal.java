package org.infinitenature.werbeo.cms.vaadin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vct.VCMSApp;

public class Portal extends VCMSApp
{
   private int piwikSiteId;
   private int portalId;
   private List<String> urlPattern = new ArrayList<>();

   
   public Portal(String name, int piwikSiteId, int portalId, String... urlPattern)
   {
      super(name);
      this.piwikSiteId = piwikSiteId;
      this.portalId = portalId;
      this.urlPattern = Arrays.asList(urlPattern);
   }
   
   public Portal(String name, int piwikSiteId, int portalId)
   {
      super(name);
      this.piwikSiteId = piwikSiteId;
      this.portalId = portalId;
   }

   public Portal(String name, String imageURL, String description,
         int piwikSiteId, int portalId)
   {
      super(name, imageURL, description);
      this.piwikSiteId = piwikSiteId;
      this.portalId = portalId;
   }

   public int getPiwikSiteId()
   {
      return piwikSiteId;
   }

   public void setPiwikSiteId(int piwikSiteId)
   {
      this.piwikSiteId = piwikSiteId;
   }

   public int getPortalId()
   {
      return portalId;
   }

   public void setPortalId(int portalId)
   {
      this.portalId = portalId;
   }

   public boolean matchUrlPattern(String hostName)
   {
      for(String pattern : urlPattern)
      {
         if(StringUtils.containsIgnoreCase(hostName, pattern))
         {
            return true;
         }
      }
      return false;
   }
}
