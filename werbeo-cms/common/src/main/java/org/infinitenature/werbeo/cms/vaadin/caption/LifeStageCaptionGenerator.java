package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class LifeStageCaptionGenerator extends I18NCaptionGenerator<Occurrence.LifeStage>
{

   public LifeStageCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
