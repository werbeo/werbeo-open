package org.infinitenature.werbeo.cms.vaadin.components;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.meta.OccurrenceField;

import com.vaadin.shared.Registration;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.datefield.DateResolution;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TabSheet.Tab;
import org.locationtech.jts.geom.Point;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

public class VagueDateTimeField extends Button
       implements HasValueAndClickListener
{
   private static final String DATE_IN_PAST = "DATE_IN_PAST";

   private static final String DATE_FORMAT_DAY = "DATE_FORMAT_DAY";

   private static final String DATE_START_FIELD = "DATE_START_FIELD";

   private static final long serialVersionUID = -5422740822456816743L;
   
   private Window window;
   private Button selectButton;
   private List<ValueChangeListener<VagueDate>> valueChangeListeners = new ArrayList<>();
   private VagueDate dateValue;
   private String timeValue;
   private DateField dateField;
   private DateField dateStartField;
   private DateField dateEndField;
   private DateField monthStartField;
   private DateField monthEndField;
   private DateField yearStartField;
   private DateField yearEndField;
   private TimeComboBox timeField;
   private Tab dateTimeTab;
   private Tab daysTab;
   private Tab yearsTab;
   private MessagesResource messages;
   private final Map<OccurrenceField, Boolean> occurrenceFieldConfigs;
   private boolean withTime;
   
  
   TabSheet tabSheet = new TabSheet();

   public VagueDateTimeField(MessagesResource messages, VagueDate vagueDate,
         String time, Map<OccurrenceField, Boolean> occurrenceFieldConfigs, boolean withTime)
   {
      this.messages = messages;
      this.occurrenceFieldConfigs = occurrenceFieldConfigs;
      this.withTime = withTime;
      selectButton = new Button(translate("CHOOSE"));
      dateField = new DateField(translate("DATE_FIELD"));
      dateStartField = new DateField(translate(DATE_START_FIELD));
      dateEndField = new DateField(translate("DATE_END_FIELD"));
      monthStartField = new DateField(translate("MONTH_START_FIELD"));
      monthEndField = new DateField(translate("MONTH_END_FIELD"));
      yearStartField = new DateField(translate("YEAR_START_FIELD"));
      yearEndField = new DateField(translate("YEAR_END_FIELD"));
      timeField = new TimeComboBox(messages, translate("TIME_FIELD"));

      dateField.setRangeEnd(LocalDate.now());
      dateField.setDateFormat(translate(DATE_FORMAT_DAY));
      dateField.setDateOutOfRangeMessage(translate(DATE_IN_PAST));
      dateStartField.setRangeEnd(LocalDate.now());
      dateStartField.setDateOutOfRangeMessage(translate(DATE_IN_PAST));
      dateStartField.setDateFormat(translate(DATE_FORMAT_DAY));
      dateEndField.setRangeEnd(LocalDate.now());
      dateEndField.setDateOutOfRangeMessage(translate(DATE_IN_PAST));
      dateEndField.setDateFormat(translate(DATE_FORMAT_DAY));
      
      monthStartField.setRangeEnd(LocalDate.now());
      monthStartField.setDateOutOfRangeMessage(translate(DATE_IN_PAST));
      monthStartField.setDateFormat(translate("VagueDateField.DATE_FORMAT_MONTH"));
      monthEndField.setRangeEnd(LocalDate.now());
      monthEndField.setDateOutOfRangeMessage(translate(DATE_IN_PAST));
      monthEndField.setDateFormat(translate("VagueDateField.DATE_FORMAT_MONTH"));
      
      yearStartField.setRangeEnd(LocalDate.now());
      yearStartField.setDateOutOfRangeMessage("Datum muss in der Vergangenheit liegen");
      yearStartField.setDateFormat(translate("DATE_FORMAT_YEAR"));
      yearStartField.setResolution(DateResolution.YEAR);
      yearStartField.setDefaultValue(LocalDate.of( LocalDate.now().getYear(),1, 1 ));
      yearEndField.setDateOutOfRangeMessage("Datum muss in der Vergangenheit liegen");
      yearEndField.setDateFormat(translate("DATE_FORMAT_YEAR"));
      yearEndField.setResolution(DateResolution.YEAR);
      yearEndField.setDefaultValue(LocalDate.of( LocalDate.now().getYear(),12, 31 ));
      
      this.dateValue = vagueDate;
      this.timeValue = time;
      
      setCaption(VagueDate.format(vagueDate, true));
      addClickListener(this);
      
      window = new Window();
      window.setWidth("500px");
      window.setHeight("300px");
      window.center();
      window.setCaption(translate("DATE_OF_RECORDING"));
      window.setModal(true);

      VerticalLayout windowLayout = new VerticalLayout();
      dateTimeTab = tabSheet.addTab(createDateTab(), translate("DAY"));
      daysTab = tabSheet.addTab(createDateRangeTab(), translate("DAYS"));
      yearsTab = tabSheet.addTab(createYearRangeTab(), translate("YEARS"));
      windowLayout.addComponent(tabSheet);
      windowLayout.addComponent(selectButton);
      window.setContent(windowLayout);
      
      initButton();
      refresh();
   }

   private void initButton()
   {
      selectButton.addClickListener(event -> {
         this.dateValue = new VagueDate();

         if (tabSheet.getSelectedTab().equals(dateTimeTab.getComponent()))
         {
            if (checkTimeFieldError())
            {
               return;
            }
            readValueOfDateTimeTab();
         }
         else if (tabSheet.getSelectedTab().equals(daysTab.getComponent()))
         {
            if (checkDaysTabError())
            {
               return;
            }
            readValueOfDaysTab();
         }
         else if (tabSheet.getSelectedTab().equals(yearsTab.getComponent()))
         {
            if (checkYearsTabError())
            {
               return;
            }
            readValueOfYearsTab();
         }

         refresh();
         UI.getCurrent().removeWindow(window);
         for (ValueChangeListener listener : valueChangeListeners)
         {
            listener.valueChange(new ValueChangeEvent(this, Point.class, true));
         }
      });
   }

   private boolean checkYearsTabError()
   {
      if (yearEndField.getValue() != null && yearStartField.getValue() != null
            && yearEndField.getValue().isBefore(yearStartField.getValue()))
      {
         Notification.show(translate("START_BEFORE_END"), Type.ERROR_MESSAGE);
         yearEndField.setValue(null);
         yearStartField.setValue(null);
         return true;
      }
      return false;
   }

   private boolean checkDaysTabError()
   {
      if (dateEndField.getValue().isBefore(dateStartField.getValue()))
      {
         Notification.show(translate("START_BEFORE_END"), Type.ERROR_MESSAGE);
         dateEndField.setValue(null);
         dateStartField.setValue(null);
         return true;
      }
      return false;
   }

   private boolean checkTimeFieldError()
   {
      if (timeField.getValue() != null)
      {
         LocalDateTime now = LocalDateTime.now();
         LocalDateTime input = LocalDateTime.of(dateField.getValue().getYear(),
               dateField.getValue().getMonth(),
               dateField.getValue().getDayOfMonth(),
               Integer.valueOf(timeField.getValue().split(":")[0]),
               Integer.valueOf(timeField.getValue().split(":")[1]));
         if (input.isAfter(now))
         {
            Notification.show(translate("TIME_IN_FUTURE"), Type.ERROR_MESSAGE);
            timeField.setValue(timeValue);
            return true;
         }
      }
      return false;
   }

   private void readValueOfYearsTab()
   {
      if (yearStartField.getValue() == null)
      {
         yearStartField.setValue(LocalDate.of(1800, 1, 1));
      }
      
      if (yearEndField.getValue() == null )
      {
         if (yearStartField.getValue() != null)
         {
            yearEndField.setValue(LocalDate.of(yearStartField.getValue().getYear(), 12, 31));
         }
         else
         {
            yearEndField.setValue(LocalDate.of(LocalDate.now().getYear(), 12, 31));
         }
      }
      
      dateValue.setType(VagueDate.VagueDateType.YEARS);
      dateValue.setFrom(yearStartField.getValue());
      // we want to search the whole year, not only the 1st of Jan
      dateValue.setTo(toLastDayOfYear(yearEndField.getValue()));
      this.timeValue = null;
   }

   private void readValueOfDaysTab()
   {
      dateValue.setType(VagueDate.VagueDateType.DAYS);
      dateValue.setFrom(dateStartField.getValue());
      dateValue.setTo(dateEndField.getValue());
      this.timeValue = null;
   }

   private void readValueOfDateTimeTab()
   {
      dateValue.setType(VagueDate.VagueDateType.DAY);
      dateValue.setFrom(dateField.getValue());
      dateValue.setTo(dateField.getValue());
      if(this.withTime && this.occurrenceFieldConfigs.containsKey(OccurrenceField.TIME_OF_DAY))
      {
         this.timeValue = timeField.getValue();
      }
   }

   private LocalDate toLastDayOfYear(LocalDate value2)
   {
      return LocalDate.of(value2.getYear(), 12, 31);
   }

   private Component createDateRangeTab()
   {
      GridLayout layout = new GridLayout(2, 1);
      layout.setMargin(new MarginInfo(true, false));
      layout.setSpacing(true);
      layout.addComponent(dateStartField);
      layout.addComponent(dateEndField);
      return layout;
   }
   
   private Component createYearRangeTab()
   {
      GridLayout layout = new GridLayout(2, 1);
      layout.setMargin(new MarginInfo(true, false));
      layout.setSpacing(true);
      layout.addComponent(yearStartField);
      layout.addComponent(yearEndField);
      return layout;
   }

   private Component createDateTab()
   {
      GridLayout layout = new GridLayout(2, 1);
      layout.setMargin(new MarginInfo(true, false));
      layout.setSpacing(true);
      layout.addComponent(dateField);
      addConditionaly(layout, timeField, OccurrenceField.TIME_OF_DAY);
      return layout;
   }


   @Override public void setValue(Object vagueDate)
   {
      
      this.dateValue = (VagueDate) vagueDate;
      refresh();
      for (ValueChangeListener listener : valueChangeListeners)
      {
         listener.valueChange(new ValueChangeEvent(this, VagueDate.class, true));
      }
   }

   private void refresh()
   {
      if(timeValue != null)
      {
         timeField.setValue(timeValue);
      }
      else
      {
         timeField.setValue(null);
      }
      
      if(dateValue != null)
      {
         if(dateValue.getType().equals(VagueDate.VagueDateType.DAY))
         {
            dateField.setValue(dateValue.getFrom());
            tabSheet.setSelectedTab(0);
         }
         else if(dateValue.getType().equals(VagueDate.VagueDateType.DAYS))
         {
            dateStartField.setValue(dateValue.getFrom());
            dateEndField.setValue(dateValue.getTo());
            tabSheet.setSelectedTab(1);
         }
         else if(dateValue.getType().equals(VagueDate.VagueDateType.YEARS))
         {
            yearStartField.setValue(dateValue.getFrom());
            yearEndField.setValue(dateValue.getTo());
            tabSheet.setSelectedTab(2);
         }
         else
         {
            dateStartField.setValue(dateValue.getFrom());
            dateEndField.setValue(dateValue.getTo());
            tabSheet.setSelectedTab(1);
         }
      }
      else
      {
         dateStartField.setValue(null);
         dateEndField.setValue(null);
         yearStartField.setValue(null);
         yearEndField.setValue(null);
         dateField.setValue(null);
         monthStartField.setValue(null);
         monthEndField.setValue(null);
      }
      setCaption(VagueDate.format(dateValue, true));
   }

   @Override public VagueDate getValue()
   {
      return dateValue;
   }

   @Override public void setRequiredIndicatorVisible(boolean b)
   {
      // nothing
   }

   @Override public boolean isRequiredIndicatorVisible()
   {
      return false;
   }

   @Override public void setReadOnly(boolean b)
   {
      // nothing
   }

   @Override public boolean isReadOnly()
   {
      return false;
   }

   @Override public Registration addValueChangeListener(
         ValueChangeListener valueChangeListener)
   {
      valueChangeListeners.add(valueChangeListener);
      return () -> valueChangeListeners.remove(valueChangeListener);
   }

   @Override public void buttonClick(ClickEvent clickEvent)
   {
      UI.getCurrent().addWindow(window);
   }

   private String translate(String field)
   {
      return messages.getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }
   
   public String getTimeValue()
   {
      return timeValue;
   }

   public void setTimeValue(String timeValue)
   {
      this.timeValue = timeValue;
      refresh();
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractSingleSelect<?> component, OccurrenceField field)
   {
      if (this.withTime && occurrenceFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component
               .setRequiredIndicatorVisible(occurrenceFieldConfigs.get(field));
      }
   }
}
