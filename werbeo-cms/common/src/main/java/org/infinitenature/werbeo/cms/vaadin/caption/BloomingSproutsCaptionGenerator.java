package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.*;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class BloomingSproutsCaptionGenerator extends I18NCaptionGenerator<BloomingSprouts>
{

   public BloomingSproutsCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
