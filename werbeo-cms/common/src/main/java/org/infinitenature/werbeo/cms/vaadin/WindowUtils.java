package org.infinitenature.werbeo.cms.vaadin;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.Registration;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import org.infinitenature.vct.VCMSComponent;

import java.util.Collections;

public class WindowUtils
{
   private WindowUtils()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static void showPluginInWindow(VCMSComponent component,
         String parameters, boolean modal)
   {
      component.init(Collections.emptyMap());
      component.enter(new ViewChangeEvent(UI.getCurrent().getNavigator(), null,
            null, null, parameters));
      Window window = new Window("", component.getVaadinComponent());
      window.setHeight("90%");
      window.setWidth("90%");
      window.center();
      window.setModal(modal);
      UI.getCurrent().addWindow(window);
      if(!modal)
      {
         Registration  registration = UI.getCurrent().addClickListener(event -> window.close());
         window.addCloseListener(e -> registration.remove());
      }
   }
}
