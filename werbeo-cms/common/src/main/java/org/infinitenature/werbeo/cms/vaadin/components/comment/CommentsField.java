package org.infinitenature.werbeo.cms.vaadin.components.comment;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.infinitenature.service.v1.types.AbstractComment;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.service.v1.types.support.BaseUUID;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.FormListener;
import org.infinitenature.werbeo.cms.vaadin.components.HorizontalRule;
import org.infinitenature.werbeo.cms.vaadin.components.comment.CommentPanel.ReplyCommentListener;
import org.infinitenature.werbeo.cms.vaadin.components.form.FormUtils;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.components.DisclosurePanel;

import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

public class CommentsField extends I18NField<CommentedSample>

{
   @FunctionalInterface
   public interface NewCommentListener
   {
      public void onNewComment(String comment, Base<?> reply);
   }

   private final ListenerSupport<ReplyCommentListener> replyListeners = new ListenerSupport<>();
   private final ListenerSupport<NewCommentListener> newCommentListeners = new ListenerSupport<>();
   private final ResponsiveLayout layout = FormUtils.createFromLayout();
   private final ComponentContainer commentsLayout = FormUtils
         .createWideRow(layout);
   private CommentedSample comments = new CommentedSample();
   private DisclosurePanel samplePanel = new DisclosurePanel();

   public CommentsField(MessagesResource messages)
   {
      super(messages);
   }

   public CommentsField(MessagesResource messages, ReplyCommentListener replyListener,
         NewCommentListener newListener)
   {
      this(messages);
      replyListeners.addListener(replyListener);
      newCommentListeners.addListener(newListener);
   }

   @Override
   public void applyLocalizations()
   {
      samplePanel.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "general.sample", getLocale()));
   }

   @Override
   protected Component initContent()
   {
      return layout;
   }

   @Override
   protected void doSetValue(CommentedSample value)
   {
      this.comments = value;
      commentsLayout.removeAllComponents();

      Map<Occurrence, List<OccurrenceComment>> occurrenceComments = value
            .getOccurrenceComments();
      List<SampleComment> sampleComments = value.getSampleComments();

      fillCommentPanel(sampleComments, samplePanel, value.getSample());
      commentsLayout.addComponent(samplePanel);

      occurrenceComments.forEach((occurrence, commentsByOccurrence) ->
      {
         DisclosurePanel panel = new DisclosurePanel(
               occurrence.getTaxon().getName());
         fillCommentPanel(commentsByOccurrence, panel, occurrence);
         commentsLayout.addComponent(panel);

      });
   }

   private void fillCommentPanel(
         List<? extends AbstractComment> comments,
         DisclosurePanel panel, BaseUUID entity)
   {
      panel.getContentWrapper().removeAllComponents();
      panel.getContentWrapper().add(new MButton("Neuer Kommentar").withIcon(
            VaadinIcons.PLUS_CIRCLE)
                  .addClickListener(() -> onNewClick(entity)));
      panel.getContentWrapper().add(new HorizontalRule());
      for (Iterator<? extends AbstractComment> iterator = comments
            .iterator(); iterator.hasNext();)
      {
         panel.getContentWrapper()
               .addComponent(new CommentPanel(getMessages(), iterator.next(),
                     (r, c) -> replyListeners
                           .fire(listener -> listener.onReplyComment(r, c))));
         if (iterator.hasNext())
         {
            panel.getContentWrapper().addComponent(new HorizontalRule());
         }
      }
   }

   private void onNewClick(BaseUUID entity)
   {
      CommentField commentField = new CommentField(getMessages());
      commentField.setSizeFull();

      Window window = new Window("Kommentar", commentField);
      window.setModal(true);
      window.center();
      window.setHeight("90%");
      window.setWidth("90%");
      UI.getCurrent().addWindow(window);
      commentField.addListner(new FormListener<CommentField>()
      {
         @Override
         public void onSave(CommentField source)
         {
            newCommentListeners
                  .fire(listener -> listener.onNewComment(source.getValue(), entity));
            window.close();
         }

         @Override
         public void onCancel(CommentField source)
         {
            window.close();
         }
      });
   }

   public Registration addListner(NewCommentListener listener)
   {
      return newCommentListeners.addListener(listener);
   }

   public boolean removeListener(NewCommentListener listener)
   {
      return newCommentListeners.removeListener(listener);
   }

   public Registration addListner(ReplyCommentListener listener)
   {
      return replyListeners.addListener(listener);
   }

   public boolean removeListener(ReplyCommentListener listener)
   {
      return replyListeners.removeListener(listener);
   }

   @Override
   public CommentedSample getValue()
   {
      return comments;
   }
}
