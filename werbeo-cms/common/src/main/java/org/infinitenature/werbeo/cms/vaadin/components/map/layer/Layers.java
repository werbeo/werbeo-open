package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.infinitenature.service.v1.types.PortalConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Layers
{
   private static final Logger LOGGER = LoggerFactory.getLogger(Layers.class);

   private List<Layer> overlays;
   private List<Layer> baselayers;

   public Layers(String geoServerURL, PortalConfiguration portalConfiguration)
   {
      overlays = new ArrayList<>();
      baselayers = new ArrayList<>();

      baselayers.add(new LOpenTopoMapLayer());
      baselayers.add(new LOpenStreetMapDELayer());
      baselayers.add(new LSatelliteLayer());

      for(org.infinitenature.service.v1.types.enums.LayerName name : portalConfiguration.getMapOverlayLayers())
      {
         switch (LayerName.valueOf(name.name()))
         {
         case MTB:
            overlays.add(new LMTBLayer(geoServerURL));
            break;
         case TOPO_MV:
            overlays.add(new LMVTopoLayer());
            break;
         case ORTHOFOTO_MV:
            overlays.add(new LMVDOPLayer());
            break;
            default:
               LOGGER.error("map overlay-layer ? does not exist", name);
         }
      }
   }

   public List<Layer> getOverlays()
   {
      return overlays;
   }

   public List<Layer> getBaselayers()
   {
      return baselayers;
   }

   public void setOverlays(List<Layer> overlays)
   {
      this.overlays = overlays;
   }

   public void setBaselayers(List<Layer> baselayers)
   {
      this.baselayers = baselayers;
   }
}
