package org.infinitenature.werbeo.cms.vaadin.components.numericamount;

import org.infinitenature.service.v1.types.Occurrence.NumericAmountAccuracy;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.PositiveIntegerField;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;

public class NumericAmountWithAccuracyField
      extends I18NField<NumericAmountWithAccuracy>
{
   private static final String WHITE_BUTTON = "white-button";
   private PositiveIntegerField nummericAmount = new PositiveIntegerField()
         .withValueChangeListener(this::onNumericAmountChange);
   private NotCountedButton notCounted;
   private AboutButton about;
   private CountedButton counted;
   private MoreThanButton moreThan;
   private NumericAmountAccuracy accuracy = null;

   public NumericAmountWithAccuracyField(MessagesResource messages)
   {
      super(messages);
      notCounted = new NotCountedButton(messages, this::onNotCountedClick);
      about = new AboutButton(messages, this::onApproximateClick);
      counted = new CountedButton(messages, this::onCountedClick);
      moreThan = new MoreThanButton(messages, this::onMoreThanClick);
   }

   protected void onNumericAmountChange(ValueChangeEvent<Integer> e)
   {
      NumericAmountWithAccuracy oldValue = getValue();
      if (accuracy == null)
      {
         setAccuracy(NumericAmountAccuracy.COUNTED);
      } else
      {
         fireEvent(createValueChange(oldValue, true));
      }
   }

   private void onNotCountedClick(Button.ClickEvent click)
   {
      setValue(null, false);
   }

   protected void onApproximateClick(Button.ClickEvent clickEvent)
   {
      setAccuracy(NumericAmountAccuracy.APPROXIMATE);
   }

   private void onCountedClick(Button.ClickEvent clickEvent)
   {
      setAccuracy(NumericAmountAccuracy.COUNTED);
   }

   private void onMoreThanClick(Button.ClickEvent clickEvent)
   {
      setAccuracy(NumericAmountAccuracy.MORETHAN);
   }

   @Override
   public void applyLocalizations()
   {
      // NOOP
   }

   @Override
   public NumericAmountWithAccuracy getValue()
   {
      Integer value = nummericAmount.getValue();
      if (value == null)
      {
         return null;
      }
      return new NumericAmountWithAccuracy(value, getAccuracy());
   }

   private NumericAmountAccuracy getAccuracy()
   {
      return accuracy;
   }

   private void setAccuracy(NumericAmountAccuracy accuracy)
   {
      NumericAmountWithAccuracy oldValue = getValue();
      this.accuracy = accuracy;
      about.removeStyleName(WHITE_BUTTON);
      counted.removeStyleName(WHITE_BUTTON);
      moreThan.removeStyleName(WHITE_BUTTON);
      if (accuracy != null)
      {
         switch (accuracy)
         {
         case APPROXIMATE:
            about.addStyleName(WHITE_BUTTON);
            break;
         case COUNTED:
            counted.addStyleName(WHITE_BUTTON);
            break;
         case MORETHAN:
            moreThan.addStyleName(WHITE_BUTTON);
            break;
         default:
            break;
         }
      }
      fireEvent(createValueChange(oldValue, true));
   }

   @Override
   protected Component initContent()
   {

      return new MVerticalLayout(
            new MHorizontalLayout(notCounted, nummericAmount),
            new MHorizontalLayout(counted, about, moreThan)).withMargin(false);
   }

   @Override
   protected void doSetValue(NumericAmountWithAccuracy value)
   {
      if (value != null)
      {
         nummericAmount.setValue(value.getAmount());
         setAccuracy(value.getAccuracy());
      } else
      {
         nummericAmount.setValue(null);
         setAccuracy(null);
      }
   }

}
