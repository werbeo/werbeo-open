package org.infinitenature.werbeo.cms.vaadin.components.indexherbariorum;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import org.infinitenature.herbariorum.client.HerbariorumClient;

import java.util.stream.Stream;

public class CountryComboBoxDataProvider
      extends AbstractBackEndDataProvider<String, String>
{
   private final HerbariorumClient client;

   CountryComboBoxDataProvider(HerbariorumClient client,
         String presetCountry)
   {
      this.client = client;
   }

   @Override
   protected Stream<String> fetchFromBackEnd(Query<String, String> query)
   {
      String nameContains = query.getFilter().orElse("");

      return client.findCountriesByName(nameContains).stream()
            .skip(query.getOffset()).limit(query.getLimit());
   }

   @Override
   protected int sizeInBackEnd(Query<String, String> query)
   {
      return client.countCountriesByName(query.getFilter().orElse(""));
   }

}
