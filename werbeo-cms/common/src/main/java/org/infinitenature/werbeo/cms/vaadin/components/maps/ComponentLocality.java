package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.service.v1.types.*;

import com.vaadin.ui.Component;

public class ComponentLocality
{
   private Locality locality;
   private Component component;
   
   
   public ComponentLocality(Locality locality, Component component)
   {
      this.locality = locality;
      this.component = component;
   }
   
   public Locality getLocality()
   {
      return locality;
   }
   public void setLocality(Locality locality)
   {
      this.locality = locality;
   }
   public Component getComponent()
   {
      return component;
   }
   public void setComponent(Component component)
   {
      this.component = component;
   }
   
   
}
