package org.infinitenature.werbeo.cms.vaadin.plugin.tables;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.LinkButton;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.teemusa.gridextensions.paging.ConfigurableFilterPagedDataProvider;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MPanel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.renderers.Renderer;

public abstract class AbstractTableViewImpl<T, COLUMNS_TYPE extends Enum, FILTER_TYPE>
      extends I18NComposite
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(AbstractTableViewImpl.class);
   private final Class<COLUMNS_TYPE> columnEnumClass;
   private final Class<T> typeClass;
   protected Grid<T> grid = new Grid<>();

   private PagingButtons pagingButtons;
   private MLabel countLabel = new MLabel();
   private LinkButton resetFilters = new LinkButton(VaadinIcons.RECYCLE,
         event -> restFilters());

   private TableObserver<FILTER_TYPE> observer;

   private ResponsiveLayout filterLayout = new ResponsiveLayout();
   private ResponsiveRow filterRow = filterLayout.addRow().withMargin(true);
   private ResponsiveRow filterLayoutSecondRow = filterLayout.addRow();
   private MHorizontalLayout exportLayout = new MHorizontalLayout()
         .withDefaultComponentAlignment(Alignment.MIDDLE_LEFT)
         .withMargin(new MarginInfo(false, true));

   protected MVerticalLayout layout;
   private Map<Column<?, ?>, COLUMNS_TYPE> captionKeys = new HashMap<>();
   private Map<Component, COLUMNS_TYPE> filterCaptionKeys = new HashMap<>();
   private Set<HasValue<?>> filterFields = new HashSet<>();

   private Panel filterPanel;
   private Panel exportPanel;

   private int resultSize = 0;

   public AbstractTableViewImpl(Class<COLUMNS_TYPE> columnEnumClass,
         Class<T> typeClass, TableObserver<FILTER_TYPE> observer,
         MessagesResource messagesResource)
   {
      super(messagesResource);
      this.columnEnumClass = columnEnumClass;
      this.typeClass = typeClass;
      this.observer = observer;
      this.pagingButtons = new PagingButtons(messagesResource);

      exportLayout.addComponent(countLabel);

      filterLayoutSecondRow.addComponent(resetFilters);

      filterPanel = new MPanel(filterLayout);
      exportPanel = new MPanel(new MVerticalLayout(exportLayout));

      layout = new MVerticalLayout(filterPanel, exportPanel, grid,
            pagingButtons);

      filterPanel.setVisible(false);

      grid.setSizeFull();
      grid.setColumnReorderingAllowed(true);
      layout.setMargin(new MarginInfo(false, true));
      setCompositionRoot(layout);
      setSizeFull();
      grid.setSelectionMode(Grid.SelectionMode.NONE);
      grid.addStyleName("occurrence-table");
      resetFilters.setVisible(false);
   }

   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id)
   {
      Column<T, Object> column = grid.addColumn(valueProvider);
      applyDefaults(column, id);
      return column;
   }

   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id, int width)
   {
      Column<T, Object> column = addColumn(valueProvider, id);
      column.setWidth(width);
      return column;
   }

   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id, Renderer renderer)
   {
      Column<T, Object> column = addColumn(valueProvider, id);
      column.setRenderer(renderer);
      return column;
   }

   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id, Renderer renderer, int pixelWidth)
   {
      Column<T, Object> column = addColumn(valueProvider, id);
      column.setRenderer(renderer);
      column.setWidth(pixelWidth);
      return column;
   }

   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id, Renderer renderer, HasValue<?> filterComponent)
   {
      Column<T, Object> column = addColumn(valueProvider, id, filterComponent);
      column.setRenderer(renderer);
      return column;
   }

   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id, Renderer renderer, HasValue<?> filterComponent,
         int pixelWidth)
   {
      Column<T, Object> column = addColumn(valueProvider, id, filterComponent);
      column.setRenderer(renderer);
      column.setWidth(pixelWidth);
      return column;
   }
   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id, HasValue<?> filterComponent)
   {
      Column<T, Object> column = addColumn(valueProvider, id);
      addFilterComponent(id, filterComponent);
      return column;
   }

   protected Column<T, Object> addColumn(ValueProvider<T, Object> valueProvider,
         COLUMNS_TYPE id, HasValue<?> filterComponent, int pixelWidth)
   {
      Column<T, Object> column = addColumn(valueProvider, id, filterComponent);
      column.setWidth(pixelWidth);
      return column;
   }

   protected Column<T, Component> addComponentColumn(
         ValueProvider<T, Component> valueProvider, COLUMNS_TYPE id)
   {
      Column<T, Component> column = grid.addComponentColumn(valueProvider);
      applyDefaults(column, id);
      return column;
   }

   protected Column<T, Component> addComponentColumn(
         ValueProvider<T, Component> valueProvider, COLUMNS_TYPE id,
         int pixelWidth)
   {
      Column<T, Component> column = addComponentColumn(valueProvider, id);
      column.setWidth(pixelWidth);
      return column;
   }

   protected Column<T, Component> addComponentColumn(
         ValueProvider<T, Component> valueProvider, COLUMNS_TYPE id,
         HasValue<?> filterComponent)
   {
      Column<T, Component> column = addComponentColumn(valueProvider, id);
      addFilterComponent(id, filterComponent);
      return column;
   }

   protected Column<T, Component> addComponentColumn(
         ValueProvider<T, Component> valueProvider, COLUMNS_TYPE id,
         HasValue<?> filterComponent, int pixelWidth)
   {
      Column<T, Component> column = addComponentColumn(valueProvider, id,
            pixelWidth);
      addFilterComponent(id, filterComponent);
      return column;
   }

   private void addFilterComponent(COLUMNS_TYPE id, HasValue<?> filterComponent)
   {
      Component filterAsComponent = addFilter(filterComponent);

      filterCaptionKeys.put(filterAsComponent, id);
   }

   protected Component addFilter(HasValue<?> filterComponent)
   {
      filterComponent.addValueChangeListener(
            event -> observer.onFilterChange(buildFilter()));
      filterFields.add(filterComponent);
      Component filterAsComponent = (Component) filterComponent;
      filterRow.addComponent(filterAsComponent);
      filterPanel.setVisible(true);
      resetFilters.setVisible(true);
      return filterAsComponent;
   }

   protected void addExportButton(Button button)
   {
      exportLayout.addComponent(button, exportLayout.getComponentCount() - 1);
   }

   protected void setFilterCaption(COLUMNS_TYPE id, Component filterAsComponent)
   {
      filterAsComponent.setCaption(getCaption(id));
   }

   protected void applyDefaults(Column<T, ?> column, COLUMNS_TYPE id)
   {
      column.setId(id.toString()).setHidable(true);
      captionKeys.put(column, id);
   }

   protected abstract FILTER_TYPE buildFilter();

   protected String getCaption(COLUMNS_TYPE column)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            typeClass.getSimpleName(), column.toString(),
            UI.getCurrent().getLocale());
   }

   protected abstract List<COLUMNS_TYPE> getSortableColumns();

   public void setDataProvider(
         ConfigurableFilterDataProvider<T, Void, FILTER_TYPE> filterDataProvider)
   {
      ConfigurableFilterPagedDataProvider<T, Void, FILTER_TYPE> pagedDataProvider = new ConfigurableFilterPagedDataProvider<>(
            filterDataProvider);
      pagingButtons.setDataProvider(pagedDataProvider);
      grid.setDataProvider(pagedDataProvider);
      List<COLUMNS_TYPE> sortableColumns = getSortableColumns();
      for (Column<T, ?> column : grid.getColumns())
      {

         Enum<?> columnId = Enum.valueOf(columnEnumClass, column.getId());
         if (sortableColumns.contains(columnId))
         {
            column.setSortable(true);
         } else
         {
            column.setSortable(false);
         }
      }

   }

   public void setSize(int size)
   {
      this.resultSize = size;
      if (isAttached())
      {
         updateCountLabel();
      }
   }

   private void updateCountLabel()
   {
      countLabel.setValue(getMessages().getMessage(
            Context.getCurrent().getPortal(), "ammount",
            UI.getCurrent().getLocale(),
            getDecimalFormat().format(resultSize)));
   }

   public void setObserver(TableObserver<FILTER_TYPE> observer)
   {
      this.observer = observer;
   }

   @Override
   public void applyLocalizations()
   {
      captionKeys.forEach((column, id) -> column.setCaption(getCaption(id)));
      resetFilters.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "filter.reset", getLocale()));
      filterCaptionKeys
            .forEach((component, id) -> setFilterCaption(id, component));
      updateCountLabel();
   }

   protected void restFilters()
   {
      filterFields.forEach(hasValue ->
      {
         try
         {
            hasValue.setValue(null);
         } catch (Exception e)
         {
            LOGGER.error("Failure resteting filter field for {}", hasValue, e);
         }
      });
   }
}
