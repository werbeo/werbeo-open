package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.ComboBox;
import org.infinitenature.service.v1.types.VagueDate;

public class DateTypeComboBox extends ComboBox<VagueDate.VagueDateType>
{
   public DateTypeComboBox()
   {
      setItems(VagueDate.VagueDateType.values());
      setValue(VagueDate.VagueDateType.DAY);
   }
}
