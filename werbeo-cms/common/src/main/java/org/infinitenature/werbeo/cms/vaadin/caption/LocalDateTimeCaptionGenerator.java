package org.infinitenature.werbeo.cms.vaadin.caption;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class LocalDateTimeCaptionGenerator
      implements WerbeoCaptionGenerator<LocalDateTime>
{

   @Override
   public String apply(LocalDateTime localDateTime)
   {
      if (localDateTime != null)
      {
         return localDateTime
               .format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM));
      } else
      {
         return "";
      }
   }

}
