package org.infinitenature.werbeo.cms.vaadin.components.map;

import org.infintenature.mtb.MTB;

public interface Values
{
   int PPRESENTATION_EPSG = 4326;
   int MTB_EPSG = MTB.getEpsgNumber();
   CoordinateSystem START_COORDINATE_SYSTEM = CoordinateSystem.EPSG4326;
}
