package org.infinitenature.werbeo.cms.navigation;

public interface NavigationTarget
{
   default boolean isInSubWindow()
   {
      return false;
   }
}
