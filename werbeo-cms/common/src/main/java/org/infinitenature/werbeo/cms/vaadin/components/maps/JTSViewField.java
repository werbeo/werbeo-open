package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.werbeo.cms.vaadin.components.map.*;
import org.vaadin.addon.leaflet.*;
import org.vaadin.addon.leaflet.util.*;

import com.vaadin.shared.Registration;
import com.vaadin.ui.*;
import org.locationtech.jts.geom.*;

public class JTSViewField extends AbstractJTSField<Geometry>
{
   public JTSViewField()
   {
      super();
   }
   
   @Override
   protected void prepareDrawing()
   {
      
   }

   @Override
   protected void prepareViewing()
   {
      
   }
   
   public void addValueComponent(Geometry value, Component component)
   {
      if(value != null)
      {
         setCRSTranslator(
            new MapCRSTranslator(Values.PPRESENTATION_EPSG, value.getSRID()));
         value = getCrsTranslator().toPresentation(value);
      }
      if(value instanceof Point)
      {
         
         LMarker marker = new LMarker(JTSUtil.toLeafletPoint((Point)value));
         if(component != null)
         {
            marker.addClickListener(event -> {
               Window window = new Window();
               window.setContent(component);
               window.setClosable(true);
               UI.getCurrent().addWindow(window);
               window.setPosition((int)event.getClientX(), (int)event.getClientY());
            });
         }
         map.addLayer(marker);
      }
      else if(value instanceof Polygon)
      {
         LPolygon layer = JTSUtil.toPolygon((Polygon)value);
         if(component != null)
         {
         layer.addClickListener(event -> {
               Window window = new Window();
               window.setContent(component);
               window.setClosable(true);
               UI.getCurrent().addWindow(window);
               window.setPosition((int)event.getClientX(), (int)event.getClientY());
            });
         }
         map.addLayer(layer);
      }
      else if(value instanceof MultiPolygon)
      {
         MultiPolygon multiPolygon = (MultiPolygon)value;
         for(int i = 0; i < multiPolygon.getNumGeometries(); i++)
         {
            map.addLayer(JTSUtil.toPolygon((Polygon)multiPolygon.getGeometryN(i)));
         }
      }
      else
      {
         
      }
   }

   @Override
   protected void prepareEditing(boolean userOriginatedValueChange)
   {
      // TODO Auto-generated method stub
      
   }
}
