package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence.Reproduction;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.ReproductionCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class ReproductionComboBox extends EnumComboBox<Reproduction>
{

   public ReproductionComboBox(MessagesResource messages)

   {
      super(messages, Reproduction.class,
            new ReproductionCaptionGenerator(messages));
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "REPRODUCTION", getLocale()));
   }
}
