package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LWmsLayer;

public class LMVDOPLayer extends LWmsLayer implements Layer
{
   private LayerName name;
   private boolean active;

   LMVDOPLayer()
   {
      setUrl("http://www.geodaten-mv.de/dienste/adv_dop");
      setDescription("Luftbild");
      setLayers("MV_DOP");
      setFormat("image/png");
      setTransparent(true); // does not cover whole world
      setMinZoom(11);
      setActive(false);
      setEnabled(true);
      name = LayerName.ORTHOFOTO_MV;
      setCaption(LayerName.ORTHOFOTO_MV.getCaption());
   }

   @Override
   public boolean isActive()
   {
      return active;
   }

   @Override
   public void setActive(boolean active)
   {
      setActiveIndicator(active);
      super.setActive(active);
   }

   @Override
   public void setActiveIndicator(boolean active)
   {
      this.active = active;
   }

   @Override
   public LayerName getName()
   {
      return name;
   }

   @Override
   public Layer clone()
   {
      return new LMVDOPLayer();
   }
}
