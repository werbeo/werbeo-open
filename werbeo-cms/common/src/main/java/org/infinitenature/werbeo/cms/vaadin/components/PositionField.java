package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Position;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;

import com.vaadin.shared.Registration;
import com.vaadin.ui.Component;

/**
 * Wrapper around {@link GeoButton} to aling proper in forms with caption
 * 
 * @author dve
 *
 */
public class PositionField extends I18NField<Position>
{
   private final GeoButton geoButton;

   public PositionField(MessagesResource messages, GeoButton geoButton)
   {
      super(messages);
      this.setCaption("");
      this.geoButton = geoButton;
   }

   @Override
   public void applyLocalizations()
   {
   }

   @Override
   public Position getValue()
   {
      return geoButton.getValue();
   }

   @Override
   protected Component initContent()
   {
      return geoButton;
   }

   @Override
   protected void doSetValue(Position value)
   {
      geoButton.setValue(value);
   }

   @Override
   public Registration addValueChangeListener(
         ValueChangeListener<Position> listener)
   {
      return geoButton.addValueChangeListener(listener);
   }

}
