package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18N;
import org.vaadin.viritin.label.Header;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;
import org.vaadin.viritin.layouts.MWindow;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;

public abstract class PopUpDialog<T extends PopUpDialog<T>> extends MWindow
      implements I18N
{
   private final MessagesResource messages;
   private final MHorizontalLayout buttonLayout = new MHorizontalLayout();
   private final Header header = new Heading();
   private final MVerticalLayout mainLayout = new MVerticalLayout(header,
         buttonLayout);
   private Component content = null;

   public PopUpDialog(MessagesResource messages)
   {
      super();
      this.messages = messages;
      setContent(mainLayout);
      addAttachListener(event -> applyLocalizations());
   }

   @Override
   public T withContent(Component content)
   {
      if (this.content != null)
      {
         mainLayout.replaceComponent(this.content, content);
      } else
      {
         mainLayout.addComponent(content, 1);
      }
      this.content = content;
      return (T) this;
   }

   public T withCaption(String caption)
   {
      setCaption(caption);
      return (T) this;
   }

   @Override
   public void setCaption(String caption)
   {
      super.setCaption(caption);
      // header will be null during parent class constructor calls
      if (this.header != null)
      {
         this.header.setText(caption);
      }
   }
   @Override
   public MessagesResource getMessages()
   {
      return messages;
   }

   public T withCloseButton(Button closeButton)
   {
      buttonLayout.addComponent(closeButton, buttonLayout.getComponentCount());
      closeButton.addClickListener(event -> close());
      return (T) this;
   }

   public T withButton(Button button)
   {
      buttonLayout.addComponentAsFirst(button);
      return (T) this;
   }

}
