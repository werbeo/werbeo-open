package org.infinitenature.werbeo.cms.vaadin.components.numericamount;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

public class MoreThanButton extends I18NButton
{

   public MoreThanButton(MessagesResource messages, ClickListener listener)
   {
      super(messages, listener);
   }

   @Override
   protected void init()
   {
      // NOOP
   }

   @Override
   protected String getKey()
   {
      return "NumericAmountAccuracy.MORETHAN";
   }

   @Override
   public void applyLocalizations()
   {
      setCaption(getEntityField("NumericAmountAccuracy", "MORETHAN"));
   }
}
