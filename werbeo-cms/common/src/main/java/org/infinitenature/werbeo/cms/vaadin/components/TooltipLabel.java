package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.Label;

public class TooltipLabel extends Label
{
   public TooltipLabel(String text, String tooltipText)
   {
      super(text);
      setDescription(tooltipText);
   }
}
