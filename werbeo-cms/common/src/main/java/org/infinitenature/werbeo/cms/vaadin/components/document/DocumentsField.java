package org.infinitenature.werbeo.cms.vaadin.components.document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.VaadinUtils;
import org.infinitenature.werbeo.cms.vaadin.components.HasAllowedOperations;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.jsoup.helper.Validate;
import org.vaadin.viritin.layouts.MPanel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class DocumentsField extends I18NField<List<Document>>
      implements HasAllowedOperations
{
   private VerticalLayout layout = new MVerticalLayout();
   private List<Document> documents = new ArrayList<>();
   private List<Document> documentsToDelete = new ArrayList<>();
   private Collection<Operation> allowedOperations = new HashSet<>();

   public DocumentsField(MessagesResource messages)
   {
      super(messages);
      doSetValue(documents);
   }

   @Override
   public void applyLocalizations()
   {
      // NOOP
   }

   @Override
   public List<Document> getValue()
   {
      return null;
   }

   @Override
   public boolean isEmpty()
   {
      return documents.isEmpty();
   }

   @Override
   protected Component initContent()
   {
      return new MPanel(layout);
   }

   @Override
   protected void doSetValue(List<Document> value)
   {
      Validate.notNull(value);
      this.documents = value;
      layout.removeAllComponents();

      documents.stream()
            .filter(doc -> doc.getType() != DocumentType.IMAGE_THUMB)
            .forEach(doc ->
            {
               DocumentField documentField = new DocumentField(getMessages());
               documentField.setValue(doc);
               documentField.setDeleteDelegate(this);
                     layout.addComponent(documentField);
                  });

      if (this.isAttached())
      {
         applyLocalizations();
      }
      applyAllowedOperations(this.allowedOperations);
   }

   public void suppressCaptions()
   {
      for (int i = 0; i < layout.getComponentCount(); i++)
      {
         ((DocumentField) layout.getComponent(i)).setWithCaption(false);
      }
      setCaption("");
   }

   @Override
   public void setAllowedOperations(Collection<Operation> allowedOperations)
   {
      this.allowedOperations = allowedOperations;
      applyAllowedOperations(this.allowedOperations);
   }

   private void applyAllowedOperations(Collection<Operation> allowedOperations)
   {

      VaadinUtils.componentStream(layout, DocumentField.class)
            .forEach(df -> df.setAllowedOperations(allowedOperations));
   }

   public void onDelete(Document document)
   {
      documents.remove(document);
      documentsToDelete.add(document);
      doSetValue(documents);
   }

   public Collection<Document> getDocumentsToDelete()
   {
      return documentsToDelete;
   }

}
