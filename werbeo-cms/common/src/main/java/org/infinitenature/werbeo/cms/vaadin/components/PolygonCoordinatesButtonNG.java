package org.infinitenature.werbeo.cms.vaadin.components;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.map.CoordinateSystem;
import org.infinitenature.werbeo.cms.vaadin.components.map.MapCRSTranslator;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateFormatter;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.WKTWriter;

import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.TextRenderer;

public class PolygonCoordinatesButtonNG extends I18NButton
      implements HasValue<Polygon>, Button.ClickListener
{
   private Window window;
   private Polygon value;
   private Grid<ListEntry> list;
   private List<ListEntry> data;
   private CoordinateSystemButton coordinateSystemButton;
   private List<ValueChangeListener> valueChangeListeners = new ArrayList<>();
   private Button selectButton;
   private Button addButton;
   private Button deleteButton;
   private Registration registration;
   private boolean readOnly;
   private TextField editor;
   private Button copyButton;
   private CoordinateSystem coordinateSystem;

   public PolygonCoordinatesButtonNG(MessagesResource messages,
         CoordinateSystem startCoordinateSystem, PortalConfiguration portalConfiguration)
   {
      super(messages);
      this.coordinateSystem = startCoordinateSystem;
      readOnly = false;
      data = new LinkedList<>();
      initGui(startCoordinateSystem, portalConfiguration);
      addClickListener(this);
   }

   private void initGui(CoordinateSystem startCoordinateSystem, PortalConfiguration portalConfiguration)
   {
      window = new Window();
      window.setWidth("600px");
      window.setHeight("400px");
      window.center();

      window.setModal(true);

      VerticalLayout verticalLayout = new VerticalLayout();
      verticalLayout.setMargin(true);
      verticalLayout.setSpacing(true);
      coordinateSystemButton = new CoordinateSystemButton(startCoordinateSystem,
            getMessages(), portalConfiguration);
      list = new Grid<>(ListEntry.class);
      list.setSelectionMode(Grid.SelectionMode.MULTI);
      editor = new TextField();
      editor.setValueChangeMode(ValueChangeMode.BLUR);
      list.getColumn("text").setCaption(setListHeader())
            .setRenderer(new TextRenderer()).setEditorComponent(editor)
            .setEditable(true).setSortable(false);
      list.getEditor().setEnabled(true);

      list.setWidth("560px");
      list.setBodyRowHeight(40);
      setEmptyRows();
      list.setHeight("200px");
      list.setItems(data);

      HorizontalLayout horizontalLayout = new HorizontalLayout();
      addButton = new Button(VaadinIcons.PLUS);
      deleteButton = new Button(VaadinIcons.MINUS);
      copyButton = new Button(VaadinIcons.DOWNLOAD);
      horizontalLayout.addComponents(addButton, deleteButton, copyButton);

      selectButton = new Button();
      verticalLayout
            .addComponents(coordinateSystemButton, list, horizontalLayout,
                  selectButton);

      window.setContent(verticalLayout);
   }

   private void setEmptyRows()
   {
      ListEntry entry1 = new ListEntry();
      ListEntry entry2 = new ListEntry();
      ListEntry entry3 = new ListEntry();
      data.add(entry1);
      data.add(entry2);
      data.add(entry3);
   }

   private void initController()
   {
      registration = coordinateSystemButton.addValueChangeListener(
            this::valueChangeInCoordinateSystemButton);

      selectButton.addClickListener(event -> {
         if (data.size() < 3)
         {
            Notification.show(translate("ERROR"), translate("THREE_CORNERS"),
                  Notification.Type.ERROR_MESSAGE);
            return;
         }

         try
         {
            this.value = readPolygon();
            if(value == null)
            {
               return;
            }
            for (ValueChangeListener listener : valueChangeListeners)
            {
               listener.valueChange(
                     new ValueChangeEvent(this, Polygon.class, true));
               UI.getCurrent().removeWindow(window);
            }
         } catch (Exception e)
         {
            Notification.show(translate("ERROR"),
                  translate("COORD_ERROR"),
                  Notification.Type.ERROR_MESSAGE);
         }
      });

      addButton.addClickListener(event -> {
         Set<ListEntry> items = list.getSelectedItems();

         if (items.size() > 1)
         {
            Notification.show(translate("NOTE"),
                  translate("ONE_MAX"),
                  Notification.Type.WARNING_MESSAGE);
            return;
         }
         ListEntry entry = new ListEntry();
         if (items.isEmpty())
         {
            data.add(entry);
         } else
         {
            ListEntry[] itemsArray = items.toArray(new ListEntry[1]);
            ListEntry item = itemsArray[0];
            data.add(data.indexOf(item), entry);
         }

         list.getDataProvider().refreshAll();
      });

      deleteButton.addClickListener(event -> {
         Set<ListEntry> items = list.getSelectedItems();
         if (items.isEmpty())
         {
            Notification.show(translate("NOTE"),
                  translate("SELECTION_MISSING"),
                  Notification.Type.WARNING_MESSAGE);
            return;
         }
         if (data.size() - items.size() >= 3)
         {
            for (ListEntry entry : items)
            {
               data.remove(entry);
            }
            list.deselectAll(); // important! otherwise deleted items remain selected
            list.getDataProvider().refreshAll();
         } else
         {
            Notification.show(translate("NOTE"), translate("THREE_CORNERS"),
                  Notification.Type.WARNING_MESSAGE);
         }
      });

      list.addItemClickListener(event -> {
         if(list.getEditor().isEnabled())
         {
            list.getEditor().editRow(event.getRowIndex());
            editor.focus();
         }
      });

      OnDemandFileDownloader.OnDemandStreamResource resource = new OnDemandFileDownloader.OnDemandStreamResource()
      {
         @Override
         public InputStream getStream()
         {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(coordinateSystem.getName()).append("\n");
            for (ListEntry entry : data)
            {
               if(entry.getText() != null && !entry.getText().isEmpty())
               {
                  stringBuilder.append(entry.getText().replaceAll("\\.", ",")
                        .replaceAll(" ", "\t")).append("\n");
               }
            }
            return new ByteArrayInputStream(
                  stringBuilder.toString().getBytes());
         }

         @Override
         public String getFilename()
         {
            return "Koordinaten.dat";
         }
      };

      OnDemandFileDownloader downloader = new OnDemandFileDownloader(resource);
      downloader.extend(copyButton);
   }

   private Polygon readPolygon() throws IllegalStateException
   {
      if (data.size() < 3)
      {
         throw new IllegalStateException(
               "A polygon must have at least three corners.");
      }

      Coordinate[] coordinates = new Coordinate[data.size() + 1];
      for (int i = 0; i < data.size(); i++)
      {
         Coordinate coordinate = readCoordinate(i);
         if (coordinate == null)
         {
            throw new IllegalStateException("A coordinate was empty.");
         }
         coordinates[i] = coordinate;
      }

      // finalize polygon with the starting point
      Coordinate coordinate = readCoordinate(0);
      coordinates[data.size()] = coordinate;

      GeometryHelper geometryHelper = new GeometryHelper();

      Polygon polygon = geometryHelper.getPolygon(coordinates,
            coordinateSystemButton.getValue().getEpsg());

      try
      {
         geometryHelper.parse(new WKTWriter().write(polygon),
               coordinateSystemButton.getValue().getEpsg());
      }
      catch(Exception e)
      {
         Notification.show(translate("ERROR"), translate("INVALID_POLYGON"),
               Notification.Type.ERROR_MESSAGE);
         return null;
      }

      return polygon;
   }

   private void transformCoordinates(int oldEpsg, int newEpsg)
   {
      try
      {
         List<ListEntry> transformedData = new ArrayList<>();
         // oldEpsg = presentation
         // newEpsg = model
         MapCRSTranslator translator = new MapCRSTranslator(oldEpsg, newEpsg);
         GeometryHelper helper = new GeometryHelper();
         for (int i = 0; i < data.size(); i++)
         {
            Coordinate coordinate = readCoordinate(i);
            if (coordinate == null)
            {
               transformedData.add(new ListEntry(""));
               continue;
            }
            Point corner = helper.getPoint(coordinate.y, coordinate.x, oldEpsg);
            Point transformedCorner = (Point) translator.toModel(corner);
            Coordinate transformedCoordinate = transformedCorner
                  .getCoordinate();
            transformedData.add(new ListEntry(
                  CoordinateFormatter.formatWithSpace(newEpsg, transformedCoordinate.x, transformedCoordinate.y)));
         }
         if (!transformedData.isEmpty())
         {
            data = transformedData;
            list.setItems(data);
            list.getDataProvider().refreshAll();
         }
      } catch (Exception e)
      {
         registration.remove();
         coordinateSystemButton.setValue(CoordinateSystem.get(oldEpsg));
         registration = coordinateSystemButton.addValueChangeListener(
               this::valueChangeInCoordinateSystemButton);
         Notification.show(translate("ERROR"), translate("NO_TRANFORM"),
               Notification.Type.ERROR_MESSAGE);
      }
   }

   private void valueChangeInCoordinateSystemButton(
         ValueChangeEvent<CoordinateSystem> event)
   {
      CoordinateSystem newCoordinateSytem = event.getValue();
      transformCoordinates(coordinateSystem.getEpsg(),
            newCoordinateSytem.getEpsg());
      coordinateSystem = newCoordinateSytem;
      list.getColumn("text").setCaption(setListHeader());
   }

   private String setListHeader()
   {
      return translate("LAT_LONG") + " " + coordinateSystemButton.getValue()
            .getName();
   }

   private Coordinate readCoordinate(int i)
   {
      String coordinateString = data.get(i).getText();
      if(coordinateString == null)
      {
         return null;
      }

      coordinateString = coordinateString.trim()
            .replaceAll(" +", " ");
      if (coordinateString.isEmpty())
      {
         return null;
      }
      String northingString = coordinateString.split(" ")[0];
      String eastingString = coordinateString.split(" ")[1];

      double northing = Double.valueOf(northingString.replaceAll(",", "."));
      double easting = Double.valueOf(eastingString.replaceAll(",", "."));

      Coordinate coordinate = new Coordinate();
      coordinate.y = northing;
      coordinate.x = easting;
      return coordinate;
   }

   @Override
   public void setValue(Polygon modelPolygon)
   {
      this.value = modelPolygon;
      data = new ArrayList<>();
      if (modelPolygon != null)
      {
         // Presentation epsg = coordinate system choosen
         // Model epsg = polygon epsg
         MapCRSTranslator translator = new MapCRSTranslator(
               coordinateSystemButton.getValue().getEpsg(),
               modelPolygon.getSRID());
         Polygon presentationPolygon = (Polygon) translator
               .toPresentation(modelPolygon);
         Coordinate[] coordinates = presentationPolygon.getCoordinates();
         for (Coordinate coordinate : coordinates)
         {
            double easting = coordinate.x;
            double northing = coordinate.y;
            ListEntry entry = new ListEntry(
                  CoordinateFormatter.formatWithSpace(coordinateSystemButton.getValue().getEpsg(), easting, northing));
            data.add(entry);
         }
         data.remove(
               data.size() - 1); // the first=last corner is recorded twice
      } else
      {
         setEmptyRows();
      }
      list.setItems(data);
      list.getDataProvider().refreshAll();
   }

   @Override
   public Polygon getValue()
   {
      return value;
   }

   @Override
   public void setRequiredIndicatorVisible(boolean b)
   {

   }

   @Override
   public boolean isRequiredIndicatorVisible()
   {
      return false;
   }

   @Override
   public void setReadOnly(boolean b)
   {
      readOnly = b;
      coordinateSystemButton.setReadOnly(b);
      selectButton.setEnabled(!b);
      addButton.setEnabled(!b);
      deleteButton.setEnabled(!b);
      list.getEditor().setEnabled(!b);
   }

   @Override
   public boolean isReadOnly()
   {
      return readOnly;
   }

   @Override
   public Registration addValueChangeListener(
         ValueChangeListener<Polygon> valueChangeListener)
   {
      valueChangeListeners.add(valueChangeListener);
      return (Registration) () -> valueChangeListeners.remove(valueChangeListener);
   }

   @Override
   public void buttonClick(ClickEvent clickEvent)
   {
      UI.getCurrent().addWindow(window);
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   public void setCoordinateSystems(List<CoordinateSystem> items)
   {
      coordinateSystemButton.setItems(items);
   }

   @Override
   protected void init()
   {

   }

   @Override
   public void applyLocalizations()
   {
      this.setCaption(translate("DATA_BY_HAND"));
      initController();
      coordinateSystemButton.applyLocalizations();
      window.setCaption(translate("DATA_BY_HAND"));
      list.getEditor().setSaveCaption(translate("SAVE"));
      list.getEditor().setCancelCaption(translate("CANCEL"));
      selectButton.setCaption(translate("SHOW"));
   }

   @Override
   protected String getKey()
   {
      return "";
   }

   public CoordinateSystem getCoordinateSystem()
   {
      return coordinateSystem;
   }
}
