package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.NumericAmountAccuracy;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class NumericAmountAccuracyCaptionGenerator
      extends I18NCaptionGenerator<NumericAmountAccuracy>
{

   public NumericAmountAccuracyCaptionGenerator(
         MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
