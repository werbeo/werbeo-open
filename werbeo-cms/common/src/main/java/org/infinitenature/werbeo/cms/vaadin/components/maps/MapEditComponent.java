package org.infinitenature.werbeo.cms.vaadin.components.maps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.components.CoordinateSystemButton;
import org.infinitenature.werbeo.cms.vaadin.components.PolygonCoordinatesButtonNG;
import org.infinitenature.werbeo.cms.vaadin.components.TextFieldWithButton;
import org.infinitenature.werbeo.cms.vaadin.components.map.CoordinateSystem;
import org.infinitenature.werbeo.cms.vaadin.components.map.MapCRSTranslator;
import org.infinitenature.werbeo.cms.vaadin.components.map.Values;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.Layer;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.LayerName;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.Layers;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.MapConfigurator;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.HasValue;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

public class MapEditComponent extends I18NComposite implements HasValue<Position>, MTBQuadrantSelector
{
   private JTSEditField editMap = new JTSEditField(this);
   private CoordinateSystemButton coordinateSystemButton;
   private TextFieldWithButton latLonTextField;
   private static final Logger LOGGER = LoggerFactory.getLogger(MapEditComponent.class);
   private PositionType positionType;
   private RadioButtonGroup<PositionType> positionTypeRadioButton;
   private RadioButtonGroup<Selection> mtbQuadrantRadioButton;
   private TextField mtbStringInputField;
   private Registration mtbStringInputFieldRegistration;
   private Registration positionTypeRadioButtonRegistration;
   private Registration mtbQuadrantRadioButtonRegistration;
   private Registration editMapRegistration;
   private PolygonCoordinatesButtonNG polygonInputButton;
   private Button polygonDeleteButton;
   private Layers layers;
   private HorizontalLayout mtbLayout;
   private HorizontalLayout polygonControls;
   private List<ValueChangeListener<Position>> valueChangeListeners = new ArrayList<HasValue.ValueChangeListener<Position>>();
   private String activeBaseLayer = "OpenStreetMap";
   private File uploadingFile;
   private Button applyShapeFileButton = new Button("Übernehmen");

   private String uploadedWkt;
   private CoordinateSystem uploadedCoordinateSystem = CoordinateSystem.EPSG4326;

   private ShapeFileProcessor shapeFileProcessor = new ShapeFileProcessor();

   private List<String> activeOverlays = new ArrayList<String>();

   private CoordinateSystem modelCoordinateSystem = CoordinateSystem.EPSG4326;

   private String geoserverUrl;

   private VerticalLayout mainLayout;

   private PortalConfiguration portalConfiguration;

   private PositionTypeChangeListener positionTypeChangeListener;

   private Button uploadShapeButton;

   enum Selection
   {
      WHOLE_MTB,
      MTB_QUADRANT_1DIGIT,
      MTB_QUADRANT_2DIGIT,
      MTB_QUADRANT_3DIGIT
   }

   public MapEditComponent(String geoserverUrl, PositionType initialPositionType,
         PortalConfiguration portalConfiguration, boolean allowShapeFileUpload,
          PositionType... positionTypes)
   {
      super(new MessagesResourceMock());
      this.portalConfiguration = portalConfiguration;
      this.geoserverUrl = geoserverUrl;

      setupEditMap(portalConfiguration);

      coordinateSystemButton = new CoordinateSystemButton(
            Values.START_COORDINATE_SYSTEM, getMessages(), portalConfiguration);
      coordinateSystemButton.addValueChangeListener(event -> onCoordinateSystemButtonValueChange());
      latLonTextField = new TextFieldWithButton(translate("COORDINATES") + " ( " + Values.START_COORDINATE_SYSTEM.getName() + ")" ,
            translate("INPUT_FORMAT"), VaadinIcons.CROSSHAIRS,
            coordinateSystemButton);

      latLonTextField.addStyleName("occurrence-input-field");
      latLonTextField.addStyleName("with-border");

      polygonInputButton = new PolygonCoordinatesButtonNG(getMessages(),
            Values.START_COORDINATE_SYSTEM, portalConfiguration);
      polygonInputButton.addValueChangeListener(event ->
      {
         modelCoordinateSystem = polygonInputButton.getCoordinateSystem();
         editMap.setValue(polygonInputButton.getValue());
      });

      polygonDeleteButton = new Button(translate("DELETE_POLYGON"));
      polygonDeleteButton.addClickListener(event -> setValue(null));



      uploadShapeButton = new Button("Shape als ZIP-Datei hochladen");
      uploadShapeButton.addClickListener(click -> showShapeUploadWindow());

      mtbQuadrantRadioButton = new RadioButtonGroup<Selection>();
      mtbQuadrantRadioButton.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
      mtbQuadrantRadioButton.setItems(Selection.values());
      mtbQuadrantRadioButton.setValue(Selection.WHOLE_MTB);


      mtbStringInputField = new TextField("Messtischblatt");

      mtbLayout = new HorizontalLayout(mtbStringInputField, mtbQuadrantRadioButton);

      positionTypeRadioButton = new RadioButtonGroup<Position.PositionType>();

      mainLayout = new VerticalLayout();
      mainLayout.setMargin(new MarginInfo(true, false));
      mainLayout.addComponent(positionTypeRadioButton);
      mainLayout.addComponent(mtbLayout);
      polygonControls = new HorizontalLayout();
      polygonControls.addComponent(polygonInputButton);
      polygonControls.addComponent(polygonDeleteButton);
      if(allowShapeFileUpload)
      {
         polygonControls.addComponent(uploadShapeButton);
      }
      mainLayout.addComponent(polygonControls);
      mainLayout.addComponent(latLonTextField);
      mainLayout.addComponent(editMap);
      mainLayout.setExpandRatio(editMap, 1F);

      positionTypeRadioButton.setItems(positionTypes);
      positionTypeRadioButton.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
      positionTypeRadioButton.setSelectedItem(initialPositionType);
      showAndHideControls(initialPositionType);
      initializeMap(initialPositionType);
      this.positionType = initialPositionType;
      activateListeners();

      setCompositionRoot(mainLayout);
      setSizeFull();
   }

   public double getPositionLat()
   {
      return editMap.getMap().getCenter().getLat();
   }

   public double getPositionLon()
   {
      return editMap.getMap().getCenter().getLon();
   }

   public double getZoom()
   {
      return editMap.getMap().getZoomLevel();
   }


   private List<CoordinateSystem> map(
         List<org.infinitenature.service.v1.types.enums.CoordinateSystem> coordinateSystems)
   {
      List<CoordinateSystem> systems = new ArrayList<>();
      coordinateSystems
            .forEach(apiSystem -> systems.add(CoordinateSystem.get(apiSystem)));
      return systems;
   }

   private void showShapeUploadWindow()
   {
      Window window = new Window("Shape ZIP-datei Upload");
      window.center();
      window.setWidth("40%");
      window.setHeight("40%");

      ComboBox<CoordinateSystem> comboBox = new ComboBox<>(
            "Koordinatensystem");
      comboBox.setItemCaptionGenerator(CoordinateSystem::getName);
      comboBox.setValue(Values.START_COORDINATE_SYSTEM);
      comboBox.setEmptySelectionAllowed(false);
      comboBox.setItems(map(portalConfiguration.getCoordinateSystems()));
      comboBox.setSizeFull();
      comboBox.setWidth("300px");
      comboBox.setPopupWidth("500px");

      comboBox.addValueChangeListener(
            event -> uploadedCoordinateSystem = event.getValue());
      Upload upload = new Upload();

      upload.setCaption("Upload Shape-ZIP-Datei");
      upload.setButtonCaption("Hochladen");
      upload.addSucceededListener(this::onFinish);
      upload.setReceiver((fileName, mimeType) ->
      {
         Path dir;
         try
         {
            dir = Files.createTempDirectory("userShapeUpload");
            uploadingFile = dir.resolve(fileName).toFile();
            return new FileOutputStream(uploadingFile);
         } catch (IOException e)
         {
            LOGGER.error("Error creating stream for upload file", e);
            return null;
         }

      });


      applyShapeFileButton.setEnabled(false);
      applyShapeFileButton.addClickListener(click -> {

         if(uploadedWkt != null && uploadedCoordinateSystem != null)
         {
            Position position = new Position();
            position.setType(PositionType.SHAPE);
            position.setWktEpsg(uploadedCoordinateSystem.getEpsg());
            position.setEpsg(uploadedCoordinateSystem.getEpsg());
            position.setWkt(uploadedWkt);
            setValue(position);
            uploadedWkt = null;
            uploadedCoordinateSystem = CoordinateSystem.EPSG4326;
         }
         UI.getCurrent().removeWindow(window);

      });


      HorizontalLayout inputLayout = new HorizontalLayout(comboBox, upload);
      window.setContent(new VerticalLayout(inputLayout, applyShapeFileButton));
      UI.getCurrent().addWindow(window);
   }

   public void onFinish(Upload.SucceededEvent event)
   {
      try
      {
         LOGGER.info("Finished upload of:" + event.getFilename());
         uploadedWkt = shapeFileProcessor.process(uploadingFile);
         System.out.println("-------------------------------");
         System.out.println(uploadedWkt);
         System.out.println("-------------------------------");
         applyShapeFileButton.setEnabled(uploadedCoordinateSystem != null);
         uploadingFile = null;
      }
      catch(Exception e)
      {
         UI.getCurrent().showNotification(
               "Fehler bei der Verarbeitung der Shape-Datei",
               Type.ERROR_MESSAGE);
      }
   }

   public void setPositionTypeChangeListener(PositionTypeChangeListener listener)
   {
      this.positionTypeChangeListener = listener;
   }

   private void setupEditMap(PortalConfiguration portalConfiguration)
   {

      layers = new Layers(geoserverUrl, portalConfiguration);
      syncActiveState();
      editMap.setConfigurator(new MapConfigurator(layers));
      editMap.getMap().addBaseLayerChangeListener(event ->
      {
         activeBaseLayer = event.getName();
      });
      editMap.getMap().addOverlayAddListener(event ->
      {
         activeOverlays.add(event.getName());
      });
      editMap.getMap().addOverlayRemoveListener(event ->
      {
         activeOverlays.remove(event.getName());
      });
      if(editMapRegistration != null)
      {
         editMapRegistration.remove();
      }
      editMapRegistration = editMap.addValueChangeListener(event ->{
         onMapValueChanged(editMap.getValue());
      });
      Context context = Context.getCurrent();
      if (context.getMapPositionConfig().getLastMapPositionLat() != null
            && context.getMapPositionConfig().getLastMapPositionLon() != null
            && context.getMapPositionConfig().getLastMapZoom() != null)
      {
         setupPositionAndZoom(
               context.getMapPositionConfig().getLastMapPositionLat(),
               context.getMapPositionConfig().getLastMapPositionLon(),
               context.getMapPositionConfig().getLastMapZoom());
      }
      else
      {
         setupPositionAndZoom(portalConfiguration.getMapInitialLatitude(),
               portalConfiguration.getMapInitialLongitude(),
               portalConfiguration.getMapInitialZoom());
      }
   }


   private void syncActiveState()
   {
      if (layers != null)
      {
         for (Layer layer : layers.getOverlays())
         {
            layer.setActive(
                  activeOverlays.contains(layer.getName().getCaption()));
         }

         for (Layer layer : layers.getBaselayers())
         {
            layer.setActive(StringUtils.equals(layer.getName().getCaption(),
                  activeBaseLayer));
         }
      }
   }

   private void onCoordinateSystemButtonValueChange()
   {
      modelCoordinateSystem = coordinateSystemButton.getValue();
      if(CoordinateSystem.getGaussKruegerSystems().contains(modelCoordinateSystem))
      {
         latLonTextField.setPlaceholder(translate("INPUT_FORMAT_GK"));
      }
      else
      {
         latLonTextField.setPlaceholder(translate("INPUT_FORMAT"));
      }
      latLonTextField.setCaption(translate("COORDINATES") + " ( " + modelCoordinateSystem.getName() + ")");
      Geometry mapGeo = editMap.getValue();
      if(mapGeo != null)
      {
         mapGeo = convertToModelIfNecessary(mapGeo);
         onMapValueChanged(mapGeo);
      }
   }

   private void onMTBTextInputChange(String value, boolean updateValue)
   {
      // check valid
      try
      {
         MTB mtb = MTBHelper.toMTB(value);
         Position position = new Position();
         org.infinitenature.service.v1.types.MTB serviceMTB = new org.infinitenature.service.v1.types.MTB();
         serviceMTB.setMtb(mtb.getMtb());
         position.setMtb(serviceMTB);
         position.setWkt(mtb.toWkt());
         position.setWktEpsg(Values.MTB_EPSG);
         position.setEpsg(Values.MTB_EPSG);
         position.setType(Position.PositionType.MTB);
         setMtbQuadRadio(value);
         if(updateValue)
         {
            setValue(position);
         }

      }
      catch(Exception e)
      {
         // no valid mtb
      }
   }

   private void setMtbQuadRadio(String value)
   {
      switch(value.length())
      {
      case 4:
         mtbQuadrantRadioButton.setValue(Selection.WHOLE_MTB);
         break;
      case 6:
         mtbQuadrantRadioButton.setValue(Selection.MTB_QUADRANT_1DIGIT);
         break;
      case 7:
         mtbQuadrantRadioButton.setValue(Selection.MTB_QUADRANT_2DIGIT);
         break;
      case 8:
         mtbQuadrantRadioButton.setValue(Selection.MTB_QUADRANT_3DIGIT);
         break;
      }
   }

   private void onCoordinateInputChange()
   {
      if (latLonTextField.getTextField().getValue().isEmpty())
      {
         editMap.setValue(null);
         return;
      }
      drawManualPointToMap(CoordinateSplitter
            .split(latLonTextField.getTextField().getValue()));
   }

   private void onMapValueChanged(Geometry geometry)
   {
      if (geometry != null)
      {
         geometry = convertToModelIfNecessary(geometry);
         if (geometry instanceof Point)
         {
            // mute valueChangeListener
            latLonTextField.setValue("" + truncAndRemoveTrailingZeros(geometry.getCoordinate().y)
                  + " " + truncAndRemoveTrailingZeros(geometry.getCoordinate().x));
            latLonTextField.addShortcutListener(new ShortcutListener("Shortcut Name",
                  ShortcutAction.KeyCode.ENTER, null)
            {
               @Override
               public void handleAction(Object sender, Object target)
               {
                  onCoordinateInputChange();
               }
            });
         } else if (geometry instanceof Polygon)
         {
            // check polygon validity
            try
            {
               new GeometryHelper().parse(new WKTWriter().write(geometry),
                     coordinateSystemButton.getValue().getEpsg());
            }
            catch(Exception e)
            {
               Notification.show(translate("ERROR"), translate("INVALID_POLYGON"),
                     Notification.Type.ERROR_MESSAGE);
               editMap.setValue(null);
               return;
            }


            polygonInputButton.setValue((Polygon) geometry);
         }
         for (ValueChangeListener<Position> listener : valueChangeListeners)
         {
            listener.valueChange(
                  new ValueChangeEvent<Position>(this, null, true));
         }
      }
   }

   private String truncAndRemoveTrailingZeros(double x)
   {
      String stringValue = BigDecimal.valueOf(x)
            .setScale(6, RoundingMode.HALF_UP)
            .toPlainString();
      while(stringValue.endsWith("0"))
      {
         stringValue = stringValue.substring(0, stringValue.length() -2);
      }
      return stringValue;
   }

   private void drawManualPointToMap(double[] coordinates)
   {
      if(coordinates.length == 2)
      {
         Point point = new GeometryHelper().getPoint(coordinates[0], coordinates[1],
               coordinateSystemButton.getValue().getEpsg());
         if(editMapRegistration != null)
         {
            editMapRegistration.remove();
         }
         editMap.setValue(point);
         editMapRegistration = editMap.addValueChangeListener(event -> onMapValueChanged(editMap.getValue()));
      }
   }

   public void setupPositionAndZoom(double lat, double lon, double zoom)
   {
      editMap.getMap().setCenter(lat, lon);
      editMap.getMap().setZoomLevel(zoom);
   }

   @Override
   public void applyLocalizations()
   {
      coordinateSystemButton.applyLocalizations();
      polygonInputButton.applyLocalizations();
      mtbQuadrantRadioButton.setItemCaptionGenerator(item -> translate(item.name()));
      positionTypeRadioButton.setItemCaptionGenerator(item -> translate(item.name()));
      mtbStringInputField.setCaption(translate("MTB_INPUT"));
      mtbQuadrantRadioButton.setCaption(translate("CHOICE_IN_MAP"));
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }


   private void onChangePositionType(PositionType positionType)
   {
      this.positionType = positionType;
      Context.getCurrent().getMapPositionConfig()
      .setLastMapPositionLat(getPositionLat());
      Context.getCurrent().getMapPositionConfig()
            .setLastMapPositionLon(getPositionLon());
      Context.getCurrent().getMapPositionConfig().setLastMapZoom(getZoom());

      resetControls();
      initializeMap(positionType);
      if(positionType.equals(PositionType.SHAPE))
      {
         modelCoordinateSystem = Values.START_COORDINATE_SYSTEM;
         editMap.startPolygon();
      }
      else if(PositionType.MTB.equals(positionType))
      {
         modelCoordinateSystem = CoordinateSystem.get(Values.MTB_EPSG);
      }
      else if(positionType.equals(PositionType.POINT))
      {
         coordinateSystemButton.setValue(Values.START_COORDINATE_SYSTEM);
         onCoordinateSystemButtonValueChange();
      }

      showAndHideControls(positionType);

      if(positionTypeChangeListener != null)
      {
         positionTypeChangeListener.positionTypeChanged(positionType);
      }
   }

   private void resetControls()
   {
      muteListeners();
      latLonTextField.setValue("");
      mtbQuadrantRadioButton.setValue(Selection.WHOLE_MTB);
      mtbStringInputField.setValue("");
      polygonInputButton.setValue(null);
      activateListeners();
   }

   private void initializeMap(PositionType positionType)
   {
      // remove editMap, substitute with new one

      mainLayout.removeComponent(editMap);
      editMap = new JTSEditField(this);
      setupEditMap(portalConfiguration);
      mainLayout.addComponent(editMap);
      mainLayout.setExpandRatio(editMap, 1F);
      editMap.setPositionType(positionType);
   }

   private void showAndHideControls(PositionType positionType)
   {
      if(PositionType.POINT.equals(positionType))
      {
         latLonTextField.setVisible(true);
         mtbLayout.setVisible(false);
         polygonControls.setVisible(false);
      }
      else if(PositionType.MTB.equals(positionType))
      {
         latLonTextField.setVisible(false);
         for(Layer layer : layers.getOverlays())
         {
            if(layer.getName().equals(LayerName.MTB))
            {
               layer.setActive(true);
               layer.setActiveIndicator(true);
               editMap.setConfigurator(new MapConfigurator(layers));
               break;
            }
         }
         polygonControls.setVisible(false);
         mtbLayout.setVisible(true);
      }
      else if(PositionType.SHAPE.equals(positionType))
      {
         latLonTextField.setVisible(false);
         polygonControls.setVisible(true);
         mtbLayout.setVisible(false);
      }
   }

   @Override
   public void setValue(Position value)
   {
      muteListeners();
      if(value != null)
      {
         if(value.getType().equals(PositionType.MTB))
         {
            mtbStringInputField.setValue(value.getMtb().getMtb());
            onMTBTextInputChange(value.getMtb().getMtb(), false);
            positionType = PositionType.MTB;
         }
         else
         {
            coordinateSystemButton.setValue(CoordinateSystem.get(value.getEpsg()));
            modelCoordinateSystem = coordinateSystemButton.getValue();
            if(value.getType().equals(PositionType.POINT))
            {
               latLonTextField.setCaption(translate("COORDINATES") + " ( " + modelCoordinateSystem.getName() + ")");
               positionType = PositionType.POINT;
            }
            else
            {
               positionType = PositionType.SHAPE;
            }
         }

         Geometry geometry = readGeometry(value);
         positionTypeRadioButton.setSelectedItem(value.getType());
         initializeMap(positionType);
         editMap.setValue(geometry);
      }
      else
      {
         initializeMap(positionType);
         if(positionType.equals(PositionType.SHAPE))
         {
            polygonInputButton.setValue(null);
            editMap.startPolygon();
         }
         modelCoordinateSystem = CoordinateSystem.EPSG4326;
         if(positionTypeChangeListener != null)
         {
            positionTypeChangeListener.positionTypeChanged(positionType);
         }
      }
      activateListeners();
      showAndHideControls(positionType);
   }

   private void activateListeners()
   {

      latLonTextField.addShortcutListener(new ShortcutListener("Shortcut Name",
            ShortcutAction.KeyCode.ENTER, null)
      {
         @Override
         public void handleAction(Object sender, Object target)
         {
            onCoordinateInputChange();
         }
      });
      mtbStringInputFieldRegistration = mtbStringInputField.addValueChangeListener(event -> onMTBTextInputChange(event.getValue(), true));
      positionTypeRadioButtonRegistration = positionTypeRadioButton
            .addValueChangeListener(event -> onChangePositionType(
                  positionTypeRadioButton.getValue()));
      mtbQuadrantRadioButtonRegistration = mtbQuadrantRadioButton.addValueChangeListener(event -> editMap.setValue(null));
      editMapRegistration = editMap.addValueChangeListener(event ->{
         onMapValueChanged(editMap.getValue());
      });
   }

   private void muteListeners()
   {
      if(mtbStringInputFieldRegistration != null)
      {
         mtbStringInputFieldRegistration.remove();
      }
      if(positionTypeRadioButtonRegistration != null)
      {
         positionTypeRadioButtonRegistration.remove();
      }
      if(mtbQuadrantRadioButtonRegistration != null)
      {
         mtbQuadrantRadioButtonRegistration.remove();
      }
      if(editMapRegistration != null)
      {
         editMapRegistration.remove();
      }
   }

   @Override
   public Position getValue()
   {
      if(editMap.getValue() != null)
      {
         Geometry geometry = editMap.getValue();
         if(geometry instanceof Polygon )
         {
            Polygon polygon = (Polygon)geometry;
            if (polygon != null && !CGAlgorithms
                  .isCCW(polygon.getExteriorRing().getCoordinates()))
            {
               polygon = polygon.reverse();
            }
            geometry = polygon;
         }
         else if(geometry instanceof MultiPolygon && geometry.getNumGeometries() == 1)
         {
            MultiPolygon polygon = (MultiPolygon)geometry;
            geometry = polygon.getGeometryN(0);
            
            if (geometry != null && !CGAlgorithms
                  .isCCW(geometry.getCoordinates()))
            {
               geometry = geometry.reverse();
            }
         }
         else if(geometry instanceof MultiPolygon)
         {
            MultiPolygon polygon = (MultiPolygon)geometry;
            if(polygon.getNumGeometries() == 1)
            if (polygon != null && !CGAlgorithms
                  .isCCW(polygon.getCoordinates()))
            {
               polygon = polygon.reverse();
            }
            geometry = polygon;
         }
         return new PositionCreator().createFromGeometry(
            convertToModelIfNecessary(geometry), positionType,
            mtbQuadrantRadioButton.getValue());
      }
      else
      {
         return null;
      }
   }

   public void setBlur(int blurInMeters)
   {
      if(getValue() != null && getValue().getType().equals(PositionType.POINT))
      {
         editMap.draw(CircleCreator.createCircle((Point)readGeometry(getValue()), blurInMeters));
      }
   }

   private Geometry convertToModelIfNecessary(Geometry geometry)
   {
      if (!modelCoordinateSystem.equals(CoordinateSystem.UNSUPPORTED)
            && geometry.getSRID() != modelCoordinateSystem.getEpsg())
      {
         return new MapCRSTranslator(geometry.getSRID(),
               modelCoordinateSystem.getEpsg()).toModel(geometry);
      } else
      {
         return geometry;
      }
   }

   @Override
   public Registration addValueChangeListener(
         ValueChangeListener<Position> listener)
   {
      valueChangeListeners.add(listener);
      return null;
   }

   @Override
   public boolean isReadOnly()
   {
      return super.isReadOnly();
   }

   @Override
   public void setRequiredIndicatorVisible(boolean visible)
   {
      super.setRequiredIndicatorVisible(visible);
   }

   @Override
   public void setReadOnly(boolean readOnly)
   {
      super.setReadOnly(readOnly);
   }

   @Override
   public boolean isRequiredIndicatorVisible()
   {
      return super.isRequiredIndicatorVisible();
   }

   protected Geometry readGeometry(Position value)
   {
      Geometry geometry = null;
      try
      {
         geometry = new GeometryHelper()
               .parseToJts(value.getWkt(), value.getWktEpsg());
      } catch (ParseException e)
      {
         LOGGER.error("Failure loading geometry", e);
      }
      return geometry;
   }

   public Component[] getControlFields()
   {
      List<Component> fields = new ArrayList<Component>();
      fields.add(latLonTextField);
      return fields.toArray(new Component[fields.size()]);
   }

   @Override
   public Selection getSelection()
   {
      return mtbQuadrantRadioButton.getValue();
   }

   @Override
   public void setMTBString(String mtbString)
   {
      if(mtbStringInputFieldRegistration != null)
      {
         mtbStringInputFieldRegistration.remove();
      }
      this.mtbStringInputField.setValue(mtbString);
      setMtbQuadRadio(mtbString);
      mtbStringInputFieldRegistration = mtbStringInputField
            .addValueChangeListener(
                  event -> onMTBTextInputChange(event.getValue(), true));
      for (ValueChangeListener<Position> listener : valueChangeListeners)
      {
         listener.valueChange(
               new ValueChangeEvent<Position>(this, null, true));
      }
   }
}
