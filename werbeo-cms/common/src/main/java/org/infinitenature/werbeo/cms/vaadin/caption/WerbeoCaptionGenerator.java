package org.infinitenature.werbeo.cms.vaadin.caption;

import org.vaadin.viritin.fields.CaptionGenerator;

import com.vaadin.ui.ItemCaptionGenerator;

public interface WerbeoCaptionGenerator<T>
      extends ItemCaptionGenerator<T>, CaptionGenerator<T>
{
   /**
    * just delegeates to {@link ItemCaptionGenerator#apply(Object)}
    */
   @Override
   default public String getCaption(T option)
   {
      return apply(option);
   }
}
