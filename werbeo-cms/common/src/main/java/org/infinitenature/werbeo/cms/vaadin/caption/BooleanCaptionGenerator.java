package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class BooleanCaptionGenerator extends I18NCaptionGenerator<Boolean>
{

   public BooleanCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
