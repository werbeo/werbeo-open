package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.service.v1.types.Position.PositionType;

public interface PositionTypeChangeListener
{
   public void positionTypeChanged(PositionType positionType);
}
