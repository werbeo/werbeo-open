package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.UI;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusFukarekCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class SettlementStatusFukarekComboBox
      extends EnumComboBox<Occurrence.SettlementStatusFukarek>
{
   public SettlementStatusFukarekComboBox(MessagesResource messagesResource)
   {
      super(messagesResource, Occurrence.SettlementStatusFukarek.class,
            new SettlementStatusFukarekCaptionGenerator(messagesResource));
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      setEmptySelectionCaption(translate("EMPTY"));
      setCaption(translate("OCCURRENCE_STATUS"));
   }
}
