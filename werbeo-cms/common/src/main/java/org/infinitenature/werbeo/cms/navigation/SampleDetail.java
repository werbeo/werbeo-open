package org.infinitenature.werbeo.cms.navigation;

import java.util.UUID;

public class SampleDetail implements NavigationTarget
{
   private final UUID sampleUUID;

   public SampleDetail(UUID sampleUUID)
   {
      super();
      this.sampleUUID = sampleUUID;
   }

   public UUID getSampleUUID()
   {
      return sampleUUID;
   }
}
