package org.infinitenature.werbeo.cms.vaadin.components.form;

import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.button.DeleteButton;
import org.infinitenature.werbeo.cms.vaadin.components.button.SaveButton;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.infinitenature.werbeo.cms.vaadin.notification.NotificationUtils;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import com.vaadin.data.Binder;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public abstract class WerbeoForm<T extends Base<?>> extends
      I18NComposite
      implements FormView<T>
{
   private final SaveButton saveButtonTop;
   private final SaveButton saveButton;
   private final FormView.FormObsever<T> observer;
   protected final Binder<T> binder;
   private final Class<T> clazz;
   private final VerticalLayout mainLayout;
   private final DeleteButton deleteButton;
   private final DeleteButton deleteButtonTop;

   public WerbeoForm(MessagesResource messages, Class<T> clazz,
         FormView.FormObsever<T> observer)
   {
      super(messages);
      this.observer = observer;
      this.binder = new Binder<>(clazz);
      this.clazz = clazz;
      this.saveButton = new SaveButton(messages, e -> onSaveClick());
      this.saveButtonTop = new SaveButton(messages, e -> onSaveClick());
      this.deleteButton = new DeleteButton(messages, e -> onDeleteClick());
      this.deleteButtonTop = new DeleteButton(messages, e -> onDeleteClick());

      MHorizontalLayout topButtonLayout = new MHorizontalLayout()
            .withDefaultComponentAlignment(Alignment.MIDDLE_RIGHT)
            .withFullWidth().space().with(deleteButtonTop, saveButtonTop);
      MHorizontalLayout bottomButtonLayout = new MHorizontalLayout()
            .withDefaultComponentAlignment(Alignment.MIDDLE_RIGHT)
            .withFullWidth().space().with(deleteButton, saveButton);

      mainLayout = FormUtils.createMainLayout().with(topButtonLayout,
            bottomButtonLayout);
      setCompositionRoot(mainLayout);
   }

   protected void addLayoutPart(Component part)
   {
      mainLayout.addComponent(part, mainLayout.getComponentCount() - 1);
   }

   private void onSaveClick()
   {
      observer.onSave(binder.getBean());
   }

   private void onDeleteClick()
   {
      observer.onDelete(binder.getBean());
   }

   @Override
   public void reset()
   {
      binder.removeBean();
      try
      {
         binder.setBean(clazz.newInstance());
      } catch (InstantiationException | IllegalAccessException e)
      {
         throw new RuntimeException(
               "Failure creating new instance of " + clazz.getSimpleName(), e);
      }
   }

   public abstract String getEntityIdentifier();

   public abstract Object[] getMessageParamter(T entity);

   @Override
   public void success(T entity)
   {
      NotificationUtils.showSaveSuccess(
            getMessages().getMessage(Context.getCurrent().getPortal(),
                  "general.saved", getLocale()),
            getMessages().getMessage(Context.getCurrent().getPortal(),
                  getEntityIdentifier() + ".saved", getLocale(),
                  getMessageParamter(entity)));
   }

   @Override
   public void failure(T entity)
   {
      NotificationUtils.showSaveSuccess(
            getMessages().getMessage(Context.getCurrent().getPortal(),
                  "general.failure", getLocale()),
            getMessages().getMessage(Context.getCurrent().getPortal(),
                  getEntityIdentifier() + ".savedFailed", getLocale(),
                  getMessageParamter(entity)));
   }

   @Override
   public void setEntity(T entity)
   {
      binder.setBean(entity);
      boolean allowUpdate = entity.getAllowedOperations()
            .contains(Operation.UPDATE);
      boolean allowCreate = entity.getAllowedOperations()
            .contains(Operation.CREATE);
      boolean allowDelete = entity.getAllowedOperations()
            .contains(Operation.DELETE);
      boolean allowSave = allowUpdate || allowCreate;

      saveButton.setEnabled(allowSave);
      saveButtonTop.setEnabled(allowSave);

      deleteButton.setEnabled(allowDelete);
      deleteButtonTop.setEnabled(
            entity.getAllowedOperations().contains(Operation.DELETE));
   }

}
