package org.infinitenature.werbeo.cms.vaadin.plugin.tables;

import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

import com.vaadin.data.provider.ConfigurableFilterDataProvider;

public interface TableView<T, FILTER_TYPE> extends CMSVaadinView
{
   void setDataProvider(
         ConfigurableFilterDataProvider<T, Void, FILTER_TYPE> dataProvider);
}
