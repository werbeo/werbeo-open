package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class RecordStatusCaptionGenerator extends I18NCaptionGenerator<Occurrence.RecordStatus>
{
   public RecordStatusCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }
}
