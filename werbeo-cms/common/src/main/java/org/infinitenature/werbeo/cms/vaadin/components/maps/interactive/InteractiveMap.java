package org.infinitenature.werbeo.cms.vaadin.components.maps.interactive;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.IAMapFilter;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.LOccurrenceLayer;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.Layers;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.MapConfigurator;
import org.infinitenature.werbeo.cms.vaadin.components.maps.JTSViewField;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.viritin.layouts.MVerticalLayout;

public class InteractiveMap extends I18NComposite
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(InteractiveMap.class);
   private JTSViewField map = new JTSViewField();
   private final MVerticalLayout mainLayout = new MVerticalLayout(map);
   private InstanceConfig instanceConfig;
   private LOccurrenceLayer occurrenceOverlay;
   private String token;

   public InteractiveMap(MessagesResource messages,
         PortalConfiguration portalConfiguration, InstanceConfig instanceConfig)
   {
      super(messages);
      this.map.setConfigurator(new MapConfigurator(new Layers(
            instanceConfig.getGeoserver().getUrl(), portalConfiguration)));
      this.map.getMap().setCenter(portalConfiguration.getMapInitialLatitude(),
            portalConfiguration.getMapInitialLongitude());
      this.map.getMap().setZoomLevel(portalConfiguration.getMapInitialZoom());
      this.map.getMap().addClickListener(
            event -> LOGGER.debug("Click on map at {}", event));
      this.instanceConfig = instanceConfig;
      this.occurrenceOverlay = createLayer(instanceConfig);

      setCompositionRoot(mainLayout);
      setSizeFull();

   }

   LOccurrenceLayer createLayer(InstanceConfig instanceConfig)
   {
      LOccurrenceLayer layer = new LOccurrenceLayer(
            instanceConfig.getGeoserver().getUrl(), "werbeo:mv-occ");
      layer.setZindex(200);
      layer.setTileSize(1024);
      layer.addLoadingListener(event -> System.out.println("LOADING"));
      layer.addLoadListener(event -> System.out.println("LOAD"));
      return layer;
   }

   @Override
   public void attach()
   {
      super.attach();
      // Add late, else its hidden by the base layers
      map.getMap().addOverlay(occurrenceOverlay, "Funde");

   }

   public void setFilter(IAMapFilter filter)
   {
      map.getMap().removeLayer(occurrenceOverlay);
      occurrenceOverlay = createLayer(instanceConfig);
      if (filter != null)
      {
         String cqlString = filter.getCQL(token);
         if (StringUtils.isNotBlank(cqlString))
         {
            occurrenceOverlay.setCQLFilter(cqlString);
         } else
         {
            occurrenceOverlay.setCQLFilter(null);
         }
      } else
      {
         occurrenceOverlay.setCQLFilter(null);
      }
      map.getMap().addOverlay(occurrenceOverlay,
            "Funde" + RandomUtils.nextInt());
   }

   @Override
   public void applyLocalizations()
   {
      // Nothing to translate
   }

   public void setToken(String token)
   {
      this.token = token;
   }
}
