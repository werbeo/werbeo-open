package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent.Selection;

public class MTBUtils
{       
   public static String processQuadrantSelectiom(String mtbString, Selection selection)
   {
      if (Selection.WHOLE_MTB.equals(selection))
      {
         return mtbString.substring(0, 4);
      }
      if (Selection.MTB_QUADRANT_1DIGIT.equals(selection))
      {
         return mtbString.substring(0, 6);
      }
      if (Selection.MTB_QUADRANT_2DIGIT.equals(selection))
      {
         return mtbString.substring(0, 7);
      }
      if (Selection.MTB_QUADRANT_3DIGIT.equals(selection))
      {
         return mtbString.substring(0, 8);
      }
      return mtbString;
   }
}
