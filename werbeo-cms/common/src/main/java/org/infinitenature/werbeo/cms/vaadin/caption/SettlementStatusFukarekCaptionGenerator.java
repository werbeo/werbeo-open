package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.SettlementStatusFukarek;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class SettlementStatusFukarekCaptionGenerator
      extends I18NCaptionGenerator<SettlementStatusFukarek>
{

   public SettlementStatusFukarekCaptionGenerator(
         MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
