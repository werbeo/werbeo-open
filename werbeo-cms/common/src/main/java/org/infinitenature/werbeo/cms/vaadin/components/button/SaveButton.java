package org.infinitenature.werbeo.cms.vaadin.components.button;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

import com.vaadin.icons.VaadinIcons;

public class SaveButton extends I18NButton
{

   public SaveButton(MessagesResource messages)
   {
      super(messages);
   }

   public SaveButton(MessagesResource messages, ClickListener listener)
   {
      super(messages, listener);
   }

   @Override
   protected void init()
   {
      setIcon(VaadinIcons.PLUS_CIRCLE_O);
   }

   @Override
   protected String getKey()
   {
      return "SAVE";
   }

   @Override
   public SaveButton addClickListener(MClickListener listener)
   {
      return (SaveButton) super.addClickListener(listener);
   }

}
