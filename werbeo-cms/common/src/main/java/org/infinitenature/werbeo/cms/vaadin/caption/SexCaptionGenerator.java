package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class SexCaptionGenerator extends I18NCaptionGenerator<Occurrence.Sex>
{

   public SexCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
