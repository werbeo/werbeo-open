package org.infinitenature.werbeo.cms.vaadin.components.comment;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.infinitenature.service.v1.types.AbstractComment;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.FormListener;
import org.infinitenature.werbeo.cms.vaadin.components.comment.CommentPanel.ReplyCommentListener;
import org.infinitenature.werbeo.cms.vaadin.components.comment.ListenerSupport.HasListener;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.infinitenature.werbeo.common.commons.WerbeoStringUtils;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.label.RichText;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.jarektoro.responsivelayout.ResponsiveColumn;
import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

public class CommentPanel extends I18NField<AbstractComment>
      implements HasListener<ReplyCommentListener>
{
   @FunctionalInterface
   public interface ReplyCommentListener
   {
      public void onReplyComment(String reply, AbstractComment comment);
   }

   private final ListenerSupport<ReplyCommentListener> replyListeners = new ListenerSupport<>();
   private AbstractComment comment;
   private final ResponsiveLayout layout = new ResponsiveLayout();
   private final ResponsiveRow row;

   private final MButton replyButton = new MButton(VaadinIcons.REPLY)
         .addClickListener(this::onReplyClick);

   private final RichText metaContent = new RichText();
   private final MVerticalLayout metaLayout = new MVerticalLayout(metaContent,
         replyButton).withMargin(false);
   private final RichText content = new RichText();

   public CommentPanel(MessagesResource messages)
   {
      super(messages);
      row = layout.addRow();
      ResponsiveColumn metaColumn = new ResponsiveColumn(12, 12, 3, 3);
      ResponsiveColumn contentColumn = new ResponsiveColumn(12, 12, 9, 9);

      row.addColumn(metaColumn);

      row.addColumn(contentColumn);
      metaColumn.setComponent(metaLayout);
      contentColumn.setComponent(content);
   }

   public CommentPanel(MessagesResource messages, ReplyCommentListener replyListener)
   {
      this(messages);
      addListner(replyListener);
   }

   public CommentPanel(MessagesResource messages, AbstractComment value)
   {
      this(messages);
      setValue(value);
   }

   public CommentPanel(MessagesResource messages, AbstractComment value,
         ReplyCommentListener replyListener)
   {
      this(messages, replyListener);
      setValue(value);
   }

   public void onReplyClick()
   {
      CommentField commentField = new CommentField(getMessages());
      commentField.setSizeFull();
      commentField
            .setValue(WerbeoStringUtils.prefixEachLin(comment.getComment(), "> "));
      Window window = new Window("Kommentar", commentField);
      window.setModal(true);
      window.center();
      window.setHeight("90%");
      window.setWidth("90%");
      UI.getCurrent().addWindow(window);
      commentField.addListner(new FormListener<CommentField>()
      {
         @Override
         public void onSave(CommentField source)
         {
            replyListeners.fire(
                  listener -> listener.onReplyComment(source.getValue(), comment));
            window.close();
         }

         @Override
         public void onCancel(CommentField source)
         {
            window.close();
         }
      });

   }

   @Override
   public void applyLocalizations()
   {
      Portal portal = Context.getCurrent().getPortal();
      metaContent.withMarkDown(
            getMessages().getMessage(portal, "comment", getLocale(), comment
                  .getCreatedBy(),
            comment.getCreationDate().format(DateTimeFormatter
                  .ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT)
                        .withLocale(getLocale()))));
      content.withMarkDown(comment.getComment());
      replyButton.setCaption(
            getMessages().getMessage(portal, "general.reply", getLocale()));
   }

   @Override
   public AbstractComment getValue()
   {
      return comment;
   }

   @Override
   protected Component initContent()
   {
      return layout;
   }

   @Override
   protected void doSetValue(AbstractComment value)
   {
      this.comment = value;
   }

   @Override
   public Registration addListner(ReplyCommentListener listener)
   {
      return replyListeners.addListener(listener);
   }

   @Override
   public boolean removeListener(ReplyCommentListener listener)
   {
      return replyListeners.removeListener(listener);
   }

}
