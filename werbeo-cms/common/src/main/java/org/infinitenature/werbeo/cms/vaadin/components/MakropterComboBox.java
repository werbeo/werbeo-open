package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence.Makropter;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.MakropterCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class MakropterComboBox extends EnumComboBox<Makropter>
{

   public MakropterComboBox(MessagesResource messages)
   {
      super(messages, Makropter.class, new MakropterCaptionGenerator(messages));
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "MAKROPTER", getLocale()));
   }
}
