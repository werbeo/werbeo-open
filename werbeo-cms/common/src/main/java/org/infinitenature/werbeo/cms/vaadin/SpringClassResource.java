package org.infinitenature.werbeo.cms.vaadin;

import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.vaadin.server.ClassResource;
import com.vaadin.server.DownloadStream;

public class SpringClassResource extends ClassResource
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(SpringClassResource.class);
   private final String resourceName;

   public SpringClassResource(Class<?> associatedClass, String resourceName)
   {
      super(associatedClass, resourceName);
      this.resourceName = resourceName;
   }

   public SpringClassResource(String resourceName)
   {
      super(resourceName);
      this.resourceName = resourceName;
   }

   @Override
   public DownloadStream getStream()
   {
      try
      {
         InputStream stream = new ClassPathResource(resourceName)
               .getInputStream();
         final DownloadStream ds = new DownloadStream(stream, getMIMEType(),
               getFilename());
         ds.setBufferSize(getBufferSize());
         ds.setCacheTime(getCacheTime());
         return ds;
      } catch (IOException e)
      {
         LOGGER.error("Failure finding resource {}, for class {}", resourceName,
               getAssociatedClass(), e);
         return new DownloadStream(null, getMIMEType(), getFilename());
      }

   }

}
