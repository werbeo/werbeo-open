package org.infinitenature.werbeo.cms.vaadin;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.time.StopWatch;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.SizeChangeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;
import com.vaadin.shared.data.sort.SortDirection;

public abstract class WerbeoDataProvider<T, F, S>
      extends AbstractBackEndDataProvider<T, F>
{
   private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
   protected final Werbeo werbeo;
   private int lastSize = -1;
   private List<SizeChangeListener> sizeChangeListeners = new ArrayList<>();

   public WerbeoDataProvider(Werbeo werbeo)
   {
      super();
      this.werbeo = werbeo;
   }

   public void addSizeChangeListener(SizeChangeListener sizeChangeListener)
   {
      this.sizeChangeListeners.add(sizeChangeListener);
   }

   @Override
   protected Stream<T> fetchFromBackEnd(Query<T, F> query)
   {
      StopWatch sw = new StopWatch();
      sw.start();
      SortOrder sortOrder = SortOrder.ASC;
      S sortField = getDefaultSortField();
      if (!query.getSortOrders().isEmpty())
      {
         com.vaadin.data.provider.SortOrder<String> vaadinSortOrder = query
               .getSortOrders().get(0);
         sortOrder = toSortOrder(vaadinSortOrder);
         sortField = toSortField(vaadinSortOrder);
      }
      LOGGER.debug("Fetch - Offset: {}, Limit: {}, Filter: {}",
            query.getOffset(), query.getLimit(), query.getFilter());

      List<T> content = getContent(query.getFilter(), sortField, sortOrder,
            query.getOffset(), query.getLimit());
      LOGGER.debug("Fetch size: {}", content.size());
      sw.stop();
      LOGGER.info("Fetch data took {} ms", sw.getTime());
      return content.stream();
   }

   protected abstract List<T> getContent(Optional<F> filter, S sortField,
         SortOrder sortOrder, int offset, int limit);

   protected abstract S toSortField(
         com.vaadin.data.provider.SortOrder<String> vaadinSortOrder);

   public static SortOrder toSortOrder(
         com.vaadin.data.provider.SortOrder<String> vaadinSortOrder)
   {
      return vaadinSortOrder.getDirection() == SortDirection.ASCENDING
            ? SortOrder.ASC
            : SortOrder.DESC;
   }

   protected abstract S getDefaultSortField();

   @Override
   protected int sizeInBackEnd(Query<T, F> query)
   {

      StopWatch sw = new StopWatch();
      sw.start();
      LOGGER.debug("count - Filter: {}", query.getFilter());
      int size = 0;
      try
      {
         size = getSize(query);
      } catch (WebApplicationException wae)
      {
         try (Response response = wae.getResponse();)
         {
            String entity = response.readEntity(String.class);
            LOGGER.error("Failure getting size from service, response was: {}",
                  entity, wae);
         }

      }
      LOGGER.debug("count size: {}", size);
      sw.stop();
      LOGGER.info("Count data took {} ms", sw.getTime());
      if (lastSize != size)
      {
         lastSize = size;
         for (SizeChangeListener listener : sizeChangeListeners)
         {
            listener.onSizeChange(size);
         }
      }
      return size;

   }

   protected abstract int getSize(Query<T, F> query);

}
