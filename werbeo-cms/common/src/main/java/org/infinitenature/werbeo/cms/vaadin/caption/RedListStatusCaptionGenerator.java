package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.*;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class RedListStatusCaptionGenerator
      extends I18NCaptionGenerator<Taxon.RedListStatus>
{

   public RedListStatusCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
