package org.infinitenature.werbeo.cms.vaadin.caption;

public class LatLongCaptionGenerator implements WerbeoCaptionGenerator<Double>
{
   @Override
   public String apply(Double option)
   {
      if (option == null)
      {
         return "";
      } else
      {
         return String.format("%f", option);
      }
   }
}
