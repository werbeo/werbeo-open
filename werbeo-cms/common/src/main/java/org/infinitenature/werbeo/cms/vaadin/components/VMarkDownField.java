package org.infinitenature.werbeo.cms.vaadin.components;

import org.vaadin.viritin.label.RichText;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.TextArea;

/**
 * to be replaced by the viritin component when its released
 * 
 * @author dve
 *
 */
public class VMarkDownField extends CustomField<String>
{
   private final HorizontalLayout mainLayout = new HorizontalLayout();
   private final TabSheet tabSheet = new TabSheet();
   private final TextArea textArea = new TextArea();
   private final RichText previewLabel = new RichText();
   private Tab entry;
   private Tab preview;

   private String editCaption = "Edit";
   private String previewCaption = "Preview";

   public VMarkDownField()
   {
      mainLayout.setSizeFull();
      mainLayout.setMargin(false);
      mainLayout.setSpacing(false);
      textArea.setSizeFull();
      previewLabel.setSizeFull();
      tabSheet.setSizeFull();

      buildTabs();

      tabSheet.addSelectedTabChangeListener(e ->
      {
         if (tabSheet.getSelectedTab().equals(previewLabel))
         {
            previewLabel.withMarkDown(textArea.getValue());
         }
      });
      mainLayout.addComponent(tabSheet);
   }

   private void buildTabs()
   {
      tabSheet.removeAllComponents();
      entry = tabSheet.addTab(textArea, editCaption);
      preview = tabSheet.addTab(previewLabel, previewCaption);
   }

   @Override
   public void setReadOnly(boolean readOnly)
   {
      super.setReadOnly(readOnly);
      textArea.setReadOnly(readOnly);
      mainLayout.removeAllComponents();
      if (readOnly)
      {
         mainLayout.addComponent(previewLabel);
      } else
      {
         buildTabs();
         mainLayout.addComponent(tabSheet);
      }
   }

   @Override
   public String getValue()
   {
      return textArea.getValue();
   }

   @Override
   protected Component initContent()
   {

      return mainLayout;
   }

   @Override
   protected void doSetValue(String value)
   {
      textArea.setValue(value);
      previewLabel.withMarkDown(value);
   }

   public void setPreviewCaption(String previewCaption)
   {
      this.previewCaption = previewCaption;
      preview.setCaption(previewCaption);
   }

   public VMarkDownField withPreviewCaption(String previewCaption)
   {
      setPreviewCaption(previewCaption);
      return this;
   }

   public void setEditCaption(String editCaption)
   {
      this.editCaption = editCaption;
      entry.setCaption(editCaption);
   }

   public VMarkDownField withEditCaption(String editCaption)
   {
      setEditCaption(editCaption);
      return this;
   }

   public String getPreviewCaption()
   {
      return previewCaption;
   }

   public String getEditCaption()
   {
      return editCaption;
   }

}
