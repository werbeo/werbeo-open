package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class AvailabilityCaptionGenerator
      extends I18NCaptionGenerator<Availability>
{

   public AvailabilityCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
