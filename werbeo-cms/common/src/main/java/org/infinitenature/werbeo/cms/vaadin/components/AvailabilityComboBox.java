package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.AvailabilityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class AvailabilityComboBox extends EnumComboBox<Availability>
{

   public AvailabilityComboBox(MessagesResource messages)
   {
      super(messages, Availability.class,
            new AvailabilityCaptionGenerator(messages));
   }

}
