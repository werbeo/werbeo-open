package org.infinitenature.werbeo.cms.vaadin;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Filters
{
   public static final Filters NO_FILTERS = new Filters();
   private TaxonBase filterTaxon;
   private SurveyBase filterSurvey;
   private VagueDate filterDate;
   private Position filterPosition;
   private Integer filterBlur;
   private String occExternalKey;
   private ValidationStatus validationStatus;
   private Person validator;

   public Person getValidator()
   {
      return validator;
   }

   public void setValidator(Person validator)
   {
      this.validator = validator;
   }

   public String getOccExternalKey()
   {
      return occExternalKey;
   }

   public Integer getFilterBlur()
   {
      return filterBlur;
   }

   public void setFilterBlur(Integer filterBlur)
   {
      this.filterBlur = filterBlur;
   }

   public TaxonBase getFilterTaxon()
   {
      return filterTaxon;
   }

   public void setFilterTaxon(TaxonBase filterTaxon)
   {
      this.filterTaxon = filterTaxon;
   }

   public SurveyBase getFilterSurvey()
   {
      return filterSurvey;
   }

   public void setFilterSurvey(SurveyBase filterSurvey)
   {
      this.filterSurvey = filterSurvey;
   }

   public Position getFilterPosition()
   {
      return filterPosition;
   }

   public void setFilterPosition(Position filterPosition)
   {
      this.filterPosition = filterPosition;
   }

   public boolean isSet()
   {
      return !this.equals(NO_FILTERS);
   }

   public VagueDate getFilterDate()
   {
      return filterDate;
   }

   public void setFilterDate(VagueDate filterDate)
   {
      this.filterDate = filterDate;
   }

   public void setOccExternalKey(String occExternalKey)
   {
      this.occExternalKey = occExternalKey;
   }

   public ValidationStatus getValidationStatus()
   {
      return validationStatus;
   }

   public void setValidationStatus(ValidationStatus validationStatus)
   {
      this.validationStatus = validationStatus;
   }

   @Override
   public String toString()
   {
      return FiltersBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return FiltersBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return FiltersBeanUtil.doEquals(this, obj);
   }
}
