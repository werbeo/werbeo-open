package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.stream.Stream;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.SurveySortField;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

public class SurveyComboBoxDataProvider extends AbstractBackEndDataProvider<SurveyBase, String>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyComboBoxDataProvider.class);
   private final Werbeo werbeo;

   public SurveyComboBoxDataProvider(Werbeo werbeo)
   {
      super();
      this.werbeo = werbeo;
   }

   @Override protected Stream<SurveyBase> fetchFromBackEnd(
         Query<SurveyBase, String> query)
   {
      LOGGER.debug("Fetch - Offset: {}, Limit: {}, Filter: {}",query.getOffset(), query.getLimit(), query.getFilter());
      SurveyFilter filter = new SurveyFilter();
      filter.setPortalId(Context.getCurrent().getPortal().getPortalId());
      filter.setNameContains(query.getFilter().orElse(""));
      return werbeo.surveys().find(query.getOffset(), query.getLimit(),
            SurveySortField.ID, SortOrder.ASC, filter).getContent().stream();
   }

   @Override protected int sizeInBackEnd(Query<SurveyBase, String> query)
   {
      SurveyFilter filter = new SurveyFilter();
      filter.setPortalId(Context.getCurrent().getPortal().getPortalId());
      filter.setNameContains(query.getFilter().orElse(""));
      return (int) werbeo.surveys()
            .count(filter).getAmount();
   }
}
