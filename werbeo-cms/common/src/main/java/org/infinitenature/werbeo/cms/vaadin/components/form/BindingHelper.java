package org.infinitenature.werbeo.cms.vaadin.components.form;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.meta.EntityField;
import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

import com.vaadin.data.Binder;
import com.vaadin.data.Binder.Binding;
import com.vaadin.data.Binder.BindingBuilder;
import com.vaadin.data.HasValue;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;

public abstract class BindingHelper<FIELD_TYPE extends EntityField<?>>
      implements Serializable
{

   protected Map<FIELD_TYPE, Boolean> fieldConfigs;
   protected Map<String, Component> fieldMapping;

   protected <ENTITY_TYPE, PROPERTY_TYPE> Binding<ENTITY_TYPE, PROPERTY_TYPE> doIfActive(
         FIELD_TYPE field, Supplier<Binding<ENTITY_TYPE, PROPERTY_TYPE>> func)
   {
      if (this.fieldConfigs.containsKey(field))
      {
         return func.get();
      } else
      {
         return null;
      }
   }

   public <E extends Base<?>, T> void addField(
         ComponentContainer componentContainer, HasValue<T> hasValue,
         FIELD_TYPE field)
   {
      appendComponent(componentContainer, hasValue, field, null);
   }

   public <E extends Base<?>, T> Binding<E, T> addField(Binder<E> binder,
         ComponentContainer componentContainer, HasValue<T> hasValue,
         FIELD_TYPE field)
   {
      return addField(binder, componentContainer, hasValue, field,
            field.getProperty(), null);
   }

   public <E extends Base<?>, T> Binding<E, T> addField(Binder<E> binder,
         ComponentContainer componentContainer, HasValue<T> hasValue,
         FIELD_TYPE field, String property)
   {
      return addField(binder, componentContainer, hasValue, field, property,
            null);
   }
   public <E extends Base<?>, T> Binding<E, T> addField(Binder<E> binder,
         ComponentContainer componentContainer,
         HasValue<T> hasValue,
         FIELD_TYPE field, String property, String translationSubKey)
   {
      return doIfActive(field, () ->
      {
         Binding<E, T> binding = null;
         BindingBuilder<E, T> builder = binder.forField(hasValue);

         if (this.fieldConfigs.get(field).booleanValue())
         {
            if (!(hasValue instanceof CheckBox))
            {
               builder = builder.asRequired();
            }
            if (hasValue instanceof ComboBox<?>)
            {
               ((ComboBox<?>) hasValue).setEmptySelectionAllowed(false);
            }
         }
         binding = builder.bind(property);
         appendComponent(componentContainer, hasValue, field,
               translationSubKey);

         return binding;
      });
   }

   private <T> void appendComponent(ComponentContainer componentContainer,
         HasValue<T> hasValue, FIELD_TYPE field, String translationSubKey)
   {
      Component component = (Component) hasValue;
      component.setWidth("100%");
      componentContainer.addComponent(component);

      String mappingKey = StringUtils.isBlank(translationSubKey)
            ? field.toString()
            : field.toString() + "." + translationSubKey;
      fieldMapping.put(mappingKey, component);
   }

   public void applyLocalizations(MessagesResource messages,
         org.infinitenature.werbeo.cms.vaadin.Portal portal, Locale locale)
   {
      fieldMapping.forEach((field, component) ->
      {
         String caption = messages.getEntityField(portal, getEntityKey(),
               field, locale);
         component.setCaption(caption);
      });
   }

   protected abstract String getEntityKey();

}
