package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class MakropterCaptionGenerator extends I18NCaptionGenerator<Occurrence.Makropter>
{

   public MakropterCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
