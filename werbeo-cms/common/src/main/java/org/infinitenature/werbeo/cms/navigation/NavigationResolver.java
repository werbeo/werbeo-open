package org.infinitenature.werbeo.cms.navigation;

public interface NavigationResolver
{

   public void navigateTo(NavigationTarget target);
}
