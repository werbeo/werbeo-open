package org.infinitenature.werbeo.cms.vaadin.components.comment;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.FormListener;
import org.infinitenature.werbeo.cms.vaadin.components.MarkDownField;
import org.infinitenature.werbeo.cms.vaadin.components.button.DismissButton;
import org.infinitenature.werbeo.cms.vaadin.components.button.SaveButton;
import org.infinitenature.werbeo.cms.vaadin.components.comment.ListenerSupport.HasListener;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;

import com.jarektoro.responsivelayout.ResponsiveColumn;
import com.jarektoro.responsivelayout.ResponsiveColumn.ColumnComponentAlignment;
import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;

public class CommentField extends
      I18NField<String>
      implements HasListener<FormListener<CommentField>>
{
   private final ListenerSupport<FormListener<CommentField>> listeners = new ListenerSupport<>();

   private final ResponsiveLayout layout = new ResponsiveLayout()
         .withFullSize();
   private final ResponsiveRow contentRow = layout.addRow();
   private final MarkDownField content;
   private final ResponsiveRow buttonRow = layout.addRow()
         .withAlignment(Alignment.MIDDLE_RIGHT);
   private final DismissButton cancelButton;
   private final SaveButton saveButton;
   private final ResponsiveColumn submitColumn = buttonRow.addColumn()
         .withDisplayRules(12, 12, 6, 6).withGrow(true);
   private final ResponsiveColumn cancelCloumn = buttonRow.addColumn()
         .withDisplayRules(12, 12, 6, 6).withGrow(true);

   public CommentField(MessagesResource messages)
   {
      super(messages);
      this.content = new MarkDownField(getMessages());
      this.content.setHeight("30em");
      this.content.setWidth("100%");
      this.contentRow.setWidth("100%");
      ResponsiveColumn contentColumn = new ResponsiveColumn();
      contentColumn.setWidth("100%");
      contentColumn.setComponent(content);
      this.contentRow.addColumn(contentColumn);
      this.cancelButton = new DismissButton(
            getMessages())
            .addClickListener(this::onCancelClick);
      this.saveButton = new SaveButton(getMessages())
            .addClickListener(this::onSaveClick);
      this.submitColumn.withComponent(saveButton,
            ColumnComponentAlignment.RIGHT);
      this.cancelCloumn.withComponent(cancelButton,
            ColumnComponentAlignment.RIGHT);
   }

   public void onSaveClick()
   {
      listeners.fire(listener -> listener.onSave(this));
   }

   public void onCancelClick()
   {
      listeners.fire(listener -> listener.onCancel(this));
   }

   @Override
   public void applyLocalizations()
   {
      // nothing to localize
   }

   @Override
   public Registration addListner(FormListener<CommentField> listener)
   {
      return listeners.addListener(listener);
   }

   @Override
   public boolean removeListener(FormListener<CommentField> listener)
   {
      return listeners.removeListener(listener);
   }

   @Override
   public String getValue()
   {
      return content.getValue();
   }

   @Override
   protected Component initContent()
   {
      return layout;
   }

   @Override
   protected void doSetValue(String value)
   {
      content.setValue(value);
   }
}
