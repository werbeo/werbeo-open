package org.infinitenature.werbeo.cms.vaadin.components.maps;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.werbeo.cms.vaadin.components.map.MapCRSTranslator;
import org.infinitenature.werbeo.cms.vaadin.components.map.Values;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.Layers;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.MapConfigurator;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.HasValue;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Composite;
import com.vaadin.ui.VerticalLayout;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;

@SuppressWarnings("serial")
public class MapViewComponent extends Composite  implements HasValue<Locality>
{
   private JTSViewField map = new JTSViewField();
   private static final Logger LOGGER = LoggerFactory.getLogger(MapViewComponent.class);

   public MapViewComponent(String geoserverUrl, PortalConfiguration portalConfiguration)
   {

      map.setConfigurator(new MapConfigurator(new Layers(geoserverUrl, portalConfiguration)));
      setupPosition(portalConfiguration.getMapInitialLatitude(),
            portalConfiguration.getMapInitialLongitude(),
            portalConfiguration.getMapInitialZoom());
      VerticalLayout layout = new VerticalLayout();
      layout.addComponent(map);

      setCompositionRoot(layout);
      setSizeFull();
   }


   public void setValues(List<ComponentLocality> componentLocalities, boolean zoomToContent)
   {
      for (ComponentLocality locality : componentLocalities)
      {
         if (locality.getLocality().getPosition().getType()
               .equals(PositionType.POINT))
         {
            drawPoint(locality);
            if(zoomToContent)
            {
               map.getMap().zoomToContent();
            }
         } 
         else if (locality.getLocality().getPosition().getType()
               .equals(PositionType.MTB))
         {
            drawMTB(locality);
            if (zoomToContent)
            {
               map.getMap().zoomToContent();
               if (locality.getLocality().getPosition().getMtb().getMtb()
                     .length() > 6)
               {
                  map.getMap().setZoomLevel(12);
               } else
               {
                  map.getMap().setZoomLevel(10);
               }
            }
         } 
         else if (locality.getLocality().getPosition().getType()
               .equals(PositionType.SHAPE))
         {
            drawPolygon(locality);
            if(zoomToContent)
            {
               map.getMap().zoomToContent();
            }
         }
      }

   }

   private void drawPolygon(ComponentLocality locality)
   {
      Geometry center = readGeometry(locality.getLocality().getPosition());
      map.addValueComponent(center, locality.getComponent());
   }


   private void drawMTB(ComponentLocality locality)
   {
      MTB mtb = MTBHelper.toMTB(locality.getLocality().getPosition().getMtb().getMtb());
      String mtbWkt = mtb.toWkt();

      Polygon mtbGeometry;
      try
      {
         GeometryHelper geometryHelper = new GeometryHelper();
         mtbGeometry = (Polygon) geometryHelper
               .parseToJts(mtbWkt, Values.MTB_EPSG);
         MapCRSTranslator polygonTranslator = new MapCRSTranslator(Values.PPRESENTATION_EPSG, Values.MTB_EPSG);
         mtbGeometry = (Polygon) polygonTranslator.toPresentation(mtbGeometry);
         map.addValueComponent(mtbGeometry, locality.getComponent());
      } catch (Exception e)
      {
         throw new IllegalStateException(
               "failure translating MTB wkt into Polygon");
      }
   }


   private void drawPoint(ComponentLocality locality)
   {
      Geometry center = readGeometry(locality.getLocality().getPosition());
      map.addValueComponent(center, locality.getComponent());
      if(locality.getLocality().getBlur() != null && locality.getLocality().getBlur() > 0)
      {
         map.addValueComponent(CircleCreator.createCircle((Point) center,
               locality.getLocality().getBlur()), null);
      }
   }


   private Geometry readGeometry(Position value)
   {
      Geometry geometry = null;
      try
      {
         GeometryHelper geometryHelper = new GeometryHelper();
         geometry = geometryHelper
               .parseToJts(value.getWkt(), value.getWktEpsg());
      } catch (ParseException e)
      {
         LOGGER.error("Failure loading geometry", e);
      }
      return geometry;
   }

   private void setupPosition(double lat, double lon, double zoom)
   {
      map.getMap().setCenter(lat, lon);
      map.getMap().setZoomLevel(zoom);
   }


   /**
    * Used to show ONE Location (E.g. OccurrenceDetails)
    */
   @Override
   public void setValue(Locality value)
   {
      ComponentLocality componentLocality = new ComponentLocality(value, null);
      List<ComponentLocality> localities = new ArrayList<ComponentLocality>();
      localities.add(componentLocality);
      setValues(localities, true);
      if(PositionType.POINT.equals(value.getPosition().getType()))
      {
         map.getMap().setZoomLevel(12);
      }
   }


   @Override
   public Locality getValue()
   {
      // TODO Auto-generated method stub
      return null;
   }


   @Override
   public Registration addValueChangeListener(
         ValueChangeListener<Locality> listener)
   {
      return null;
   }

   @Override
   public boolean isReadOnly()
   {
      return super.isReadOnly();
   }

   @Override
   public boolean isRequiredIndicatorVisible()
   {
      return super.isRequiredIndicatorVisible();
   }

   @Override
   public void setReadOnly(boolean readOnly)
   {
   }

   @Override
   public void setRequiredIndicatorVisible(boolean visible)
   {
      super.setRequiredIndicatorVisible(visible);
   }
}
