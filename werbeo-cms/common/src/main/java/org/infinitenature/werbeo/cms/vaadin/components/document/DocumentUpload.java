package org.infinitenature.werbeo.cms.vaadin.components.document;

import java.io.File;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class DocumentUpload
{
   @NotBlank
   private String caption;
   @NotNull
   private File file;

   private String mimeType;
   
   public DocumentUpload(String caption, File file, String mimetype)
   {
      super();
      this.caption = caption;
      this.file = file;
      this.mimeType = mimetype;
   }

   public String getMimeType()
   {
      return mimeType;
   }

   public void setMimeType(String mimeType)
   {
      this.mimeType = mimeType;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   public File getFile()
   {
      return file;
   }

   public void setFile(File file)
   {
      this.file = file;
   }

   @Override
   public String toString()
   {
      return DocumentUploadBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return DocumentUploadBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return DocumentUploadBeanUtil.doEquals(this, obj);
   }
}
