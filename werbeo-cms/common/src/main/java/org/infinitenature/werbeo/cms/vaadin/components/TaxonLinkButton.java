package org.infinitenature.werbeo.cms.vaadin.components;

public class TaxonLinkButton extends LinkButton
{

   public TaxonLinkButton(String caption, ClickListener listener)
   {
      super(caption, listener);
      super.addStyleName("taxon-button");
   }

}
