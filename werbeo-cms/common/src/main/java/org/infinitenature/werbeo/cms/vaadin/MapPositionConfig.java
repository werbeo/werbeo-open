package org.infinitenature.werbeo.cms.vaadin;

public class MapPositionConfig
{
   private Double lastMapPositionLon;
   private Double lastMapPositionLat;
   private Double lastMapZoom;

   public Double getLastMapPositionLon()
   {
      return lastMapPositionLon;
   }

   public void setLastMapPositionLon(Double lastMapPositionLon)
   {
      this.lastMapPositionLon = lastMapPositionLon;
   }

   public Double getLastMapPositionLat()
   {
      return lastMapPositionLat;
   }

   public void setLastMapPositionLat(Double lastMapPositionLat)
   {
      this.lastMapPositionLat = lastMapPositionLat;
   }

   public Double getLastMapZoom()
   {
      return lastMapZoom;
   }

   public void setLastMapZoom(Double lastMapZoom)
   {
      this.lastMapZoom = lastMapZoom;
   }
}