package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.List;
import java.util.stream.Stream;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.PeopleFilter;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.Query;

public class PersonComboBoxDataProvider
      extends AbstractBackEndDataProvider<Person, String>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PersonComboBoxDataProvider.class);
   private final Werbeo werbeo;

   private final Boolean onlyValidators;
   public PersonComboBoxDataProvider(Werbeo werbeo)
   {
      super();
      this.werbeo = werbeo;
      this.onlyValidators = false;
   }


   public PersonComboBoxDataProvider(Werbeo werbeo, Boolean onlyValidators)
   {
      super();
      this.werbeo = werbeo;
      this.onlyValidators = onlyValidators;
   }

   @Override
   protected Stream<Person> fetchFromBackEnd(Query<Person, String> query)
   {
      SortOrder sortOrder = SortOrder.ASC;
      PersonField sortField = PersonField.NAME;

      LOGGER.debug("Fetch - Offset: {}, Limit: {}, Filter: {}",
            query.getOffset(), query.getLimit(), query.getFilter());
      String nameContains = query.getFilter().orElse("");
      List<Person> content = werbeo.people()
            .find(query.getOffset(), query.getLimit(), sortField, sortOrder,
                  new PeopleFilter(getPortalId(), nameContains, null,
                        onlyValidators))
            .getContent();
      LOGGER.debug("Fetch size: {}", content.size());
      return content.stream();
   }

   @Override
   protected int sizeInBackEnd(Query<Person, String> query)
   {
      String nameContains = query.getFilter().orElse("");
      return (int) werbeo.people()
            .count(new PeopleFilter(getPortalId(), nameContains, null,
                  onlyValidators))
            .getAmount();
   }

   protected int getPortalId()
   {
      return Context.getCurrent().getApp().getPortalId();
   }
}
