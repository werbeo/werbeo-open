package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.data.HasValue;
import com.vaadin.ui.Button.ClickListener;

public interface HasValueAndClickListener extends HasValue, ClickListener
{

}
