package org.infinitenature.werbeo.cms.vaadin.i18n;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.vaadin.viritin.button.MButton;

public abstract class I18NButton extends MButton implements I18N
{
   private final MessagesResource messages;

   public I18NButton(MessagesResource messages)
   {
      super();
      this.messages = messages;
      init();
   }

   public I18NButton(MessagesResource messages, ClickListener listener)
   {
      super("", listener);
      this.messages = messages;
      init();
   }

   protected abstract void init();

   @Override
   public void applyLocalizations()
   {
      setCaption(getEntityField("Button", getKey()));
   }

   protected abstract String getKey();

   @Override
   public MessagesResource getMessages()
   {
      return messages;
   }

   @Override
   public void attach()
   {
      super.attach();
      applyLocalizations();
   }

}
