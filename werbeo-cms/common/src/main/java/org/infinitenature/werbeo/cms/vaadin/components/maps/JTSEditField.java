package org.infinitenature.werbeo.cms.vaadin.components.maps;

import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.werbeo.cms.vaadin.components.map.*;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent.Selection;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infintenature.mtb.*;
import org.vaadin.addon.leaflet.*;
import org.vaadin.addon.leaflet.editable.*;
import org.vaadin.addon.leaflet.jsonmodels.VectorStyle;
import org.vaadin.addon.leaflet.util.*;

import com.vaadin.shared.Registration;
import org.locationtech.jts.geom.*;

public class JTSEditField extends AbstractJTSField<Geometry> implements FeatureDrawnListener
{

   private LMarker pointMarker;
   private LPolygon polygonMarker;
   private LPolygon mtbMarker;
   private Registration clickRegistration;
   private Registration drawRegistration;
   private Registration markerRegisration;
   
   private LEditableMap editableMap;
   private LEditable lEditable;
   protected Registration featureDrawnListenerReg;
   
   private LPolygon polygonDrawMarker; 
   
   private PositionType positionType;
   
   private MTBQuadrantSelector mtbQuadrantSelector;
   
   public JTSEditField(MTBQuadrantSelector mtbQuadrantSelector)
   {
      super();
      this.mtbQuadrantSelector = mtbQuadrantSelector;
      clickRegistration = map.addClickListener(editClickListener);
   }
   
   private final LeafletClickListener editClickListener = new LeafletClickListener()
   {
      @Override
      public void onClick(LeafletClickEvent event)
      {
         if (PositionType.POINT.equals(positionType))
         {
            if (pointMarker != null)
            {
               map.removeLayer(pointMarker);
            } 
            org.vaadin.addon.leaflet.shared.Point point = event.getPoint();
            Point pointPresentation = JTSUtil.toPoint(point);
            setValue(getCrsTranslator().toModel(pointPresentation));
            
         }
         else if (PositionType.MTB.equals(positionType))
         {
            org.vaadin.addon.leaflet.shared.Point point = event.getPoint();
            Point pointPresentation = JTSUtil.toPoint(point);
            doSetMTBPolygon(pointPresentation.getX(), pointPresentation.getY(), true, getMTBZoomLevel());
         }
      }
   };
   
   private void doSetMTBPolygon(double easting, double northing, boolean writeBackMTB, double zoomLevel)
   {
      String mtbString = MTBHelper.fromCentroid(easting, northing);

      mtbString =  MTBUtils.processQuadrantSelectiom(mtbString, mtbQuadrantSelector.getSelection());
      
      MTB mtb = MTBHelper.toMTB(mtbString);
      String mtbWkt = mtb.toWkt();

      Polygon mtbGeometry;
      try
      {
         GeometryHelper geometryHelper = new GeometryHelper();
         mtbGeometry = (Polygon) geometryHelper
               .parseToJts(mtbWkt, Values.MTB_EPSG);
         MapCRSTranslator polygonTranslator = new MapCRSTranslator(Values.PPRESENTATION_EPSG, Values.MTB_EPSG);
         mtbGeometry = (Polygon) polygonTranslator.toPresentation(mtbGeometry);
      } catch (Exception e)
      {
         throw new IllegalStateException(
               "failure translating MTB wkt into Polygon");
      }

      if (mtbMarker != null)
      {
         map.removeLayer(mtbMarker);
      } 
      mtbMarker = new LPolygon();
      map.addLayer(mtbMarker);
      mtbMarker.setGeometry(mtbGeometry);
      map.setCenter(JTSUtil.toLeafletPoint(mtbGeometry.getCentroid()), zoomLevel);
      super.setValue(mtbGeometry);
      if(writeBackMTB)
      {
         mtbQuadrantSelector.setMTBString(mtbString);
      }
   }

   private final LMarker.DragEndListener editDragEndListener = new LMarker.DragEndListener()
   {
      @Override
      public void dragEnd(LMarker.DragEndEvent event)
      {
         if (PositionType.POINT.equals(positionType))
         {
            Point pointPresentation = JTSUtil.toPoint(pointMarker);
            setValue(getCrsTranslator().toModel(pointPresentation));
         }
      }
   };
   
   @Override
   protected void prepareEditing(boolean userOriginatedValueChange)
   {
      if(pointMarker != null)
      {
         drawRegistration = pointMarker.addDragEndListener(editDragEndListener);
      }
   }

   public void startPolygon()
   {
      if(featureDrawnListenerReg != null)
      {
         featureDrawnListenerReg.remove();
      }
      this.featureDrawnListenerReg = this.getEditableMap()
            .addFeatureDrawnListener(this);
      this.getEditableMap().startPolygon();
   }
   
   
   @Override
   protected void prepareDrawing()
   {
   }
   
   public void draw(Polygon polygon)
   {
      // remove already drawn polygon
      if(polygonDrawMarker != null)
      {
         map.removeLayer(polygonDrawMarker);
      }
      polygonDrawMarker = JTSUtil.toPolygon(polygon);
      map.addLayer(polygonDrawMarker);
   }

   @Override
   protected void prepareViewing()
   {
      getEditableMap().remove();
      editableMap = null;
      if (lEditable != null) {
          lEditable.remove();
          lEditable = null;
      } 
   }
   
   protected final LEditableMap getEditableMap() {
      if (editableMap == null) {
          editableMap = new LEditableMap(getMap());
      }
      return editableMap;
  }
   
   @Override
   public void setValue(Geometry value)
   {
      removeMarkers();
      if(value != null)
      {
         setCRSTranslator(
            new MapCRSTranslator(Values.PPRESENTATION_EPSG, value.getSRID()));
         value = getCrsTranslator().toPresentation(value);
      }
      
      super.setValue(value);
      if(value instanceof Point)
      {
         pointMarker = new LMarker(JTSUtil.toLeafletPoint((Point)value));
         pointMarker.addDragEndListener(editDragEndListener);
         map.addLayer(pointMarker);
         
         if(map.getZoomLevel() < 12)
         { 
            map.setCenter(pointMarker.getPoint() , 12d);
         }
         else
         {
            map.setCenter((Point)value);   
         }
         
      }
      else if(value instanceof Polygon)
      {
         if(PositionType.SHAPE.equals(positionType))
         {
            VectorStyle vectorStyle = new VectorStyle();
            vectorStyle.setWeight(1);
            vectorStyle.setFill(true);
            vectorStyle.setColor("green");
            vectorStyle.setOpacity(0.5);
            LPolygon lPolygon = JTSUtil.toPolygon((Polygon)value); 
            lPolygon.setStyle(vectorStyle);
            
            
            
            this.lEditable = new LEditable(lPolygon);
            this.lEditable.addFeatureModifiedListener(event -> setValue(
                  getCrsTranslator().toModel(JTSUtil.toPolygon(lPolygon)), true));
            polygonMarker = lPolygon;
            map.addLayer(polygonMarker);
            map.zoomToContent();
            map.setCenter(value.getCentroid());
         }
         else if(PositionType.MTB.equals(positionType))
         {
            doSetMTBPolygon(value.getCentroid().getX(), value.getCentroid().getY(), false, getMTBZoomLevel());
         }
      }
      else if(value instanceof MultiPolygon)
      {
         MultiPolygon multiPolygon = (MultiPolygon)value;
         for(int i = 0; i < multiPolygon.getNumGeometries(); i++)
         {
            map.addLayer(createLayerFromPolygon(multiPolygon.getGeometryN(i)));
            map.zoomToContent();
            map.setCenter(multiPolygon.getCentroid());
         }
      }
      
      if(value == null && positionType.equals(PositionType.SHAPE))
      {
         editableMap = new LEditableMap(getMap());
         startPolygon();
      }
   }

   private LeafletLayer createLayerFromPolygon(Geometry geometry)
   {
      VectorStyle vectorStyle = new VectorStyle();
      vectorStyle.setWeight(1);
      vectorStyle.setFill(true);
      vectorStyle.setColor("green");
      vectorStyle.setOpacity(0.5);
      LPolygon lPolygon = JTSUtil.toPolygon((Polygon)geometry); 
      lPolygon.setStyle(vectorStyle);
      return lPolygon;
   }

   private void removeMarkers()
   {
      if(pointMarker != null)
      {
         map.removeLayer(pointMarker);
      }
      if(polygonMarker != null)
      {
         map.removeLayer(polygonMarker);
      }
      if(mtbMarker != null)
      {
         map.removeLayer(mtbMarker);
      }
   }
   
   private double getMTBZoomLevel()
   {
      double zoomLevel = 10;
      if (Selection.WHOLE_MTB.equals(mtbQuadrantSelector.getSelection())
            || Selection.MTB_QUADRANT_1DIGIT
                  .equals(mtbQuadrantSelector.getSelection()))
      {
         zoomLevel = 10;
      }
      else
      {
         zoomLevel = 12;
      }
      return zoomLevel;
   }

   @Override
   public void featureDrawn(FeatureDrawnEvent event)
   {
      this.setValue(this.getCrsTranslator()
            .toModel(JTSUtil.toPolygon((LPolygon) event.getDrawnFeature())));
      this.featureDrawnListenerReg.remove();
   }

   public void setPositionType(PositionType positionType)
   {
      this.positionType = positionType;
   }
}
