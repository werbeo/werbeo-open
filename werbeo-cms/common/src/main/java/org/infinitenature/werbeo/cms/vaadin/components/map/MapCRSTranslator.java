package org.infinitenature.werbeo.cms.vaadin.components.map;

import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.vaadin.addon.leaflet.util.CRSTranslator;

import org.locationtech.jts.geom.Geometry;

public class MapCRSTranslator implements CRSTranslator
{
   private int epsgPresentation;
   private int epsgModel;
   private CoordinateTransformer transformerToPresentation;
   private CoordinateTransformer transformerToModel;

   public MapCRSTranslator(int epsgPresentation, int epsgModel)
   {
      this.epsgPresentation = epsgPresentation;
      this.epsgModel = epsgModel;

      CoordinateTransformerFactory factory = new CoordinateTransformerFactory();
      transformerToPresentation = factory
            .getCoordinateTransformer(epsgModel, epsgPresentation);
      transformerToModel = factory
            .getCoordinateTransformer(epsgPresentation, epsgModel);
   }

   @Override
   public Geometry toPresentation(Geometry geom)
   {
      if (geom == null)
      {
         return null;
      }
      if (geom.getSRID() == epsgPresentation)
      {
         return geom;
      }

      geom = transformerToPresentation.convert(geom);
      geom.setSRID(epsgPresentation);
      geom.getEnvelope().setSRID(epsgPresentation);
      geom.getCentroid().setSRID(epsgPresentation);
      return geom;
   }

   @Override
   public Geometry toModel(Geometry geom)
   {
      if (geom == null)
      {
         return null;
      }
      if (geom.getSRID() == epsgModel)
      {
         return geom;
      }
      geom = transformerToModel.convert(geom);
      geom.setSRID(epsgModel);
      geom.getEnvelope().setSRID(epsgModel);
      geom.getCentroid().setSRID(epsgModel);
      return geom;
   }
}
