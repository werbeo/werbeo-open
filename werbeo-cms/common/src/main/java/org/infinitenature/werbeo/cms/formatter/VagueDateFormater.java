package org.infinitenature.werbeo.cms.formatter;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.infinitenature.service.v1.types.VagueDate;

public class VagueDateFormater
{

   private VagueDateFormater()
   {
      throw new IllegalAccessError("Utitily class");
   }

   public static String format(VagueDate vagueDate)
   {
      switch (vagueDate.getType())
      {
      case DAY:
         return vagueDate.getTo()
               .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));

      default:
         return vagueDate.getFrom()
               .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM))
               + " - " + vagueDate.getTo().format(
                     DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
      }
   }
}
