package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.*;

import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.werbeo.cms.vaadin.*;
import org.infinitenature.werbeo.cms.vaadin.components.map.*;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

import com.vaadin.shared.Registration;
import com.vaadin.ui.*;

public class CoordinateSystemButton extends I18NButton
      implements HasValueAndClickListener
{
   private Window window;
   private Button selectButton;
   private List<ValueChangeListener> valueChangeListeners = new ArrayList<>();
   private CoordinateSystem value;
   private ComboBox<CoordinateSystem> coordinateSystemComboBox;
   private List<CoordinateSystem> items;


   public CoordinateSystemButton(CoordinateSystem value,
         MessagesResource messages, PortalConfiguration portalConfiguration)
   {
      super(messages);
      selectButton = new Button();
      coordinateSystemComboBox = new ComboBox<>();
      coordinateSystemComboBox.setSizeFull();
      coordinateSystemComboBox
            .setItemCaptionGenerator(CoordinateSystem::getName);
      coordinateSystemComboBox.setValue(Values.START_COORDINATE_SYSTEM);
      coordinateSystemComboBox.setEmptySelectionAllowed(false);

      setItems(map(portalConfiguration.getCoordinateSystems()));
      
      this.value = value;
      addClickListener(this);
      window = new Window();
      window.setWidth("500px");
      window.setHeight("220px");
      window.center();
      window.setModal(true);

      VerticalLayout windowLayout = new VerticalLayout();
      windowLayout.addComponent(coordinateSystemComboBox);
      windowLayout.addComponent(selectButton);
      windowLayout.setMargin(true);
      windowLayout.setSpacing(true);
      window.setContent(windowLayout);

      selectButton.addClickListener(event -> {
         if(!this.value.equals(coordinateSystemComboBox.getValue()))
         {
            this.value = coordinateSystemComboBox.getValue();
            for (ValueChangeListener listener : valueChangeListeners)
            {
               listener.valueChange(new ValueChangeEvent(this, CoordinateSystem.class, true));
            }
         }
         UI.getCurrent().removeWindow(window);
      });
      addStyleName("link");
   }

 
   private List<CoordinateSystem> map(
         List<org.infinitenature.service.v1.types.enums.CoordinateSystem> coordinateSystems)
   {
      List<CoordinateSystem> systems = new ArrayList<CoordinateSystem>();
      coordinateSystems.forEach(apiSystem ->
      {
         systems.add(CoordinateSystem.get(apiSystem));
      });
      return systems;
   }


   @Override
   public void setValue(Object value)
   {
      this.value = (CoordinateSystem)value;
      coordinateSystemComboBox.setValue(this.value);
   }

   @Override
   public CoordinateSystem getValue()
   {
      return value;
   }

   @Override
   public void setRequiredIndicatorVisible(boolean b)
   {

   }

   @Override
   public boolean isRequiredIndicatorVisible()
   {
      return false;
   }

   @Override
   public void setReadOnly(boolean b)
   {
      coordinateSystemComboBox.setEnabled(!b);
      selectButton.setEnabled(!b);
   }

   @Override
   public boolean isReadOnly()
   {
      return !coordinateSystemComboBox.isEnabled();
   }

   @Override
   public Registration addValueChangeListener(
         ValueChangeListener valueChangeListener)
   {
      valueChangeListeners.add(valueChangeListener);
      return new Registration()
      {
         @Override
         public void remove()
         {
            valueChangeListeners.remove(valueChangeListener);
         }
      };
   }

   @Override
   public void buttonClick(ClickEvent clickEvent)
   {
      coordinateSystemComboBox.setValue(value);
      UI.getCurrent().addWindow(window);
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   public void setItems(List<CoordinateSystem> items)
   {
      this.items = items;
      coordinateSystemComboBox.setItems(items);
   }

   public List<CoordinateSystem> getItems()
   {
      return items;
   }

   @Override
   protected void init()
   {

   }

   @Override
   public void applyLocalizations()
   {
      this.setCaption(translate("CHANGE_SYSTEM"));
      selectButton.setCaption(translate("CHOOSE_SYSTEM"));
      window.setCaption(translate("CHANGE_SYSTEM"));
   }

   @Override
   protected String getKey()
   {
      return "";
   }
}
