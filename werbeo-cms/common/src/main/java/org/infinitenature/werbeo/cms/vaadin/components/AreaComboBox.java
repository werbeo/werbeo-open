package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.properties.OccurrenceProperties;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.AreaCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComboBox;

import com.vaadin.ui.UI;

public class AreaComboBox extends I18NComboBox<Occurrence.Area>
{

   public AreaComboBox(MessagesResource messages)
   {
      super(messages);
      setItems(Occurrence.Area.values());
      setValue(null);
      setItemCaptionGenerator(new AreaCaptionGenerator(messages));
   }

   @Override
   public void applyLocalizations()
   {
      setCaption(getEntityField(OccurrenceProperties.AREA));
      setEmptySelectionCaption(
            getMessages().getMessage(Context.getCurrent().getPortal(),
                  "nullSelection", UI.getCurrent().getLocale()));
   }
}
