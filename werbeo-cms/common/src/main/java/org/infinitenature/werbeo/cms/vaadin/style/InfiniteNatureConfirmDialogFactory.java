package org.infinitenature.werbeo.cms.vaadin.style;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.DefaultConfirmDialogFactory;

public class InfiniteNatureConfirmDialogFactory extends
      DefaultConfirmDialogFactory
{
   private final double  MAX_WIDTH = 40D;
   private final double  MIN_WIDTH = 20D;
   private final double MAX_HEIGHT = 40D;
   private final double MIN_HEIGHT = 5D;

   @Override
   protected double[] getDialogDimensions(String message,
         ConfirmDialog.ContentMode style) {

      // Based on Reindeer style:
      double chrW = 0.51d;
      double chrH = 1.5d;
      double length = message != null? chrW * message.length() : 0;
      double rows = Math.ceil(length / MAX_WIDTH);

      // Estimate extra lines
      if (style == ConfirmDialog.ContentMode.TEXT_WITH_NEWLINES) {
         rows += message != null? count("\n", message): 0;
      }

      //System.out.println(message.length() + " = " + length + "em");
      //System.out.println("Rows: " + (length / MAX_WIDTH) + " = " + rows);

      // Obey maximum size
      double width = Math.min(MAX_WIDTH, length);
      double height = Math.ceil(Math.min(MAX_HEIGHT, rows * chrH));

      // Obey the minimum size
      width = Math.max(width, MIN_WIDTH);
      height = Math.max(height, MIN_HEIGHT);

      // Based on Reindeer style:
      double btnHeight = 4d;
      double vmargin = 5d;
      double hmargin = 1d;

      double[] res = new double[] { width + hmargin,
            height + btnHeight + vmargin };
      //System.out.println(res[0] + "," + res[1]);
      return res;
   }


   private int count(final String needle, final String haystack) {
      int count = 0;
      int pos = -1;
      while ((pos = haystack.indexOf(needle, pos + 1)) >= 0) {
         count++;
      }
      return count;
   }
}
