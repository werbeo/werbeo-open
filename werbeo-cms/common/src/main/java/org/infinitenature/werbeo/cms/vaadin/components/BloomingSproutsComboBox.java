package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.BloomingSprouts;
import org.infinitenature.werbeo.cms.vaadin.*;

import com.vaadin.ui.*;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComboBox;

public class BloomingSproutsComboBox extends I18NComboBox<BloomingSprouts>
{
   public BloomingSproutsComboBox(MessagesResource messages)
   {
      super(messages);
      setItems(Occurrence.BloomingSprouts.values());
      setValue(null);
      setEmptySelectionCaption(messages
            .getMessage(Context.getCurrent().getPortal(), "nullSelection",
                  UI.getCurrent().getLocale()));

      setItemCaptionGenerator(
            (ItemCaptionGenerator<BloomingSprouts>) item -> messages
                  .getEntity(null, "BloomingSprouts." + item, getLocale()));
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   @Override
   public void applyLocalizations()
   {
      setCaption(translate("FRUITFUL_SHOOTS"));
   }
}
