package org.infinitenature.werbeo.cms.vaadin.components;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.quarasek.client.representation.JournalRepresentation;
import org.infinitenature.quarasek.client.representation.PublicationRepresentation;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class PublicationField extends CustomComponent
{
   private Quarasek quarasekClient;
   private String citeId;
   private String comment;
   private PublicationRepresentation publication;
   

   Label authorLabel = new Label();
   Label titleLabel = new Label();
   Label journalLabel = new Label();
   Label publicationCommentLabel = new Label();
   
   public void setQuarasekClient(Quarasek quarasekClient)
   {
      this.quarasekClient = quarasekClient;
   }
   
   
   public void setPublication(String citeId, String comment)
   {
      this.citeId = citeId;
      this.comment = comment;
      if(quarasekClient != null)
      {
         publication = quarasekClient.publications().getByCiteId(citeId);
      }
      updateLabel();
   }
   
   private void updateLabel()
   {
      reset();
      if(publication != null)
      {
         authorLabel.setValue(publication.getAuthors().get(0).getLastName()
               + ", " + publication.getAuthors().get(0).getFirstName() + " ("
               + publication.getYear() + ")");
         titleLabel.setValue(publication.getTitle());
         if(publication.getJournal() != null)
         {
            journalLabel.setValue(pretty(publication.getJournal()));
         }
      }
      else
      {
         authorLabel.setValue("CiteId: " + citeId);
      }
      
      if(StringUtils.isNotBlank(comment))
      {
         publicationCommentLabel.setValue(comment);
      }
   }


   private String pretty(JournalRepresentation journal)
   {
      return journal.getName() + "; " + journal.getVolume() + "; " + journal.getPages();
   }


   private void reset()
   {
      authorLabel.setValue("");
      titleLabel.setValue("");
      journalLabel.setValue("");
      publicationCommentLabel.setValue("");
   }


   public PublicationField()
   {
      VerticalLayout layout = new VerticalLayout(authorLabel, titleLabel, journalLabel,
            publicationCommentLabel);
      setCompositionRoot(layout);
   }
   
   
}
