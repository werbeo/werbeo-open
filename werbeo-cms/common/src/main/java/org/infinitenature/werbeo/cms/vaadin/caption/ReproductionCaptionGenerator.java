package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class ReproductionCaptionGenerator
      extends I18NCaptionGenerator<Occurrence.Reproduction>
{

   public ReproductionCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
