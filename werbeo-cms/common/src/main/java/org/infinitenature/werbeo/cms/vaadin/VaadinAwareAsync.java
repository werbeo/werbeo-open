package org.infinitenature.werbeo.cms.vaadin;

import java.util.concurrent.CompletableFuture;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

/**
 * Utility class for async task which need the {@link UI} and
 * {@link VaadinSession}.
 *
 */
public class VaadinAwareAsync
{
   private VaadinAwareAsync()
   {
      throw new IllegalAccessError("Utility class");
   }

   /**
    * Runs a {@link Runnable} with {@link VaadinSession} and {@link UI}
    * available by {@link UI#getCurrent()} and
    * {@link VaadinSession#getCurrent()}
    */
   public static void runAsync(Runnable runnable)
   {
      VaadinRunnable vaadinRunnable = new VaadinRunnable(runnable);
      CompletableFuture.runAsync(vaadinRunnable);
   }

   public static class VaadinRunnable implements Runnable
   {
      final UI ui = UI.getCurrent();
      final VaadinSession session = VaadinSession.getCurrent();
      private Runnable runnable;

      public VaadinRunnable(Runnable runnable)
      {
         this.runnable = runnable;

      }

      @Override
      public void run()
      {
         UI.setCurrent(ui);
         VaadinSession.setCurrent(session);
         runnable.run();

      }

   }
}
