package org.infinitenature.werbeo.cms.vaadin.components.button;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

import com.vaadin.icons.VaadinIcons;

public class DeleteButton extends I18NButton
{

   public DeleteButton(MessagesResource messages, ClickListener listener)
   {
      super(messages, listener);
   }

   public DeleteButton(MessagesResource messages)
   {
      super(messages);
   }

   @Override
   protected void init()
   {
      setIcon(VaadinIcons.TRASH);
   }

   @Override
   protected String getKey()
   {
      return "DELETE";
   }

}
