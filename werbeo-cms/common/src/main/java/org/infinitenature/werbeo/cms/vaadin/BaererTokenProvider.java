package org.infinitenature.werbeo.cms.vaadin;

public interface BaererTokenProvider
{
   public String getCurrentToken();
}
