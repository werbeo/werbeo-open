package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LWmsLayer;

public class LMVTopoLayer extends LWmsLayer implements Layer
{
   private LayerName name;
   private boolean active;

   LMVTopoLayer()
   {
      setDescription("MV-Topo");
      setUrl("https://www.geodaten-mv.de/dienste/gdimv_dtk");
      setFormat("image/png");
      setLayers("mv_dtk");
      // does not cover whole world
      setTransparent(true);
      setActive(false);
      setEnabled(true);
      name = LayerName.TOPO_MV;
      setCaption(LayerName.TOPO_MV.getCaption());
   }

   @Override
   public boolean isActive()
   {
      return active;
   }

   @Override
   public void setActive(boolean active)
   {
      setActiveIndicator(active);
      super.setActive(active);
   }

   @Override
   public void setActiveIndicator(boolean active)
   {
      this.active = active;
   }

   @Override
   public LayerName getName()
   {
      return name;
   }

   @Override
   public Layer clone()
   {
      return new LMVTopoLayer();
   }
}
