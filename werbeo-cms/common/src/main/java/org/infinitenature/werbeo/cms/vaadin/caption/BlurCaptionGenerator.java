package org.infinitenature.werbeo.cms.vaadin.caption;

import java.util.Locale;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class BlurCaptionGenerator extends I18NCaptionGenerator<Integer>
{

   public BlurCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

   @Override
   public String translate(Integer item, Portal portal, Locale locale)
   {
      return getMessages().getMessage(portal, "blur", locale, item);
   }
}
