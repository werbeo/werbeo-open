package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;

public class HorizontalRule extends Label
{

   private static final long serialVersionUID = -4843934314563516957L;

   public HorizontalRule()
   {
      super("<hr>", ContentMode.HTML);
      setWidth("100%");
   }
}
