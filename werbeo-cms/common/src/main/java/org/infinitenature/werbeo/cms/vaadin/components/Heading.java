package org.infinitenature.werbeo.cms.vaadin.components;

import org.vaadin.viritin.label.Header;

public class Heading extends Header
{
   public Heading(String caption)
   {
      super(caption);
      setHeaderLevel(2);
   }

   public Heading()
   {
      super("");
      setHeaderLevel(2);
   }
}
