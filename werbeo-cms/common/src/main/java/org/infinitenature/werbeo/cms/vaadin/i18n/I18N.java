package org.infinitenature.werbeo.cms.vaadin.i18n;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.infinitenature.werbeo.cms.properties.EntityEnum;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.notification.NotificationUtils;

import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

public interface I18N extends Component
{

   void applyLocalizations();

   public MessagesResource getMessages();

   default String getEntityField(EntityEnum field)
   {
      return getEntityField(field.getEntityName(), field.getFieldName());
   }

   default String getEntityField(String entity, String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            entity, field, UI.getCurrent().getLocale());
   }

   default DecimalFormat getDecimalFormat()
   {
      NumberFormat numberFormat = NumberFormat.getNumberInstance(getLocale());
      ((DecimalFormat) numberFormat).applyPattern("###,###,###.##");
      return (DecimalFormat) numberFormat;
   }

   default void showErrorMessage(String key, Object... paramter)
   {
      String caption = getMessages().getMessage(
            Context.getCurrent().getPortal(), key + ".caption", getLocale(),
            paramter);
      String message = getMessages().getMessage(
            Context.getCurrent().getPortal(), key + ".message", getLocale(),
            paramter);
      NotificationUtils.showError(caption, message);
   }
}
