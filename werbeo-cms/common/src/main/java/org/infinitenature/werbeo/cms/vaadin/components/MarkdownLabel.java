package org.infinitenature.werbeo.cms.vaadin.components;

import org.vaadin.viritin.label.RichText;

public class MarkdownLabel extends RichText
{
   
   @Override
   public void setValue(String newStringValue)
   {
    withMarkDown(newStringValue);
   }
}
