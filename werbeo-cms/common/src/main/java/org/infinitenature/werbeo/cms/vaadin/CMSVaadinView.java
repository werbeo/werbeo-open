package org.infinitenature.werbeo.cms.vaadin;

import com.vaadin.ui.Component;

public interface CMSVaadinView extends Component
{
   public default Component getVaadinComponent()
   {
      return this;
   }
}
