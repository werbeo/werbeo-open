package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class IAMapFilter
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(IAMapFilter.class);
   private TaxonBase taxon;
   private SurveyBase survey;
   private Integer maxBlur;
   private Person validator;
   private ValidationStatus validationStatus;

   public TaxonBase getTaxon()
   {
      return taxon;
   }

   public void setTaxon(TaxonBase taxon)
   {
      this.taxon = taxon;
   }

   public SurveyBase getSurvey()
   {
      return survey;
   }

   public void setSurvey(SurveyBase survey)
   {
      this.survey = survey;
   }

   public Integer getMaxBlur()
   {
      return maxBlur;
   }

   public void setMaxBlur(Integer maxBlur)
   {
      this.maxBlur = maxBlur;
   }

   public Person getValidator()
   {
      return validator;
   }

   public void setValidator(Person validator)
   {
      this.validator = validator;
   }

   public ValidationStatus getValidationStatus()
   {
      return validationStatus;
   }

   public void setValidationStatus(ValidationStatus validationStatus)
   {
      this.validationStatus = validationStatus;
   }

   @Override
   public String toString()
   {
      return IAMapFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return IAMapFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return IAMapFilterBeanUtil.doEquals(this, obj);
   }

   public String getCQL(String token)
   {

      Set<String> params = new HashSet<>();

      if (StringUtils.isNotBlank(token))
      {
         try
         {
            params.add("bearerToken='"
                  + URLEncoder.encode(token, StandardCharsets.UTF_8.toString())
                  + "'");
         } catch (UnsupportedEncodingException e)
         {
            LOGGER.error("Failure doing urlencoding with utf-8", e);
         }
      }
      if (taxon != null)
      {
         params.add("taxon=" + taxon.getEntityId());
      }
      if (survey != null)
      {
         params.add("surveyId=" + survey.getEntityId());
      }
      if (maxBlur != null)
      {
         params.add("maxBlur=" + maxBlur);
      }
      if (validationStatus != null)
      {
         params.add("validationStatus=" + validationStatus);
      }
      if (validator != null)
      {
         params.add("validator=" + validator.getEmail());
      }
      return StringUtils.join(params, " AND ");
   }
}
