package org.infinitenature.werbeo.cms.vaadin.components.form;

import java.io.Serializable;

import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface FormView<T extends Base<?>> extends CMSVaadinView
{
   public interface FormObsever<T extends Base<?>> extends Serializable
   {
      void onSave(T entity);

      void onDelete(T entity);
   }

   void reset();

   void success(T entity);

   void failure(T entity);

   void setEntity(T entity);
}
