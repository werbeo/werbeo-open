package org.infinitenature.werbeo.cms.vaadin.components.indexherbariorum;

import java.util.Locale;

import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.herbariorum.entities.Institution;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

public class HerbariorumField extends CustomField<Institution>
{
   private ComboBox<String> countryComboBox;
   private ComboBox<Institution> institutionComboBox;
   private MessagesResource messagesResource;
   private HerbariorumClient client;
   private HorizontalLayout layout;

   public HerbariorumField(MessagesResource messagesResource)
   {
      this.messagesResource = messagesResource;
   }

   public void init(HerbariorumClient client)
   {
      this.client = client;
      layout = new HorizontalLayout();
      layout.setSpacing(true);
      layout.setMargin(false);
      if (!client.isServiceAvailable())
      {
         if(!client.initCache())
         {
            layout.addComponent(new Label(translate("NOT_AVAILABLE")));
            return;
         }
      }

      String presetCountry = translate("PRESET_COUNTRY");
      if (!this.client.isValidCountry(presetCountry))
      {
         throw new IllegalArgumentException(
               "Herbariorum Field: Preset country is not valid");
      }

      countryComboBox = new ComboBox<>(translate("COUNTRY"));
      countryComboBox.setWidth("200px");
      countryComboBox.setDataProvider(
            new CountryComboBoxDataProvider(client, presetCountry));
      countryComboBox.setValue(presetCountry);
      countryComboBox.setEmptySelectionAllowed(false);

      institutionComboBox = new ComboBox<>(translate("INSTITUTION"));
      institutionComboBox.setWidth("600px");
      institutionComboBox.setDataProvider(
            new InstitutionComboBoxDataProvider(client, presetCountry,
                  countryComboBox));
      institutionComboBox
            .setEmptySelectionCaption(translate("EMPTY_SELECTION_CAPTION"));
      institutionComboBox.setPopupWidth("auto");

      countryComboBox.addValueChangeListener(event -> {
         institutionComboBox.getDataProvider().refreshAll();
         institutionComboBox.setValue(null);
      });

      institutionComboBox.addValueChangeListener(event -> {
         if (institutionComboBox.getValue() != null)
         {
            institutionComboBox
                  .setDescription(institutionComboBox.getValue().toString());
         }
      });

      Label label = new Label();
      label.setCaptionAsHtml(true);
      label.setCaption(translate("INDEX_HERBARIORUM"));

      layout.addComponents(countryComboBox, institutionComboBox, label);
   }

   
   @Override
   protected Component initContent()
   {
      return layout;
   }

   public void setValueByCode(String code)
   {
      if (client.isServiceAvailable())
      {
         setValue(client.getByCode(code));
      }
   }

   @Override
   protected void doSetValue(Institution institution)
   {
      if (client.isServiceAvailable())
      {
         countryComboBox.setValue(institution.getAddress().getPostalCountry());
         institutionComboBox.setValue(institution);
      }
   }

   @Override
   public Institution getValue()
   {
      if (client != null && client.isServiceAvailable())
      {
         return institutionComboBox.getValue();
      }
      return null;
   }

   private String translate(String string)
   {
      return messagesResource.getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), string, Locale.getDefault());
   }



   public HerbariorumClient getClient()
   {
      return client;
   }
}
