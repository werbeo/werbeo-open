package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Resource;
import com.vaadin.ui.Button;

public class LinkButton  extends Button
{
   public LinkButton(String caption, Button.ClickListener listener) {
      super(caption, listener);
      init();
   }

   protected void init()
   {
      setStyleName("link");
   }

   public LinkButton()
   {
      super();
      init();
   }

   public LinkButton(Resource icon, ClickListener listener)
   {
      super(icon, listener);
      init();
   }

   public LinkButton(Resource icon)
   {
      super(icon);
      init();
   }

   public LinkButton(String caption, Resource icon)
   {
      super(caption, icon);
   }

   public LinkButton(String caption)
   {
      super(caption);
      init();
   }

   public LinkButton(String caption, VaadinIcons icon,
         Button.ClickListener listener)
   {
      super(caption, icon);
      this.addClickListener(listener);
      init();
   }
}
