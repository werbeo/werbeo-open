package org.infinitenature.werbeo.cms.vaadin;

import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.Occurrence.Amount;
import org.infinitenature.service.v1.types.VagueDate;

public class Preselection
{
   private Locality locality;
   private VagueDate date;
   private String habitat;
   private String observers;
   private Amount amount;
   
   public VagueDate getDate()
   {
      return date;
   }
   public void setDate(VagueDate date)
   {
      this.date = date;
   }
   
   public Locality getLocality()
   {
      return locality;
   }
   public void setLocality(Locality locality)
   {
      this.locality = locality;
   }
   public String getHabitat()
   {
      return habitat;
   }
   public void setHabitat(String habitat)
   {
      this.habitat = habitat;
   }
   public String getObservers()
   {
      return observers;
   }
   public void setObservers(String observers)
   {
      this.observers = observers;
   }
   public Amount getAmount()
   {
      return amount;
   }
   public void setAmount(Amount amount)
   {
      this.amount = amount;
   }
}
