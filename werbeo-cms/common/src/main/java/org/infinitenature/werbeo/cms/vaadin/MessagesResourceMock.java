package org.infinitenature.werbeo.cms.vaadin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@Service
public class MessagesResourceMock implements MessagesResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(MessagesResourceMock.class);
   private static final String BUNDLE_NAME = "messages";

   @Override
   public String getMessage(Portal portal, String key, Locale locale,
         Object... parameter)
   {
      return new MessageFormat(getString("message." + key, locale, portal), locale)
            .format(parameter);
   }

   @Override
   public String getErrorMessage(Portal portal, Locale locale, Throwable e,
         Object... parameter)
   {
      return getMessage(portal, "error." + e.getClass().getName(), locale,
            parameter);
   }

   @Override
   public String getEntity(Portal portal, String entity, Locale locale)
   {
      return getString(entity, locale, portal);
   }

   @Override
   public String getEntityField(Portal portal, String entity, String field,
         Locale locale)
   {
      return getString(entity + "." + field, locale, portal);
   }

   private String getString(String key, Locale locale, Portal portal)
   {
      if (portal != null && ResourceBundle.getBundle(BUNDLE_NAME, locale)
            .containsKey(key + "." + portal.getPortalId()))
      {
         return ResourceBundle.getBundle(BUNDLE_NAME, locale)
               .getString(key + "." + portal.getPortalId());
      }

      try
      {
         if (locale != null)
         {
            if (!(locale.equals(Locale.GERMANY) || locale.equals(Locale.GERMAN))
                  && ResourceBundle.getBundle(BUNDLE_NAME, locale)
                  .containsKey(key))
            {
               return ResourceBundle.getBundle(BUNDLE_NAME, locale)
                     .getString(key);
            }
            return ResourceBundle.getBundle(BUNDLE_NAME, Locale.GERMANY).getString(key);
         }
         else
         {
            return defaultString(key, locale);
         }
      }
      catch (MissingResourceException e)
      {
         return defaultString(key, locale);
      }
   }

   private String defaultString(String key, Locale locale)
   {
      LOGGER.warn(
            "The key \"{}\" is not in the resource bundel for the locale \"{}\".",
            key, locale);
      return '!' + key + '!';
   }
}
