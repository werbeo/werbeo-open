package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.Locale;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18N;

public class MarkDownField extends VMarkDownField implements I18N
{
   private final MessagesResource messages;

   public MarkDownField(MessagesResource messages)
   {
      super();
      this.messages = messages;
   }

   @Override
   public MessagesResource getMessages()
   {
      return messages;
   }

   @Override
   protected boolean setValue(String value, boolean userOriginated)
   {
      if (value != null)
      {
         return super.setValue(value, userOriginated);
      } else
      {
         return super.setValue("", userOriginated);
      }
   }

   @Override
   public void attach()
   {
      super.attach();
      applyLocalizations();
   }

   @Override
   public void applyLocalizations()
   {
      Portal portal = Context.getCurrent().getPortal();
      Locale locale = getLocale();
      setEditCaption(getMessages().getMessage(portal, "general.edit", locale));
      setPreviewCaption(
            getMessages().getMessage(portal, "general.preview", locale));
   }

}
