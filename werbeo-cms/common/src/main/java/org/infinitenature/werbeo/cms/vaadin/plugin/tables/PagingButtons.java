package org.infinitenature.werbeo.cms.vaadin.plugin.tables;

import java.util.Locale;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.teemusa.gridextensions.paging.PagedDataProvider;
import org.vaadin.teemusa.gridextensions.paging.PagingControls;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.fields.IntegerField;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.provider.DataChangeEvent;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;

public class PagingButtons extends I18NComposite
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(PagingButtons.class);

   private PagedDataProvider<?, ?> dataProvider;
   private PagingControls controls;
   private MButton firstPage = new MButton();
   private MButton lastPage = new MButton();
   private IntegerField pageNo = new IntegerField().withValue(1)
         .withValueChangeListener(this::onPageRequested);
   private Label pages = new Label();
   private MButton nextPage = new MButton();
   private MButton prevPage = new MButton();
   private Registration registration;

   public PagingButtons(MessagesResource messages)
   {
      super(messages);
      firstPage.addClickListener(e -> onFirstPageClick(e));
      prevPage.addClickListener(e -> onPrevPageClick(e));
      nextPage.addClickListener(e -> onNextPageClick(e));
      lastPage.addClickListener(e -> onLastPageClick(e));
      setCompositionRoot(new MHorizontalLayout(firstPage, prevPage, pageNo,
            pages, nextPage, lastPage));
   }

   private void onPageRequested(ValueChangeEvent<Integer> e)
   {
      if (controls != null && e.getValue() != controls.getPageNumber() + 1)
      {
         controls.setPageNumber(e.getValue() - 1);
      }
   }

   private void onLastPageClick(ClickEvent e)
   {
      if (controls != null)
      {
         controls.setPageNumber(controls.getPageCount() - 1);
      }
   }

   private void onNextPageClick(ClickEvent e)
   {
      if (controls != null)
      {
         controls.nextPage();
      }
   }

   private void onPrevPageClick(ClickEvent e)
   {
      if (controls != null)
      {
         controls.previousPage();
      }
   }

   private void onFirstPageClick(ClickEvent e)
   {
      if (controls != null)
      {
         controls.setPageNumber(0);
      }
   }

   public PagingControls getControls()
   {
      return controls;
   }

   public void setDataProvider(PagedDataProvider<?, ?> dataProvider)
   {
      if (registration != null)
      {
         registration.remove();
      }
      this.dataProvider = dataProvider;
      this.registration = dataProvider
            .addDataProviderListener(this::onDataChange);
      this.controls = dataProvider.getPagingControls();
   }

   private void onDataChange(DataChangeEvent<?> event)
   {
      try
      {
         nextPage.setEnabled(controls.hasNextPage());
         prevPage.setEnabled(controls.hasPreviousPage());
         lastPage.setEnabled(!controls.isLastPage());
         firstPage.setEnabled(!controls.isFirstPage());
         pageNo.setValue(controls.getPageNumber() + 1);
         // also show one page if there is no data
         pages.setValue(" / " + Math.max(controls.getPageCount(), 1));
      } catch (Exception e)
      {
         LOGGER.error("Failure updateding pageing controls", e);
         throw e;
      }
   }

   @Override
   public void applyLocalizations()
   {
      Portal portal = Context.getCurrent().getPortal();
      Locale locale = getLocale();
      MessagesResource messages = getMessages();
      firstPage.setCaption(messages.getMessage(portal, "paging.first", locale));
      lastPage.setCaption(messages.getMessage(portal, "paging.last", locale));
      nextPage.setCaption(messages.getMessage(portal, "paging.next", locale));
      prevPage.setCaption(messages.getMessage(portal, "paging.prev", locale));
   }

}
