package org.infinitenature.werbeo.cms.vaadin.components.document;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.lang3.Validate;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.HasAllowedOperations;
import org.infinitenature.werbeo.cms.vaadin.components.button.DeleteButton;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.infinitenature.werbeo.common.commons.WerbeoStringUtils;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Link;

public class DocumentField extends I18NField<Document>
      implements HasAllowedOperations
{

   private MLabel caption = new MLabel().withFullWidth().withUndefinedHeight();
   private Component media = new Label();
   private DeleteButton deleteButton;
   private final Layout layout;
   private Document document;
   private boolean withCaptions = true;
   private Collection<Operation> allowedOperations = new HashSet<>();
   private DocumentsField deleteDelegate;

   public DocumentField(MessagesResource messages)
   {
      super(messages);
      deleteButton = new DeleteButton(messages, this::onDelete);
      layout = new MHorizontalLayout(deleteButton, caption, media)
            .withDefaultComponentAlignment(Alignment.MIDDLE_LEFT)
            .withComponentAlignment(media, Alignment.MIDDLE_RIGHT)
            .withExpandRatio(caption,
                  2.f)
            .withExpandRatio(media, 4.5f).withFullWidth();
   }

   public DocumentField(MessagesResource messages, Document document)
   {
      this(messages);
      doSetValue(document);
   }

   private void onDelete(ClickEvent event)
   {
      if (deleteDelegate != null)
      {
         deleteDelegate.onDelete(document);
      }
   }

   @Override
   protected Component initContent()
   {
      return layout;
   }

   @Override
   public void applyLocalizations()
   {
      if (withCaptions)
      {
         this.caption.setCaption("Beschreibung");
         this.media.setCaption("Inhalt");
      }
   }

   @Override
   public Document getValue()
   {
      return null;
   }

   @Override
   public boolean isReadOnly()
   {
      return true;
   }

   @Override
   protected void doSetValue(Document value)
   {
      Validate.notNull(value, "The document must not be null");
      this.document = value;

      caption.setValue(document.getCaption());

      Component newMedia = createMedia();
      layout.replaceComponent(media, newMedia);
      media = newMedia;
      applyAllowedOperations();
   }

   private Component createMedia()
   {
      
      String urlEncoded = WerbeoStringUtils.encodeFileAndParams(document.getLink().getHref());
      
      switch (document.getType())
      {
      case AUDIO:
         Audio audio = new Audio();
         audio.setAltText(document.getCaption());
         audio.setSource(new ExternalResource(urlEncoded));
         audio.setWidth("300px");
         return audio;
      case IMAGE:
         Image image = new Image();
         image.setHeight("150px");
         image.setSource(new ExternalResource(urlEncoded));
         image.setAlternateText(document.getCaption());
         BrowserWindowOpener bwo = new BrowserWindowOpener(
               urlEncoded);
         bwo.setWindowName("_blank");
         bwo.extend(image);
         return image;
      default:
         return new Link("Download",
               new ExternalResource(document.getLink().getHref()));
      }

   }

   public DocumentField withWidth(String width)
   {
      setWidth(width);
      return this;
   }

   public void setWithCaption(boolean withCaptions)
   {
      this.withCaptions = withCaptions;
   }

   @Override
   public void setAllowedOperations(Collection<Operation> allowedOperations)
   {
      this.allowedOperations = allowedOperations;
      applyAllowedOperations();
   }

   private void applyAllowedOperations()
   {
      deleteButton.setVisible(allowedOperations.contains(Operation.UPDATE));
   }

   public void setDeleteDelegate(DocumentsField documentsField)
   {
      this.deleteDelegate = documentsField;
   }

}
