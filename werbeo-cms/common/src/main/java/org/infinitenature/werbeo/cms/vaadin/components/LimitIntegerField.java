package org.infinitenature.werbeo.cms.vaadin.components;

import org.vaadin.viritin.fields.IntegerField;

public class LimitIntegerField extends IntegerField
{
   private Integer max;
   private Integer min;
   private Integer step;

   public Integer getMax()
   {
      return max;
   }

   public void setMax(Integer max)
   {
      this.max = max;
   }

   public Integer getMin()
   {
      return min;
   }

   public void setMin(Integer min)
   {
      this.min = min;
   }

   public Integer getStep()
   {
      return step;
   }

   public void setStep(Integer step)
   {
      this.step = step;
   }

   @Override
   protected void configureHtmlElement()
   {
      super.configureHtmlElement();
      if (max != null)
      {
         s.setProperty("max", max.toString());
      }
      if (min != null)
      {
         s.setProperty("min", min.toString());
      }
      if (step != null)
      {
         s.setProperty("step", step.toString());
      }
   }
}
