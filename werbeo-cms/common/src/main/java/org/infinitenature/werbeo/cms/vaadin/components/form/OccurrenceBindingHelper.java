package org.infinitenature.werbeo.cms.vaadin.components.form;

import java.util.HashMap;
import java.util.Set;

import org.infinitenature.service.v1.types.meta.OccurrenceField;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.werbeo.cms.vaadin.fieldconfig.FieldConfigUtils;

public class OccurrenceBindingHelper extends BindingHelper<OccurrenceField>
{

   public OccurrenceBindingHelper(
         Set<OccurrenceFieldConfig> occurrenceFieldConfigs)
   {
      this.fieldConfigs = FieldConfigUtils
            .toOccurrenceConfigMap(occurrenceFieldConfigs);
      this.fieldMapping = new HashMap<>();
   }

   @Override
   protected String getEntityKey()
   {
      return "Occurrence";
   }

}
