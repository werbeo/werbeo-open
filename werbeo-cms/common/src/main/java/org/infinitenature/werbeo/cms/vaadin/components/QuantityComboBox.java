package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.UI;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.QuantityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComboBox;

public class QuantityComboBox extends I18NComboBox<Occurrence.Quantity>
{

   public QuantityComboBox(MessagesResource messages)
   {
      super(messages);
      setItems(Occurrence.Quantity.values());
      setValue(null);
      setItemCaptionGenerator(new QuantityCaptionGenerator(messages));
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   @Override
   public void applyLocalizations()
   {
      setCaption(translate("AMOUNT"));
      setEmptySelectionCaption(translate("EMPTY"));
   }
}
