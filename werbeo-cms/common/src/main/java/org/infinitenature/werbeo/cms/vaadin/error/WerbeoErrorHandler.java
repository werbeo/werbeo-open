package org.infinitenature.werbeo.cms.vaadin.error;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;

import org.infinitenature.vct.error.NotLoggedInException;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.NavigationStateCookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.Page;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@Component
public class WerbeoErrorHandler extends DefaultErrorHandler
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(WerbeoErrorHandler.class);
   @Autowired
   private MessagesResource messagesResource;

   @Override
   public void error(com.vaadin.server.ErrorEvent event)
   {
      Throwable throwable = findRelevantThrowable(event.getThrowable());
      Response response = null;
//      if (throwable instanceof WebApplicationException)
//      {
//         response = ((WebApplicationException) throwable).getResponse();
//         response.bufferEntity();
//      }
      if (throwable instanceof NotLoggedInException)
      {
         NavigationStateCookie.saveNavigationState();
         Page.getCurrent().setLocation("/sso/login");
      } else if (throwable instanceof ForbiddenException)
      {
         if (Context.getCurrent().getAuthentication().isAnonymous())
         {
            NavigationStateCookie.saveNavigationState();
            Page.getCurrent().setLocation("/sso/login");
         } else
         {
            Notification.show(
                  messagesResource.getErrorMessage(
                        Context.getCurrent().getPortal(),
                        UI.getCurrent().getLocale(), throwable),
                  Notification.Type.ERROR_MESSAGE);
         }
//      } else if (throwable instanceof BadRequestException)
//      {
//         String message = null;
//         try
//         {
//            FailureResponse body = response.readEntity(FailureResponse.class);
//            message = body.getCause();
//         } catch (ProcessingException pe)
//         {
//            message = getMessages().getErrorMessage(
//                  Context.getCurrent().getPortal(), UI.getCurrent().getLocale(),
//                  throwable);
//         }
//         Notification.show(message, Notification.Type.ERROR_MESSAGE);
//         LOGGER.info("Bad request: {}", message, throwable);
      } else
      {
         super.error(event);
      }
   }


}