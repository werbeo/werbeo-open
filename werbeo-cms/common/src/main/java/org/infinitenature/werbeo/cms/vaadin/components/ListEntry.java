package org.infinitenature.werbeo.cms.vaadin.components;

public class ListEntry
{
   String text;

   public ListEntry()
   {

   }


   public ListEntry(String text)
   {
      this.text = text;
   }

   public String getText()
   {
      return text;
   }

   public void setText(String text)
   {
      this.text = text;
   }
}
