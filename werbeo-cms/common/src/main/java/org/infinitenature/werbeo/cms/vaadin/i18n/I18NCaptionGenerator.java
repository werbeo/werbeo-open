package org.infinitenature.werbeo.cms.vaadin.i18n;

import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang3.Validate;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.caption.WerbeoCaptionGenerator;

import com.vaadin.ui.UI;

public abstract class I18NCaptionGenerator<T>
      implements WerbeoCaptionGenerator<T>
{

   private final MessagesResource messagesResource;
   private final Optional<String> nullSelectionCaption;

   public I18NCaptionGenerator(MessagesResource messagesResource)
   {
      this.messagesResource = messagesResource;
      this.nullSelectionCaption = Optional.empty();
   }

   public I18NCaptionGenerator(MessagesResource messagesResource,
         String nullSelectionCaption)
   {
      Validate.notNull(nullSelectionCaption,
            "Null selection caption may not be null");
      this.messagesResource = messagesResource;
      this.nullSelectionCaption = Optional.of(nullSelectionCaption);
   }

   @Override
   public String apply(T item)
   {
      Portal portal = Context.getCurrent().getPortal();
      Locale locale = UI.getCurrent().getLocale();
      if (item != null)
      {
         return translate(item, portal, locale);
      } else
      {
         return nullSelectionCaption.isPresent() ? nullSelectionCaption.get()
               : messagesResource.getMessage(portal, "nullSelection", locale);
      }
   }

   public String translate(T item, Portal portal, Locale locale)
   {
      return messagesResource.getEntity(portal,
            item.getClass().getSimpleName() + "." + item, locale);
   }

   public MessagesResource getMessages()
   {
      return messagesResource;
   }
}
