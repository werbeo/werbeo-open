package org.infinitenature.werbeo.cms.vaadin.components.maps;

public class CoordinateSplitter
{
   public static double[] split(String coordinateString)
   {
      try
      {
      if(coordinateString == null)
      {
         return new double[0];
      }
      
      String latString = coordinateString.split(" ")[0];
      String lonString = coordinateString.split(" ")[1];
      if (lonString == null || lonString.isEmpty())
      {
         throw new IllegalStateException();
      }
      
      return new double[] { Double.valueOf(latString.replaceAll(",", ".")),
            Double.valueOf(lonString.replaceAll(",", ".")) };
      }
      catch(Exception e)
      {
         return new double[0];
      }
   }
 }
