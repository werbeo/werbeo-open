package org.infinitenature.werbeo.cms.vaadin.components.maps;

import java.io.File;

import org.geotools.data.shapefile.files.ShpFiles;
import org.geotools.data.shapefile.shp.ShapefileReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.WKTWriter;

public class ShapeUtils
{
   private static final Logger LOGGER = LoggerFactory.getLogger(ShapeUtils.class);
   
   public static String shapeToWkt(File file)
   {
      try
      {
         ShpFiles files = new ShpFiles(file);
         ShapefileReader r = new ShapefileReader(files, false, true,
               new GeometryFactory());
         Geometry combined = null;
         while (r.hasNext())
         {
            Geometry shape = (Geometry) r.nextRecord().shape(); // do stuff }

            if (combined == null)
            {
               combined = shape;
            } else
            {
               combined = combined.union(shape);
            }

         }

         WKTWriter wktWriter = new WKTWriter();

         r.close();
         return (wktWriter.write(combined));
      } catch (Exception e)
      {
         LOGGER.error("Error parsing wkt from shape-file",e);
         return null;
      }
   }
}
