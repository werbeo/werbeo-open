package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LWmsLayer;

public class LSatelliteLayer extends LWmsLayer implements Layer
{
   private LayerName name;
   private boolean active;

   LSatelliteLayer()
   {
      setDescription("Sattellite");
      setUrl("https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}");
      setAttributionString("Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community");
//      setFormat("image/png");
//      setLayers("mv_dtk");
      // does not cover whole world
//      setTransparent(true);
      setActive(false);
      setEnabled(true);
      name = LayerName.SATELLITE;
      setCaption(LayerName.SATELLITE.getCaption());
   }

   @Override
   public boolean isActive()
   {
      return active;
   }

   @Override
   public void setActive(boolean active)
   {
      setActiveIndicator(active);
      super.setActive(active);
   }

   @Override
   public void setActiveIndicator(boolean active)
   {
      this.active = active;
   }

   @Override
   public LayerName getName()
   {
      return name;
   }

   @Override
   public Layer clone()
   {
      return new LSatelliteLayer();
   }
}
