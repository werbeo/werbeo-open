package org.infinitenature.werbeo.cms.vaadin.components.filter;

import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.LinkButton;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.vaadin.viritin.layouts.MPanel;

import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;

public class FilterPanel<T> extends I18NField<T>
{
   private LinkButton resetFilters = new LinkButton(VaadinIcons.RECYCLE,
         event -> restFilters());
   private Set<HasValue<?>> filterFields = new HashSet<>();
   private final ResponsiveLayout filterLayout = new ResponsiveLayout();
   private final ResponsiveRow filterRow = filterLayout.addRow()
         .withMargin(true);
   private final ResponsiveRow filterLayoutSecondRow = filterLayout.addRow()
         .withComponents(resetFilters);
   private final Panel filterPanel = new MPanel(filterLayout);
   private final Binder<T> binder;
   private final Class<T> filterClass;
   private T value;

   public FilterPanel(MessagesResource messages, Class<T> filterClass)
   {
      super(messages);
      this.filterClass = filterClass;
      this.binder = new Binder<>(filterClass);

   }

   public Component addFilter(HasValue<?> filterComponent, String property)
   {
      binder.bind(filterComponent, property);
      filterFields.add(filterComponent);
      Component filterAsComponent = (Component) filterComponent;
      filterRow.addComponent(filterAsComponent);
      filterPanel.setVisible(true);
      filterComponent.addValueChangeListener(this::onFilterComponetValueChange);
      resetFilters.setVisible(true);
      return filterAsComponent;
   }

   void onFilterComponetValueChange(ValueChangeEvent<?> event)
   {

      try
      {

         T filter = filterClass.newInstance();
         binder.writeBeanIfValid(filter);

         setValue(filter, true);
         // fireEvent(createValueChange(oldValue, true));
      } catch (InstantiationException | IllegalAccessException e1)
      {
         e1.printStackTrace();
      }
   }


   private void restFilters()
   {
      setValue(null);
   }

   @Override
   public void applyLocalizations()
   {
      // Nothing to translate
   }

   @Override
   public T getValue()
   {
      return value;
   }

   @Override
   protected Component initContent()
   {
      return filterPanel;
   }

   @Override
   protected void doSetValue(T value)
   {
      this.value = value;
      binder.readBean(value);
   }

}
