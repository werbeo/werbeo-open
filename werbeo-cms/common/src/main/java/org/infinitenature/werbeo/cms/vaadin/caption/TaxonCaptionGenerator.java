package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.TaxonBase;

public class TaxonCaptionGenerator implements WerbeoCaptionGenerator<TaxonBase>
{

   @Override
   public String apply(TaxonBase item)
   {
      if (item != null)
      {
         return item.getName();
      } else
      {
         return null;
      }
   }
}
