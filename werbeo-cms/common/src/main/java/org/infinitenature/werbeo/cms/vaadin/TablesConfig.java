package org.infinitenature.werbeo.cms.vaadin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.infinitenature.service.v1.types.Occurrence;

import com.vaadin.data.provider.GridSortOrder;

public class TablesConfig
{
   private List<GridSortOrder<Occurrence>> occurrenceTableSortOrders = new ArrayList<GridSortOrder<Occurrence>>();
   private List<GridSortOrder<Occurrence>> myOccurrenceTableSortOrders = new ArrayList<GridSortOrder<Occurrence>>();

   private List<String> occurrenceTableColumns = new ArrayList<String>();
   private List<String> myOccurrenceTableColumns = new ArrayList<String>();
   
   public List<GridSortOrder<Occurrence>> getOccurrenceTableSortOrders()
   {
      return occurrenceTableSortOrders;
   }
   public void setOccurrenceTableSortOrders(
         List<GridSortOrder<Occurrence>> occurrenceTableSortOrders)
   {
      this.occurrenceTableSortOrders = occurrenceTableSortOrders;
   }
   public List<GridSortOrder<Occurrence>> getMyOccurrenceTableSortOrders()
   {
      return myOccurrenceTableSortOrders;
   }
   public void setMyOccurrenceTableSortOrders(
         List<GridSortOrder<Occurrence>> myOccurrenceTableSortOrders)
   {
      this.myOccurrenceTableSortOrders = myOccurrenceTableSortOrders;
   }
   public List<String> getOccurrenceTableColumns()
   {
      return occurrenceTableColumns;
   }
   public void setOccurrenceTableColumns(List<String> occurrenceTableColumns)
   {
      this.occurrenceTableColumns = occurrenceTableColumns;
   }
   public List<String> getMyOccurrenceTableColumns()
   {
      return myOccurrenceTableColumns;
   }
   public void setMyOccurrenceTableColumns(List<String> myOccurrenceTableColumns)
   {
      this.myOccurrenceTableColumns = myOccurrenceTableColumns;
   }
}
