package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.*;

import org.infinitenature.service.v1.types.*;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.werbeo.cms.vaadin.*;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

import com.vaadin.data.HasValue;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.ui.*;

public class GeoButton extends I18NButton
      implements HasValue<Position>, Button.ClickListener
{
   private List<ValueChangeListener<Position>> valueChangeListeners = new ArrayList<>();
   private Window window;
   private MapEditComponent mapEditComponent;
   private String caption;
   private String captionActive;
   private Button resetButton;
   private Button selectButton;

   public GeoButton(String caption, MessagesResource messages,
         PortalConfiguration portalConfiguration)
   {
      this(messages, portalConfiguration);
      this.caption = caption;
   }

   public GeoButton(MessagesResource messages,
         PortalConfiguration portalConfiguration)
   {
      super(messages);
      addClickListener(this);
      mapEditComponent = new MapEditComponent("https://wms.test.infinitenature.org/geoserver/werbeo/wms",PositionType.SHAPE, 
                  portalConfiguration, true, PositionType.SHAPE,PositionType.MTB
                  );
      resetButton = new Button();
      selectButton = new Button();

      window = new Window();
      window.setWidth("800px");
      window.setHeight("750px");
      window.center();
      setIcon(VaadinIcons.MAP_MARKER);

      resetButton.addClickListener(event -> {
         mapEditComponent.setValue(null);
         UI.getCurrent().removeWindow(window);
         for (ValueChangeListener<Position> listener : valueChangeListeners)
         {
            listener.valueChange(
                  new ValueChangeEvent(this, Position.class, true));
         }
         indicateSearchStatus(mapEditComponent.getValue() != null);
      });

      selectButton.addClickListener(event -> {
         UI.getCurrent().removeWindow(window);
         for (ValueChangeListener<Position> listener : valueChangeListeners)
         {
            listener.valueChange(
                  new ValueChangeEvent(this, Position.class, true));
         }
         indicateSearchStatus(mapEditComponent.getValue() != null);
      });

      VerticalLayout layout = new VerticalLayout();
      layout.setSizeFull();
      layout.setMargin(true);
      layout.setSpacing(true);
      layout.addComponent(mapEditComponent);
      layout.setExpandRatio(mapEditComponent, 1F);
      HorizontalLayout buttonLayout = new HorizontalLayout();
      buttonLayout.addComponent(resetButton);
      buttonLayout.addComponent(selectButton);
      layout.addComponent(buttonLayout);
      mapEditComponent.setValue(null);
      window.setContent(layout);
   }

   @Override
   protected void init()
   {

   }

   @Override
   protected String getKey()
   {
      return caption;
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      this.resetButton.setCaption(translate("RESET"));
      this.selectButton.setCaption(translate("CHOOSE"));
      this.captionActive = translate("ACTIV");
   }

   private void indicateSearchStatus(boolean searching)
   {
      if (searching)
      {
         addStyleName("button-searching");
         super.setCaption(caption + " " + captionActive);
      }
      else
      {
         removeStyleName("button-searching");
         super.setCaption(caption);
      }
   }

   @Override
   public void setValue(Position geometry)
   {
      if (geometry == null)
      {
         mapEditComponent.setValue(null);
         for (ValueChangeListener<Position> listener : valueChangeListeners)
         {
            listener.valueChange(
                  new ValueChangeEvent(this, Position.class, true));
         }
         indicateSearchStatus(false);
      }
      else
      {
         mapEditComponent.setValue(geometry);
         indicateSearchStatus(true);
      }
   }

   @Override
   public Position getValue()
   {
      return mapEditComponent.getValue();
   }

   @Override
   public void setRequiredIndicatorVisible(boolean b)
   {

   }

   @Override
   public boolean isRequiredIndicatorVisible()
   {
      return false;
   }

   @Override
   public void setReadOnly(boolean b)
   {
      if (b)
      {
         throw new IllegalArgumentException(("must always be read only false"));
      }
   }

   @Override
   public boolean isReadOnly()
   {
      return false;
   }

   @Override
   public Registration addValueChangeListener(
         ValueChangeListener<Position> valueChangeListener)
   {
      valueChangeListeners.add(valueChangeListener);
      return (Registration) () -> valueChangeListeners
            .remove(valueChangeListener);
   }

   @Override
   public void buttonClick(ClickEvent clickEvent)
   {
      UI.getCurrent().addWindow(window);
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
      super.setCaption(caption);
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }
}
