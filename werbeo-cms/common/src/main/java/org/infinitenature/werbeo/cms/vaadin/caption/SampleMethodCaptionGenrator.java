package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.SampleBase;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class SampleMethodCaptionGenrator
      extends I18NCaptionGenerator<SampleBase.SampleMethod>
{

   public SampleMethodCaptionGenrator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
