package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence.LifeStage;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.LifeStageCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class LifeStageComboBox extends EnumComboBox<LifeStage>
{

   public LifeStageComboBox(MessagesResource messages)
   {
      super(messages, LifeStage.class, new LifeStageCaptionGenerator(messages));
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "LIFE_STAGE", getLocale()));
   }
}
