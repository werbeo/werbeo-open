package org.infinitenature.werbeo.cms.properties;

public interface EntityEnum
{
   default String getFieldName()
   {
      return name();
   }

   String name();

   String getEntityName();
}
