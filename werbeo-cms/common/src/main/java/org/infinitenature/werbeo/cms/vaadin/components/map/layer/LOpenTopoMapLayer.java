package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LTileLayer;

public class LOpenTopoMapLayer extends LTileLayer implements Layer
{
   private LayerName name;
   private boolean active;

   LOpenTopoMapLayer()
   {
      super("https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png");
      setMaxZoom(17);
      setAttributionString(
            "MapField data: &copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a>, <a href=\"http://viewfinderpanoramas.org\">SRTM</a> | MapField style: &copy; <a href=\"https://opentopomap.org\">OpenTopoMap</a> (<a href=\"https://creativecommons.org/licenses/by-sa/3.0/\">CC-BY-SA</a>)");
      setActive(false);
      name = LayerName.OPEN_TOPO_MAP;
      setCaption(LayerName.OPEN_TOPO_MAP.getCaption());
   }

   @Override
   public boolean isActive()
   {
      return active;
   }

   @Override
   public void setActive(boolean active)
   {
      setActiveIndicator(active);
      super.setActive(active);
   }

   @Override
   public void setActiveIndicator(boolean active)
   {
      this.active = active;
   }

   @Override
   public LayerName getName()
   {
      return name;
   }

   @Override
   public Layer clone()
   {
      return new LOpenTopoMapLayer();
   }
}
