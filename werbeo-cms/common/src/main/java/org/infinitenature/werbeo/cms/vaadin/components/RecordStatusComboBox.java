package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.UI;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComboBox;

public class RecordStatusComboBox extends I18NComboBox<Occurrence.RecordStatus>
{
   public RecordStatusComboBox(MessagesResource messages)
   {
      super(messages);
      setItems(Occurrence.RecordStatus.values());
      setValue(Occurrence.RecordStatus.COMPLETE);
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   @Override
   public void applyLocalizations()
   {
      setCaption(translate("WORK_IN_PROGRESS"));
   }
}
