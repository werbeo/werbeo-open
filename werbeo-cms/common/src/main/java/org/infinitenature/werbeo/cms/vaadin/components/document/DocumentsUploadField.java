package org.infinitenature.werbeo.cms.vaadin.components.document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MGridLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.Registration;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;

public class DocumentsUploadField extends I18NField<Collection<DocumentUpload>>
{
   private Button addButton = new MButton(VaadinIcons.FILE_ADD,
         this::onAddUpload);
   private MGridLayout grid = new MGridLayout(2, 1)
         .withDefaultComponentAlignment(Alignment.BOTTOM_CENTER)
         .withMargin(false);
   private MVerticalLayout layout = new MVerticalLayout(addButton, grid);
   private List<DocumentUploadField> documentUpoladFields = new ArrayList<>();
   private List<MButton> removeButtons = new ArrayList<>();

   public DocumentsUploadField(MessagesResource messages)
   {
      super(messages);
   }

   private void onValueChange(ValueChangeEvent<DocumentUpload> e)
   {
      fireEvent(new ValueChangeEvent<>(this, this.getValue(), true));
   }

   private DocumentUploadField onAddUpload(ClickEvent event)
   {
      DocumentUploadField documentUploadField = new DocumentUploadField(
            getMessages());
      Registration registration = documentUploadField
            .addValueChangeListener(this::onValueChange);
      MButton removeRow = new MButton(VaadinIcons.FILE_REMOVE);
      removeRow.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "remove", getLocale()));

      removeRow.addClickListener(e ->
      {
         documentUpoladFields.remove(documentUploadField);
         removeButtons.remove(removeRow);
         registration.remove();
         fireEvent(new ValueChangeEvent<>(this, this.getValue(), true));
         buildGrid();
      });
      removeRow.addClickListener(e -> removeButtons.remove(removeRow));
      documentUpoladFields.add(documentUploadField);
      removeButtons.add(removeRow);
      buildGrid();
      fireEvent(new ValueChangeEvent<>(this, this.getValue(), true));
      return documentUploadField;
   }

   private void buildGrid()
   {
      grid.removeAllComponents();

      for (DocumentUploadField documentUploadField : documentUpoladFields)
      {
         grid.add(documentUploadField, removeButtons
               .get(documentUpoladFields.indexOf(documentUploadField)));
      }
   }

   @Override
   public void applyLocalizations()
   {
      addButton.setCaption(
            getMessages().getMessage(Context.getCurrent().getPortal(),
                  "Button.newAttachment", getLocale()));
   }

   @Override
   public Collection<DocumentUpload> getValue()
   {
      return documentUpoladFields.stream().map(f -> f.getValue()).collect(Collectors.toList());
   }

   @Override
   protected Component initContent()
   {
      return layout;
   }

   @Override
   protected void doSetValue(Collection<DocumentUpload> uploads)
   {
      documentUpoladFields.clear();
      grid.removeAllComponents();
      removeButtons.clear();
      
      if(uploads != null)
      {
         for(DocumentUpload upload : uploads)
         {
            onAddUpload(null).setValue(upload);
         }
      }
      buildGrid();
   }

   public boolean isValid()
   {
      return !documentUpoladFields.stream().map(f -> f.isValid())
            .filter(b -> b == false).findFirst().isPresent();
   }

}
