package org.infinitenature.werbeo.cms;

import java.util.HashMap;
import java.util.Map;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "werbeo.cms")
public class InstanceConfig
{
   private String mapsBaseURL = "http://loe3.loe.auf.uni-rostock.de/maps/";
   private Map<Integer, String> mapExternalCmsUrl = new HashMap<>();
   
   private String quarasekUrl;

   public static class Geoserver
   {
      private String url = "https://wms.test.infinitenature.org/geoserver/werbeo/wms";

      public String getUrl()
      {
         return url;
      }

      public void setUrl(String url)
      {
         this.url = url;
      }
   }

   private Geoserver geoserver = new Geoserver();

   public Geoserver getGeoserver()
   {
      return geoserver;
   }

   public void setGeoserver(Geoserver geoserver)
   {
      this.geoserver = geoserver;
   }

   public String getExternalCmsUrl(int portalId)
   {
      return this.mapExternalCmsUrl.get(portalId);
   }

   public Map<Integer, String> getMapExternalCmsUrl()
   {
      return mapExternalCmsUrl;
   }

   public void setMapExternalCmsUrl(Map<Integer, String> mapExternalCmsUrl)
   {
      this.mapExternalCmsUrl = mapExternalCmsUrl;
   }

   public String getMapsBaseURL()
   {
      return mapsBaseURL;
   }

   public void setMapsBaseURL(String mapsBaseURL)
   {
      this.mapsBaseURL = mapsBaseURL;
   }

   public boolean isTranslateTaxa()
   {
      return Context.getCurrent().getPortal().getPortalId() == 6;
   }

   public String getQuarasekUrl()
   {
      return quarasekUrl;
   }

   public void setQuarasekUrl(String quarasekUrl)
   {
      this.quarasekUrl = quarasekUrl;
   }
}
