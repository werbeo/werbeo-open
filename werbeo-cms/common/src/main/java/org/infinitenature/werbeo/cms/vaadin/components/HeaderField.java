package org.infinitenature.werbeo.cms.vaadin.components;

import org.vaadin.viritin.fields.CaptionGenerator;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.fluency.ui.FluentCustomField;
import org.vaadin.viritin.label.Header;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;

public class HeaderField<T> extends CustomField<T>
      implements FluentCustomField<LabelField<T>, T>
{
   private static final long serialVersionUID = 1833222201993254187L;

   private T value;

   @Override
   protected void doSetValue(T value)
   {
      this.value = value;
      updateLabel();
   }

   @Override
   public T getValue()
   {
      return value;
   }

   private static class ToStringCaptionGenerator<T>
	 implements CaptionGenerator<T>
   {

      private static final long serialVersionUID = 1149675718238329960L;

      @Override
      public String getCaption(T option)
      {
	 if (option == null)
	 {
	    return "";
	 }
	 else
	 {
	    return option.toString();
	 }
      }
   }

   public HeaderField(String caption)
   {
      super();
      setCaption(caption);
   }

   private CaptionGenerator<T> captionGenerator = new ToStringCaptionGenerator<>();
   private Header header = new Header("");

   public HeaderField()
   {
   }

   @Override
   protected Component initContent()
   {
      updateLabel();

      return header;
   }

   protected void updateLabel()
   {
      String caption;
      if (captionGenerator != null && getValue() != null)
      {
	 caption = captionGenerator.getCaption(getValue());
      }
      else if (getValue() != null)
      {
	 caption = getValue().toString();
      }
      else
      {
         caption = null;
      }
      header.setValue(caption);
   }

   /**
    * Sets the CaptionGenerator for creating the label value.
    *
    * @param captionGenerator
    *           the caption generator used to format the displayed property
    */
   public void setCaptionGenerator(CaptionGenerator<T> captionGenerator)
   {
      this.captionGenerator = captionGenerator;
      updateLabel();
   }

   public void setHeader(Header header)
   {
      this.header = header;
   }

   public Header getHeader()
   {
      return header;
   }

}
