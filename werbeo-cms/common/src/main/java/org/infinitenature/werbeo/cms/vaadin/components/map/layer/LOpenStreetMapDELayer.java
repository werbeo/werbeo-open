package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LTileLayer;

public class LOpenStreetMapDELayer extends LTileLayer implements Layer
{
   private LayerName name;
   private boolean active;

   LOpenStreetMapDELayer()
   {
      super("https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png");
      setMaxZoom(18);
      setAttributionString(
            "&copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a>");
      setActive(true);
      name = LayerName.OPEN_STREET_MAP;
      setCaption(LayerName.OPEN_STREET_MAP.getCaption());
   }

   @Override
   public boolean isActive()
   {
      return active;
   }

   @Override
   public void setActive(boolean active)
   {
      setActiveIndicator(active);
      super.setActive(active);
   }

   @Override
   public void setActiveIndicator(boolean active)
   {
      this.active = active;
   }

   @Override
   public LayerName getName()
   {
      return name;
   }

   @Override
   public Layer clone()
   {
      return new LOpenStreetMapDELayer();
   }
}
