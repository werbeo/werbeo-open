package org.infinitenature.werbeo.cms.vaadin.components.numericamount;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

public class CountedButton extends I18NButton
{

   public CountedButton(MessagesResource messages, ClickListener listener)
   {
      super(messages, listener);
   }

   @Override
   protected void init()
   {
      // NOOP
   }

   @Override
   protected String getKey()
   {
      return "NumericAmountAccuracy.COUNTED";
   }

   @Override
   public void applyLocalizations()
   {
      setCaption(getEntityField("NumericAmountAccuracy", "COUNTED"));
   }
}
