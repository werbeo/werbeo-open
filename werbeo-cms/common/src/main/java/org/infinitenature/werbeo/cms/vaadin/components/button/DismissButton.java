package org.infinitenature.werbeo.cms.vaadin.components.button;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

import com.vaadin.icons.VaadinIcons;

public class DismissButton extends I18NButton
{

   public DismissButton(MessagesResource messages, ClickListener listener)
   {
      super(messages, listener);
   }

   public DismissButton(MessagesResource messages)
   {
      super(messages);
   }

   @Override
   protected String getKey()
   {
      return "DISMISS";
   }

   @Override
   protected void init()
   {
      setIcon(VaadinIcons.CLOSE);
   }

   @Override
   public DismissButton addClickListener(MClickListener listener)
   {
      return (DismissButton) super.addClickListener(listener);
   }

}
