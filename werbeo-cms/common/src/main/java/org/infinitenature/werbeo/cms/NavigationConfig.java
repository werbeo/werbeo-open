package org.infinitenature.werbeo.cms;

import java.util.HashSet;

// TODO: this has to be refactored!

public enum NavigationConfig
{
   DATEN_AUSGABE (new int[]{}, "Datenausgabe", "", false),
   DATEN_AUSGABE_MV(new int[]{2,3,4,5},"Datenausgabe", "datenausgabe",  true),
   DATEN_AUSGABE_NO_EXTERNAL_LINK(new int[]{6,7,8,9,10,11},"Datenausgabe", "",  false),

   DATEN_EINGABE (new int[]{}, "Dateneingabe", "", false),
   DATEN_EINGABE_MV(new int[]{2,3,4,5},"Dateneingabe", "dateneingabe", true),
   DATEN_EINGABE_NO_EXTERNAL_LINK(new int[]{6,7,8,9,10,11},"Dateneingabe", "", false),

   EINZELFUND_EINGABE(new int[]{2,3,4,5,6,7,8,9,10},"Einzelfundeingabe", "eingabe", false),
   KARTIERLISTE(new int[]{2,3,4,5,6},"Kartierliste", "kartierliste", false),

   FUNDE(new int[]{2,3,4,5,6,7,8,9,10,11},"Funde", "funde", false),
   IMPORT(new int[]{2,3,4,5,6,7,8,9,10,11},"Import", "Import", false),
   MEINE_FUNDE(new int[]{2,3,4,5,6,7,8,9,10,11},"Meine Funde", "meineFunde", false),

   MEINE_EXPORTE(new int[]{2,3,4,5,6,7,8,9,10,11},"Meine Exporte", "exporte", false),

   VERBREITUNG(new int[]{2,3,4,5,6,7,8,9,10,11},"Verbreitung", "species", false),

   BEDINGUNGEN(new int[]{},"Bedingungen", "bedingungen", false),
   NUTZUNGSBEDINGUNGEN(new int[]{},"Nutzungsbedingungen", "nutzungsbedingungen", false),

   DATENSCHUTZ(new int[]{}, "Datenschutz", "datenschutzbedingungen", false),
   

   IMPRESSUM(new int[]{}, "Impressum", "", false),
   IMPRESSUM_HEUSCHRECKEN(new int[]{6,7,8,9,10,11},"Impressum", "imprint", false),

   BEGEHUNG(new int[]{2,3,4,5,6,7,8,9,10,11}, "Begehung", "begehung", false),
   
   PROJEKTE(new int[]{2,3,4,5,6,7,8,9,10,11}, "Projekte", "projekte", false),
   PROJEKT(new int[]{2,3,4,5,6,7,8,9,10,11}, "Projekt", "projekt", false),
   BENUTZER(new int[]{2,3,4,5,6,7,8,9,10,11}, "Benutzer", "benutzer", false),

   REGISTRIERUNG(new int[]{}, "Einwilligung Registrierung", "", false);

   private String breadcrumbText;
   private String navigationTarget;
   private boolean externalCmsLink;
   private HashSet<Integer> portalIds;

   NavigationConfig(int[] portalIds, String breadcrumbText, String navigationTarget, boolean externalCmsLink)
   {
      this.portalIds = new HashSet<>();
      for(int id: portalIds)
      {
         this.portalIds.add(id);
      }
      this.breadcrumbText = breadcrumbText;
      this.navigationTarget = navigationTarget;
      this.externalCmsLink = externalCmsLink;
   }

   public String getBreadcrumbText()
   {
      return breadcrumbText;
   }

   public String getNavigationTarget()
   {
      return navigationTarget;
   }

   public boolean isExternalCmsLink()
   {
      return externalCmsLink;
   }

   public static NavigationConfig getEnum(String breadcrumbText, int portalId)
   {
      for (NavigationConfig target : NavigationConfig.values())
      {
         if (target.getBreadcrumbText().equals(breadcrumbText)
               && target.portalIds.contains(portalId))
         {
            return target;
         }
      }
      return null;
   }
}
