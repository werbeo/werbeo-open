package org.infinitenature.werbeo.cms.formatter;

import java.text.DecimalFormat;
import java.util.ResourceBundle;

import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PositionFormatter
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PositionFormatter.class);

   private PositionFormatter()
   {
      throw new IllegalAccessError("Uitility class");
   }

   public static String getSrefSystem(Position position)
   {
      String enteredSerfSystem = null;
      String key = null;
      if (position.getType() == PositionType.MTB)
      {
         key = "mtbqqq";
      } else
      {
         key = "sref." + position.getEpsg() + ".name";
      }
      try
      {
         ResourceBundle srefSystems = ResourceBundle.getBundle("srefsystems");
         enteredSerfSystem = srefSystems.getString(key);
      } catch (Exception e)
      {
         LOGGER.error(
               "Missing entry in srefsystems.properties for key: " + key);
         enteredSerfSystem = "epsg:" + position.getEpsg();
      }
      return enteredSerfSystem;
   }

   public static String getSref(Position position)
   {
      StringBuilder enteredSref = new StringBuilder();

      if (position.getType() == PositionType.MTB)
      {
         enteredSref.append(position.getMtb().getMtb());
      } else
      {
         DecimalFormat decimalFormat = new DecimalFormat("#.######");
         enteredSref
               .append(decimalFormat.format(position.getPosCenterLatitude()));
         enteredSref.append(" ");
         enteredSref
               .append(decimalFormat.format(position.getPosCenterLongitude()));
      }
      return enteredSref.toString();
   }
}
