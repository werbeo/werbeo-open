package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

public interface Layer
{
   boolean isActive();
   void setActive(boolean active);
   void setActiveIndicator(boolean active);
   LayerName getName();
   String getCaption();
}
