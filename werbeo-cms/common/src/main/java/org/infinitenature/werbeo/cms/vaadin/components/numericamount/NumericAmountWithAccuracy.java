package org.infinitenature.werbeo.cms.vaadin.components.numericamount;

import org.infinitenature.service.v1.types.Occurrence.NumericAmountAccuracy;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class NumericAmountWithAccuracy
{

   private int amount;
   private NumericAmountAccuracy accuracy;

   public static NumericAmountWithAccuracy of(Integer amount,
         NumericAmountAccuracy accuray)
   {
      if (amount != null && accuray != null)
      {
         return new NumericAmountWithAccuracy(amount.intValue(), accuray);
      }
      return null;
   }

   public NumericAmountWithAccuracy()
   {
      super();
   }

   public NumericAmountWithAccuracy(int amount, NumericAmountAccuracy accuracy)
   {
      super();
      this.amount = amount;
      this.accuracy = accuracy;
   }

   public int getAmount()
   {
      return amount;
   }

   public void setAmount(int amount)
   {
      this.amount = amount;
   }

   public NumericAmountAccuracy getAccuracy()
   {
      return accuracy;
   }

   public void setAccuracy(NumericAmountAccuracy accuracy)
   {
      this.accuracy = accuracy;
   }

   @Override
   public String toString()
   {
      return NumericAmountWithAccuracyBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return NumericAmountWithAccuracyBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return NumericAmountWithAccuracyBeanUtil.doEquals(this, obj);
   }
}
