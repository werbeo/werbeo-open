package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.Quantity;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class QuantityCaptionGenerator extends I18NCaptionGenerator<Quantity>
{

   public QuantityCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

}
