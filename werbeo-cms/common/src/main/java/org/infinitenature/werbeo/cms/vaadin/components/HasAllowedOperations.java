package org.infinitenature.werbeo.cms.vaadin.components;

import java.util.Collection;

import org.infinitenature.service.v1.types.enums.Operation;

public interface HasAllowedOperations
{
   public void setAllowedOperations(Collection<Operation> allowedOperations);
}
