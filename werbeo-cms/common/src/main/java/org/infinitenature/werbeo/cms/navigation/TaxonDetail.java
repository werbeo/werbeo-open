package org.infinitenature.werbeo.cms.navigation;

public class TaxonDetail implements NavigationTarget
{

   public TaxonDetail(String taxon)
   {
      this.taxon = taxon;
   }

   public String getTaxon()
   {
      return taxon;
   }

   public void setTaxon(String taxon)
   {
      this.taxon = taxon;
   }

   private String taxon;
}
