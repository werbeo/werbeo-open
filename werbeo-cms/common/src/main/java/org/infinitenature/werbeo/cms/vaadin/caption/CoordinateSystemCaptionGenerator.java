package org.infinitenature.werbeo.cms.vaadin.caption;

import com.vaadin.ui.UI;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.components.CoordinateSystemButton;
import org.infinitenature.werbeo.cms.vaadin.components.map.CoordinateSystem;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

import java.util.Locale;

public class CoordinateSystemCaptionGenerator
      extends I18NCaptionGenerator<Integer>
{
   public CoordinateSystemCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

   @Override
   public String apply(Integer epsg)
   {
      Portal portal = Context.getCurrent().getPortal();
      Locale locale = UI.getCurrent().getLocale();
      if (epsg == null)
      {
         return "";
      } else if (CoordinateSystem.UNSUPPORTED == CoordinateSystem.get(epsg))
      {
         return String.valueOf(epsg);
      } else
      {
         return CoordinateSystem.get(epsg).getName();
      }
   }
}
