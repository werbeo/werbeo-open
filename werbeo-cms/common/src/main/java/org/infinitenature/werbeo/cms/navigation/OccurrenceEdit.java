package org.infinitenature.werbeo.cms.navigation;

import java.util.UUID;

public class OccurrenceEdit implements NavigationTarget
{
   private final UUID occurrenceUUID;

   public OccurrenceEdit(UUID occurrenceUUID)
   {
      super();
      this.occurrenceUUID = occurrenceUUID;
   }

   public UUID getOccurrenceUUID()
   {
      return occurrenceUUID;
   }
}
