package org.infinitenature.werbeo.cms.vaadin.components.document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;

public class DocumentUploadField extends I18NField<DocumentUpload>
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(DocumentUploadField.class);

   private MTextField caption = new MTextField();
   private Label valid = new MLabel().withWidth("2ex").withFullHeight();
   private Component preview = new MLabel().withCaption("Vorschau");
   private File uploadingFile = null;
   private File uploadedFile = null;
   private String mimeType = null;
   private Upload newMedia = new Upload();
   private MHorizontalLayout layout = new MHorizontalLayout(valid, caption,
         preview).withDefaultComponentAlignment(Alignment.MIDDLE_LEFT)
               .withComponentAlignment(preview, Alignment.MIDDLE_RIGHT)
               .withComponentAlignment(valid, Alignment.BOTTOM_CENTER)
               .withExpandRatio(caption, 4.f).withExpandRatio(preview, 1.5f)
               .withFullWidth();
   private int maxUploadSize = 0;

   public DocumentUploadField(MessagesResource messages)
   {
      super(messages);
      this.caption.setRequiredIndicatorVisible(true);
      this.caption.setValueChangeMode(ValueChangeMode.EAGER);
      this.caption.addValueChangeListener(e -> validate());
      this.valid.setContentMode(ContentMode.HTML);
      this.valid.setValue(VaadinIcons.CLOSE_BIG.getHtml());
      this.newMedia.setWidth("10em");
   }

   @Override
   public void applyLocalizations()
   {
      Portal portal = Context.getCurrent().getPortal();
      // empty strings for layout
      this.newMedia.setCaption("");
      this.valid.setCaption("");
      this.newMedia.setButtonCaption(getMessages().getMessage(portal,
            "general.selectFile", getLocale()));
      this.preview.setCaption(
            getMessages().getMessage(portal, "general.preview", getLocale()));
      this.caption.setCaption(getMessages().getMessage(portal,
            "general.description", getLocale()));
   }

   @Override
   protected Component initContent()
   {
      newMedia.addSucceededListener(this::onFinish);
      newMedia.addFailedListener(this::onFailure);
      newMedia.setAcceptMimeTypes("image/png, image/jpeg, audio/x-wav");
      newMedia.setReceiver((fileName, mimeType) ->
      {
         Path dir;
         try
         {
            dir = Files.createTempDirectory("userMediaUpload");
            uploadingFile = dir.resolve(fileName).toFile();
            return new FileOutputStream(uploadingFile);
         } catch (IOException e)
         {
            throw new RuntimeException(e);
         }

      });

      layout = new MHorizontalLayout(valid, caption, preview, newMedia);
      return layout;
   }

   public void onFailure(Upload.FailedEvent event)
   {
      uploadingFile = null;
      validate();
   }

   public void onFinish(Upload.SucceededEvent event)
   {
      uploadedFile = uploadingFile;
      uploadingFile = null;
      if (!validSize())
      {
         try
         {
            Files.delete(uploadedFile.toPath());
         } catch (IOException e)
         {
            LOGGER.error("Failure deleting too big file", e);
         }

         showErrorMessage("upload.limit", maxUploadSize,
               uploadedFile.getName());
         uploadedFile = null;
      }
      mimeType = event.getMIMEType();
      LOGGER.info("Uploaded file {} of mime type {}", uploadedFile, mimeType);
      updatePreview();
      validate();
   }

   private void updatePreview()
   {

      FileResource fs = new FileResource(uploadedFile);
      if (mimeType.toLowerCase().startsWith("image"))
      {

         Image image = new Image();
         image.setSource(fs);
         image.setWidth("300px");

         preview = image;
      } else if (mimeType.toLowerCase().startsWith("audio"))
      {
         Audio audio = new Audio();
         audio.setSource(fs);
         audio.setWidth("300px");
         preview = audio;
      }
      UI.getCurrent().access(() ->
      {
         layout.removeAllComponents();
         layout.add(valid, caption, preview, newMedia);
      });
      applyLocalizations();
   }

   private void validate()
   {
      if (isValid())
      {
         valid.setValue(VaadinIcons.CHECK.getHtml());
      } else
      {
         valid.setValue(VaadinIcons.CLOSE_BIG.getHtml());
      }
      fireEvent(new ValueChangeEvent<>(this, getValue(), true));
   }

   public boolean isValid()
   {
      return uploadedFile != null && StringUtils.isNotBlank(caption.getValue());
   }

   private boolean validSize()
   {
      if (maxUploadSize == 0)
      {
         return true;
      }
      try
      {
         long size = Files.size(uploadedFile.toPath());
         if (size > maxUploadSize * 1024 * 1024)
         {
            return false;
         } else
         {
            return true;
         }
      } catch (IOException e)
      {
         LOGGER.error("Failure getting file size", e);
         return false;
      }
   }

   @Override
   public DocumentUpload getValue()
   {
      return new DocumentUpload(caption.getValue(), uploadedFile, mimeType);
   }

   @Override
   protected void doSetValue(DocumentUpload value)
   {
      caption.setValue(value.getCaption());
      uploadedFile = value.getFile();
      mimeType = value.getMimeType();
      updatePreview();
   }

   @Override
   public int hashCode()
   {
      return super.hashCode();
   }

   @Override
   public boolean equals(Object obj)
   {
      return super.equals(obj);
   }

   /**
    * Sets the maximum allowed size for an upload
    *
    * @param maxUploadSize
    *           0 for unlimited
    */
   public void setMaxUploadSize(int maxUploadSize)
   {
      this.maxUploadSize = maxUploadSize;
   }
}
