package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LWmsLayer;

public class LMTBLayer extends LWmsLayer implements Layer
{
   private LayerName name;
   private boolean active;

   LMTBLayer(String geoserverURL)
   {
      setUrl(geoserverURL);
      setLayers("werbeo:mtb");
      setTransparent(true);
      setFormat("image/png");
      setActive(false);
      setMinZoom(8);
      setEnabled(true);
      name = LayerName.MTB;
      setCaption(LayerName.MTB.getCaption());
   }

   @Override
   public boolean isActive()
   {
      return active;
   }

   @Override
   public void setActive(boolean active)
   {
      setActiveIndicator(active);
      super.setActive(active);
   }

   @Override
   public void setActiveIndicator(boolean active)
   {
      this.active = active;
   }

   @Override
   public LayerName getName()
   {
      return name;
   }

   @Override
   public Layer clone()
   {
      return new LMTBLayer(getUrl());
   }

}
