package org.infinitenature.werbeo.cms.properties;

public enum SurveyProperties implements EntityEnum
{
   AVAILABILITY;

   @Override
   public String getEntityName()
   {
      return "Survey";
   }

}
