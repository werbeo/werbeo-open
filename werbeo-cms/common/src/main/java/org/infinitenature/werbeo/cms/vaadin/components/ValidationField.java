package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.werbeo.cms.theme.werbeo.Images;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.ValidationStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NField;
import org.vaadin.viritin.button.MButton;

import com.vaadin.server.Resource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

public class ValidationField extends I18NField<Validation>
{

   private final MButton button = new MButton().withCaption("")
         .withListener(this::onClick).withStyleName("table-circle-button");
   private final ValidationStatusCaptionGenerator captionGenerator;
   private Validation validation = null;

   public ValidationField(MessagesResource messages)
   {
      this(messages, null);
   }

   public ValidationField(MessagesResource messages, Validation value)
   {
      super(messages);
      this.captionGenerator = new ValidationStatusCaptionGenerator(messages);
      this.doSetValue(value);
   }

   private void onClick(Button.ClickEvent event)
   {
      ValidationDialog popUpDialog = new ValidationDialog(getMessages());
      popUpDialog.setBean(getValue());
      popUpDialog.addValidationListener(this::onValidationChange);
      popUpDialog.setWidth("30%");
      UI.getCurrent().addWindow(popUpDialog);
   }

   private void onValidationChange(ValidationDialog.ValidationEvent event)
   {
      setValue(event.validation, true);
   }

   private Validation updateValue(ValidationStatus status)
   {
      Validation newValue = new Validation();
      newValue.setStatus(status);
      return newValue;
   }

   @Override
   public void applyLocalizations()
   {
      String caption = captionGenerator
            .getCaption(getValidationStatus(getValue()));
      button.setIconAlternateText(caption);
      button.setDescription(caption);
   }

   @Override
   public Validation getValue()
   {
      return validation;
   }

   @Override
   protected Component initContent()
   {
      return button;
   }

   @Override
   protected void doSetValue(Validation value)
   {
      this.validation = value;
      button.setIcon(getIcon(getValidationStatus(value)));
      if (isAttached())
      {
         applyLocalizations();
      }
   }

   ValidationStatus getValidationStatus(Validation value)
   {
      return value == null ? null : value.getStatus();
   }

   private Resource getIcon(ValidationStatus status)
   {
      return Images.getIcon(status);
   }

   public ValidationField withValue(Validation value)
   {
      setValue(value);
      return this;
   }

   public ValidationField withReadOnly()
   {
      return withReadOnly(true);
   }

   public ValidationField withReadOnly(boolean readOnly)
   {
      setReadOnly(readOnly);
      return this;
   }

   public ValidationField withValueChangelistener(
         ValueChangeListener<Validation> listener)
   {
      addValueChangeListener(listener);
      return this;
   }

   @Override
   public void setReadOnly(boolean readOnly)
   {
      super.setReadOnly(readOnly);
      button.setEnabled(!readOnly);
   }
}
