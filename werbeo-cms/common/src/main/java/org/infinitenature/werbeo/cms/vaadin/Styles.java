package org.infinitenature.werbeo.cms.vaadin;

public class Styles
{
   private Styles()
   {
      throw new IllegalAccessError("Utility class!");
   }

   public static final String EVEN = "even";
   public static final String ODD = "odd";
}
