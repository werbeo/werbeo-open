package org.infinitenature.werbeo.cms.vaadin.components;

import com.vaadin.ui.UI;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.VitalityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class VitalityComboBox extends EnumComboBox<Occurrence.Vitality>
{

   public VitalityComboBox(MessagesResource messagesResource)
   {
      super(messagesResource, Occurrence.Vitality.class,
            new VitalityCaptionGenerator(messagesResource));
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      setCaption(translate("VITALITY"));
      setEmptySelectionCaption(
            getMessages().getMessage(Context.getCurrent().getPortal(),
                  "nullSelection", UI.getCurrent().getLocale()));
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }
}
