package org.infinitenature.werbeo.cms.vaadin.components.button;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NButton;

import com.vaadin.icons.VaadinIcons;

public class AcceptButton extends I18NButton
{

   public AcceptButton(MessagesResource messages, ClickListener listener)
   {
      super(messages, listener);
   }

   public AcceptButton(MessagesResource messages)
   {
      super(messages);
   }

   @Override
   protected String getKey()
   {
      return "ACCEPT";
   }

   @Override
   protected void init()
   {
      setIcon(VaadinIcons.CHECK);
   }

}
