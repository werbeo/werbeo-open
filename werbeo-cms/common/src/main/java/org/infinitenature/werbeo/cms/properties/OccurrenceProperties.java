package org.infinitenature.werbeo.cms.properties;

public enum OccurrenceProperties implements EntityEnum
{
   AREA, ENTITY, DETERMINER, AMOUNT, EXTERNAL_KEY, SETTLEMENT_STATUS_FUKAREK, SEX, REPRODUCTION, DETERMINATION_COMMENT;

   @Override
   public String getEntityName()
   {
      return "Occurrence";
   }

}
