package org.infinitenature.werbeo.cms.vaadin.components;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.i18n.EnumComboBox;

public class SettlementStatusComboBox
      extends EnumComboBox<Occurrence.SettlementStatus>
{
   public SettlementStatusComboBox(MessagesResource messagesResource)
   {
      super(messagesResource, Occurrence.SettlementStatus.class,
            new SettlementStatusCaptionGenerator(messagesResource));
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "SETTLEMENT_STATUS", getLocale()));
   }
}
