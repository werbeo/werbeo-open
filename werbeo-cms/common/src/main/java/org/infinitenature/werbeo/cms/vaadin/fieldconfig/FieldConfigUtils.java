package org.infinitenature.werbeo.cms.vaadin.fieldconfig;

import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.infinitenature.service.v1.types.meta.OccurrenceField;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.service.v1.types.meta.TaxonField;
import org.infinitenature.service.v1.types.meta.TaxonFieldConfig;

public class FieldConfigUtils
{
   private FieldConfigUtils()
   {
      throw new IllegalAccessError("Utitlity class");
   }

   public static Map<OccurrenceField, Boolean> toOccurrenceConfigMap(
         Set<OccurrenceFieldConfig> occurrenceFieldConfigs)
   {
      Map<OccurrenceField, Boolean> occurrenceConfigMap = new HashMap<>();
      occurrenceFieldConfigs.forEach(config -> occurrenceConfigMap
            .put(config.getField(), config.isMandantory()));
      return Collections.unmodifiableMap(occurrenceConfigMap);
   }

   public static Map<SampleField, Boolean> toSampleConfigMap(
         Set<SampleFieldConfig> sampleFieldConfigs)
   {
      Map<SampleField, Boolean> sampleConfigMap = new HashMap<>();
      sampleFieldConfigs.forEach(config -> sampleConfigMap
            .put(config.getField(), config.isMandantory()));
      return Collections.unmodifiableMap(sampleConfigMap);
   }

   public static Map<TaxonField, Boolean> toTaxonFieldConfigMap(
         Set<TaxonFieldConfig> taxonFieldConfigs)
   {
      Map<TaxonField, Boolean> taxonConfigMap = new HashMap<>();
      taxonFieldConfigs.forEach(config -> taxonConfigMap.put(config.getField(),
            config.isMandantory()));
      return Collections.unmodifiableMap(taxonConfigMap);
   }

   public static Map<SurveyField, Boolean> toSurveyFieldConfigMap(
         Set<SurveyFieldConfig> surveyFieldConfigs)
   {
      Map<SurveyField, Boolean> surveyConfigMap = new EnumMap<>(
            SurveyField.class);
      surveyFieldConfigs.forEach(config -> surveyConfigMap
            .put(config.getField(), config.isMandantory()));
      return Collections.unmodifiableMap(surveyConfigMap);
   }
}
