package org.infinitenature.werbeo.cms.vaadin.caption;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NCaptionGenerator;

public class ValidationStatusCaptionGenerator
      extends I18NCaptionGenerator<ValidationStatus>
{

   public ValidationStatusCaptionGenerator(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

   public ValidationStatusCaptionGenerator(MessagesResource messagesResource,
         String nullSelectionCaption)
   {
      super(messagesResource, nullSelectionCaption);
   }

}
