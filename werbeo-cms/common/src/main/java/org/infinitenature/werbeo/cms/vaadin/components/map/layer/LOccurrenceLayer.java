package org.infinitenature.werbeo.cms.vaadin.components.map.layer;

import org.vaadin.addon.leaflet.LWmsLayer;

public class LOccurrenceLayer extends LWmsLayer implements Layer
{
   private LayerName name;
   private boolean active;

   public LOccurrenceLayer(String geoserverURL, String wmsLayerName)

   {
      setUrl(geoserverURL);
      setLayers(wmsLayerName);
      setTransparent(true);
      setFormat("image/png");
      setActive(true);
      setEnabled(true);
      setStyles("werbeo-occ");
      setOpacity(.50);

      name = LayerName.OCCURRENCES;
      setCaption(LayerName.OCCURRENCES.getCaption());
   }

   @Override
   public boolean isActive()
   {
      return active;
   }

   @Override
   public void setActive(boolean active)
   {
      setActiveIndicator(active);
      super.setActive(active);
   }

   @Override
   public void setActiveIndicator(boolean active)
   {
      this.active = active;
   }

   @Override
   public LayerName getName()
   {
      return name;
   }
}
