package org.vaadin.teemusa.gridextensions.paging;

import com.vaadin.data.provider.ConfigurableFilterDataProvider;

public class ConfigurableFilterPagedDataProvider<T, Q, C> extends
      PagedDataProvider<T, Q> implements ConfigurableFilterDataProvider<T, Q, C>
{

   private final ConfigurableFilterDataProvider<T, Q, C> configurableFilterDataProvider;

   public ConfigurableFilterPagedDataProvider(
         ConfigurableFilterDataProvider<T, Q, C> configurableFilterDataProvider)
   {
      super(configurableFilterDataProvider);
      this.configurableFilterDataProvider = configurableFilterDataProvider;

   }

   public ConfigurableFilterPagedDataProvider(
         ConfigurableFilterDataProvider<T, Q, C> configurableFilterDataProvider,
         int pageLength)
   {
      super(configurableFilterDataProvider, pageLength);
      this.configurableFilterDataProvider = configurableFilterDataProvider;
   }

   @Override
   public void setFilter(C filter)
   {
      configurableFilterDataProvider.setFilter(filter);
   }

}
