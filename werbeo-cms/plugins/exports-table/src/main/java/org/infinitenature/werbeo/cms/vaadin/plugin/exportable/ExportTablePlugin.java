package org.infinitenature.werbeo.cms.vaadin.plugin.exportable;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.plugin.exportable.OccurrenceExportTableView.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.Map;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ExportTablePlugin implements VCMSComponent, Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ExportTablePlugin.class);

   private final Werbeo werbeo;
   private final NavigationResolver navigationResolver;
   private final MessagesResource messagesResource;

   private final OccurrenceExportTableView view;
   private OccurrenceExportDataProvider dataProvider;

   public ExportTablePlugin(@Autowired Werbeo werbeo,
         @Autowired NavigationResolver navigationResolver,
         @Autowired MessagesResource messagesResource)
   {
      super();
      this.werbeo = werbeo;
      this.navigationResolver = navigationResolver;
      this.messagesResource = messagesResource;
      this.view = new OccurrenceExportViewImpl(this, messagesResource);
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      dataProvider = setData();
      this.view.setSize(dataProvider.getSize(null));
      UI.getCurrent().setPollInterval(30000);
      UI.getCurrent().addPollListener(pollEvent -> {
         dataProvider.refreshAll();
         this.view.setSize(dataProvider.getSize(null));
      });
   }

   @Override
   public void onFilterChange(OccurrenceExportFilter filter)
   {
      // NOOP since there is no filerable columns
   }

   @Override
   public InputStream onDownloadClick(OccurrenceExport export)
   {
      Response response = werbeo.occurrenceExports().getExport(export.getId(), Context.getCurrent().getPortal().getPortalId());
      Object entity = response.getEntity();
      return (InputStream) entity;
   }

   @Override
   public void onDeleteClick(OccurrenceExport export)
   {
      try
      {
         werbeo.occurrenceExports().delete(export.getId(),
               Context.getCurrent().getPortal().getPortalId());
         dataProvider.refreshAll();
         Notification.show(translate("DELETED") + " " + export.getId());
      }
      catch (Exception e)
      {
         Notification.show(translate("NOT_DELETED") + " " + export.getId());
      }
   }

   private OccurrenceExportDataProvider setData()
   {
      OccurrenceExportDataProvider dataProvider = new OccurrenceExportDataProvider(
            werbeo);
      view.setDataProvider(dataProvider.withConfigurableFilter());
      return dataProvider;
   }

   private String translate(String field)
   {
      return messagesResource.getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }
}
