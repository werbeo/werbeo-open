package org.infinitenature.werbeo.cms.vaadin.plugin.exportable;

import java.io.InputStream;

import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableObserver;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableView;

public interface OccurrenceExportTableView
      extends TableView<OccurrenceExport, OccurrenceExportFilter>
{
   public enum Columns
   {
      DATE, UUID, STATUS, FORMAT, DOWNLOAD, DELETE;
   }

   public static interface Observer
         extends TableObserver<OccurrenceExportFilter>
   {
      public InputStream onDownloadClick(OccurrenceExport export);
      public void onDeleteClick(OccurrenceExport export);
   }

   public void setSize(int size);
}
