package org.infinitenature.werbeo.cms.vaadin.plugin.exportable;

import java.util.List;
import java.util.Optional;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.WerbeoDataProvider;

import com.vaadin.data.provider.Query;

public class OccurrenceExportDataProvider extends
      WerbeoDataProvider<OccurrenceExport, OccurrenceExportFilter, OccurrenceExportField>
{

   public OccurrenceExportDataProvider(Werbeo werbeo)
   {
      super(werbeo);
   }

   @Override
   protected List<OccurrenceExport> getContent(
         Optional<OccurrenceExportFilter> filter,
         OccurrenceExportField sortField, SortOrder sortOrder, int offset,
         int limit)
   {
      return werbeo.occurrenceExports()
            .findExports(Context.getCurrent().getApp().getPortalId(), offset,
                  limit, sortField, SortOrder.DESC)
            .getContent();
   }

   @Override
   protected OccurrenceExportField toSortField(
         com.vaadin.data.provider.SortOrder<String> vaadinSortOrder)
   {
      return null;
   }

   @Override
   protected OccurrenceExportField getDefaultSortField()
   {
      return OccurrenceExportField.DATE;
   }
   
   

   @Override
   protected int getSize(Query<OccurrenceExport, OccurrenceExportFilter> query)
   {
      return (int) werbeo.occurrenceExports()
            .count(Context.getCurrent().getApp().getPortalId()).getAmount();
   }

}
