package org.infinitenature.werbeo.cms.vaadin.plugin.exportable;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.infinitenature.service.v1.types.JobStatus;
import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.plugin.exportable.OccurrenceExportTableView.Columns;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.AbstractTableViewImpl;
import org.vaadin.viritin.button.DeleteButton;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.label.MLabel;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

public class OccurrenceExportViewImpl extends
      AbstractTableViewImpl<OccurrenceExport, Columns, OccurrenceExportFilter>
      implements OccurrenceExportTableView
{

   private MLabel csvNote = new MLabel();
   public OccurrenceExportViewImpl(Observer observer,
         MessagesResource messagesResource)
   {
      super(Columns.class, OccurrenceExport.class, observer, messagesResource);
      addColumn(OccurrenceExport::getCreationDate, Columns.DATE);
      addColumn(OccurrenceExport::getId, Columns.UUID);
      addColumn(OccurrenceExport::getStatus, Columns.STATUS);
      addColumn(OccurrenceExport::getFileFormat, Columns.FORMAT);
      grid.setHeight("800px");

      addComponentColumn(export -> createDownloadButton(observer, export),
            Columns.DOWNLOAD);
      addComponentColumn(export -> createDeleteButton(observer, export),
            Columns.DELETE);

      layout.addComponentAsFirst(csvNote);
   }

   private Component createDeleteButton(Observer observer,
         OccurrenceExport export)
   {
      DeleteButton deleteButton = new DeleteButton();
      deleteButton.setConfirmWindowCaption("");
      deleteButton.setCancelCaption(translate("CANCEL"));
      deleteButton.setCaption("");
      deleteButton.setIcon(VaadinIcons.TRASH);
      deleteButton.setConfirmationText(export.getId() + "\n" + translate("QUESTION"));
      deleteButton.setStyleName(ValoTheme.BUTTON_TINY);
      deleteButton.withEnabled(JobStatus.RUNNING != export.getStatus());
      deleteButton
            .addClickListener(clickEvent -> observer.onDeleteClick(export));
      return deleteButton;
   }

   private MButton createDownloadButton(Observer observer,
         OccurrenceExport export)
   {
      MButton downloadButton = new MButton(VaadinIcons.DOWNLOAD)
            .withStyleName(ValoTheme.BUTTON_TINY)
            .withEnabled(export.getStatus() == JobStatus.FINISHED);

      StreamResource streamResource = createDownloadStreamResource(export,
            observer);
      FileDownloader fileDownloader = new FileDownloader(streamResource);
      fileDownloader.extend(downloadButton);
      return downloadButton;
   }

   private StreamResource createDownloadStreamResource(OccurrenceExport export,
         Observer observer)
   {
      return new StreamResource(new StreamSource()
      {
         @Override
         public InputStream getStream()
         {
            return observer.onDownloadClick(export);
         }
      }, export.getId() + "." + export.getFileFormat());
   }

   @Override
   protected OccurrenceExportFilter buildFilter()
   {
      return new OccurrenceExportFilter();
   }

   @Override
   protected List<Columns> getSortableColumns()
   {
      return Arrays.asList(Columns.UUID);
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      csvNote.setValue(translate("CSV_NOTE"));
   }
}
