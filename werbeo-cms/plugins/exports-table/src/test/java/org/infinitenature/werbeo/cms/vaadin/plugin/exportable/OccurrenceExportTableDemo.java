package org.infinitenature.werbeo.cms.vaadin.plugin.exportable;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.NavigationTarget;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

public class OccurrenceExportTableDemo extends AbstractTest
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceExportTableDemo.class);
   private ExportTablePlugin plugin;

   public OccurrenceExportTableDemo()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));

      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);


      Werbeo werbeo = mock(Werbeo.class);

      when(werbeo.occurrenceExports())
            .thenReturn(new MockOccurrenceExportResource()
      );
      NavigationResolver resolver = new NavigationResolver()
      {
         @Override
         public void navigateTo(NavigationTarget target)
         {
            LOGGER.info("NavigationTarget: {}", target);
         }
      };

      plugin = new ExportTablePlugin(werbeo, resolver,
            new MessagesResourceMock());
   }

   @Override
   public Component getTestComponent()
   {
      plugin.init(new HashMap<>());
      return plugin.getVaadinComponent();
   }

}
