package org.infinitenature.werbeo.cms.vaadin.plugin.iamap;

import java.util.Map;

import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.BaererTokenProvider;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.IAMapFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IAMapPlugin implements VCMSComponent, IAMapView.Observer
{
   private final Werbeo werbeo;
   private final IAMapView view;
   private final InstanceConfig instanceConfig;
   private final BaererTokenProvider baererTokenProvider;
   public IAMapPlugin(@Autowired MessagesResource messagesResource,
         @Autowired InstanceConfig instanceConfig, @Autowired Werbeo werbeo,
         @Autowired BaererTokenProvider tokenProvider)
   {
      int portalId = Context.getCurrent().getPortal().getPortalId();
      this.werbeo = werbeo;
      this.instanceConfig = instanceConfig;
      this.baererTokenProvider = tokenProvider;
      this.view = new IAMapViewImpl(
            messagesResource, instanceConfig, werbeo.portals()
                  .getPortalConfiguration(portalId).getPortalConfiguration(),
            this);
   }

   @Override
   public Component getVaadinComponent()
   {
      return view;
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      view.setToken(baererTokenProvider.getCurrentToken());
      view.setTaxonDataProvider(new TaxonComboBoxDataProvider(werbeo,
            instanceConfig, false, false, true));
      view.setSurveyComboboxDataProvider(
            new SurveyComboBoxDataProvider(werbeo));
      view.setValidatorComboBoxDataProvider(
            new PersonComboBoxDataProvider(werbeo, true));
      view.setMapFilter(new IAMapFilter());
   }

   @Override
   public void onFilterChange(IAMapFilter filter)
   {
      view.setToken(baererTokenProvider.getCurrentToken());
      view.setMapFilter(filter);
   }

}
