package org.infinitenature.werbeo.cms.vaadin.plugin.iamap;

import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.properties.SampleProperties;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.PersonCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.components.LimitIntegerField;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.ValidationStatusComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.filter.FilterPanel;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.IAMapFilter;
import org.infinitenature.werbeo.cms.vaadin.components.maps.interactive.InteractiveMap;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;

public class IAMapViewImpl extends CustomComponent implements IAMapView
{
   private final MessagesResource messages;
   private final InstanceConfig instanceConfig;

   private final TaxonComboBox taxonComboBox;
   private final ComboBox<SurveyBase> surveyComboBox = new ComboBox<>();
   private final LimitIntegerField blurFilterField = new LimitIntegerField();
   private final ValidationStatusComboBox validationStatusFilterField;
   private final ComboBox<Person> validatorFilterComboBox = new ComboBox<>();
   private final InteractiveMap interactiveMap;
   private final FilterPanel<IAMapFilter> filterPanel;
   private final Observer observer;

   public IAMapViewImpl(MessagesResource messagesResource,
         InstanceConfig instanceConfig, PortalConfiguration portalConfiguration,
         Observer observer)
   {
      this.instanceConfig = instanceConfig;
      this.messages = messagesResource;

      this.taxonComboBox = new TaxonComboBox(messagesResource, true);
      this.surveyComboBox.setItemCaptionGenerator(SurveyBase::getName);
      this.validationStatusFilterField = new ValidationStatusComboBox(
            messagesResource);
      this.validatorFilterComboBox
            .setItemCaptionGenerator(new PersonCaptionGenerator());
      this.interactiveMap = new InteractiveMap(messagesResource,
            portalConfiguration, instanceConfig);
      this.interactiveMap.setHeight("20cm");

      this.filterPanel = new FilterPanel<>(messagesResource, IAMapFilter.class);
      this.filterPanel.addFilter(taxonComboBox, "taxon");
      this.filterPanel.addFilter(surveyComboBox, "survey");
      this.filterPanel.addFilter(blurFilterField, "maxBlur");
      this.filterPanel.addFilter(validatorFilterComboBox, "validator");
      this.filterPanel.addFilter(validationStatusFilterField,
            "validationStatus");
      this.filterPanel.addValueChangeListener(
            event -> observer.onFilterChange(event.getValue()));
      this.observer = observer;

      setCompositionRoot(new MVerticalLayout(filterPanel, interactiveMap));

   }

   @Override
   public void setToken(String token)
   {
      interactiveMap.setToken(token);
   }

   @Override
   public void setTaxonDataProvider(TaxonComboBoxDataProvider provider)
   {
      this.taxonComboBox.setDataProvider(provider);
   }

   @Override
   public void setSurveyComboboxDataProvider(
         SurveyComboBoxDataProvider dataProvider)
   {
      this.surveyComboBox.setDataProvider(dataProvider);
   }

   @Override
   public void setValidatorComboBoxDataProvider(
         PersonComboBoxDataProvider dataProvider)
   {
      this.validatorFilterComboBox.setDataProvider(dataProvider);
   }

   @Override
   public void setMapFilter(IAMapFilter filter)
   {
      interactiveMap.setFilter(filter);
   }

   @Override
   public void attach()
   {
      super.attach();
      applyLocalizations();
   }

   @Override
   public void applyLocalizations()
   {
      surveyComboBox.setCaption(getEntityField(SampleProperties.SURVEY));
      blurFilterField.setCaption(getEntityField(SampleProperties.BLUR));
      validationStatusFilterField
            .setCaption(getEntityField("Occurrence", "VALIDATION.STATUS"));
      validatorFilterComboBox
            .setCaption(getEntityField("Occurrence", "VALIDATION.VALIDATOR"));
   }

   @Override
   public MessagesResource getMessages()
   {
      return this.messages;
   }

}
