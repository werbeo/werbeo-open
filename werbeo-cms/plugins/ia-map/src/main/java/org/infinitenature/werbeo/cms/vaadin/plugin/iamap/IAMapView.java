package org.infinitenature.werbeo.cms.vaadin.plugin.iamap;

import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.map.layer.IAMapFilter;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18N;

public interface IAMapView extends CMSVaadinView, I18N
{
   public static interface Observer
   {
      public void onFilterChange(IAMapFilter filter);
   }

   void setTaxonDataProvider(TaxonComboBoxDataProvider provider);

   void setMapFilter(IAMapFilter filter);

   void setToken(String token);

   void setSurveyComboboxDataProvider(SurveyComboBoxDataProvider dataProvider);

   void setValidatorComboBoxDataProvider(
         PersonComboBoxDataProvider dataProvider);
}
