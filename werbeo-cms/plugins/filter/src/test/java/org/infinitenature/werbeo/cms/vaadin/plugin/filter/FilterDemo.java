package org.infinitenature.werbeo.cms.vaadin.plugin.filter;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.Min;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.PortalField;
import org.infinitenature.service.v1.types.enums.Filter;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.FilterResponse;
import org.infinitenature.service.v1.types.response.PortalBaseResponse;
import org.infinitenature.service.v1.types.response.PortalConfigResponse;
import org.infinitenature.service.v1.types.response.PortalSliceResponse;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

@Theme("flora-mv")
public class FilterDemo extends AbstractTest
{
   private FilterPlugin filterPlugin;

   public FilterDemo()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
      Werbeo werbeo = mock(Werbeo.class);

      Map<Filter, Collection<PortalBase>> filters = new HashMap<>();
      filters.put(Filter.ONLY_OWN_DATA, new ArrayList<>());
      PortalBase portal = new PortalBase();
      portal.setName("Flora-MV");

      filters.get(Filter.ONLY_OWN_DATA).add(portal);
      when(werbeo.portals()).thenReturn(new PortalResource()
      {

         @Override
         public PortalBaseResponse getPortal(@Min(1) int portalId)
         {
            return null;
         }

         @Override
         public FilterResponse getFilter(@Min(1) int portalId)
         {
            return new FilterResponse(filters);
         }

         @Override
         public PortalConfigResponse getPortalConfiguration(@Min(1) int portalId)
         {
            return null;
         }

         @Override
         public PortalSliceResponse findPortals(@Min(0) int offset, int limit,
               PortalField sortField, SortOrder sortOrder)
         {
            return null;
         }

         @Override
         public CountResponse count()
         {
            return null;
         }

         @Override
         public PortalSliceResponse findAssociatedPortals(@Min(1) int portalId)
         {
            return null;
         }
      });
      filterPlugin = new FilterPlugin(new MessagesResourceMock(), werbeo);

      filterPlugin.init(Collections.emptyMap());
   }

   @Override
   public Component getTestComponent()
   {
      return filterPlugin.getVaadinComponent();
   }

}
