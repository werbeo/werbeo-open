package org.infinitenature.werbeo.cms.vaadin.plugin.filter;

import java.util.Map;

import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FilterPlugin implements VCMSComponent
{
   private Werbeo werbeo;
   private FilterPluginView view;

   @Autowired
   public FilterPlugin(MessagesResource messagesResource, Werbeo werbeo)
   {
      this.werbeo = werbeo;
      view = new FilterPluginViewImpl(messagesResource);

   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      view.setFilters(werbeo.portals()
            .getFilter(Context.getCurrent().getPortal().getPortalId())
            .getFilters());
   }

}
