package org.infinitenature.werbeo.cms.vaadin.plugin.filter;

import java.util.*;
import java.util.Map.Entry;

import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.enums.Filter;
import org.infinitenature.werbeo.cms.vaadin.*;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.vaadin.viritin.components.DisclosurePanel;

import com.vaadin.shared.ui.*;
import com.vaadin.ui.Label;

public class FilterPluginViewImpl extends I18NComposite
      implements FilterPluginView
{
   private DisclosurePanel panel;
   private Map<Filter, Collection<PortalBase>> filters = new HashMap<>();

   public FilterPluginViewImpl(MessagesResource messages)
   {
      super(messages);

      panel = new DisclosurePanel();
      setCompositionRoot(panel);
      panel.setMargin(new MarginInfo(false,true));
   }

   @Override
   public void setFilters(Map<Filter, Collection<PortalBase>> filters)
   {
      this.filters = filters;
      if (isAttached())
      {
         updateFilterLabels();
      }
   }

   private void updateFilterLabels()
   {
      panel.getContentWrapper().removeAllComponents();
      for (Entry<Filter, Collection<PortalBase>> entry : filters.entrySet())
      {
         entry.getValue().forEach(portal -> {
            panel.getContentWrapper().add(new Label(getMessages()
                  .getMessage(Context.getCurrent().getPortal(), "Filter.label",
                        getLocale(), portal.getName(), getMessages()
                              .getMessage(Context.getCurrent().getPortal(),
                                    "Filter." + entry.getKey(), getLocale())
                              + getMessages()
                              .getMessage(Context.getCurrent().getPortal(),
                                    "Filter.EMAIL", getLocale())),
                  ContentMode.HTML));
         });
      }
      setVisible(!filters.isEmpty());
   }

   @Override
   public void applyLocalizations()
   {
      panel.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "Filter.caption", getLocale()));
      updateFilterLabels();
   }

}
