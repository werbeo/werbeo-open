package org.infinitenature.werbeo.cms.vaadin.plugin.filter;

import java.util.Collection;
import java.util.Map;

import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.enums.Filter;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface FilterPluginView extends CMSVaadinView
{
   public void setFilters(Map<Filter, Collection<PortalBase>> filters);
}
