package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import java.util.Collection;
import java.util.UUID;

import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.service.v1.types.AbstractComment;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.response.OccurrenceCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SampleCommentsSliceResponse;
import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;
import org.infinitenature.werbeo.cms.vaadin.Preselection;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentUpload;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent;

public interface OccurrenceInputView extends CMSVaadinView
{
   interface Observer
   {
      public void onSaveClick(Sample sample, UUID occurrenceId, Preselection preselection);

      public void onDeleteClick(Sample sample);

      public void onReplyCommentClick(String reply, AbstractComment comment);

      public void onNewCommentClick(String comment, Base<?> entity);
   }

   void setWerbeo(Werbeo werbeo, Person loggedInUser, InstanceConfig instanceConfig);

   void setTopSurveyId(int topSurveyId);


   void setSample(Sample sample, UUID occurrenceId);

   void reset();

   void setHerbariorumClient(HerbariorumClient client);


   Collection<DocumentUpload> getDocumentsToUpload();

   void showProgress();

   void success(String message);

   void failure(String message);

   void removeProgress();

   Collection<Document> getDocumentsToDelete();

   Double getLastMapPositionLat();

   Double getLastMapPositionLon();

   Double getLastMapZoom();

   void setupPositionAndZoom(double lat, double lon, double zoom);

   void preselect(Preselection preselection);

   void setComments(OccurrenceCommentsSliceResponse comments,
         SampleCommentsSliceResponse comments2);

   void setQuarasekClient(Quarasek quarasekClient);

}
