package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Herbarium;
import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.Amount;
import org.infinitenature.service.v1.types.Occurrence.RecordStatus;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatusFukarek;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.VagueDate.VagueDateType;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.service.v1.types.meta.OccurrenceField;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.response.OccurrenceCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SampleCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SampleFieldConfigResponse;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.formatter.PersonFormatter;
import org.infinitenature.werbeo.cms.properties.OccurrenceProperties;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Preselection;
import org.infinitenature.werbeo.cms.vaadin.components.AdviceCombobox;
import org.infinitenature.werbeo.cms.vaadin.components.AmountComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.AreaComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.BloomingSproutsComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.Heading;
import org.infinitenature.werbeo.cms.vaadin.components.LifeStageComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.MakropterComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.PositiveIntegerField;
import org.infinitenature.werbeo.cms.vaadin.components.ProgressIndicator;
import org.infinitenature.werbeo.cms.vaadin.components.PublicationField;
import org.infinitenature.werbeo.cms.vaadin.components.ReproductionComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.SampleMethodComobBox;
import org.infinitenature.werbeo.cms.vaadin.components.SettlementStatusComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.SettlementStatusFukarekComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.SexComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TextFieldWithButton;
import org.infinitenature.werbeo.cms.vaadin.components.VagueDateTimeField;
import org.infinitenature.werbeo.cms.vaadin.components.VitalityComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.comment.CommentedSample;
import org.infinitenature.werbeo.cms.vaadin.components.comment.CommentsField;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentUpload;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsField;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsUploadField;
import org.infinitenature.werbeo.cms.vaadin.components.form.FormUtils;
import org.infinitenature.werbeo.cms.vaadin.components.indexherbariorum.HerbariorumField;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent;
import org.infinitenature.werbeo.cms.vaadin.components.maps.PositionTypeChangeListener;
import org.infinitenature.werbeo.cms.vaadin.components.numericamount.NumericAmountWithAccuracy;
import org.infinitenature.werbeo.cms.vaadin.components.numericamount.NumericAmountWithAccuracyField;
import org.infinitenature.werbeo.cms.vaadin.fieldconfig.FieldConfigUtils;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.infinitenature.werbeo.cms.vaadin.notification.NotificationUtils;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MGridLayout;

import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.jarektoro.responsivelayout.ResponsiveRow.SpacingSize;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractSingleSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class OccurrenceInputViewImpl extends I18NComposite
      implements OccurrenceInputView, PositionTypeChangeListener

{
   private static final String STYLE_OCCURRENCE_INPUT_FIELD = "occurrence-input-field";
   private MessagesResource messagesResource = new MessagesResourceMock();
   private OccurrenceInputView.Observer observer;
   private int topSurveyId = 14;
   private GeometryHelper geometryHelper = new GeometryHelper();

   private Sample sample;

   // General
   private VerticalLayout mainLayout = new VerticalLayout();

   // private GridLayout mandatoryInputLayout = new GridLayout(2, 2);

   private TaxonComboBox taxonField;
   private TextField determinationCommentField = new TextField();

   private Window preselectionWindow;
   private CheckBox locationCheckbox;
   private CheckBox locationTextCheckbox;
   private CheckBox dateCheckbox;
   private CheckBox habitatCheckbox;
   private CheckBox observersCheckbox;
   private CheckBox amountCheckbox;
   private Button preselectButton;
   private Button saveWithoutPreselectionButton;
   private Label preselectionText;

   private VagueDateTimeField dateTimeButton;
   private CommentsField commentsField;

   private TextFieldWithButton dateField;
   private ComboBox<Person> determinerField = new ComboBox<>();
   private ComboBox<Person> recorderField = new ComboBox<>();
   private TextField findersField = new TextField("Mitbeobachter");

   private ComponentContainer comments;

   // Status
   private AdviceCombobox<SettlementStatusFukarek> adviceStatusCombobox;
   private SettlementStatusComboBox settlementStatusField;
   private VitalityComboBox vitalityField;
   private SexComboBox sexField;
   private ReproductionComboBox reproductionField;
   private SampleMethodComobBox sampleMethodField;
   private LifeStageComboBox lifeStageField;
   private MakropterComboBox makropterField;


   private AreaComboBox areaField = new AreaComboBox(messagesResource);
   private BloomingSproutsComboBox bloomingSproutsField = new BloomingSproutsComboBox(
         messagesResource);
   private PositiveIntegerField numericAmmountField = new PositiveIntegerField();
   private NumericAmountWithAccuracyField numericAmountWithAccuracyField;
   private AdviceCombobox<Amount> amountField;

   // Document
   private HerbariorumField herbariorumField;
   private TextField herbaryNumber = new TextField("Herbarnr./Ablageort");
   private DocumentsUploadField occurrenceDocumentUploadField = new DocumentsUploadField(
         messagesResource);
   private DocumentsField occurrenceDocumentsField = new DocumentsField(
         messagesResource);
   
   private PublicationField publicationField = new PublicationField();
   private MapEditComponent mapField;
   private TextField blurField = new TextField();
   private TextField locationCommentField = new TextField(
         "Fundortbeschreibung");
   private TextField localityField = new TextField("Fundort");
   private TextField habitatField = new TextField("Wuchsort/Habitat");

   private Button saveButtonTop = new MButton("Speichern", e -> tryToSave());
   private Button saveButtonBottom = new MButton("Speichern", e -> tryToSave());
   private Button deleteButtonTop = new MButton(VaadinIcons.TRASH, "Löschen",
         e -> observer.onDeleteClick(sample)).withVisible(false);
   private Button deleteButtonBottom = new MButton(VaadinIcons.TRASH, "Löschen",
         e -> observer.onDeleteClick(sample)).withVisible(false);
   private Person currentPerson;
   private Tab personTab;
   private Tab quantiyTab;
   private Tab publicationTab;
   private Tab statusTab;
   private Tab habitatTab;
   private Tab herbaryTab;
   private Tab commentTab;
   private Tab remarkTab;
   private Tab attachmentsTab;
   private Heading mandatoryHeader = new Heading();
   private Heading optionalHeader = new Heading();
   private final Map<SampleField, Boolean> sampleFieldConfigs;
   private final Map<OccurrenceField, Boolean> occurrenceFieldConfigs;
   private Occurrence occurrence;
   private ProgressIndicator progressIndicator;
   private Window progressWindow;
   private TextArea remark;
   private TabSheet tabSheet;

   public OccurrenceInputViewImpl(Sample sample, OccurrenceInputView.Observer observer,
         MessagesResource messagesResource,
         SampleFieldConfigResponse sampleFieldConfig,
         Set<OccurrenceFieldConfig> occurrenceFieldConfig,
         PortalConfiguration portalConfiguration)
   {
      super(messagesResource);
      this.observer = observer;
      this.sampleFieldConfigs = FieldConfigUtils
            .toSampleConfigMap(sampleFieldConfig.getFieldConfigs());
      this.occurrenceFieldConfigs = FieldConfigUtils
            .toOccurrenceConfigMap(occurrenceFieldConfig);
      this.dateTimeButton = new VagueDateTimeField(messagesResource,
            null, null, occurrenceFieldConfigs, true);
      dateField = new TextFieldWithButton(
            "Datum der Aufnahme", "mit Klick auf Kalender wählen",
            VaadinIcons.CALENDAR, dateTimeButton);
      this.mapField = new MapEditComponent(
            "https://wms.test.infinitenature.org/geoserver/werbeo/wms", PositionType.POINT,
            portalConfiguration, false, PositionType.POINT, PositionType.SHAPE, PositionType.MTB);

      this.adviceStatusCombobox = new AdviceCombobox<Occurrence.SettlementStatusFukarek>(
            messagesResource, new SettlementStatusFukarekComboBox(
                  messagesResource));
      this.settlementStatusField = new SettlementStatusComboBox(
            messagesResource);
      this.sexField = new SexComboBox(messagesResource);
      this.lifeStageField = new LifeStageComboBox(messagesResource);
      this.makropterField = new MakropterComboBox(messagesResource);
      this.sampleMethodField = new SampleMethodComobBox(messagesResource,
            sampleFieldConfig);
      this.reproductionField = new ReproductionComboBox(messagesResource);
      this.commentsField = new CommentsField(getMessages(),
            observer::onReplyCommentClick, observer::onNewCommentClick);

      this.vitalityField = new VitalityComboBox(messagesResource);
      this.taxonField = new TaxonComboBox(messagesResource, false);
      this.numericAmountWithAccuracyField = new NumericAmountWithAccuracyField(
            messagesResource);
      this.amountField = new AdviceCombobox<Amount>(messagesResource, new AmountComboBox(messagesResource));
      dateTimeButton.addValueChangeListener(event -> {
         if (this.occurrenceFieldConfigs.containsKey(OccurrenceField.TIME_OF_DAY))
         {
            dateField.getTextField()
                  .setValue(VagueDate.format(dateTimeButton.getValue(), false)
                        + (dateTimeButton.getTimeValue() == null ? ""
                              : (" " + dateTimeButton.getTimeValue())));
         }
         else
         {
            dateField.getTextField()
                  .setValue(VagueDate.format(dateTimeButton.getValue(), false));
         }
      });
      mainLayout.setSizeFull();
      dateTimeButton.setStyleName("filter-geo-button");
      HorizontalLayout topButtonLayout = new HorizontalLayout();
      topButtonLayout.addComponents(deleteButtonTop, saveButtonTop);
      topButtonLayout.addStyleName("align-right");
      HorizontalLayout topLayout = new HorizontalLayout();
      topLayout.addComponent(mandatoryHeader);
      topLayout.addComponent(topButtonLayout);
      topLayout.addStyleName("input-top-line");
      remark = new TextArea("");
      remark.setWidth("500px");

      mainLayout.addComponent(topLayout);
      mainLayout.addComponent(createMandatoryPanel());
      mainLayout.addComponent(optionalHeader);
      mainLayout.addComponent(createOptionalPanel());
      mainLayout.setMargin(new MarginInfo(false, true));

      HorizontalLayout bottomButtonLayout = new HorizontalLayout();
      bottomButtonLayout.addComponents(deleteButtonBottom, saveButtonBottom);
      bottomButtonLayout.addStyleName("align-right");
      mainLayout.addComponent(bottomButtonLayout);
      setCompositionRoot(mainLayout);


      preselectionWindow = new Window();
      locationCheckbox = new CheckBox("location");
      locationCheckbox.setValue(true);
      locationTextCheckbox = new CheckBox("locationText");
      locationTextCheckbox.setValue(true);
      dateCheckbox = new CheckBox("date");
      dateCheckbox.setValue(true);
      habitatCheckbox = new CheckBox("habitat");
      habitatCheckbox.setValue(true);
      observersCheckbox = new CheckBox("habitat");
      observersCheckbox.setValue(true);
      amountCheckbox = new CheckBox("amount");
      amountCheckbox.setValue(true);
      preselectButton = new Button("preselect & save");
      preselectionText = new Label();
      preselectionText.addStyleName("v-window-header");
      saveWithoutPreselectionButton = new Button("only save");
      

      initValues();
      this.sample = sample;
   }

   protected void tryToSave()
   {
      boolean valid = prepareSampleForSaving();
      if (valid)
      {

         // preselectionDialog only for new samples
         if(sample.getEntityId() == null)
         {
            // show dialog, for choosing preselection-values
            setupAndShowPreselectionWindow();
         }
         else
         {
            // Just save
            observer.onSaveClick(this.sample, occurrence.getEntityId(), null);
         }
      }
   }

   private void setupAndShowPreselectionWindow()
   {
      preselectButton.addClickListener(click -> {
         Preselection preselection = new Preselection();
         if(dateCheckbox.getValue().booleanValue())
         {
            preselection.setDate(this.sample.getDate());
         }
         if(locationCheckbox.getValue().booleanValue())
         {
            preselection.setLocality(new Locality());
            preselection.getLocality().setBlur(sample.getLocality().getBlur());
            preselection.getLocality().setPosition(sample.getLocality().getPosition());
         }
         if(locationTextCheckbox.getValue().booleanValue())
         {
            if(preselection.getLocality() == null)
            {
               preselection.setLocality(new Locality());
            }
            preselection.getLocality().setLocality(sample.getLocality().getLocality());
            preselection.getLocality().setLocationComment(sample.getLocality().getLocationComment());
         }
         if(habitatCheckbox.getValue().booleanValue())
         {
            preselection.setHabitat(sample.getOccurrences().get(0).getHabitat());
         }
         if(observersCheckbox.getValue().booleanValue())
         {
            preselection.setObservers(sample.getOccurrences().get(0).getObservers());
         }
         if(amountCheckbox.getValue().booleanValue())
         {
            preselection.setAmount(sample.getOccurrences().get(0).getAmount());
         }

         observer.onSaveClick(this.sample, occurrence.getEntityId(), preselection);
         preselectionWindow.close();
      });

      saveWithoutPreselectionButton.addClickListener(click ->
      {
         observer.onSaveClick(this.sample, occurrence.getEntityId(), null);
         preselectionWindow.close();
      });

      HorizontalLayout buttonRow = new HorizontalLayout(saveWithoutPreselectionButton, preselectButton);
      VerticalLayout layout = new VerticalLayout(preselectionText, dateCheckbox,
            locationCheckbox, locationTextCheckbox, habitatCheckbox,
            observersCheckbox,amountCheckbox, buttonRow);
      layout.setMargin(true);
      preselectionWindow.setContent(layout);
      preselectionWindow.setWidth("570px");
      preselectionWindow.setHeight("360px");
      preselectionWindow.center();
      UI.getCurrent().addWindow(preselectionWindow);
   }

   private Component createOptionalPanel()
   {
      Panel panel = new Panel();

      tabSheet = new TabSheet();
      tabSheet.addStyleName("margin-left");
      tabSheet.addStyleName("occurrence-input-tabsheet");
      personTab = addTab(tabSheet, createPersonsLayout());
      quantiyTab = addTab(tabSheet, createQuantityLayout());
      statusTab = addTab(tabSheet, createStatusLayout());
      habitatTab = addTab(tabSheet, createLocationLayout());
      herbaryTab = addTab(tabSheet, createDocumentLayout());
      this.comments = createCommentsLayout();
      remarkTab = addTab(tabSheet,createRemarkLayout());
      commentTab = addTab(tabSheet, createCommentsLayout());
      attachmentsTab = addTab(tabSheet, createPictureLayout());
      publicationTab = addTab(tabSheet, createPublicationLayout());
      panel.setContent(tabSheet);
      return panel;
   }

   private Tab addTab(TabSheet sheet, ComponentContainer tabContent)
   {
      Tab tab = sheet.addTab(tabContent);
      if (tabContent.getComponentCount() == 0)
      {
         sheet.removeTab(tab);
      } else
      {
         Component firstComponent = tabContent.iterator().next();
         if (firstComponent instanceof ResponsiveRow)
         {
            ResponsiveRow row = (ResponsiveRow) firstComponent;
            if (row.getComponentCount() == 0)
            {
               sheet.removeTab(tab);
            }
         }
      }
      return tab;
   }

   private ComponentContainer createCommentsLayout()
   {
      ResponsiveLayout commentsLayout = FormUtils.createFromLayout();
      ResponsiveRow row = FormUtils.createWideRow(commentsLayout);
      row.setMargin(true);
      
      row.addComponent(commentsField);
      return row;

   }
   
   private ComponentContainer createRemarkLayout()
   {
      GridLayout layout = new MGridLayout(2, 1);
      layout.setMargin(true);
      layout.setSpacing(true);
      layout.addComponent(remark);
      return layout;
   }

   private ComponentContainer createPictureLayout()
   {
      GridLayout layout = new MGridLayout(2, 1);

      addConditionaly(layout, occurrenceDocumentUploadField,
            OccurrenceField.MEDIA);
      addConditionaly(layout, occurrenceDocumentsField, OccurrenceField.MEDIA);
      return layout;
   }

   private ComponentContainer createDocumentLayout()
   {
      HorizontalLayout layout = new HorizontalLayout();
      layout.setMargin(true);
      VerticalLayout vlayout = new VerticalLayout();
      herbariorumField = new HerbariorumField(messagesResource);

      if (occurrenceFieldConfigs.containsKey(OccurrenceField.HERBARIUM))
      {
         vlayout.addComponent(herbariorumField);
         vlayout.addComponent(herbaryNumber);
         layout.addComponent(vlayout);
      }
      herbaryNumber.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);

      return layout;
   }

   private ComponentContainer createLocationLayout()
   {
      ResponsiveLayout layout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = layout.addRow();
      row.setMargin(true);
      row.setSpacing(true);

      addConditionaly(row, habitatField, OccurrenceField.HABITAT);
      habitatField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      return layout;
   }
   
   private ComponentContainer createPublicationLayout()
   {
      ResponsiveLayout layout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = layout.addRow();
      row.setMargin(true);
      row.setSpacing(true);
      row.addComponent(publicationField);
      return layout;
   }

   private ComponentContainer createStatusLayout()
   {
      ResponsiveLayout layout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = layout.addRow();
      row.setMargin(true);
      row.setSpacing(true);

      addConditionaly(row, settlementStatusField,
            OccurrenceField.SETTLEMENT_STATUS);
      addConditionaly(row, sexField, OccurrenceField.SEX);
      addConditionaly(row, reproductionField, OccurrenceField.REPRODUCTION);
      addConditionaly(row, sampleMethodField, SampleField.SAMPLE_METHOD);
      addConditionaly(row, adviceStatusCombobox,
            OccurrenceField.SETTLEMENT_STATUS_FUKAREK);
      addConditionaly(row, vitalityField, OccurrenceField.VITALITY);
      addConditionaly(row, lifeStageField, OccurrenceField.LIFE_STAGE);
      addConditionaly(row, makropterField, OccurrenceField.MAKROPTER);

      adviceStatusCombobox.setComboboxStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      vitalityField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      sexField.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      reproductionField.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      sampleMethodField.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      lifeStageField.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      makropterField.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      return layout;
   }

   private ComponentContainer createQuantityLayout()
   {
      ResponsiveLayout layout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = layout.addRow();
      row.setMargin(true);
      row.setSpacing(true);

      bloomingSproutsField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      areaField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      numericAmmountField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      amountField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      addConditionaly(row, areaField, OccurrenceField.AREA);
      addConditionaly(row, bloomingSproutsField,
            OccurrenceField.BLOOMING_SPROUTS);
      if (!addConditionaly(row, numericAmountWithAccuracyField,
            OccurrenceField.NUMERIC_AMOUNT,
            OccurrenceField.NUMERIC_AMOUNT_ACCURACY))
      {
         addConditionaly(row, numericAmmountField, OccurrenceField.NUMERIC_AMOUNT);
      }
      addConditionaly(row, amountField, OccurrenceField.AMOUNT);
      return layout;
   }

   private boolean addConditionaly(ComponentContainer componentContainer,
         AbstractField<?> component, OccurrenceField... fields)
   {
      boolean containsAll = false;
      for (OccurrenceField field : fields)
      {
         if (occurrenceFieldConfigs.containsKey(field))
         {
            containsAll = true;
         } else
         {
            containsAll = false;
            break;
         }
      }
      if (containsAll)
      {
         componentContainer.addComponent(component);
         component
               .setRequiredIndicatorVisible(
                     occurrenceFieldConfigs.get(fields[0]));
         return true;
      }
      return false;
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractSingleSelect<?> component, OccurrenceField field)
   {
      if (occurrenceFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component
               .setRequiredIndicatorVisible(occurrenceFieldConfigs.get(field));
      }
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractField<?> component, SampleField field)
   {
      if (sampleFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component.setRequiredIndicatorVisible(sampleFieldConfigs.get(field));
      }
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractSingleSelect<?> component, SampleField field)
   {
      if (sampleFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component.setRequiredIndicatorVisible(sampleFieldConfigs.get(field));
      }
   }

   private ComponentContainer createPersonsLayout()
   {
      ResponsiveLayout layout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = layout.addRow();
      row.setMargin(true);
      row.setSpacing(true);

      determinerField.setItemCaptionGenerator(PersonFormatter::format);
      determinerField.setPopupWidth("auto");
      determinerField.setCaption("Bestimmer");
      // LoggedIn User as Determiner can not be changed
      determinerField.setEnabled(Context.getCurrent().isAdmin());
      determinerField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);

      recorderField.setItemCaptionGenerator(PersonFormatter::format);
      recorderField.setPopupWidth("auto");
      recorderField.setCaption("Beobachter");
      // LoggedIn User as Determiner can not be changed
      recorderField.setEnabled(Context.getCurrent().isAdmin());
      recorderField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);

      findersField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);

      addConditionaly(row, determinerField, OccurrenceField.DETERMINER);
      addConditionaly(row, recorderField, SampleField.RECORDER);
      addConditionaly(row, findersField, OccurrenceField.OBSERVERS);
      return layout;
   }

   private Component createMandatoryPanel()
   {
      ResponsiveLayout resLayout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = resLayout.addRow();
      row.setSpacing(SpacingSize.SMALL, true);

      Panel panel = new Panel();
      VerticalLayout layout = new VerticalLayout();

      taxonField.setItemCaptionGenerator(TaxonBase::getName);
      taxonField.setPopupWidth("auto");
      taxonField.setCaption("Art");
      taxonField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      dateTimeButton.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      locationCommentField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      // dateField.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      dateField.setWidth("100%");
      dateField.addStyleName("with-border");
      dateField.getTextField().setEnabled(false);


      blurField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      blurField.setCaption("Ungenauigkeit der Ortsangabe in m");
      blurField.setRequiredIndicatorVisible(true);
      blurField.addValueChangeListener(event ->
      {
         try
         {
            mapField.setBlur(Integer.valueOf(blurField.getValue()));
         } catch (Exception e)
         {
         }
      });
      localityField.setStyleName(STYLE_OCCURRENCE_INPUT_FIELD);
      localityField.setDescription(
            "Bsp.: Peenetal bei Gützkow oder Galgenberg bei Franzburg");
      mapField.setHeight("800px");
      mapField.setWidth("100%");
      mapField.setPositionTypeChangeListener(this);

      determinationCommentField.addStyleName(STYLE_OCCURRENCE_INPUT_FIELD);

      addConditionaly(row, taxonField, OccurrenceField.TAXON);
      row.addComponents(dateField);
      addConditionaly(row, determinationCommentField,
            OccurrenceField.DETERMINATION_COMMENT);
      row.addComponents(mapField.getControlFields());
      row.addComponents(blurField);
      addConditionaly(row, localityField, SampleField.LOCALITY);
      addConditionaly(row, locationCommentField, SampleField.LOCATION_COMMENT);

      layout.addComponent(resLayout);
      GridLayout mapLayout = new GridLayout(1, 1);
      mapLayout.setMargin(new MarginInfo(false, false));
      mapLayout.setSizeFull();
      mapLayout.addComponent(mapField);
      layout.addComponent(mapLayout);
      layout.addComponent(new GridLayout(1, 1));
      layout.setSpacing(false);
      panel.setContent(layout);
      return panel;

   }

   private void refreshMandatoryLayout()
   {

   }

   private void initValues()
   {
      VagueDate date = new VagueDate();
      date.setFrom(LocalDate.now());
      date.setType(VagueDateType.DAY);
      dateTimeButton.setValue(date);
      if (this.occurrenceFieldConfigs.containsKey(OccurrenceField.TIME_OF_DAY))
      {
         dateTimeButton.setTimeValue(null);// do not force user to delete time, if they do not have time value
         dateField.getTextField()
               .setValue(VagueDate.format(dateTimeButton.getValue(), false));
      }
      else
      {
         dateField.getTextField()
               .setValue(VagueDate.format(dateTimeButton.getValue(), false));
      }

      taxonField.setRequiredIndicatorVisible(true);
      mapField.addValueChangeListener(event ->
      {
         int precision = 0;

         if (mapField.getValue().getType().equals(PositionType.POINT)
               && StringUtils.isNotBlank(blurField.getValue()))
         {
            precision = Integer.valueOf(blurField.getValue());
         }
         else
         {
            precision = geometryHelper.calculatePrecision(
                  mapField.getValue().getWkt(), mapField.getValue().getEpsg());
         }
         if(precision > 0)
         {
            blurField.setValue("" + precision

               );
         }
         else
         {
            blurField.setValue("");
         }
         if(StringUtils.isNotBlank(blurField.getValue()))
         {
            mapField.setBlur(Integer.valueOf(blurField.getValue()));
         }
      });
   }

   private boolean prepareSampleForSaving()
   {
      Person determiner = determinerField.getValue() != null
            ? determinerField.getValue()
            : currentPerson;

      Person finder = recorderField.getValue() != null
            ? recorderField.getValue()
            : currentPerson;

      if (!occurrenceDocumentUploadField.isValid())
      {
         NotificationUtils.showError("", "Die Anhänge sind nicht korrekt");
         return false;
      }
      if (mapField.getValue() == null)
      {
         NotificationUtils.showError("",
               "Bitte einen Fundort in der Karte  angeben !");
         return false;
      }

      if (taxonField.getValue() == null)
      {
         NotificationUtils.showError("", "Bitte ein Taxon auswählen !");
         return false;
      }

      Locality locality = new Locality();
      locality.setPosition(mapField.getValue());

      if (blurField.getValue().isEmpty())
      {
         NotificationUtils.showError("",
               "Bitte 'Ungenauigkeit der Ortsangabe' angeben !");
         return false;
      } else
      {
         try
         {
            int precision = Integer.parseInt(blurField.getValue());
            if (precision > 450000)
            {
               NotificationUtils.showError("",
                     "Bitte 'Ungenauigkeit der Ortsangabe' mit maximal 450000 angeben !");
               return false;
            }
            else if (precision == 0)
            {
               NotificationUtils.showError("",
                     "Bitte 'Ungenauigkeit der Ortsangabe' mit mindestens 1 angeben !");
               return false;
            }
         } catch (NumberFormatException e)
         {
            NotificationUtils.showError("",
                  "Bitte 'Ungenauigkeit der Ortsangabe' als Zahl angeben !");
            return false;
         }
      }

      locality.setBlur(Integer.valueOf(blurField.getValue()));
      if (sampleFieldConfigs.containsKey(SampleField.LOCATION_COMMENT))
      {
         locality.setLocationComment(locationCommentField.getValue());
      }
      if (sampleFieldConfigs.containsKey(SampleField.LOCALITY))
      {
         locality.setLocality(this.localityField.getValue());
      }


      sample.setLocality(locality);

      sample.setRecorder(finder);
      if(sample.getSurvey() == null)
      {
         sample.setSurvey(prepapreSurvey());
      }

      if (sample.getOccurrences().isEmpty())
      {
         occurrence = new Occurrence();
         occurrence.setEntityId(UUID.randomUUID());
         sample.getOccurrences().add(occurrence);
      }
      if (occurrenceFieldConfigs
            .containsKey(OccurrenceField.DETERMINATION_COMMENT))
      {
         occurrence
               .setDeterminationComment(determinationCommentField.getValue());
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.NUMERIC_AMOUNT)
            && occurrenceFieldConfigs
                  .containsKey(OccurrenceField.NUMERIC_AMOUNT_ACCURACY))
      {
    	  if(numericAmountWithAccuracyField.getValue() != null)
    	  {
	         occurrence.setNumericAmount(
	               numericAmountWithAccuracyField.getValue().getAmount());
	         occurrence.setNumericAmountAccuracy(
	               numericAmountWithAccuracyField.getValue().getAccuracy());
    	  }
      } else
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.NUMERIC_AMOUNT))
      {
         occurrence.setNumericAmount(numericAmmountField.getValue());
      }

      occurrence.setCoveredArea(areaField.getValue());
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setBloomingSprouts(bloomingSproutsField.getValue());
      occurrence.setSex(sexField.getValue());
      occurrence.setLifeStage(lifeStageField.getValue());
      occurrence.setMakropter(makropterField.getValue());
      occurrence.setReproduction(reproductionField.getValue());
      sample.setSampleMethod(sampleMethodField.getValue());
      sample.setDate(dateTimeButton.getValue());
      occurrence.setTimeOfDay(dateTimeButton.getTimeValue());
      occurrence.setDeterminer(determiner);
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.OBSERVERS))
      {
         occurrence.setObservers(findersField.getValue());
      }
      
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.REMARK))
      {
         occurrence.setRemark(remark.getValue());
      }

      if (herbariorumField.getValue() != null)
      {
         if (StringUtils.isBlank(herbaryNumber.getValue()))
         {
            NotificationUtils.showError("",
                  "Bitte Herbarnr./Ablageort angeben !");
            return false;
         }

         Herbarium herbarium = new Herbarium();
         herbarium.setCode(herbariorumField.getValue().getCode());
         herbarium.setHerbary(herbaryNumber.getValue());
         occurrence.setHerbarium(herbarium);
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.HABITAT))
      {
         occurrence.setHabitat(habitatField.getValue());
      }
      occurrence.setSettlementStatusFukarek(
            adviceStatusCombobox.getValue());
      occurrence.setSettlementStatus(settlementStatusField.getValue());
      occurrence.setTaxon(taxonField.getValue());
      occurrence.setVitality(vitalityField.getValue());
      occurrence.setAmount(amountField.getValue());

      return true;
   }

   private SurveyBase prepapreSurvey()
   {
      SurveyBase surveyBase = new SurveyBase();
      surveyBase.setEntityId(topSurveyId);
      return surveyBase;
   }

   @Override
   public void setWerbeo(Werbeo werbeo, Person loggedInUser, InstanceConfig instanceConfig)
   {
      this.currentPerson = loggedInUser;
      taxonField.setDataProvider(new TaxonComboBoxDataProvider(werbeo,
            instanceConfig, false, true, true));
      determinerField.setDataProvider(new PersonComboBoxDataProvider(werbeo));
      recorderField.setDataProvider(new PersonComboBoxDataProvider(werbeo));
   }

   @Override
   public void setTopSurveyId(int topSurveyId)
   {
      this.topSurveyId = topSurveyId;
   }

   @Override
   public void preselect(Preselection preselection)
   {
      if (preselection.getDate() != null)
      {
         dateTimeButton.setValue(preselection.getDate());
         dateField.getTextField()
               .setValue(VagueDate.format(dateTimeButton.getValue(), false));
      }

      if(preselection.getLocality() != null)
      {
         if(preselection.getLocality().getLocality() != null)
         {
            localityField
            .setValue(preselection.getLocality().getLocality());
         }
         if(preselection.getLocality().getLocationComment() != null)
         {
            locationCommentField
            .setValue(preselection.getLocality().getLocationComment());
         }
         if(preselection.getLocality().getBlur() != null)
         {
            blurField.setValue(
                  String.valueOf(preselection.getLocality().getBlur()));
            mapField.setBlur(Integer.valueOf(blurField.getValue()));
         }
         if(preselection.getLocality().getPosition() != null)
         {
            mapField.setValue(preselection.getLocality().getPosition());
         }
      }

      if(preselection.getHabitat() != null )
      {
         habitatField
               .setValue(preselection.getHabitat());
      }
      if(preselection.getObservers() != null )
      {
         findersField.setValue(preselection.getObservers());
      }
      if(preselection.getAmount() != null)
      {
         amountField.setValue(preselection.getAmount());      
      }
   }


   @Override
   public void setSample(Sample sample, UUID occurrenceId)
   {
      this.sample = sample;
      occurrence = select(sample.getOccurrences(), occurrenceId);

      if (sample != null && !sample.getOccurrences().isEmpty()
            && sample.getOccurrences().get(0).getDeterminer() != null)
      {
         determinerField
               .setValue(sample.getOccurrences().get(0).getDeterminer());
      } else if (determinerField.getValue() == null)
      {
         determinerField.setValue(currentPerson);
      }

      if (sample != null && sample.getRecorder() != null)
      {
         recorderField.setValue(sample.getRecorder());
      } else if (recorderField.getValue() == null)
      {
         recorderField.setValue(currentPerson);
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.NUMERIC_AMOUNT)
            && occurrenceFieldConfigs
                  .containsKey(OccurrenceField.NUMERIC_AMOUNT_ACCURACY))
      {
         numericAmountWithAccuracyField.setValue(
               NumericAmountWithAccuracy.of(occurrence.getNumericAmount(),
                     occurrence.getNumericAmountAccuracy()));
      } else if (occurrence.getNumericAmount() != null)
      {
         numericAmmountField.setValue(occurrence.getNumericAmount());
      }
      if (occurrence.getDeterminationComment() != null)
      {
         determinationCommentField
               .setValue(occurrence.getDeterminationComment());
      }
      areaField.setValue(occurrence.getCoveredArea());
      bloomingSproutsField.setValue(occurrence.getBloomingSprouts());
      dateTimeButton.setValue(sample.getDate());
      if (this.occurrenceFieldConfigs.containsKey(OccurrenceField.TIME_OF_DAY))
      {
         dateTimeButton.setTimeValue(occurrence.getTimeOfDay());
         dateField.getTextField()
               .setValue(VagueDate.format(dateTimeButton.getValue(), false)
                     + (dateTimeButton.getTimeValue() == null ? ""
                           : (" " + dateTimeButton.getTimeValue())));
      }
      else
      {
         dateField.getTextField()
               .setValue(VagueDate.format(dateTimeButton.getValue(), false));
      }
      if (occurrence.getObservers() != null)
      {
         findersField.setValue(occurrence.getObservers());
      }
      if (occurrence.getHerbarium() != null)
      {
         if (herbariorumField.getClient() != null
               && herbariorumField.getClient().isServiceAvailable())
         {
            herbariorumField.setValue(herbariorumField.getClient()
                  .getByCode(occurrence.getHerbarium().getCode()));
         }
         if (occurrence.getHerbarium().getHerbary() != null)
         {
            herbaryNumber.setValue(occurrence.getHerbarium().getHerbary());
         }
      }

      mapField.setValue(sample.getLocality().getPosition());
      blurField.setValue(
            String.valueOf(sample.getLocality().getBlur()));
      mapField.setBlur(Integer.valueOf(blurField.getValue()));
      if (sample.getLocality().getLocationComment() != null)
      {
         locationCommentField.setValue(
               sample.getLocality().getLocationComment());
      }
      if (sample.getLocality().getLocality() != null)
      {
         localityField
               .setValue(sample.getLocality().getLocality());
      }
      if (occurrence.getHabitat() != null)
      {
         habitatField
               .setValue(occurrence.getHabitat());
      }

      adviceStatusCombobox.setValue(occurrence.getSettlementStatusFukarek());
      settlementStatusField.setValue(occurrence.getSettlementStatus());
      taxonField.setValue(occurrence.getTaxon());
      vitalityField.setValue(occurrence.getVitality());
      sexField.setValue(occurrence.getSex());
      lifeStageField.setValue(occurrence.getLifeStage());
      makropterField.setValue(occurrence.getMakropter());
      reproductionField.setValue(occurrence.getReproduction());
      amountField.setValue(occurrence.getAmount());
      sampleMethodField.setValue(sample.getSampleMethod());
      saveButtonTop.setVisible(
            sample.getAllowedOperations().contains(Operation.UPDATE));
      saveButtonBottom.setVisible(
            sample.getAllowedOperations().contains(Operation.UPDATE));
      deleteButtonTop.setVisible(
            sample.getAllowedOperations().contains(Operation.DELETE));
      deleteButtonBottom.setVisible(
            sample.getAllowedOperations().contains(Operation.DELETE));
      occurrenceDocumentsField.setValue(occurrence.getDocuments().stream()
            .filter(d -> d.getType() == DocumentType.IMAGE
                  || d.getType() == DocumentType.AUDIO)
            .collect(Collectors.toList()));
      occurrenceDocumentsField
            .setAllowedOperations(sample.getAllowedOperations());
      if(occurrence.getRemark() != null )
      {
         remark.setValue(occurrence.getRemark());
      }
      
      if(StringUtils.isNotBlank(occurrence.getCiteId()))
      {
         publicationField.setPublication(occurrence.getCiteId(), occurrence.getCiteComment());
         publicationTab.setVisible(true);
      }
      else
      {
         publicationTab.setVisible(false);
      }
      
      
         
      refreshMandatoryLayout();
   }

   private Occurrence select(List<Occurrence> occurrences, UUID occurrenceId)
   {
      for (Occurrence occurrence : occurrences)
      {
         if (occurrenceId.equals(occurrence.getEntityId()))
         {
            return occurrence;
         }
      }
      return null;
   }

   @Override
   public void reset()
   {
      sample = new Sample();
      determinerField.setValue(currentPerson);
      recorderField.setValue(currentPerson);
   }

   @Override
   public void setHerbariorumClient(HerbariorumClient client)
   {
      this.herbariorumField.init(client);
   }
   
   @Override
   public void setQuarasekClient(Quarasek quarasekClient)
   {
      publicationField.setQuarasekClient(quarasekClient);
   }


   @Override
   public void applyLocalizations()
   {
      Locale locale = this.getLocale();
      org.infinitenature.werbeo.cms.vaadin.Portal portal = Context.getCurrent()
            .getPortal();

      personTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.person", locale));
      quantiyTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.quantity", locale));
      statusTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.status", locale));
      habitatTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.habitat", locale));
      publicationTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.publication", locale));
      herbaryTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.herbary", locale));
      commentTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.comment", locale));
      attachmentsTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.attachment", locale));
      mandatoryHeader.setValue(getMessages().getMessage(portal,
            "group.mandantory.header", locale));
      optionalHeader.setValue(
            getMessages().getMessage(portal, "group.optional.header", locale));
      determinationCommentField.setCaption(getMessages().getEntityField(portal,
            "Occurrence", "DETERMINATION_COMMENT", locale));

      adviceStatusCombobox.setEmptySelectionCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "SettlementStatusFukarekComboBox", "EMPTY", getLocale()));
      adviceStatusCombobox.setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "SettlementStatusFukarekComboBox", "OCCURRENCE_STATUS", getLocale()));
      adviceStatusCombobox.setAdviceHtmlText(getMessages().getEntityField(
            Context.getCurrent().getPortal(),
            "SettlementStatusFukarekComboBox",
            "MANUAL", getLocale()));
      settlementStatusField.setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "SETTLEMENT_STATUS", getLocale()));


      remarkTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.remark", locale));
      quantiyTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.quantity", locale));
      statusTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.status", locale));
      habitatTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.habitat", locale));
      herbaryTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.herbary", locale));
      commentTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.comment", locale));
      mandatoryHeader.setValue(getMessages().getMessage(portal,
            "group.mandantory.header", locale));
      optionalHeader.setValue(
            getMessages().getMessage(portal, "group.optional.header", locale));
      determinationCommentField.setCaption(getMessages().getEntityField(portal,
            "Occurrence", "DETERMINATION_COMMENT", locale));
      sexField.setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "SEX", getLocale()));
      lifeStageField.setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "LIFE_STAGE", getLocale()));
      makropterField.setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "Occurrence", "MAKROPTER", getLocale()));
      reproductionField.setCaption(getMessages().getEntityField(portal,
            "Occurrence", "REPRODUCTION", locale));
      sampleMethodField.setCaption(getMessages().getEntityField(portal,
            "Sample", "SAMPLE_METHOD", locale));
      numericAmmountField
            .setCaption(getEntityField(OccurrenceProperties.AMOUNT));
      numericAmountWithAccuracyField
            .setCaption(getEntityField(OccurrenceProperties.AMOUNT));
      amountField.setCaption(getEntityField(OccurrenceProperties.AMOUNT));
      amountField.setAdviceHtmlText(getMessages().getEntityField(
            Context.getCurrent().getPortal(),
            "AmountComboBox",
            "MANUAL", getLocale()));
      dateCheckbox.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.RECORDING_DATE", locale));
      locationCheckbox.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.LOCATION_POSITION", locale));
      locationTextCheckbox.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.LOCATION_TEXT", locale));
      preselectButton.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.PRESELECT_BUTTON", locale));
      saveWithoutPreselectionButton.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.SAVE_WITHOUT_PRESELECT_BUTTON", locale));
      preselectionText.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.PRESELECT_WINDOW_TEXT", locale));
      habitatCheckbox.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.HABITAT", locale));
      observersCheckbox.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.OBSERVERS", locale));
      amountCheckbox.setCaption(getMessages().getEntity(portal,
            "OccurrenceInputViewImpl.AMOUNT", locale));
//      preselectionWindow.setCaption(getMessages().getEntity(portal,
//            "OccurrenceInputViewImpl.PRESELECT_WINDOW_HEADER", locale));
      remark.setCaption(getMessages().getEntityField(portal,
            "Occurrence", "REMARK", locale));
   }

   @Override
   public void showProgress()
   {
      UI ui = this.getUI();

      progressIndicator = new ProgressIndicator(new MessagesResourceMock());
      progressIndicator
            .setSteps(1 + occurrenceDocumentUploadField.getValue().size());
      progressWindow = new Window("Fortschritt", progressIndicator);
      progressWindow.setWidth("30%");
      progressWindow.center();
      progressWindow.setModal(true);
      progressWindow.setClosable(false);
      ui.addWindow(progressWindow);
   }

   @Override
   public void success(String message)
   {
      progressIndicator.success(message);
      NotificationUtils.showSaveSuccess("",
            "Datensatz erfolgreich gespeichert");
   }

   @Override
   public void failure(String message)
   {
      progressIndicator.failure(message);
      NotificationUtils.showError("",
            "Datensatz konnte nicht gespeichert werden");
   }

   @Override
   public void removeProgress()
   {
      if (progressIndicator.hasFailure())
      {
         progressWindow.setClosable(true);
      } else
      {
         getUI().removeWindow(progressWindow);
      }
   }

   @Override
   public Collection<DocumentUpload> getDocumentsToUpload()
   {
      return occurrenceDocumentUploadField.getValue();
   }

   @Override
   public Collection<Document> getDocumentsToDelete()
   {
      return occurrenceDocumentsField.getDocumentsToDelete();
   }

   protected AreaComboBox getAreaField()
   {
      return areaField;
   }

   @Override
   public void positionTypeChanged(PositionType positionType)
   {
      setupBlurFieldByPositionType(positionType);
      blurField.setValue("");
   }

   private void setupBlurFieldByPositionType(PositionType positionType)
   {
      if(PositionType.POINT.equals(positionType))
      {
         blurField.setReadOnly(false);
      }
      else
      {
         blurField.setReadOnly(true);
      }
   }

   @Override
   public Double getLastMapPositionLat()
   {
      return mapField.getPositionLat();
   }

   @Override
   public Double getLastMapPositionLon()
   {
      return mapField.getPositionLon();
   }

   @Override
   public Double getLastMapZoom()
   {
      return mapField.getZoom();
   }

   @Override
   public void setupPositionAndZoom(double lat, double lon, double zoom)
   {
      mapField.setupPositionAndZoom(lat, lon, zoom);
   }
   
   @Override
   public void setComments(OccurrenceCommentsSliceResponse occurreceComments,
         SampleCommentsSliceResponse sampleComents)
   {
      commentsField.setValue(new CommentedSample(sample,
            occurreceComments.getContent(), sampleComents.getContent()));
      if(!commentsField.getValue().hasComments())
      {
         tabSheet.removeTab(commentTab);
      }
   }

}