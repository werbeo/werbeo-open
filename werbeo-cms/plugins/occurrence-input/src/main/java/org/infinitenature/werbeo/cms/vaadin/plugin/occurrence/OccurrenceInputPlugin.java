package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Base64;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.service.v1.types.AbstractComment;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SampleBase;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.service.v1.types.support.MediaUpload;
import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.navigation.MyOccurrences;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.OccurrenceEdit;
import org.infinitenature.werbeo.cms.navigation.OccurrenceNew;
import org.infinitenature.werbeo.cms.navigation.Occurrences;
import org.infinitenature.werbeo.cms.navigation.SampleEdit;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Preselection;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OccurrenceInputPlugin implements VCMSComponent, OccurrenceInputView.Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceInputPlugin.class);

   private Werbeo werbeo;
   @Autowired
   private HerbariorumClient client;

   private OccurrenceInputView view;

   @Autowired
   private NavigationResolver navigationResolver;
   @Autowired
   private InstanceConfig instanceConfig;

   private UUID occurrenceUUID;

   private MessagesResource messages;

   public OccurrenceInputPlugin(
         @Autowired
               MessagesResource messages,
         @Autowired
               Werbeo werbeo)
   {
      int portalId = Context.getCurrent().getPortal().getPortalId();
      Sample sample = new Sample();
      view = new OccurrenceInputViewImpl(sample , this, messages,
            werbeo.samples().getFieldConfig(portalId),
            werbeo.occurrences().getFieldConfig(portalId).getFieldConfigs(),
            werbeo.portals().getPortalConfiguration(
                  Context.getCurrent().getPortal().getPortalId())
                  .getPortalConfiguration());
      Context context = Context.getCurrent();
      if (context.getMapPositionConfig().getLastMapPositionLat() != null
            && context.getMapPositionConfig().getLastMapPositionLon() != null
            && context.getMapPositionConfig().getLastMapZoom() != null)
      {
         view.setupPositionAndZoom(
               context.getMapPositionConfig().getLastMapPositionLat(),
               context.getMapPositionConfig().getLastMapPositionLon(),
               context.getMapPositionConfig().getLastMapZoom());
      }
      
      // set preselected values from last occurrenceInput
      if(context.getPreselection() != null)
      {
         view.preselect(context.getPreselection());
      }
      
      this.messages = messages;
      this.werbeo = werbeo;
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      view.setWerbeo(werbeo, null, instanceConfig);
      view.setTopSurveyId(werbeo.portals().getPortalConfiguration(
            Context.getCurrent().getPortal().getPortalId())
            .getPortalConfiguration().getDefaultDataEntrySurveyId());
      view.setHerbariorumClient(client);
      view.setQuarasekClient(Quarasek.getInstance(instanceConfig.getQuarasekUrl()));
   }

   @Override
   public void enter(ViewChangeEvent event)
   {
      updateView();
      updateOccurrenceView(event);
   }

   private void updateView()
   {
      Person person = createIndiciaUser();
      view.setWerbeo(werbeo, person, instanceConfig);
      UI.getCurrent().getScrollTop();
   }

   /*
    * find logged in user in Incdicia, does not already exist in Indicia? create it!
    */
   private Person createIndiciaUser()
   {
      Authentication authentication = Context.getCurrent().getAuthentication();

      Person person = new Person();
      person.setEmail(authentication.getEmail().get());
      person.setFirstName(authentication.getGivenName().orElse(""));
      person.setLastName(authentication.getFamilyName().orElse(""));

      return person;
   }

   private void updateOccurrenceView(ViewChangeEvent event)
   {
      try
      {
         if (StringUtils.isNotBlank(event.getParameters()))
         {
            occurrenceUUID = UUID.fromString(event.getParameters());

            LOGGER.info("Going to load occurrence {}", occurrenceUUID);
            Occurrence occurrence = werbeo.occurrences()
                  .get(Context.getCurrent().getApp().getPortalId(),
                        occurrenceUUID).getOccurrence();

            UI.getCurrent().access(() -> {
               Sample sample = werbeo.samples()
            .get(Context.getCurrent().getPortal().getPortalId(),
                  occurrence.getSample().getEntityId()).getSample();
               if(sample.getOccurrences().size() > 1)
               {
                  // more than 1 occurrence, go to kartierliste!
                  navigationResolver
                  .navigateTo(new SampleEdit(sample.getEntityId()));
               }
               view.setSample(sample,
                     occurrenceUUID);
               view.setComments(werbeo.occurrenceComments()
                     .getComments(
                           Context.getCurrent().getPortal().getPortalId(),
                           occurrence.getEntityId(), 0, 1000,
                           CommentField.MOD_DATE, SortOrder.DESC),
                     werbeo.sampleComments().getComments(
                           Context.getCurrent().getPortal().getPortalId(),
                           occurrence.getSample().getEntityId(), 0,
                           1000,
                           CommentField.MOD_DATE, SortOrder.DESC));

            });
         }
         else
         {
            view.reset();
            occurrenceUUID = null;
         }
      } catch (Exception e)
      {
         LOGGER.error("Failure loading occurrence", e);
      }
   }

   @Override
   public void onReplyCommentClick(String reply, AbstractComment comment)
   {
      int portalId = Context.getCurrent().getPortal().getPortalId();
      if (comment instanceof OccurrenceComment)
      {
         werbeo.occurrenceComments().saveComment(portalId,
               ((OccurrenceComment) comment).getTargetId(),
               new OccurrenceComment(reply));
      }
      else if (comment instanceof SampleComment)
      {
         werbeo.sampleComments().saveComment(portalId,
               ((SampleComment) comment).getTargetId(),
               new SampleComment(reply));
      }
      else
      {
         LOGGER.error("unknown type of comment {}", comment);
      }
      navigationResolver.navigateTo(new OccurrenceEdit(occurrenceUUID));
   }

   @Override
   public void onNewCommentClick(String comment, Base<?> entity)
   {
      int portalId = Context.getCurrent().getPortal().getPortalId();
      if (entity instanceof SampleBase)
      {
         werbeo.sampleComments().saveComment(portalId,
               (UUID) entity.getEntityId(), new SampleComment(comment));
      }
      else if (entity instanceof Occurrence)
      {
         werbeo.occurrenceComments().saveComment(portalId,
               (UUID) entity.getEntityId(), new OccurrenceComment(comment));
      } else
      {
         LOGGER.error("unknown type of comment {}", comment);
      }
      navigationResolver.navigateTo(new OccurrenceEdit(occurrenceUUID));
   }

   @Override
   public void onSaveClick(Sample sample, UUID occurrenceId, Preselection preselection)
   {
      view.showProgress();
      Collection<DocumentUpload> documentsToUpload = view
            .getDocumentsToUpload();
      Collection<Document> documentsToDelete = view.getDocumentsToDelete();
      try
      {
         int portalId = Context.getCurrent().getPortal().getPortalId();
         werbeo.samples().save(portalId,
               sample);

         view.success(messages.getMessage(Context.getCurrent().getPortal(),
               "sample.saved", view.getLocale()));

         for (Document documentToDelete : documentsToDelete)
         {
            werbeo.occurrenceMedia().deleteMedia(portalId, occurrenceId,
                  documentToDelete.getLink().getHref());
         }
         for (DocumentUpload documentUpolad : documentsToUpload)
         {
            try (InputStream picture = new FileInputStream(
                  documentUpolad.getFile());)
            {
               byte[] data = IOUtils.toByteArray(picture);
               String base64Encoded = Base64.getEncoder().encodeToString(data);
               MediaUpload mediaUpload = new MediaUpload(
                     documentUpolad.getFile().getName(),
                     documentUpolad.getCaption(), base64Encoded);
               werbeo.occurrenceMedia().attachMedia(portalId, occurrenceId,
                     mediaUpload);
               view.success(messages.getMessage(
                     Context.getCurrent().getPortal(), "media.upload.success",
                     view.getLocale(), documentUpolad.getFile().getName()));

            } catch (Exception e)
            {
               view.failure(messages.getMessage(
                     Context.getCurrent().getPortal(), "media.upload.failure",
                     view.getLocale(), documentUpolad.getFile().getName()));
               LOGGER.error("Failure uploading {}", documentUpolad, e);
            }

         }
      } catch (Exception e)
      {
         view.failure(messages.getMessage(Context.getCurrent().getPortal(),
               "data.save.failure", view.getLocale()));
         LOGGER.error("Failure saveing", e);
         view.failure("Fehler beim Speichern");
      } finally
      {
         view.removeProgress();
         Context.getCurrent().getMapPositionConfig()
               .setLastMapPositionLat(view.getLastMapPositionLat());
         Context.getCurrent().getMapPositionConfig()
               .setLastMapPositionLon(view.getLastMapPositionLon());
         Context.getCurrent().getMapPositionConfig()
               .setLastMapZoom(view.getLastMapZoom());
         Context.getCurrent().setPreselection(preselection);
         view.reset();
         navigationResolver.navigateTo(new OccurrenceNew());
      }

   }

   @Override
   public void onDeleteClick(Sample sample)
   {
      werbeo.samples().delete(Context.getCurrent().getPortal().getPortalId(),
            sample.getEntityId());
      view.reset();
      navigationResolver.navigateTo(new MyOccurrences());
   }

}
