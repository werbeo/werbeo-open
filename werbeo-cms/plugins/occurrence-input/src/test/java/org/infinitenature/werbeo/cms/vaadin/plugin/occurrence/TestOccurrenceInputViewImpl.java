package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.service.v1.resources.mock.MockData;
import org.infinitenature.service.v1.types.AbstractComment;
import org.infinitenature.service.v1.types.Occurrence.Area;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.response.SampleFieldConfigResponse;
import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.Preselection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

class TestOccurrenceInputViewImpl
{
   private OccurrenceInputViewImpl view;
   private MockData mockData = new MockData();
   private Sample sampleToSave;
   private Sample sampleToDelete;

   @BeforeEach
   void setUp() throws Exception
   {
      Context context = new Context();
      context.setApp(new Portal("test", 2, 2));
      VaadinSession session = mock(VaadinSession.class);
      when(session.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(context);
      VaadinSession.setCurrent(session);

      UI ui = mock(UI.class);
      when(ui.getLocale()).thenReturn(Locale.GERMANY);
      UI.setCurrent(ui);
      OccurrenceInputView.Observer observer = new OccurrenceInputView.Observer()
      {
         @Override
         public void onDeleteClick(Sample sample)
         {
            sampleToDelete = sample;
         }

         @Override
         public void onReplyCommentClick(String reply, AbstractComment comment)
         {
            // NOOP
         }

         @Override
         public void onNewCommentClick(String comment, Base<?> entity)
         {
            // NOOP
         }

         @Override
         public void onSaveClick(Sample sample, UUID occurrenceId,
               Preselection preselection)
         {
            sampleToSave = sample;
         }
      };
      Set<OccurrenceFieldConfig> occurrenceFieldConfig = new HashSet<>();

      Set<SampleFieldConfig> sampleFieldConfig = new HashSet<>();
      PortalConfiguration config = new PortalConfiguration();
      config.setMapInitialLatitude(12d);
      config.setMapInitialLongitude(13d);
      config.setMapInitialZoom(12d);
      view = new OccurrenceInputViewImpl(new Sample(), observer,
            new MessagesResourceMock(),
            new SampleFieldConfigResponse(sampleFieldConfig),
            occurrenceFieldConfig, config);
   }

   @Test
   @Disabled("strange problems")
   void test()
   {
      Sample sample = mockData.getSamples().stream()
            .filter(s -> s.getOccurrences().size() == 2).findFirst().get();
      sample.getOccurrences()
            .forEach(o -> o.setCoveredArea(Area.MORE_THAN_HUNDRED));
      UUID occurrenceToIgnore = sample.getOccurrences().get(0).getEntityId();
      UUID occurrenceToSave = sample.getOccurrences().get(1).getEntityId();
      view.setSample(sample, occurrenceToSave);
      view.getAreaField().setValue(null);

      view.tryToSave();

      assertThat(sampleToSave.getOccurrences().get(0).getEntityId(),
            is((occurrenceToIgnore)));
      assertThat(sampleToSave.getOccurrences().get(0).getCoveredArea(),
            is(Area.MORE_THAN_HUNDRED));

      assertThat(sampleToSave.getOccurrences().get(1).getEntityId(),
            is(occurrenceToSave));
      assertThat(sampleToSave.getOccurrences().get(1).getCoveredArea(),
            is(nullValue()));
   }

}
