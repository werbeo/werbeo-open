package org.infinitenature.werbeo.cms.vaadin.plugin.tc;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.HtmlPlugin;
import org.infinitenature.werbeo.cms.vaadin.plugin.tc.TCView.Observer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class AcceptancePlugin implements VCMSComponent, Observer
{

   private final HtmlPlugin htmlPlugin;
   private final MessagesResource messageResource;
   private TCView tcView;
   private final Werbeo werbeo;

   public AcceptancePlugin(@Autowired HtmlPlugin htmlPlugin,
         @Autowired Werbeo werbeo, @Autowired MessagesResource messageResource)
   {
      super();
      this.htmlPlugin = htmlPlugin;
      this.werbeo = werbeo;
      this.messageResource = messageResource;
   }

   @Override
   public Component getVaadinComponent()
   {
      return tcView.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      Map<String, String> params = new HashMap<>(parameter);
      params.put(HtmlPlugin.PARAMETER_CONTENT,
            werbeo.termsAndConditions()
                  .getConditions(
                        Context.getCurrent().getPortal().getPortalId())
                  .getText());
      htmlPlugin.init(params);

      tcView = new TCViewImpl(messageResource, htmlPlugin.getVaadinComponent(),
            this);

      tcView.setEnabled(Context.getCurrent().isAcceptanceNecessary());
   }
   
   @Override
   public void enter(ViewChangeEvent event)
   {
      if(StringUtils.isNotBlank(event.getParameters()))
      {
         if("datenschutzbedingungen".equals(event.getParameters()))
         {
            tcView.showMarkdownText(werbeo.termsAndConditions()
                  .getPrivacyDeclaration(
                        Context.getCurrent().getPortal().getPortalId())
                  .getText());
         }
         else if("nutzungsbedingungen".equals(event.getParameters()))
         {
            tcView.showMarkdownText(werbeo.termsAndConditions()
                  .getToc(
                        Context.getCurrent().getPortal().getPortalId())
                  .getText());
         }
      }
   }

   @Override
   public void onAccept()
   {
      werbeo.termsAndConditions()
            .accept(Context.getCurrent().getPortal().getPortalId());
      UI.getCurrent().getSession().close();
      VaadinService.getCurrentRequest().getWrappedSession().invalidate();
      Page.getCurrent().setLocation("/sso/login");
   }

   @Override
   public void onDecline()
   {
      Page.getCurrent().setLocation("/sso/logout");
   }

}
