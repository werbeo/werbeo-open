package org.infinitenature.werbeo.cms.vaadin.plugin.tc;

import org.infinitenature.werbeo.cms.vaadin.plugin.html.HTMLPluginView;

public interface TCView extends HTMLPluginView
{
   public static interface Observer
   {
      public void onAccept();

      public void onDecline();

   }
   
   public void showMarkdownText(String string);
}
