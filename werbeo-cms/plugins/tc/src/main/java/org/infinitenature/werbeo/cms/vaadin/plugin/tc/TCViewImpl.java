package org.infinitenature.werbeo.cms.vaadin.plugin.tc;

import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.button.*;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.HTMLPluginView;
import org.vaadin.viritin.label.RichText;
import org.vaadin.viritin.layouts.*;

import com.vaadin.ui.*;

public class TCViewImpl extends I18NComposite implements TCView
{
   private final AcceptButton acceptButton;
   private final DismissButton dismissButton;
   private final HTMLPluginView htmlView;
   private RichText richText = new RichText();
   Window textWindow = new Window();

   public TCViewImpl(MessagesResource messages, HTMLPluginView htmlView,
         Observer observer)
   {
      super(messages);
      this.htmlView = htmlView;
      
      acceptButton = new AcceptButton(messages, event -> observer.onAccept());
      dismissButton = new DismissButton(messages,
            event -> observer.onDecline());

      
      Button closeButton = new Button("Schließen");
      closeButton.addClickListener(click -> UI.getCurrent().removeWindow(textWindow));
      VerticalLayout windowLayout = new VerticalLayout(richText, closeButton);
      textWindow.setContent(windowLayout);
      textWindow.setWidth("90%");
      textWindow.center();
      setCompositionRoot(new MVerticalLayout(htmlView,
            new MHorizontalLayout(acceptButton, dismissButton)));
   }

   @Override
   public void applyLocalizations()
   {
      acceptButton.applyLocalizations();
      dismissButton.applyLocalizations();
   }

   @Override
   public void setContent(String content)
   {
      htmlView.setContent(content);
   }

   @Override
   public void setEnabled(boolean enabled)
   {
      super.setEnabled(enabled);
      acceptButton.setVisible(enabled);
      dismissButton.setVisible(enabled);
   }
   
   @Override
   public void showMarkdownText(String string)
   {
      richText.withMarkDown(string);
      getUI().getCurrent().addWindow(textWindow);
   }
}
