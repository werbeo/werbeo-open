package org.infinitenature.werbeo.cms.vaadin.plugin.tc;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.validation.constraints.Min;

import org.infinitenature.service.v1.resources.TermsAndConditionsResource;
import org.infinitenature.service.v1.types.response.TextResponse;
import org.infinitenature.service.v1.types.response.TermsAndConditonsAcceptResponse;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.plugin.html.HtmlPlugin;
import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

public class TCDemoAccepted extends AbstractTest
{
   public TCDemoAccepted()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));
      Authentication springAuth = mock(Authentication.class);

      Collection list = new ArrayList();
      list.add(new GrantedAuthority()
      {

         @Override
         public String getAuthority()
         {
            return RoleNameFactory.getKeycloakRoleName(1, Roles.ACCEPTED);
         }
      });
      when(springAuth.getAuthorities()).thenReturn(list);
      context.setSpringAuthentication(springAuth);
      org.infinitenature.vct.Authentication auth = new org.infinitenature.vct.Authentication()
      {
         @Override
         public String getPrincipal()
         {
            return "A user";
         }

         @Override
         public boolean isAnonymous()
         {
            return false;
         }
      };
      context.setAuthentication(auth);
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
   }

   @Override
   public Component getTestComponent()
   {
      Werbeo werbeoMock = mock(Werbeo.class);

      TermsAndConditionsResource resource = new TermsAndConditionsResource()
      {
         @Override
         public TextResponse getConditions(
               @Min(1) int portalId)
         {
            return new TextResponse("# Nutzerbedingungen für "
                  + portalId + "\nDas ist der Text...");
         }

         @Override
         public TermsAndConditonsAcceptResponse accept(@Min(1) int portalId)
         {
            return new TermsAndConditonsAcceptResponse(true);
         }

         @Override
         public TextResponse getPrivacyDeclaration(@Min(1) int portalId)
         {
            // TODO Auto-generated method stub
            return null;
         }

         @Override
         public TextResponse getToc(@Min(1) int portalId)
         {
            // TODO Auto-generated method stub
            return null;
         }
      };
      when(werbeoMock.termsAndConditions()).thenReturn(resource);
      AcceptancePlugin pluginUT = new AcceptancePlugin(
            new HtmlPlugin(), werbeoMock, new MessagesResourceMock());

      pluginUT.init(Collections.emptyMap());
      return pluginUT.getVaadinComponent();
   }

}
