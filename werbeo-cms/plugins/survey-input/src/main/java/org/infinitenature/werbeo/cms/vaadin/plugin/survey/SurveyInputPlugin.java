package org.infinitenature.werbeo.cms.vaadin.plugin.survey;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.ObfuscationPolicy;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.enums.Permission;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.Surveys;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.plugin.survey.SurveyInputView.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SurveyInputPlugin implements VCMSComponent, Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyInputPlugin.class);
   private SurveyInputView view;
   private final Werbeo werbeo;
   private final MessagesResource messages;
   private final NavigationResolver navigationResolver;

   public SurveyInputPlugin(@Autowired MessagesResource messagesResource,
         @Autowired Werbeo werbeo,
         @Autowired NavigationResolver navigationResolver)
   {
      this.messages = messagesResource;
      this.werbeo = werbeo;
      this.navigationResolver = navigationResolver;
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      this.view = new SurveyInputExtendedViewImpl(messages,
            this);
   }

   @Override
   public void enter(ViewChangeEvent event)
   {
      Integer surveyId;
      if (StringUtils.isNotBlank(event.getParameters()))
      {
         surveyId = Integer.parseInt(event.getParameters());
         SurveyBase survey = werbeo.surveys()
               .getSurvey(Context.getCurrent().getPortal().getPortalId(),
                     surveyId)
               .getSurvey();
         view.setSurvey(survey);
      } else
      {
         view.reset();
      }
   }

   @Override
   public void onSave(SurveyBase survey)
   {
      try
      {
         survey.setObfuscationPolicies(generateObfuscationPolicies(view.getObfuscations()));
         survey.setWerbeoOriginal(view.isWerbeoOrginal());
         survey.setAllowDataEntry(view.isAllowDataEntry());
         werbeo.surveys().save(Context.getCurrent().getPortal().getPortalId(),
               survey);
         view.success(survey);
      } catch (Exception e)
      {
         view.failure(survey);
      } finally
      {
         navigationResolver.navigateTo(new Surveys());
      }
   }

   private List<ObfuscationPolicy> generateObfuscationPolicies(
         List<Obfuscation> obfuscations)
   {
      List<ObfuscationPolicy> policies = new ArrayList<>();
      for (Obfuscation obfuscation : obfuscations)
      {
         if (obfuscation.getLocation() != null)
         {
            policies.add(
                  new ObfuscationPolicy(obfuscation.getRole().getRoleName(),
                        obfuscation.getLocation().getPermissionName()));
         }
         if (obfuscation.isPersons())
         {
            policies.add(
                  new ObfuscationPolicy(obfuscation.getRole().getRoleName(),
                        Permission.PERSONS.getPermissionName()));
         }
      }
      return policies;
   }

   @Override
   public void onDelete(SurveyBase entity)
   {
      LOGGER.error("Not yet implementet");
   }
}
