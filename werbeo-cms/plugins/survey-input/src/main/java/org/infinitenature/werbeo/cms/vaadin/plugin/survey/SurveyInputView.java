package org.infinitenature.werbeo.cms.vaadin.plugin.survey;

import java.util.List;

import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface SurveyInputView extends CMSVaadinView
{
   public interface Observer
   {
      void onSave(SurveyBase entity);

      void onDelete(SurveyBase entity);
   }

   public void setSurvey(SurveyBase surveyBase);

   public void reset();

   public void success(SurveyBase survey);

   public void failure(SurveyBase survey);

   public List<Obfuscation> getObfuscations();

   public boolean isWerbeoOrginal();

   public boolean isAllowDataEntry();
}
