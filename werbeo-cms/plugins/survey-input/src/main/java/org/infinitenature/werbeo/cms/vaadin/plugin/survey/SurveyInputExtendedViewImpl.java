package org.infinitenature.werbeo.cms.vaadin.plugin.survey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.service.v1.types.ObfuscationPolicy;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.enums.Permission;
import org.infinitenature.service.v1.types.enums.Role;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.Heading;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.vaadin.viritin.button.MButton;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SurveyInputExtendedViewImpl extends I18NComposite implements SurveyInputView
{
   private Button saveButtonBottom ;
   private Button deleteButtonBottom;

   private final TextField nameField = new TextField();
   private final TextArea descriptionField = new TextArea();
   private final CheckBox allowAnonymousAccessField = new CheckBox();
   private final CheckBox werbeoOriginalField = new CheckBox();
   private final CheckBox allowDataEntryField = new CheckBox();

   private Column roleColumn;
   private Column locationColumn;
   private Column personsColumn;

   private Heading heading;
   private Heading obfuscationHeading;

   private Observer observer;

   private SurveyBase survey;
   
   private Window saveAdviceWindow;
   private Label saveAdviceLabel;

   private List<Obfuscation> obfuscations = new ArrayList<>();

   Grid<Obfuscation> obfuscationGrid;

   public SurveyInputExtendedViewImpl(MessagesResource messages, Observer observer)
   {
      super(messages);
      prepareObfuscations();
      saveButtonBottom = new MButton("Speichern und Abschließen", e ->
      {
         observer.onSave(survey);
         UI.getCurrent().addWindow(saveAdviceWindow);
      });
      deleteButtonBottom = new MButton(VaadinIcons.TRASH, "Löschen",e -> observer.onDelete(survey)).withVisible(false);
      this.observer = observer;


      nameField.setWidth(350, Unit.PIXELS);
      descriptionField.setWidth(350, Unit.PIXELS);

      // Change Listeners
      nameField.addValueChangeListener(
            event -> survey.setName(nameField.getValue()));
      descriptionField.addValueChangeListener(
            event -> survey.setDescription(descriptionField.getValue()));
      allowAnonymousAccessField
      .addValueChangeListener(event -> survey.setAvailability(
            allowAnonymousAccessField.getValue() ? Availability.FREE
                  : Availability.RESTRICTED));



      VerticalLayout mainLayout = new VerticalLayout();
      heading = new Heading(messages.getEntity(null, "SurveyInput.SETTINGS", getLocale()));
      mainLayout.addComponent(heading);

      // survey basedata
      mainLayout.addComponent(nameField);
      mainLayout.addComponent(descriptionField);
      mainLayout.addComponent(allowAnonymousAccessField);
      mainLayout.addComponent(werbeoOriginalField);
      mainLayout.addComponent(allowDataEntryField);

      // obfuscation matrix

      obfuscationHeading = new Heading(messages.getEntity(null, "SurveyInput.OBFUSCATION", getLocale()));
      mainLayout.addComponent(obfuscationHeading);
      obfuscationGrid = new Grid<Obfuscation>();
      obfuscationGrid.setWidth(50, Unit.PERCENTAGE);
      obfuscationGrid.setHeightByRows(4);
      obfuscationGrid.setSizeFull();
      obfuscationGrid.addStyleName("occurrence-table");
      roleColumn = obfuscationGrid.addComponentColumn(obfuscation ->
      {
         return new Label(messages.getEntity(null,
               "Role." + obfuscation.getRole().getRoleName(), getLocale())
               + " (" + obfuscation.getRole().getRoleName() + ")");
      }).setCaption(getMessages().getEntity(null, "SurveyInput.ROLE", getLocale())).setWidth(280);

      locationColumn = obfuscationGrid.addComponentColumn(obfuscation -> {
         ComboBox<Permission> locationCombobox = new ComboBox<Permission>(
               null,
               Arrays.asList(new Permission[] { Permission.LOCATION_MTBQ,
                     Permission.LOCATION_MTBQQ, Permission.LOCATION_MTBQQQ,
                     Permission.LOCATION_POINT }));
         locationCombobox.setValue(obfuscation.getLocation());
         locationCombobox.addValueChangeListener(
               event -> obfuscation.setLocation(locationCombobox.getValue()));
         locationCombobox.setItemCaptionGenerator(new ItemCaptionGenerator<Permission>()
         {
            @Override
            public String apply(Permission item)
            {
               return messages.getEntity(null,
                     "Permission." + item.getPermissionName(), getLocale());
            }
         });
         return locationCombobox;

      }).setCaption(
            getMessages().getEntity(null, "SurveyInput.LOCATION", getLocale())).setWidth(370);

      personsColumn = obfuscationGrid.addComponentColumn(obfuscation -> {
         CheckBox personsCheckbox = new CheckBox();
         personsCheckbox.setValue(obfuscation.isPersons());
         personsCheckbox.addValueChangeListener(
               event -> obfuscation.setPersons(personsCheckbox.getValue()));
         return personsCheckbox;
      }).setCaption(
            getMessages().getEntity(null, "SurveyInput.PERSONS", getLocale())).setWidth(290);


      mainLayout.addComponent(obfuscationGrid);


      HorizontalLayout bottomButtonLayout = new HorizontalLayout();
      bottomButtonLayout.addComponents(deleteButtonBottom,saveButtonBottom);
      mainLayout.addComponent(bottomButtonLayout);
      obfuscationGrid.setItems(obfuscations);
      setCompositionRoot(mainLayout);
      
      saveAdviceLabel = new Label(getMessages().getEntity(null,
            "SurveyInput.SAVE_ADVICE_TEXT", getLocale()), ContentMode.HTML);
      saveAdviceWindow = new Window(getMessages().getEntity(null,
            "SurveyInput.SAVE_ADVICE_HEADER", getLocale()));
      saveAdviceWindow.setWidth("40%");
      saveAdviceWindow.setHeight("290px");
      saveAdviceWindow.center();
      Button saveAdviceButton = new Button("OK");
      saveAdviceButton.addClickListener(click -> saveAdviceWindow.close());
      VerticalLayout windowLayout = new VerticalLayout(saveAdviceLabel, saveAdviceButton);
      saveAdviceWindow.setContent(windowLayout);
   }

   /**
    *
    */
   private static final long serialVersionUID = 1L;

   @Override
   public void applyLocalizations()
   {
      nameField.setCaption(
            getMessages().getEntity(null, "SurveyInput.NAME", getLocale()));
      descriptionField.setCaption(getMessages().getEntity(null,
            "SurveyBase.DESCRIPTION", getLocale()));
      allowAnonymousAccessField.setCaption(getMessages().getEntity(null,
            "SurveyInput.ANONYMOUS", getLocale()));
      werbeoOriginalField.setCaption(
            getMessages().getEntityField(Context.getCurrent().getPortal(),
                  "SurveyBase", "WERBEO_ORIGINAL", getLocale()));
      allowDataEntryField.setCaption(
            getMessages().getEntityField(Context.getCurrent().getPortal(),
                  "SurveyBase", "ALLOW_DATA_ENTRY", getLocale()));
      heading.setValue(getMessages().getEntity(null, "SurveyInput.SETTINGS", getLocale()));
      obfuscationHeading.setValue(getMessages().getEntity(null,
            "SurveyInput.OBFUSCATION", getLocale()));
      roleColumn.setCaption(getMessages().getEntity(null, "SurveyInput.ROLE", getLocale()));
      locationColumn.setCaption(getMessages().getEntity(null, "SurveyInput.LOCATION", getLocale()));
      personsColumn.setCaption(getMessages().getEntity(null, "SurveyInput.PERSONS", getLocale()));
      
      saveAdviceLabel.setValue(getMessages().getEntity(null,
            "SurveyInput.SAVE_ADVICE_TEXT", getLocale()));
      saveAdviceWindow.setCaption(getMessages().getEntity(null,
            "SurveyInput.SAVE_ADVICE_HEADER", getLocale()));
   }


   @Override
   public void setSurvey(SurveyBase surveyBase)
   {
      this.survey = surveyBase;
      prepareObfuscations();
      obfuscationGrid.setItems(obfuscations);
      nameField.setValue(surveyBase.getName());
      descriptionField.setValue(surveyBase.getDescription() != null ? surveyBase.getDescription():"");
      allowAnonymousAccessField
            .setValue(surveyBase.getAvailability().equals(Availability.FREE));
      werbeoOriginalField.setValue(surveyBase.getWerbeoOriginal());
      allowDataEntryField.setValue(surveyBase.getAllowDataEntry());
   }

   private void prepareObfuscations()
   {
      obfuscations.clear();

      obfuscations.add(
            new Obfuscation(Role.ANONYMOUS, getLocation(survey, Role.ANONYMOUS),
                  getPersons(survey, Role.ANONYMOUS)));
      obfuscations.add(
            new Obfuscation(Role.ACCEPTED, getLocation(survey, Role.ACCEPTED),
                  getPersons(survey, Role.ACCEPTED)));
      obfuscations.add(
            new Obfuscation(Role.APPROVED, getLocation(survey, Role.APPROVED),
                  getPersons(survey, Role.APPROVED)));
      obfuscations.add(
            new Obfuscation(Role.VALIDATOR, getLocation(survey, Role.VALIDATOR),
                  getPersons(survey, Role.VALIDATOR)));

   }

   private Permission getLocation(SurveyBase survey2, Role role)
   {
      if(survey != null)
      {
         for(ObfuscationPolicy policy : survey.getObfuscationPolicies())
         {
            if(policy.getRole().equals(role) && policy.getPermission().toString().contains("LOCATION_"))
            {
               return policy.getPermission();
            }
         }
      }
      return null;
   }


   private boolean getPersons(SurveyBase survey2, Role role)
   {
      if(survey != null)
      {
         for(ObfuscationPolicy policy : survey.getObfuscationPolicies())
         {
            if (policy.getRole().equals(role)
                  && policy.getPermission().equals(Permission.PERSONS))
            {
               return true;
            }
         }
      }
      return false;
   }


   @Override
   public void reset()
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void success(SurveyBase survey)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void failure(SurveyBase survey)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public List<Obfuscation> getObfuscations()
   {
      return obfuscations;
   }

   @Override
   public boolean isWerbeoOrginal()
   {
      return werbeoOriginalField.getValue() != null
            ? werbeoOriginalField.getValue()
            : false;
   }

   @Override
   public boolean isAllowDataEntry()
   {
      return allowDataEntryField.getValue() != null
            ? allowDataEntryField.getValue()
            : false;
   }
}
