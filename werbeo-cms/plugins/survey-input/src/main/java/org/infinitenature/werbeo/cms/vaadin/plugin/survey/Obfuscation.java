package org.infinitenature.werbeo.cms.vaadin.plugin.survey;

import org.infinitenature.service.v1.types.enums.Permission;
import org.infinitenature.service.v1.types.enums.Role;

public class Obfuscation
{
   private Role role;
   private Permission location;
   private boolean persons;
   
   public Obfuscation(Role role, Permission location, boolean persons)
   {
      this.role = role;
      this.location = location;
      this.persons = persons;
   }
   
   public Obfuscation()
   {
      super();
   }
   
   public Role getRole()
   {
      return role;
   }
   public void setRole(Role role)
   {
      this.role = role;
   }
  
   public boolean isPersons()
   {
      return persons;
   }
   public void setPersons(boolean persons)
   {
      this.persons = persons;
   }
   public Permission getLocation()
   {
      return location;
   }
   public void setLocation(Permission location)
   {
      this.location = location;
   }
}
