package org.infinitenature.werbeo.cms.vaadin.plugin.survey;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.infinitenature.werbeo.cms.vaadin.Context;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;

public class SurveyInputDemo extends AbstractSurveyInputDemo
{
   @Override
   protected ViewChangeEvent getViewChangeEvent()
   {
      ViewChangeEvent event = mock(ViewChangeEvent.class);
      when(event.getParameters()).thenReturn("4");
      return event;
   }
   @Override
   protected void modifyContext(Context context)
   {
   }

}
