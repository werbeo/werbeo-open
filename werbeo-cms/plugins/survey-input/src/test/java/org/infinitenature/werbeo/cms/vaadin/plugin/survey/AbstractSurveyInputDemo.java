package org.infinitenature.werbeo.cms.vaadin.plugin.survey;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.apache.commons.lang3.time.StopWatch;
import org.infinitenature.service.v1.resources.mock.MockFactory;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.NavigationTarget;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

public abstract class AbstractSurveyInputDemo extends AbstractTest
{
   final static private Logger LOGGER = LoggerFactory
         .getLogger(AbstractSurveyInputDemo.class);
   private static MockFactory mockFactory = new MockFactory();

   private SurveyInputPlugin plugin;

   public AbstractSurveyInputDemo()
   {
      Werbeo werbeo = mock(Werbeo.class);
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));

      modifyContext(context);
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      StopWatch sw = StopWatch.createStarted();
      plugin = new SurveyInputPlugin(new MessagesResourceMock(), werbeo,
            new NavigationResolver()
            {
               @Override
               public void navigateTo(NavigationTarget target)
               {
                  LOGGER.info("NavigationTarget: {}", target);
               }
            });
      LOGGER.info("Creating SurveyTablePlugin took {}ms", sw.getTime());

      when(werbeo.surveys()).thenReturn(mockFactory.getSurveysResource());
   }

   protected abstract void modifyContext(Context context);

   @Override
   public Component getTestComponent()
   {
      plugin.nonUIinit(new HashMap<>());
      plugin.init(new HashMap<>());
      plugin.enter(getViewChangeEvent());
      return plugin.getVaadinComponent();
   }

   protected abstract ViewChangeEvent getViewChangeEvent();

}
