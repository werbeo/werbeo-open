//package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetableinter;
//
//import static org.hamcrest.Matchers.is;
//import static org.hamcrest.Matchers.notNullValue;
//import static org.junit.Assert.assertThat;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//import java.util.Collections;
//import java.util.Locale;
//
//import org.infinitenature.service.v1.resources.PortalResource;
//import org.infinitenature.service.v1.types.PortalConfiguration;
//import org.infinitenature.service.v1.types.response.PortalConfigResponse;
//import org.infinitenature.vct.VCMSContext;
//import org.infinitenature.werbeo.client.Werbeo;
//import org.infinitenature.werbeo.cms.vaadin.Context;
//import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
//import org.infinitenature.werbeo.cms.vaadin.Portal;
//import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetableinter.OccurrenceTableInteractivePlugin;
//import org.junit.Before;
//import org.junit.Test;
//
//import com.vaadin.server.VaadinSession;
//import com.vaadin.ui.UI;
//
//public class TestOccurrenceTablePlugin
//{
//   private OccurrenceTableInteractivePlugin occurrenceTablePlugin;
//   private Werbeo werbeoMock;
//
//   @Before
//   public void setUp()
//   {
//      Context context = new Context();
//      context.setApp(new Portal("test", 2, 2));
//      VaadinSession session = mock(VaadinSession.class);
//      when(session.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(context);
//      VaadinSession.setCurrent(session);
//
//      UI ui = mock(UI.class);
//      when(ui.getLocale()).thenReturn(Locale.GERMANY);
//      UI.setCurrent(ui);
//
//      werbeoMock = mock(Werbeo.class);
//      PortalResource portalResource = mock(PortalResource.class);
//      when(werbeoMock.portals()).thenReturn(portalResource);
//      PortalConfiguration portalConfiguration = new PortalConfiguration();
//      portalConfiguration.setMapInitialZoom(8.0);
//      portalConfiguration.setMapInitialLatitude(53.9);
//      portalConfiguration.setMapInitialLongitude(12.25);
//      PortalConfigResponse portalConfigResponse = new PortalConfigResponse(portalConfiguration);
//      when(portalResource.getPortalConfiguration(2)).thenReturn(portalConfigResponse);
//
//      occurrenceTablePlugin = new OccurrenceTableInteractivePlugin(
//            new MessagesResourceMock(), werbeoMock);
//
//
//   }
//
//   @Test
//   public void testInit()
//   {
//      occurrenceTablePlugin.init(Collections.emptyMap());
//      assertThat(occurrenceTablePlugin.getVaadinComponent(),
//            is(notNullValue()));
//   }
//
//}
