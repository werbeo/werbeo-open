//package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetableinter;
//
//import com.vaadin.ui.Button;
//import com.vaadin.ui.ComboBox;
//import com.vaadin.ui.Composite;
//import com.vaadin.ui.Grid;
//import com.vaadin.ui.HorizontalLayout;
//import com.vaadin.ui.Label;
//import com.vaadin.ui.VerticalLayout;
//import com.vaadin.ui.themes.ValoTheme;
//import org.locationtech.jts.io.WKTReader;
//import org.infinitenature.service.v1.types.*;
//import org.infinitenature.werbeo.client.Werbeo;
//import org.infinitenature.werbeo.cms.formatter.PersonFormatter;
//import org.infinitenature.werbeo.cms.vaadin.Context;
//import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
//import org.infinitenature.werbeo.cms.vaadin.components.GeoButton;
//import org.infinitenature.werbeo.cms.vaadin.components.LinkButton;
//import org.infinitenature.werbeo.cms.vaadin.components.NewOccurrenceLinkButton;
//import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
//import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBox;
//import org.infinitenature.werbeo.cms.vaadin.components.VagueDateField;
//
//public class OccurrenceEditTableViewImpl extends Composite
//      implements OccurrenceEditTableView
//{
//   private Grid<Occurrence> grid = new Grid<>();
//   private VerticalLayout layout = new VerticalLayout();
//   private Button saveButton = new Button("Änderungen speichern");
//
//   private Sample sample;
//   private Werbeo werbeo;
//
//   private NewOccurrenceLinkButton newOccButton;
//
//   private MessagesResource messagesResource;
//
//   public OccurrenceEditTableViewImpl(MessagesResource messagesResource,
//         Werbeo werbeo)
//   {
//      this.werbeo = werbeo;
//      this.messagesResource = messagesResource;
//      addColumns(werbeo.portals().getPortalConfiguration(
//            Context.getCurrent().getPortal().getPortalId())
//            .getPortalConfiguration());
//
//      grid.setSelectionMode(Grid.SelectionMode.NONE);
//      grid.setSizeFull();
//
//      layout.addComponent(grid);
//      layout.setSizeFull();
//      layout.setMargin(false);
//
//      HorizontalLayout buttonLayout = new HorizontalLayout();
//      LinkButton newButton = new LinkButton("Neue Begehung", click -> {
//         Sample newSample = new Sample();
//         newSample.setSurvey(sample.getSurvey());
//         newSample.getOccurrences().add(new Occurrence());
//         this.sample = newSample;
//         grid.setItems(sample.getOccurrences());
//         grid.scrollTo(sample.getOccurrences().size() - 1);
//         saveButton.setVisible(true);
//      });
//
//      LinkButton copyButton = new LinkButton("Begehung kopieren", click -> {
//
//         Sample newSample = new Sample();
//         newSample.setLocality(sample.getLocality());
//         newSample.setDate(sample.getDate());
//         newSample.setSurvey(sample.getSurvey());
//         newSample.getOccurrences().add(new Occurrence());
//
//         this.sample = newSample;
//         grid.setItems(sample.getOccurrences());
//         grid.scrollTo(sample.getOccurrences().size() - 1);
//         saveButton.setVisible(true);
//      });
//
//      newOccButton = new NewOccurrenceLinkButton("Neue Beobachtung", werbeo,
//            messagesResource);
//      newOccButton.addSelectButtonListener(click -> {
//         Occurrence occurrence = new Occurrence();
//         sample.getOccurrences().add(occurrence);
//         occurrence.setRecordStatus(newOccButton.getRecordStatus());
//         occurrence.setDeterminer(newOccButton.getDeterminer());
//         occurrence.setSettlementStatus(newOccButton.getSettlementStatus());
//         occurrence.setAmount(newOccButton.getAmount());
//         grid.setItems(sample.getOccurrences());
//         grid.scrollTo(sample.getOccurrences().size() - 1);
//         saveButton.setVisible(true);
//      });
//
//      saveButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
//
//      saveButton.addClickListener(event -> {
//         werbeo.samples()
//               .save(Context.getCurrent().getPortal().getPortalId(), sample);
//         saveButton.setVisible(false);
//      });
//      saveButton.setVisible(false);
//
//      buttonLayout
//            .addComponents(newOccButton, copyButton, newButton, saveButton);
//      layout.addComponent(buttonLayout);
//      setCompositionRoot(layout);
//   }
//
//   @Override
//   public void setSample(Sample sample)
//   {
//      this.sample = sample;
//      grid.setItems(sample.getOccurrences());
//   }
//
//   private void addColumns(PortalConfiguration portalConfiguration)
//   {
//
//      grid.addComponentColumn(occurrence -> {
//
//         if (occurrence.equals(sample.getOccurrences().get(0)))
//         {
//            return new Label(sample.getSurvey().getName());
//         }
//         else
//         {
//            return new Label();
//         }
//
//      }).setCaption("Projekt");
//
//      grid.addComponentColumn(occurrence -> {
//
//         TaxonComboBox taxonComboBox = new TaxonComboBox(messagesResource,
//               werbeo);
//         taxonComboBox.setValue(occurrence.getTaxon());
//         taxonComboBox.addValueChangeListener(event -> {
//            occurrence.setTaxon(taxonComboBox.getValue());
//            saveButton.setVisible(true);
//         });
//         return taxonComboBox;
//      }).setCaption("Taxon");
//
//      grid.addComponentColumn(occurrence -> {
//
//         if (occurrence.equals(sample.getOccurrences().get(0)))
//         {
//            GeoButton geoButton = new GeoButton(
//                  sample.getLocality().getPosition() != null ?
//                        toShortGeoText(sample.getLocality().getPosition()) :
//                        "Auswählen", messagesResource, portalConfiguration);
//            geoButton.addValueChangeListener(event -> {
//               sample.getLocality().setPosition(geoButton.getValue());
//               saveButton.setVisible(true);
//               grid.setItems(sample.getOccurrences());
//               // TODO: let user input blur
//               sample.getLocality().setBlur(1);
//            });
//            geoButton.setStyleName("link");
//            return geoButton;
//         }
//         else
//         {
//            return new Label();
//         }
//      }).setCaption("Fundort").setWidth(250);
//
//      grid.addComponentColumn(occurrence -> {
//         if (occurrence.equals(sample.getOccurrences().get(0)))
//         {
//            VagueDateField dateButton = new VagueDateField(messagesResource,
//                  sample.getDate());
//            dateButton.addValueChangeListener(event -> {
//               sample.setDate(dateButton.getValue());
//               grid.setItems(sample.getOccurrences());
//               saveButton.setVisible(true);
//            });
//            dateButton.setStyleName("link");
//
//            return dateButton;
//         }
//         else
//         {
//            return new Label();
//         }
//      }).setCaption("Funddatum");
//
//      grid.addComponentColumn(occurrence -> {
//
//         ComboBox<Person> finderComboBox = new ComboBox<>();
//         finderComboBox.setDataProvider(new PersonComboBoxDataProvider(werbeo));
//         finderComboBox.setValue(sample.getRecorder());
//         finderComboBox.setItemCaptionGenerator(PersonFormatter::format);
//         finderComboBox.setPopupWidth("auto");
//         finderComboBox.addValueChangeListener(event -> {
//            sample.setRecorder(finderComboBox.getValue());
//            saveButton.setVisible(true);
//         });
//         return finderComboBox;
//
//      }).setCaption("Finder");
//
//      grid.addComponentColumn(occurrence -> {
//         NewOccurrenceLinkButton detailsButton = new NewOccurrenceLinkButton(
//               "Details", werbeo, messagesResource);
//         detailsButton.addSelectButtonListener(click -> {
//            occurrence.setRecordStatus(detailsButton.getRecordStatus());
//            occurrence.setDeterminer(detailsButton.getDeterminer());
//            occurrence.setSettlementStatus(detailsButton.getSettlementStatus());
//            occurrence.setAmount(detailsButton.getAmount());
//            grid.setItems(sample.getOccurrences());
//            grid.scrollTo(sample.getOccurrences().size() - 1);
//            saveButton.setVisible(true);
//         });
//         return detailsButton;
//      }).setCaption("Details");
//
//      grid.addComponentColumn(occurrence -> {
//
//         Button deleteButton = new Button("Löschen");
//         deleteButton.addClickListener(event -> {
//            sample.getOccurrences().remove(occurrence);
//            grid.setItems(sample.getOccurrences());
//            saveButton.setVisible(true);
//         });
//         deleteButton.addStyleName(ValoTheme.BUTTON_PRIMARY);
//         return deleteButton;
//      });
//   }
//
//   private String toShortGeoText(Position position)
//   {
//      WKTReader wktReader = new WKTReader();
//      try
//      {
//         return round(wktReader.read(position.getWkt()).getCentroid().getX())
//               + ", " + round(
//               wktReader.read(position.getWkt()).getCentroid().getY());
//      } catch (Exception e)
//      {
//         return "";
//      }
//   }
//
//   private String round(double number)
//   {
//      return String.valueOf(number).substring(0, 5);
//   }
//}
