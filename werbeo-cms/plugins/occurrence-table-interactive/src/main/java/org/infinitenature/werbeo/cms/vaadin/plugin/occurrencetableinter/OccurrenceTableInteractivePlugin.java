//package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetableinter;
//
//import java.util.Map;
//import java.util.UUID;
//
//import org.apache.commons.lang3.StringUtils;
//import org.infinitenature.commons.pagination.SortOrder;
//import org.infinitenature.service.v1.resources.SurveyFilter;
//import org.infinitenature.service.v1.types.Occurrence;
//import org.infinitenature.service.v1.types.Sample;
//import org.infinitenature.service.v1.types.SurveySortField;
//import org.infinitenature.vct.VCMSComponent;
//import org.infinitenature.werbeo.client.Werbeo;
//import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
//import org.infinitenature.werbeo.cms.vaadin.Context;
//import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
//import org.infinitenature.werbeo.cms.vaadin.VaadinAwareAsync;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.context.annotation.Scope;
//
//import com.vaadin.navigator.ViewChangeListener;
//import com.vaadin.spring.annotation.SpringComponent;
//import com.vaadin.ui.Component;
//import com.vaadin.ui.UI;
//
//@SpringComponent
//@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
//public class OccurrenceTableInteractivePlugin
//      implements VCMSComponent
//{
//   private static final Logger LOGGER = LoggerFactory
//         .getLogger(OccurrenceTableInteractivePlugin.class);
//   @Autowired
//   private Werbeo werbeo;
//
//   @Autowired
//   private NavigationResolver navigationResolver;
//   private OccurrenceEditTableView view;
//
//
//   public OccurrenceTableInteractivePlugin(@Autowired MessagesResource messagesResource, @Autowired Werbeo werbeo)
//   {
//      view = new OccurrenceEditTableViewImpl( messagesResource, werbeo);
//   }
//
//   @Override
//   public void enter(ViewChangeListener.ViewChangeEvent event)
//   {
//      VaadinAwareAsync.runAsync(() -> updateView(event));
//   }
//
//   private void updateView(ViewChangeListener.ViewChangeEvent event)
//   {
//      try
//      {
//         // New Sample/Occurrence
//         if (StringUtils.isBlank(event.getParameters()))
//         {
//            Sample newSample = new Sample();
//            SurveyFilter filter = new SurveyFilter();
//            filter.setPortalId(Context.getCurrent().getPortal().getPortalId());
//            newSample.setSurvey(werbeo.surveys()
//                  .find(0, 1, SurveySortField.ID, SortOrder.ASC, filter)
//                  .getContent()
//                  .get(0));
//            UI.getCurrent().access(() -> view.setSample(newSample));
//
//         }
//         else // Edit existing occurrence
//         {
//            UUID occurrenceUUID = UUID.fromString(event.getParameters());
//            LOGGER.info("Going to load occurrence {}", occurrenceUUID);
//            Occurrence occurrence = werbeo.occurrences()
//                  .get(Context.getCurrent().getApp().getPortalId(),
//                        occurrenceUUID).getOccurrence();
//            UI.getCurrent().access(() -> view.setSample(werbeo.samples()
//                  .get(Context.getCurrent().getPortal().getPortalId(),
//                        occurrence.getSample().getEntityId()).getSample()));
//
//         }
//      }
//      catch (Exception e)
//      {
//         LOGGER.error("Failure loading occurrence", e);
//      }
//   }
//
//   @Override
//   public Component getVaadinComponent()
//   {
//      return view.getVaadinComponent();
//   }
//
//   @Override
//   public void init(Map<String, String> parameter)
//   {
//      LOGGER.debug("Init with parameter: {}", parameter);
//   }
//
//   public void setWerbeo(Werbeo werbeo)
//   {
//      this.werbeo = werbeo;
//   }
//
//
//}
