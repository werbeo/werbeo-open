package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetableinter;

public interface ColumnEnum
{
   public String getColumnId();
}
