package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetableinter;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableObserver;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableView;

public interface OccurrenceEditTableView extends CMSVaadinView

{
   public enum Columns
   {
      TAXON, DATE, MTB, SREF_SYSTEM, PRECISION, DETERMINER, MOD_DATE, UUID, EXTERNAL_KEY, COMMENT, PUBLICATION, SREF, DETAILS;
   }

   public void setSample(Sample sample);
}
