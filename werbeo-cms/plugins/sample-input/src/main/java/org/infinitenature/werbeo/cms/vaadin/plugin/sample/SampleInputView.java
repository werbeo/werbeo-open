package org.infinitenature.werbeo.cms.vaadin.plugin.sample;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.response.OccurrenceCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SampleCommentsSliceResponse;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentUpload;

public interface SampleInputView extends CMSVaadinView
{
   void setWerbeo(Werbeo werbeo, Person loggedInUser, InstanceConfig instanceConfig);

   void setTopSurveyId(int topSurveyId);


   void setSample(Sample sample);

   void reset();

   void setHerbariorumClient(HerbariorumClient client);

   void setSampleComments(SampleCommentsSliceResponse comments);

   void setOccurrenceComments(OccurrenceCommentsSliceResponse comments);

   Map<UUID, Collection<DocumentUpload>> getDocumentsToUpload();

   Map<UUID, Collection<Document>> getDocumentsToDelete();

   Double getLastMapPositionLat();

   Double getLastMapPositionLon();

   Double getLastMapZoom();

   void setupPositionAndZoom(double lat, double lon, double zoom);
   
   void setQuarasekClient(Quarasek quarasekClient);

}
