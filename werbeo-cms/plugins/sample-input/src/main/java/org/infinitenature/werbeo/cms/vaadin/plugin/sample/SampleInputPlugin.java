package org.infinitenature.werbeo.cms.vaadin.plugin.sample;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Base64;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.support.MediaUpload;
import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.Occurrences;
import org.infinitenature.werbeo.cms.navigation.SampleNew;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentUpload;
import org.infinitenature.werbeo.cms.vaadin.notification.NotificationUtils;
import org.infinitenature.werbeo.cms.vaadin.plugin.sample.SampleInputViewImpl.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SampleInputPlugin implements VCMSComponent, Observer
{
   private static final Logger LOGGER = LoggerFactory
	 .getLogger(SampleInputPlugin.class);
   @Autowired
   private Werbeo werbeo;

   @Autowired
   private HerbariorumClient client;
   @Autowired
   private InstanceConfig instanceConfig;

   private SampleInputView view;

   @Autowired
   private NavigationResolver navigationResolver;

   public SampleInputPlugin(@Autowired
         Werbeo werbeo,@Autowired
         MessagesResource messages)
   {
      int portalId = Context.getCurrent().getPortal().getPortalId();
      view = new SampleInputViewImpl(new Sample(), this,
            werbeo.samples().getFieldConfig(portalId),
            werbeo.occurrences().getFieldConfig(portalId).getFieldConfigs(),
            werbeo.portals()
                  .getPortalConfiguration(
                        Context.getCurrent().getPortal().getPortalId())
                  .getPortalConfiguration(), messages);
      Context context = Context.getCurrent();
      if (context.getMapPositionConfig().getLastMapPositionLat() != null
            && context.getMapPositionConfig().getLastMapPositionLon() != null
            && context.getMapPositionConfig().getLastMapZoom() != null)
      {
         view.setupPositionAndZoom(
               context.getMapPositionConfig().getLastMapPositionLat(),
               context.getMapPositionConfig().getLastMapPositionLon(),
               context.getMapPositionConfig().getLastMapZoom());
      }
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      view.setWerbeo(werbeo, null, instanceConfig);
      view.setTopSurveyId(werbeo.portals()
            .getPortalConfiguration(
                  Context.getCurrent().getPortal().getPortalId())
            .getPortalConfiguration().getDefaultDataEntrySurveyId());
      view.setHerbariorumClient(client);
      view.setQuarasekClient(Quarasek.getInstance(instanceConfig.getQuarasekUrl()));
   }


   @Override
   public void enter(ViewChangeEvent event)
   {
      updateView();
      updateOccurrenceView(event);
   }

  private void updateView()
   {
      Person person = createIndiciaUser();
      view.setWerbeo(werbeo, person, instanceConfig);
      UI.getCurrent().getScrollTop();
   }

   /*
    * find logged in user in Incdicia, does not already exist in Indicia? create it!
    */
   private Person createIndiciaUser()
   {
      Authentication authentication = Context.getCurrent().getAuthentication();

      Person person = new Person();
      person.setEmail(authentication.getEmail().get());
      person.setFirstName(authentication.getGivenName().orElse(""));
      person.setLastName(authentication.getFamilyName().orElse(""));

      return person;
   }

   private void updateOccurrenceView(ViewChangeEvent event)
   {
      try
      {
         if (StringUtils.isNotBlank(event.getParameters()))
         {
            UUID sampleUUID = UUID.fromString(event.getParameters());

            LOGGER.info("Going to load sample {}", sampleUUID);
              UI.getCurrent().access(() -> {view.setSample(
                  werbeo.samples()
                        .get(Context.getCurrent().getPortal().getPortalId(),
                              sampleUUID)
                        .getSample());
         });


         } else
         {
            view.reset();
         }
      } catch (Exception e)
      {
         LOGGER.error("Failure loading occurrence", e);
      }
   }

   @Override
   public void onSaveClick(Sample sample)
   {
      ProgressBar progressBar = new ProgressBar();
      Window window = new Window();
      window.setModal(true);
      window.setClosable(false);
      window.setContent(progressBar);

      UI.getCurrent().addWindow(window);
      Map<UUID, Collection<DocumentUpload>> documentsToUpload = view
            .getDocumentsToUpload();
      Map<UUID, Collection<Document>> documentsToDelete = view
            .getDocumentsToDelete();

      float p = 100 / (1 + documentsToUpload.size());
      try
      {
         int portalId = Context.getCurrent().getPortal().getPortalId();
         werbeo.samples().save(portalId,
               sample);
         progressBar.setValue(progressBar.getValue() + p);

         for (Entry<UUID, Collection<Document>> entry : documentsToDelete
               .entrySet())
         {
            for (Document documentToDelete : entry.getValue())
            {
               werbeo.occurrenceMedia().deleteMedia(portalId, entry.getKey(),
                     documentToDelete.getLink().getHref());
            }
         }
         for (UUID occurrenceId : documentsToUpload.keySet())
         {
            Collection<DocumentUpload> docs = documentsToUpload
                  .get(occurrenceId);

            for (DocumentUpload documentUpolad : docs)
            {
               try (InputStream picture = new FileInputStream(
                     documentUpolad.getFile());)
               {
                  byte[] data = IOUtils.toByteArray(picture);
                  String base64Encoded = Base64.getEncoder()
                        .encodeToString(data);
                  MediaUpload mediaUpload = new MediaUpload(
                        documentUpolad.getFile().getName(),
                        documentUpolad.getCaption(), base64Encoded);
                  werbeo.occurrenceMedia().attachMedia(portalId, occurrenceId,
                        mediaUpload);
               } catch (Exception e)
               {
                  NotificationUtils.showError("Fehler beim hochladen",
                        "Das Hochladen von "
                              + documentUpolad.getFile().getName()
                              + " ist fehlgeschlagen");
                  LOGGER.error("Failure uploading {}", documentUpolad, e);
               } finally
               {
                  progressBar.setValue(progressBar.getValue() + p);
               }
            }
         }
         NotificationUtils.showSaveSuccess("",
               "Datensatz erfolgreich gespeichert");
      } catch (Exception e)
      {
         LOGGER.error("Failure saveing", e);
         NotificationUtils.showError("Fehler beim Speichern",
               "Das Speichern der kartierliste ist fehlgeschlagen ");
      } finally
      {
         UI.getCurrent().removeWindow(window);
         Context.getCurrent().getMapPositionConfig()
               .setLastMapPositionLat(view.getLastMapPositionLat());
         Context.getCurrent().getMapPositionConfig()
               .setLastMapPositionLon(view.getLastMapPositionLon());
         Context.getCurrent().getMapPositionConfig()
               .setLastMapZoom(view.getLastMapZoom());
         view.reset();
      }

      view.reset();
      navigationResolver.navigateTo(new SampleNew());
   }

   @Override
   public void onDeleteClick(Sample sample)
   {
      werbeo.samples().delete(Context.getCurrent().getPortal().getPortalId(),
            sample.getEntityId());
      view.reset();
      navigationResolver.navigateTo(new Occurrences());
   }

}
