package org.infinitenature.werbeo.cms.vaadin.plugin.sample;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.Amount;
import org.infinitenature.service.v1.types.Occurrence.RecordStatus;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatusFukarek;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.VagueDate.VagueDateType;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.service.v1.types.meta.OccurrenceField;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.response.OccurrenceCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SampleCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SampleFieldConfigResponse;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.formatter.PersonFormatter;
import org.infinitenature.werbeo.cms.properties.OccurrenceProperties;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.caption.AmountItemCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.AreaCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.BloomingSproutsCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.ReproductionCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusFukarekCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SexCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.VitalityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.components.AdviceCombobox;
import org.infinitenature.werbeo.cms.vaadin.components.AmountComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.AreaComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.BloomingSproutsComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.Heading;
import org.infinitenature.werbeo.cms.vaadin.components.LifeStageComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.MakropterComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.PositiveIntegerField;
import org.infinitenature.werbeo.cms.vaadin.components.PublicationField;
import org.infinitenature.werbeo.cms.vaadin.components.ReproductionComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.SampleMethodComobBox;
import org.infinitenature.werbeo.cms.vaadin.components.SettlementStatusComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.SettlementStatusFukarekComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.SexComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TextFieldWithButton;
import org.infinitenature.werbeo.cms.vaadin.components.TimeComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.VagueDateTimeField;
import org.infinitenature.werbeo.cms.vaadin.components.VitalityComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentUpload;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsField;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsUploadField;
import org.infinitenature.werbeo.cms.vaadin.components.indexherbariorum.HerbariorumField;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapEditComponent;
import org.infinitenature.werbeo.cms.vaadin.components.numericamount.NumericAmountWithAccuracy;
import org.infinitenature.werbeo.cms.vaadin.components.numericamount.NumericAmountWithAccuracyField;
import org.infinitenature.werbeo.cms.vaadin.fieldconfig.FieldConfigUtils;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.infinitenature.werbeo.cms.vaadin.notification.NotificationUtils;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.layouts.MGridLayout;

import com.jarektoro.responsivelayout.ResponsiveLayout;
import com.jarektoro.responsivelayout.ResponsiveRow;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.SerializableComparator;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.grid.ScrollDestination;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractSingleSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class SampleInputViewImpl  extends I18NComposite
      implements SampleInputView
{
   private static final String OCCURRENCE_INPUT_FIELD = "occurrence-input-field";
   private MessagesResource messagesResource = new MessagesResourceMock();
   private Observer observer;
   private int topSurveyId = 14;
   private Werbeo werbeo;
   private InstanceConfig instanceConfig;
   private PublicationField publicationField = new PublicationField();

   public static interface Observer
   {
      public void onSaveClick(Sample sample);

      public void onDeleteClick(Sample sample);
   }

   private Sample sample;

   private Occurrence selectedOccurrence;

   //General
   private VerticalLayout mainLayout = new VerticalLayout();

   private Grid<Occurrence> occurrenceGrid = new Grid<>();

   private TextField determinationCommentField = new TextField();

   private Panel optionalPanel;

   private Map<UUID, Collection<DocumentUpload>> documentUploads = new HashMap<>();
   private Map<UUID, Collection<Document>> documentsToDelete = new HashMap<>();
   private Tab personTab;
   private Tab quantiyTab;
   private Tab statusTab;
   private Tab habitatTab;
   private Tab herbaryTab;
   private Tab remarkTab;
   private Tab attachmentsTab;
   private Tab publicationTab;
   private Heading mandatoryHeader = new Heading();
   private Heading optionalHeader = new Heading();

   GridLayout mandatoryInputLayout = new GridLayout(2,2);

   private TaxonComboBox taxonField;
   private VagueDateTimeField dateButton = new VagueDateTimeField(
         messagesResource, null, null, Collections.EMPTY_MAP, false);
   private TextFieldWithButton dateField = new TextFieldWithButton(
         "Datum der Aufnahme", "mit Klick auf Kalender wählen",
         VaadinIcons.CALENDAR, dateButton);
   private ComboBox<Person> determinerField = new ComboBox<>();
   private ComboBox<Person> recorderField = new ComboBox<>();
   private TextField findersField = new TextField("Mitbeobachter");
   private SettlementStatusFukarekComboBox occurrenceStatusComboBox = new SettlementStatusFukarekComboBox(messagesResource);

   private GridLayout comments;


   private final Map<SampleField, Boolean> sampleFieldConfigs;
   private final Map<OccurrenceField, Boolean> occurrenceFieldConfigs;

   // Status
   private AdviceCombobox<SettlementStatusFukarek> settlementStatusFukarekField;
   private SettlementStatusComboBox settlementStatusField;
   private VitalityComboBox vitalityField;
   private SexComboBox sexField;
   private ReproductionComboBox reproductionField;
   private SampleMethodComobBox sampleMethodField;
   private LifeStageComboBox lifeStageField;
   private MakropterComboBox makropterField;
   
   private TextArea remark;

   // Quantity
   private AreaComboBox areaField = new AreaComboBox(messagesResource);
   private BloomingSproutsComboBox bloomingSproutsField = new BloomingSproutsComboBox(
         messagesResource);
   private PositiveIntegerField numericAmmountField = new PositiveIntegerField();
   private NumericAmountWithAccuracyField numericAmountWithAccuracyField;
   private AdviceCombobox<Amount> amountField;

   // Document
   private HerbariorumField herbariorumField;
   private TextField herbaryNumber = new TextField("Herbarnr./Ablageort");
   private DocumentsUploadField occurrenceDocumentUploadField = new DocumentsUploadField(
         messagesResource);
   private DocumentsField occurrenceDocumentsField = new DocumentsField(
         messagesResource);

   private MapEditComponent mapField;
   private TextField blurField = new TextField();
   private TextField locationCommentField = new TextField("Fundortbeschreibung");
   private TextField localityField = new TextField("Fundort");
   private TextField habitatField = new TextField("Wuchsort/Habitat");

   private Button saveButtonTop = new MButton("Speichern und Abschließen", e -> tryToSave());
   private Button saveButtonBottom = new MButton("Speichern und Abschließen", e -> tryToSave());
   private Button deleteButtonTop = new MButton(VaadinIcons.TRASH, "Löschen",
         e -> observer.onDeleteClick(sample)).withVisible(false);
   private Button deleteButtonBottom = new MButton(VaadinIcons.TRASH, "Löschen",
         e -> observer.onDeleteClick(sample)).withVisible(false);
   private Person currentPerson;

   private Column areaColumn = null;
   private Column bloomingSproutsColumn = null;
   private Column amountColumn = null;
   private Column numericAmountColumn = null;
   private Column reproductionColumn = null;
   private Column settlementStatusColumn = null;
   private Column settlementStatusFukarekColumn = null;
   private Column sexColumn = null;
   private Column vitalityColumn = null;
   private Column habitatColumn = null;

   private GeometryHelper geometryHelper = new GeometryHelper();

   public SampleInputViewImpl(Sample sample, Observer observer,
         SampleFieldConfigResponse sampleFieldConfig,
         Set<OccurrenceFieldConfig> occurrenceFieldConfig,
         PortalConfiguration portalConfiguration, MessagesResource messagesResource)
   {
      super(messagesResource);
      this.sampleFieldConfigs = FieldConfigUtils
            .toSampleConfigMap(sampleFieldConfig.getFieldConfigs());
      this.occurrenceFieldConfigs = FieldConfigUtils
            .toOccurrenceConfigMap(occurrenceFieldConfig);
      mapField = new MapEditComponent(
            "https://wms.test.infinitenature.org/geoserver/werbeo/wms",PositionType.SHAPE,
             portalConfiguration, false, PositionType.POINT, PositionType.SHAPE, PositionType.MTB);
      this.observer = observer;
      this.settlementStatusFukarekField = new AdviceCombobox<Occurrence.SettlementStatusFukarek>(
            messagesResource, new SettlementStatusFukarekComboBox(
                  messagesResource));
      this.settlementStatusFukarekField.setComboboxStyleName(OCCURRENCE_INPUT_FIELD);
      this.settlementStatusField = new SettlementStatusComboBox(
            messagesResource);
      this.sexField = new SexComboBox(messagesResource);
      this.sampleMethodField = new SampleMethodComobBox(messagesResource,
            sampleFieldConfig);
      this.reproductionField = new ReproductionComboBox(messagesResource);
      this.lifeStageField = new LifeStageComboBox(messagesResource);
      this.makropterField = new MakropterComboBox(messagesResource);
      this.vitalityField = new VitalityComboBox(messagesResource);
      this.taxonField = new TaxonComboBox(messagesResource, false);
      this.numericAmountWithAccuracyField = new NumericAmountWithAccuracyField(
            messagesResource);
      this.amountField = new AdviceCombobox<Occurrence.Amount>(messagesResource, new AmountComboBox(messagesResource));

      this.remark = new TextArea();
      remark.setWidth("500px");
      
      setupOccurrenceGrid();
      dateButton.addValueChangeListener(event ->{
         dateField.getTextField().setValue(VagueDate.format(dateButton.getValue(), false));
      });
      mainLayout.setSizeFull();
      dateButton.setStyleName("filter-geo-button");
      HorizontalLayout topButtonLayout = new HorizontalLayout();
      topButtonLayout.addComponents(deleteButtonTop, saveButtonTop);
      topButtonLayout.addStyleName("align-right");
      HorizontalLayout topLayout = new HorizontalLayout();
      mandatoryHeader = new Heading("Pflichtangaben");
      topLayout.addComponent(mandatoryHeader);
      topLayout.addComponent(topButtonLayout);
      topLayout.addStyleName("input-top-line");

      mainLayout.addComponent(topLayout);
      mainLayout.addComponent(createMandatoryPanel());
      mainLayout.addComponent(createOccurrencePanel());
      optionalHeader = new Heading("Fakultative Angaben");
      optionalHeader.setVisible(false);
      mainLayout.addComponent(optionalHeader);
      mainLayout.addComponent(createOptionalPanel());
      mainLayout.setMargin(new MarginInfo(false, true));

      HorizontalLayout bottomButtonLayout = new HorizontalLayout();
      bottomButtonLayout.addComponents(deleteButtonBottom,saveButtonBottom);
      bottomButtonLayout.addStyleName("align-right");
      mainLayout.addComponent(bottomButtonLayout);
      setCompositionRoot(mainLayout);

      initValues();

      mapField.addValueChangeListener(event ->
      {
         int precision = 0;

         if(mapField.getValue().getType().equals(PositionType.POINT) && StringUtils.isNotBlank(blurField.getValue()))
         {
            precision = Integer.valueOf(blurField.getValue());
         }
         else
         {
            precision = geometryHelper.calculatePrecision(mapField.getValue().getWkt(), mapField.getValue().getEpsg());
         }

         if(precision > 0)
         {
            blurField.setValue("" + precision);
         }
         if(StringUtils.isNotBlank(blurField.getValue()))
         {
            mapField.setBlur(Integer.valueOf(blurField.getValue()));
         }
      });
      mapField.setValue(null);
      addChangeListenersToFields();
      this.sample = sample;
   }

   private void addChangeListenersToFields()
   {
      // entries in grid shall be updated, whenever occurrence data is updated

      areaField.addValueChangeListener(event -> {
         selectedOccurrence.setCoveredArea(areaField.getValue());
         refreshGrid();
         });

      bloomingSproutsField.addValueChangeListener(event -> {
         selectedOccurrence.setBloomingSprouts(bloomingSproutsField.getValue());
         refreshGrid();
         });

      numericAmmountField.addValueChangeListener(event -> {
         selectedOccurrence.setNumericAmount(numericAmmountField.getValue());
         refreshGrid();
         });


      reproductionField.addValueChangeListener(event -> {
         selectedOccurrence.setReproduction(reproductionField.getValue());
         refreshGrid();
         });

      settlementStatusField.addValueChangeListener(event -> {
         selectedOccurrence.setSettlementStatus(settlementStatusField.getValue());
         refreshGrid();
         });

      settlementStatusFukarekField.addValueChangeListener(event -> {
         selectedOccurrence.setSettlementStatusFukarek(settlementStatusFukarekField.getValue());
         refreshGrid();
         });

      sexField.addValueChangeListener(event -> {
         selectedOccurrence.setSex(sexField.getValue());
         refreshGrid();
         });

      lifeStageField.addValueChangeListener(event -> {
         selectedOccurrence.setLifeStage(lifeStageField.getValue());
         refreshGrid();
      });

      makropterField.addValueChangeListener(event -> {
         selectedOccurrence.setMakropter(makropterField.getValue());
         refreshGrid();
      });

      vitalityField.addValueChangeListener(event -> {
         selectedOccurrence.setVitality(vitalityField.getValue());
         refreshGrid();
         });
      determinationCommentField.addValueChangeListener(event -> {
         selectedOccurrence.setDeterminationComment(determinationCommentField.getValue());
         refreshGrid();
         });

      occurrenceDocumentUploadField.addValueChangeListener(event -> {
         documentUploads.put(selectedOccurrence.getEntityId(), occurrenceDocumentUploadField.getValue());
      });

      habitatField.addValueChangeListener(event -> {
         selectedOccurrence.setHabitat(habitatField.getValue());
         refreshGrid();
         });
      
      remark.addValueChangeListener(event -> {
         selectedOccurrence.setRemark(remark.getValue());
         refreshGrid();
      });

      numericAmountWithAccuracyField.addValueChangeListener(event ->
      {
         if (event.getValue() != null)
         {
         selectedOccurrence.setNumericAmount(event.getValue().getAmount());
         selectedOccurrence
                  .setNumericAmountAccuracy(event.getValue().getAccuracy());
         }
         else
         {
            selectedOccurrence.setNumericAmount(null);
            selectedOccurrence.setNumericAmountAccuracy(null);
         }
         refreshGrid();
      });
      amountField.addValueChangeListener(event ->
      {
         selectedOccurrence.setAmount(amountField.getValue());
         refreshGrid();
      });
   }

   private void refreshGrid()
   {
      if (!sample.getOccurrences().isEmpty())
      {
         occurrenceGrid.setItems(sample.getOccurrences());
         occurrenceGrid.recalculateColumnWidths();
      }
   }

   private void setupOccurrenceGrid()
   {
      occurrenceGrid.setSelectionMode(SelectionMode.SINGLE);

      setupOccurrenceGridColumns();
      occurrenceGrid.addStyleName("occurrence-table-sample-input");
      occurrenceGrid.addStyleName("occurrence-table");
      occurrenceGrid.addStyleName("occurrence-table-view");
      occurrenceGrid.setSizeFull();
      occurrenceGrid.addSelectionListener(event -> {
         if(occurrenceGrid.getSelectedItems() != null && !occurrenceGrid.getSelectedItems().isEmpty())
         {
            updateDocumentsToDelete();
            selectedOccurrence = occurrenceGrid.getSelectedItems().iterator().next();
            updateFieldsWithSelectedOccurrence();
         }
         else
         {
            // don't allow unselect, select the already selected occurrence, when user tries to unselect the row
            occurrenceGrid.select(selectedOccurrence);
         }
      });
   }

   void updateDocumentsToDelete()
   {
      if(!occurrenceDocumentsField.getDocumentsToDelete().isEmpty()) {
         if (!documentsToDelete
               .containsKey(selectedOccurrence.getEntityId()))
         {
            documentsToDelete.put(selectedOccurrence.getEntityId(), new HashSet<>());
         }
         documentsToDelete.get(selectedOccurrence.getEntityId()).addAll(occurrenceDocumentsField.getDocumentsToDelete());
         selectedOccurrence.getDocuments()
               .removeAll(occurrenceDocumentsField.getDocumentsToDelete());
      }
   }

   private void setupOccurrenceGridColumns()
   {
      occurrenceGrid.addComponentColumn(occurrence -> {
         Button button = new Button("Entfernen");
         button.addClickListener(click -> {
            sample.getOccurrences().remove(occurrence);
            refreshGrid();
         });
         return button;
      });
      occurrenceGrid.addComponentColumn(occurrence ->
      {
         TaxonComboBox comboBox = new TaxonComboBox(messagesResource, werbeo,
               instanceConfig, false, true,
               false).withSelected(
                     occurrence
                           .getTaxon())
                     .withOnChange(
                           event -> occurrence.setTaxon(event.getValue()))
                     .withSizeFull();
         comboBox.addFocusListener(event -> {
           occurrenceGrid.select(occurrence);
         });
         return comboBox;
      }).setCaption("Taxon").setWidth(520).setComparator(new SerializableComparator<Occurrence>()
      {
         @Override
         public int compare(Occurrence o1, Occurrence o2)
         {
            return o1.getTaxon().getName().compareTo(o2.getTaxon().getName());
         }
      });
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.TIME_OF_DAY))
      {
         occurrenceGrid.addComponentColumn(occurrence -> {
            TimeComboBox timeBox = new TimeComboBox(messagesResource)
                  .withSelected(occurrence.getTimeOfDay())
                  .withOnChange(
                        event -> occurrence.setTimeOfDay(event.getValue()))
                  .withSizeFull();
            return timeBox;
         }).setCaption("Uhrzeit").setWidth(150)
               .setComparator(new SerializableComparator<Occurrence>()
               {
                  @Override
                  public int compare(Occurrence o1, Occurrence o2)
                  {
                     return o1.getTimeOfDay().compareTo(o2.getTimeOfDay());
                  }
               });
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.AREA))
      {
         areaColumn = occurrenceGrid.addColumn(
               occurrence -> new AreaCaptionGenerator(messagesResource)
                     .apply(occurrence.getCoveredArea()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.BLOOMING_SPROUTS))
      {
         bloomingSproutsColumn = occurrenceGrid.addColumn(occurrence ->
         new BloomingSproutsCaptionGenerator(messagesResource).apply(occurrence.getBloomingSprouts()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.AMOUNT))
      {
         amountColumn = occurrenceGrid.addColumn(
               occurrence -> new AmountItemCaptionGenerator(messagesResource)
                     .apply(occurrence.getAmount()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.NUMERIC_AMOUNT))
      {
         numericAmountColumn = occurrenceGrid.addColumn(occurrence -> occurrence.getNumericAmount());
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.REPRODUCTION))
      {
         reproductionColumn = occurrenceGrid.addColumn(
               occurrence -> new ReproductionCaptionGenerator(messagesResource)
                     .apply(occurrence.getReproduction()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.SETTLEMENT_STATUS))
      {
         settlementStatusColumn = occurrenceGrid
               .addColumn(occurrence -> new SettlementStatusCaptionGenerator(
                     messagesResource).apply(occurrence.getSettlementStatus()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.SETTLEMENT_STATUS_FUKAREK))
      {
         settlementStatusFukarekColumn = occurrenceGrid.addColumn(
               occurrence -> new SettlementStatusFukarekCaptionGenerator(
                     messagesResource)
                           .apply(occurrence.getSettlementStatusFukarek()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.SEX))
      {
         sexColumn = occurrenceGrid.addColumn(
               occurrence -> new SexCaptionGenerator(messagesResource)
                     .apply(occurrence.getSex()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.VITALITY))
      {
         vitalityColumn = occurrenceGrid.addColumn(
               occurrence -> new VitalityCaptionGenerator(messagesResource)
                     .apply(occurrence.getVitality()));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.HABITAT))
      {
         habitatColumn = occurrenceGrid.addColumn(
               occurrence -> occurrence.getHabitat());
      }

      occurrenceGrid.recalculateColumnWidths();
   }

   private void tryToSave()
   {
      boolean valid = prepareSampleForSaving();
      if (valid)
      {
         observer.onSaveClick(this.sample);
      }
   }

   private Component createOccurrencePanel()
   {
      Panel panel = new Panel();
      VerticalLayout occurrencePanel = new VerticalLayout();

      HorizontalLayout addTaxonLayout = new HorizontalLayout();
      Label label = new Label("Neue Art");
      Button addTaxonButton = new Button("Hinzufügen");
      addTaxonButton.setEnabled(false);
      taxonField.setStyleName(OCCURRENCE_INPUT_FIELD);

      taxonField.addValueChangeListener(event -> {
         addTaxonButton.setEnabled(taxonField.getValue() != null);
      });

      addTaxonLayout.addComponent(label);
      addTaxonLayout.addComponent(taxonField);

      addTaxonButton.addClickListener(click ->{

         if(taxonField.getValue() != null)
         {
            optionalHeader.setVisible(true);
            optionalPanel.setVisible(true);

            Occurrence occurrence = new Occurrence();
            occurrence.setTaxon(taxonField.getValue());
            occurrence.setRecordStatus(RecordStatus.COMPLETE);
            occurrence.setEntityId(UUID.randomUUID());
            occurrence.setDeterminer(currentPerson);
            if (occurrenceFieldConfigs.containsKey(OccurrenceField.OBSERVERS))
            {
               occurrence.setObservers(findersField.getValue());
            }
            sample.getOccurrences().add(occurrence);
            refreshGrid();
            occurrenceGrid.select(occurrence);
            occurrenceGrid.scrollTo(sample.getOccurrences().indexOf(occurrence), ScrollDestination.START);
            taxonField.setValue(null);
         }
      });
      addTaxonLayout.addComponent(addTaxonButton);

      occurrencePanel.addComponent(addTaxonLayout);
      occurrencePanel.addComponent(occurrenceGrid);

      panel.setContent(occurrencePanel);
      return panel;
   }

   private Component createOptionalPanel()
   {
      optionalPanel = new Panel();

      TabSheet sheet = new TabSheet();
      sheet.addStyleName("margin-left");
      sheet.addStyleName("occurrence-input-tabsheet");
      personTab = addTab(sheet, createPersonsLayout());
      quantiyTab = addTab(sheet, createQuantityLayout());
      statusTab = addTab(sheet, createStatusLayout());
      habitatTab = addTab(sheet, createLocationLayout());
      herbaryTab = addTab(sheet, createDocumentLayout());
      remarkTab = addTab(sheet, createCommentsLayout());
      attachmentsTab = addTab(sheet, createPictureLayout());
      publicationTab = addTab(sheet, createPublicationLayout());

      optionalPanel.setContent(sheet);
      optionalPanel.setVisible(false);
      return optionalPanel;
   }

   private ComponentContainer createPictureLayout()
   {
      GridLayout layout = new MGridLayout(2, 1);

      addConditionaly(layout, occurrenceDocumentUploadField,
            OccurrenceField.MEDIA);
      addConditionaly(layout, occurrenceDocumentsField, OccurrenceField.MEDIA);
      return layout;
   }

   private Tab addTab(TabSheet sheet, ComponentContainer tabContent)
   {
      Tab tab = sheet.addTab(tabContent);
      if (tabContent.getComponentCount() == 0)
      {
         sheet.removeTab(tab);
      }
      return tab;
   }

   private ComponentContainer createCommentsLayout()
   {
      comments = new GridLayout(1, 1);
      comments.setSizeFull();
      comments.setMargin(true);
      comments.setSpacing(true);
      addConditionaly(comments, determinationCommentField,
            OccurrenceField.DETERMINATION_COMMENT);
      addConditionaly(comments, remark,
            OccurrenceField.REMARK);
      return comments;
   }

   private ComponentContainer createDocumentLayout()
   {
      HorizontalLayout layout = new HorizontalLayout();
      layout.setMargin(true);
      herbariorumField = new HerbariorumField(messagesResource);
      VerticalLayout herbarLayout = new VerticalLayout();
      herbaryNumber.setStyleName(OCCURRENCE_INPUT_FIELD);

      addConditionaly(herbarLayout, herbariorumField, OccurrenceField.HERBARIUM);
      addConditionaly(herbarLayout, herbaryNumber, OccurrenceField.HERBARIUM);


      if(herbarLayout.getComponentCount() > 0 )
      {
         layout.addComponent(herbarLayout);
      }
      return layout;
   }

   private ComponentContainer createLocationLayout()
   {
      GridLayout gridLayout = new GridLayout(2, 2);
      gridLayout.setSizeFull();
      gridLayout.setMargin(true);
      gridLayout.setSpacing(true);

      addConditionaly(gridLayout, habitatField, OccurrenceField.HABITAT);
      habitatField.setStyleName(OCCURRENCE_INPUT_FIELD);
      return gridLayout;
   }
   
   private ComponentContainer createPublicationLayout()
   {
      ResponsiveLayout layout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = layout.addRow();
      row.setMargin(true);
      row.setSpacing(true);
      row.addComponent(publicationField);
      return layout;
   }

   private ComponentContainer createStatusLayout()
   {
      ResponsiveLayout layout = new ResponsiveLayout()
            .withDefaultRules(12, 12, 12, 6).withSpacing();
      ResponsiveRow row = layout.addRow();
      row.setMargin(true);
      row.setSpacing(true);

      addConditionaly(row, settlementStatusField,
            OccurrenceField.SETTLEMENT_STATUS);
      addConditionaly(row, sexField, OccurrenceField.SEX);
      addConditionaly(row, reproductionField,
            OccurrenceField.REPRODUCTION);

      addConditionaly(row, settlementStatusFukarekField,
            OccurrenceField.SETTLEMENT_STATUS_FUKAREK);
      addConditionaly(row, vitalityField, OccurrenceField.VITALITY);
      addConditionaly(row, lifeStageField, OccurrenceField.LIFE_STAGE);
      addConditionaly(row, makropterField, OccurrenceField.MAKROPTER);

      vitalityField.setStyleName(OCCURRENCE_INPUT_FIELD);
      sexField.addStyleName(OCCURRENCE_INPUT_FIELD);
      reproductionField.addStyleName(OCCURRENCE_INPUT_FIELD);
      lifeStageField.addStyleName(OCCURRENCE_INPUT_FIELD);
      makropterField.addStyleName(OCCURRENCE_INPUT_FIELD);

      return layout;
   }

   private ComponentContainer createQuantityLayout()
   {
      GridLayout gridLayout = new GridLayout(2, 2);
      gridLayout.setMargin(true);
      gridLayout.setSizeFull();
      gridLayout.setSpacing(true);

      bloomingSproutsField.setStyleName(OCCURRENCE_INPUT_FIELD);
      areaField.setStyleName(OCCURRENCE_INPUT_FIELD);

      addConditionaly(gridLayout, areaField, OccurrenceField.AREA);
      addConditionaly(gridLayout, bloomingSproutsField,
            OccurrenceField.BLOOMING_SPROUTS);
      if (!addConditionaly(gridLayout, numericAmountWithAccuracyField,
            OccurrenceField.NUMERIC_AMOUNT,
            OccurrenceField.NUMERIC_AMOUNT_ACCURACY))
      {
         addConditionaly(gridLayout, numericAmmountField,
               OccurrenceField.NUMERIC_AMOUNT);
      }
      addConditionaly(gridLayout, amountField, OccurrenceField.AMOUNT);
      return gridLayout;
   }

   private ComponentContainer createPersonsLayout()
   {
      GridLayout gridLayout = new GridLayout(2, 1);
      gridLayout.setSizeFull();
      gridLayout.setSpacing(true);
      gridLayout.setMargin(true);
      determinerField.setItemCaptionGenerator(PersonFormatter::format);
      determinerField.setPopupWidth("auto");
      determinerField.setCaption("Bestimmer");
      // #604: Admins can change determiners/recorders
      determinerField.setEnabled(Context.getCurrent().isAdmin());
      determinerField.setStyleName(OCCURRENCE_INPUT_FIELD);

      recorderField.setItemCaptionGenerator(PersonFormatter::format);
      recorderField.setPopupWidth("auto");
      recorderField.setCaption("Beobachter");
      // #604: Admins can change determiners/recorders
      recorderField.setEnabled(Context.getCurrent().isAdmin());
      recorderField.setStyleName(OCCURRENCE_INPUT_FIELD);

      if (occurrenceFieldConfigs.containsKey(OccurrenceField.DETERMINER))
      {
         determinerField.setRequiredIndicatorVisible(
               occurrenceFieldConfigs.get(OccurrenceField.DETERMINER));
         gridLayout.addComponent(determinerField);
      }
      if (sampleFieldConfigs.containsKey(SampleField.RECORDER))
      {
         gridLayout.addComponent(recorderField);
         recorderField.setRequiredIndicatorVisible(
               sampleFieldConfigs.get(SampleField.RECORDER));
      }



      findersField.setStyleName(OCCURRENCE_INPUT_FIELD);
      return gridLayout;
   }

   private Component createMandatoryPanel()
   {
      Panel panel = new Panel();
      VerticalLayout layout = new VerticalLayout();


      findersField.addValueChangeListener(change ->{
         for(Occurrence occ : sample.getOccurrences())
         {
            occ.setObservers(findersField.getValue());
         }
      });

      dateButton.addStyleName(OCCURRENCE_INPUT_FIELD);
      locationCommentField.setStyleName(OCCURRENCE_INPUT_FIELD);
      dateField.addStyleName(OCCURRENCE_INPUT_FIELD);

      dateField.addStyleName("with-border");
      dateField.getTextField().setEnabled(false);

      blurField.setStyleName(OCCURRENCE_INPUT_FIELD);
      blurField.setCaption("Ungenauigkeit der Ortsangabe in m");
      blurField.setRequiredIndicatorVisible(true);
      blurField.addValueChangeListener(event ->
      {
         mapField.setBlur(Integer.valueOf(blurField.getValue()));
      });
      localityField.setStyleName(OCCURRENCE_INPUT_FIELD);
      localityField.setDescription("Bsp.: Peenetal bei Gützkow oder Galgenberg bei Franzburg");
      mapField.setHeight("800px");
      mapField.setWidth("100%");

      determinationCommentField.addStyleName(OCCURRENCE_INPUT_FIELD);

      mandatoryInputLayout.setSizeFull();
      mandatoryInputLayout.setSpacing(true);
      mandatoryInputLayout.setMargin(false);

      refreshMandatoryLayout();

      layout.addComponent(mandatoryInputLayout);
      GridLayout mapLayout = new  GridLayout(1,1);
      mapLayout.setMargin(new MarginInfo(false, false));
      mapLayout.setSizeFull();
      mapLayout.addComponent(mapField);
      layout.addComponent(mapLayout);
      layout.addComponent(new GridLayout(1, 1));
      layout.setSpacing(false);
      panel.setContent(layout);
      return panel;

   }

   private boolean addConditionaly(ComponentContainer componentContainer,
         AbstractField<?> component, OccurrenceField... fields)
   {
      boolean containsAll = false;
      for (OccurrenceField field : fields)
      {
         if (occurrenceFieldConfigs.containsKey(field))
         {
            containsAll = true;
         } else
         {
            containsAll = false;
            break;
         }
      }
      if (containsAll)
      {
         componentContainer.addComponent(component);
         component
               .setRequiredIndicatorVisible(occurrenceFieldConfigs.get(fields[0]));
         return true;
      }
      return false;
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractField<?> component, OccurrenceField field)
   {
      if (occurrenceFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component
               .setRequiredIndicatorVisible(occurrenceFieldConfigs.get(field));
      }
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractSingleSelect<?> component, OccurrenceField field)
   {
      if (occurrenceFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component
               .setRequiredIndicatorVisible(occurrenceFieldConfigs.get(field));
      }
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractField<?> component, SampleField field)
   {
      if (sampleFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component.setRequiredIndicatorVisible(sampleFieldConfigs.get(field));
      }
   }

   private void addConditionaly(ComponentContainer componentContainer,
         AbstractSingleSelect<?> component, SampleField field)
   {
      if (sampleFieldConfigs.containsKey(field))
      {
         componentContainer.addComponent(component);
         component.setRequiredIndicatorVisible(sampleFieldConfigs.get(field));
      }
   }

   private void refreshMandatoryLayout()
   {
      mandatoryInputLayout.removeAllComponents();
      mandatoryInputLayout.addComponents(dateField);
      mandatoryInputLayout.addComponents(blurField);
      addConditionaly(mandatoryInputLayout, localityField,
            SampleField.LOCALITY);
      addConditionaly(mandatoryInputLayout, locationCommentField,
            SampleField.LOCATION_COMMENT);
      mandatoryInputLayout.addComponents(mapField.getControlFields());
      addConditionaly(mandatoryInputLayout, sampleMethodField, SampleField.SAMPLE_METHOD);
      sampleMethodField.addStyleName(OCCURRENCE_INPUT_FIELD);
      addConditionaly(mandatoryInputLayout, findersField,
            OccurrenceField.OBSERVERS);
   }

   private void initValues()
   {
      VagueDate date = new VagueDate();
      date.setFrom(LocalDate.now());
      date.setType(VagueDateType.DAY);
      dateButton.setValue(date);
      dateField.getTextField().setValue(VagueDate.format(dateButton.getValue(), false));
   }

   private boolean prepareSampleForSaving()
   {
      Person finder = recorderField.getValue() != null
            ? recorderField.getValue()
            : currentPerson;

      if (!occurrenceDocumentUploadField.isValid())
      {
         NotificationUtils.showError("", "Die Anhänge sind nicht korrekt");
         return false;
      }
      if (mapField.getValue() == null)
      {
         NotificationUtils.showError("", "Bitte einen Fundort in der Karte  angeben !");
         return false;
      }

      Locality locality = new Locality();
      locality.setPosition(mapField.getValue());

      if (blurField.getValue().isEmpty())
      {
         NotificationUtils.showError("", "Bitte 'Ungenauigkeit der Ortsangabe' angeben !");
         return false;
      }
      else
      {
         try
         {
            int precision = Integer.parseInt(blurField.getValue());
            if(precision > 450000)
            {
               NotificationUtils.showError("", "Bitte 'Ungenauigkeit der Ortsangabe' mit maximal 450000 angeben !");
               return false;
            }
         }
         catch(NumberFormatException e)
         {
            NotificationUtils.showError("", "Bitte 'Ungenauigkeit der Ortsangabe' als Zahl angeben !");
            return false;
         }
      }

      locality.setBlur(Integer.valueOf(blurField.getValue()));
      if (sampleFieldConfigs.containsKey(SampleField.LOCATION_COMMENT))
      {
         locality.setLocationComment(locationCommentField.getValue());
      }
      if (sampleFieldConfigs.containsKey(SampleField.LOCALITY))
      {
         locality.setLocality(this.localityField.getValue());
      }

      sample.setLocality(locality);

      sample.setRecorder(finder);

      if(sample.getSurvey() == null)
      {
         sample.setSurvey(prepapreSurvey());
      }

      sample.setLocality(locality);

      sample.setRecorder(finder);
      sample.setSurvey(prepapreSurvey());
      sample.setDate(dateButton.getValue());
      Occurrence occurrence;

      if (sample.getOccurrences().isEmpty())
      {
         occurrence = new Occurrence();
         sample.getOccurrences().add(occurrence);
      } else
      {
         occurrence = sample.getOccurrences().get(0);
      }


      if (herbariorumField.getValue() != null)
      {
         if (StringUtils.isBlank(herbaryNumber.getValue()))
         {
            NotificationUtils.showError("", "Bitte Herbarnr./Ablageort angeben !");
            return false;
         }
      }

      applyValues(occurrence);

      return true;
   }

   private void applyValues(Occurrence occurrence)
   {
      if (occurrence == null)
      {
         return;
      }
      Person determiner = determinerField.getValue() != null
            ? determinerField.getValue()
            : currentPerson;
      sample.setSampleMethod(sampleMethodField.getValue());
      sample.setDate(dateButton.getValue());
      occurrence.setDeterminer(determiner);

   }

   private SurveyBase prepapreSurvey()
   {
      SurveyBase surveyBase = new SurveyBase();
      surveyBase.setEntityId(topSurveyId);
      return surveyBase;
   }

   @Override
   public void setWerbeo(Werbeo werbeo, Person loggedInUser, InstanceConfig instanceConfig)
   {
      this.currentPerson = loggedInUser;
      this.werbeo = werbeo;
      this.instanceConfig = instanceConfig;
      taxonField.setDataProvider(new TaxonComboBoxDataProvider(werbeo,
            instanceConfig, false, true, true));
      determinerField
            .setDataProvider(new PersonComboBoxDataProvider(werbeo));
      recorderField
      .setDataProvider(new PersonComboBoxDataProvider(werbeo));
   }

   @Override
   public void setTopSurveyId(int topSurveyId)
   {
      this.topSurveyId = topSurveyId;
   }

   @Override
   public void setSample(Sample sample)
   {
      this.sample = sample;
      refreshGrid();
      if(!sample.getOccurrences().isEmpty())
      {
         // select newest occurrence
         selectedOccurrence = sample.getOccurrences().get( sample.getOccurrences().size() - 1);

         if (occurrenceFieldConfigs.containsKey(OccurrenceField.OBSERVERS))
         {
            findersField.setValue(selectedOccurrence.getObservers() == null ? ""
                  : selectedOccurrence.getObservers());
         }
      }
      occurrenceGrid.select(selectedOccurrence);
      if (sample != null && !sample.getOccurrences().isEmpty()
            && sample.getOccurrences().get(0).getDeterminer() != null)
      {
         determinerField
               .setValue(sample.getOccurrences().get(0).getDeterminer());
      }
      else if (determinerField.getValue() == null)
      {
         determinerField.setValue(currentPerson);
      }

      if (sample != null && sample.getRecorder() != null)
      {
         recorderField.setValue(sample.getRecorder());
      }
      else if (recorderField.getValue() == null)
      {
         recorderField.setValue(currentPerson);
      }
      mapField.setValue(sample.getLocality().getPosition());
      blurField.setValue(String.valueOf(sample.getLocality().getBlur()));
      mapField.setBlur(Integer.valueOf(blurField.getValue()));
      if (sample.getLocality().getLocationComment() != null)
      {
         locationCommentField
               .setValue(sample.getLocality().getLocationComment());
      }
      if (sample.getLocality().getLocality() != null)
      {
         localityField.setValue(sample.getLocality().getLocality());
      }

      updateFieldsWithSelectedOccurrence();

      dateButton.setValue(sample.getDate());
      dateField.getTextField()
            .setValue(VagueDate.format(dateButton.getValue(), false));

      sampleMethodField.setValue(sample.getSampleMethod());

      saveButtonTop.setVisible(
            sample.getAllowedOperations().contains(Operation.UPDATE));
      saveButtonBottom.setVisible(
            sample.getAllowedOperations().contains(Operation.UPDATE));
      deleteButtonTop.setVisible(
            sample.getAllowedOperations().contains(Operation.DELETE));
      deleteButtonBottom.setVisible(
            sample.getAllowedOperations().contains(Operation.DELETE));

      refreshMandatoryLayout();
      optionalHeader.setVisible(true);
      optionalPanel.setVisible(true);
   }

   private void updateFieldsWithSelectedOccurrence()
   {
      areaField.setValue(selectedOccurrence.getCoveredArea());
      bloomingSproutsField.setValue(selectedOccurrence.getBloomingSprouts());


      if (selectedOccurrence.getHerbarium() != null)
      {
         if(herbariorumField.getClient() != null && herbariorumField.getClient().isServiceAvailable())
         {
            herbariorumField
                  .setValue(herbariorumField.getClient().getByCode(selectedOccurrence.getHerbarium().getCode()));
         }
         if (selectedOccurrence.getHerbarium().getHerbary() != null)
         {
            herbaryNumber.setValue(selectedOccurrence.getHerbarium().getHerbary());
         }
      }
      occurrenceStatusComboBox.setValue(this.selectedOccurrence.getSettlementStatusFukarek());
      vitalityField.setValue(this.selectedOccurrence.getVitality());
      sexField.setValue(selectedOccurrence.getSex());
      settlementStatusFukarekField
            .setValue(selectedOccurrence.getSettlementStatusFukarek());
      settlementStatusField.setValue(selectedOccurrence.getSettlementStatus());
      numericAmmountField.setValue(selectedOccurrence.getNumericAmount());
      numericAmountWithAccuracyField.setValue(
            NumericAmountWithAccuracy.of(selectedOccurrence.getNumericAmount(),
                  selectedOccurrence.getNumericAmountAccuracy()));
      reproductionField.setValue(selectedOccurrence.getReproduction());
      lifeStageField.setValue(selectedOccurrence.getLifeStage());
      makropterField.setValue(selectedOccurrence.getMakropter());
      occurrenceDocumentsField.setValue(selectedOccurrence.getDocuments().stream()
            .filter(d -> d.getType() == DocumentType.IMAGE
                  || d.getType() == DocumentType.AUDIO)
            .collect(Collectors.toList()));
      occurrenceDocumentsField
            .setAllowedOperations(sample.getAllowedOperations());
      occurrenceDocumentUploadField.setValue(documentUploads.get(selectedOccurrence.getEntityId()));
      determinationCommentField
            .setValue(selectedOccurrence.getDeterminationComment() != null
                  ? selectedOccurrence.getDeterminationComment()
                  : "");
      habitatField.setValue(selectedOccurrence.getHabitat() != null ? selectedOccurrence.getHabitat() : "");

      remark.setValue(selectedOccurrence.getRemark() != null ? selectedOccurrence.getRemark() : "");
      
      if (selectedOccurrence.getObservers() != null)
      {
         findersField.setValue(selectedOccurrence.getObservers());
      }
      if (StringUtils.isNotBlank(selectedOccurrence.getCiteId()))
      {
         publicationField.setPublication(selectedOccurrence.getCiteId(),
               selectedOccurrence.getCiteComment());
         publicationTab.setVisible(true);
      } else
      {
         publicationTab.setVisible(false);
      }
   }

   @Override
   public void reset()
   {
      sample = new Sample();
      determinerField.setValue(currentPerson);
      recorderField.setValue(currentPerson);
   }

   @Override
   public void setHerbariorumClient(HerbariorumClient client)
   {
      this.herbariorumField.init(client);
   }

   @Override
   public void setSampleComments(SampleCommentsSliceResponse comments)
   {
      for (SampleComment comment : comments.getContent())
      {

         this.comments.addComponent(new Label(DateTimeFormatter
               .ofPattern("dd.MM.yyyy").format(comment.getCreationDate())));
         this.comments.addComponent(new Label(comment.getComment()));
      }
   }

   @Override
   public void setOccurrenceComments(OccurrenceCommentsSliceResponse comments)
   {
      for (OccurrenceComment comment : comments.getContent())
      {
         this.comments.addComponent(new Label(DateTimeFormatter
               .ofPattern("dd.MM.yyyy").format(comment.getCreationDate())));
         this.comments.addComponent(new Label(comment.getComment()));
      }
   }

   @Override
   public void applyLocalizations()
   {
      Locale locale = this.getLocale();
      org.infinitenature.werbeo.cms.vaadin.Portal portal = Context.getCurrent()
            .getPortal();
      personTab.setCaption(getMessages()
            .getMessage(portal, "occurrence.group.person", locale));
      quantiyTab.setCaption(getMessages()
            .getMessage(portal, "occurrence.group.quantity", locale));
      statusTab.setCaption(getMessages()
            .getMessage(portal, "occurrence.group.status", locale));
      habitatTab.setCaption(getMessages()
            .getMessage(portal, "occurrence.group.habitat", locale));
      herbaryTab.setCaption(getMessages()
            .getMessage(portal, "occurrence.group.herbary", locale));
      remarkTab.setCaption(getMessages()
            .getMessage(portal, "occurrence.group.remark", locale));
      attachmentsTab.setCaption(getMessages()
            .getMessage(portal, "occurrence.group.attachment", locale));
      publicationTab.setCaption(getMessages().getMessage(portal,
            "occurrence.group.publication", locale));
      mandatoryHeader.setValue(getMessages()
            .getMessage(portal, "group.mandantory.header", locale));
      optionalHeader.setValue(
            getMessages().getMessage(portal, "group.optional.header", locale));
      determinationCommentField.setCaption(getMessages()
            .getEntityField(portal, "Occurrence", "DETERMINATION_COMMENT",
                  locale));
      sexField.setCaption(
            getMessages().getEntityField(portal, "Occurrence", "SEX", locale));

      if(sexColumn != null)
      {
         sexColumn.setCaption(
               getMessages().getEntityField(portal, "Occurrence", "SEX", locale));
      }

      reproductionField.setCaption(getMessages()
            .getEntityField(portal, "Occurrence", "REPRODUCTION", locale));

      lifeStageField.setCaption(getMessages()
            .getEntityField(portal, "Occurrence", "LIFE_STAGE", locale));

      makropterField.setCaption(getMessages()
            .getEntityField(portal, "Occurrence", "MAKROPTER", locale));


      settlementStatusFukarekField.setEmptySelectionCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "SettlementStatusFukarekComboBox", "EMPTY", getLocale()));
      settlementStatusFukarekField.setCaption(getMessages().getEntityField(Context.getCurrent().getPortal(),
            "SettlementStatusFukarekComboBox", "OCCURRENCE_STATUS", getLocale()));
      settlementStatusFukarekField.setAdviceHtmlText(getMessages().getEntityField(
            Context.getCurrent().getPortal(),
            "SettlementStatusFukarekComboBox",
            "MANUAL", getLocale()));

      if(reproductionColumn != null)
      {
         reproductionColumn.setCaption(getMessages()
               .getEntityField(portal, "Occurrence", "REPRODUCTION", locale));
      }
      sampleMethodField.setCaption(getMessages()
            .getEntityField(portal, "Sample", "SAMPLE_METHOD", locale));
      numericAmmountField
            .setCaption(getEntityField(OccurrenceProperties.AMOUNT));
      amountField.setCaption(getEntityField(OccurrenceProperties.AMOUNT));
      amountField.setAdviceHtmlText(getMessages().getEntityField(
            Context.getCurrent().getPortal(),
            "AmountComboBox",
            "MANUAL", getLocale()));
      if(numericAmountColumn != null)
      {
         numericAmountColumn.setCaption(getEntityField(OccurrenceProperties.AMOUNT));
      }
      if(areaColumn != null)
      {
         areaColumn.setCaption(getMessages()
               .getEntityField(portal, "Occurrence", "AREA", locale));
      }
      if(bloomingSproutsColumn != null)
      {
         bloomingSproutsColumn.setCaption(getMessages()
               .getEntityField(portal, "Occurrence", "BLOOMING_SPROUTS", locale));
      }
      if(settlementStatusColumn != null)
      {
         settlementStatusColumn.setCaption(getMessages()
               .getEntityField(portal, "Occurrence", "SETTLEMENT_STATUS", locale));
      }
      if(settlementStatusFukarekColumn != null)
      {
         settlementStatusFukarekColumn.setCaption(getMessages()
               .getEntityField(portal, "Occurrence", "SETTLEMENT_STATUS_FUKAREK", locale));

      }
      if(vitalityColumn != null)
      {
         vitalityColumn.setCaption(getMessages()
               .getEntityField(portal, "Occurrence", "VITALITY", locale));
      }
      if(habitatColumn != null)
      {
         habitatColumn.setCaption(getMessages()
               .getEntityField(portal, "Occurrence", "HABITAT", locale));
      }
      
      remark.setCaption(getMessages().getEntityField(portal,
            "Occurrence", "REMARK", locale));
      
      

   }

   @Override
   public Map<UUID, Collection<Document>> getDocumentsToDelete()
   {
      updateDocumentsToDelete();
      return documentsToDelete;
   }

   @Override
   public Map<UUID, Collection<DocumentUpload>> getDocumentsToUpload()
   {
      return documentUploads;
   }

   @Override
   public Double getLastMapPositionLat()
   {
      return mapField.getPositionLat();
   }

   @Override
   public Double getLastMapPositionLon()
   {
      return mapField.getPositionLon();
   }

   @Override
   public Double getLastMapZoom()
   {
      return mapField.getZoom();
   }

   @Override
   public void setupPositionAndZoom(double lat, double lon, double zoom)
   {
      mapField.setupPositionAndZoom(lat, lon, zoom);
   }
   
   @Override
   public void setQuarasekClient(Quarasek quarasekClient)
   {
      publicationField.setQuarasekClient(quarasekClient);
   }
}
