package org.infinitenature.werbeo.cms.vaadin.plugin.html;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.vaadin.viritin.label.RichText;

public class TestRichText
{
   private RichText richText;
   
   @Before
   public void setUp()
   {
      richText = new RichText();
   }
   
   @Test
   public void testHeader()
   {
      richText.withMarkDown("# header");
      assertThat("<h1>header</h1>\n", is(richText.getText()));
      
      richText.withMarkDown("## header");
      assertThat("<h2>header</h2>\n", is(richText.getText()));
      
      richText.withMarkDown("### header");
      assertThat("<h3>header</h3>\n", is(richText.getText()));
      
      richText.withMarkDown("#### header");
      assertThat("<h4>header</h4>\n", is(richText.getText()));
      
      richText.withMarkDown("##### header");
      assertThat("<h5>header</h5>\n", is(richText.getText()));
      
      richText.withMarkDown("###### header");
      assertThat("<h6>header</h6>\n", is(richText.getText()));
   }
   
   @Test
   public void testBreakLine()
   {
      richText.withMarkDown("test");
      assertThat("<p>test</p>\n", is(richText.getText()));
      
      richText.withMarkDown("test  \ntest2");
      assertThat("<p>test\n<br  />test2</p>\n", is(richText.getText()));
   }
   
   @Test
   public void testCitation()
   {
      richText.withMarkDown("> Dies ist ein Zitat. \n> Dies auch.");
      assertThat("<blockquote><p>Dies ist ein Zitat.\n<br  />Dies auch.</p>\n</blockquote>\n", is(richText.getText()));
   }
   
   @Test
   public void testLists()
   {
      richText.withMarkDown("1. Hund\n2. Katze\n3. Maus");
      assertThat("<ol>\n<li>Hund</li>\n<li>Katze</li>\n<li>Maus</li>\n</ol>\n", is(richText.getText()));
      
      richText.withMarkDown("+ Hund\n+ Katze\n+ Maus");
      assertThat("<ul>\n<li>Hund</li>\n<li>Katze</li>\n<li>Maus</li>\n</ul>\n", is(richText.getText()));
      
      richText.withMarkDown("1. Hund\n    Bello ist klasse.\n2. Katze\n3. Maus");
      assertThat("<ol>\n<li>Hund\n<br  />Bello ist klasse.</li>\n<li>Katze</li>\n<li>Maus</li>\n</ol>\n", is(richText.getText()));
   }
   
   @Test
   public void testCodeBlocks()
   {
      richText.withMarkDown("    Dies ist ein Code-Block.");
      assertThat("<pre><code>Dies ist ein Code-Block.\n</code></pre>\n", is(richText.getText()));
   }
   
   @Test
   public void testLine()
   {
      richText.withMarkDown("---");
      assertThat("<hr />\n", is(richText.getText()));
   }
   
   @Test
   public void testLink()
   {
      richText.withMarkDown("Dies ist [ein Beispiel](http://example.com \"Der Linktitel\") für einen Inline-Link.");
      assertThat("<p>Dies ist <a href=\"http://example.com\" title=\"Der Linktitel\">ein Beispiel</a> für einen Inline-Link.</p>\n", is(richText.getText()));
   }
   
   @Test
   public void testStrong()
   {
      richText.withMarkDown("__Doppelte Unterstriche__");
      assertThat("<p><strong>Doppelte Unterstriche</strong></p>\n", is(richText.getText()));
   }
}
