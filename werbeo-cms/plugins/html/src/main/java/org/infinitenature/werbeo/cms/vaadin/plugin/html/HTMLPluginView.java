package org.infinitenature.werbeo.cms.vaadin.plugin.html;

import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface HTMLPluginView extends CMSVaadinView
{
   /**
    *
    * @param content
    *           the content with markdown markup
    */
   public void setContent(String content);
}
