package org.infinitenature.werbeo.cms.vaadin.plugin.html;

import java.util.Map;

import org.infinitenature.vct.VCMSComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HtmlPlugin implements VCMSComponent
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(HtmlPlugin.class);
   public static final String PARAMETER_CONTENT = "content";
   private Map<String, String> parameter;
   private HTMLPluginView view = new HTMLPluginViewImpl();

   @Override
   public HTMLPluginView getVaadinComponent()
   {
      return view;

   }

   @Override
   public void init(Map<String, String> parameter)
   {
      this.parameter = parameter;
      view.setContent(parameter.get(PARAMETER_CONTENT));
   }
}
