package org.infinitenature.werbeo.cms.vaadin.plugin.html;

import java.util.Map;

import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrivacyPlugin implements VCMSComponent
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PrivacyPlugin.class);
   public static final String PARAMETER_CONTENT = "content";
   private Map<String, String> parameter;
   private HTMLPluginView view = new HTMLPluginViewImpl();

   
   private Werbeo werbeo;
   private MessagesResource messagesResource;
   
   public PrivacyPlugin(@Autowired MessagesResource messages,
         @Autowired Werbeo werbeo)
   {
      this.werbeo = werbeo;
      this.messagesResource = messages;
   }
   
   @Override
   public HTMLPluginView getVaadinComponent()
   {
      return view;

   }

   @Override
   public void init(Map<String, String> parameter)
   {
      this.parameter = parameter;
      parameter.put(HtmlPlugin.PARAMETER_CONTENT,
            werbeo.termsAndConditions()
                  .getPrivacyDeclaration(
                        Context.getCurrent().getPortal().getPortalId())
                  .getText());
      view.setContent(parameter.get(PARAMETER_CONTENT));
   }
}
