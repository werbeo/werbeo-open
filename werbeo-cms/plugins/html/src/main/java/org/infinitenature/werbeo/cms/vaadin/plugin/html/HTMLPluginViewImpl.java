package org.infinitenature.werbeo.cms.vaadin.plugin.html;

import org.vaadin.viritin.label.RichText;

import com.vaadin.ui.Composite;

public class HTMLPluginViewImpl extends Composite
      implements HTMLPluginView
{
   private RichText richText = new RichText();

   public HTMLPluginViewImpl()
   {
      setCompositionRoot(richText);
   }

   @Override
   public void setContent(String content)
   {
      richText.withMarkDown(content);
   }
}
