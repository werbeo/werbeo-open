package org.infinitenature.werbeo.cms.vaadin.plugin.filter;

import java.io.InputStream;

public interface TemplateInputStreamProvider
{
   InputStream provide();
   InputStream provideHelp();
}
