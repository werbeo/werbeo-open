package org.infinitenature.werbeo.cms.vaadin.plugin.filter;

import java.io.InputStream;
import java.util.Map;

import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CSVUploadPlugin implements VCMSComponent, CSVContentConsumer, TemplateInputStreamProvider
{
   private Werbeo werbeo;
   private CSVUploadPluginView view;

   @Autowired
   public CSVUploadPlugin(MessagesResource messagesResource, Werbeo werbeo)
   {
      this.werbeo = werbeo;
      view = new CSVUploadPluginViewImpl(messagesResource, this, this);
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      // NOTHING to do here
   }

   @Override
   public void consume(String csvContent)
   {
      werbeo.occurrenceImports().doImportCsv(
            Context.getCurrent().getPortal().getPortalId(), csvContent);
   }

   @Override
   public InputStream provide()
   {
      return (InputStream) werbeo.occurrenceImports()
            .getCsvTemplate(Context.getCurrent().getPortal().getPortalId())
            .getEntity();
   }
   
   @Override
   public InputStream provideHelp()
   {
      return (InputStream) werbeo.occurrenceImports()
            .getCsvTemplateHelp(Context.getCurrent().getPortal().getPortalId())
            .getEntity();
   }

}
