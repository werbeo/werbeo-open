package org.infinitenature.werbeo.cms.vaadin.plugin.filter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.vaadin.viritin.button.MButton;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class CSVUploadPluginViewImpl extends I18NComposite
      implements CSVUploadPluginView
{
   
   private File uploadingFile;
   private CSVContentConsumer csvContentConsumer;
   private TemplateInputStreamProvider inputStreamProvider;

   public CSVUploadPluginViewImpl(MessagesResource messages,
         CSVContentConsumer csvContentConsumer,
         TemplateInputStreamProvider provider)
   {
      super(messages);
      this.csvContentConsumer = csvContentConsumer;
      this.inputStreamProvider = provider;
      VerticalLayout layout = new VerticalLayout();
      Upload upload = new Upload();
      upload.setCaption("Upload CSV-Datei");
      upload.setButtonCaption("Hochladen");
      upload.addSucceededListener(this::onFinish);
      upload.setReceiver((fileName, mimeType) ->
      {
         Path dir;
         try
         {
            dir = Files.createTempDirectory("userCsvUpload");
            uploadingFile = dir.resolve(fileName).toFile();
            return new FileOutputStream(uploadingFile);
         } catch (IOException e)
         {
            throw new RuntimeException(e);
         }
      });
      layout.addComponent(upload);
      layout.addComponent(createTemplateDownloadButton());
      layout.addComponent(createTemplateHelpDownloadButton());
      setCompositionRoot(layout);
   }

   public void onFinish(Upload.SucceededEvent event)
   {
      try
      {
         String content = FileUtils.readFileToString(uploadingFile);
         csvContentConsumer.consume(content);
         Notification.show("Die CSV-Datei wurde hochgeladen. \nEs werden "
               + (content.split("\r\n|\r|\n").length - 1)
               + " Datensätze importiert.", Type.WARNING_MESSAGE);
         uploadingFile = null;
      }
      catch(Exception e)
      {
         
      }
      
   }

   @Override
   public void applyLocalizations()
   {
      // TODO: do it
   }
   
   private MButton createTemplateDownloadButton()
   {
      MButton downloadButton = new MButton(VaadinIcons.DOWNLOAD);
            
      downloadButton.setCaption("Vorlage herunterladen");

      StreamResource streamResource = createDownloadStreamResource("vorlage.csv");
      FileDownloader fileDownloader = new FileDownloader(streamResource);
      fileDownloader.extend(downloadButton);
      return downloadButton;
   }
   
   private MButton createTemplateHelpDownloadButton()
   {
      MButton downloadButton = new MButton(VaadinIcons.DOWNLOAD);
            
      downloadButton.setCaption("Hinweistexte herunterladen");

      StreamResource streamResource = createDownloadHelpStreamResource("hinweise.txt");
      FileDownloader fileDownloader = new FileDownloader(streamResource);
      fileDownloader.extend(downloadButton);
      return downloadButton;
   }

   private StreamResource createDownloadStreamResource(String fileName)
   {
      return new StreamResource(new StreamSource()
      {
         @Override
         public InputStream getStream()
         {
            return inputStreamProvider.provide();
         }
      }, fileName);
   }
   
   private StreamResource createDownloadHelpStreamResource(String fileName)
   {
      return new StreamResource(new StreamSource()
      {
         @Override
         public InputStream getStream()
         {
            return inputStreamProvider.provideHelp();
         }
      }, fileName);
   }

}
