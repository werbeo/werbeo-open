package org.infinitenature.werbeo.cms.vaadin.plugin.filter;

public interface CSVContentConsumer
{
 public void consume(String csvContent);
}
