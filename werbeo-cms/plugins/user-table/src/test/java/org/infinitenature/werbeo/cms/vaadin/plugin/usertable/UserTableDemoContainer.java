package org.infinitenature.werbeo.cms.vaadin.plugin.usertable;

import org.infinitenature.werbeo.cms.vaadin.Context;

public class UserTableDemoContainer extends AbstractUserTableDemo
{

   @Override
   protected void modifyContext(Context context)
   {
      context.getPortal().setPortalId(2);
   }
}
