package org.infinitenature.werbeo.cms.vaadin.plugin.usertable;

import java.util.Map;

import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.SurveyEdit;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserTablePlugin
      implements VCMSComponent, UserTableView.Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(UserTablePlugin.class);
   @Autowired
   protected Werbeo werbeo;
   @Autowired
   private NavigationResolver navigationResolver;
   protected UserTableView view;
   protected MessagesResource messagesResource;

   public UserTablePlugin(@Autowired MessagesResource messages,
         @Autowired Werbeo werbeo)
   {
      this.messagesResource = messages;
      view = new UserTableViewImpl(this, this.messagesResource,
            werbeo);
   }


   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      LOGGER.debug("Init with parameter: {}", parameter);
      view.getVaadinComponent().setSizeFull();
   }

   @Override
   public void onEditClick(SurveyBase survey)
   {
      navigationResolver.navigateTo(new SurveyEdit(survey.getEntityId()));
   }


   public void setWerbeo(Werbeo werbeo)
   {
      this.werbeo = werbeo;
   }


   public void setNavigationResolver(NavigationResolver navigationResolver)
   {
      this.navigationResolver = navigationResolver;
   }

}
