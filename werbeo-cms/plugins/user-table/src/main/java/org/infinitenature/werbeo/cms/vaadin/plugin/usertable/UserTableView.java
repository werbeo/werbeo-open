package org.infinitenature.werbeo.cms.vaadin.plugin.usertable;

import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableObserver;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableView;

public interface UserTableView extends CMSVaadinView
{
   public enum Columns
   {
      ID, MOD_DATE, NAME, DESCRIPTION, AVAILABILITY, CONTAINER, EDIT
   }

   public static interface Observer
   {
      void onEditClick(SurveyBase occurrence);
   }


}
