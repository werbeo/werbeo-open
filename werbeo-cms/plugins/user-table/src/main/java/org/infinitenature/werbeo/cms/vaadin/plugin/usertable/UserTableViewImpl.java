package org.infinitenature.werbeo.cms.vaadin.plugin.usertable;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.User;
import org.infinitenature.service.v1.types.enums.Role;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class UserTableViewImpl extends I18NComposite
      implements UserTableView
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(UserTableViewImpl.class);
   private Observer observer;

   private Werbeo werbeo;

   private Grid<User> grid;

   private TextField emailFilter = new TextField();


   public UserTableViewImpl(Observer observer,
         MessagesResource messagesResource, Werbeo werbeo)
   {
      super(messagesResource);
      this.werbeo = werbeo;
      this.observer = observer;

      grid = new Grid<User>();
      grid.setId("user-grid");
      grid.setHeight("860px");
      grid.setWidth("100%");

      int portalId = Context.getCurrent().getApp().getPortalId();
      grid.addColumn(User::getEmail).setCaption("Benutzer Email");
      grid.addComponentColumn(user -> {

         CheckBox approvedCheckbox = createCheckBox(portalId,
               "Bestätigt (APPROVED)", user,
               Roles.APPROVED.getRoleName());

         CheckBox validatorCheckbox = createCheckBox(portalId,
               "Validierer", user,
               Roles.VALIDATOR.getRoleName());

         CheckBox adminCheckbox = createCheckBox(portalId,
               "Portal-Admin", user,
               Roles.ADMIN.getRoleName());



         HorizontalLayout checkboxLayout = new HorizontalLayout(
               approvedCheckbox, validatorCheckbox, adminCheckbox);
         return checkboxLayout;
      }).setCaption("Auswahl der Rollen");

      emailFilter.addValueChangeListener(event ->
      {
         if (StringUtils.isNotBlank(emailFilter.getValue())
               && emailFilter.getValue().length() > 1)
         {
            refresh();
         }
      });

      emailFilter.setCaption("Email / Name (mind. 2 Buchstaben)");

      VerticalLayout mainLayout= new VerticalLayout(emailFilter, grid);
      setCompositionRoot(mainLayout);
      grid.addStyleName("occurrence-table");
   }


   private CheckBox createCheckBox(int portalId, String caption, User user,
         String roleName)
   {
      CheckBox checkbox = new CheckBox(caption);
      checkbox.setValue(user.getRoles().contains(Role.valueOf(roleName)));
      checkbox.addValueChangeListener(event -> {
         if(checkbox.getValue().booleanValue())
         {
            this.werbeo.users().addRole(portalId, user.getEmail(),
                  Role.valueOf(roleName));
         }
         else
         {
            this.werbeo.users().removeRole(portalId, user.getEmail(),
                  Role.valueOf(roleName));
         }
         refresh();
      });
      return checkbox;
   }


   private void refresh()
   {
      grid.setItems(
            werbeo.users().find(Context.getCurrent().getApp().getPortalId(),
                  0, Integer.MAX_VALUE, emailFilter.getValue()).getContent());
   }


   @Override
   public void applyLocalizations()
   {

   }
}
