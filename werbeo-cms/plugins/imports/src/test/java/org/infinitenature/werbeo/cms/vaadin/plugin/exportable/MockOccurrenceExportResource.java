package org.infinitenature.werbeo.cms.vaadin.plugin.exportable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceExportResource;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceFilterPOST;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.JobStatus;
import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportStatusResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportsInfoResponse;

final class MockOccurrenceExportResource implements OccurrenceExportResource
{
   private List<OccurrenceExport> exports = new ArrayList<>();

   public MockOccurrenceExportResource()
   {
      exports.add(export(JobStatus.FINISHED));
      exports.add(export(JobStatus.CANCELD));
      exports.add(export(JobStatus.REMOVED));
      exports.add(export(JobStatus.RUNNING));
      exports.add(export(JobStatus.FAILURE));
   }

   private OccurrenceExport export(JobStatus status)
   {
      OccurrenceExport export = new OccurrenceExport();
      export.setId(UUID.randomUUID());
      export.setStatus(status);
      return export;
   }

   @Override
   public OccurrenceExportStatusResponse getStatus(UUID exportId,
         @Min(1) int portalId)
   {
      return null;
   }

   @Override
   public Response getExport(UUID exportId, @Min(1) int portalId)
   {
      return null;
   }

   @Override
   public OccurrenceExportsInfoResponse findExports(@Min(1) int portalId,
         @Min(0) int offset, int limit, OccurrenceExportField sortField,
         SortOrder sortOrder)
   {

      return new OccurrenceExportsInfoResponse(exports.stream().skip(offset)
            .limit(limit).collect(Collectors.toList()), limit);
   }

   @Override
   public OccurrenceExportResponse create(OccurrenceFilter filter,
         @NotNull ExportFormat exportFormat)
   {
      return null;
   }

   @Override
   public DeletedResponse delete(UUID exportId, @Min(1) int portalId)
   {
      return new DeletedResponse();
   }

   @Override
   public CountResponse count(@Min(1) int portalId)
   {
      return new CountResponse(exports.size(), "mock-data");
   }

   @Override
   public OccurrenceExportResponse createLargeWKT(
         @Valid OccurrenceFilterPOST filter, @NotNull ExportFormat exportFormat)
   {
      // TODO Auto-generated method stub
      return null;
   }
}