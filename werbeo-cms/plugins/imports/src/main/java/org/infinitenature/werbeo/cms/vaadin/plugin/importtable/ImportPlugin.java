package org.infinitenature.werbeo.cms.vaadin.plugin.importtable;

import java.io.InputStream;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.infinitenature.service.v1.types.OccurrenceImport;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.plugin.importtable.ImportTableView.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ImportPlugin implements VCMSComponent, Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ImportPlugin.class);

   private final Werbeo werbeo;
   private final NavigationResolver navigationResolver;
   private final MessagesResource messagesResource;

   private final ImportTableView view;
   private ImportDataProvider dataProvider;

   public ImportPlugin(@Autowired Werbeo werbeo,
         @Autowired NavigationResolver navigationResolver,
         @Autowired MessagesResource messagesResource)
   {
      super();
      this.werbeo = werbeo;
      this.navigationResolver = navigationResolver;
      this.messagesResource = messagesResource;
      this.view = new ImportTableViewImpl(this, messagesResource);
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      dataProvider = setData();
      this.view.setSize(dataProvider.getSize(null));
      UI.getCurrent().setPollInterval(30000);
      UI.getCurrent().addPollListener(pollEvent -> {
         dataProvider.refreshAll();
         this.view.setSize(dataProvider.getSize(null));
      });
   }

   @Override
   public void onFilterChange(ImportFilter filter)
   {
      // NOOP since there is no filerable columns
   }

   @Override
   public InputStream onDownloadClick(OccurrenceImport csvImport)
   {
      Response response = werbeo.occurrenceImports().getImport(
            csvImport.getId(), Context.getCurrent().getPortal().getPortalId());
      Object entity = response.getEntity();
      return (InputStream) entity;
   }
   
   @Override
   public InputStream onDownloadErrorLogClick(OccurrenceImport csvImport)
   {
      Response response = werbeo.occurrenceImports().getErrorLog(
            csvImport.getId(), Context.getCurrent().getPortal().getPortalId());
      Object entity = response.getEntity();
      return (InputStream) entity;
   }

   @Override
   public void onDeleteClick(OccurrenceImport csvImport)
   {
      try
      {
         werbeo.occurrenceImports().delete(csvImport.getId(),
               Context.getCurrent().getPortal().getPortalId());
         dataProvider.refreshAll();
         Notification.show(translate("DELETED") + " " + csvImport.getId());
      }
      catch (Exception e)
      {
         Notification.show(translate("NOT_DELETED") + " " + csvImport.getId());
      }
   }

   private ImportDataProvider setData()
   {
      ImportDataProvider dataProvider = new ImportDataProvider(
            werbeo);
      view.setDataProvider(dataProvider.withConfigurableFilter());
      return dataProvider;
   }

   private String translate(String field)
   {
      return messagesResource.getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }
}
