package org.infinitenature.werbeo.cms.vaadin.plugin.importtable;

import java.io.InputStream;

import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.service.v1.types.OccurrenceImport;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableObserver;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableView;

public interface ImportTableView
      extends TableView<OccurrenceImport, ImportFilter>
{
   public enum Columns
   {
      DATE, UUID, STATUS, DOWNLOAD, DELETE, DOWNLOAD_LOG;
   }

   public static interface Observer
         extends TableObserver<ImportFilter>
   {
      public InputStream onDownloadClick(OccurrenceImport csvImport);
      public InputStream onDownloadErrorLogClick(OccurrenceImport csvImport);
      public void onDeleteClick(OccurrenceImport csvImport);
   }

   public void setSize(int size);
}
