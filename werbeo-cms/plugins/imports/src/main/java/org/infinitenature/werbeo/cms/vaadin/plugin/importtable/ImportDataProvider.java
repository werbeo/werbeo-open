package org.infinitenature.werbeo.cms.vaadin.plugin.importtable;

import java.util.List;
import java.util.Optional;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.service.v1.types.OccurrenceImport;
import org.infinitenature.service.v1.types.OccurrenceImportField;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.WerbeoDataProvider;

import com.vaadin.data.provider.Query;

public class ImportDataProvider extends
      WerbeoDataProvider<OccurrenceImport, ImportFilter, OccurrenceImportField>
{

   public ImportDataProvider(Werbeo werbeo)
   {
      super(werbeo);
   }

   @Override
   protected List<OccurrenceImport> getContent(
         Optional<ImportFilter> filter,
         OccurrenceImportField sortField, SortOrder sortOrder, int offset,
         int limit)
   {
      return werbeo.occurrenceImports()
            .findImports(Context.getCurrent().getApp().getPortalId(), offset,
                  limit, sortField, SortOrder.DESC)
            .getContent();
   }

   @Override
   protected OccurrenceImportField toSortField(
         com.vaadin.data.provider.SortOrder<String> vaadinSortOrder)
   {
      return null;
   }

   @Override
   protected OccurrenceImportField getDefaultSortField()
   {
      return OccurrenceImportField.DATE;
   }
   
   

   @Override
   protected int getSize(Query<OccurrenceImport, ImportFilter> query)
   {
      return (int) werbeo.occurrenceImports()
            .count(Context.getCurrent().getApp().getPortalId()).getAmount();
   }

}
