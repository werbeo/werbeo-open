package org.infinitenature.werbeo.cms.vaadin.plugin.importtable;

import java.io.InputStream;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.infinitenature.service.v1.types.JobStatus;
import org.infinitenature.service.v1.types.OccurrenceImport;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.plugin.importtable.ImportTableView.Columns;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.AbstractTableViewImpl;
import org.vaadin.viritin.button.DeleteButton;
import org.vaadin.viritin.button.MButton;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

public class ImportTableViewImpl extends
      AbstractTableViewImpl<OccurrenceImport, Columns, ImportFilter>
      implements ImportTableView
{

   public ImportTableViewImpl(Observer observer,
         MessagesResource messagesResource)
   {
      super(Columns.class, OccurrenceImport.class, observer, messagesResource);
      
      addColumn(csvImport ->
      {
         return csvImport.getCreationDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
      }, Columns.DATE);
      addColumn(OccurrenceImport::getId, Columns.UUID);
      addColumn(OccurrenceImport::getStatus, Columns.STATUS);
      grid.setHeight("800px");

      addComponentColumn(csvImport -> createDownloadButton(observer, csvImport),
            Columns.DOWNLOAD);
      
      addComponentColumn(csvImport -> createDownloadErrorButton(observer, csvImport),
            Columns.DOWNLOAD_LOG);
      
      addComponentColumn(csvImport -> createDeleteButton(observer, csvImport),
            Columns.DELETE);
   }

   private Component createDeleteButton(Observer observer,
         OccurrenceImport csvImport)
   {
      DeleteButton deleteButton = new DeleteButton();
      deleteButton.setConfirmWindowCaption("");
      deleteButton.setCancelCaption(translate("CANCEL"));
      deleteButton.setCaption("");
      deleteButton.setIcon(VaadinIcons.TRASH);
      deleteButton.setConfirmationText(csvImport.getId() + "\n" + translate("QUESTION"));
      deleteButton.setStyleName(ValoTheme.BUTTON_TINY);
      deleteButton.withEnabled(JobStatus.RUNNING != csvImport.getStatus());
      deleteButton
            .addClickListener(clickEvent -> observer.onDeleteClick(csvImport));
      return deleteButton;
   }

   private MButton createDownloadButton(Observer observer,
         OccurrenceImport csvImport)
   {
      MButton downloadButton = new MButton(VaadinIcons.DOWNLOAD)
            .withStyleName(ValoTheme.BUTTON_TINY)
            .withEnabled(csvImport.getStatus() == JobStatus.FINISHED);

      StreamResource streamResource = createDownloadStreamResource(csvImport,
            observer);
      FileDownloader fileDownloader = new FileDownloader(streamResource);
      fileDownloader.extend(downloadButton);
      return downloadButton;
   }
   
   private MButton createDownloadErrorButton(Observer observer,
         OccurrenceImport csvImport)
   {
      MButton downloadButton = new MButton(VaadinIcons.DOWNLOAD)
            .withStyleName(ValoTheme.BUTTON_TINY);
            
      StreamResource streamResource = createDownloadErrorStreamResource(csvImport,
            observer);
      FileDownloader fileDownloader = new FileDownloader(streamResource);
      fileDownloader.extend(downloadButton);
      return downloadButton;
   }

   private StreamResource createDownloadStreamResource(OccurrenceImport csvImport,
         Observer observer)
   {
      return new StreamResource(new StreamSource()
      {
         @Override
         public InputStream getStream()
         {
            return observer.onDownloadClick(csvImport);
         }
      }, "csv-import-"
            + csvImport.getCreationDate().format(DateTimeFormatter.ofPattern("ddMMyyyy-HH_mm_ss"))
            + ".csv" );
   }
   
   private StreamResource createDownloadErrorStreamResource(OccurrenceImport csvImport,
         Observer observer)
   {
      return new StreamResource(new StreamSource()
      {
         @Override
         public InputStream getStream()
         {
            return observer.onDownloadErrorLogClick(csvImport);
         }
      }, "csv-import-errors"
            + csvImport.getCreationDate().format(DateTimeFormatter.ofPattern("ddMMyyyy-HH_mm_ss"))
            + ".log" );
   }

   @Override
   protected ImportFilter buildFilter()
   {
      return new ImportFilter();
   }

   @Override
   protected List<Columns> getSortableColumns()
   {
      return Arrays.asList(Columns.UUID);
   }

   private String translate(String field)
   {
      return getMessages().getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }
}
