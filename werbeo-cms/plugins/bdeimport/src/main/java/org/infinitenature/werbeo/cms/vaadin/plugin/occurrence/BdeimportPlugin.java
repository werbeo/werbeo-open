package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BdeimportPlugin implements VCMSComponent, BdeimportView.Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(BdeimportPlugin.class);
   @Autowired
   private Werbeo werbeo;

   @Autowired
   private MessagesResource messages;

   private BdeimportView view;

   private String fileName;

   public BdeimportPlugin()
   {
      view = new BdeimportViewImpl(this);
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      view.init(messages);
   }

   public void setWerbeo(Werbeo werbeo)
   {
      this.werbeo = werbeo;
   }

   @Override
   public OutputStream receiveUpload(String filename, String mimeType)
   {
      this.fileName = filename;

      File dir = new File(FileUtils.getTempDirectory() + "/"
            + Context.getCurrent().getPortal().getPortalId() + "/"
            + createUserDir(Context.getCurrent().getAuthentication()));
      dir.mkdirs();
      File file = new File(dir, filename);

      try
      {
         return new FileOutputStream(file);
      } catch (FileNotFoundException e)
      {
         e.printStackTrace();
         return null;
      }
   }

   @Override
   public void uploadSucceeded()
   {
      try
      {
         String input = FileUtils
               .readFileToString(new File(FileUtils.getTempDirectory() + "/"
                     + Context.getCurrent().getPortal().getPortalId() + "/"
                     + createUserDir(Context.getCurrent().getAuthentication())
                     + "/" + fileName), "utf-8");
         String sampleCount = werbeo.occurrenceImports().doImportXml(
               Context.getCurrent().getPortal().getPortalId(), input);
         view.importSuccess(sampleCount);

      } catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   private String createUserDir(Authentication authentication)
   {
      return authentication.getEmail().get().replace("@", "").replace(".", "");
   }
}
