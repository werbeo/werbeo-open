package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

import com.vaadin.ui.Upload;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

public interface BdeimportView extends CMSVaadinView
{

   void init(MessagesResource messages);

   public static interface Observer extends Upload.Receiver
   {
      void uploadSucceeded();
   }
   
   public void importSuccess(String sampleCount);
}
