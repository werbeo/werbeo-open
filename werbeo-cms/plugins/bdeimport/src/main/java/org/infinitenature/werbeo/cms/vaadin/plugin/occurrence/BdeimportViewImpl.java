
package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import com.vaadin.ui.*;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;

@SuppressWarnings("serial")
public class BdeimportViewImpl extends CustomComponent
      implements BdeimportView
{

   private VerticalLayout mainLayout = new VerticalLayout();
   private Observer observer;
   private Label successLabel = new Label("");
   private Upload upload;
   private MessagesResource messages;
   private final Label infoLabel;
   private String successLabelCaption;

   public BdeimportViewImpl(Observer observer)
   {
      this.observer = observer;
      infoLabel = new Label();
      mainLayout.addComponent(infoLabel);
      successLabel.setVisible(false);
      upload = new Upload("", observer);
      mainLayout.addComponent(upload);
      setCompositionRoot(mainLayout);
      upload.addSucceededListener(event -> {
         observer.uploadSucceeded();
      });
      mainLayout.addComponent(successLabel);
   }

   @Override
   public void importSuccess(String sampleCount)
   {
      successLabel.setCaption(sampleCount + " " + successLabelCaption);
      successLabel.setVisible(true);
      upload.setEnabled(false);
   }

   public void init(MessagesResource messages)
   {
      this.messages = messages;
      infoLabel.setCaption(translate("ADD_DATA"));
      upload.setButtonCaption(translate("CHOOSE_FILE_UPLOAD"));
      successLabelCaption = translate("IMPORT_SUCCESS");
   }

   private String translate(String field)
   {
      return messages.getEntityField(Context.getCurrent().getPortal(),
            this.getClass().getSimpleName(), field,
            UI.getCurrent().getLocale());
   }

}
