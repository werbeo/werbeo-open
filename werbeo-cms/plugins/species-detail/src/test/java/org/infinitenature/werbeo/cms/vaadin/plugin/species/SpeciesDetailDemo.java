package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import static org.mockito.Mockito.mock;

import java.util.HashMap;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.testing.client.WerbeoMockFactory;
import org.vaadin.addonhelpers.AbstractTest;
import org.vaadin.viritin.button.MButton;

import com.vaadin.annotations.Push;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Push
public abstract class SpeciesDetailDemo extends AbstractTest
{
   private SpeciesDetailPlugin plugin;
   protected final WerbeoMockFactory werbeoMockFactory = new WerbeoMockFactory();

   public SpeciesDetailDemo()
   {
      Context context = createContext();

      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
      Werbeo werbeo = werbeoMockFactory.get();
      plugin = new SpeciesDetailPlugin(new MessagesResourceMock(),
            new InstanceConfig());
      plugin.setWerbeo(werbeo);
   }

   abstract Context createContext();

   @Override
   public Component getTestComponent()
   {
      UI.getCurrent().setNavigator(new Navigator(UI.getCurrent(), new Panel()));
      plugin.init(new HashMap<>());

      return new VerticalLayout(plugin.getVaadinComponent(),
            new MButton("Naviagte to Urtica", e -> onClick(e)));
   }

   private void onClick(ClickEvent e)
   {
      plugin.enter(new ViewChangeEvent(mock(Navigator.class), null, null, null,
            "Urtica dioica"));
   }

}
