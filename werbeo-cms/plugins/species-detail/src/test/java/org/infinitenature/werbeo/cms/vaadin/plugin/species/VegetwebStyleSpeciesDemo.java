package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Portal;

public class VegetwebStyleSpeciesDemo extends SpeciesDetailDemo
{
   @Override
   Context createContext()
   {
      {
         Context context = new Context();
         context.setApp(new Portal("Heuschrecken Deutschland", 2, 2));
         return context;
      }
   }
}