package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Portal;

public class FloraMVStyleSpeciesDemo extends SpeciesDetailDemo
{
   @Override
   Context createContext()
   {
      {
         Context context = new Context();
         context.setApp(new Portal("Flora-MV", 1, 1));
         return context;
      }
   }
}
