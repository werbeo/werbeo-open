package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;

import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface SpeciesDetailView extends CMSVaadinView
{
   interface Observer
   {
      void onTaxonSelected(Optional<TaxonBase> taxon);
   }

   void addLinkCreator(Function<Taxon, LinkDescriptor> speciesLinkCreator);

   void setTaxon(Taxon taxon);

   void setCoveredMTB(Collection<MTB> coveredMTBs);
}
