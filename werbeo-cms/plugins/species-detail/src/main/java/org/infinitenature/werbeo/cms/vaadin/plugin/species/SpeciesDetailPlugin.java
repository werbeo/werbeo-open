package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.vct.TitleUpdateListener;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.TaxonDetail;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SpeciesDetailPlugin
      implements VCMSComponent, SpeciesDetailView.Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SpeciesDetailPlugin.class);
   @Autowired
   private Werbeo werbeo;
   @Autowired
   private NavigationResolver navigationResolver;
   @Autowired
   private InstanceConfig instanceConfig;

   private SpeciesDetailView view;
   private MessagesResource messages;
   private Taxon taxon = null;
   private PortalConfiguration portalConfiguration;
   private TitleUpdateListener titleUpdateListener;

   public SpeciesDetailPlugin(@Autowired MessagesResource messages,
         @Autowired InstanceConfig instanceConfig)
   {
      this.messages = messages;
      this.instanceConfig = instanceConfig;
   }

   @Override
   public void setTitleUpdateListener(TitleUpdateListener titleUpdateListener)
   {
      this.titleUpdateListener = titleUpdateListener;
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void nonUIinit(Map<String, String> parameter)
   {
      portalConfiguration = werbeo
            .portals()
            .getPortalConfiguration(
                  Context.getCurrent().getPortal().getPortalId())
            .getPortalConfiguration();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      view = new SpeciesDetailViewImpl(messages, werbeo, this, instanceConfig,
            portalConfiguration.getStaticMapStyle(), portalConfiguration);

      if (Context.getCurrent().getPortal().getName().equals("Flora-MV"))
      {
         view.addLinkCreator(linkTaxon -> new LinkDescriptor(
               "https://karten.deutschlandflora.de/map.phtml?config=taxnr"
                     + linkTaxon.getExternalKey(),
               linkTaxon.getName() + " auf www.deutschlandflora.de"));
      } else if (Context.getCurrent().getPortal().getName()
            .equals("Heuschrecken Deutschland"))
      {
         view.addLinkCreator(linkTaxon -> new LinkDescriptor(
               "http://dgfo-articulata.de/heuschrecken/arten/"
                     + linkTaxon.getName().replace(" ", "_"),
               "Informationen zu " + linkTaxon.getName() + " bei der DGfO"));
      }
   }

   @Override
   public void enter(ViewChangeEvent event)
   {
      String parameters = event.getParameters();
      loadTaxon(parameters);
      if (taxon != null)
      {
         titleUpdateListener.onTitleParameterUpdate();

         view.setTaxon(taxon);

         view.setCoveredMTB(werbeo.taxa()
               .getCoveredMTB(Context.getCurrent().getPortal().getPortalId(),
                     taxon.getEntityId(), true)
               .getCoveredMTBs());
      }
   }

   @Override
   public Map<String, String> getTitleParameters()
   {
      Map<String, String> parameter = new HashMap<>();
      if (taxon != null)
      {
         parameter.put("TAXON", taxon.getName());
      } else
      {
         parameter.put("TAXON", "Verbreitung");
      }
      return parameter;
   }

   private void loadTaxon(String parameters)
   {
      String taxonName = parameters.replace("%20", " ");
      LOGGER.info("Enter with taxonName: {}", taxonName);
      if (taxonName.isEmpty())
      {
         taxon = null;
         return;
      }
      try
      {
         List<TaxonBase> content = werbeo.taxa()
               .find(0, 1,
                     new TaxaFilter(Context.getCurrent().getApp().getPortalId(),
                           taxonName, new HashSet<>(), true, null, true, null, false))
               .getContent();
         if (!content.isEmpty())
         {
         TaxonBase taxonBase = content.get(0);
         Optional<Taxon> preferredTaxon = werbeo.taxa()
               .findAllSynonyms(Context.getCurrent().getApp().getPortalId(),
                     taxonBase.getEntityId())
               .getContent().stream()
               .filter(Taxon::isPreferred)
               .findFirst();
         if (preferredTaxon.isPresent())
         {
            // load the taxon, to get documents-info for distribution map
             taxon = werbeo.taxa()
                     .getTaxon(Context.getCurrent().getApp().getPortalId(),
                           preferredTaxon.get().getEntityId())
                     .getTaxon();
         } else
         {
            LOGGER.warn("No preferred taxon found for {} with id {}",taxonBase.getName(), taxonBase.getEntityId());
         }
      } else
      {
         LOGGER.info("No taxon with taxonName: {} found", taxonName);
      }
      } catch (Exception e)
      {
         taxon = null;
         Notification.show("Hinweis", "Keine Daten vorhanden",
               Notification.Type.HUMANIZED_MESSAGE);
      }
   }

   @Override
   public void setInitialParameters(String parameters)
   {
      loadTaxon(parameters);
   }

   @Override
   public Optional<String> getSeoDescription()
   {
      if (taxon != null)
      {
         String message = messages.getMessage(Context.getCurrent().getApp(),
               "seo.description.speciesdetail", Locale.GERMANY,
               taxon.getName());
         if (!message.startsWith("!"))
         {
            return Optional
                  .of(message);
         }
      }
      return VCMSComponent.super.getSeoDescription();
   }

   @Override
   public Optional<String> getSeoImageURL()
   {
      if (taxon != null)
      {
         switch (portalConfiguration.getStaticMapStyle())
         {
         case FLORA_MV:
            return MapsUtils.findFloraMVStyleMapThumbnailURL(taxon);
         case VEGETWEB:
            return MapsUtils.findVegetwebStyleMapThumbnailURL(taxon);

         }
      }
      return VCMSComponent.super.getSeoImageURL();

   }

   public void setWerbeo(Werbeo werbeo)
   {
      this.werbeo = werbeo;
   }

   @Override
   public void onTaxonSelected(Optional<TaxonBase> taxon)
   {
      if (taxon.isPresent())
      {
         if (this.taxon == null
               || (!this.taxon.getEntityId().equals(taxon.get().getEntityId())))
         {
            String taxonName = taxon.get().getName();
            if (taxonName.contains("("))
            {
               taxonName = taxonName
                     .substring(0, taxon.get().getName().indexOf('(')).trim();
            }

            taxonName = taxonName.replace(" ", "%20").replaceAll("\"", "");
            TaxonDetail target = new TaxonDetail(taxonName);

            navigationResolver.navigateTo(target);
         }
      } else
      {
         navigationResolver.navigateTo(new TaxonDetail(null));
      }
   }
}
