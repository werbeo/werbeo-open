package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import java.util.Optional;

import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Taxon;

public class MapsUtils
{
   private MapsUtils()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static Optional<String> findVegetwebStyleMapThumbnailURL(Taxon taxon)
   {
      return taxon.getDocuments().stream()
            .filter(doc -> doc.getType()
                  .equals(DocumentType.MAP_TAXON_VEGETWEB_STYLE_THUMB_L))
            .map(doc -> doc.getLink().getHref()).findFirst();
   }

   public static Optional<String> findFloraMVStyleMapThumbnailURL(
         Taxon taxon)
   {
      return taxon.getDocuments().stream()
            .filter(doc -> doc.getType()
                  .equals(DocumentType.MAP_TAXON_FLORA_MV_STYLE_THUMB))
            .map(doc -> doc.getLink().getHref()).findFirst();
   }
}
