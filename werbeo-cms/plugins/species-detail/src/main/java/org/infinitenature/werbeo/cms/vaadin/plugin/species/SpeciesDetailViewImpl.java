package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.enums.StaticMapStyle;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.maps.ComponentLocality;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapViewComponent;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SpeciesDetailViewImpl extends CustomComponent
      implements SpeciesDetailView
{
   private VerticalLayout mainLayout = new VerticalLayout();
   private VerticalLayout linkLayout = new VerticalLayout();
   private MapViewComponent mapComponent;
   private TaxonComboBox taxonComboBox;
   private final InstanceConfig instanceConfig;
   private List<Function<Taxon, LinkDescriptor>> speciesLinkCreators = new ArrayList<>();
   private final StaticMapStyle staticMapStyle;
   public SpeciesDetailViewImpl(MessagesResource messages, Werbeo werbeo,
         Observer observer, InstanceConfig instanceConfig,
         StaticMapStyle staticMapStyle, PortalConfiguration portalConfiguration)
   {
      this.instanceConfig = instanceConfig;
      this.staticMapStyle = staticMapStyle;

      mapComponent = new MapViewComponent(
            "https://wms.test.infinitenature.org/geoserver/werbeo/wms",
            portalConfiguration);
      Panel panel = new Panel();
      HorizontalLayout taxonLayout = new HorizontalLayout();
      taxonLayout.setMargin(new MarginInfo(true, true));
      taxonComboBox = new TaxonComboBox(messages, werbeo, instanceConfig, true,
            false, true);
      taxonComboBox.addStyleName("occurrence-input-field");
      taxonComboBox.addValueChangeListener(event -> observer
            .onTaxonSelected(Optional.ofNullable(event.getValue())));
      taxonLayout.addComponent(taxonComboBox);
      HorizontalLayout contentLayout = new HorizontalLayout();
      contentLayout.setMargin(new MarginInfo(false, true));
      mainLayout.addComponent(taxonLayout);
      HorizontalLayout map2Layout = new HorizontalLayout();
      map2Layout.setWidth("700px");
      map2Layout.setHeight("500px");
      map2Layout.addComponent(mapComponent);
      contentLayout.addComponent(map2Layout);
      contentLayout.addComponent(linkLayout);
      mainLayout.addComponent(contentLayout);

      mainLayout.setMargin(new MarginInfo(false, true));
      mainLayout.setSizeFull();
      mainLayout.addComponent(new GridLayout(1, 1));
      panel.setContent(mainLayout);
      setCompositionRoot(panel);
   }

   @Override
   public void setCoveredMTB(Collection<MTB> coveredMTBs)
   {
      coveredMTBs = new HashSet<MTB>(coveredMTBs);
      mapComponent.setValues(coveredMTBs.stream().map(mtb ->
      {
         Position position = new Position();
         position.setMtb(mtb);
         position.setType(PositionType.MTB);
         Locality locality = new Locality();
         locality.setPosition(position);
         ComponentLocality componentLocality = new ComponentLocality(locality, null);
         return componentLocality;
      }).collect(Collectors.toList()), false);
   }

   @Override
   public void addLinkCreator(
         Function<Taxon, LinkDescriptor> speciesLinkCreator)
   {
      this.speciesLinkCreators.add(speciesLinkCreator);
   }


   @Override
   public void setTaxon(Taxon taxon)
   {
      linkLayout.removeAllComponents();
      taxonComboBox.setValue(taxon);


      Image embedded = createMapThumbnail(taxon);
      linkLayout.addComponent(embedded);
      speciesLinkCreators
            .forEach(c -> linkLayout
                  .addComponent(createLinkComponent(c.apply(taxon))));
   }

   private Link createLinkComponent(LinkDescriptor linkDescriptor)
   {
      Link link = new com.vaadin.ui.Link(linkDescriptor.getDescription(),
            new ExternalResource(linkDescriptor.getTarget()));
      link.setTargetName("_blank");
      link.setIcon(VaadinIcons.EXTERNAL_LINK);
      return link;
   }

   public Image createMapThumbnail(Taxon selectedTaxon)
   {
      switch (staticMapStyle)
      {
      case FLORA_MV:
         return floraStyleMap(selectedTaxon);
      case VEGETWEB:

         return vegetwebStyleMap(selectedTaxon);
      default:
         return new Image();
      }
   }

   private Image vegetwebStyleMap(Taxon taxon)
   {
      Optional<String> thumbURL = MapsUtils.findVegetwebStyleMapThumbnailURL(taxon);

      ExternalResource externalResourceThumb = new ExternalResource(
            thumbURL.orElse(""));
      Image embeddedThumb = new Image(null, externalResourceThumb);

      Optional<String> bigURL = taxon.getDocuments().stream().filter(
            doc -> doc.getType().equals(DocumentType.MAP_TAXON_VEGETWEB_STYLE))
            .map(doc -> doc.getLink().getHref()).findFirst();

      ExternalResource externalResourceBig = new ExternalResource(
            bigURL.orElse(""));

      Image embeddedBig = new Image(null, externalResourceBig);
      embeddedBig.setSizeUndefined();
      embeddedBig.setHeight("100%");

      embeddedBig.addStyleName("dist-map-big");
      embeddedThumb.addClickListener(click ->
      {

         Window window = new Window();
         window.center();
         window.setWidth("95%");
         window.setHeight("70%");
         HorizontalLayout layout = new HorizontalLayout();
         layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
         layout.setSizeFull();
         layout.setSpacing(true);
         layout.setMargin(true);
         layout.addComponent(embeddedBig);
         window.setContent(layout);
         UI.getCurrent().addWindow(window);
      });

      embeddedThumb.addStyleName("clickable");
      return embeddedThumb;
   }

   private Image floraStyleMap(Taxon selectedTaxon)
   {
      Optional<String> thumbURL = MapsUtils.findFloraMVStyleMapThumbnailURL(selectedTaxon);

      ExternalResource externalResourceThumb = new ExternalResource(
            thumbURL.orElse(""));
      Image embeddedThumb = new Image(null, externalResourceThumb);

      Optional<String> bigURL = selectedTaxon.getDocuments().stream().filter(
            doc -> doc.getType().equals(DocumentType.MAP_TAXON_FLORA_MV_STYLE))
            .map(doc -> doc.getLink().getHref()).findFirst();

      ExternalResource externalResourceBig = new ExternalResource(
            bigURL.orElse(""));

      Image embeddedBig = new Image(null, externalResourceBig);

      embeddedBig.addStyleName("dist-map-big");
      Image legende = new Image(null,
            new ThemeResource("img/dist_map_legende.png"));
      legende.addStyleName("legende-image");

      embeddedThumb.addClickListener(click ->
      {

         Window window = new Window();
         window.center();
         window.setWidth("95%");
         window.setHeight("70%");
         HorizontalLayout layout = new HorizontalLayout();
         layout.setSizeFull();
         layout.addStyleName("dist-map-layout");
         layout.setSpacing(true);
         layout.setMargin(true);
         layout.addComponent(embeddedBig);
         layout.addComponent(legende);
         layout.addStyleName("overflow-auto");
         window.setContent(layout);
         UI.getCurrent().addWindow(window);
      });

      embeddedThumb.addStyleName("clickable");
      return embeddedThumb;
   }
}
