package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class LinkDescriptor
{
   private final String target;
   private final String description;

   public LinkDescriptor(String target, String description)
   {
      super();
      this.target = target;
      this.description = description;
   }

   public String getTarget()
   {
      return target;
   }

   public String getDescription()
   {
      return description;
   }

   @Override
   public String toString()
   {
      return LinkDescriptorBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return LinkDescriptorBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return LinkDescriptorBeanUtil.doEquals(this, obj);
   }
}
