package org.infinitenature.werbeo.cms.vaadin.plugin.species;

import org.infinitenature.service.v1.types.Taxon;

@FunctionalInterface
public interface SpeciesLinkCreator
{
   public LinkDescriptor createLink(Taxon taxon);
}
