package org.infinitenature.werbeo.cms.vaadin.plugin.breadcrump;

import com.vaadin.ui.UI;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.components.Breadcrumb;
import org.vaadin.viritin.label.RichText;

import com.vaadin.ui.Composite;
import com.vaadin.ui.CustomComponent;

public class BreadcrumpPluginViewImpl extends CustomComponent
      implements BreadcrumpPluginView
{
   private Breadcrumb breadcrumb;

   public BreadcrumpPluginViewImpl(String externalCmsUrl)
   {
      breadcrumb = new Breadcrumb(externalCmsUrl);
      setCompositionRoot(breadcrumb);
   }

   @Override
   public void setContent(String content)
   {
      breadcrumb.setHierarchy(content.split("#"));
   }
}
