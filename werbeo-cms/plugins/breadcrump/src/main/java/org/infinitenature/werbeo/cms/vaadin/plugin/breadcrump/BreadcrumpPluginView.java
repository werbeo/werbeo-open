package org.infinitenature.werbeo.cms.vaadin.plugin.breadcrump;

import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface BreadcrumpPluginView extends CMSVaadinView
{
   /**
    *
    * @param content
    *          
    */
   void setContent(String content);
}
