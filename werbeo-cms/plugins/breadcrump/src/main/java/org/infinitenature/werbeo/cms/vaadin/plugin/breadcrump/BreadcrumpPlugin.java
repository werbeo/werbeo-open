package org.infinitenature.werbeo.cms.vaadin.plugin.breadcrump;

import java.util.Map;

import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BreadcrumpPlugin implements VCMSComponent
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(BreadcrumpPlugin.class);
   public static final String PARAMETER_CONTENT = "content";

   private Map<String, String> parameter;
   private BreadcrumpPluginView view;

   public BreadcrumpPlugin(@Autowired InstanceConfig instanceConfig)
   {
      view = new BreadcrumpPluginViewImpl(instanceConfig.getExternalCmsUrl(
            Context.getCurrent().getPortal().getPortalId()));
   }

   @Override
   public BreadcrumpPluginView getVaadinComponent()
   {
      return view;
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      this.parameter = parameter;
      view.setContent(parameter.get(PARAMETER_CONTENT));
   }
}
