package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.werbeo.cms.formatter.VagueDateFormater;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.vaadin.alump.lazylayouts.LazyComponentRequestEvent;
import org.vaadin.alump.lazylayouts.LazyVerticalLayout;
import org.vaadin.viritin.label.MLabel;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.data.provider.DataChangeEvent;
import com.vaadin.data.provider.GridSortOrder;
import com.vaadin.data.provider.Query;
import com.vaadin.data.provider.QuerySortOrder;
import com.vaadin.event.SortEvent.SortListener;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Composite;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.components.grid.ColumnReorderListener;
import com.vaadin.ui.components.grid.ColumnVisibilityChangeListener;

public class OccurrenceTableViewMobileImpl extends Composite
      implements OccurrenceTableView
{
   private MVerticalLayout mainLayout = new MVerticalLayout().withMargin(false);
   private ConfigurableFilterDataProvider<Occurrence, Void, OccurrenceFilter> dataProvider;
   private TaxonComboBoxDataProvider taxonDataProvider;
   private int size;
   private int itemsLoaded = 0;
   private LazyVerticalLayout layout;

   public OccurrenceTableViewMobileImpl()
   {
      setCompositionRoot(mainLayout);

      Panel panel = new Panel();

      panel.setSizeFull();
      mainLayout.setSizeFull();
      mainLayout.add(panel);
      layout = new LazyVerticalLayout();
      layout.enableLazyLoading(this::loadNext);
      panel.setContent(layout);
      panel.setHeight(
            Math.round(UI.getCurrent().getPage().getBrowserWindowHeight() * .7)
                  + "px");

   }

   @Override
   public void setDataProvider(
         ConfigurableFilterDataProvider<Occurrence, Void, OccurrenceFilter> dataProvider)
   {
      this.dataProvider = dataProvider;
      this.dataProvider.addDataProviderListener(this::onDataChange);
      this.dataProvider.setFilter(OccurrenceFilter.NO_FILTER);
      this.dataProvider.size(new Query<>());
      loadNext(null);
   }

   private void loadNext(LazyComponentRequestEvent event)
   {
      if (itemsLoaded < size)
      {
         List<QuerySortOrder> sortOrders = new ArrayList<>();
         sortOrders.add(new QuerySortOrder(
               OccurrenceTableView.Columns.MOD_DATE.toString(),
               SortDirection.ASCENDING));
         Void filter = null;
         Query<Occurrence, Void> query = new Query<>(itemsLoaded, 4, sortOrders,
               null, filter);

         List<Occurrence> data = dataProvider.fetch(query)
               .collect(Collectors.toList());
         itemsLoaded = itemsLoaded + data.size();

         for (Occurrence occurrence : data)
         {
            layout.addComponent(new OccurrencePanel(occurrence));
         }
      }

      if (itemsLoaded >= size)
      {
         layout.disableLazyLoading();
      }
   }

   private void onDataChange(DataChangeEvent<Occurrence> e)
   {

      dataProvider.size(new Query<>());
      itemsLoaded = 0;
      layout.removeAllComponents();
      layout.enableLazyLoading(this::loadNext);
      loadNext(null);
   }

   @Override
   public void setTaxonDataProvider(TaxonComboBoxDataProvider provider)
   {
      this.taxonDataProvider = provider;

   }

   @Override
   public void setSurveyDataProvider(SurveyComboBoxDataProvider provider)
   {
      // NOOP
   }

   @Override
   public void setSize(int size)
   {
      this.size = size;
   }

   class OccurrencePanel extends Composite
   {
      public OccurrencePanel(Occurrence occurrence)
      {
         MVerticalLayout occurrenceLayout = new MVerticalLayout(
               new MLabel(VagueDateFormater
                     .format(occurrence.getSample().getDate())),
               new MLabel(occurrence.getTaxon().getName()));

         setCompositionRoot(occurrenceLayout);
      }
   }

   @Override
   public void showData(boolean b)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public OccurrenceFilter buildFilter()
   {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public void refreshGrid()
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void addSortListener(SortListener<GridSortOrder<Occurrence>> listener)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void addColumnVisibilityChangeListener(
         ColumnVisibilityChangeListener listener)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void setSortOrder(List<GridSortOrder<Occurrence>> sortOrders)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void showColumnsWithOrder(List<String> additionalColumns)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void addColumnReorderListener(ColumnReorderListener listener)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public Grid getGrid()
   {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public void setValidatorDataProvider(
         PersonComboBoxDataProvider personComboBoxDataProvider)
   {
      // NOOP
   }

}
