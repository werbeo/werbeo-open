package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

public interface ColumnEnum
{
   public String getColumnId();
}
