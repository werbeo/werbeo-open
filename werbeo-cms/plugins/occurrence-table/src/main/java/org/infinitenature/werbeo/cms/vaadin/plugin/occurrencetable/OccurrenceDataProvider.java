package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.service.v1.types.Herbarium;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.WerbeoDataProvider;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.OccurrenceTableView.Columns;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.provider.Query;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

public class OccurrenceDataProvider
      extends WerbeoDataProvider<Occurrence, OccurrenceFilter, OccurrenceField>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceDataProvider.class);

   private String forceOwnerFilter = null;
   private HerbariorumClient herbariorumClient;
   private MessagesResource messagesResource;


   public OccurrenceDataProvider(Werbeo werbeo,
         HerbariorumClient herbariorumClient, MessagesResource messagesResource)
   {
      super(werbeo);
      this.herbariorumClient = herbariorumClient;
      this.messagesResource = messagesResource;
   }

   @Override
   protected OccurrenceField getDefaultSortField()
   {
      return OccurrenceField.DETERMINER;
   }

   @Override
   protected List<Occurrence> getContent(Optional<OccurrenceFilter> filter,
         OccurrenceField sortField, SortOrder sortOrder, int offset, int limit)
   {
      try
      {
         OccurrenceFilter occurrenceFilter = filter
               .orElse(OccurrenceFilter.NO_FILTER);
         List<Occurrence> occurrences = werbeo.occurrences()
               .findPOST(offset, limit, sortField, sortOrder, OccurrenceFilterMapper
                     .mapFilterPOST(occurrenceFilter, forceOwnerFilter), false)
               .getContent();
         occurrences.forEach(occurrence ->
         {
            if (occurrence.getHerbarium() != null && StringUtils
                  .isNotBlank(occurrence.getHerbarium().getCode()))
            {
               occurrence.getHerbarium().setInstitution(herbariorumClient
                     .getByCode(occurrence.getHerbarium().getCode()));
            }
            if (occurrence.getHerbarium() == null)
            {
               occurrence.setHerbarium(new Herbarium());
            }
         });
         return occurrences;
      } catch (WebApplicationException e)
      {
         LOGGER.error("Error fetching size", e);
         handleException(e);
         return new ArrayList<>();
      }
   }

   private void handleException(WebApplicationException e)
   {
      try (Response response = e.getResponse();)
      {
         String entity = response.readEntity(String.class);
         entity = entity.replace("{\"cause\":\"", "");
         entity = entity.substring(0, entity.indexOf("\""));
         entity = entity.substring(entity.indexOf("- ") + 2).trim();
         LOGGER.error("Failure getting size from service, response was: {}",
               entity, e);

         Notification.show(
               messagesResource.getMessage(Context.getCurrent().getPortal(),
                     entity, Locale.GERMAN),
               Type.ERROR_MESSAGE);
      }

   }

   @Override
   protected OccurrenceField toSortField(
         com.vaadin.data.provider.SortOrder<String> vaadinSortOrder)
   {
      OccurrenceTableView.Columns column = Columns
            .valueOf(vaadinSortOrder.getSorted());
      switch (column)
      {
      case TAXON:
         return OccurrenceField.TAXON;
      case DETERMINER:
         return OccurrenceField.DETERMINER;
      case DATE:
         return OccurrenceField.DATE;
      case MOD_DATE:
         return OccurrenceField.MOD_DATE;
      case MEDIA:
         return OccurrenceField.MEDIA;
      case VALIDATION:
         return OccurrenceField.VALIDATION_STATUS;
      default:
         LOGGER.error("Trying to sort by the not supported field {}",
               vaadinSortOrder.getSorted());
         return OccurrenceField.DETERMINER;

      }
   }

   @Override
   protected int getSize(Query<Occurrence, OccurrenceFilter> query)
   {

      OccurrenceFilter occurrenceFilter = query.getFilter()
            .orElse(OccurrenceFilter.NO_FILTER);
      try
      {
         return (int) werbeo.occurrences().countPOST(OccurrenceFilterMapper
               .mapFilterPOST(occurrenceFilter, forceOwnerFilter)).getAmount();
      }
      catch(WebApplicationException e)
      {
         LOGGER.error("Error fetching size", e);
         handleException(e);
         return 0;
      }

   }

   public void setForceOwnerFilter(String forceOwnerFilter)
   {
      this.forceOwnerFilter = forceOwnerFilter;
   }

   public String getForceOwnerFilter()
   {
      return forceOwnerFilter;
   }
}
