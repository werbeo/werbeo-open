package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.enums.ExportPolicy;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.service.v1.types.meta.OccurrenceField;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.werbeo.cms.formatter.PersonFormatter;
import org.infinitenature.werbeo.cms.formatter.PositionFormatter;
import org.infinitenature.werbeo.cms.formatter.PublicationFormatter;
import org.infinitenature.werbeo.cms.formatter.VagueDateFormater;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Filters;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.AmountItemCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.AreaCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.AvailabilityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.BloomingSproutsCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.BooleanCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.LifeStageCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.MakropterCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.NumericAmountAccuracyCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.PersonCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.QuantityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.ReproductionCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SampleMethodCaptionGenrator;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusFukarekCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SexCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.VitalityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.components.GeoButton;
import org.infinitenature.werbeo.cms.vaadin.components.LimitIntegerField;
import org.infinitenature.werbeo.cms.vaadin.components.NullableTextField;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.PositionField;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBox;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonLinkButton;
import org.infinitenature.werbeo.cms.vaadin.components.TextFieldWithButton;
import org.infinitenature.werbeo.cms.vaadin.components.TooltipLabel;
import org.infinitenature.werbeo.cms.vaadin.components.VagueDateTimeField;
import org.infinitenature.werbeo.cms.vaadin.components.ValidationField;
import org.infinitenature.werbeo.cms.vaadin.components.ValidationStatusComboBox;
import org.infinitenature.werbeo.cms.vaadin.fieldconfig.FieldConfigUtils;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable.OccurrenceTableView.Columns;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.AbstractTableViewImpl;
import org.infinitenature.werbeo.common.commons.WerbeoStringUtils;
import org.markdown4j.Markdown4jProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.fields.MTextField.AutoComplete;

import com.vaadin.data.provider.GridSortOrder;
import com.vaadin.data.provider.GridSortOrderBuilder;
import com.vaadin.event.SortEvent.SortListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ExternalResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.ColumnReorderListener;
import com.vaadin.ui.components.grid.ColumnVisibilityChangeListener;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.NumberRenderer;

@SuppressWarnings("serial")
public class OccurrenceTableViewImpl
      extends AbstractTableViewImpl<Occurrence, Columns, OccurrenceFilter>
      implements OccurrenceTableView
{
   private static final String WHITE_BUTTON = "white-button";

   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceTableViewImpl.class);

   private final Map<SampleField, Boolean> sampleFieldConfigs;
   private final Map<OccurrenceField, Boolean> occurrenceFieldConfigs;
   private final Map<SurveyField, Boolean> surveyFieldConfigs;
   private Observer observer;

   private final TaxonComboBox taxonComboBox;
   private final ComboBox<SurveyBase> surveyComboBox = new ComboBox<>();
   private final GeoButton geoButton;
   private final LimitIntegerField blurFilterField = new LimitIntegerField();
   private final VagueDateTimeField dateTimeButton;
   private final TextFieldWithButton dateField;
   private final NullableTextField occExtrenalKeyField;
   private final ValidationStatusComboBox validationStatusFilterField;
   private final ComboBox<Person> validatorFilterComboBox;
   private final MButton csvExportButton;
   private MButton winArtExportButton = new MButton(VaadinIcons.DOWNLOAD,
         event -> export(ExportFormat.WINART)).withStyleName("white-button");
   private Column<Occurrence, Object> columnForInitialSorting;
   private List<Column<Occurrence, ?>> columsToHide = new ArrayList<>();
   private PortalConfiguration portalConfiguration;
   private String noExportMessage;

   @Override
   protected List<Columns> getSortableColumns()
   {
      return Arrays.asList(Columns.TAXON, Columns.DETERMINER, Columns.DATE,
            Columns.MOD_DATE, Columns.MEDIA, Columns.VALIDATION);
   }

   @Override
   public void refreshGrid()
   {
      grid.getDataProvider().refreshAll();
   }



   private void export(ExportFormat exportFormat)
   {
      observer.onExportClick(buildFilter(), exportFormat);
   }

   public OccurrenceTableViewImpl(Observer observer,
         MessagesResource messagesResource, Filters filters,
         PortalConfiguration portalConfiguration,
         Set<SampleFieldConfig> sampleFieldConfigs,
         Set<OccurrenceFieldConfig> occurrenceFieldConfigs,
         Set<SurveyFieldConfig> surveyFieldConfigs)
   {
      super(Columns.class, Occurrence.class, observer, messagesResource);
      this.observer = observer;
      this.portalConfiguration = portalConfiguration;
      this.sampleFieldConfigs = FieldConfigUtils
            .toSampleConfigMap(sampleFieldConfigs);
      this.occurrenceFieldConfigs = FieldConfigUtils
            .toOccurrenceConfigMap(occurrenceFieldConfigs);
      this.dateTimeButton = new VagueDateTimeField(messagesResource,
            null, null, this.occurrenceFieldConfigs, false);
      this.dateField = new TextFieldWithButton(
            "Datum der Aufnahme", "Klick auf Kalender ->",
            VaadinIcons.CALENDAR, dateTimeButton);

      this.surveyFieldConfigs = FieldConfigUtils
            .toSurveyFieldConfigMap(surveyFieldConfigs);

      if (isCsvExportForbidden())
      {
         this.csvExportButton = new MButton(VaadinIcons.DOWNLOAD,
               clickEvent -> displayCsvExportMessage())
                     .withStyleName(WHITE_BUTTON);
      }
      else
      {
         this.csvExportButton = new MButton(VaadinIcons.DOWNLOAD,
               clickEvent -> export(ExportFormat.CSV))
                     .withStyleName(WHITE_BUTTON);
      }

      this.taxonComboBox = new TaxonComboBox(messagesResource, true);
      this.geoButton = new GeoButton(messagesResource, portalConfiguration);
      this.geoButton.addStyleName(WHITE_BUTTON);

      this.surveyComboBox.setPopupWidth("auto");
      this.surveyComboBox.setWidth("250px");

      this.occExtrenalKeyField = (NullableTextField) new NullableTextField()
            .withAutoCapitalizeOff()
            .withAutoCorrectOff().setAutocomplete(AutoComplete.on);

      this.validationStatusFilterField = new ValidationStatusComboBox(
            getMessages(), "");
      this.validatorFilterComboBox = new ComboBox<>();
      this.validatorFilterComboBox
            .setItemCaptionGenerator(new PersonCaptionGenerator());

      if(filters.isSet())
      {
         setFilters(filters);
      }

      addStyleName("occurrence-table-view");
      addColumns();
      addFilter(new PositionField(messagesResource, geoButton));
      addExportButton(csvExportButton);
      addExportButton(winArtExportButton);
      
      grid.setSortOrder(
            new GridSortOrderBuilder<Occurrence>().thenDesc(columnForInitialSorting));
      grid.setId("occurrence-grid");
      grid.setFrozenColumnCount(1);

      layout.setExpandRatio(grid, 1);
      surveyComboBox.setItemCaptionGenerator(SurveyBase::getName);
      blurFilterField.setWidth("70px");
      blurFilterField.setMin(1);
      grid.setHeight("800px");
      dateTimeButton.addValueChangeListener(event -> dateField.getTextField()
            .setValue(VagueDate.format(dateTimeButton.getValue(), false)));
      dateField.addStyleName("with-border");
      dateField.getTextField().setEnabled(false);
      dateField.getTextField().addStyleName("width_85");
   }

   private boolean isCsvExportForbidden()
   {
      return observer.isCheckCSVExportRights()
            && portalConfiguration.getExportPolicies()
                  .contains(ExportPolicy.NO_CSV_EXPORT_FOR_NOT_APPROVED_USERS)
                  && !Context.getCurrent().isUserApproved();
   }


   private void displayCsvExportMessage()
   {

      VerticalLayout content = new VerticalLayout();
      content.addComponent(new Label(noExportMessage, ContentMode.HTML));

      ConfirmDialog confirmDialog = new ConfirmDialog();
      confirmDialog.setContent(content);
      confirmDialog.setStyleName("export-notification");
      confirmDialog.center();
      content.addComponent(new Button("Schließen", click->  {UI.getCurrent().removeWindow(confirmDialog);}));
      UI.getCurrent().addWindow(confirmDialog);

   }

   @Override
   public void showData(boolean show)
   {
      csvExportButton.setVisible(show);
   }

   @Override
   public void setTaxonDataProvider(TaxonComboBoxDataProvider provider)
   {
      taxonComboBox.setDataProvider(provider);
   }

   @Override
   public void setSurveyDataProvider(SurveyComboBoxDataProvider provider)
   {
      surveyComboBox.setDataProvider(provider);
   }

   @Override
   public void setValidatorDataProvider(
         PersonComboBoxDataProvider personComboBoxDataProvider)
   {
      validatorFilterComboBox.setDataProvider(personComboBoxDataProvider);
   }

   @Override
   public OccurrenceFilter buildFilter()
   {
      OccurrenceFilter filter = new OccurrenceFilter(dateTimeButton.getValue() != null ? dateTimeButton.getValue().getFrom():null,
            dateTimeButton.getValue() != null ? dateTimeButton.getValue().getTo(): null,
            taxonComboBox.getValue() != null
                  ? taxonComboBox.getValue().getEntityId()
                  : null,
            geoButton.getValue() != null ? geoButton.getValue() : null,
            blurFilterField.getValue() != null ? blurFilterField.getValue()
                  : null,
            surveyComboBox.getValue() != null
                  ? surveyComboBox.getValue().getEntityId()
                  : null,
            occExtrenalKeyField.getValue() != null
                  ? occExtrenalKeyField.getValue()
                  : null,
            validationStatusFilterField.getValue(),
            validatorFilterComboBox.getValue());
      observer.updateFilters( dateTimeButton.getValue(),
            taxonComboBox.getValue(), geoButton.getValue(),
            blurFilterField.getValue(), surveyComboBox.getValue(),
            occExtrenalKeyField.getValue(),
            validationStatusFilterField.getValue());
      return filter;
   }

   private void addColumns()
   {
      // first column frozen, this should be taxon
      addComponentColumn(
            occurrence -> new TaxonLinkButton(occurrence.getTaxon().getName(),
                  event -> observer
                        .onTaxonClick(occurrence.getTaxon().getName())){
            },
            Columns.TAXON, taxonComboBox, 220);

      if (occurrenceFieldConfigs.containsKey(OccurrenceField.MEDIA))
      {
         addComponentColumn(occurrence ->
         {
            Optional<Document> thumb = occurrence.getDocuments().stream()
                  .filter(doc -> doc.getType() == DocumentType.IMAGE_THUMB
                        || doc.getType() == DocumentType.AUDIO)
                  .findFirst();

            if (thumb.isPresent()
                  && thumb.get().getType() == DocumentType.IMAGE_THUMB)
            {
               VerticalLayout layout = new VerticalLayout();
               Image image = new Image(null,
                     new ExternalResource(WerbeoStringUtils.encodeFileAndParams(
                           thumb.get().getLink().getHref())));
               image.setHeight(60, Unit.PIXELS);
               image.setWidth(60, Unit.PIXELS);
               image.addStyleName("table-circle-button");
               image.setDescription(
                     createBigImagesTags(occurrence.getDocuments()),
                     ContentMode.HTML);
               layout.addComponent(image);
               layout.setMargin(false);
               return layout;
            } else if (thumb.isPresent()
                  && thumb.get().getType() == DocumentType.AUDIO)
            {
               Audio audio = new Audio();
               audio.setSource(new ExternalResource(WerbeoStringUtils
                     .encodeFileAndParams(thumb.get().getLink().getHref())));
               audio.addStyleName("table-circle-button");
               return audio;
            }
            else
            {
               return new Label();
            }
         }, Columns.MEDIA, 140);
      }

      if(sampleFieldConfigs.containsKey(SampleField.RECORDER)) // Finder
      {
         addColumn(occurrence -> PersonFormatter
               .format(occurrence.getSample().getRecorder()), Columns.RECORDER, 220);
      }

      columnForInitialSorting = addColumn(occurrence ->
      {
         try
         {
            return VagueDateFormater.format(occurrence.getSample().getDate());
         } catch (Exception e)
         {
            LOGGER.error("Failure formating date of occurrence {}", occurrence);
            return "";
         }
      }, Columns.DATE, dateField, 134);

      if (sampleFieldConfigs.containsKey(SampleField.LOCATION_MTB))
      {
         addColumn(
               occurrence -> occurrence.getSample().getLocality().getPosition().getMtb().getMtb(), Columns.MTB, 109);
      }

      if (sampleFieldConfigs.containsKey(SampleField.BLUR))
      {
         addColumn(occurrence -> occurrence.getSample().getLocality().getBlur(),
               Columns.PRECISION, new NumberRenderer("%d m"), blurFilterField,
               158);
      }

      // info button
      addComponentColumn(
            occurrence -> new MButton("",
                  event -> observer.onInfoClick(occurrence))
                  .withStyleName("table-circle-button").withIcon(VaadinIcons.FILE_TEXT),
            Columns.INFO, 104);

      // edit button
      addComponentColumn(occurrence -> new MButton("",
                  event -> observer.onEditClick(occurrence))
                  .withStyleName("table-circle-button")
                  .withIcon(VaadinIcons.EDIT).withEnabled(occurrence
                        .getAllowedOperations().contains(Operation.UPDATE)),
            Columns.EDIT, 150);

      
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.VALIDATION))
      {
         addComponentColumn(occurrence ->
         new ValidationField(getMessages(), occurrence.getValidation())
               .withReadOnly(!Context.getCurrent().isValdiator())
               .withValueChangelistener(event -> observer
                     .onValidationChange(occurrence, event.getValue())),
               Columns.VALIDATION, validationStatusFilterField, 158);

         addColumn(
               occurrence -> renderMarkdown(occurrence.getValidation() != null
               ? occurrence.getValidation().getComment()
                     : ""),
               Columns.VALIDATION_COMMENT, new HtmlRenderer(), 300);

         if (Context.getCurrent().isAccepted())
         {
            addFilter(validatorFilterComboBox);
         }
      }

      if (sampleFieldConfigs.containsKey(SampleField.LOCATION))
      {
         columsToHide.add(addComponentColumn(
               occurrence -> new TooltipLabel(
                     PositionFormatter.getSref(
                           occurrence.getSample().getLocality().getPosition()),
                     PositionFormatter.getSrefSystem(
                           occurrence.getSample().getLocality().getPosition())),
               Columns.SREF, 177));

         columsToHide.add(addComponentColumn(occurrence -> new Label(PositionFormatter.getSrefSystem(
                     occurrence.getSample().getLocality().getPosition())) ,
         Columns.SREF_SYSTEM, 77));
      }

      if(Context.getCurrent().isAdmin())
      {
         if(occurrenceFieldConfigs.containsKey(OccurrenceField.DETERMINER))
         {
            columsToHide.add(
                  addColumn(occurrence -> occurrence.getDeterminer() != null
                        ? occurrence.getDeterminer().getEmail()
                        : "", Columns.CONTACT, 220));
         }
      }

      // columns to hide
      columsToHide.add(
            addColumn(
                  occ -> new BooleanCaptionGenerator(getMessages())
                        .apply(occ.getSample().getSurvey().getWerbeoOriginal()),
                  Columns.WERBEO_ORIGINAL));

      if (occurrenceFieldConfigs
            .containsKey(OccurrenceField.DETERMINATION_COMMENT))
      {
         columsToHide.add(addColumn(occ -> occ.getDeterminationComment(), Columns.DETERMINATION_COMMENT, 220));
      }
      
      
      
      if (surveyFieldConfigs.containsKey(SurveyField.AVAILABILITY))
      {
         columsToHide.add(addColumn(occurrence -> new AvailabilityCaptionGenerator(getMessages())
               .apply(occurrence.getSample().getSurvey().getAvailability()),
               Columns.AVAILABILITY));
      }
      // always available
      columsToHide.add(addColumn(occurrence -> occurrence.getSample().getSurvey().getName(),
            Columns.SURVEY, surveyComboBox, 150));
      if(occurrenceFieldConfigs.containsKey(OccurrenceField.DETERMINER)) // Bestimmer
      {
         columsToHide.add(addColumn(occurrence -> PersonFormatter.format(occurrence.getDeterminer()),
               Columns.DETERMINER, 220));
      }
        columsToHide.add(addColumn(
              occurrence -> occurrence.getModificationDate()
              .format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")),
        Columns.MOD_DATE, 230));
      if (occurrenceFieldConfigs
            .containsKey(OccurrenceField.ID))
      {
         columsToHide.add(addColumn(Occurrence::getEntityId, Columns.UUID, 280));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.EXTERNAL_KEY))
      {
         columsToHide.add(addColumn(Occurrence::getExternalKey,
               Columns.EXTERNAL_KEY, occExtrenalKeyField, 220));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.PUBLICATION))
      {
         columsToHide.add(addColumn(occurrence -> PublicationFormatter.formatShort(occurrence.getReference()), Columns.PUBLICATION, 220));
      }
      if (sampleFieldConfigs.containsKey(SampleField.LOCATION_COMMENT))
      {
         columsToHide.add(addColumn(
               occurrence -> occurrence.getSample().getLocality().getLocationComment(), Columns.LOCATION_COMMENT, 280));
      }
      if (sampleFieldConfigs.containsKey(SampleField.LOCALITY))
      {
         columsToHide.add(addColumn(
               occurrence -> occurrence.getSample().getLocality().getLocality(),
               Columns.LOCATION_NAME, 220));
      }

      if (sampleFieldConfigs.containsKey(SampleField.SAMPLE_METHOD))
      {
         columsToHide.add(addColumn(
               occurrence -> new SampleMethodCaptionGenrator(getMessages())
                     .apply(occurrence.getSample().getSampleMethod()),
               Columns.SAMPLE_METHOD, 240));
      }
      if(occurrenceFieldConfigs.containsKey(OccurrenceField.HERBARIUM))
      {
         columsToHide.add(addComponentColumn(occurrence -> {
            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.setMargin(false);
            verticalLayout.setSpacing(false);
            if (occurrence.getHerbarium() != null
                  && occurrence.getHerbarium().getInstitution() != null
                  && StringUtils.isNotEmpty(occurrence.getHerbarium()
                        .getInstitution().getOrganization()))
            {
               verticalLayout.addComponent(new Label(     occurrence.getHerbarium().getInstitution().getOrganization()));
            }
            if (occurrence.getHerbarium() != null && StringUtils
                  .isNotEmpty(occurrence.getHerbarium().getHerbary()))
            {
               verticalLayout.addComponent(
                     new Label(occurrence.getHerbarium().getHerbary()));
            }
            return verticalLayout;
         }, Columns.HERBARY, 280));
      }

      if (occurrenceFieldConfigs.containsKey(OccurrenceField.HABITAT))
      {
         columsToHide.add(addColumn(occurrence -> occurrence.getHabitat(),
               Columns.HABITAT, 300));
      }
      if(occurrenceFieldConfigs.containsKey(OccurrenceField.OBSERVERS))
      {
         columsToHide.add(addColumn(occurrence -> occurrence.getObservers(),
               Columns.OBSERVERS, 280));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.AMOUNT))
      {
         columsToHide
               .add(addColumn(
                     occurrence -> new AmountItemCaptionGenerator(getMessages())
                           .apply(occurrence.getAmount()),
                     Columns.AMOUNT, 250));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.NUMERIC_AMOUNT)
            && occurrenceFieldConfigs
                  .containsKey(OccurrenceField.NUMERIC_AMOUNT_ACCURACY))
      {
         NumericAmountAccuracyCaptionGenerator captionGenerator = new NumericAmountAccuracyCaptionGenerator(
               getMessages());
         columsToHide.add(addColumn(
               occurrence -> captionGenerator
                     .getCaption(occurrence.getNumericAmountAccuracy()) + " "
                     + occurrence.getNumericAmount(),
               Columns.AMOUNT_NUMERIC, 200));
      } else if (occurrenceFieldConfigs
            .containsKey(OccurrenceField.NUMERIC_AMOUNT))
      {
         columsToHide.add(addColumn(occurrence -> occurrence.getNumericAmount(),
               Columns.AMOUNT_NUMERIC, 200));
      } else if (occurrenceFieldConfigs.containsKey(OccurrenceField.QUANTITY))
      {
         columsToHide
               .add(addColumn(
                     occurrence -> new QuantityCaptionGenerator(getMessages())
                           .apply(occurrence.getQuantity()),
                     Columns.QUANTITY, 200));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.SETTLEMENT_STATUS))
      {
         columsToHide.add(addColumn(
               occurrence -> new SettlementStatusCaptionGenerator(getMessages())
                     .apply(occurrence.getSettlementStatus()),
               Columns.SETTLEMENT_STATUS, 300));
      }
      if (occurrenceFieldConfigs
            .containsKey(OccurrenceField.SETTLEMENT_STATUS_FUKAREK))
      {
         columsToHide.add(addColumn(
               occurrence -> new SettlementStatusFukarekCaptionGenerator(
                     getMessages())
                           .apply(occurrence.getSettlementStatusFukarek()),
               Columns.SETTLEMENT_STATUS_FUKAREK, 350));
      }
      if(occurrenceFieldConfigs.containsKey(OccurrenceField.VITALITY))
      {
         columsToHide
               .add(addColumn(
                     occurrence -> new VitalityCaptionGenerator(getMessages())
                           .apply(occurrence.getVitality()),
                     Columns.VITALITY, 220));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.AREA))
      {
         columsToHide.add(addColumn(
               occurrence -> new AreaCaptionGenerator(getMessages())
                     .apply(occurrence.getCoveredArea()),
               Columns.AREA, 220));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.BLOOMING_SPROUTS))
      {
         columsToHide.add(addColumn(
               occurrence -> new BloomingSproutsCaptionGenerator(getMessages())
                     .apply(occurrence.getBloomingSprouts()),
               Columns.BLOOMING_SPROUTS, 200));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.SEX))
      {
         columsToHide.add(
               addColumn(occurrence -> new SexCaptionGenerator(getMessages())
                     .apply(occurrence.getSex()), Columns.SEX, 220));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.REPRODUCTION))
      {
         columsToHide.add(addColumn(
               occurrence -> new ReproductionCaptionGenerator(getMessages())
                     .apply(occurrence.getReproduction()),
               Columns.REPRODUCTION, 200));
      }
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.TIME_OF_DAY))
      {
         columsToHide.add(addColumn(occurrence -> occurrence.getTimeOfDay(),
               Columns.TIME_OF_DAY));
      }
      
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.MAKROPTER))
      {
         columsToHide.add(
               addColumn(occ -> new MakropterCaptionGenerator(getMessages())
                     .apply(occ.getMakropter()), Columns.MAKROPTER, 220));
      }

      if (occurrenceFieldConfigs.containsKey(OccurrenceField.LIFE_STAGE))
      {
         columsToHide
               .add(addColumn(
                     occ -> new LifeStageCaptionGenerator(getMessages())
                           .apply(occ.getLifeStage()),
                     Columns.LIFE_STAGE, 220));
      }
      
      columsToHide.add(addColumn(
            occurrence -> occurrence.getSample().getSurvey().getDescription(),
            Columns.SURVEY_DESCRIPTION, 250));
      hide(columsToHide);
   }

   private String renderMarkdown(String string)
   {
      if (StringUtils.isNotBlank(string))
      {
         try
         {
            return new Markdown4jProcessor().process(string);
         } catch (IOException e)
         {
            return "";
         }
      }
      return "";
   }

   private String createBigImagesTags(List<Document> documents)
   {
      StringBuilder builder = new StringBuilder();
      for(Document document : documents)
      {
         if (document.getType().equals(DocumentType.IMAGE_THUMB))
         {
            builder
                  .append("<img height=\"130px\" src=\""
                        + WerbeoStringUtils
                              .encodeFileAndParams(document.getLink().getHref())
                        + "\"></img>&nbsp;");
         }
      }
      return builder.toString();
   }

   private void hide(List<Column<Occurrence,?>> columsToHide)
   {
      columsToHide.forEach(column -> {
         column.setHidable(true);
         column.setHidden(true);
      });
   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();

      geoButton.applyLocalizations();
      geoButton.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "filter.geo", getLocale()));
      validatorFilterComboBox.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "filter.validator", getLocale()));
      if (isCsvExportForbidden())
      {
         csvExportButton.setCaption(
               getMessages().getMessage(Context.getCurrent().getPortal(),
                     "export.csv.message", getLocale()));
      }
      else
      {
         csvExportButton.setCaption(getMessages().getMessage(
               Context.getCurrent().getPortal(), "export.csv", getLocale()));
      }
      
      winArtExportButton.setCaption(getMessages().getMessage(
            Context.getCurrent().getPortal(), "export.winart", getLocale()));

      this.noExportMessage = getMessages().getEntity(
            Context.getCurrent().getPortal(),
            this.getClass().getSimpleName() + ".NO_EXPORT_MESSAGE",
            getLocale());
   }

   private void setFilters(Filters filters)
   {
      if(filters.getFilterBlur() != null)
      {
         blurFilterField.setValue(filters.getFilterBlur());
      }

      if(filters.getFilterDate() != null )
      {
         dateTimeButton.setValue(filters.getFilterDate());
      }

      if(filters.getFilterPosition() != null)
      {
         geoButton.setValue(filters.getFilterPosition());
      }
      if(filters.getFilterTaxon() != null)
      {
         taxonComboBox.setValue(filters.getFilterTaxon());
      }
      if(filters.getFilterSurvey() != null)
      {
         surveyComboBox.setValue(filters.getFilterSurvey());
      }
      if (filters.getOccExternalKey() != null)
      {
         occExtrenalKeyField.setValue(filters.getOccExternalKey());
      }
      if (filters.getValidationStatus() != null)
      {
         validationStatusFilterField.setValue(filters.getValidationStatus());
      }
      if (filters.getValidator() != null)
      {
         validatorFilterComboBox.setValue(filters.getValidator());
      }
   }

   @Override
   public void addSortListener(SortListener<GridSortOrder<Occurrence>> listener)
   {
      grid.addSortListener(listener);
   }

   @Override
   public void addColumnVisibilityChangeListener(ColumnVisibilityChangeListener listener)
   {
      grid.addColumnVisibilityChangeListener(listener);
   }

   @Override
   public void addColumnReorderListener(ColumnReorderListener listener)
   {
      grid.addColumnReorderListener(listener);
   }

   @Override
   public void setSortOrder(List<GridSortOrder<Occurrence>> sortOrders)
   {
      grid.setSortOrder(sortOrders);
   }

   @Override
   public void showColumnsWithOrder(List<String> columnsToShow)
   {
      for(Column column : grid.getColumns())
      {
         if(columnsToShow.contains(column.getId()))
         {
            column.setHidable(true);
            column.setHidden(false);
         }
         else
         {
            column.setHidable(true);
            column.setHidden(true);
         }
      }
      grid.setColumnOrder(columnsToShow.toArray(new String[columnsToShow.size()]));
   }

   @Override
   public Grid getGrid()
   {
      // TODO Auto-generated method stub
      return grid;
   }
}
