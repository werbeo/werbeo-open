package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.time.LocalDate;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Position;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceFilter
{
   public static final OccurrenceFilter NO_FILTER = new OccurrenceFilter();
   private final LocalDate from;
   private final LocalDate to;
   private final Position position;
   private final Integer maxBlur;
   private final Integer surveyId;
   private final String occExternalKey;
   private final ValidationStatus validationStatus;

   public Integer getTaxonId()
   {
      return taxonId;
   }

   private final Integer taxonId;
   private Person validator;

   private OccurrenceFilter()
   {
      this(null, null, null, null, null, null, null, null, null);
   }

   public OccurrenceFilter(LocalDate from, LocalDate to, Integer taxonId,
         Position position, Integer maxBlur, Integer surveyId,
         String occExternalKey, ValidationStatus validationStatus,
         Person validator)
   {
      this.from = from;
      this.to = to;
      this.taxonId = taxonId;
      this.position = position;
      this.maxBlur = maxBlur;
      this.surveyId = surveyId;
      this.occExternalKey = occExternalKey;
      this.validationStatus = validationStatus;
      this.validator = validator;
   }

   public Position getPosition()
   {
      return position;
   }

   public LocalDate getFrom()
   {
      return from;
   }

   public LocalDate getTo()
   {
      return to;
   }

   public Integer getMaxBlur()
   {
      return maxBlur;
   }

   public Integer getSurveyId()
   {
      return surveyId;
   }

   public String getOccExternalKey()
   {
      return occExternalKey;
   }

   public ValidationStatus getValidationStatus()
   {
      return validationStatus;
   }

   public Person getValidator()
   {
      return validator;
   }

   public void setValidator(Person validator)
   {
      this.validator = validator;
   }
   @Override
   public int hashCode()
   {
      return OccurrenceFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceFilterBeanUtil.doEquals(this, obj);
   }

   @Override
   public String toString()
   {
      return OccurrenceFilterBeanUtil.doToString(this);
   }
}
