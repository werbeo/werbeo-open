package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.util.List;

import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableObserver;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableView;

import com.vaadin.data.provider.GridSortOrder;
import com.vaadin.event.SortEvent.SortListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.ColumnReorderListener;
import com.vaadin.ui.components.grid.ColumnVisibilityChangeListener;

public interface OccurrenceTableView
      extends TableView<Occurrence, OccurrenceFilter>
{
   public enum Columns
   {
      TAXON,
      DATE,
      MTB,
      PRECISION,
      DETERMINER,
      DETERMINATION_COMMENT,
      CONTACT,
      MOD_DATE,
      MAKROPTER,
      UUID,
      EXTERNAL_KEY,
      COMMENT,
      PUBLICATION,
      SREF,
      SREF_SYSTEM,
      INFO,
      EDIT,
      SURVEY,
      LIFE_STAGE,
      LOCATION_COMMENT,
      LOCATION_NAME,
      SURVEY_DESCRIPTION,
      SURVEY_OWNER,
      RECORDER,
      SAMPLE_METHOD,
      HERBARY,
      TAXON_AUTHORITY,
      TAXON_GROUP,
      HABITAT,
      OBSERVERS,
      AMOUNT,
      AMOUNT_NUMERIC,
      QUANTITY,
      SETTLEMENT_STATUS,
      SETTLEMENT_STATUS_FUKAREK,
      VITALITY,
      AREA,
      BLOOMING_SPROUTS,
      SEX,
      REPRODUCTION,
      MEDIA, AVAILABILITY,
      TIME_OF_DAY, VALIDATION, WERBEO_ORIGINAL, VALIDATION_COMMENT;
   }

   void setTaxonDataProvider(TaxonComboBoxDataProvider provider);

   void setSurveyDataProvider(SurveyComboBoxDataProvider provider);

   public static interface Observer extends TableObserver<OccurrenceFilter>
   {
      public void onEditClick(Occurrence occurrence);
      public void onTaxonClick(String taxon);
      public void onExportClick(OccurrenceFilter filter, ExportFormat format);
      public void onInfoClick(Occurrence occurrence);
      public void updateFilters(VagueDate date, TaxonBase value,
            Position value2, Integer value3, SurveyBase value4,
            String occExternalKey, ValidationStatus validationStatus);
      public boolean isCheckCSVExportRights();

      public void onValidationChange(Occurrence occurrence, Validation value);
   }

   void setSize(int size);

   void showData(boolean b);

   OccurrenceFilter buildFilter();

   void refreshGrid();

   void addSortListener(SortListener<GridSortOrder<Occurrence>> listener);

   void addColumnVisibilityChangeListener(
         ColumnVisibilityChangeListener listener);

   void setSortOrder(List<GridSortOrder<Occurrence>> sortOrders);

   void showColumnsWithOrder(List<String> additionalColumns);

   void addColumnReorderListener(ColumnReorderListener listener);

   Grid getGrid();

   void setValidatorDataProvider(
         PersonComboBoxDataProvider personComboBoxDataProvider);

}
