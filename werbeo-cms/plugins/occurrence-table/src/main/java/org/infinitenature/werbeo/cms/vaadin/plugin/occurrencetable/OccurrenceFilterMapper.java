package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.werbeo.cms.vaadin.Context;

public class OccurrenceFilterMapper
{
   public static int getPortalId()
   {
      return Context.getCurrent().getApp().getPortalId();
   }

   public static org.infinitenature.service.v1.resources.OccurrenceFilter mapFilter(
         OccurrenceFilter occurrenceFilter, String owner)

   {
      return new org.infinitenature.service.v1.resources.OccurrenceFilter(
            getPortalId(), occurrenceFilter.getFrom(), occurrenceFilter.getTo(),
            occurrenceFilter.getTaxonId(),
            (occurrenceFilter.getPosition() != null && !PositionType.MTB.equals(occurrenceFilter.getPosition().getType()))
                  ? occurrenceFilter.getPosition().getWkt()
                  : null,
            getMtb(occurrenceFilter), occurrenceFilter.getMaxBlur(), true,
            owner, occurrenceFilter.getSurveyId(), null, null,
            occurrenceFilter.getValidationStatus(),
            occurrenceFilter.getValidator() != null
                  ? occurrenceFilter.getValidator().getEmail()
                  : null,
            occurrenceFilter.getOccExternalKey());
   }
   
   public static org.infinitenature.service.v1.resources.OccurrenceFilterPOST mapFilterPOST(
         OccurrenceFilter occurrenceFilter, String owner)

   {
      return new org.infinitenature.service.v1.resources.OccurrenceFilterPOST(
            getPortalId(), occurrenceFilter.getFrom(), occurrenceFilter.getTo(),
            occurrenceFilter.getTaxonId(),
            (occurrenceFilter.getPosition() != null && !PositionType.MTB.equals(occurrenceFilter.getPosition().getType()))
                  ? occurrenceFilter.getPosition().getWkt()
                  : null,
            getMtb(occurrenceFilter), occurrenceFilter.getMaxBlur(), true,
            owner, occurrenceFilter.getSurveyId(), null, null,
            occurrenceFilter.getValidationStatus(),
            occurrenceFilter.getValidator() != null
                  ? occurrenceFilter.getValidator().getEmail()
                  : null,
            occurrenceFilter.getOccExternalKey());
   }

   static String getMtb(OccurrenceFilter occurrenceFilter)
   {
      Position position = occurrenceFilter.getPosition();

      return getMtb(position);
   }

   static String getMtb(Position position)
   {
      if (position != null)
      {
         return position.getMtb() != null ? position.getMtb().getMtb() : null;
      } else
      {
         return null;
      }
   }

}
