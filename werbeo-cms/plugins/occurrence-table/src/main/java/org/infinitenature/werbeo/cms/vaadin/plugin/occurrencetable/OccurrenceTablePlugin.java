package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.service.v1.types.response.OccurrenceResponse;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.repository.ComponentFactory;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.OccurrenceEdit;
import org.infinitenature.werbeo.cms.navigation.OccurrenceExport;
import org.infinitenature.werbeo.cms.navigation.TaxonDetail;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Filters;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.WindowUtils;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrence.OccurrenceDetailPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.data.provider.GridSortOrder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid.Column;

@SpringComponent
@Primary // needed because MyOccurrenceTable inherits from this class
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OccurrenceTablePlugin
      implements VCMSComponent, OccurrenceTableView.Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceTablePlugin.class);
   @Autowired
   protected Werbeo werbeo;
   @Autowired
   private NavigationResolver navigationResolver;
   @Autowired
   private ComponentFactory componentFactory;
   @Autowired
   private InstanceConfig instanceConfig;
   @Autowired
   private HerbariorumClient herbariorumClient;

   protected OccurrenceTableView view;
   protected ConfigurableFilterDataProvider<Occurrence, Void, OccurrenceFilter> occurrenceDataProvider;
   protected MessagesResource messagesResource;
   private String foceOwnerFilter = null;

   public OccurrenceTablePlugin(
         @Autowired
               MessagesResource messagesResource)
   {
      this.messagesResource = messagesResource;
   }

   protected void initView()
   {
      Context context = Context.getCurrent();
      int portalId = context.getPortal().getPortalId();
      view = new OccurrenceTableViewImpl(this, this.messagesResource,
            context.getFilters(), werbeo.portals()
            .getPortalConfiguration(context.getPortal().getPortalId())
            .getPortalConfiguration(),
            werbeo.samples().getFieldConfig(portalId).getFieldConfigs(),
            werbeo.occurrences().getFieldConfig(portalId).getFieldConfigs(),
            werbeo.surveys().getFieldConfig(portalId).getFieldConfigs());
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      initView();
      LOGGER.debug("Init with parameter: {}", parameter);
      OccurrenceDataProvider dataProvider = createDataProvider();

      dataProvider.addSizeChangeListener(size -> view.setSize(size));
      occurrenceDataProvider = dataProvider.withConfigurableFilter();
      view.setDataProvider(occurrenceDataProvider);
      view.setTaxonDataProvider(new TaxonComboBoxDataProvider(werbeo, instanceConfig, false, false, true));
      view.setSurveyDataProvider(new SurveyComboBoxDataProvider(werbeo));
      view.setValidatorDataProvider(
            new PersonComboBoxDataProvider(werbeo, true));
      view.getVaadinComponent().setSizeFull();

      if (Context.getCurrent().getFilters().isSet())
      {
         onFilterChange(view.buildFilter());
      }

      setupSortingAndColumns(
            Context.getCurrent().getTablesConfig().getOccurrenceTableColumns(),
            Context.getCurrent().getTablesConfig()
                  .getOccurrenceTableSortOrders());
   }

   public void setupSortingAndColumns(List<String> occurrenceTableColumns,
         List<GridSortOrder<Occurrence>> sortOrders)
   {

      if(occurrenceTableColumns.isEmpty())
      {
         // initially set column Ids in context
         occurrenceTableColumns.addAll(extractVisibleColumnIds());
      }

      view.addColumnVisibilityChangeListener(event ->{
         if(event.getColumn().isHidden())
         {
            occurrenceTableColumns.remove(event.getColumn().getId());
         }
         else if(!occurrenceTableColumns.contains(event.getColumn().getId()))
         {
            occurrenceTableColumns.add(event.getColumn().getId());
         }
      });

      view.addColumnReorderListener(event ->
      {
         List<String> columnIds = extractVisibleColumnIds();
         occurrenceTableColumns.clear();
         occurrenceTableColumns
               .addAll(columnIds);
      });


      if (!occurrenceTableColumns.isEmpty())
      {
         view.showColumnsWithOrder(occurrenceTableColumns);
      }

      //  sorting, depending on last sorting in session
      if (!sortOrders.isEmpty())
      {
         view.setSortOrder(sortOrders);
      }

      view.addSortListener(event ->{
         sortOrders.clear();
         sortOrders.addAll(event.getSortOrder());
      });

   }

   private List<String> extractVisibleColumnIds()
   {
      List<String> columnIds = new ArrayList<>();
      for (Object column : view.getGrid().getColumns())
      {
         if (!((Column) column).isHidden())
         {
            columnIds.add(((Column) column).getId());
         }
      }
      return columnIds;
   }

   protected OccurrenceDataProvider createDataProvider()
   {
      OccurrenceDataProvider dataProvider = new OccurrenceDataProvider(werbeo,
            herbariorumClient, messagesResource);
      return dataProvider;
   }

   public void setWerbeo(Werbeo werbeo)
   {
      this.werbeo = werbeo;
   }

   @Override
   public void onFilterChange(OccurrenceFilter occurrenceFilter)
   {
      occurrenceDataProvider.setFilter(occurrenceFilter);
   }

   @Override
   public void onEditClick(Occurrence occurrence)
   {
      navigationResolver
            .navigateTo(new OccurrenceEdit(occurrence.getEntityId()));
   }

   @Override
   public void onExportClick(OccurrenceFilter filter, ExportFormat exportFormat)
   {
      werbeo.occurrenceExports().createLargeWKT(
            OccurrenceFilterMapper.mapFilterPOST(filter, getFoceOwnerFilter()),
            exportFormat);
      navigationResolver.navigateTo(new OccurrenceExport());
   }

   @Override
   public void onValidationChange(Occurrence occurrence, Validation value)
   {
      OccurrenceResponse response = werbeo.occurrences().validate(
            Context.getCurrent().getPortal().getPortalId(),
            occurrence.getEntityId(), value);
      LOGGER.info("validation status for {} is now {}",
            response.getOccurrence().getEntityId(),
            response.getOccurrence().getValidation());
   }

   @Override
   public void onTaxonClick(String taxon)
   {
      navigationResolver.navigateTo(new TaxonDetail(taxon));
   }

   protected void setFoceOwnerFilter(String foceOwnerFilter)
   {
      this.foceOwnerFilter = foceOwnerFilter;
   }

   protected String getFoceOwnerFilter()
   {
      return foceOwnerFilter;
   }

   public void setNavigationResolver(NavigationResolver navigationResolver)
   {
      this.navigationResolver = navigationResolver;
   }

   @Override
   public void onInfoClick(Occurrence occurrence)
   {
      OccurrenceDetailPlugin detailPlugin = componentFactory
            .getComponent(OccurrenceDetailPlugin.class);
      WindowUtils.showPluginInWindow(detailPlugin,
            occurrence.getEntityId().toString(),
            false);
   }

   @Override
   public void updateFilters(VagueDate date, TaxonBase taxon,
         Position position, Integer blur, SurveyBase survey,
         String occExternalKey, ValidationStatus validationStatus)
   {
      Filters filters = Context.getCurrent().getFilters();
      filters.setFilterBlur(blur);
      filters.setFilterPosition(position);
      filters.setFilterSurvey(survey);
      filters.setFilterTaxon(taxon);
      filters.setFilterDate(date);
      filters.setOccExternalKey(occExternalKey);
      filters.setValidationStatus(validationStatus);
   }

   public void setComponentFactory(ComponentFactory componentFactory)
   {
      this.componentFactory = componentFactory;
   }

   public void setInstanceConfig(InstanceConfig instanceConfig)
   {
      this.instanceConfig = instanceConfig;
   }

   @Override
   public boolean isCheckCSVExportRights()
   {
      return true;
   }
}
