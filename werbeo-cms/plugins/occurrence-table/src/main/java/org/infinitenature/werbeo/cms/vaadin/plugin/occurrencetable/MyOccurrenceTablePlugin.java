package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.util.Map;
import java.util.Optional;

import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Filters;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.ViewMode;
import org.infinitenature.werbeo.cms.vaadin.components.PersonComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.SurveyComboBoxDataProvider;
import org.infinitenature.werbeo.cms.vaadin.components.TaxonComboBoxDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MyOccurrenceTablePlugin extends OccurrenceTablePlugin
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(MyOccurrenceTablePlugin.class);

   @Autowired
   private InstanceConfig instanceConfig;

   public MyOccurrenceTablePlugin(MessagesResource messagesResource)
   {
      super(messagesResource);
   }

   @Override
   protected OccurrenceDataProvider createDataProvider()
   {
      OccurrenceDataProvider dataProvider = super.createDataProvider();
      Optional<String> email = Context.getCurrent().getAuthentication()
            .getEmail();
      if (email.isPresent())
      {
         dataProvider.setForceOwnerFilter(email.get());
         setFoceOwnerFilter(email.get());
      }
      else
      {
         LOGGER.warn("MyOccurrence view without a user");
      }
      return dataProvider;
   }

   @Override
   protected void initView()
   {
      Context context = Context.getCurrent();
      int portalId = context.getPortal().getPortalId();
      if (context.getViewMode() == ViewMode.MOBILE)
      {
         view = new OccurrenceTableViewMobileImpl();
      } else
      {
         Filters filters = context.getMyFilters();

         view = new OccurrenceTableViewImpl(this, this.messagesResource,
               filters, werbeo.portals()
               .getPortalConfiguration(context.getPortal().getPortalId())
               .getPortalConfiguration(),
               werbeo.samples().getFieldConfig(portalId).getFieldConfigs(),
               werbeo.occurrences().getFieldConfig(portalId).getFieldConfigs(),
               werbeo.surveys().getFieldConfig(portalId).getFieldConfigs());
      }
      view.refreshGrid();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      initView();

      LOGGER.debug("Init with parameter: {}", parameter);
      OccurrenceDataProvider dataProvider = createDataProvider();

      dataProvider.addSizeChangeListener(size -> view.setSize(size));
      occurrenceDataProvider = dataProvider.withConfigurableFilter();
      view.setDataProvider(occurrenceDataProvider);
      view.setTaxonDataProvider(new TaxonComboBoxDataProvider(werbeo, instanceConfig, false, false, false));
      view.setSurveyDataProvider(new SurveyComboBoxDataProvider(werbeo));
      view.setValidatorDataProvider(
            new PersonComboBoxDataProvider(werbeo, true));
      view.getVaadinComponent().setSizeFull();


      setupSortingAndColumns(
            Context.getCurrent().getTablesConfig().getMyOccurrenceTableColumns(),
            Context.getCurrent().getTablesConfig()
                  .getMyOccurrenceTableSortOrders());

      if (Context.getCurrent().getMyFilters().isSet())
      {
         onFilterChange(view.buildFilter());
      }
   }

   @Override
   public void updateFilters(VagueDate date, TaxonBase taxon,
         Position position, Integer blur, SurveyBase survey,
         String occExternalKey, ValidationStatus validationStatus)
   {
      Filters filters = Context.getCurrent().getMyFilters();
      filters.setFilterBlur(blur);
      filters.setFilterPosition(position);
      filters.setFilterSurvey(survey);
      filters.setFilterTaxon(taxon);
      filters.setFilterDate(date);
      filters.setOccExternalKey(occExternalKey);
      filters.setValidationStatus(validationStatus);
   }
   
   
   @Override
   public boolean isCheckCSVExportRights()
   {
      return false;
   }
}
