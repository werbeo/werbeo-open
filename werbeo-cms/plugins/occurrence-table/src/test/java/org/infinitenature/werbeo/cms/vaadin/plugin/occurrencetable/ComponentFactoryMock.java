package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import org.infinitenature.herbariorum.client.HerbariorumGermanClient;
import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSLayout;
import org.infinitenature.vct.repository.ComponentFactory;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.plugin.occurrence.OccurrenceDetailPlugin;

public class ComponentFactoryMock implements ComponentFactory
{
   private final Werbeo werbeo;

   ComponentFactoryMock(Werbeo werbeo)
   {
      this.werbeo = werbeo;
   }

   @Override
   public VCMSLayout getLayout(String layoutImplementationClass)
   {
      return null;
   }

   @Override
   public <T extends VCMSComponent> T getComponent(Class<T> clazz)
   {
      InstanceConfig instanceConfig = new InstanceConfig();

      return (T) new OccurrenceDetailPlugin(new MessagesResourceMock(),
            instanceConfig, werbeo, new HerbariorumGermanClient());
   }

   @Override
   public VCMSAppTemplate getAppTemplate(
         String appTemplateImplementationClass)
   {
      return null;
   }

   @Override
   public VCMSComponent get(String componentImplementationClass)
   {
      return null;
   }
}