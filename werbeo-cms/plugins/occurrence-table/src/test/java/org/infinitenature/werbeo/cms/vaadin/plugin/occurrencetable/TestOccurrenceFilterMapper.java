package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.vaadin.server.VaadinSession;

class TestOccurrenceFilterMapper
{
   @BeforeEach
   void setUp()
   {
      Context context = new Context();
      context.setApp(new Portal("test", 2, 2));
      VaadinSession session = mock(VaadinSession.class);
      when(session.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(context);
      VaadinSession.setCurrent(session);

   }

   @Test
   @DisplayName("empty filter")
   void test001()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, null, null, null, null);

      org.infinitenature.service.v1.resources.OccurrenceFilter mappedFilter = OccurrenceFilterMapper
            .mapFilter(filter, null);
      assertThat(mappedFilter.getMtb(), is(nullValue()));
   }

   @Test
   @DisplayName("filter by validation status")
   void test002()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, null, null, ValidationStatus.VALID, null);

      org.infinitenature.service.v1.resources.OccurrenceFilter mappedFilter = OccurrenceFilterMapper
            .mapFilter(filter, null);
      assertThat(mappedFilter.getValidationStatus(),
            is(ValidationStatus.VALID));
   }

   @Test
   @DisplayName("filter by validator")
   void test003()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, null, null, null,
            new Person(1, "Test", "Validator", "test@validator.de"));

      org.infinitenature.service.v1.resources.OccurrenceFilter mappedFilter = OccurrenceFilterMapper
            .mapFilter(filter, null);
      assertThat(mappedFilter.getValidator(), is("test@validator.de"));
   }

   @Test
   @DisplayName("filter by external key")
   void test004()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, null, "occExternalKey", null, null);

      org.infinitenature.service.v1.resources.OccurrenceFilter mappedFilter = OccurrenceFilterMapper
            .mapFilter(filter, null);
      assertThat(mappedFilter.getOccExternalKey(), is("occExternalKey"));
   }
}
