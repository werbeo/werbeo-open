package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.PortalConfigResponse;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

public class TestOccurrenceTablePlugin
{
   private OccurrenceTablePlugin occurrenceTablePlugin;
   private Werbeo werbeoMock;
   private OccurrenceResource occurrencesMock;

   @Before
   public void setUp()
   {
      Context context = new Context();
      context.setApp(new Portal("test", 2, 2));
      VaadinSession session = mock(VaadinSession.class);
      when(session.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(context);
      VaadinSession.setCurrent(session);

      UI ui = mock(UI.class);
      when(ui.getLocale()).thenReturn(Locale.GERMANY);
      UI.setCurrent(ui);

      occurrenceTablePlugin = new OccurrenceTablePlugin(
            new MessagesResourceMock());

      werbeoMock = mock(Werbeo.class, Mockito.RETURNS_DEEP_STUBS);
      Set<OccurrenceFieldConfig> occurrenceFieldConfigs = new HashSet<>();
      Set<SampleFieldConfig> sampleFieldConfigs = new HashSet<>();
      when(werbeoMock.occurrences().getFieldConfig(4).getFieldConfigs()).thenReturn(occurrenceFieldConfigs);
      when(werbeoMock.samples().getFieldConfig(4).getFieldConfigs()).thenReturn(sampleFieldConfigs);
      occurrenceTablePlugin.setWerbeo(werbeoMock);

      occurrencesMock = mock(OccurrenceResource.class);
      when(occurrencesMock.find(any(Integer.class), any(Integer.class), any(OccurrenceField.class),
            any(SortOrder.class), any(OccurrenceFilter.class),
            any(Boolean.class))).thenReturn(new OccurrenceSliceResponse());
      when(occurrencesMock.count(any(OccurrenceFilter.class)))
            .thenReturn(new CountResponse(10, "mock-data"));
      occurrenceTablePlugin.setWerbeo(werbeoMock);

      PortalResource portalResource = mock(PortalResource.class);
      when(werbeoMock.portals()).thenReturn(portalResource);

      PortalConfiguration portalConfiguration = new PortalConfiguration();
      portalConfiguration.setMapInitialZoom(8.0);
      portalConfiguration.setMapInitialLatitude(53.9);
      portalConfiguration.setMapInitialLongitude(12.25);
      PortalConfigResponse portalConfigResponse = new PortalConfigResponse(portalConfiguration);
      when(portalResource.getPortalConfiguration(2)).thenReturn(portalConfigResponse);

   }

   @Test
   public void testInit()
   {
      VaadinSession session = VaadinSession.getCurrent();
      UI ui = UI.getCurrent();
      occurrenceTablePlugin.init(Collections.emptyMap());
      assertTrue(occurrenceTablePlugin.getVaadinComponent() != null);
   }

}
