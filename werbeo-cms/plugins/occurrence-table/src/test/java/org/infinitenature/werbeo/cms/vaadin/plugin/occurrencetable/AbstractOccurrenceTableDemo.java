package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import java.util.HashMap;

import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.testing.client.WerbeoMockFactory;
import org.infinitenature.werbeo.testing.cms.utils.NavigationResolverMock;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;

public abstract class AbstractOccurrenceTableDemo extends AbstractTest
{

   private OccurrenceTablePlugin plugin;

   protected final WerbeoMockFactory werbeoMockFactory = new WerbeoMockFactory();

   public AbstractOccurrenceTableDemo()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));

      customizeContext(context);
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);
      Werbeo werbeo = werbeoMockFactory.get();
      plugin = new OccurrenceTablePlugin(new MessagesResourceMock());

      plugin.setWerbeo(werbeo);
      plugin.setNavigationResolver(new NavigationResolverMock());

      plugin.setComponentFactory(new ComponentFactoryMock(werbeo));
      plugin.setInstanceConfig(new InstanceConfig());

   }

   abstract void customizeContext(Context context);

   @Override
   public Component getTestComponent()
   {
      UI.getCurrent().setNavigator(new Navigator(UI.getCurrent(), new Panel()));
      plugin.init(new HashMap<>());
      return plugin.getVaadinComponent();
   }

}
