package org.infinitenature.werbeo.cms.vaadin.plugin.occurrencetable;

import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.ViewMode;

public class MobileOccurrenceTableDemo extends AbstractOccurrenceTableDemo
{

   @Override
   void customizeContext(Context context)
   {
      context.setViewMode(ViewMode.MOBILE);
   }

}
