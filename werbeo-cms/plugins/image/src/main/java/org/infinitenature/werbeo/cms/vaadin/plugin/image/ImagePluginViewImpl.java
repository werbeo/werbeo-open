package org.infinitenature.werbeo.cms.vaadin.plugin.image;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Composite;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public class ImagePluginViewImpl extends Composite
        implements ImagePluginView
{
    private CssLayout layout = new CssLayout();

    public ImagePluginViewImpl()
    {
        layout.setSizeFull();
        setCompositionRoot(layout);
    }

    @Override
    public void setContent(String... images)
    {
        if(images.length == 1)
        {
            ThemeResource resource = new ThemeResource("img/" + images[0]);
            Image vaadinImage = new Image(null, resource);
            vaadinImage.setWidth("90%");
            layout.addComponent(vaadinImage);
        }
        else
        {
            for(String image: images)
            {
                ThemeResource resource = new ThemeResource("img/" + image);
                Image vaadinImage = new Image(null, resource);
                layout.addComponent(vaadinImage);
            }
            layout.addComponent(new Label(" "));
        }


    }
}
