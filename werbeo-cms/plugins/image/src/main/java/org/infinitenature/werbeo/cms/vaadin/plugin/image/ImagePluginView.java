package org.infinitenature.werbeo.cms.vaadin.plugin.image;

import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface ImagePluginView extends CMSVaadinView
{
   public void setContent(String... images);
}
