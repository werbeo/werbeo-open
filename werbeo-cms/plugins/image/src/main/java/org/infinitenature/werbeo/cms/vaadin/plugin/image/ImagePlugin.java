package org.infinitenature.werbeo.cms.vaadin.plugin.image;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import org.infinitenature.vct.VCMSComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.Map;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ImagePlugin implements VCMSComponent
{
    private static final Logger LOGGER = LoggerFactory
            .getLogger(ImagePlugin.class);
    public static final String PARAMETER_TITLE = "title";
    public static final String PARAMETER_IMAGE = "image";
    public static final String PARAMETER_ABSOLUTE_WIDTH = "absoluteWidth";
    public static final String PARAMETER_ABSOLUTE_HEIGHT = "absoluteHeight";
    private Map<String, String> parameter;
    private ImagePluginView view = new ImagePluginViewImpl();

    @Override
    public Component getVaadinComponent()
    {
        return view.getVaadinComponent();

    }

    @Override
    public void init(Map<String, String> parameter)
    {
        this.parameter = parameter;

        String[] images = parameter.get(PARAMETER_IMAGE).split("#");

        view.setContent(images);
    }
}
