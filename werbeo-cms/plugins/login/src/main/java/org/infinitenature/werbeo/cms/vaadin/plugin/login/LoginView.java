package org.infinitenature.werbeo.cms.vaadin.plugin.login;

import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

import com.vaadin.server.Resource;

public interface LoginView extends CMSVaadinView
{

   public void setLoggedOut();

   public void setLoggedIn(String userName);

   public static interface Observer
   {
      public void onClick();

   }

   public void setIcon(Resource resource);
}
