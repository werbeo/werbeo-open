package org.infinitenature.werbeo.cms.vaadin.plugin.login;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Optional;

import org.infinitenature.vct.Authentication;
import org.infinitenature.vct.LoginChangeListener;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.NavigationStateCookie;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

import de.bripkens.gravatar.DefaultImage;
import net.birdirbir.vaadin.gravatars.GravatarResource;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LoginPlugin
      implements VCMSComponent, LoginView.Observer, LoginChangeListener
{

   private LoginView loginView;

   public LoginPlugin()
   {
      super();
      loginView = new LoginViewImpl(this);
   }

   public LoginPlugin(LoginView loginView)
   {
      super();
      this.loginView = loginView;
   }

   @Override
   public Component getVaadinComponent()
   {
      return loginView.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      loginChanged();
   }

   @Override
   public void onClick()
   {
      if (Context.getCurrent().getAuthentication().isAnonymous())
      {
         NavigationStateCookie.saveNavigationState();
         Page.getCurrent().setLocation("/sso/login");
      } else
      {
         Page.getCurrent().setLocation("/sso/logout");
      }

   }

   @Override
   public void loginChanged()
   {
      Authentication authentication = Context.getCurrent().getAuthentication();
      Optional<String> email = authentication.getEmail();
      if (!authentication.isAnonymous() && email.isPresent())
      {
         loginView.setLoggedIn(email.get());
         try
         {
            ExternalResource resource = GravatarResource.builder().size(25)
                  .https(true)
                  .standardDefaultImage(DefaultImage.BLANK).build()
                  .get(email.get());
            loginView.setIcon(resource);
         } catch (UnsupportedEncodingException e)
         {
            // Will not happen, we use a DefaultImage
         }
      } else
      {
         loginView.setLoggedOut();
         loginView.setIcon(VaadinIcons.KEY_O);
      }
   }
}
