package org.infinitenature.werbeo.cms.vaadin.plugin.login;

import com.vaadin.server.Resource;
import com.vaadin.ui.*;

public class LoginViewImpl extends MenuBar implements LoginView
{

   private final Observer observer;
   private MenuItem item;

   public LoginViewImpl(Observer loginViewObserver)
   {
      this.observer = loginViewObserver;
      item = addItem("", l -> observer.onClick());
      setLoggedOut();
      setAutoOpen(true);
      addStyleName("no-height");
   }

   
   @Override
   public void setIcon(Resource icon)
   {
      item.setIcon(icon);
   }
   
   @Override
   public void setLoggedOut()
   {
      item.setText("Anmelden");
   }

   @Override
   public void setLoggedIn(String userName)
   {
      item.setText("Abmelden (" + userName + ")");
   }

   @Override
   public Component getVaadinComponent()
   {
      return this;
   }

}
