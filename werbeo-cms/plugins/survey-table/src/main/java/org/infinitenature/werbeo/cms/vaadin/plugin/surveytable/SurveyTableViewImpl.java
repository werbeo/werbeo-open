package org.infinitenature.werbeo.cms.vaadin.plugin.surveytable;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.caption.AvailabilityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.plugin.surveytable.SurveyTableView.Columns;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.AbstractTableViewImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.viritin.button.MButton;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.provider.GridSortOrderBuilder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.HtmlRenderer;

@SuppressWarnings("serial")
public class SurveyTableViewImpl
      extends AbstractTableViewImpl<SurveyBase, Columns, SurveyFilter>
      implements SurveyTableView
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyTableViewImpl.class);
   private Observer observer;

   private Column<SurveyBase, Object> columnModDate;

   private ComboBox<Boolean> containerFilter = new ComboBox<>();
   private TextField nameFilter = new TextField();

   private Set<SurveyField> fields;
   private Binder<SurveyFilter> filterBinder;

   @Override
   protected List<Columns> getSortableColumns()
   {
      return Arrays.asList(Columns.ID);
   }

   public SurveyTableViewImpl(Observer observer,
         MessagesResource messagesResource, Set<SurveyFieldConfig> fieldConfigs)
   {
      super(Columns.class, SurveyBase.class, observer, messagesResource);
      this.observer = observer;

      fields = EnumSet.copyOf(fieldConfigs.stream()
            .map(SurveyFieldConfig::getField).collect(Collectors.toSet()));
      addStyleName("survey-table-view");
      addColumns();

      grid.setSortOrder(
            new GridSortOrderBuilder<SurveyBase>().thenDesc(columnModDate));
      grid.setId("survey-grid");

      HorizontalLayout bottomButtonLayout = new HorizontalLayout();

      layout.addComponent(bottomButtonLayout);
      layout.setExpandRatio(grid, 1);

      grid.setHeight("860px");

      filterBinder = new Binder<>(SurveyFilter.class);
      if (fields.contains(SurveyField.CONTAINER))
      {
         containerFilter.setItems(Boolean.TRUE, Boolean.FALSE);
         filterBinder.forField(containerFilter).bind("container");
         addFilter(containerFilter);
      }
      filterBinder.forField(nameFilter).bind("nameContains");
      addFilter(nameFilter);

   }

   @Override
   public SurveyFilter buildFilter()
   {
      SurveyFilter filter = new SurveyFilter();
      try
      {
         filterBinder.writeBean(filter);
      } catch (ValidationException e)
      {
         LOGGER.error("Failure reading filter");
      }
      return filter;
   }

   private void addColumns()
   {
      DateTimeFormatter formatter = DateTimeFormatter
            .ofPattern("dd.MM.yyyy HH:mm");
      columnModDate = addColumn(
            survey -> survey.getModificationDate().format(formatter),
            Columns.MOD_DATE);

      addColumn(SurveyBase::getName, Columns.NAME);
      // edit button
      addComponentColumn(occurrence -> new MButton("",
            event -> observer.onEditClick(occurrence))
                  .withStyleName("table-circle-button")
                  .withIcon(VaadinIcons.EDIT).withEnabled(occurrence
                        .getAllowedOperations().contains(Operation.UPDATE)),
            Columns.EDIT, 150);
      addColumn(SurveyBase::getDescription, Columns.DESCRIPTION);
      if (fields.contains(SurveyField.CONTAINER))
      {
         ValueProvider<SurveyBase, Object> valueProvider = survey -> survey
               .getContainer().equals(
                     Boolean.FALSE)
               ? VaadinIcons.CLOSE_CIRCLE.getHtml()
               : VaadinIcons.CHECK_CIRCLE.getHtml();
         addColumn(
               valueProvider,
               Columns.CONTAINER, new HtmlRenderer());
      }
      if (fields.contains(SurveyField.AVAILABILITY))
      {
         addColumn(survey -> new AvailabilityCaptionGenerator(getMessages())
               .apply(survey.getAvailability()), Columns.AVAILABILITY);
      }

   }

   @Override
   public void applyLocalizations()
   {
      super.applyLocalizations();
      containerFilter.setCaption(getCaption(Columns.CONTAINER));
      nameFilter.setCaption(getCaption(Columns.NAME));
   }
}
