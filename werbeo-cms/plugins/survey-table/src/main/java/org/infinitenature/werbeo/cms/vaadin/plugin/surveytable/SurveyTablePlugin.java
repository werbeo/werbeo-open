package org.infinitenature.werbeo.cms.vaadin.plugin.surveytable;

import java.util.Map;

import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.SurveyEdit;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SurveyTablePlugin
      implements VCMSComponent, SurveyTableView.Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyTablePlugin.class);
   @Autowired
   protected Werbeo werbeo;
   @Autowired
   private NavigationResolver navigationResolver;
   protected SurveyTableView view;
   protected ConfigurableFilterDataProvider<SurveyBase, Void, SurveyFilter> surveyDataProvider;
   protected MessagesResource messagesResource;

   public SurveyTablePlugin(@Autowired MessagesResource messagesResource)
   {
      this.messagesResource = messagesResource;
   }

   protected void initView()
   {

      view = new SurveyTableViewImpl(this, this.messagesResource,
            werbeo.surveys().getFieldConfig(Context.getCurrent().getPortal()
                  .getPortalId())
                  .getFieldConfigs());

   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      initView();
      LOGGER.debug("Init with parameter: {}", parameter);
      SurveyDataProvider dataProvider = createDataProvider();

      dataProvider.addSizeChangeListener(size -> view.setSize(size));
      surveyDataProvider = dataProvider.withConfigurableFilter();
      view.setDataProvider(surveyDataProvider);

      view.getVaadinComponent().setSizeFull();
   }

   @Override
   public void onEditClick(SurveyBase survey)
   {
      navigationResolver.navigateTo(new SurveyEdit(survey.getEntityId()));
   }

   protected SurveyDataProvider createDataProvider()
   {
      return new SurveyDataProvider(werbeo);
   }

   public void setWerbeo(Werbeo werbeo)
   {
      this.werbeo = werbeo;
   }

   @Override
   public void onFilterChange(SurveyFilter occurrenceFilter)
   {
      surveyDataProvider.setFilter(occurrenceFilter);
   }

   public void setNavigationResolver(NavigationResolver navigationResolver)
   {
      this.navigationResolver = navigationResolver;
   }

}
