package org.infinitenature.werbeo.cms.vaadin.plugin.surveytable;

import java.util.List;
import java.util.Optional;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.SurveySortField;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.WerbeoDataProvider;
import org.infinitenature.werbeo.cms.vaadin.plugin.surveytable.SurveyTableView.Columns;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.data.provider.Query;

public class SurveyDataProvider
      extends WerbeoDataProvider<SurveyBase, SurveyFilter, SurveySortField>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyDataProvider.class);

   public SurveyDataProvider(Werbeo werbeo)
   {
      super(werbeo);
   }

   @Override
   protected SurveySortField getDefaultSortField()
   {
      return SurveySortField.ID;
   }

   @Override
   protected List<SurveyBase> getContent(Optional<SurveyFilter> filter,
         SurveySortField sortField, SortOrder sortOrder, int offset, int limit)
   {
      SurveyFilter surveyFilter = filter.orElse(new SurveyFilter());
      surveyFilter.setPortalId(Context.getCurrent().getPortal().getPortalId());
      return werbeo.surveys()
            .find(offset, limit, sortField, sortOrder, surveyFilter)
            .getContent();
   }

   @Override
   protected SurveySortField toSortField(
         com.vaadin.data.provider.SortOrder<String> vaadinSortOrder)
   {
      SurveyTableView.Columns column = Columns
            .valueOf(vaadinSortOrder.getSorted());
      switch (column)
      {
      case ID:
         return SurveySortField.ID;
      default:
         LOGGER.error("Trying to sort by the not supported field {}",
               vaadinSortOrder.getSorted());
         return SurveySortField.ID;
      }
   }

   @Override
   protected int getSize(Query<SurveyBase, SurveyFilter> query)
   {
      SurveyFilter surveyFilter = query.getFilter().orElse(new SurveyFilter());
      surveyFilter.setPortalId(Context.getCurrent().getPortal().getPortalId());
      return (int) werbeo.surveys().count(surveyFilter).getAmount();
   }
}
