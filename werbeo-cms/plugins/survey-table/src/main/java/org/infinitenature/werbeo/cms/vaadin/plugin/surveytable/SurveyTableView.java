package org.infinitenature.werbeo.cms.vaadin.plugin.surveytable;

import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableObserver;
import org.infinitenature.werbeo.cms.vaadin.plugin.tables.TableView;

public interface SurveyTableView extends TableView<SurveyBase, SurveyFilter>
{
   public enum Columns
   {
      ID, MOD_DATE, NAME, DESCRIPTION, AVAILABILITY, CONTAINER, EDIT
   }

   public static interface Observer extends TableObserver<SurveyFilter>
   {
      void onEditClick(SurveyBase occurrence);
   }

   void setSize(int size);

   SurveyFilter buildFilter();

}
