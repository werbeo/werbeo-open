package org.infinitenature.werbeo.cms.vaadin.plugin.surveyetable;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.Min;

import org.apache.commons.lang3.time.StopWatch;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.resources.mock.RandomEnum;
import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.service.v1.types.response.SurveyBaseResponse;
import org.infinitenature.service.v1.types.response.SurveyFieldConfigResponse;
import org.infinitenature.service.v1.types.response.SurveysSliceResponse;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.navigation.NavigationResolver;
import org.infinitenature.werbeo.cms.navigation.NavigationTarget;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.plugin.surveytable.SurveyTablePlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.addonhelpers.AbstractTest;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

public abstract class AbstractSurveyTableDemo extends AbstractTest
{
   final static private Logger LOGGER = LoggerFactory
         .getLogger(AbstractSurveyTableDemo.class);

   private static List<SurveyBase> surveys = new ArrayList<>();
   static
   {
      RandomEnum<Availability> randomAvailablities = new RandomEnum<>(
            Availability.class);
      for (int id = 1; id < 100; id++)
      {
         Availability availabiltiy = randomAvailablities.random();
         Person owner = new Person(1, "John", "Doe");
         SurveyBase survey = new SurveyBase(id, "Survey - " + id, availabiltiy,
               owner);
         survey.setDescription("A description for survey - " + id);
         survey.setModificationDate(
               LocalDateTime.of(2019, 12, 12, 8, 33).minusHours(id));
         survey.setContainer(id % 2 == 0);
         surveys.add(survey);
      }
   }
   private SurveyTablePlugin plugin;

   public AbstractSurveyTableDemo()
   {
      Context context = new Context();
      context.setApp(new Portal("Test", 1, 1));

      modifyContext(context);
      VaadinSession.getCurrent().setAttribute(VCMSContext.SESSION_KEY, context);

      StopWatch sw = StopWatch.createStarted();
      plugin = new SurveyTablePlugin(new MessagesResourceMock());
      LOGGER.info("Creating SurveyTablePlugin took {}ms", sw.getTime());
      Werbeo werbeo = mock(Werbeo.class);
      plugin.setWerbeo(werbeo);
      plugin.setNavigationResolver(new NavigationResolver()
      {
         @Override
         public void navigateTo(NavigationTarget target)
         {
            LOGGER.info("NavigationTarget: {}", target);
         }
      });

      SurveysResource surveysResource = new SurveysResource()
      {

         @Override
         public SaveOrUpdateResponse save(@Min(1) int portalId,
               SurveyBase survey)
         {
            return null;
         }

         @Override
         public SurveyBaseResponse getSurvey(@Min(1) int portalId, int id)
         {
            return null;
         }

         @Override
         public SurveysSliceResponse find(@Min(0) int offset, int limit,
               org.infinitenature.service.v1.types.SurveySortField sortField,
               SortOrder sortOrder, SurveyFilter filter)
         {
            Stream<SurveyBase> stream = filter(filter);
            List<SurveyBase> result = stream.skip(offset).limit(limit)
                  .collect(Collectors.toList());
            return new SurveysSliceResponse(result, 0);
         }

         @Override
         public CountResponse count(SurveyFilter filter)
         {
            Stream<SurveyBase> stream = filter(filter);
            return new CountResponse(stream.count(), "mock-data");
         }

         private Stream<SurveyBase> filter(SurveyFilter filter)
         {
            Stream<SurveyBase> stream = surveys.stream();
            return stream;
         }

         @Override
         public SurveyFieldConfigResponse getFieldConfig(@Min(1) int portalId)
         {

            Set<SurveyFieldConfig> fieldConfigs = new HashSet<>();
            fieldConfigs
                  .add(new SurveyFieldConfig(SurveyField.AVAILABILITY, true));
            if (portalId == 2)
            {
               fieldConfigs
                     .add(new SurveyFieldConfig(SurveyField.CONTAINER, true));
            }
            fieldConfigs.add(new SurveyFieldConfig(SurveyField.NAME, true));
            fieldConfigs
                  .add(new SurveyFieldConfig(SurveyField.DESCRIPTION, true));
            return new SurveyFieldConfigResponse(fieldConfigs);
         }

      };
      when(werbeo.surveys()).thenReturn(surveysResource);

   }

   protected abstract void modifyContext(Context context);

   @Override
   public Component getTestComponent()
   {
      plugin.init(new HashMap<>());
      return plugin.getVaadinComponent();
   }

}
