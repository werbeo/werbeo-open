package org.infinitenature.werbeo.cms.vaadin.plugin.surveyetable;

import org.infinitenature.werbeo.cms.vaadin.Context;

public class SurveyTableDemoContainer extends AbstractSurveyTableDemo
{

   @Override
   protected void modifyContext(Context context)
   {
      context.getPortal().setPortalId(2);
   }
}
