package org.infinitenature.werbeo.cms.vaadin.plugin.surveyetable;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;

import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.service.v1.types.response.SurveyFieldConfigResponse;
import org.infinitenature.vct.VCMSContext;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResourceMock;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.plugin.surveytable.SurveyTablePlugin;
import org.junit.Before;
import org.junit.Test;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

public class TestSurveyTablePlugin
{
   private SurveyTablePlugin surveyTablePlugin;
   private Werbeo werbeoMock;
   private VaadinSession session;
   private Context context;

   @Before
   public void setUp()
   {
      context = new Context();
      context.setApp(new Portal("test", 2, 2));
      session = mock(VaadinSession.class);
      when(session.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(context);
      VaadinSession.setCurrent(session);

      UI ui = mock(UI.class);
      when(ui.getLocale()).thenReturn(Locale.GERMANY);
      UI.setCurrent(ui);

      surveyTablePlugin = new SurveyTablePlugin(new MessagesResourceMock());
      werbeoMock = mock(Werbeo.class);
      SurveysResource surveyResourceMock = mock(SurveysResource.class);
      when(surveyResourceMock.getFieldConfig(anyInt()))
            .thenReturn(new SurveyFieldConfigResponse(new HashSet<>(Arrays
                  .asList(new SurveyFieldConfig(SurveyField.NAME, true)))));
      when(werbeoMock.surveys()).thenReturn(surveyResourceMock);
      surveyTablePlugin.setWerbeo(werbeoMock);
   }

   @Test
   public void testInit()
   {
      VaadinSession testsession = session;
      Context testcontext = context;
      surveyTablePlugin.init(Collections.emptyMap());
      assertThat(surveyTablePlugin.getVaadinComponent(), is(notNullValue()));
   }

}
