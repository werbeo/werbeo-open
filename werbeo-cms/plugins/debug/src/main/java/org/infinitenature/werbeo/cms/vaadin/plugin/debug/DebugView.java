package org.infinitenature.werbeo.cms.vaadin.plugin.debug;

import java.util.List;

import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.ServiceInfo;
import org.infinitenature.service.v1.types.UserInfo;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface DebugView extends CMSVaadinView
{
   public void setServiceUser(UserInfo userInfo);

   public void setServiceVersion(ServiceInfo serviceInfo);

   public void setCMSVersion(ServiceInfo readCMSVersion);

   public void setAccisoatedPortals(List<PortalBase> portals);
}
