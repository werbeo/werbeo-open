package org.infinitenature.werbeo.cms.vaadin.plugin.debug;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.infinitenature.service.v1.types.ServiceInfo;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DebugPlugin implements VCMSComponent
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(DebugPlugin.class);

   @Autowired
   private Werbeo werbeo;
   private DebugView debugView;

   public DebugPlugin()
   {
      super();
      debugView = new DebugViewImpl();
   }

   public DebugPlugin(DebugView loginView)
   {
      super();
      this.debugView = loginView;
   }

   @Override
   public Component getVaadinComponent()
   {
      return debugView.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      debugView.setServiceUser(werbeo.info().getServiceUser().getUserInfo());
      debugView
            .setServiceVersion(werbeo.info().getServiceInfo().getServiceInfo());
      debugView.setCMSVersion(loadServiceInfo());
      debugView.setAccisoatedPortals(werbeo.portals()
            .findAssociatedPortals(Context.getCurrent().getApp().getPortalId())
            .getContent());
   }

   private ServiceInfo loadServiceInfo()
   {
      ServiceInfo info = new ServiceInfo();
      String name = "version.properties";
      try (InputStream input = DebugPlugin.class.getClassLoader()
            .getResourceAsStream(name))
      {

         Properties prop = new Properties();

         if (input == null)
         {
            LOGGER.error("Can't find {} to load service version info", name);
         }

         prop.load(input);

         info.setBuildDate(prop.getProperty("build.date"));
         info.setVersion(prop.getProperty("version"));

      } catch (IOException ex)
      {
         LOGGER.error("Failure reading service version info from {}", name, ex);
      }
      return info;
   }

}
