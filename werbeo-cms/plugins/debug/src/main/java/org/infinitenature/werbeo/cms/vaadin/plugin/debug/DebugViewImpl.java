package org.infinitenature.werbeo.cms.vaadin.plugin.debug;

import java.util.List;
import java.util.stream.Collectors;

import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.ServiceInfo;
import org.infinitenature.service.v1.types.UserInfo;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.vaadin.viritin.fields.LabelField;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;

public class DebugViewImpl extends CustomComponent implements DebugView
{

   private FormLayout form = new FormLayout();

   public DebugViewImpl()
   {
      setCompositionRoot(form);
   }

   @Override
   public void setServiceUser(UserInfo userInfo)
   {
      form.addComponent(
            new LabelField<>("ServiceUser").withValue(userInfo.getEmail()));
      form.addComponent(new LabelField<>("Portal-ID")
            .withValue(Context.getCurrent().getPortal().getPortalId()));
      form.addComponent(new LabelField<>("Portal-Name")
            .withValue(Context.getCurrent().getPortal().getName()));
   }

   @Override
   public void setServiceVersion(ServiceInfo serviceInfo)
   {
      form.addComponent(new LabelField<>("Service-Version")
            .withValue(serviceInfo.getVersion()));
      form.addComponent(new LabelField<>("Service-Build-Date")
            .withValue(serviceInfo.getBuildDate()));
   }

   @Override
   public void setCMSVersion(ServiceInfo cmsInfo)
   {
      form.addComponent(
            new LabelField<>("CMS-Version").withValue(cmsInfo.getVersion()));
      form.addComponent(new LabelField<>("CMS-Build-Date")
            .withValue(cmsInfo.getBuildDate()));
   }

   @Override
   public void setAccisoatedPortals(List<PortalBase> portals)
   {
      String portalsText = portals.stream()
            .map(portal -> portal.getEntityId() + " - " + portal.getName())
            .collect(Collectors.joining(", "));
      form.addComponent(
            new LabelField<>("Verbundene Protale").withValue(portalsText));
   }

}
