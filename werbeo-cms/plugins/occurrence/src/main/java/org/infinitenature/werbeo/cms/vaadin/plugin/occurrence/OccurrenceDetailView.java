package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import java.util.List;

import org.infinitenature.herbariorum.entities.Institution;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Taxon.RedListStatus;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.werbeo.cms.vaadin.CMSVaadinView;

public interface OccurrenceDetailView extends CMSVaadinView
{
   void setOccurrence(Occurrence occurrence);

   void setTaxonRedListStatus(RedListStatus redListStatus);
   void setInstitution(Institution institution);
   void setValidator(Person validator);

   public static interface Observer
   {

   }

   void setGermanTaxa(List<TaxonBase> content);
}
