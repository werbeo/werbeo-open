package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.infinitenature.herbariorum.client.HerbariorumClient;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.service.v1.types.Language;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OccurrenceDetailPlugin
      implements VCMSComponent, OccurrenceDetailView.Observer
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceDetailPlugin.class);

   private final Werbeo werbeo;

   private final OccurrenceDetailView view;

   private final HerbariorumClient herbariorumClient;

   private static final Set<Language> germanOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.DEU)));

   public OccurrenceDetailPlugin(
         @Autowired
               MessagesResource messagesResource,
         @Autowired
               InstanceConfig instanceConfig,
         @Autowired
               Werbeo werbeo,
         @Autowired
            HerbariorumClient herbariorumClient)
   {
      int portalId = Context.getCurrent().getPortal().getPortalId();
      this.view = new OccurrenceDetailViewImpl(this, messagesResource,
            instanceConfig.getGeoserver().getUrl(),
            werbeo.samples().getFieldConfig(portalId).getFieldConfigs(),
            werbeo.occurrences().getFieldConfig(portalId).getFieldConfigs(),
            werbeo.taxa().getFieldConfig(portalId).getFieldConfigs(),
            werbeo.surveys().getFieldConfig(portalId)
                  .getFieldConfigs(),
            werbeo.portals().getPortalConfiguration(portalId)
                  .getPortalConfiguration(), Quarasek.getInstance(instanceConfig.getQuarasekUrl()));
      this.werbeo = werbeo;
      this.herbariorumClient = herbariorumClient;
   }

   @Override
   public Component getVaadinComponent()
   {
      return view.getVaadinComponent();
   }

   @Override
   public void init(Map<String, String> parameter)
   {

   }

   @Override
   public void enter(ViewChangeEvent event)
   {

      UUID occurrenceUUID = UUID.fromString(event.getParameters());
      LOGGER.info("Going to load occurrence {}", occurrenceUUID);
      Occurrence occurrence = werbeo.occurrences()
            .get(Context.getCurrent().getApp().getPortalId(), occurrenceUUID)
            .getOccurrence();
      // workaround for #1062
      // TODO: remove after fix
      if(occurrence.getValidation() == null)
      {
         occurrence.setValidation(new Validation());
      }
      view.setOccurrence(occurrence);
      view.setTaxonRedListStatus(
            werbeo.taxa().getTaxon(Context.getCurrent().getApp().getPortalId(),
                  occurrence.getTaxon().getEntityId()).getTaxon()
                  .getRedListStatus());
      List<Taxon> synonyms = werbeo.taxa()
            .findAllSynonyms(Context.getCurrent().getApp().getPortalId(),
                  occurrence.getTaxon().getEntityId())
            .getContent();
      view.setGermanTaxa(synonyms.stream()
            .filter(taxon -> taxon.getLanguage() == Language.DEU)
            .map(taxa -> (TaxonBase) taxa).collect(Collectors.toList()));

      if (occurrence.getValidation() != null && occurrence.getValidation().getValidator()!=null)
      {
         view.setValidator(occurrence.getValidation().getValidator());
      } else
      {
         view.setValidator(null);
      }

      if(occurrence.getHerbarium() != null)
      {
         view.setInstitution(
               herbariorumClient.getByCode(occurrence.getHerbarium().getCode()));
      }
      else
      {
         view.setInstitution(null);
      }
   }
}
