package org.infinitenature.werbeo.cms.vaadin.plugin.occurrence;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.herbariorum.entities.Institution;
import org.infinitenature.quarasek.client.Quarasek;
import org.infinitenature.quarasek.client.representation.PublicationRepresentation;
import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.service.v1.types.Herbarium;
import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.Amount;
import org.infinitenature.service.v1.types.Occurrence.Area;
import org.infinitenature.service.v1.types.Occurrence.BloomingSprouts;
import org.infinitenature.service.v1.types.Occurrence.LifeStage;
import org.infinitenature.service.v1.types.Occurrence.Makropter;
import org.infinitenature.service.v1.types.Occurrence.Quantity;
import org.infinitenature.service.v1.types.Occurrence.RecordStatus;
import org.infinitenature.service.v1.types.Occurrence.Reproduction;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatus;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatusFukarek;
import org.infinitenature.service.v1.types.Occurrence.Sex;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Occurrence.Vitality;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.SampleBase.SampleMethod;
import org.infinitenature.service.v1.types.Taxon.RedListStatus;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.meta.OccurrenceField;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.service.v1.types.meta.TaxonField;
import org.infinitenature.service.v1.types.meta.TaxonFieldConfig;
import org.infinitenature.werbeo.cms.properties.SampleProperties;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.MessagesResource;
import org.infinitenature.werbeo.cms.vaadin.Portal;
import org.infinitenature.werbeo.cms.vaadin.caption.AmountItemCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.AreaCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.AvailabilityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.BloomingSproutsCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.BlurCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.BooleanCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.CoordinateSystemCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.HerbariumHerbaryCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.LifeStageCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.LocalDateTimeCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.MakropterCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.PersonCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.QuantityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.RecordStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.RedListStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.ReproductionCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SampleMethodCaptionGenrator;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SettlementStatusFukarekCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.SexCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.TaxonCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.VagueDateCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.ValidationStatusCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.caption.VitalityCaptionGenerator;
import org.infinitenature.werbeo.cms.vaadin.components.HeaderField;
import org.infinitenature.werbeo.cms.vaadin.components.document.DocumentsField;
import org.infinitenature.werbeo.cms.vaadin.components.form.OccurrenceBindingHelper;
import org.infinitenature.werbeo.cms.vaadin.components.form.SurveyBindingHelper;
import org.infinitenature.werbeo.cms.vaadin.components.maps.MapViewComponent;
import org.infinitenature.werbeo.cms.vaadin.fieldconfig.FieldConfigUtils;
import org.infinitenature.werbeo.cms.vaadin.i18n.I18NComposite;
import org.vaadin.viritin.fields.CaptionGenerator;
import org.vaadin.viritin.fields.LabelField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MHorizontalLayout;

import com.vaadin.data.Binder;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class OccurrenceDetailViewImpl extends I18NComposite
      implements OccurrenceDetailView
{

   private static final String POSITION = "Position";
   private static final String SAMPLE = "Sample";
   private static final String OCCURRENCE = "Occurrence";
   private static final String OCCURRENCE_IMAGE_CONTAINER = "occurrence-image-container";
   private static final String OCCURRENCE_MAIN = "occurrence-main";
   private static final String OCCURRENCE_LABEL = "occurrence-label";
   private static final String OCCURRENCE_HEADER = "occurrence-header";
   public static final String DARKWHITE = "complete";

   private Binder<Occurrence> binder = new Binder<>(Occurrence.class);;

   private HeaderField<TaxonBase> taxonField = new HeaderField<>();
   private LabelField<String> locationCommentField = new LabelField<>();
   private LabelField<String> localityField = new LabelField<>();
   private LabelField<String> habitatField = new LabelField<>();
   private LabelField<Amount> amountField = new LabelField<>();
   private LabelField<SettlementStatus> settlementStatusField = new LabelField<>();
   private LabelField<SettlementStatusFukarek> settlementStatusFukarekField = new LabelField<>();
   private LabelField<RecordStatus> recordStatusField = new LabelField<>();
   private LabelField<String> determinationCommentField = new LabelField<>();
   private LabelField<VagueDate> dateField = new LabelField<>();
   private LabelField<MTB> mtbField = new LabelField<>();
   private LabelField<Integer> coordinateSystemField = new LabelField<>();
   private LabelField<Double> latitudeField = new LabelField<>();
   private LabelField<Double> longitudeField = new LabelField<>();
   private LabelField<String> surveyField = new LabelField<>();
   private LabelField<UUID> idField = new LabelField<>();
   private LabelField<Integer> blurField = new LabelField<>();
   private LabelField<String> externalKeyField = new LabelField<>();
   private LabelField<Quantity> quantityField = new LabelField<>();
   private LabelField<Area> areaField = new LabelField<>();
   private LabelField<Sex> sexField = new LabelField<>();
   private LabelField<Reproduction> reproductionField = new LabelField<>();
   private LabelField<SampleMethod> sampleMethodField = new LabelField<>();
   private LabelField<Integer> numericAmountField = new LabelField<>();
   private DocumentsField documentsField;
   private MapViewComponent mapComponent;
   private LabelField<RedListStatus> redListStatusField = new LabelField<>();
   private LabelField<String> observersField = new LabelField<>();
   private LabelField<String> remarkField = new LabelField<>();
   private LabelField<Person> determinerField = new LabelField<>();
   private LabelField<Person> recorderField = new LabelField<>();
   private LabelField<Vitality> vitalityField = new LabelField<>();
   private LabelField<BloomingSprouts> bloomingSproutsField = new LabelField<>();
   private LabelField<String> herbariumInstitutionField = new LabelField<>();
   private LabelField<Herbarium> herbariumInfoField = new LabelField<>();
   private LabelField<Availability> availablityField = new LabelField<>();
   private LabelField<Makropter> makropterField = new LabelField<>();
   private LabelField<LifeStage> lifestageField = new LabelField<>();
   private LabelField<Person> validatorField = new LabelField<>();
   private LabelField<ValidationStatus> validationStatusField = new LabelField<>();
   private LabelField<String> validationCommentField = new LabelField<>();
   private LabelField<LocalDateTime> validationDateField = new LabelField<>();
   private LabelField<Boolean> werbeoOriginalField = new LabelField<>();
   private LabelField<String> publicationField = new LabelField<>();
   private LabelField<String> publicationCiteCommentField = new LabelField<>();

   private final Observer observer;
   private final String geoserverURL;
   private final Map<OccurrenceField, Boolean> occurrenceFieldConfigs;
   private final Map<SampleField, Boolean> sampleFieldConfigs;
   private final Map<TaxonField, Boolean> taxonFieldConfigs;

   private final Panel scrollBarPanel;

   private final SurveyBindingHelper surveyBindingHelper;
   private final OccurrenceBindingHelper occurrenceBindingHelper;
   private VerticalLayout headerLayout;
   private Quarasek quarasekClient;

   public OccurrenceDetailViewImpl(Observer observer,
         MessagesResource messagesResource, String geoserverURL,
         Set<SampleFieldConfig> sampleFieldConfigs,
         Set<OccurrenceFieldConfig> occurrenceFieldConfigs,
         Set<TaxonFieldConfig> taxonFieldConfigs,
         Set<SurveyFieldConfig> surveyFieldConfigs,
         PortalConfiguration portalConfiguration, Quarasek quarasekClient)
   {
      super(messagesResource);
      this.observer = observer;
      this.quarasekClient = quarasekClient;
      this.geoserverURL = geoserverURL;
      this.sampleFieldConfigs = FieldConfigUtils
            .toSampleConfigMap(sampleFieldConfigs);

      this.taxonFieldConfigs = FieldConfigUtils
            .toTaxonFieldConfigMap(taxonFieldConfigs);

      this.occurrenceFieldConfigs = FieldConfigUtils
            .toOccurrenceConfigMap(occurrenceFieldConfigs);

      this.documentsField = new DocumentsField(messagesResource);

      this.surveyBindingHelper = new SurveyBindingHelper(surveyFieldConfigs);
      this.occurrenceBindingHelper = new OccurrenceBindingHelper(
            occurrenceFieldConfigs);
      HorizontalLayout mainLayout = new MHorizontalLayout().withSpacing(true)
            .withMargin(true);
      mainLayout.addStyleName(DARKWHITE);
      VerticalLayout leftWrapper = new VerticalLayout();
      leftWrapper.setSpacing(true);
      FormLayout leftLayout = new MFormLayout();
      leftLayout.setMargin(false);
      leftLayout.addStyleName(OCCURRENCE_MAIN);
      scrollBarPanel = new Panel(leftWrapper);
      scrollBarPanel.setHeight("100%");
      scrollBarPanel.setWidth("100%");
      mainLayout.addComponent(scrollBarPanel);

      VerticalLayout rightLayout = new VerticalLayout();
      rightLayout.setSpacing(true);
      rightLayout.addStyleName(OCCURRENCE_IMAGE_CONTAINER);
      mainLayout.addComponent(rightLayout);

      setCompositionRoot(mainLayout);

      leftWrapper.addComponent(leftLayout);
      addFields(leftWrapper,leftLayout, rightLayout, portalConfiguration);

      setSizeFull();
   }

   private void addFields(VerticalLayout leftWrapper, FormLayout leftLayout, VerticalLayout rightLayout, PortalConfiguration portalConfiguration)
   {
      taxonField.setCaptionGenerator(new TaxonCaptionGenerator());
      taxonField.getHeader().setHeaderLevel(2);
      taxonField.addStyleName(OCCURRENCE_HEADER);

      mapComponent = new MapViewComponent(geoserverURL, portalConfiguration);
      mapComponent.setId("mapComponent");
      blurField.setCaptionGenerator(new BlurCaptionGenerator(getMessages()));

      availablityField.setCaptionGenerator(
            new AvailabilityCaptionGenerator(getMessages()));

      amountField
            .setCaptionGenerator(new AmountItemCaptionGenerator(getMessages()));
      amountField.addStyleName(OCCURRENCE_LABEL);

      settlementStatusField.setCaptionGenerator(
            new SettlementStatusCaptionGenerator(getMessages()));
      settlementStatusField.addStyleName(OCCURRENCE_LABEL);

      settlementStatusFukarekField.setCaptionGenerator(
            new SettlementStatusFukarekCaptionGenerator(getMessages()));

      recordStatusField.addStyleNames(OCCURRENCE_LABEL);
      recordStatusField.setCaptionGenerator(
            new RecordStatusCaptionGenerator(getMessages()));


      dateField.addStyleName(OCCURRENCE_LABEL);
      dateField.setCaptionGenerator(new VagueDateCaptionGenerator());

      mtbField.addStyleName(OCCURRENCE_LABEL);

      surveyField.addStyleName(OCCURRENCE_LABEL);

      idField.addStyleName(OCCURRENCE_LABEL);

      werbeoOriginalField
            .setCaptionGenerator(new BooleanCaptionGenerator(getMessages()));
      areaField.setCaptionGenerator(new AreaCaptionGenerator(getMessages()));

      quantityField
            .setCaptionGenerator(new QuantityCaptionGenerator(getMessages()));

      sexField.setCaptionGenerator(new SexCaptionGenerator(getMessages()));

      reproductionField.setCaptionGenerator(
            new ReproductionCaptionGenerator(getMessages()));

      sampleMethodField.setCaptionGenerator(
            new SampleMethodCaptionGenrator(getMessages()));

      redListStatusField.setCaptionGenerator(new RedListStatusCaptionGenerator(getMessages()));

      recorderField.setCaptionGenerator(new PersonCaptionGenerator());
      determinerField.setCaptionGenerator(new PersonCaptionGenerator());
      coordinateSystemField.setCaptionGenerator(new CoordinateSystemCaptionGenerator(getMessages()));
      
      publicationField.setCaptionAsHtml(true);
      publicationField.setCaptionGenerator(new CaptionGenerator<String>()
      {
         @Override
         public String getCaption(String citeId)
         {
            if (StringUtils.isNotBlank(citeId))
            {
               PublicationRepresentation publication = quarasekClient
                     .publications().getByCiteId(citeId);
               if (publication != null)
               {
                  try
                  {
                     String publicationString = publication.getAuthors().get(0).getLastName()
                           + ", "
                           + publication.getAuthors().get(0).getFirstName() + " (" + publication.getYear() + ")<br/>";
                     
                     publicationString = publicationString + publication.getTitle() + "<br/>";
                     if(publication.getJournal() != null)
                     {
                        publicationString = publicationString 
                              + publication.getJournal().getName() + ", "
                              + publication.getJournal().getVolume() + "; "
                              + publication.getJournal().getPages();
                              
                     }
                     return publicationString;
                  }
                  catch(Exception e)
                  {
                     return "";
                  }
               } else
               {
                  return ("CiteId: " + citeId);
               }
            } else
            {
               return null;
            }
         }
      });

      vitalityField.setCaptionGenerator(new VitalityCaptionGenerator(getMessages()));
      bloomingSproutsField.setCaptionGenerator(new BloomingSproutsCaptionGenerator(getMessages()));
      herbariumInfoField.setCaptionGenerator(new HerbariumHerbaryCaptionGenerator());
      makropterField
            .setCaptionGenerator(new MakropterCaptionGenerator(getMessages()));
      lifestageField
            .setCaptionGenerator(new LifeStageCaptionGenerator(getMessages()));

      validationDateField
            .setCaptionGenerator(new LocalDateTimeCaptionGenerator());
      validatorField.setCaptionGenerator(new PersonCaptionGenerator());
      validationStatusField.setCaptionGenerator(
            new ValidationStatusCaptionGenerator(getMessages()));
      headerLayout = new VerticalLayout();
      headerLayout.setSpacing(false);
      headerLayout.setMargin(false);
      headerLayout.addComponentAsFirst(taxonField);
      leftWrapper.addComponentAsFirst(headerLayout);

      if(sampleFieldConfigs.containsKey(SampleField.RECORDER))
      {
         leftLayout.addComponent(recorderField);
      }
      occurrenceBindingHelper.addField(binder, leftLayout, determinerField,
            OccurrenceField.DETERMINER);
      occurrenceBindingHelper.addField(binder, leftLayout, remarkField,
            OccurrenceField.REMARK);
      occurrenceBindingHelper.addField(binder, leftLayout, observersField,
            OccurrenceField.OBSERVERS);
      occurrenceBindingHelper.addField(binder, leftLayout,
            determinationCommentField, OccurrenceField.DETERMINATION_COMMENT);
      if (sampleFieldConfigs.containsKey(SampleField.LOCALITY))
      {
         leftLayout.addComponent(localityField);
      }
      if (sampleFieldConfigs.containsKey(SampleField.LOCATION_COMMENT))
      {
         leftLayout.addComponent(locationCommentField);
      }
      occurrenceBindingHelper.addField(binder, leftLayout, habitatField,
            OccurrenceField.HABITAT);
      occurrenceBindingHelper.addField(binder, leftLayout, amountField,
            OccurrenceField.AMOUNT);
      occurrenceBindingHelper.addField(binder, leftLayout, quantityField,
            OccurrenceField.QUANTITY);
      occurrenceBindingHelper.addField(binder, leftLayout, bloomingSproutsField,
            OccurrenceField.BLOOMING_SPROUTS);
      occurrenceBindingHelper.addField(binder, leftLayout, vitalityField,
            OccurrenceField.VITALITY);
      occurrenceBindingHelper.addField(binder, leftLayout,
            settlementStatusField, OccurrenceField.SETTLEMENT_STATUS);
      occurrenceBindingHelper.addField(binder, leftLayout,
            settlementStatusFukarekField,
            OccurrenceField.SETTLEMENT_STATUS_FUKAREK);
      occurrenceBindingHelper.addField(binder, leftLayout, areaField,
            OccurrenceField.AREA);
      occurrenceBindingHelper.addField(binder, leftLayout, recordStatusField,
            OccurrenceField.RECORD_STATUS);
      occurrenceBindingHelper.addField(binder, leftLayout, herbariumInfoField,
            OccurrenceField.HERBARIUM);
      occurrenceBindingHelper.addField(leftLayout, herbariumInstitutionField,
            OccurrenceField.HERBARIUM);

      if (sampleFieldConfigs.containsKey(SampleField.DATE))
      {
         leftLayout.addComponent(dateField);
      }
      leftLayout.addComponent(mtbField);
      leftLayout.addComponent(coordinateSystemField);
      coordinateSystemField.setId("coordinateSystemField");
      leftLayout.addComponent(latitudeField);
      latitudeField.setId("latitudeField");
      leftLayout.addComponent(longitudeField);
      longitudeField.setId("longitudeField");
      leftLayout.addComponent(surveyField);
      surveyBindingHelper.addField(binder, leftLayout, werbeoOriginalField,
            SurveyField.WERBEO_ORIGINAL, "sample.survey.werbeoOriginal");
      occurrenceBindingHelper.addField(binder, leftLayout, makropterField,
            OccurrenceField.MAKROPTER);
      occurrenceBindingHelper.addField(binder, leftLayout, lifestageField,
            OccurrenceField.LIFE_STAGE);
      surveyBindingHelper.addField(binder, leftLayout, availablityField,
            SurveyField.AVAILABILITY, "sample.survey.availability");

      leftLayout.addComponent(idField);
      leftLayout.addComponent(blurField);

      occurrenceBindingHelper.addField(binder, leftLayout, externalKeyField,
            OccurrenceField.EXTERNAL_KEY);
      occurrenceBindingHelper.addField(binder, leftLayout, sexField,
            OccurrenceField.SEX);
      occurrenceBindingHelper.addField(binder, leftLayout, reproductionField,
            OccurrenceField.REPRODUCTION);
      occurrenceBindingHelper.addField(binder, leftLayout, numericAmountField,
            OccurrenceField.NUMERIC_AMOUNT);

      occurrenceBindingHelper.addField(binder, leftLayout,
            validationStatusField, OccurrenceField.VALIDATION,
            "validation.status", "STATUS");
      occurrenceBindingHelper.addField(binder, leftLayout, validationDateField,
            OccurrenceField.VALIDATION, "validation.validationTime", "TIME");
      occurrenceBindingHelper.addField(binder, leftLayout, validationCommentField,
            OccurrenceField.VALIDATION, "validation.comment", "COMMENT");
      if (occurrenceFieldConfigs.containsKey(OccurrenceField.VALIDATION))
      {
         leftLayout.addComponent(validatorField);
      }


      if (sampleFieldConfigs.containsKey(SampleField.SAMPLE_METHOD))
      {
         leftLayout.addComponent(sampleMethodField);
      }
      if (taxonFieldConfigs.containsKey(TaxonField.RED_LIST_STATUS))
      {
         leftLayout.addComponent(redListStatusField);
      }
      
      leftLayout.addComponent(publicationField);
      leftLayout.addComponent(publicationCiteCommentField);

      occurrenceBindingHelper.addField(binder, leftWrapper, documentsField,
            OccurrenceField.MEDIA);

      mapComponent.setSizeFull();


      rightLayout.addComponent(mapComponent);
      rightLayout.setSizeFull();
      rightLayout.setExpandRatio(mapComponent, 1F);

      binder.bind(taxonField, "taxon");
      binder.bind(recorderField, "sample.recorder");
      binder.bind(recordStatusField, "recordStatus");
      binder.bind(dateField, "sample.date");
      binder.bind(mtbField, "sample.locality.position.mtb.mtb");
      binder.bind(coordinateSystemField, "sample.locality.position.epsg");
      binder.bind(latitudeField, "sample.locality.position.posCenterLatitude");
      binder.bind(longitudeField, "sample.locality.position.posCenterLongitude");
      binder.bind(localityField, "sample.locality.locality");
      binder.bind(locationCommentField, "sample.locality.locationComment");
      binder.bind(surveyField, "sample.survey.name");
      binder.bind(idField, "entityId");
      binder.bind(mapComponent, "sample.locality");
      binder.bind(blurField, "sample.locality.blur");
      binder.bind(sampleMethodField, "sample.sampleMethod");
      binder.bind(publicationField, "citeId");
      binder.bind(publicationCiteCommentField, "citeComment");
   }

   @Override
   public void setOccurrence(Occurrence occurrence)
   {
      binder.readBean(occurrence);
      determineVisibilityOfFields(occurrence);
      binder.setReadOnly(true);
      documentsField.suppressCaptions();
   }

   private void determineVisibilityOfFields(Occurrence occurrence)
   {
      binder.getFields().forEach(field -> {
         if (field instanceof LabelField)
         {
            LabelField<?> labelField = ((LabelField) field);
            if (labelField.isEmpty())
            {
               labelField.setVisible(false);
            } else if (StringUtils.isNotBlank(labelField.getId()))
            {
               if((labelField.getId().equalsIgnoreCase("latitudeField")
                     || labelField.getId().equalsIgnoreCase("longitudeField")
                     || labelField.getId()
                     .equalsIgnoreCase("coordinateSytemField")) && !occurrence
                     .getSample().getLocality().getPosition().getType()
                     .equals(Position.PositionType.POINT))
               {
                  labelField.setVisible(false);
               }
            }
         } else if (field.isEmpty() && field instanceof DocumentsField)
         {
            ((DocumentsField) field).setVisible(false);
         }
      });
   }

   @Override
   public void applyLocalizations()
   {

      Portal portal = Context.getCurrent().getPortal();
      Locale locale = UI.getCurrent().getLocale();

      occurrenceBindingHelper.applyLocalizations(getMessages(), portal, locale);

      surveyBindingHelper.applyLocalizations(getMessages(), portal, locale);
      mapComponent.setCaption(
            getMessages().getEntityField(portal, SAMPLE, "POSITION", locale));

      blurField.setCaption(
            getMessages().getEntityField(portal, SAMPLE, "BLUR", locale));

      recordStatusField.setCaption(getMessages().getEntityField(portal,
            OCCURRENCE, "RECORD_STATUS", locale));

      mtbField.setCaption(
            getMessages().getEntityField(portal, SAMPLE, "MTB", locale));
      coordinateSystemField.setCaption(getEntityField(POSITION,"SREF_SYSTEM"));
      latitudeField.setCaption(getEntityField(POSITION, "LATITUDE"));
      longitudeField.setCaption(getEntityField(POSITION, "LONGITUDE"));
      dateField.setCaption(
            getMessages().getEntityField(portal, SAMPLE, "DATE", locale));
      surveyField.setCaption(
            getMessages().getEntityField(portal, SAMPLE, "SURVEY", locale));
      idField.setCaption(
            getMessages().getEntityField(portal, OCCURRENCE, "ID", locale));

      sampleMethodField
            .setCaption(getEntityField(SampleProperties.SAMPLE_METHOD));
      habitatField
            .setCaption(getMessages().getEntityField(portal, SAMPLE,
                  "HABITAT", locale));
      localityField.setCaption(
            getMessages().getEntityField(portal, SAMPLE, "LOCALITY", locale));
      locationCommentField.setCaption(getMessages().getEntityField(portal,
            SAMPLE, "LOCATION_COMMENT", locale));

      redListStatusField.setCaption(
            getMessages().getEntityField(portal, "Taxon", "RED_LIST_STATUS",
                  locale));
      recorderField.setCaption(getEntityField(SAMPLE, "RECORDER"));
      publicationField.setCaption(getEntityField(OCCURRENCE, "PUBLICATION"));

      herbariumInstitutionField
            .setCaption(getEntityField("OccurrenceInputViewImpl", "HERBARY"));
      herbariumInfoField.setCaption(getEntityField("OccurrenceInputViewImpl", "HERBARY_INFO"));

      validatorField.setCaption(getMessages().getEntityField(portal,
            OCCURRENCE, "VALIDATION.VALIDATOR", locale));
   }

   @Override
   public void setTaxonRedListStatus(RedListStatus redListStatus)
   {
      redListStatusField.setValue(redListStatus);
   }

   @Override
   public void setValidator(Person validator)
   {
      validatorField.setValue(validator);
   }

   @Override
   public void setInstitution(Institution institution)
   {
      if(institution != null)
      {
         herbariumInstitutionField.setValue(
               institution.getOrganization() + ", " + institution.getAddress()
                     .getPostalCountry());
      }
      else
      {
         herbariumInstitutionField.setVisible(false);
      }
   }

   @Override
   public void setGermanTaxa(List<TaxonBase> germanTaxa)
   {
      for (TaxonBase germanTaxon : germanTaxa)
      {
         HeaderField<TaxonBase> headerField = new HeaderField<>();
         headerField.setValue(germanTaxon);
         headerField.setCaptionGenerator(new TaxonCaptionGenerator());
         headerField.getHeader().setHeaderLevel(3);
         headerField.addStyleName(OCCURRENCE_HEADER);
         headerLayout.addComponent(headerField);
      }
   }
}
