package org.infinitenature.werbeo.cms.vaadin.app;

import java.util.Map;

import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.ViewMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.navigator.View;
import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class HeuschreckenAppTemplate implements VCMSAppTemplate
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(HeuschreckenAppTemplate.class);

   private Component dynamicContentComponent = new Label();

   private MVerticalLayout layout = new MVerticalLayout()
         .expand(dynamicContentComponent).withFullSize()
         .withId("heuschrecken-portal-app-layout");

   private NavigationBar navigationBar;
   private VCMSComponent loginComponent;

   @Autowired
   private InstanceConfig instanceConfig;

   @Override
   public Component getVaadinComponent()
   {
      return navigationBar;
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      ViewMode viewMode = Context.getCurrent().getViewMode();
      if (viewMode == ViewMode.MOBILE)
      {
         layout.setMargin(false);
      }

      int portalId = Context.getCurrent().getPortal().getPortalId();

      MenuBar termsAndConditionsBar = new MenuBar();
      MenuItem termsAndConditionsItem = termsAndConditionsBar
            .addItem("Nutzungsbedingungen", l -> {
               UI.getCurrent().getNavigator().navigateTo("nutzungsbedingungen");
               UI.getCurrent().setScrollTop(0);
            });

      MenuBar dataSecurityBar = new MenuBar();
      MenuItem dataSecurityItem = dataSecurityBar.addItem("Datenschutz", l -> {
         UI.getCurrent().getNavigator().navigateTo("datenschutzbedingungen");
         UI.getCurrent().setScrollTop(0);
      });

      MenuBar impressumBar = new MenuBar();
      MenuItem impressumItem = impressumBar.addItem("Impressum", l -> {
         UI.getCurrent().getNavigator().navigateTo("imprint");
         UI.getCurrent().setScrollTop(0);
      });
      MenuBar infiniteNatureBar = new MenuBar();
      MenuItem infiniteNatureItem = infiniteNatureBar.addItem("InfiniteNature",
            l -> UI.getCurrent().getPage()
                  .setLocation("https://www.infinitenature.org/"));

      MenuBar inputBar = new MenuBar();
      inputBar.setAutoOpen(true);
      MenuItem inputItem = inputBar.addItem("Dateneingabe");

      inputItem.addItem("Einzelfundeingabe",
            l -> UI.getCurrent().getNavigator().navigateTo("eingabe"));
      inputItem.addItem("Kartierliste",
            l -> UI.getCurrent().getNavigator().navigateTo("kartierliste"));
      MenuItem importMenuItem = inputItem.addItem("CSV Importe",
            l -> UI.getCurrent().getNavigator().navigateTo("importe"));
       importMenuItem.setVisible(Context.getCurrent().isAdmin());
      

      MenuBar outputBar = new MenuBar();
      outputBar.setAutoOpen(true);
      MenuItem outputItem = outputBar.addItem("Datenausgabe");

      outputItem.addItem("Funde",
            l -> UI.getCurrent().getNavigator().navigateTo("funde"));
      outputItem.addItem("Meine Funde",
            l -> UI.getCurrent().getNavigator().navigateTo("meineFunde"));
      outputItem.addItem("Meine Exporte",
            l -> UI.getCurrent().getNavigator().navigateTo("exporte"));
      outputItem.addItem("Verbreitung",
            l -> UI.getCurrent().getNavigator().navigateTo("species"));
      MenuItem projectsItem = outputItem.addItem("Projekte",
            l -> UI.getCurrent().getNavigator().navigateTo("projekte"));
      projectsItem.setVisible(Context.getCurrent().isAdmin());
      MenuItem usersItem = outputItem.addItem("Benutzer",
            l -> UI.getCurrent().getNavigator().navigateTo("benutzer"));
      usersItem.setVisible(Context.getCurrent().isAdmin());

      MenuBar contactBar = new MenuBar();
      MenuItem contactItem = contactBar.addItem("Kontakt",
            l -> UI.getCurrent().getPage().setLocation(
                  instanceConfig.getExternalCmsUrl(portalId) + "kontakt"));

      Image logo = new Image(null, new ThemeResource("img/logo_header.png"));
      logo.addStyleName("logo-image");
      logo.addClickListener(l -> UI.getCurrent().getPage()
            .setLocation(instanceConfig.getExternalCmsUrl(portalId)));

      Image sponsorsLeft = new Image(null,
            new ThemeResource("img/logoleiste_03-2018_links.png"));
      sponsorsLeft.addStyleName("v-link");
      sponsorsLeft.addClickListener(event -> UI.getCurrent().getPage()
            .open("https://www.bmu.de/themen/natur-biologische-vielfalt-arten/naturschutz-biologische-vielfalt/foerderprogramme/bundesprogramm-biologische-vielfalt/",
                  "_blank", false));
      Image sponsorsCenter = new Image(null,
            new ThemeResource("img/logoleiste_03-2018_mitte.png"));
      sponsorsCenter.addStyleName("v-link");
      sponsorsCenter.addClickListener(event -> UI.getCurrent().getPage()
            .open("https://www.bmu.de/", "_blank", false));
      Image sponsorsRight = new Image(null,
            new ThemeResource("img/logoleiste_03-2018_rechts.png"));
      sponsorsRight.addStyleName("v-link");
      sponsorsRight.addClickListener(event -> UI.getCurrent().getPage()
            .open("https://www.bfn.de/", "_blank", false));

      navigationBar = NavigationBarBuilder.get().withTitle("")
            .withAppBarIconComponent(logo)
            .withAppBarElements(inputBar, outputBar, contactBar,
                  loginComponent.getVaadinComponent())
            .withLogoElements(sponsorsLeft, sponsorsCenter, sponsorsRight)
            .withFooterElements(termsAndConditionsBar, dataSecurityBar,
                  impressumBar, infiniteNatureBar).build();
      navigationBar.getContentHolder().addComponent(layout);
   }

   @Override
   public void addTemplateComponent(String area, VCMSComponent component)
   {
      Component vaadinComponent = component.getVaadinComponent();
      vaadinComponent.setSizeFull();
      if ("LOGIN".equals(area))
      {
         loginComponent = component;
      }

   }

   @Override
   public void showView(View view)
   {
      layout.replaceComponent(dynamicContentComponent, view.getViewComponent());
      dynamicContentComponent = view.getViewComponent();
   }

}
