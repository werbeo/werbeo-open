package org.infinitenature.werbeo.cms.vaadin.app;

import java.io.IOException;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NavigationBar extends CustomLayout
{
   private final VerticalLayout contentHolder = new VerticalLayout();
   private final Panel contentPanel = new Panel(contentHolder);
   private final CssLayout appBar = new CssLayout();
   private final Label title = new Label("");
   private final HorizontalLayout titleWrapper = new HorizontalLayout(title);
   private final HorizontalLayout appBarElementWrapper = new HorizontalLayout();
   private final HorizontalLayout footer = new HorizontalLayout();
   private final HorizontalLayout logos = new HorizontalLayout();

   public NavigationBar() throws IOException
   {
      super(NavigationBar.class.getResourceAsStream("navbar-template.html"));
      contentPanel.addStyleName(ValoTheme.PANEL_BORDERLESS);
      contentPanel.addStyleName("no-shadow");
      addComponent(appBar, "app-bar-elements");
      addComponent(contentPanel, "content");
      addComponent(logos, "logos");
      addComponent(footer, "footer");
      addStyleName("content-proper");
      appBar.addComponents(titleWrapper, appBarElementWrapper);
      appBar.addStyleName("nav-bar-responsive");
      appBarElementWrapper.setSpacing(false);
      appBarElementWrapper.setMargin(false);
      appBarElementWrapper.addStyleName("nav-bar-menu");
      titleWrapper.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
      titleWrapper.addStyleName("nav-bar-title");
      titleWrapper.setMargin(false);
      logos.setSpacing(false);
      logos.setMargin(false);
      footer.setSpacing(false);
      footer.setMargin(false);
      footer.addStyleName("nav-bar-menu");
   }

   public void addAppBarElement(Component component) {
      appBarElementWrapper.addComponent(component);
      appBarElementWrapper.setComponentAlignment(component, Alignment.MIDDLE_LEFT);
   }

   public void setTitle(String title)
   {
      this.title.setValue(title);
   }

   public void addAppBarIcon(Component appBarIconComponent) {
      titleWrapper.addComponentAsFirst(appBarIconComponent);
   }

   public VerticalLayout getContentHolder() {
      return contentHolder;
   }

   public void addLogoElement(Component component)
   {
      logos.addComponent(component);
   }

   public void addFooterElement(Component component)
   {
      footer.addComponent(component);
   }
}
