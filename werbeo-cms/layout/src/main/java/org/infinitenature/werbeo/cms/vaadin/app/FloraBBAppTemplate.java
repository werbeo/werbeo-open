package org.infinitenature.werbeo.cms.vaadin.app;

import com.vaadin.navigator.View;
import com.vaadin.server.ThemeResource;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import org.infinitenature.vct.VCMSAppTemplate;
import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.werbeo.cms.InstanceConfig;
import org.infinitenature.werbeo.cms.vaadin.Context;
import org.infinitenature.werbeo.cms.vaadin.ViewMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.util.Map;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class FloraBBAppTemplate implements VCMSAppTemplate
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(FloraMVAppTemplate.class);

   private Component dynamicContentComponent = new Label();

   private MVerticalLayout layout = new MVerticalLayout()
         .expand(dynamicContentComponent).withFullSize()
         .withId("flora-mv-app-layout");

   private NavigationBar navigationBar;
   private VCMSComponent loginComponent;
   
   
   @Autowired
   private InstanceConfig instanceConfig;

   @Override
   public Component getVaadinComponent()
   {
      return navigationBar;
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      ViewMode viewMode = Context.getCurrent().getViewMode();
      if (viewMode == ViewMode.MOBILE)
      {
         layout.setMargin(false);
      }

      MenuBar startBar = new MenuBar();
      startBar.setAutoOpen(true);
      int portalId = Context.getCurrent().getPortal().getPortalId();
      MenuItem startItem = startBar.addItem("Startseite", l -> UI.getCurrent().getPage().setLocation(instanceConfig.getExternalCmsUrl(portalId)));
      
      
      startItem.addItem("Erste Schritte", l -> UI.getCurrent().getPage().setLocation(instanceConfig.getExternalCmsUrl(portalId) + "erste_schritte"));
      startItem.addItem("Nutzungsbedingungen", l -> {
         UI.getCurrent().getNavigator().navigateTo("nutzungsbedingungen");
         UI.getCurrent().setScrollTop(0);
      });
      startItem.addItem("Datenschutz", l -> {
         UI.getCurrent().getNavigator().navigateTo("datenschutzbedingungen");
         UI.getCurrent().setScrollTop(0);
      });
      startItem.addItem("Impressum", l -> UI.getCurrent().getPage().setLocation(instanceConfig.getExternalCmsUrl(portalId) + "impressum"));
      
      MenuBar inputBar = new MenuBar();
      inputBar.setAutoOpen(true);
      MenuItem inputItem = inputBar.addItem("Dateneingabe", l -> UI.getCurrent().getPage().setLocation(instanceConfig.getExternalCmsUrl(portalId) + "dateneingabe"));
      
      inputItem.addItem("Einzelfundeingabe", l -> UI.getCurrent().getNavigator().navigateTo("eingabe"));
      inputItem.addItem("Kartierliste",
            l -> UI.getCurrent().getNavigator().navigateTo("kartierliste"));
      
      MenuBar outputBar = new MenuBar();
      outputBar.setAutoOpen(true);
      MenuItem outputItem = outputBar.addItem("Datenausgabe", l -> UI.getCurrent().getPage().setLocation(instanceConfig.getExternalCmsUrl(portalId) + "datenausgabe"));
      
      outputItem.addItem("Funde", l -> UI.getCurrent().getNavigator().navigateTo("funde"));
      outputItem.addItem("Meine Funde", l -> UI.getCurrent().getNavigator().navigateTo("meineFunde"));
      outputItem.addItem("Meine Exporte", l -> UI.getCurrent().getNavigator().navigateTo("exporte"));
      outputItem.addItem("Verbreitung", l -> UI.getCurrent().getNavigator().navigateTo("species"));
      MenuItem projectsItem = outputItem.addItem("Projekte",
            l -> UI.getCurrent().getNavigator().navigateTo("projekte"));
      projectsItem.setVisible(Context.getCurrent().isAdmin());
      MenuItem usersItem = outputItem.addItem("Benutzer",
            l -> UI.getCurrent().getNavigator().navigateTo("benutzer"));
      usersItem.setVisible(Context.getCurrent().isAdmin());
      
      MenuBar miscBar = new MenuBar();
      miscBar.setAutoOpen(true);
      MenuItem miscItem = miscBar.addItem("Weitere Inhalte");
      
      miscItem.addItem("InfiniteNature",l -> UI.getCurrent().getPage().setLocation("https://www.infinitenature.org/"));
      
      
      MenuBar contactBar = new MenuBar();
      miscBar.setAutoOpen(true);
      MenuItem contactItem = contactBar.addItem("Kontakt",
            l -> UI.getCurrent().getPage().setLocation(
                  instanceConfig.getExternalCmsUrl(portalId) + "kontakt"));

      Image logo = new Image( null, new ThemeResource("img/logo_header.png"));
      logo.addStyleName("logo-image");
      logo.addClickListener(clickEvent -> UI.getCurrent().getPage().setLocation(instanceConfig.getExternalCmsUrl(portalId)));

      Image sponsorsLeft = new Image(null,
            new ThemeResource("img/logoleiste_03-2018_links.png"));
      sponsorsLeft.addStyleName("v-link");
      sponsorsLeft.addClickListener(event -> UI.getCurrent().getPage()
            .open("https://www.bmu.de/themen/natur-biologische-vielfalt-arten/naturschutz-biologische-vielfalt/foerderprogramme/bundesprogramm-biologische-vielfalt/",
                  "_blank", false));
      Image sponsorsCenter = new Image(null,
            new ThemeResource("img/logoleiste_03-2018_mitte.png"));
      sponsorsCenter.addStyleName("v-link");
      sponsorsCenter.addClickListener(event -> UI.getCurrent().getPage()
            .open("https://www.bmu.de/", "_blank", false));
      Image sponsorsRight = new Image(null,
            new ThemeResource("img/logoleiste_03-2018_rechts.png"));
      sponsorsRight.addStyleName("v-link");
      sponsorsRight.addClickListener(event -> UI.getCurrent().getPage()
            .open("https://www.bfn.de/", "_blank", false));
      
      navigationBar = NavigationBarBuilder.get().withTitle("")
            .withAppBarIconComponent(logo)
            .withAppBarElements(startBar, inputBar,outputBar, miscBar, contactBar, loginComponent.getVaadinComponent())
            .withLogoElements(sponsorsLeft, sponsorsCenter, sponsorsRight)
            .withFooterElements().build();
      navigationBar.getContentHolder().addComponent(layout);
   }

   @Override
   public void addTemplateComponent(String area, VCMSComponent component)
   {
      Component vaadinComponent = component.getVaadinComponent();
      vaadinComponent.setSizeFull();
      if ("LOGIN".equals(area))
      {
         loginComponent = component;
      }
   }

   @Override
   public void showView(View view)
   {
      layout.replaceComponent(dynamicContentComponent, view.getViewComponent());
      dynamicContentComponent = view.getViewComponent();

   }

}
