package org.infinitenature.werbeo.cms.vaadin.app;

import com.vaadin.ui.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NavigationBarBuilder
{
   private String title;
   private Component appBarIconComponent;
   private List<Component> appBarElements = new ArrayList<>();
   private NavigationBar instance = null;
   private List<Component> footerElements = new ArrayList<>();
   private List<Component> logoElements = new ArrayList<>();

   public static NavigationBarBuilder get()
   {
      return new NavigationBarBuilder();
   }

   public NavigationBar build()
   {
      if (instance == null)
      {
         try
         {
            instance = new NavigationBar();
         } catch (IOException e)
         {
            return null;
         }
      }

      instance.setTitle(title);
      appBarElements.forEach(instance::addAppBarElement);
      if(appBarIconComponent != null)instance.addAppBarIcon(appBarIconComponent);
      logoElements.forEach(instance::addLogoElement);
      footerElements.forEach(instance::addFooterElement);
      return instance;
   }

   public NavigationBarBuilder withTitle(String title)
   {
      this.title = title;
      return this;
   }

   public NavigationBarBuilder withAppBarIconComponent(Component resourceButton)
   {
      this.appBarIconComponent = resourceButton;
      return this;
   }

   public NavigationBarBuilder withAppBarElements(Component... element)
   {
      this.appBarElements.addAll(Arrays.asList(element));
      return this;
   }

   public NavigationBarBuilder withLogoElements(Component... element)
   {
      this.logoElements.addAll(Arrays.asList(element));
      return this;
   }

   public NavigationBarBuilder withFooterElements(Component... element)
   {
      this.footerElements.addAll(Arrays.asList(element));
      return this;
   }
}
