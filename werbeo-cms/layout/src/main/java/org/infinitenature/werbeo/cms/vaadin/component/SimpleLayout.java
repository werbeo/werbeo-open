package org.infinitenature.werbeo.cms.vaadin.component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.infinitenature.vct.VCMSComponent;
import org.infinitenature.vct.VCMSLayout;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Component;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimpleLayout implements VCMSLayout
{
   public static final String AREA_FOOTER = "SOUTH";
   public static final String AREA_CENTER = "CENTER";

   private MVerticalLayout centerLayout = new MVerticalLayout()
         .withId("center-layout").withMargin(false);

   private MHorizontalLayout southLayout = new MHorizontalLayout()
         .withId("south-layout").withMargin(false);

   private MVerticalLayout mainLayout = new MVerticalLayout(centerLayout,
         southLayout).expand(centerLayout).withFullSize().withId("main-layout")
               .withMargin(false);

   private Set<VCMSComponent> components = new HashSet<>();

   @Override
   public Component getVaadinComponent()
   {
      return mainLayout;
   }

   @Override
   public void init(Map<String, String> parameter)
   {
      // NOOP
   }

   @Override
   public void enter(ViewChangeEvent event)
   {
      components.forEach(component -> component.enter(event));
   }

   @Override
   public void addComponent(String area, VCMSComponent component,
         boolean expand)
   {
      components.add(component);
      Component vaadinComponent = component.getVaadinComponent();
      vaadinComponent.setSizeFull();
      switch (area)
      {

      case AREA_FOOTER:
         southLayout.addComponent(vaadinComponent);
         break;

      case AREA_CENTER:
         centerLayout.addComponent(vaadinComponent);
         if (expand)
         {
            centerLayout.expand(vaadinComponent);
         }
         break;
      default:
         break;
      }
   }
}
