package org.infinitenature.werbe.test.support;

import java.util.UUID;

public class SampleTestUtil
{
   public static final UUID VALID_SAMPLE_ID_0 = UUID
         .fromString("f4c5fc17-2c4a-46ae-8821-fb49db190148");
   public static final UUID VALID_SAMPLE_ID_1 = UUID
         .fromString("81f92875-b650-45b7-8071-85425fdfd1cc");
   public static final UUID UNKNOWN_SAMPLE_ID = UUID
         .fromString("4c5fc17-2c4a-46ae-8821-fb49db190149");

   private SampleTestUtil()
   {
      throw new IllegalAccessError("Utitlity class");
   }
}
