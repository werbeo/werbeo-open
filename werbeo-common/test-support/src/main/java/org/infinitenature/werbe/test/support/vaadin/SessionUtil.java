package org.infinitenature.werbe.test.support.vaadin;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Locale;

import org.infinitenature.vct.VCMSContext;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

public class SessionUtil
{
   private SessionUtil()
   {
      throw new IllegalAccessError("Utitity class");
   }

   public static void mockVaadin(Locale locale, VCMSContext context)
   {
      VaadinSession session = mock(VaadinSession.class);
      when(session.getAttribute(VCMSContext.SESSION_KEY)).thenReturn(context);
      VaadinSession.setCurrent(session);
      UI ui = mock(UI.class);

      when(ui.getLocale()).thenReturn(locale);
      UI.setCurrent(ui);
   }
}
