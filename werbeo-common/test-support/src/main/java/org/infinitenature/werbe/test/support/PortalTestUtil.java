package org.infinitenature.werbe.test.support;

public class PortalTestUtil
{

   public static final int DEMO_ID = 1;
   public static final int VEGETWEB_ID = 2;
   public static final int FLORA_BB_ID = 3;
   public static final int FLORA_MV_ID = 4;
   public static final int REST_SRV_ID = 5;
   public static final int HEUSCHRECKEN_D_ID = 6;

   private PortalTestUtil()
   {
      throw new IllegalAccessError("Utitlity class");
   }
}
