package org.infinitenature.werbe.test.support;

public class UserTestUtil
{

   public static final String LOGIN_CTOMALAA = "ctomalaa@ucoz.com";
   public static final String PASSWORD_CTOMALAA = "ctomalaa";
   public static final String LOGIN_SELLINSF = "sellinsf@bluehost.com";
   public static final String PASSWORD_SELLINSF = "sellinsf";
   public static final String PASSWORD_JBLUND = "jblund";
   public static final String LOGIN_NO_TC = "jblund@bluehost.com";
   public static final String PASSWORD_NO_TC = "jblund";
   public static final String LOGIN_ACCEPT_TC = "hmejer@bluehost.com";
   public static final String PASSWORD_ACCEPT_TC = "hmejer";
   public static final String USER_LOGIN_A = LOGIN_CTOMALAA;
   public static final String USER_LOGIN_B = LOGIN_SELLINSF;
   public static final String LOGIN_NOT_APPROVED = "awalters8@wunderground.com";
   public static final String PASSWORD_NOT_APPROVED = "awalters8";
   public static final String LOGIN_HOGEN = "botanik@xxxxxxx.org";
   public static final String PASSWORD_HOGEN = "xxxxxxx";

   private UserTestUtil()
   {
      throw new IllegalAccessError("Utitity class");
   }

}
