package org.infinitenature.werbeo.common.commons;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestWerbeoStringUtils
{
   @Test
   void testUrlEncode() throws Exception
   {
      String encoded = WerbeoStringUtils.encodeFileAndParams("http://localhost/indicia/upload/1604583716k2_?_stra?e.png");
      assertEquals("http://localhost/indicia/upload/1604583716k2_%3F_stra%3Fe.png", encoded);
   }
}
