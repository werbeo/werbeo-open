package org.infinitenature.werbeo.common.commons.coordinate.impl;


import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.junit.Before;
import org.junit.Test;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.io.WKTWriter;


public class TestTransform
{

   @Before
   public void setUp() throws Exception
   {
   }

   @Test
   public void test() throws Exception
   {

      CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:4258"); // equivalent
                                                                     // zu 4326
      CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:3398");
      WKTReader wktReader = new WKTReader(
            new GeometryFactory(new PrecisionModel()));
      WKTWriter wktWriter = new WKTWriter();
      MathTransform mathTransform = CRS.findMathTransform(sourceCRS, targetCRS,
            false);
      Geometry geom = wktReader.read("POINT(54.094434 13.387527)");
      Geometry transformed = JTS.transform(geom, mathTransform);
      System.out.println(wktWriter.write(transformed));
   }

}
