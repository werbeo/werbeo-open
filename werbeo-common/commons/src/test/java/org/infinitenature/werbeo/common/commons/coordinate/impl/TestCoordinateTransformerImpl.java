package org.infinitenature.werbeo.common.commons.coordinate.impl;

import static org.junit.Assert.assertEquals;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.junit.Test;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.io.WKTWriter;

public class TestCoordinateTransformerImpl
{
   CoordinateTransformerFactory factory = new CoordinateTransformerFactory();

   @Test
   public void testEpsg258337()
   {
      double[] pos258337 = new double[] { 3347720.6103999997, 5861061.5408 };

      for (int i = 0; i < 20; i++)
      {
         long start = System.currentTimeMillis();
         CoordinateTransformer coordinateTransformer = factory
               .getCoordinateTransformer(258337, 4326);
         System.out.println(System.currentTimeMillis() - start + " ms");
         start = System.currentTimeMillis();
         coordinateTransformer = factory.getCoordinateTransformer(4326, 31468);
         System.out.println(System.currentTimeMillis() - start + " ms");
         start = System.currentTimeMillis();
         CoordinateTransformer coordinateTransformerInv = factory
               .getCoordinateTransformer(31468, 4326);
         System.out.println(System.currentTimeMillis() - start + " ms");

      }

      // System.out.println(pos4326[0] + ", " + pos4326[1]);
   }

   @Test
   public void testEpsg4326to31468()
   {
      CoordinateTransformer coordinateTransformer = factory
            .getCoordinateTransformer(4326, 31468);
      CoordinateTransformer coordinateTransformerInv = factory
            .getCoordinateTransformer(31468, 4326);
      double[] pos4326 = new double[] { 10.943231574066, 53.55008258943 };
      double[] pos31468 = coordinateTransformer.convert(pos4326);
      double[] invert = coordinateTransformerInv.convert(pos31468);
      assertEquals(4430059.9132d, pos31468[0], 0.1d);
      assertEquals(5935917.6805d, pos31468[1], 0.1d);
      assertEquals(10.9432, invert[0], 0.001d);
      assertEquals(53.5500, invert[1], 0.001d);
   }

   @Test
   public void testEpsg31468to4326()
   {
      CoordinateTransformer coordinateTransformer = factory
            .getCoordinateTransformer(31468, 4326);
      CoordinateTransformer coordinateTransformerInv = factory
            .getCoordinateTransformer(4326, 31468);
      double[] pos31468 = new double[] { 4502721, 6004775 };
      double[] pos4326 = coordinateTransformer.convert(pos31468);
      assertEquals(12.04010d, pos4326[0], 0.1d);
      assertEquals(54.17338d, pos4326[1], 0.1d);
      double[] invert = coordinateTransformerInv.convert(pos4326);
      assertEquals(4502721.0, invert[0], 0.1d);
      assertEquals(6004775.0, invert[1], 0.1d);
   }

   @Test
   public void testConvert()
   {
      double[] pos4326 = new double[] { 13.387527, 54.094434 };

      System.out.println("| EPSG | LONG | LAT |");
      System.out.println("| ---- | ---- | --- |");
      for (int target : new int[] { 4326, 3398, 5678, 31468, 25833 })
      {
         CoordinateTransformer coordinateTransformer = factory
               .getCoordinateTransformer(4326, target);

         double[] converted = coordinateTransformer.convert(pos4326);
         System.out.println("| " + target + " | " + converted[0] + " | "
               + converted[1] + " |");
      }

   }

   @Test
   public void testConvertWKT()
         throws NoSuchAuthorityCodeException, FactoryException, ParseException,
         MismatchedDimensionException, TransformException
   {


      CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:4326");
      CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:3398");
      WKTReader wktReader = new WKTReader(
            new GeometryFactory(new PrecisionModel()));
      WKTWriter wktWriter = new WKTWriter();
      MathTransform mathTransform = CRS.findMathTransform(sourceCRS, targetCRS,
            true);

      org.locationtech.jts.geom.Geometry geom = wktReader
            .read("POINT(54.094434 13.387527)");
      org.locationtech.jts.geom.Geometry transformed = JTS.transform(geom,
            mathTransform);

      System.out.println(
            wktWriter.write(transformed));
   }

   @Test
   public void testConvertJTS()
         throws NoSuchAuthorityCodeException, FactoryException,
         TransformException
   {
      double[] pos4326 = new double[] { 13.387527, 54.094434 };
      Coordinate source = new Coordinate(pos4326[1], pos4326[0]);

      CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:4326");
      System.out.println(sourceCRS.toWKT());
      System.out.println("| EPSG | LONG | LAT |");
      System.out.println("| ---- | ---- | --- |");
      for (int target : new int[] { 4326, 3398, 5678, 31468, 25833 })
      {
         CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:" + target);

         MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS,
               true);

         Coordinate converted = JTS.transform(source, (Coordinate) null,
               transform);
         System.out.println("| " + target + " | " + converted.x + " | "
               + converted.y + " |");

         System.out.println(targetCRS.toWKT());
      }

   }
}
