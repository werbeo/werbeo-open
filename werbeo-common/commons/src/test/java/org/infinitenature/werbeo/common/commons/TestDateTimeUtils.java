package org.infinitenature.werbeo.common.commons;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestDateTimeUtils
{

   @Test
   @DisplayName("Test time zone moving")
   void test001()
   {
      LocalDateTime utcDateTime = LocalDateTime.of(2020, 6, 16, 12, 29);
      assertThat(
            DateTimeUtils.toZone(utcDateTime, ZoneId.of("UTC"),
                  ZoneId.of("Europe/Berlin")),
            is(LocalDateTime.of(2020, 6, 16, 14, 29)));
   }

   @Test
   @DisplayName("Test get first day of the week")
   void test002()
   {
      LocalDateTime saturday = LocalDateTime.of(2020, 07, 11, 10, 00);

      assertThat(DateTimeUtils
            .getStartOfWeek(saturday),
            is(LocalDate.of(2020, 07, 06)));
   }

   @Test
   @DisplayName("Test get last day of the week")
   void test003()
   {
      LocalDateTime saturday = LocalDateTime.of(2020, 07, 11, 10, 00);

      assertThat(DateTimeUtils.getEndOfWeek(saturday),
            is(LocalDate.of(2020, 07, 12)));
   }

   @Test
   @DisplayName("Test get first day of the week - which is current day")
   void test004()
   {
      LocalDateTime saturday = LocalDateTime.of(2020, 07, 06, 10, 00);

      assertThat(DateTimeUtils.getStartOfWeek(saturday),
            is(LocalDate.of(2020, 07, 06)));
   }

   @Test
   @DisplayName("Test get last day of the week - which is current day")
   void test005()
   {
      LocalDateTime saturday = LocalDateTime.of(2020, 07, 12, 10, 00);

      assertThat(DateTimeUtils.getEndOfWeek(saturday),
            is(LocalDate.of(2020, 07, 12)));
   }
}
