package org.infinitenature.werbeo.common.commons.coordinate.impl;

import static org.junit.Assert.assertEquals;

import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.junit.Before;
import org.junit.Test;

public class TestGeometryHelper
{
   private GeometryHelper geometryHelper;
   private CoordinateTransformer transformer;

   @Before
   public void setUp() throws Exception
   {
      geometryHelper = new GeometryHelper();
      transformer = new CoordinateTransformerFactory()
	    .getCoordinateTransformer(MTB.getEpsgNumber(), MTB.getEpsgNumber());
   }

   @Test
   public void testGetCenterMTB()
   {
      assertEquals(MTBHelper.toMTB("1927/124"), geometryHelper.getCenterMTB(
	    MTBHelper.toMTB("1927/124").toWkt(), MTB.getEpsgNumber(),
	    transformer));

      transformer = new CoordinateTransformerFactory()
	    .getCoordinateTransformer(31468, MTB.getEpsgNumber());

      assertEquals(MTBHelper.toMTB("2231/411"), geometryHelper
            .getCenterMTB("POINT( 4428898 5956773 )", 31468, transformer));
   }

}
