package org.infinitenature.werbeo.common.commons.coordinate.impl;

import org.infinitenature.werbeo.common.commons.coordinate.CoordinateFormatter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCoordinateFormatter
{
   @Test
   public void testFormat_epsg4326()
   {
      int epsg = 4326;
      double latitude = 54.308533123;
      double longitude = 13.060822123;

      String formatted_1 = CoordinateFormatter.format(epsg, longitude, latitude);
      String formatted_2 = CoordinateFormatter.format(epsg, latitude);
      String formatted_3 = CoordinateFormatter.format(epsg, longitude);

      assertEquals("54.308533, 13.060822", formatted_1);
      assertEquals("54.308533", formatted_2);
      assertEquals("13.060822", formatted_3);
   }

   @Test
   public void testFormat_epsg3398()
   {
      int epsg = 3398;
      double latitude = 53.85;
      double longitude = 12.41;

      String formatted_1 = CoordinateFormatter.format(epsg, longitude, latitude);
      String formatted_2 = CoordinateFormatter.format(epsg, latitude);
      String formatted_3 = CoordinateFormatter.format(epsg, longitude);

      assertEquals("53.850000, 12.410000", formatted_1);
      assertEquals("53.850000", formatted_2);
      assertEquals("12.410000", formatted_3);
   }

   @Test
   public void testFormat_epsg3357_doesNotExist()
   {
      int epsg = 3357;
      double latitude = 54.00150;
      double longitude = 12.81675;

      String formatted_1 = CoordinateFormatter.format(epsg, longitude, latitude);
      String formatted_2 = CoordinateFormatter.format(epsg, latitude);
      String formatted_3 = CoordinateFormatter.format(epsg, longitude);

      assertEquals("54.001500, 12.816750", formatted_1);
      assertEquals("54.001500", formatted_2);
      assertEquals("12.816750", formatted_3);
   }

   @Test
   public void testFormat_epsg3857()
   {
      int epsg = 3857;
      double latitude = 54.00150;
      double longitude = 12.81675;

      String formatted_1 = CoordinateFormatter.format(epsg, longitude, latitude);
      String formatted_2 = CoordinateFormatter.format(epsg, latitude);
      String formatted_3 = CoordinateFormatter.format(epsg, longitude);

      assertEquals("54.001500, 12.816750", formatted_1);
      assertEquals("54.001500", formatted_2);
      assertEquals("12.816750", formatted_3);
   }

   @Test
   public void testFormat_epsg25833()
   {
      int epsg = 25833;
      double latitude = 5861061.3408;
      double longitude = 3347720.6103;

      String formatted_1 = CoordinateFormatter.format(epsg, longitude, latitude);
      String formatted_2 = CoordinateFormatter.format(epsg, latitude);
      String formatted_3 = CoordinateFormatter.format(epsg, longitude);

      assertEquals("5861061, 3347721", formatted_1);
      assertEquals("5861061", formatted_2);
      assertEquals("3347721", formatted_3);
   }

   @Test
   public void testFormat_epsg31468()
   {
      int epsg = 31468;
      double latitude = 5985950.12345;
      double longitude = 4553658.12345;

      String formatted_1 = CoordinateFormatter.format(epsg, longitude, latitude);
      String formatted_2 = CoordinateFormatter.format(epsg, latitude);
      String formatted_3 = CoordinateFormatter.format(epsg, longitude);

      assertEquals("5985950, 4553658", formatted_1);
      assertEquals("5985950", formatted_2);
      assertEquals("4553658", formatted_3);
   }
}
