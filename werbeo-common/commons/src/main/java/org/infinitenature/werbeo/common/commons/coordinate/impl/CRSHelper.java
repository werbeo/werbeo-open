package org.infinitenature.werbeo.common.commons.coordinate.impl;

import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

public class CRSHelper
{
   private CRSHelper()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static final String CRS4745 = "GEOGCS[\"RD/83\","
	 + " DATUM[\"Rauenberg_Datum_83\","
	 + " SPHEROID[\"Bessel 1841\",6377397.155,299.1528128,"
	 + " AUTHORITY[\"EPSG\",\"7004\"]],"
	 + "TOWGS84[-104.1, -49.1, -9.9, 0.971, -2.917, 0.714, -11.68],"
	 + " AUTHORITY[\"EPSG\",\"6745\"]]," + "PRIMEM[\"Greenwich\",0,"
	 + "AUTHORITY[\"EPSG\",\"8901\"]],"
	 + "UNIT[\"degree\",0.01745329251994328,"
	 + " AUTHORITY[\"EPSG\",\"9122\"]]," + " AUTHORITY[\"EPSG\",\"4745\"]]";
   public static final String CRS258337 = "PROJCS[\"ETRS89_UTM_Zone33_BB_7\",GEOGCS[\"GCS_ETRS_1989\",DATUM[\"D_ETRS_1989\",SPHEROID[\"GRS_1980\",6378137.0,298.257222101],TOWGS84[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]],PRIMEM[\"Greenwich\",0.0],UNIT[\"Degree\",0.0174532925199433]],PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"False_Easting\",3500000.0],PARAMETER[\"False_Northing\",0.0],PARAMETER[\"Central_Meridian\",15.0],PARAMETER[\"Scale_Factor\",0.9996],PARAMETER[\"Latitude_Of_Origin\",0.0],UNIT[\"Meter\",1.0]]\'";

   public static CoordinateReferenceSystem getCRS(int srid)
	 throws FactoryException, NoSuchAuthorityCodeException
   {
      CoordinateReferenceSystem sourceCRS;
      switch (srid)
      {
      case 4745:
	 sourceCRS = CRS.parseWKT(CRS4745);
	 break;
      case 258337:
	 sourceCRS = CRS.parseWKT(CRS258337);
	 break;
      default:
	 sourceCRS = CRS.decode("EPSG:" + srid, true);
	 break;
      }
      return sourceCRS;
   }

}
