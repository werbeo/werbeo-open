package org.infinitenature.werbeo.common.commons.coordinate.impl;

import org.geotools.geometry.GeometryBuilder;
import org.geotools.geometry.jts.JTS;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.IndiciaTransformException;
import org.opengis.geometry.DirectPosition;
import org.opengis.geometry.Geometry;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.io.WKTWriter;

public class CoordinateTransformerImpl implements CoordinateTransformer
{
   public static final String CAN_T_CONVERT = "Can't convert ";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CoordinateTransformerImpl.class);
   public static final String FROM = " from ";
   public static final String TO = " to ";

   private CoordinateReferenceSystem sourceCRS;
   private CoordinateReferenceSystem targetCRS;
   private MathTransform mathTransform;

   private GeometryBuilder geometryBuilder;

   private WKTReader wktReader = new WKTReader(new GeometryFactory());
   private WKTWriter wktWriter = new WKTWriter();

   public CoordinateTransformerImpl(CoordinateReferenceSystem sourceCRS,
         CoordinateReferenceSystem targetCRS, MathTransform mathTransform,
         GeometryBuilder geometryBuilder)
   {
      this.sourceCRS = sourceCRS;
      this.targetCRS = targetCRS;
      this.mathTransform = mathTransform;
      this.geometryBuilder = geometryBuilder;
   }

   @Override
   public DirectPosition convert(DirectPosition source)
   {
      try
      {
         return mathTransform.transform(source, null);
      } catch (Exception e)
      {
         String message = CAN_T_CONVERT + source.getCoordinate()[0] + ", "
               + source.getCoordinate()[1] + FROM + sourceCRS.getName()
               + TO + targetCRS.getName();
         LOGGER.error(message, e);
         throw new IndiciaTransformException(message, e);
      }
   }

   @Override
   public double[] convert(double[] sourceCoordinate)
   {
      DirectPosition sourcePosition = geometryBuilder
            .createDirectPosition(sourceCoordinate);
      return convert(sourcePosition).getCoordinate();
   }

   @Override
   public Geometry convert(Geometry sourceGeometry)
   {
      try
      {
         return sourceGeometry.transform(targetCRS);
      } catch (Exception e)
      {
         String message = CAN_T_CONVERT + sourceGeometry + FROM
               + sourceCRS.getName() + TO + targetCRS.getName();
         LOGGER.error(message, e);
         throw new IndiciaTransformException(message, e);
      }
   }

   @Override
   public org.locationtech.jts.geom.Geometry convert(
         org.locationtech.jts.geom.Geometry sourceGeometry)
   {
      try
      {
         return JTS.transform(sourceGeometry, mathTransform);
      } catch (Exception e)
      {
         String message = CAN_T_CONVERT
                 + sourceGeometry + FROM
               + sourceCRS.getName() + TO + targetCRS.getName();
         LOGGER.error(message, e);
         throw new IndiciaTransformException(message, e);
      }
   }

   @Override
   public CoordinateReferenceSystem getSourceCRS()
   {
      return sourceCRS;
   }

   @Override
   public CoordinateReferenceSystem getTargetCRS()
   {
      return targetCRS;
   }

   @Override
   public MathTransform getMathTransform()
   {
      return mathTransform;
   }

   @Override
   public String convert(String wellKnownText)
   {
      try
      {
         org.locationtech.jts.geom.Geometry geom = wktReader
               .read(wellKnownText);
         org.locationtech.jts.geom.Geometry transformed = JTS.transform(geom,
               mathTransform);
         return wktWriter.write(transformed);
      } catch (Exception e)
      {
         String message = "Failure converting wellKnownText from "
               + sourceCRS.getName() + TO + targetCRS.getName();
         LOGGER.error(message, e);
         throw new IndiciaTransformException(message, e);
      }
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("CoordinateTransformerImpl@");
      builder.append(System.identityHashCode(this));
      builder.append(" [sourceCRS=");
      builder.append(sourceCRS);
      builder.append(", targetCRS=");
      builder.append(targetCRS);
      builder.append("]");
      return builder.toString();
   }

}