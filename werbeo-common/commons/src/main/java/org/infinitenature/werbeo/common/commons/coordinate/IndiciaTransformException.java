package org.infinitenature.werbeo.common.commons.coordinate;

@SuppressWarnings("serial")
public class IndiciaTransformException extends RuntimeException
{
   public IndiciaTransformException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public IndiciaTransformException(String message)
   {
      super(message);
   }
}
