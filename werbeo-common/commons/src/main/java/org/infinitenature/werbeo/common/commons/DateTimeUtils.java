package org.infinitenature.werbeo.common.commons;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateTimeUtils
{
   private DateTimeUtils()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static LocalDateTime toZone(final LocalDateTime time,
         final ZoneId fromZone, final ZoneId toZone)
   {
      final ZonedDateTime zonedtime = time.atZone(fromZone);
      final ZonedDateTime converted = zonedtime.withZoneSameInstant(toZone);
      return converted.toLocalDateTime();
   }

   public static LocalDate getLocalDate(final LocalDateTime localDateTime)
   {
      return LocalDate.of(localDateTime.getYear(),
            localDateTime.getMonthValue(), localDateTime.getDayOfMonth());
   }

   public static LocalDate getStartOfWeek(final LocalDateTime localDateTime)
   {
      return getStartOfWeek(localDateTime, DayOfWeek.MONDAY);
   }

   public static LocalDate getEndOfWeek(final LocalDateTime localDateTime)
   {
      return getEndOfWeek(localDateTime, DayOfWeek.SUNDAY);
   }

   public static LocalDate getEndOfWeek(LocalDateTime localDateTime,
         DayOfWeek endOfWeek)
   {
      LocalDate currentDay = getLocalDate(localDateTime);
      while (currentDay.getDayOfWeek() != endOfWeek)
      {
         currentDay = currentDay.plusDays(1l);
      }
      return currentDay;
   }

   public static LocalDate getStartOfWeek(
         final LocalDateTime localDateTime,
         final DayOfWeek startOfWeek)
   {
      LocalDate currentDay = getLocalDate(localDateTime);
      while (currentDay.getDayOfWeek() != startOfWeek)
      {
         currentDay = currentDay.minusDays(1l);
      }
      return currentDay;
   }
}
