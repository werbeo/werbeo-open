package org.infinitenature.werbeo.common.commons.coordinate.impl;


import org.geotools.geometry.GeometryBuilder;
import org.geotools.geometry.iso.text.WKTParser;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.common.commons.coordinate.IndiciaTransformException;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiLineString;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.opengis.geometry.DirectPosition;
import org.opengis.geometry.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeometryHelper
{
   private CoordinateTransformerFactory factory;
   
   private static final Logger LOGGER = LoggerFactory
         .getLogger(GeometryHelper.class);

   public GeometryHelper()
   {
      factory = new CoordinateTransformerFactory();
   }


   public Geometry parse(String wkt, int srid)
   {
      try
      {
         WKTParser wktParser = new WKTParser(
               new GeometryBuilder(CRSHelper.getCRS(srid)));
         return wktParser.parse(wkt);
      } catch (Exception e)
      {
         LOGGER.error("Error parsing wkt: " + wkt, e);
         throw new IndiciaTransformException("Failure parsing wkt", e);
      }
   }

   public DirectPosition getCenter(String wkt, int srid)
   {
      Geometry geometry = parse(wkt, srid);
      return geometry.getCentroid();
   }

   public org.locationtech.jts.geom.Geometry parseToJts(String wkt, int srid)
         throws ParseException
   {
      WKTReader wktReader = new WKTReader(new GeometryFactory(new PrecisionModel(), srid));
      wktReader.read(wkt);
      return wktReader.read(wkt);
   }

   public MTB getCenterMTB(String wkt, int srid,
         CoordinateTransformer transformer)
   {
      DirectPosition center = getCenter(wkt, srid);
      return MTBHelper
            .toMTB(MTBHelper.fromCentroid(transformer.convert(center)));
   }

   public Point getPoint(double latitude, double longitude, int srid)
   {
      GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(),srid);
      return geometryFactory.createPoint(new Coordinate(longitude,latitude));
   }

   public Polygon getPolygon(double[] northWest, double[] southEast, int srid)
   {
      Coordinate[] coordinates = new Coordinate[4];
      coordinates[0] = new Coordinate(northWest[0], southEast[1]);
      coordinates[1] = new Coordinate(southEast[0], southEast[1]);
      coordinates[2] = new Coordinate(southEast[0], northWest[1]);
      coordinates[3] = new Coordinate(northWest[0], northWest[1]);
      return getPolygon(coordinates, srid);
   }

   public Polygon getPolygon(Coordinate[] coordinates, int srid)
   {
      GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(),srid);
      return geometryFactory.createPolygon(coordinates);
   }

   public double[] findPosSE(org.locationtech.jts.geom.Geometry geom, boolean... doNotTest)
   {
      double south = Double.MAX_VALUE;
      double east = Double.MIN_VALUE;
      double antiEast = Double.MAX_VALUE; // in case the geom crosses the antimeridian

      Coordinate[] coordinates = geom.getCoordinates();
      for(Coordinate coordinate : coordinates)
      {
         south = Double.min(south, coordinate.x);
         east = Double.max(east, coordinate.y);
         antiEast = Double.min(antiEast, coordinate.y);
      }

      if(doNotTest != null && doNotTest.length > 0 && doNotTest[0] == false)
      {
         Polygon polygon = getPolygon(findPosNW(geom, false), new double[]{south, east}, geom.getSRID());
         if(!polygon.getCentroid().within(geom))
         {
            return new double[]{south, antiEast};
         }
      }

      return new double[]{south, east};
   }

   public double[] findPosNW(org.locationtech.jts.geom.Geometry geom, boolean... doNotTest)
   {
      double north = Double.MIN_VALUE;
      double west = Double.MAX_VALUE;
      double antiWest = Double.MIN_VALUE; // in case the geom crosses the antimeridian

      Coordinate[] coordinates = geom.getCoordinates();
      for(Coordinate coordinate : coordinates)
      {
         north = Double.max(north, coordinate.x);
         west = Double.min(west, coordinate.y);
      }

      if(doNotTest != null && doNotTest.length > 0 && doNotTest[0] == false)
      {
         Polygon polygon = getPolygon(findPosSE(geom, false), new double[]{north, west}, geom.getSRID());
         if(!polygon.getCentroid().within(geom))
         {
            return new double[]{north, antiWest};
         }
      }
      return new double[]{north, west};
   }

   public int calculatePrecision(String wkt, int epsg)
   {
      try
      {
         // not wgs84 wkts need to be converted to wgs84
         if(epsg != 4326)
         {
            wkt = factory.getCoordinateTransformer(epsg, 4326).convert(wkt);
         }
         GeometryFactory geometryFactory = new GeometryFactory();
         WKTReader reader = new WKTReader(geometryFactory);

         double area = 0;
         Object geom = reader.read(wkt);
         if (geom instanceof Point)
         {
            // no precision for point
         }
         else if (geom instanceof Polygon)
         {
            Polygon polygon = (Polygon) geom;
            area = polygon.getArea() * 6371 * 1000000d;
         }
         else if (geom instanceof MultiPolygon)
         {
            MultiPolygon multiPolygon = (MultiPolygon) geom;
            area = multiPolygon.getArea();
         }
         else
         {
            MultiLineString multiPolygon = (MultiLineString) geom;
            area = multiPolygon.getArea();
         }

         double length = (Math.sqrt(area));
         // pythagoras!
         double diagonal = Math.sqrt(length * length + length * length);
         // half the diagonal
         int precision = (int) (diagonal / 2);
         return precision;
      } catch (Exception e)
      {
         e.printStackTrace();
         return 0;
      }
   }
}