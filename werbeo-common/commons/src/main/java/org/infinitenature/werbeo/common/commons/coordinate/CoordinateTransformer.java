package org.infinitenature.werbeo.common.commons.coordinate;

import org.opengis.geometry.*;
import org.opengis.referencing.crs.*;
import org.opengis.referencing.operation.*;

public interface CoordinateTransformer
{
   public double[] convert(double[] sourceCoordinate);

   public org.opengis.geometry.Geometry convert(org.opengis.geometry.Geometry sourceGeometry);

   public org.locationtech.jts.geom.Geometry convert(org.locationtech.jts.geom.Geometry sourceGeometry);
   
   public String convert(String wkt);

   public CoordinateReferenceSystem getSourceCRS();

   public CoordinateReferenceSystem getTargetCRS();

   public MathTransform getMathTransform();

   DirectPosition convert(DirectPosition source);
}
