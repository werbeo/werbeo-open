package org.infinitenature.werbeo.common.commons.coordinate;

import java.io.Serializable;

import org.geotools.geometry.GeometryBuilder;
import org.geotools.referencing.CRS;
import org.infinitenature.werbeo.common.commons.coordinate.impl.CRSHelper;
import org.infinitenature.werbeo.common.commons.coordinate.impl.CoordinateTransformerImpl;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@SuppressWarnings("serial")
@Service
public class CoordinateTransformerFactory implements Serializable
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CoordinateTransformerFactory.class);

   public CoordinateTransformer getCoordinateTransformer(int source, int target)
   {
      try
      {
         CoordinateReferenceSystem sourceCRS;
         CoordinateReferenceSystem targetCRS;

         sourceCRS = CRSHelper.getCRS(source);
         targetCRS = CRSHelper.getCRS(target);

         MathTransform mathTransform = CRS.findMathTransform(sourceCRS,
               targetCRS, true);
         GeometryBuilder geometryBuilder = new GeometryBuilder(sourceCRS);
         return new CoordinateTransformerImpl(sourceCRS, targetCRS,
               mathTransform, geometryBuilder);

      } catch (Exception e)
      {
         String error = "Can't create CoordinateTransformer for convert "
               + source + " to " + target;
         LOGGER.error(error, e);
         throw new IndiciaTransformException(error, e);
      }
   }
}
