package org.infinitenature.werbeo.common.commons.coordinate;

@SuppressWarnings("serial")
public class IndiciaMismatchedDimensionException extends RuntimeException
{
   public IndiciaMismatchedDimensionException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public IndiciaMismatchedDimensionException(String message)
   {
      super(message);
   }
}
