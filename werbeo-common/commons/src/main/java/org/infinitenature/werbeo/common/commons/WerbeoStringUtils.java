package org.infinitenature.werbeo.common.commons;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class WerbeoStringUtils extends org.apache.commons.lang3.StringUtils
{
   public static String prefixEachLin(String text, String prefix)
   {
      StringBuilder result = new StringBuilder();
      try (BufferedReader reader = new BufferedReader(new StringReader(text)))
      {
         String line = reader.readLine();
         while (line != null)
         {
            result.append(prefix).append(line).append("\n");

            line = reader.readLine();
         }
      } catch (IOException exc)
      {
         // quit
      }
      return result.toString();
   }
   
   
   public static String encodeFileAndParams(String url)
   {
      String encoded;
      String stringToEncode = url.substring(url.lastIndexOf("/") + 1);
      try
      {
         encoded = URLEncoder.encode(stringToEncode, "utf-8");
      }
      catch(UnsupportedEncodingException e)
      {
         encoded = stringToEncode;
      }
      return url.replace(stringToEncode, encoded);
   }
   
   
   
}

