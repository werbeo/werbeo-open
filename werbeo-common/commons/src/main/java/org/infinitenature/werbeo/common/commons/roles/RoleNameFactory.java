package org.infinitenature.werbeo.common.commons.roles;

import org.apache.commons.lang3.Validate;

public class RoleNameFactory
{
   private static final String WERBEO_ROLE_PREFIX = "WERBEO_";

   private RoleNameFactory()
   {
      throw new IllegalAccessError("Utitlity class");
   }

   public static String getKeycloakRoleName(int portalId, Roles role)
   {
      Validate.inclusiveBetween(1, Integer.MAX_VALUE, portalId);
      Validate.notNull(role);

      return WERBEO_ROLE_PREFIX + portalId + "_" + role.getRoleName();
   }

   public static String getKeycloakRoleName(int portalId, String role)
   {
      Validate.inclusiveBetween(1, Integer.MAX_VALUE, portalId);
      Validate.notNull(role);

      return WERBEO_ROLE_PREFIX + portalId + "_" + role;
   }

   public static String getInternalRoleName(int portalId, String role)
   {
      Validate.inclusiveBetween(1, Integer.MAX_VALUE, portalId);
      Validate.notNull(role);

      return role.replace(WERBEO_ROLE_PREFIX + portalId + "_", "");
   }
}
