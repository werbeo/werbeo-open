package org.infinitenature.werbeo.common.commons.roles;

public enum Roles
{
   APPROVED("APPROVED"), ADMIN("ADMIN"), ACCEPTED("ACCEPTED"), VALIDATOR(
         "VALIDATOR");

   private final String roleName;

   private Roles(String roleName)
   {
      this.roleName = roleName;
   }

   public String getRoleName()
   {
      return roleName;
   }

}
