package org.infinitenature.werbeo.common.commons.coordinate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CoordinateFormatter
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CoordinateFormatter.class);

   public static String format(int epsg, double easting, double northing)
   {
      return format(epsg, northing) + ", " + format(epsg, easting);
   }

   public static String formatWithSpace(int epsg, double easting, double northing)
   {
      return format(epsg, northing) + " " + format(epsg, easting);
   }

   public static String format(int epsg, double coordinate)
   {
      String format;
      if (epsg == 25833 || epsg == 31468)
      {
         format = "%7.0f";
      } else if (epsg == 4326 || epsg == 3398 || epsg == 3857)
      {
         format = "%8.6f";
      } else
      {
         LOGGER.error("Error formatting coordinates, EPSG = {} not recognized", epsg);
         format = "%f";
      }

      return (String.format(format, coordinate)).replaceAll(",", ".");
   }




}
