package org.infinitenature.werbeo.service.common.logging;

public class LoggerFactory
{
   private LoggerFactory()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static Logger getLogger(Class<?> clazz)
   {
      return new Logger(org.slf4j.LoggerFactory.getLogger(clazz));
   }
}
