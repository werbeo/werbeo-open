package org.infinitenature.werbeo.service.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.support.CoordinateSystem;
import org.infinitenature.werbeo.service.core.api.enity.support.ExportPolicy;
import org.infinitenature.werbeo.service.core.api.enity.support.LayerName;
import org.infinitenature.werbeo.service.core.api.enity.support.ObfuscationPolicy;
import org.infinitenature.werbeo.service.core.api.enity.support.StaticMapStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "werbeo.service")
public class InstanceConfig
{
   public static class Piwik
   {
      /**
       * The piwik authorization token
       */
      private String authToken;
      /**
       * The piwik url
       */
      private String hostUrl;
      /**
       * The pwiki siteId
       */
      private int siteId;

      public String getAuthToken()
      {
         return authToken;
      }

      public void setAuthToken(String authToken)
      {
         this.authToken = authToken;
      }

      public String getHostUrl()
      {
         return hostUrl;
      }

      public void setHostUrl(String hostUrl)
      {
         this.hostUrl = hostUrl;
      }

      public int getSiteId()
      {
         return siteId;
      }

      public void setSiteId(int siteId)
      {
         this.siteId = siteId;
      }
   }

   public static class KeycloakAdmin
   {
      private String user;
      private String password;
      private String realm;
      private String clientId;

      public String getUser()
      {
         return user;
      }

      public void setUser(String user)
      {
         this.user = user;
      }

      public String getPassword()
      {
         return password;
      }

      public void setPassword(String password)
      {
         this.password = password;
      }

      public String getRealm()
      {
         return realm;
      }

      public void setRealm(String realm)
      {
         this.realm = realm;
      }

      public String getClientId()
      {
         return clientId;
      }

      public void setClientId(String clientId)
      {
         this.clientId = clientId;
      }

   }

   private Piwik piwik;

   public Piwik getPiwik()
   {
      return piwik;
   }

   public void setPiwik(Piwik piwik)
   {
      this.piwik = piwik;
   }

   private KeycloakAdmin keycloakAdmin;

   public KeycloakAdmin getKeycloakAdmin()
   {
      return keycloakAdmin;
   }

   public void setKeycloakAdmin(KeycloakAdmin keycloak)
   {
      this.keycloakAdmin = keycloak;
   }

   /**
    * The base URL where the static maps can be loaded from
    */
   private String staticMapsURL = "https://loe3.loe.auf.uni-rostock.de/maps";

   /**
    * The base URL of the local media
    */
   private String localMediaURL = "https://loe3.loe.auf.uni-rostock.de/indicia/upload";
   /**
    * The data directory of the service
    */
   private String dir;

   private boolean useTaxonCache = true;
   /**
    * The maximum size of occurrence area filter in km²
    */
   private double areaFilterMaxSize = 100;

   public String getDir()
   {
      return dir;
   }

   public void setDir(String dir)
   {
      this.dir = dir;
   }

   /**
    * The default data entry survey per portal
    */
   private Map<Integer, Integer> defaultDataEntrySurveys = new HashMap<>();

   /**
    * The available taxa list per portal
    */
   private Map<Integer, Set<Integer>> taxonListsPerPortal = new HashMap<>();

   /**
    * The available taxa groups per portal
    * <p>
    * If not declared all all groups which are available will be delivered
    */
   private Map<Integer, Set<String>> taxonGroupsPerPortal = new HashMap<>();

   private int importerThreads = 2;

   public Map<Integer, Set<String>> getTaxonGroupsPerPortal()
   {
      return taxonGroupsPerPortal;
   }

   public void setTaxonGroupsPerPortal(
         Map<Integer, Set<String>> taxaGroupsPerPortal)
   {
      this.taxonGroupsPerPortal = taxaGroupsPerPortal;
   }

   /**
    * The sytle of the static maps per portal
    */
   private Map<Integer, StaticMapStyle> staticMapStyle = new HashMap<>();

   public Map<Integer, StaticMapStyle> getStaticMapStyle()
   {
      return staticMapStyle;
   }

   public void setStaticMapStyle(Map<Integer, StaticMapStyle> staticMapStyle)
   {
      this.staticMapStyle = staticMapStyle;
   }

   public int getImporterThreads()
   {
      return importerThreads;
   }

   /**
    * The number of threads used for the importer
    *
    * @param importerThreads
    */
   public void setImporterThreads(int importerThreads)
   {
      this.importerThreads = importerThreads;
   }

   public Map<Integer, Integer> getDefaultDataEntrySurveys()
   {
      return defaultDataEntrySurveys;
   }

   public void setDefaultDataEntrySurveys(
         Map<Integer, Integer> defaultDataEntrySurveys)
   {
      this.defaultDataEntrySurveys = defaultDataEntrySurveys;
   }

   public String getStaticMapsURL()
   {
      return staticMapsURL;
   }

   public void setStaticMapsURL(String staticMapsURL)
   {
      this.staticMapsURL = staticMapsURL;
   }

   public Map<Integer, Set<Integer>> getTaxonListsPerPortal()
   {
      return taxonListsPerPortal;
   }

   public void setTaxonListsPerPortal(
         Map<Integer, Set<Integer>> taxonListPerPortal)
   {
      this.taxonListsPerPortal = taxonListPerPortal;
   }

   public String getLocalMediaURL()
   {
      return localMediaURL;
   }

   public void setLocalMediaURL(String localMediaURL)
   {
      this.localMediaURL = localMediaURL;
   }

   private Map<Integer, Double> mapInitialZoom = new HashMap<>();
   private Map<Integer, Double> mapInitialLatitude = new HashMap<>();
   private Map<Integer, Double> mapInitialLongitude = new HashMap<>();
   private Map<Integer, List<LayerName>> mapOverlayLayersPerPortal = new HashMap<>();

   /**
    * Maximum file upload size per portal in MB. If no value is given 3MB is the
    * default
    */
   private Map<Integer, Integer> maxUploadSize = new HashMap<>();

   public Map<Integer, Double> getMapInitialZoom()
   {
      return mapInitialZoom;
   }

   public void setMapInitialZoom(Map<Integer, Double> mapInitialZoom)
   {
      this.mapInitialZoom = mapInitialZoom;
   }

   public Map<Integer, Double> getMapInitialLatitude()
   {
      return mapInitialLatitude;
   }

   public void setMapInitialLatitude(Map<Integer, Double> mapInitialLatitude)
   {
      this.mapInitialLatitude = mapInitialLatitude;
   }

   public Map<Integer, Double> getMapInitialLongitude()
   {
      return mapInitialLongitude;
   }

   public void setMapInitialLongitude(Map<Integer, Double> mapInitialLongitude)
   {
      this.mapInitialLongitude = mapInitialLongitude;
   }

   public Map<Integer, List<LayerName>> getMapOverlayLayersPerPortal()
   {
      return mapOverlayLayersPerPortal;
   }

   public void setMapOverlayLayersPerPortal(
         Map<Integer, List<LayerName>> mapOverlayLayersPerPortal)
   {
      this.mapOverlayLayersPerPortal = mapOverlayLayersPerPortal;
   }

   public Map<Integer, Integer> getMaxUploadSize()
   {
      return maxUploadSize;
   }

   public void setMaxUploadSize(Map<Integer, Integer> maxUploadSize)
   {
      this.maxUploadSize = maxUploadSize;
   }

   public boolean isUseTaxonCache()
   {
      return useTaxonCache;
   }

   public void setUseTaxonCache(boolean useTaxonCache)
   {
      this.useTaxonCache = useTaxonCache;
   }

   private Map<Integer, List<ObfuscationPolicy>> obfuscationPoliciesPerPortal = new HashMap<>();
   private Map<Integer, List<ExportPolicy>> exportPoliciesPerPortal = new HashMap<>();

   public Map<Integer, List<ObfuscationPolicy>> getObfuscationPoliciesPerPortal()
   {
      return obfuscationPoliciesPerPortal;
   }

   public void setObfuscationPoliciesPerPortal(
         Map<Integer, List<ObfuscationPolicy>> obfuscationPoliciesPerPortal)
   {
      this.obfuscationPoliciesPerPortal = obfuscationPoliciesPerPortal;
   }

   public void setExportPoliciesPerPortal(
         Map<Integer, List<ExportPolicy>> exportPoliciesPerPortal)
   {
      this.exportPoliciesPerPortal = exportPoliciesPerPortal;
   }

   public Map<Integer, List<ExportPolicy>> getExportPoliciesPerPortal()
   {
      return exportPoliciesPerPortal;
   }

   /**
    * Set to true if for a portal anonymous access is allowed. If not set no
    * anonymous access is allowed.
    */
   private Map<Integer, Boolean> anonymousAccessAllowedPerPortal = new HashMap<>();

   public Map<Integer, Boolean> getAnonymousAccessAllowedPerPortal()
   {
      return anonymousAccessAllowedPerPortal;
   }

   public void setAnonymousAccessAllowedPerPortal(
         Map<Integer, Boolean> anonymousAccessAllowedPerPortal)
   {
      this.anonymousAccessAllowedPerPortal = anonymousAccessAllowedPerPortal;
   }

   public double getAreaFilterMaxSize()
   {
      return areaFilterMaxSize;
   }

   public void setAreaFilterMaxSize(double areaFilterMaxSize)
   {
      this.areaFilterMaxSize = areaFilterMaxSize;
   }

   private Map<Integer, List<CoordinateSystem>> coordinateSystemsPerPortal = new HashMap<>();

   public Map<Integer, List<CoordinateSystem>> getCoordinateSystemsPerPortal()
   {
      return coordinateSystemsPerPortal;
   }

   public void setCoordinateSystemsPerPortal(
         Map<Integer, List<CoordinateSystem>> coordinateSystemsPerPortal)
   {
      this.coordinateSystemsPerPortal = coordinateSystemsPerPortal;
   }

}
