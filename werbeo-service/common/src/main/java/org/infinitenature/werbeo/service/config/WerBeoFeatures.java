package org.infinitenature.werbeo.service.config;

import org.togglz.core.Feature;
import org.togglz.core.annotation.EnabledByDefault;
import org.togglz.core.annotation.Label;

public enum WerBeoFeatures implements Feature
{
   @Label("Use occurrences search index")
   @EnabledByDefault
   USE_OCCURRENCE_INDEX,

   @Label("Build the occurrence search index")
   @EnabledByDefault
   BUILD_OCCURRENCE_INDEX,

   @Label("Clean the index")
   CLEAN_INDEX,

   @Label("Update index on save")
   @EnabledByDefault
   UPDATE_INDEX_ON_SAVE,

   @Label("Show free data")
   @EnabledByDefault
   SHOW_FREE_DATA,

   @Label("Log request and response on non 2xx HTTP Requests")
   LOG_REQUESTS_ON_ERROR,

   @Label("Allow get taxa from not configured lists")
   ALLOW_GET_ALL_TAXA,

   @Label("Support old qunatiy field next to numeric ammount. #914")
   @EnabledByDefault
   LEGACY_QUANTITY_SUPPORT,

   @Label("Support for validation field in frontend. #848")
   @EnabledByDefault
   VALIDATION_FIELD,

   @Label("Track rest service calls")
   @EnabledByDefault
   TRACK_REST_SERVICE_CALLS
}
