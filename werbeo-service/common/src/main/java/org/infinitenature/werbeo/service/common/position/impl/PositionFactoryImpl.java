package org.infinitenature.werbeo.service.common.position.impl;

import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.Position.PositionType;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.opengis.geometry.DirectPosition;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component
public class PositionFactoryImpl implements PositionFactory
{

   private GeometryHelper geometryHelper = new GeometryHelper();

   private CoordinateTransformerFactory coordinateTransformerFactory = new CoordinateTransformerFactory();

   @Override
   public Position create(String wellKnownText, int srid)
   {
      Position position = new Position();
      if (wellKnownText.contains("POINT"))
      {
         position.setType(PositionType.POINT);
      } else
      {
         position.setType(PositionType.SHAPE);
      }
      position.setWkt(wellKnownText);
      position.setEpsg(srid);
      position.setWktEpsg(srid);
      position.setMtb(geometryHelper.getCenterMTB(wellKnownText, srid,
            coordinateTransformerFactory.getCoordinateTransformer(srid,
                  MTB.getEpsgNumber())));
      DirectPosition center = geometryHelper.getCenter(wellKnownText, srid);
      position.setPosCenter(center.getCoordinate());
      return position;
   }

   @Override
   public Position createShapeType(Position position)
   {
      position.setType(PositionType.SHAPE);
      return position;
   }

   @Override
   public Position create(MTB mtb)
   {
      Position position = new Position();
      position.setMtb(mtb);
      position.setType(PositionType.MTB);
      position.setWkt(mtb.toWkt());
      position.setEpsg(MTB.getEpsgNumber());
      position.setWktEpsg(MTB.getEpsgNumber());
      position.setPosCenter(new double[] { mtb.getCenter()[0].toDouble(),
            mtb.getCenter()[1].toDouble() });
      return position;
   }

   @Override
   public Position createFromMTB(String mtb)
   {
      return create(MTBHelper.toMTB(mtb));
   }

   @Override
   public Position create(double easting, double northing, int srid)
   {
      Position position = new Position();
      position.setType(PositionType.POINT);
      position.setEpsg(srid);
      position.setPosCenter(new double[] { easting, northing });
      CoordinateTransformer coordinateTransformer = coordinateTransformerFactory
            .getCoordinateTransformer(srid, MTB.getEpsgNumber());
      double[] converted = coordinateTransformer
            .convert(new double[] { easting, northing });
      position.setMtb(MTBHelper
            .toMTB(MTBHelper.fromCentroid(converted[0], converted[1])));
      position.setWktEpsg(srid);
      position.setWkt("POINT(" + easting + " " + northing + ")");
      return position;
   }

   /**
    *
    * Create Position depending of geoLocCode possible geoLocCodes:DE MV BB BE
    * SN ST TH BY BW HE RP NI NW SH HB HH SL
    *
    *
    */
   @Override
   public Position create(String geoLocCode)
   {
      // TODO: get shape for
      return null;
   }

}