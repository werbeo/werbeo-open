package org.infinitenature.werbeo.service.common.position;

import java.io.Serializable;

import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infintenature.mtb.MTB;

/**
 * Factory to create position objects from a variety of sources
 *
 * @author dve
 *
 */
public interface PositionFactory extends Serializable
{
   /**
    * Creates a position object from a coordinate
    *
    * @param easting
    * @param northing
    * @param srid
    * @return
    */
   public Position create(double easting, double northing, int srid);

   /**
    * Creates a position object from a wellKnownText string
    *
    * @param wellKnownText
    * @param srid
    * @return
    */
   public Position create(String wellKnownText, int srid);

   /**
    * Creates a position object from a MTB
    *
    * @param mtb
    * @return
    */
   public Position create(MTB mtb);

   /**
    * Creates a position object from a MTB string
    *
    * @param mtb
    * @return
    */
   public Position createFromMTB(String mtb);

   public Position create(String geoLocCode);

   Position createShapeType(Position position);
}
