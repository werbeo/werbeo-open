package org.infinitenature.spring.data.domain;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class OffsetPageable implements Pageable
{
   private Sort sort;
   private int offset;
   private int limit;

   public OffsetPageable()
   {
      super();
   }

   public OffsetPageable(int offset, int limit)
   {
      super();
      this.offset = offset;
      this.limit = limit;
      this.sort = Sort.by(Direction.ASC, "id");
   }

   @Override
   public int getPageNumber()
   {
      throw new IllegalArgumentException("Pages are not supported!");
   }

   @Override
   public int getPageSize()
   {
      return limit;
   }

   @Override
   public long getOffset()
   {
      return offset;
   }

   @Override
   public Sort getSort()
   {
      return sort;
   }

   @Override
   public Pageable next()
   {
      throw new IllegalArgumentException("Pages are not supported!");
   }

   @Override
   public Pageable previousOrFirst()
   {
      throw new IllegalArgumentException("Pages are not supported!");
   }

   @Override
   public Pageable first()
   {
      throw new IllegalArgumentException("Pages are not supported!");
   }

   @Override
   public boolean hasPrevious()
   {
      // TODO Auto-generated method stub
      return false;
   }

   public void setOffset(int offset)
   {
      this.offset = offset;
   }

   public void setLimit(int limit)
   {
      this.limit = limit;
   }

   public void setSort(Sort sort)
   {
      this.sort = sort;
   }
}
