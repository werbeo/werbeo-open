package org.infinitenature.werbeo.service.config;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@TestPropertySource(locations = "classpath:SettingsTest.properties")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "org.infinitenature.werbeo.service.config" })
@Configuration
public class TestInstanceConfig
{

   @Autowired
   private InstanceConfig instanceConfig;

   @Test
   public void test_taxonListsPerPortal()
   {
      assertThat(instanceConfig.getTaxonListsPerPortal().get(5), hasSize(2));
      assertThat(instanceConfig.getTaxonListsPerPortal().get(5),
            containsInAnyOrder(1, 2));
   }

   @Test
   public void test_dataEntrySurveys()
   {
      assertThat(instanceConfig.getDefaultDataEntrySurveys().get(3), is(7));
   }

   @Test
   public void test_getDir()
   {
      assertThat(instanceConfig.getDir(), is("/tmp"));
   }

}
