package org.infinitenature.service.common.coordinate;

import org.geotools.geometry.jts.*;
import org.geotools.referencing.*;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.junit.*;
import org.opengis.referencing.operation.*;

import org.locationtech.jts.geom.*;
import org.locationtech.jts.io.*;

public class TestJTS
{
   @Test
   public void testJTS() throws Exception
   {
      CoordinateTransformerFactory coordinateTransformerFactory = new CoordinateTransformerFactory();
      org.locationtech.jts.io.WKTReader wktReader = new org.locationtech.jts.io.WKTReader(
	    new GeometryFactory(new PrecisionModel(PrecisionModel.FLOATING),
		  900913));
      Geometry geom = wktReader
	    .read("MULTIPOLYGON (((1427388 6955132, 1427368 6955124, 1427350 6955122, 1427333 6955129, 1427320 6955146, 1427312 6955180, 1427319 6955226, 1427342 6955254, 1427356 6955260, 1427392 6955260, 1427405 6955252, 1427413 6955239, 1427428 6955219, 1427432 6955189, 1427427 6955165, 1427405 6955144, 1427388 6955132)))");

      CoordinateTransformer ct = coordinateTransformerFactory
	    .getCoordinateTransformer(900913, 4326);

      MathTransform mt = CRS.findMathTransform(ct.getSourceCRS(),
	    ct.getTargetCRS(), false);
      Geometry transformed = JTS.transform(geom, mt);
      WKTWriter wktWriter = new WKTWriter();
      String wkt = wktWriter.write(transformed);
      System.out.println(wkt);
   }
}
