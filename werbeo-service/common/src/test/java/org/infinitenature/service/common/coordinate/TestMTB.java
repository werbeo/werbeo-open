package org.infinitenature.service.common.coordinate;

import java.util.Collection;

import org.geotools.geometry.GeometryBuilder;
import org.geotools.geometry.iso.text.WKTParser;
import org.geotools.referencing.CRS;
import org.junit.Test;
import org.opengis.geometry.Boundary;
import org.opengis.geometry.Geometry;
import org.opengis.geometry.coordinate.LineString;
import org.opengis.geometry.coordinate.PointArray;
import org.opengis.geometry.coordinate.Position;
import org.opengis.geometry.primitive.Curve;
import org.opengis.geometry.primitive.Primitive;
import org.opengis.geometry.primitive.SurfaceBoundary;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

public class TestMTB
{

   @Test
   public void test() throws Exception
   {
      String mtb1234_1wkt = "POLYGON((1261620.89565706 7313496.60798383,1261620.89565706 7323146.54457674,1270897.51988991 7323146.54457674,1270897.51988991 7313496.60798383,1261620.89565706 7313496.60798383))";
      GeometryBuilder gb = new GeometryBuilder("EPSG:900913");
      WKTParser parser = new WKTParser(gb);
      Geometry mtb1234_1 = parser.parse(mtb1234_1wkt);
      CoordinateReferenceSystem crs31468 = CRS.decode("EPSG:31468");
      Geometry mtb1234_1_31468 = mtb1234_1.transform(crs31468);
      System.out.println(mtb1234_1);
      System.out.println(mtb1234_1_31468.toString().replace(",", ",\n"));
      Boundary boundary = mtb1234_1_31468.getBoundary();
      System.out.println(boundary);
      SurfaceBoundary ls = (SurfaceBoundary) boundary;
      Collection col = ls.getExterior().getElements();
      for (Object o : col)
      {
	 Primitive p = (Primitive) o;
	 Curve c = (Curve) p;
	 LineString ls1 = c.asLineString(0, 0);

	 PointArray pa = ls1.getControlPoints();
	 System.out.println(pa.get(0));
	 System.out.println(pa.get(1));
	 System.out.println(pa.get(2));
	 System.out.println(pa.get(3));
	 for (Position position : pa)
	 {
	    System.out.println(position.getDirectPosition());
	 }
      }
   }

}
