package org.infinitenature.service.common.coordinate;

import org.geotools.geometry.GeometryBuilder;
import org.geotools.referencing.CRS;
import org.junit.Before;
import org.junit.Test;
import org.opengis.geometry.DirectPosition;
import org.opengis.geometry.Geometry;
import org.opengis.geometry.primitive.Point;
import org.opengis.referencing.crs.CRSAuthorityFactory;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

public class Test900913
{

   @Before
   public void setUp() throws Exception
   {
   }

   @Test
   public void test900913() throws Exception
   {
      CRSAuthorityFactory factory = CRS.getAuthorityFactory(true);
      CoordinateReferenceSystem epsg900913 = factory
	    .createCoordinateReferenceSystem("EPSG:900913");
      CoordinateReferenceSystem epsg31468 = factory
	    .createCoordinateReferenceSystem("EPSG:31468");
      boolean lenient = false;
      MathTransform transform31468to900913 = CRS.findMathTransform(epsg31468,
	    epsg900913, lenient);
      MathTransform transform900913to31468 = transform31468to900913.inverse();

      GeometryBuilder builder900913 = new GeometryBuilder(epsg900913);

      DirectPosition pos = builder900913.createDirectPosition(new double[] {
	    1374825.07630716, 7089578.35749279 });
      System.out.println(transform900913to31468.transform(pos, null));

      org.geotools.geometry.iso.io.wkt.WKTReader wktReader = new org.geotools.geometry.iso.io.wkt.WKTReader(
	    epsg900913);
      Geometry geomtry = wktReader
	    .read("POINT(1216468.38575871 7089730.33588233)");
      System.out.println(geomtry);
      Point point = (Point) geomtry;
      pos = point.getDirectPosition();
      System.out.println(pos);

   }
}
