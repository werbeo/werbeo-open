package org.infinitenature.service.common.coordinate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.common.position.impl.PositionFactoryImpl;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.Position.PositionType;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.junit.Before;
import org.junit.Test;

public class PositionFactoryImplTest
{
   private PositionFactory positionFactory;

   @Before
   public void setUp()
   {
      PositionFactoryImpl impl = new PositionFactoryImpl();
      positionFactory = impl;
   }

   @Test
   public void testCreateStringInt()
   {
      MTB mtb = MTBHelper.toMTB("1920/123");

      Position position = positionFactory.create(mtb.toWkt(), 4745);
      assertEquals(PositionType.SHAPE, position.getType());
      assertEquals(4745, position.getEpsg());
      assertEquals(4745, position.getWktEpsg());
      assertEquals(mtb.toWkt(), position.getWkt());
      assertEquals(mtb, position.getMtb());

   }

   @Test
   public void testCreateMTB() throws Exception
   {
      MTB mtb = MTBHelper.toMTB("1920/123");
      Position position = positionFactory.create(mtb);
      assertNotNull(position);
      assertEquals(mtb, position.getMtb());
      assertEquals(Position.PositionType.MTB, position.getType());
      assertEquals(mtb.toWkt(), position.getWkt());
      assertEquals(4314, position.getEpsg());
      assertEquals(4314, position.getWktEpsg());
      assertNotNull(position.getPosCenter());
   }

   @Test
   public void testCreateFromMTB()
   {
      MTB mtb = MTBHelper.toMTB("1920/123");
      Position position = positionFactory.createFromMTB("1920/123");
      assertNotNull(position);
      assertEquals(mtb, position.getMtb());
      assertEquals(Position.PositionType.MTB, position.getType());
      assertEquals(mtb.toWkt(), position.getWkt());
      assertEquals(4314, position.getEpsg());
      assertEquals(4314, position.getWktEpsg());
      assertNotNull(position.getPosCenter());
   }

   @Test
   public void testPointMTB()
   {
      Position point = positionFactory.create(13.392634, 54.100988, 4326);
      assertEquals("1846/343", point.getMtb().getMtb());
   }

   @Test
   public void testCreateDoubleDoubleInt()
   {
      Position point = positionFactory.create(5192989, 5933647, 31469);
      assertEquals("POINT(5192989.0 5933647.0)", point.getWkt());
      assertEquals(31469, point.getWktEpsg());
   }

}
