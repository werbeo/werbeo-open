werbeo-service
==============

This is the REST based service for werbeo.

Build and run
-------------
Build with:

```
$ mvn install
```

Run with: 

```
$ cd werbeo-service-impl
$ mvn spring-boot:run
```



Documentation
-------------
This service is documented via java doc in the `werbeo-service-api` module. You can access the docs and a swagger-ui at [localhost:8087](http://localhost:8087/doc/index.html), on the [test instance](https://service.test.infinitenature.org/doc/index.html) or at the [live instance](https://service.infinitenature.org/doc/index.html).