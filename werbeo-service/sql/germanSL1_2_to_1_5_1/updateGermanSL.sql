UPDATE o SET o.taxa_taxon_list = (SELECT id FROM detail_taxa_taxon_lists WHERE external_key = '6008' AND taxon_list_id = 4 AND language_iso  = 'lat')
FROM occurrences o JOIN taxa_taxon_lists ttl ON (o.taxa_taxon_list_id = ttl.id) JOIN taxa t ON (ttl.taxon_id = t.id) WHERE ttl.taxon_list_id = 1 AND t.external_key = '6008';

\copy (select distinct taxon_external_key from detail_occurrences where taxon_list_id = 1) to 'usedtaxa.csv' WITH CSV;


update
	occurrences 
set
	taxa_taxon_list_id = (
	select
		id
	from
		detail_taxa_taxon_lists
	where
		external_key = '6008'
		and taxon_list_id = 4
		and language_iso = 'lat')
where
	id in (
	select
		id
	from
		detail_occurrences docc
	where
		docc.taxon_list_id = 1
		and docc.taxon_external_key = '6008')