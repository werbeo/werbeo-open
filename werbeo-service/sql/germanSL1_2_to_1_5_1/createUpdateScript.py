import csv
from pygments.lexers.csound import newline
from reportlab.lib.PyFontify import idRE

idReplacment = {}
with open('usedtaxa-loe2.csv', newline='') as usedTaxaFile:
    usedTaxa = usedTaxaFile.readlines()
    for line in usedTaxa:
        idReplacment[line.strip()] = line.strip()
                
with open('germanSL1_2_to_1_5_1.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        idReplacment[row['Old_Nr']] = row['New_Nr']

sql = """
update
    occurrences 
set
    taxa_taxon_list_id = (
    select
        id
    from
        detail_taxa_taxon_lists
    where
        external_key = '{}'
        and taxon_list_id = 4
        and language_iso = 'lat'),
    updated_on = now() + id * interval '0.000001 second'
where
    id in (
    select
        id
    from
        detail_occurrences docc
    where
        docc.taxon_list_id = 1
        and docc.taxon_external_key = '{}'
        and docc.website_id = 2);
    """        

with open('update-loe2-flora-bb.sql', 'w') as outputFile:
    for oldId in idReplacment:  
        outputFile.write(sql.format(idReplacment[oldId],oldId))
