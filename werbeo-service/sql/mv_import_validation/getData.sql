select
	oav_uuid.text_value as uuid,
	d_o.external_key,
	d_o.taxon_external_key,
	d_o.taxon,
	surveys.title,
	d_o.date_start,
	d_o.date_end ,
	d_o.date_type ,
	oa2.term as vorkommensStatus,
	sav_fundort.text_value as fundort,
	d_o.id,
	string_agg(oc.comment, ' | ') as occurrence_comments,
	string_agg(sc.comment, ' | ') as sample_comments
from
	detail_occurrences d_o
left join occurrence_comments oc on
	(occurrence_id = d_o.id)
left join sample_comments sc on
	(sc.sample_id = d_o.sample_id)
join occurrence_attribute_values oav_uuid on
	(d_o.id = oav_uuid.occurrence_id
	and oav_uuid.occurrence_attribute_id = (
	select
		oa.id
	from
		occurrence_attributes oa
	where
		oa.caption = 'UUID'))
left join occurrence_attribute_values oav_vs on
	(d_o.id = oav_vs.occurrence_id
	and oav_vs.occurrence_attribute_id = (
	select
		oa.id
	from
		occurrence_attributes oa
	where
		oa.caption = 'VorkStatus'))
left join sample_attribute_values sav_fundort on
	(d_o.sample_id = sav_fundort.sample_id
	and sav_fundort.sample_attribute_id = (
	select
		sa.id
	from
		sample_attributes sa
	where
		sa.caption = 'Fundort'))
left join terms oa2 on
	(oav_vs.int_value = oa2.id)
join surveys on
	(d_o.survey_id = surveys.id)
where
	taxon_external_key = '1'
	and d_o.website_id = 4
group by
	oav_uuid.text_value,
	fundort ,
	vorkommensstatus ,
	surveys.title,
	d_o.date_start,
	d_o.date_end ,
	d_o.date_type ,
	d_o.external_key ,
	d_o.taxon_external_key ,
	d_o.taxon,
	d_o.id;
