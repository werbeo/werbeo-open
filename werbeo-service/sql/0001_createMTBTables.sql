--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: mtb; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA mtb;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: mtb; Type: TABLE; Schema: mtb; Owner: -
--

CREATE TABLE mtb.mtb (
    id character(8) NOT NULL,
    name varchar(254) NOT NULL,
    geom public.geometry NOT NULL,
    orig_geom public.geometry NOT NULL,
    CONSTRAINT enforce_dims_mtb_geom CHECK ((public.st_ndims(geom) = 2)),
    CONSTRAINT enforce_dims_mtb_orig_geom CHECK ((public.st_ndims(orig_geom) = 2)),
    CONSTRAINT enforce_srid_mtb_geom CHECK ((public.st_srid(geom) = 900913)),
    CONSTRAINT enforce_srid_mtb_orig_geom CHECK ((public.st_srid(orig_geom) = 4314))
);


--
-- Name: v_mtb; Type: VIEW; Schema: mtb; Owner: -
--

CREATE VIEW mtb.v_mtb AS
 SELECT mtb.id,
    mtb.geom
   FROM mtb.mtb
  WHERE (length(mtb.id) = 4);


--
-- Name: v_mtbq; Type: VIEW; Schema: mtb; Owner: -
--

CREATE VIEW mtb.v_mtbq AS
 SELECT mtb.id,
    mtb.geom
   FROM mtb.mtb
  WHERE (length(mtb.id) = 6);


--
-- Name: v_mtbqq; Type: VIEW; Schema: mtb; Owner: -
--

CREATE VIEW mtb.v_mtbqq AS
 SELECT mtb.id,
    mtb.geom
   FROM mtb.mtb
  WHERE (length(mtb.id) = 7);


--
-- Name: v_mtbqqq; Type: VIEW; Schema: mtb; Owner: -
--

CREATE VIEW mtb.v_mtbqqq AS
 SELECT mtb.id,
    mtb.geom
   FROM mtb.mtb
  WHERE (length(mtb.id) = 8);


--
-- Name: pk_mtb; Type: CONSTRAINT; Schema: mtb; Owner: -
--

ALTER TABLE ONLY mtb.mtb
    ADD CONSTRAINT pk_mtb PRIMARY KEY (id);



--
-- PostgreSQL database dump complete
--
