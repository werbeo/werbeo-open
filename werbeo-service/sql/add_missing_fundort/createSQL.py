import xml.etree.ElementTree as ET
from fileinput import filename
sql = """

--
-- UUID:    {uuid}
-- Fundort: {fundort}
--
INSERT INTO sample_attribute_values (
    sample_id,
    sample_attribute_id,
    text_value,
    created_on,
    created_by_id,
    updated_on,
    updated_by_id)
SELECT
    (
        SELECT
            sample_id
        FROM
            sample_attribute_values
        WHERE
            sample_attribute_id = (
                SELECT
                    id
                FROM
                    sample_attributes
                WHERE
                    caption = 'UUID')
                AND deleted = FALSE
                AND text_value = '{uuid}'),
        (
            SELECT
                id
            FROM
                sample_attributes
            WHERE
                caption = 'Fundort'), '{fundort}', now(), 1, now(), 1
WHERE
    NOT EXISTS (
        SELECT
            *
        FROM
            sample_attribute_values
        WHERE
            sample_id = (
                SELECT
                    sample_id
                FROM
                    sample_attribute_values
                WHERE
                    sample_attribute_id = (
                        SELECT
                            id
                        FROM
                            sample_attributes
                        WHERE
                            caption = 'UUID')
                        AND deleted = FALSE
                        AND text_value = '{uuid}')
                    AND sample_attribute_id = (
                        SELECT
                            id
                        FROM
                            sample_attributes
                        WHERE
                            caption = 'Fundort'))
                AND EXISTS (
                    SELECT
                        *
                    FROM
                        sample_attribute_values
                    WHERE
                        sample_attribute_id = (
                            SELECT
                                id
                            FROM
                                sample_attributes
                            WHERE
                                caption = 'UUID')
                            AND deleted = FALSE
                            AND text_value = '{uuid}');
                            
"""
files = ["CHARA_DATA-HERBAR", "CHARA_DATA-LITERATUR", "CHARA_DATA-ONLINE", "CHARA_DATA-OTHER",
         "HOEHERE_PFLANZEN_DATA-HERBAR", "HOEHERE_PFLANZEN_DATA-LITERATUR", "HOEHERE_PFLANZEN_DATA-ONLINE",
         "HOEHERE_PFLANZEN_DATA-OTHER", "HOEHERE_PFLANZEN_DATA-RASTER"]
total = 0
with open('update.sql', 'w') as outputFile:
    for filename in files:
        byFile = 0
        fullname = '/home/dve/git/flora-mv-alt/export/' + filename + '.xml'
        print(fullname)
        tree = ET.parse(fullname)
        root = tree.getroot()
        samples = root.find('{http://www.example.org/export}samples')
   
        for sample in samples.findall('{http://www.example.org/export}sample'):
            uuids = sample.find('{http://www.example.org/export}uuid').text
            fundorts = sample.find('{http://www.example.org/export}position').find('{http://www.example.org/export}description').text
            if fundorts is not None:
                outputFile.write(sql.format(uuid = uuids, fundort = fundorts.replace("\n", " ")))
                total += 1
                byFile += 1
        print(total, byFile)