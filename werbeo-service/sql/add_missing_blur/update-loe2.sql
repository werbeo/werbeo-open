

-- Global-ID 519314

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 519314');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 519314');


-- Global-ID 526936

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 526936');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 526936');


-- Global-ID 526966

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 526966');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 526966');


-- Global-ID 1929621

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 1929621');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 1929621');


-- Global-ID 1929622

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 1929622');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 1929622');


-- Global-ID 1929623

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 1929623');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 1929623');


-- Global-ID 2829956

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2829956');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2829956');


-- Global-ID 2829961

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2829961');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2829961');


-- Global-ID 2829962

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2829962');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2829962');


-- Global-ID 2830047

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2830047');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2830047');


-- Global-ID 2886924

UPDATE
    sample_attribute_values
SET
    int_value = 100,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2886924');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2886924');


-- Global-ID 2912872

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2912872');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2912872');


-- Global-ID 2928654

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2928654');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2928654');


-- Global-ID 2928656

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2928656');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2928656');


-- Global-ID 2934275

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934275');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934275');


-- Global-ID 2934276

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934276');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934276');


-- Global-ID 2934296

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934296');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934296');


-- Global-ID 2934997

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934997');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2934997');


-- Global-ID 2935942

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2935942');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2935942');


-- Global-ID 2966668

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966668');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966668');


-- Global-ID 2966773

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966773');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966773');


-- Global-ID 2966815

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966815');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966815');


-- Global-ID 2966819

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966819');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2966819');


-- Global-ID 2978364

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2978364');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2978364');


-- Global-ID 2979123

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979123');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979123');


-- Global-ID 2979128

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979128');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979128');


-- Global-ID 2979261

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979261');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979261');


-- Global-ID 2979263

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979263');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979263');


-- Global-ID 2979405

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979405');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2979405');


-- Global-ID 2980161

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2980161');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2980161');


-- Global-ID 2980162

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2980162');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2980162');


-- Global-ID 2981270

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2981270');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2981270');


-- Global-ID 2981271

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2981271');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2981271');


-- Global-ID 2981276

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2981276');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2981276');


-- Global-ID 2992277

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2992277');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2992277');


-- Global-ID 2992304

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2992304');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2992304');


-- Global-ID 2995703

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995703');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995703');


-- Global-ID 2995795

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995795');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995795');


-- Global-ID 2995796

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995796');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995796');


-- Global-ID 2995797

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995797');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2995797');


-- Global-ID 2997853

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2997853');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 2997853');


-- Global-ID 3009293

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009293');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009293');


-- Global-ID 3009313

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009313');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009313');


-- Global-ID 3009418

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009418');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009418');


-- Global-ID 3009422

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009422');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009422');


-- Global-ID 3009514

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009514');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009514');


-- Global-ID 3009613

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009613');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009613');


-- Global-ID 3009615

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009615');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009615');


-- Global-ID 3009619

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009619');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009619');


-- Global-ID 3009637

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009637');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3009637');


-- Global-ID 3010235

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010235');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010235');


-- Global-ID 3010252

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010252');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010252');


-- Global-ID 3010477

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010477');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010477');


-- Global-ID 3010479

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010479');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010479');


-- Global-ID 3010480

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010480');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010480');


-- Global-ID 3010481

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010481');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010481');


-- Global-ID 3010482

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010482');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010482');


-- Global-ID 3010491

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010491');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010491');


-- Global-ID 3010493

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010493');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010493');


-- Global-ID 3010494

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010494');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010494');


-- Global-ID 3010495

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010495');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010495');


-- Global-ID 3010498

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010498');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010498');


-- Global-ID 3010871

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010871');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010871');


-- Global-ID 3010872

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010872');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010872');


-- Global-ID 3010873

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010873');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010873');


-- Global-ID 3010874

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010874');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3010874');


-- Global-ID 3011043

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3011043');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3011043');


-- Global-ID 3011123

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3011123');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3011123');


-- Global-ID 3020742

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3020742');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3020742');


-- Global-ID 3022566

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022566');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022566');


-- Global-ID 3022568

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022568');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022568');


-- Global-ID 3022588

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022588');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022588');


-- Global-ID 3022592

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022592');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3022592');


-- Global-ID 3027211

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027211');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027211');


-- Global-ID 3027212

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027212');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027212');


-- Global-ID 3027259

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027259');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027259');


-- Global-ID 3027261

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027261');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027261');


-- Global-ID 3027262

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027262');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027262');


-- Global-ID 3027269

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027269');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027269');


-- Global-ID 3027275

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027275');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027275');


-- Global-ID 3027276

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027276');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027276');


-- Global-ID 3027342

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027342');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027342');


-- Global-ID 3027343

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027343');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027343');


-- Global-ID 3027437

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027437');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027437');


-- Global-ID 3027438

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027438');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027438');


-- Global-ID 3027515

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027515');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027515');


-- Global-ID 3027516

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027516');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027516');


-- Global-ID 3027518

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027518');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027518');


-- Global-ID 3027530

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027530');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027530');


-- Global-ID 3027982

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027982');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027982');


-- Global-ID 3027984

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027984');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027984');


-- Global-ID 3027985

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027985');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027985');


-- Global-ID 3027986

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027986');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027986');


-- Global-ID 3027996

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027996');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3027996');


-- Global-ID 3028007

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028007');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028007');


-- Global-ID 3028009

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028009');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028009');


-- Global-ID 3028010

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028010');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028010');


-- Global-ID 3028012

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028012');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028012');


-- Global-ID 3028014

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028014');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028014');


-- Global-ID 3028015

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028015');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028015');


-- Global-ID 3028016

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028016');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028016');


-- Global-ID 3028021

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028021');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028021');


-- Global-ID 3028032

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028032');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028032');


-- Global-ID 3028033

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028033');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028033');


-- Global-ID 3028034

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028034');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028034');


-- Global-ID 3028036

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028036');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028036');


-- Global-ID 3028037

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028037');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028037');


-- Global-ID 3028038

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028038');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028038');


-- Global-ID 3028039

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028039');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028039');


-- Global-ID 3028053

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028053');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028053');


-- Global-ID 3028057

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028057');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028057');


-- Global-ID 3028064

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028064');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028064');


-- Global-ID 3028065

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028065');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028065');


-- Global-ID 3028066

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028066');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028066');


-- Global-ID 3028148

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028148');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028148');


-- Global-ID 3028302

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028302');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028302');


-- Global-ID 3028303

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028303');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028303');


-- Global-ID 3028311

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028311');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3028311');


-- Global-ID 3030695

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030695');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030695');


-- Global-ID 3030696

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030696');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030696');


-- Global-ID 3030732

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030732');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030732');


-- Global-ID 3030735

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030735');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3030735');


-- Global-ID 3043312

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043312');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043312');


-- Global-ID 3043315

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043315');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043315');


-- Global-ID 3043316

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043316');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043316');


-- Global-ID 3043337

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043337');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043337');


-- Global-ID 3043385

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043385');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043385');


-- Global-ID 3043781

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043781');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043781');


-- Global-ID 3043782

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043782');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043782');


-- Global-ID 3043784

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043784');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043784');


-- Global-ID 3043786

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043786');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043786');


-- Global-ID 3043833

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043833');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043833');


-- Global-ID 3043873

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043873');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043873');


-- Global-ID 3043911

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043911');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043911');


-- Global-ID 3043912

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043912');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3043912');


-- Global-ID 3044072

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044072');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044072');


-- Global-ID 3044074

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044074');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044074');


-- Global-ID 3044075

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044075');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044075');


-- Global-ID 3044076

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044076');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044076');


-- Global-ID 3044077

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044077');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044077');


-- Global-ID 3044078

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044078');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044078');


-- Global-ID 3044079

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044079');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044079');


-- Global-ID 3044081

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044081');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044081');


-- Global-ID 3044083

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044083');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044083');


-- Global-ID 3044084

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044084');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044084');


-- Global-ID 3044085

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044085');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044085');


-- Global-ID 3044086

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044086');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3044086');


-- Global-ID 3851315

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851315');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851315');


-- Global-ID 3851316

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851316');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851316');


-- Global-ID 3851318

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851318');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851318');


-- Global-ID 3851320

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851320');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851320');


-- Global-ID 3851393

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851393');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851393');


-- Global-ID 3851477

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851477');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851477');


-- Global-ID 3851695

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851695');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3851695');


-- Global-ID 3855072

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3855072');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3855072');


-- Global-ID 3855314

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3855314');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3855314');


-- Global-ID 3855865

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3855865');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3855865');


-- Global-ID 3856131

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856131');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856131');


-- Global-ID 3856132

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856132');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856132');


-- Global-ID 3856137

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856137');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856137');


-- Global-ID 3856138

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856138');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856138');


-- Global-ID 3856147

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856147');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856147');


-- Global-ID 3856210

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856210');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856210');


-- Global-ID 3856248

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856248');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856248');


-- Global-ID 3856270

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856270');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856270');


-- Global-ID 3856274

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856274');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856274');


-- Global-ID 3856275

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856275');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856275');


-- Global-ID 3856280

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856280');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856280');


-- Global-ID 3856282

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856282');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856282');


-- Global-ID 3856283

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856283');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856283');


-- Global-ID 3856284

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856284');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856284');


-- Global-ID 3856285

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856285');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856285');


-- Global-ID 3856286

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856286');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856286');


-- Global-ID 3856353

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856353');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3856353');


-- Global-ID 3870699

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870699');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870699');


-- Global-ID 3870701

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870701');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870701');


-- Global-ID 3870704

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870704');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870704');


-- Global-ID 3870705

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870705');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870705');


-- Global-ID 3870706

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870706');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870706');


-- Global-ID 3870707

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870707');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870707');


-- Global-ID 3870709

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870709');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870709');


-- Global-ID 3870710

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870710');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870710');


-- Global-ID 3870711

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870711');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870711');


-- Global-ID 3870714

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870714');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870714');


-- Global-ID 3870715

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870715');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870715');


-- Global-ID 3870716

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870716');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870716');


-- Global-ID 3870717

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870717');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870717');


-- Global-ID 3870718

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870718');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870718');


-- Global-ID 3870719

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870719');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870719');


-- Global-ID 3870722

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870722');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870722');


-- Global-ID 3870723

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870723');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870723');


-- Global-ID 3870724

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870724');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870724');


-- Global-ID 3870725

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870725');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870725');


-- Global-ID 3870726

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870726');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870726');


-- Global-ID 3870727

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870727');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870727');


-- Global-ID 3870728

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870728');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870728');


-- Global-ID 3870729

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870729');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870729');


-- Global-ID 3870730

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870730');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870730');


-- Global-ID 3870731

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870731');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870731');


-- Global-ID 3870732

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870732');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870732');


-- Global-ID 3870736

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870736');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870736');


-- Global-ID 3870737

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870737');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870737');


-- Global-ID 3870738

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870738');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870738');


-- Global-ID 3870739

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870739');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870739');


-- Global-ID 3870740

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870740');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870740');


-- Global-ID 3870741

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870741');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870741');


-- Global-ID 3870742

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870742');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870742');


-- Global-ID 3870743

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870743');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870743');


-- Global-ID 3870744

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870744');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870744');


-- Global-ID 3870748

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870748');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870748');


-- Global-ID 3870751

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870751');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870751');


-- Global-ID 3870752

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870752');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870752');


-- Global-ID 3870754

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870754');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870754');


-- Global-ID 3870755

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870755');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3870755');


-- Global-ID 3871156

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871156');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871156');


-- Global-ID 3871157

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871157');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871157');


-- Global-ID 3871158

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871158');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871158');


-- Global-ID 3871159

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871159');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871159');


-- Global-ID 3871160

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871160');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871160');


-- Global-ID 3871162

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871162');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871162');


-- Global-ID 3871163

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871163');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871163');


-- Global-ID 3871166

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871166');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871166');


-- Global-ID 3871167

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871167');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871167');


-- Global-ID 3871381

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871381');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871381');


-- Global-ID 3871382

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871382');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871382');


-- Global-ID 3871383

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871383');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871383');


-- Global-ID 3871389

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871389');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871389');


-- Global-ID 3871409

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871409');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871409');


-- Global-ID 3871410

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871410');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871410');


-- Global-ID 3871413

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871413');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871413');


-- Global-ID 3871414

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871414');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871414');


-- Global-ID 3871416

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871416');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871416');


-- Global-ID 3871417

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871417');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871417');


-- Global-ID 3871418

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871418');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871418');


-- Global-ID 3871420

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871420');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3871420');


-- Global-ID 3873642

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3873642');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3873642');


-- Global-ID 3873643

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3873643');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3873643');


-- Global-ID 3874346

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3874346');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3874346');


-- Global-ID 3875092

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875092');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875092');


-- Global-ID 3875093

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875093');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875093');


-- Global-ID 3875094

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875094');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875094');


-- Global-ID 3875095

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875095');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875095');


-- Global-ID 3875096

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875096');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875096');


-- Global-ID 3875097

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875097');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875097');


-- Global-ID 3875098

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875098');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875098');


-- Global-ID 3875100

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875100');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875100');


-- Global-ID 3875101

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875101');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875101');


-- Global-ID 3875178

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875178');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875178');


-- Global-ID 3875179

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875179');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875179');


-- Global-ID 3875875

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875875');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875875');


-- Global-ID 3875880

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875880');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875880');


-- Global-ID 3875881

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875881');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875881');


-- Global-ID 3875882

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875882');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875882');


-- Global-ID 3875885

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875885');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3875885');


-- Global-ID 3876004

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876004');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876004');


-- Global-ID 3876005

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876005');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876005');


-- Global-ID 3876019

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876019');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876019');


-- Global-ID 3876020

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876020');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876020');


-- Global-ID 3876021

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876021');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876021');


-- Global-ID 3876022

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876022');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876022');


-- Global-ID 3876023

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876023');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876023');


-- Global-ID 3876024

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876024');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876024');


-- Global-ID 3876028

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876028');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876028');


-- Global-ID 3876032

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876032');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876032');


-- Global-ID 3876043

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876043');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876043');


-- Global-ID 3876044

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876044');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876044');


-- Global-ID 3876046

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876046');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876046');


-- Global-ID 3876048

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876048');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876048');


-- Global-ID 3876049

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876049');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876049');


-- Global-ID 3876054

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876054');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876054');


-- Global-ID 3876055

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876055');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876055');


-- Global-ID 3876148

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876148');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876148');


-- Global-ID 3876154

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876154');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876154');


-- Global-ID 3876155

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876155');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3876155');


-- Global-ID 3877428

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877428');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877428');


-- Global-ID 3877429

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877429');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877429');


-- Global-ID 3877431

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877431');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877431');


-- Global-ID 3877561

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877561');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877561');


-- Global-ID 3877562

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877562');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877562');


-- Global-ID 3877565

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877565');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877565');


-- Global-ID 3877566

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877566');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877566');


-- Global-ID 3877567

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877567');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877567');


-- Global-ID 3877568

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877568');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877568');


-- Global-ID 3877569

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877569');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877569');


-- Global-ID 3877648

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877648');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877648');


-- Global-ID 3877650

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877650');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877650');


-- Global-ID 3877653

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877653');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877653');


-- Global-ID 3877656

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877656');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877656');


-- Global-ID 3877657

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877657');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877657');


-- Global-ID 3877658

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877658');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877658');


-- Global-ID 3877659

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877659');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877659');


-- Global-ID 3877660

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877660');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877660');


-- Global-ID 3877742

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877742');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877742');


-- Global-ID 3877743

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877743');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877743');


-- Global-ID 3877745

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877745');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877745');


-- Global-ID 3877775

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877775');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877775');


-- Global-ID 3877776

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877776');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877776');


-- Global-ID 3877801

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877801');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877801');


-- Global-ID 3877802

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877802');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877802');


-- Global-ID 3877805

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877805');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877805');


-- Global-ID 3877807

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877807');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877807');


-- Global-ID 3877871

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877871');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877871');


-- Global-ID 3877938

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877938');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877938');


-- Global-ID 3877939

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877939');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877939');


-- Global-ID 3877942

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877942');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877942');


-- Global-ID 3877976

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877976');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877976');


-- Global-ID 3877977

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877977');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877977');


-- Global-ID 3877978

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877978');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877978');


-- Global-ID 3877979

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877979');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877979');


-- Global-ID 3877980

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877980');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877980');


-- Global-ID 3877981

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877981');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877981');


-- Global-ID 3877982

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877982');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3877982');


-- Global-ID 3878481

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878481');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878481');


-- Global-ID 3878482

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878482');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878482');


-- Global-ID 3878736

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878736');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878736');


-- Global-ID 3878739

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878739');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878739');


-- Global-ID 3878741

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878741');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878741');


-- Global-ID 3878742

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878742');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878742');


-- Global-ID 3878743

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878743');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878743');


-- Global-ID 3878897

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878897');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878897');


-- Global-ID 3878899

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878899');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878899');


-- Global-ID 3878901

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878901');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878901');


-- Global-ID 3878902

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878902');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878902');


-- Global-ID 3878919

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878919');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3878919');


-- Global-ID 3879037

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3879037');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3879037');


-- Global-ID 3879038

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3879038');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3879038');


-- Global-ID 3883017

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883017');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883017');


-- Global-ID 3883018

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883018');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883018');


-- Global-ID 3883021

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883021');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883021');


-- Global-ID 3883022

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883022');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883022');


-- Global-ID 3883023

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883023');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883023');


-- Global-ID 3883024

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883024');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883024');


-- Global-ID 3883030

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883030');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3883030');


-- Global-ID 3884834

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884834');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884834');


-- Global-ID 3884836

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884836');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884836');


-- Global-ID 3884837

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884837');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884837');


-- Global-ID 3884838

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884838');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884838');


-- Global-ID 3884840

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884840');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884840');


-- Global-ID 3884841

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884841');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884841');


-- Global-ID 3884843

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884843');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884843');


-- Global-ID 3884849

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884849');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884849');


-- Global-ID 3884850

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884850');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884850');


-- Global-ID 3884851

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884851');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884851');


-- Global-ID 3884852

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884852');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884852');


-- Global-ID 3884853

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884853');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884853');


-- Global-ID 3884854

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884854');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884854');


-- Global-ID 3884855

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884855');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884855');


-- Global-ID 3884856

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884856');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884856');


-- Global-ID 3884859

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884859');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884859');


-- Global-ID 3884860

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884860');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884860');


-- Global-ID 3884861

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884861');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884861');


-- Global-ID 3884863

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884863');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884863');


-- Global-ID 3884864

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884864');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884864');


-- Global-ID 3884865

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884865');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884865');


-- Global-ID 3884873

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884873');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884873');


-- Global-ID 3884874

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884874');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884874');


-- Global-ID 3884875

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884875');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884875');


-- Global-ID 3884876

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884876');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884876');


-- Global-ID 3884878

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884878');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884878');


-- Global-ID 3884879

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884879');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884879');


-- Global-ID 3884881

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884881');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884881');


-- Global-ID 3884882

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884882');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3884882');


-- Global-ID 3885021

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885021');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885021');


-- Global-ID 3885238

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885238');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885238');


-- Global-ID 3885239

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885239');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885239');


-- Global-ID 3885240

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885240');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885240');


-- Global-ID 3885241

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885241');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885241');


-- Global-ID 3885242

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885242');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885242');


-- Global-ID 3885243

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885243');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885243');


-- Global-ID 3885244

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885244');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885244');


-- Global-ID 3885245

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885245');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885245');


-- Global-ID 3885247

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885247');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885247');


-- Global-ID 3885248

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885248');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885248');


-- Global-ID 3885249

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885249');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885249');


-- Global-ID 3885254

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885254');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885254');


-- Global-ID 3885255

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885255');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885255');


-- Global-ID 3885259

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885259');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885259');


-- Global-ID 3885260

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885260');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885260');


-- Global-ID 3885262

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885262');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885262');


-- Global-ID 3885263

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885263');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885263');


-- Global-ID 3885264

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885264');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885264');


-- Global-ID 3885265

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885265');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885265');


-- Global-ID 3885266

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885266');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885266');


-- Global-ID 3885267

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885267');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885267');


-- Global-ID 3885268

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885268');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885268');


-- Global-ID 3885269

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885269');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885269');


-- Global-ID 3885270

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885270');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885270');


-- Global-ID 3885271

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885271');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885271');


-- Global-ID 3885272

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885272');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885272');


-- Global-ID 3885273

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885273');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885273');


-- Global-ID 3885274

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885274');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885274');


-- Global-ID 3885275

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885275');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885275');


-- Global-ID 3885276

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885276');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885276');


-- Global-ID 3885279

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885279');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885279');


-- Global-ID 3885282

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885282');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885282');


-- Global-ID 3885284

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885284');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885284');


-- Global-ID 3885285

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885285');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885285');


-- Global-ID 3885286

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885286');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885286');


-- Global-ID 3885288

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885288');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885288');


-- Global-ID 3885293

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885293');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885293');


-- Global-ID 3885294

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885294');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885294');


-- Global-ID 3885296

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885296');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885296');


-- Global-ID 3885297

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885297');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885297');


-- Global-ID 3885298

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885298');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885298');


-- Global-ID 3885299

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885299');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885299');


-- Global-ID 3885455

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885455');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885455');


-- Global-ID 3885456

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885456');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885456');


-- Global-ID 3885463

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885463');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885463');


-- Global-ID 3885537

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885537');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885537');


-- Global-ID 3885538

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885538');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885538');


-- Global-ID 3885672

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885672');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3885672');


-- Global-ID 3888118

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888118');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888118');


-- Global-ID 3888119

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888119');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888119');


-- Global-ID 3888122

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888122');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888122');


-- Global-ID 3888125

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888125');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888125');


-- Global-ID 3888126

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888126');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888126');


-- Global-ID 3888158

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888158');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888158');


-- Global-ID 3888187

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888187');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888187');


-- Global-ID 3888189

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888189');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888189');


-- Global-ID 3888198

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888198');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888198');


-- Global-ID 3888200

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888200');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888200');


-- Global-ID 3888201

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888201');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888201');


-- Global-ID 3888202

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888202');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888202');


-- Global-ID 3888207

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888207');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888207');


-- Global-ID 3888208

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888208');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888208');


-- Global-ID 3888209

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888209');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888209');


-- Global-ID 3888211

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888211');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888211');


-- Global-ID 3888212

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888212');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888212');


-- Global-ID 3888221

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888221');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888221');


-- Global-ID 3888238

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888238');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888238');


-- Global-ID 3888239

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888239');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888239');


-- Global-ID 3888240

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888240');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888240');


-- Global-ID 3888242

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888242');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888242');


-- Global-ID 3888243

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888243');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888243');


-- Global-ID 3888244

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888244');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888244');


-- Global-ID 3888245

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888245');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888245');


-- Global-ID 3888255

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888255');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888255');


-- Global-ID 3888256

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888256');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888256');


-- Global-ID 3888257

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888257');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888257');


-- Global-ID 3888258

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888258');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888258');


-- Global-ID 3888345

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888345');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3888345');


-- Global-ID 3896598

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896598');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896598');


-- Global-ID 3896599

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896599');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896599');


-- Global-ID 3896600

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896600');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896600');


-- Global-ID 3896601

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896601');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896601');


-- Global-ID 3896602

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896602');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896602');


-- Global-ID 3896603

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896603');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896603');


-- Global-ID 3896604

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896604');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896604');


-- Global-ID 3896650

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896650');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896650');


-- Global-ID 3896651

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896651');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896651');


-- Global-ID 3896653

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896653');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896653');


-- Global-ID 3896654

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896654');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896654');


-- Global-ID 3896655

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896655');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896655');


-- Global-ID 3896656

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896656');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896656');


-- Global-ID 3896657

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896657');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896657');


-- Global-ID 3896658

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896658');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896658');


-- Global-ID 3896659

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896659');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896659');


-- Global-ID 3896660

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896660');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896660');


-- Global-ID 3896662

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896662');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896662');


-- Global-ID 3896663

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896663');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896663');


-- Global-ID 3896664

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896664');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896664');


-- Global-ID 3896794

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896794');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896794');


-- Global-ID 3896796

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896796');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896796');


-- Global-ID 3896798

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896798');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896798');


-- Global-ID 3896800

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896800');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896800');


-- Global-ID 3896801

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896801');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896801');


-- Global-ID 3896892

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896892');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3896892');


-- Global-ID 3897105

UPDATE
    sample_attribute_values
SET
    int_value = 200,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897105');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897105');


-- Global-ID 3897263

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897263');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897263');


-- Global-ID 3897264

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897264');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897264');


-- Global-ID 3897265

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897265');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897265');


-- Global-ID 3897267

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897267');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897267');


-- Global-ID 3897270

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897270');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897270');


-- Global-ID 3897271

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897271');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897271');


-- Global-ID 3897272

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897272');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897272');


-- Global-ID 3897273

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897273');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3897273');


-- Global-ID 3899673

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899673');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899673');


-- Global-ID 3899674

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899674');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899674');


-- Global-ID 3899675

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899675');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899675');


-- Global-ID 3899676

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899676');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899676');


-- Global-ID 3899677

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899677');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899677');


-- Global-ID 3899678

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899678');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899678');


-- Global-ID 3899680

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899680');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899680');


-- Global-ID 3899684

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899684');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899684');


-- Global-ID 3899687

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899687');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899687');


-- Global-ID 3899689

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899689');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899689');


-- Global-ID 3899691

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899691');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899691');


-- Global-ID 3899692

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899692');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899692');


-- Global-ID 3899693

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899693');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899693');


-- Global-ID 3899694

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899694');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899694');


-- Global-ID 3899695

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899695');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899695');


-- Global-ID 3899696

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899696');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899696');


-- Global-ID 3899697

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899697');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899697');


-- Global-ID 3899925

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899925');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899925');


-- Global-ID 3899927

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899927');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899927');


-- Global-ID 3899928

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899928');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899928');


-- Global-ID 3899929

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899929');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899929');


-- Global-ID 3899931

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899931');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899931');


-- Global-ID 3899932

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899932');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899932');


-- Global-ID 3899933

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899933');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899933');


-- Global-ID 3899934

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899934');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3899934');


-- Global-ID 3900353

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3900353');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3900353');


-- Global-ID 3900607

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3900607');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3900607');


-- Global-ID 3910613

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910613');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910613');


-- Global-ID 3910614

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910614');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910614');


-- Global-ID 3910615

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910615');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910615');


-- Global-ID 3910616

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910616');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910616');


-- Global-ID 3910617

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910617');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910617');


-- Global-ID 3910618

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910618');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910618');


-- Global-ID 3910619

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910619');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910619');


-- Global-ID 3910620

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910620');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910620');


-- Global-ID 3910621

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910621');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910621');


-- Global-ID 3910622

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910622');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910622');


-- Global-ID 3910623

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910623');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910623');


-- Global-ID 3910624

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910624');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910624');


-- Global-ID 3910625

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910625');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910625');


-- Global-ID 3910626

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910626');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910626');


-- Global-ID 3910627

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910627');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910627');


-- Global-ID 3910628

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910628');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910628');


-- Global-ID 3910629

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910629');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910629');


-- Global-ID 3910630

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910630');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910630');


-- Global-ID 3910631

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910631');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910631');


-- Global-ID 3910632

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910632');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910632');


-- Global-ID 3910633

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910633');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910633');


-- Global-ID 3910634

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910634');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910634');


-- Global-ID 3910636

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910636');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910636');


-- Global-ID 3910637

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910637');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910637');


-- Global-ID 3910712

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910712');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910712');


-- Global-ID 3910713

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910713');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910713');


-- Global-ID 3910714

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910714');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910714');


-- Global-ID 3910715

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910715');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910715');


-- Global-ID 3910716

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910716');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910716');


-- Global-ID 3910718

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910718');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910718');


-- Global-ID 3910719

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910719');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910719');


-- Global-ID 3910720

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910720');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910720');


-- Global-ID 3910721

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910721');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910721');


-- Global-ID 3910722

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910722');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910722');


-- Global-ID 3910723

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910723');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910723');


-- Global-ID 3910725

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910725');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910725');


-- Global-ID 3910726

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910726');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910726');


-- Global-ID 3910728

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910728');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910728');


-- Global-ID 3910729

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910729');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910729');


-- Global-ID 3910730

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910730');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910730');


-- Global-ID 3910731

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910731');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910731');


-- Global-ID 3910732

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910732');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910732');


-- Global-ID 3910733

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910733');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910733');


-- Global-ID 3910734

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910734');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910734');


-- Global-ID 3910735

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910735');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910735');


-- Global-ID 3910736

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910736');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910736');


-- Global-ID 3910737

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910737');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910737');


-- Global-ID 3910738

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910738');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910738');


-- Global-ID 3910739

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910739');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910739');


-- Global-ID 3910740

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910740');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910740');


-- Global-ID 3910741

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910741');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910741');


-- Global-ID 3910743

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910743');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910743');


-- Global-ID 3910745

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910745');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910745');


-- Global-ID 3910746

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910746');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910746');


-- Global-ID 3910747

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910747');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910747');


-- Global-ID 3910748

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910748');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910748');


-- Global-ID 3910749

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910749');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910749');


-- Global-ID 3910750

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910750');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910750');


-- Global-ID 3910826

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910826');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910826');


-- Global-ID 3910828

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910828');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910828');


-- Global-ID 3910830

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910830');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910830');


-- Global-ID 3910832

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910832');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910832');


-- Global-ID 3910834

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910834');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910834');


-- Global-ID 3910847

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910847');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910847');


-- Global-ID 3910850

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910850');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910850');


-- Global-ID 3910860

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910860');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910860');


-- Global-ID 3910862

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910862');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910862');


-- Global-ID 3910878

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910878');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910878');


-- Global-ID 3910880

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910880');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910880');


-- Global-ID 3910882

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910882');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910882');


-- Global-ID 3910885

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910885');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910885');


-- Global-ID 3910888

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910888');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910888');


-- Global-ID 3910889

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910889');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910889');


-- Global-ID 3910891

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910891');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910891');


-- Global-ID 3910894

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910894');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910894');


-- Global-ID 3910897

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910897');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910897');


-- Global-ID 3910899

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910899');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910899');


-- Global-ID 3910900

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910900');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910900');


-- Global-ID 3910903

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910903');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910903');


-- Global-ID 3910904

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910904');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910904');


-- Global-ID 3910905

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910905');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910905');


-- Global-ID 3910906

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910906');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910906');


-- Global-ID 3910908

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910908');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910908');


-- Global-ID 3910909

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910909');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910909');


-- Global-ID 3910910

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910910');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910910');


-- Global-ID 3910912

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910912');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910912');


-- Global-ID 3910989

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910989');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910989');


-- Global-ID 3910990

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910990');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910990');


-- Global-ID 3910991

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910991');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910991');


-- Global-ID 3910993

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910993');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910993');


-- Global-ID 3910995

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910995');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910995');


-- Global-ID 3910996

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910996');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910996');


-- Global-ID 3910997

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910997');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3910997');


-- Global-ID 3911000

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911000');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911000');


-- Global-ID 3911001

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911001');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911001');


-- Global-ID 3911004

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911004');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911004');


-- Global-ID 3911005

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911005');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911005');


-- Global-ID 3911006

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911006');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911006');


-- Global-ID 3911007

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911007');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911007');


-- Global-ID 3911009

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911009');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911009');


-- Global-ID 3911011

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911011');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911011');


-- Global-ID 3911012

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911012');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911012');


-- Global-ID 3911014

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911014');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911014');


-- Global-ID 3911015

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911015');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911015');


-- Global-ID 3911016

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911016');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911016');


-- Global-ID 3911017

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911017');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911017');


-- Global-ID 3911018

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911018');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911018');


-- Global-ID 3911019

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911019');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911019');


-- Global-ID 3911021

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911021');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911021');


-- Global-ID 3911022

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911022');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911022');


-- Global-ID 3911023

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911023');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911023');


-- Global-ID 3911024

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911024');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911024');


-- Global-ID 3911163

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911163');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911163');


-- Global-ID 3911165

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911165');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911165');


-- Global-ID 3911166

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911166');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911166');


-- Global-ID 3911167

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911167');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911167');


-- Global-ID 3911168

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911168');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911168');


-- Global-ID 3911169

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911169');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911169');


-- Global-ID 3911170

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911170');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911170');


-- Global-ID 3911188

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911188');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911188');


-- Global-ID 3911190

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911190');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911190');


-- Global-ID 3911294

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911294');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911294');


-- Global-ID 3911295

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911295');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911295');


-- Global-ID 3911297

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911297');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911297');


-- Global-ID 3911298

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911298');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911298');


-- Global-ID 3911301

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911301');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911301');


-- Global-ID 3911302

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911302');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911302');


-- Global-ID 3911303

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911303');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911303');


-- Global-ID 3911304

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911304');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911304');


-- Global-ID 3911305

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911305');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911305');


-- Global-ID 3911306

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911306');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911306');


-- Global-ID 3911307

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911307');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911307');


-- Global-ID 3911308

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911308');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911308');


-- Global-ID 3911311

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911311');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911311');


-- Global-ID 3911312

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911312');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911312');


-- Global-ID 3911313

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911313');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911313');


-- Global-ID 3911314

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911314');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911314');


-- Global-ID 3911317

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911317');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911317');


-- Global-ID 3911318

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911318');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911318');


-- Global-ID 3911319

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911319');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911319');


-- Global-ID 3911320

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911320');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911320');


-- Global-ID 3911324

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911324');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911324');


-- Global-ID 3911325

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911325');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911325');


-- Global-ID 3911326

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911326');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911326');


-- Global-ID 3911328

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911328');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911328');


-- Global-ID 3911329

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911329');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911329');


-- Global-ID 3911330

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911330');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911330');


-- Global-ID 3911331

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911331');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911331');


-- Global-ID 3911332

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911332');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911332');


-- Global-ID 3911333

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911333');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911333');


-- Global-ID 3911358

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911358');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911358');


-- Global-ID 3911359

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911359');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911359');


-- Global-ID 3911362

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911362');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911362');


-- Global-ID 3911365

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911365');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911365');


-- Global-ID 3911366

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911366');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911366');


-- Global-ID 3911367

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911367');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911367');


-- Global-ID 3911368

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911368');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911368');


-- Global-ID 3911369

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911369');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911369');


-- Global-ID 3911370

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911370');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911370');


-- Global-ID 3911371

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911371');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911371');


-- Global-ID 3911373

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911373');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911373');


-- Global-ID 3911374

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911374');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911374');


-- Global-ID 3911375

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911375');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911375');


-- Global-ID 3911376

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911376');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911376');


-- Global-ID 3911377

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911377');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911377');


-- Global-ID 3911378

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911378');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911378');


-- Global-ID 3911379

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911379');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911379');


-- Global-ID 3911380

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911380');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911380');


-- Global-ID 3911381

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911381');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911381');


-- Global-ID 3911382

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911382');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911382');


-- Global-ID 3911383

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911383');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911383');


-- Global-ID 3911385

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911385');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911385');


-- Global-ID 3911386

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911386');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911386');


-- Global-ID 3911387

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911387');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911387');


-- Global-ID 3911388

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911388');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911388');


-- Global-ID 3911389

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911389');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911389');


-- Global-ID 3911390

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911390');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911390');


-- Global-ID 3911391

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911391');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911391');


-- Global-ID 3911392

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911392');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911392');


-- Global-ID 3911393

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911393');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911393');


-- Global-ID 3911394

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911394');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911394');


-- Global-ID 3911397

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911397');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911397');


-- Global-ID 3911398

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911398');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911398');


-- Global-ID 3911399

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911399');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911399');


-- Global-ID 3911400

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911400');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911400');


-- Global-ID 3911401

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911401');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911401');


-- Global-ID 3911402

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911402');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911402');


-- Global-ID 3911403

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911403');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911403');


-- Global-ID 3911405

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911405');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911405');


-- Global-ID 3911406

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911406');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911406');


-- Global-ID 3911408

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911408');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911408');


-- Global-ID 3911410

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911410');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911410');


-- Global-ID 3911411

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911411');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911411');


-- Global-ID 3911413

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911413');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911413');


-- Global-ID 3911414

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911414');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911414');


-- Global-ID 3911415

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911415');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911415');


-- Global-ID 3911417

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911417');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911417');


-- Global-ID 3911418

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911418');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911418');


-- Global-ID 3911419

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911419');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911419');


-- Global-ID 3911420

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911420');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911420');


-- Global-ID 3911421

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911421');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911421');


-- Global-ID 3911422

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911422');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911422');


-- Global-ID 3911423

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911423');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911423');


-- Global-ID 3911424

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911424');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911424');


-- Global-ID 3911426

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911426');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911426');


-- Global-ID 3911427

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911427');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911427');


-- Global-ID 3911428

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911428');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911428');


-- Global-ID 3911429

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911429');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911429');


-- Global-ID 3911432

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911432');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911432');


-- Global-ID 3911433

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911433');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911433');


-- Global-ID 3911434

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911434');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911434');


-- Global-ID 3911435

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911435');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911435');


-- Global-ID 3911436

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911436');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911436');


-- Global-ID 3911437

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911437');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911437');


-- Global-ID 3911438

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911438');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911438');


-- Global-ID 3911439

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911439');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911439');


-- Global-ID 3911440

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911440');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911440');


-- Global-ID 3911441

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911441');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911441');


-- Global-ID 3911442

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911442');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911442');


-- Global-ID 3911444

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911444');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911444');


-- Global-ID 3911447

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911447');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911447');


-- Global-ID 3911448

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911448');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911448');


-- Global-ID 3911449

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911449');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911449');


-- Global-ID 3911451

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911451');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911451');


-- Global-ID 3911452

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911452');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911452');


-- Global-ID 3911453

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911453');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911453');


-- Global-ID 3911455

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911455');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911455');


-- Global-ID 3911456

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911456');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911456');


-- Global-ID 3911458

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911458');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911458');


-- Global-ID 3911459

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911459');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911459');


-- Global-ID 3911460

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911460');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911460');


-- Global-ID 3911461

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911461');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911461');


-- Global-ID 3911462

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911462');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911462');


-- Global-ID 3911463

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911463');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911463');


-- Global-ID 3911466

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911466');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911466');


-- Global-ID 3911467

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911467');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911467');


-- Global-ID 3911468

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911468');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911468');


-- Global-ID 3911470

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911470');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911470');


-- Global-ID 3911472

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911472');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911472');


-- Global-ID 3911473

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911473');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911473');


-- Global-ID 3911474

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911474');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911474');


-- Global-ID 3911475

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911475');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911475');


-- Global-ID 3911476

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911476');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911476');


-- Global-ID 3911482

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911482');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911482');


-- Global-ID 3911483

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911483');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911483');


-- Global-ID 3911486

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911486');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911486');


-- Global-ID 3911487

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911487');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911487');


-- Global-ID 3911488

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911488');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911488');


-- Global-ID 3911489

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911489');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911489');


-- Global-ID 3911490

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911490');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911490');


-- Global-ID 3911491

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911491');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911491');


-- Global-ID 3911493

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911493');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911493');


-- Global-ID 3911531

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911531');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911531');


-- Global-ID 3911533

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911533');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911533');


-- Global-ID 3911536

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911536');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911536');


-- Global-ID 3911538

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911538');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911538');


-- Global-ID 3911539

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911539');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911539');


-- Global-ID 3911540

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911540');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911540');


-- Global-ID 3911541

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911541');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911541');


-- Global-ID 3911542

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911542');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911542');


-- Global-ID 3911543

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911543');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911543');


-- Global-ID 3911544

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911544');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911544');


-- Global-ID 3911545

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911545');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911545');


-- Global-ID 3911546

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911546');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911546');


-- Global-ID 3911547

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911547');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911547');


-- Global-ID 3911548

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911548');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911548');


-- Global-ID 3911549

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911549');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911549');


-- Global-ID 3911551

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911551');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911551');


-- Global-ID 3911552

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911552');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911552');


-- Global-ID 3911553

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911553');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911553');


-- Global-ID 3911554

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911554');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911554');


-- Global-ID 3911555

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911555');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911555');


-- Global-ID 3911556

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911556');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911556');


-- Global-ID 3911561

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911561');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911561');


-- Global-ID 3911563

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911563');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911563');


-- Global-ID 3911564

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911564');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911564');


-- Global-ID 3911586

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911586');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911586');


-- Global-ID 3911589

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911589');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911589');


-- Global-ID 3911590

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911590');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911590');


-- Global-ID 3911591

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911591');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911591');


-- Global-ID 3911592

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911592');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911592');


-- Global-ID 3911593

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911593');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911593');


-- Global-ID 3911644

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911644');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911644');


-- Global-ID 3911646

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911646');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911646');


-- Global-ID 3911648

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911648');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911648');


-- Global-ID 3911649

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911649');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911649');


-- Global-ID 3911650

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911650');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911650');


-- Global-ID 3911651

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911651');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911651');


-- Global-ID 3911652

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911652');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911652');


-- Global-ID 3911653

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911653');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911653');


-- Global-ID 3911654

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911654');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911654');


-- Global-ID 3911655

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911655');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911655');


-- Global-ID 3911656

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911656');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911656');


-- Global-ID 3911658

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911658');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911658');


-- Global-ID 3911659

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911659');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911659');


-- Global-ID 3911661

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911661');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911661');


-- Global-ID 3911665

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911665');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911665');


-- Global-ID 3911666

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911666');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911666');


-- Global-ID 3911668

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911668');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911668');


-- Global-ID 3911669

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911669');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911669');


-- Global-ID 3911671

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911671');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911671');


-- Global-ID 3911674

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911674');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911674');


-- Global-ID 3911676

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911676');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911676');


-- Global-ID 3911678

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911678');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911678');


-- Global-ID 3911679

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911679');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911679');


-- Global-ID 3911680

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911680');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911680');


-- Global-ID 3911681

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911681');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911681');


-- Global-ID 3911682

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911682');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911682');


-- Global-ID 3911683

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911683');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911683');


-- Global-ID 3911684

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911684');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911684');


-- Global-ID 3911685

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911685');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911685');


-- Global-ID 3911688

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911688');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911688');


-- Global-ID 3911689

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911689');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911689');


-- Global-ID 3911690

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911690');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911690');


-- Global-ID 3911691

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911691');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911691');


-- Global-ID 3911693

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911693');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911693');


-- Global-ID 3911694

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911694');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911694');


-- Global-ID 3911695

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911695');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911695');


-- Global-ID 3911697

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911697');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911697');


-- Global-ID 3911698

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911698');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911698');


-- Global-ID 3911699

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911699');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911699');


-- Global-ID 3911700

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911700');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911700');


-- Global-ID 3911702

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911702');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911702');


-- Global-ID 3911703

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911703');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911703');


-- Global-ID 3911704

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911704');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911704');


-- Global-ID 3911705

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911705');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911705');


-- Global-ID 3911706

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911706');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911706');


-- Global-ID 3911722

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911722');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911722');


-- Global-ID 3911723

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911723');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911723');


-- Global-ID 3911724

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911724');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911724');


-- Global-ID 3911726

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911726');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911726');


-- Global-ID 3911727

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911727');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911727');


-- Global-ID 3911728

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911728');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911728');


-- Global-ID 3911729

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911729');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911729');


-- Global-ID 3911730

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911730');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911730');


-- Global-ID 3911732

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911732');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911732');


-- Global-ID 3911733

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911733');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911733');


-- Global-ID 3911736

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911736');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911736');


-- Global-ID 3911738

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911738');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911738');


-- Global-ID 3911858

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911858');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911858');


-- Global-ID 3911859

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911859');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911859');


-- Global-ID 3911860

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911860');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911860');


-- Global-ID 3911863

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911863');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911863');


-- Global-ID 3911865

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911865');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911865');


-- Global-ID 3911866

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911866');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911866');


-- Global-ID 3911868

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911868');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3911868');


-- Global-ID 3912279

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912279');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912279');


-- Global-ID 3912280

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912280');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912280');


-- Global-ID 3912281

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912281');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912281');


-- Global-ID 3912284

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912284');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912284');


-- Global-ID 3912295

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912295');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912295');


-- Global-ID 3912338

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912338');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912338');


-- Global-ID 3912339

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912339');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912339');


-- Global-ID 3912341

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912341');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912341');


-- Global-ID 3912342

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912342');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912342');


-- Global-ID 3912343

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912343');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912343');


-- Global-ID 3912344

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912344');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912344');


-- Global-ID 3912345

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912345');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912345');


-- Global-ID 3912346

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912346');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912346');


-- Global-ID 3912347

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912347');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912347');


-- Global-ID 3912348

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912348');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912348');


-- Global-ID 3912351

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912351');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912351');


-- Global-ID 3912353

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912353');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912353');


-- Global-ID 3912375

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912375');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912375');


-- Global-ID 3912376

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912376');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912376');


-- Global-ID 3912377

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912377');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912377');


-- Global-ID 3912378

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912378');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912378');


-- Global-ID 3912379

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912379');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912379');


-- Global-ID 3912380

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912380');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912380');


-- Global-ID 3912503

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912503');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912503');


-- Global-ID 3912504

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912504');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912504');


-- Global-ID 3912505

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912505');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912505');


-- Global-ID 3912506

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912506');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912506');


-- Global-ID 3912508

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912508');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912508');


-- Global-ID 3912509

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912509');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912509');


-- Global-ID 3912510

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912510');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912510');


-- Global-ID 3912511

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912511');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912511');


-- Global-ID 3912512

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912512');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912512');


-- Global-ID 3912513

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912513');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912513');


-- Global-ID 3912514

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912514');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912514');


-- Global-ID 3912517

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912517');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912517');


-- Global-ID 3912518

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912518');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912518');


-- Global-ID 3912519

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912519');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912519');


-- Global-ID 3912533

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912533');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912533');


-- Global-ID 3912534

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912534');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912534');


-- Global-ID 3912536

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912536');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912536');


-- Global-ID 3912537

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912537');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912537');


-- Global-ID 3912539

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912539');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912539');


-- Global-ID 3912541

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912541');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912541');


-- Global-ID 3912542

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912542');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912542');


-- Global-ID 3912543

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912543');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912543');


-- Global-ID 3912544

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912544');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912544');


-- Global-ID 3912546

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912546');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912546');


-- Global-ID 3912547

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912547');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912547');


-- Global-ID 3912549

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912549');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912549');


-- Global-ID 3912553

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912553');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912553');


-- Global-ID 3912554

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912554');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912554');


-- Global-ID 3912555

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912555');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912555');


-- Global-ID 3912558

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912558');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912558');


-- Global-ID 3912559

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912559');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912559');


-- Global-ID 3912560

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912560');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912560');


-- Global-ID 3912563

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912563');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912563');


-- Global-ID 3912564

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912564');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912564');


-- Global-ID 3912565

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912565');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912565');


-- Global-ID 3912566

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912566');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912566');


-- Global-ID 3912567

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912567');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912567');


-- Global-ID 3912568

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912568');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912568');


-- Global-ID 3912569

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912569');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912569');


-- Global-ID 3912570

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912570');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912570');


-- Global-ID 3912571

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912571');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912571');


-- Global-ID 3912572

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912572');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912572');


-- Global-ID 3912573

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912573');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912573');


-- Global-ID 3912574

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912574');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912574');


-- Global-ID 3912575

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912575');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912575');


-- Global-ID 3912576

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912576');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912576');


-- Global-ID 3912578

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912578');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912578');


-- Global-ID 3912579

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912579');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912579');


-- Global-ID 3912580

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912580');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912580');


-- Global-ID 3912581

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912581');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912581');


-- Global-ID 3912583

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912583');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912583');


-- Global-ID 3912584

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912584');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912584');


-- Global-ID 3912585

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912585');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912585');


-- Global-ID 3912616

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912616');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912616');


-- Global-ID 3912617

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912617');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912617');


-- Global-ID 3912618

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912618');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912618');


-- Global-ID 3912619

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912619');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912619');


-- Global-ID 3912620

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912620');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912620');


-- Global-ID 3912622

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912622');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912622');


-- Global-ID 3912623

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912623');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912623');


-- Global-ID 3912656

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912656');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912656');


-- Global-ID 3912658

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912658');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912658');


-- Global-ID 3912659

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912659');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912659');


-- Global-ID 3912661

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912661');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912661');


-- Global-ID 3912662

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912662');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912662');


-- Global-ID 3912663

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912663');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912663');


-- Global-ID 3912664

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912664');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912664');


-- Global-ID 3912665

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912665');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912665');


-- Global-ID 3912667

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912667');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912667');


-- Global-ID 3912668

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912668');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912668');


-- Global-ID 3912670

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912670');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912670');


-- Global-ID 3912677

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912677');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912677');


-- Global-ID 3912685

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912685');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912685');


-- Global-ID 3912686

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912686');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912686');


-- Global-ID 3912687

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912687');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912687');


-- Global-ID 3912689

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912689');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912689');


-- Global-ID 3912690

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912690');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912690');


-- Global-ID 3912712

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912712');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912712');


-- Global-ID 3912713

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912713');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912713');


-- Global-ID 3912714

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912714');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912714');


-- Global-ID 3912717

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912717');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912717');


-- Global-ID 3912718

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912718');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912718');


-- Global-ID 3912719

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912719');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912719');


-- Global-ID 3912720

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912720');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912720');


-- Global-ID 3912747

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912747');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912747');


-- Global-ID 3912748

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912748');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912748');


-- Global-ID 3912749

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912749');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912749');


-- Global-ID 3912751

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912751');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912751');


-- Global-ID 3912752

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912752');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912752');


-- Global-ID 3912753

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912753');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912753');


-- Global-ID 3912754

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912754');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912754');


-- Global-ID 3912762

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912762');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912762');


-- Global-ID 3912763

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912763');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912763');


-- Global-ID 3912764

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912764');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912764');


-- Global-ID 3912765

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912765');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912765');


-- Global-ID 3912768

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912768');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912768');


-- Global-ID 3912770

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912770');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912770');


-- Global-ID 3912771

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912771');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912771');


-- Global-ID 3912772

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912772');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912772');


-- Global-ID 3912773

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912773');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912773');


-- Global-ID 3912787

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912787');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912787');


-- Global-ID 3912788

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912788');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912788');


-- Global-ID 3912789

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912789');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912789');


-- Global-ID 3912791

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912791');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912791');


-- Global-ID 3912792

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912792');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912792');


-- Global-ID 3912793

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912793');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912793');


-- Global-ID 3912794

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912794');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912794');


-- Global-ID 3912796

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912796');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912796');


-- Global-ID 3912797

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912797');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912797');


-- Global-ID 3912801

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912801');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912801');


-- Global-ID 3912803

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912803');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912803');


-- Global-ID 3912804

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912804');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912804');


-- Global-ID 3912805

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912805');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912805');


-- Global-ID 3912806

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912806');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912806');


-- Global-ID 3912807

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912807');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912807');


-- Global-ID 3912810

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912810');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912810');


-- Global-ID 3912811

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912811');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912811');


-- Global-ID 3912812

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912812');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912812');


-- Global-ID 3912813

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912813');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912813');


-- Global-ID 3912814

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912814');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912814');


-- Global-ID 3912817

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912817');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912817');


-- Global-ID 3912819

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912819');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912819');


-- Global-ID 3912902

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912902');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912902');


-- Global-ID 3912904

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912904');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912904');


-- Global-ID 3912905

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912905');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912905');


-- Global-ID 3912906

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912906');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912906');


-- Global-ID 3912907

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912907');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912907');


-- Global-ID 3912908

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912908');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912908');


-- Global-ID 3912909

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912909');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912909');


-- Global-ID 3912911

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912911');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912911');


-- Global-ID 3912912

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912912');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912912');


-- Global-ID 3912913

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912913');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912913');


-- Global-ID 3912914

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912914');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912914');


-- Global-ID 3912915

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912915');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912915');


-- Global-ID 3912916

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912916');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912916');


-- Global-ID 3912917

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912917');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912917');


-- Global-ID 3912918

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912918');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912918');


-- Global-ID 3912919

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912919');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912919');


-- Global-ID 3912920

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912920');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912920');


-- Global-ID 3912921

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912921');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912921');


-- Global-ID 3912922

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912922');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912922');


-- Global-ID 3912923

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912923');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912923');


-- Global-ID 3912925

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912925');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912925');


-- Global-ID 3912926

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912926');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912926');


-- Global-ID 3912927

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912927');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912927');


-- Global-ID 3912958

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912958');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912958');


-- Global-ID 3912960

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912960');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3912960');


-- Global-ID 3913587

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913587');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913587');


-- Global-ID 3913840

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913840');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913840');


-- Global-ID 3913843

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913843');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913843');


-- Global-ID 3913845

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913845');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913845');


-- Global-ID 3913847

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913847');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913847');


-- Global-ID 3913848

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913848');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913848');


-- Global-ID 3913849

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913849');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913849');


-- Global-ID 3913850

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913850');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913850');


-- Global-ID 3913851

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913851');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913851');


-- Global-ID 3913852

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913852');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913852');


-- Global-ID 3913853

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913853');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913853');


-- Global-ID 3913854

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913854');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913854');


-- Global-ID 3913855

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913855');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913855');


-- Global-ID 3913856

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913856');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913856');


-- Global-ID 3913857

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913857');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913857');


-- Global-ID 3913859

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913859');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913859');


-- Global-ID 3913862

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913862');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913862');


-- Global-ID 3913863

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913863');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913863');


-- Global-ID 3913864

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913864');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913864');


-- Global-ID 3913866

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913866');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913866');


-- Global-ID 3913868

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913868');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913868');


-- Global-ID 3913869

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913869');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913869');


-- Global-ID 3913870

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913870');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913870');


-- Global-ID 3913872

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913872');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913872');


-- Global-ID 3913873

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913873');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913873');


-- Global-ID 3913875

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913875');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913875');


-- Global-ID 3913876

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913876');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913876');


-- Global-ID 3913877

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913877');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913877');


-- Global-ID 3913878

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913878');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913878');


-- Global-ID 3913880

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913880');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913880');


-- Global-ID 3913883

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913883');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913883');


-- Global-ID 3913884

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913884');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913884');


-- Global-ID 3913885

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913885');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913885');


-- Global-ID 3913887

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913887');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913887');


-- Global-ID 3913889

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913889');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913889');


-- Global-ID 3913891

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913891');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3913891');


-- Global-ID 3914152

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914152');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914152');


-- Global-ID 3914154

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914154');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914154');


-- Global-ID 3914155

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914155');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914155');


-- Global-ID 3914156

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914156');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914156');


-- Global-ID 3914157

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914157');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914157');


-- Global-ID 3914158

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914158');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914158');


-- Global-ID 3914159

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914159');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914159');


-- Global-ID 3914161

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914161');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914161');


-- Global-ID 3914162

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914162');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914162');


-- Global-ID 3914164

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914164');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914164');


-- Global-ID 3914165

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914165');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914165');


-- Global-ID 3914166

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914166');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914166');


-- Global-ID 3914167

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914167');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914167');


-- Global-ID 3914188

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914188');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914188');


-- Global-ID 3914189

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914189');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914189');


-- Global-ID 3914326

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914326');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914326');


-- Global-ID 3914327

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914327');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914327');


-- Global-ID 3914328

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914328');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914328');


-- Global-ID 3914329

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914329');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914329');


-- Global-ID 3914330

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914330');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914330');


-- Global-ID 3914333

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914333');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914333');


-- Global-ID 3914334

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914334');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914334');


-- Global-ID 3914335

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914335');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914335');


-- Global-ID 3914336

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914336');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914336');


-- Global-ID 3914337

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914337');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914337');


-- Global-ID 3914339

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914339');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914339');


-- Global-ID 3914340

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914340');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914340');


-- Global-ID 3914343

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914343');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914343');


-- Global-ID 3914345

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914345');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914345');


-- Global-ID 3914346

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914346');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914346');


-- Global-ID 3914347

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914347');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914347');


-- Global-ID 3914348

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914348');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914348');


-- Global-ID 3914350

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914350');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914350');


-- Global-ID 3914352

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914352');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914352');


-- Global-ID 3914353

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914353');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914353');


-- Global-ID 3914354

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914354');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914354');


-- Global-ID 3914361

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914361');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914361');


-- Global-ID 3914362

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914362');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914362');


-- Global-ID 3914363

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914363');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914363');


-- Global-ID 3914364

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914364');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914364');


-- Global-ID 3914974

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914974');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914974');


-- Global-ID 3914980

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914980');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914980');


-- Global-ID 3914986

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914986');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914986');


-- Global-ID 3914993

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914993');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914993');


-- Global-ID 3914995

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914995');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914995');


-- Global-ID 3914999

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914999');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3914999');


-- Global-ID 3915000

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915000');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915000');


-- Global-ID 3915001

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915001');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915001');


-- Global-ID 3915002

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915002');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915002');


-- Global-ID 3915004

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915004');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915004');


-- Global-ID 3915005

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915005');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915005');


-- Global-ID 3915006

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915006');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915006');


-- Global-ID 3915007

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915007');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915007');


-- Global-ID 3915009

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915009');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915009');


-- Global-ID 3915010

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915010');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915010');


-- Global-ID 3915011

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915011');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915011');


-- Global-ID 3915012

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915012');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915012');


-- Global-ID 3915015

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915015');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915015');


-- Global-ID 3915016

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915016');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915016');


-- Global-ID 3915017

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915017');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915017');


-- Global-ID 3915020

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915020');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915020');


-- Global-ID 3915022

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915022');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915022');


-- Global-ID 3915023

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915023');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915023');


-- Global-ID 3915024

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915024');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915024');


-- Global-ID 3915025

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915025');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915025');


-- Global-ID 3915026

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915026');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915026');


-- Global-ID 3915027

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915027');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915027');


-- Global-ID 3915028

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915028');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915028');


-- Global-ID 3915030

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915030');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915030');


-- Global-ID 3915031

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915031');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915031');


-- Global-ID 3915032

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915032');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915032');


-- Global-ID 3915033

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915033');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915033');


-- Global-ID 3915034

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915034');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915034');


-- Global-ID 3915036

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915036');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915036');


-- Global-ID 3915038

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915038');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915038');


-- Global-ID 3915040

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915040');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915040');


-- Global-ID 3915041

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915041');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915041');


-- Global-ID 3915042

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915042');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915042');


-- Global-ID 3915043

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915043');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915043');


-- Global-ID 3915044

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915044');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915044');


-- Global-ID 3915046

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915046');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915046');


-- Global-ID 3915047

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915047');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915047');


-- Global-ID 3915050

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915050');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915050');


-- Global-ID 3915052

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915052');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915052');


-- Global-ID 3915054

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915054');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915054');


-- Global-ID 3915055

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915055');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915055');


-- Global-ID 3915057

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915057');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915057');


-- Global-ID 3915058

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915058');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915058');


-- Global-ID 3915062

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915062');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915062');


-- Global-ID 3915064

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915064');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915064');


-- Global-ID 3915065

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915065');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915065');


-- Global-ID 3915066

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915066');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915066');


-- Global-ID 3915067

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915067');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915067');


-- Global-ID 3915068

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915068');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915068');


-- Global-ID 3915070

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915070');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915070');


-- Global-ID 3915074

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915074');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915074');


-- Global-ID 3915101

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915101');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915101');


-- Global-ID 3915102

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915102');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915102');


-- Global-ID 3915103

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915103');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915103');


-- Global-ID 3915104

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915104');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915104');


-- Global-ID 3915105

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915105');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915105');


-- Global-ID 3915106

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915106');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915106');


-- Global-ID 3915107

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915107');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915107');


-- Global-ID 3915108

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915108');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915108');


-- Global-ID 3915111

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915111');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915111');


-- Global-ID 3915113

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915113');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915113');


-- Global-ID 3915114

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915114');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3915114');


-- Global-ID 3928393

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928393');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928393');


-- Global-ID 3928394

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928394');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928394');


-- Global-ID 3928396

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928396');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928396');


-- Global-ID 3928397

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928397');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928397');


-- Global-ID 3928398

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928398');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928398');


-- Global-ID 3928399

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928399');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928399');


-- Global-ID 3928400

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928400');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928400');


-- Global-ID 3928402

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928402');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928402');


-- Global-ID 3928403

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928403');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928403');


-- Global-ID 3928404

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928404');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928404');


-- Global-ID 3928405

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928405');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928405');


-- Global-ID 3928406

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928406');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928406');


-- Global-ID 3928407

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928407');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928407');


-- Global-ID 3928417

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928417');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928417');


-- Global-ID 3928418

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928418');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928418');


-- Global-ID 3928419

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928419');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928419');


-- Global-ID 3928420

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928420');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928420');


-- Global-ID 3928421

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928421');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928421');


-- Global-ID 3928423

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928423');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928423');


-- Global-ID 3928424

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928424');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928424');


-- Global-ID 3928425

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928425');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928425');


-- Global-ID 3928426

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928426');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928426');


-- Global-ID 3928428

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928428');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928428');


-- Global-ID 3928429

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928429');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928429');


-- Global-ID 3928430

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928430');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928430');


-- Global-ID 3928431

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928431');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928431');


-- Global-ID 3928433

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928433');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928433');


-- Global-ID 3928434

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928434');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928434');


-- Global-ID 3928436

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928436');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928436');


-- Global-ID 3928437

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928437');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928437');


-- Global-ID 3928438

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928438');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928438');


-- Global-ID 3928440

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928440');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928440');


-- Global-ID 3928441

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928441');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928441');


-- Global-ID 3928442

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928442');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928442');


-- Global-ID 3928443

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928443');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928443');


-- Global-ID 3928444

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928444');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928444');


-- Global-ID 3928445

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928445');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928445');


-- Global-ID 3928446

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928446');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928446');


-- Global-ID 3928447

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928447');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928447');


-- Global-ID 3928448

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928448');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928448');


-- Global-ID 3928449

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928449');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928449');


-- Global-ID 3928450

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928450');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928450');


-- Global-ID 3928451

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928451');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928451');


-- Global-ID 3928452

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928452');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928452');


-- Global-ID 3928453

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928453');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928453');


-- Global-ID 3928454

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928454');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928454');


-- Global-ID 3928455

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928455');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928455');


-- Global-ID 3928456

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928456');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928456');


-- Global-ID 3928457

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928457');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928457');


-- Global-ID 3928458

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928458');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928458');


-- Global-ID 3928459

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928459');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928459');


-- Global-ID 3928465

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928465');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928465');


-- Global-ID 3928467

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928467');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928467');


-- Global-ID 3928468

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928468');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928468');


-- Global-ID 3928469

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928469');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928469');


-- Global-ID 3928470

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928470');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928470');


-- Global-ID 3928472

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928472');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928472');


-- Global-ID 3928473

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928473');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928473');


-- Global-ID 3928474

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928474');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928474');


-- Global-ID 3928475

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928475');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928475');


-- Global-ID 3928476

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928476');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928476');


-- Global-ID 3928477

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928477');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928477');


-- Global-ID 3928480

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928480');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928480');


-- Global-ID 3928481

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928481');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928481');


-- Global-ID 3928482

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928482');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928482');


-- Global-ID 3928490

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928490');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928490');


-- Global-ID 3928491

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928491');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928491');


-- Global-ID 3928492

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928492');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928492');


-- Global-ID 3928493

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928493');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928493');


-- Global-ID 3928494

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928494');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928494');


-- Global-ID 3928497

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928497');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928497');


-- Global-ID 3928498

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928498');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928498');


-- Global-ID 3928499

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928499');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928499');


-- Global-ID 3928500

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928500');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928500');


-- Global-ID 3928502

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928502');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928502');


-- Global-ID 3928504

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928504');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928504');


-- Global-ID 3928505

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928505');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928505');


-- Global-ID 3928506

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928506');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928506');


-- Global-ID 3928507

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928507');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928507');


-- Global-ID 3928508

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928508');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928508');


-- Global-ID 3928510

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928510');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928510');


-- Global-ID 3928511

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928511');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928511');


-- Global-ID 3928512

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928512');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928512');


-- Global-ID 3928513

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928513');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928513');


-- Global-ID 3928514

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928514');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928514');


-- Global-ID 3928516

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928516');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928516');


-- Global-ID 3928517

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928517');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928517');


-- Global-ID 3928518

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928518');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928518');


-- Global-ID 3928519

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928519');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928519');


-- Global-ID 3928520

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928520');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928520');


-- Global-ID 3928521

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928521');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928521');


-- Global-ID 3928522

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928522');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928522');


-- Global-ID 3928524

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928524');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928524');


-- Global-ID 3928525

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928525');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928525');


-- Global-ID 3928526

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928526');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928526');


-- Global-ID 3928527

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928527');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928527');


-- Global-ID 3928528

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928528');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928528');


-- Global-ID 3928529

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928529');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928529');


-- Global-ID 3928557

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928557');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928557');


-- Global-ID 3928558

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928558');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928558');


-- Global-ID 3928560

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928560');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928560');


-- Global-ID 3928561

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928561');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928561');


-- Global-ID 3928562

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928562');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928562');


-- Global-ID 3928563

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928563');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928563');


-- Global-ID 3928564

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928564');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928564');


-- Global-ID 3928565

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928565');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928565');


-- Global-ID 3928566

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928566');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928566');


-- Global-ID 3928873

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928873');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928873');


-- Global-ID 3928874

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928874');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928874');


-- Global-ID 3928876

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928876');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928876');


-- Global-ID 3928877

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928877');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928877');


-- Global-ID 3928878

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928878');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3928878');


-- Global-ID 3929517

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929517');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929517');


-- Global-ID 3929528

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929528');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929528');


-- Global-ID 3929530

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929530');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929530');


-- Global-ID 3929533

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929533');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929533');


-- Global-ID 3929609

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929609');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3929609');


-- Global-ID 3931906

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931906');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931906');


-- Global-ID 3931907

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931907');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931907');


-- Global-ID 3931908

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931908');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931908');


-- Global-ID 3931910

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931910');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931910');


-- Global-ID 3931912

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931912');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931912');


-- Global-ID 3931913

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931913');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931913');


-- Global-ID 3931914

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931914');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931914');


-- Global-ID 3931915

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931915');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931915');


-- Global-ID 3931916

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931916');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931916');


-- Global-ID 3931917

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931917');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931917');


-- Global-ID 3931920

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931920');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931920');


-- Global-ID 3931921

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931921');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931921');


-- Global-ID 3931923

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931923');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931923');


-- Global-ID 3931924

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931924');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931924');


-- Global-ID 3931925

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931925');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931925');


-- Global-ID 3931926

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931926');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3931926');


-- Global-ID 3932050

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932050');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932050');


-- Global-ID 3932381

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932381');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932381');


-- Global-ID 3932382

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932382');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932382');


-- Global-ID 3932400

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932400');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932400');


-- Global-ID 3932401

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932401');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932401');


-- Global-ID 3932403

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932403');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932403');


-- Global-ID 3932404

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932404');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932404');


-- Global-ID 3932406

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932406');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932406');


-- Global-ID 3932407

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932407');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932407');


-- Global-ID 3932468

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932468');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932468');


-- Global-ID 3932469

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932469');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932469');


-- Global-ID 3932470

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932470');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932470');


-- Global-ID 3932471

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932471');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932471');


-- Global-ID 3932569

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932569');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932569');


-- Global-ID 3932570

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932570');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932570');


-- Global-ID 3932571

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932571');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932571');


-- Global-ID 3932572

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932572');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932572');


-- Global-ID 3932573

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932573');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932573');


-- Global-ID 3932744

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932744');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932744');


-- Global-ID 3932844

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932844');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932844');


-- Global-ID 3932845

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932845');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932845');


-- Global-ID 3932847

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932847');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932847');


-- Global-ID 3932848

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932848');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932848');


-- Global-ID 3932852

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932852');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3932852');


-- Global-ID 3933066

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933066');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933066');


-- Global-ID 3933067

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933067');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933067');


-- Global-ID 3933069

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933069');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933069');


-- Global-ID 3933072

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933072');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933072');


-- Global-ID 3933073

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933073');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933073');


-- Global-ID 3933075

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933075');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3933075');


-- Global-ID 3947725

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947725');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947725');


-- Global-ID 3947726

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947726');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947726');


-- Global-ID 3947728

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947728');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947728');


-- Global-ID 3947729

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947729');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947729');


-- Global-ID 3947730

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947730');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947730');


-- Global-ID 3947731

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947731');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947731');


-- Global-ID 3947732

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947732');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947732');


-- Global-ID 3947735

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947735');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947735');


-- Global-ID 3947737

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947737');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947737');


-- Global-ID 3947738

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947738');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947738');


-- Global-ID 3947739

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947739');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947739');


-- Global-ID 3947740

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947740');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947740');


-- Global-ID 3947741

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947741');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947741');


-- Global-ID 3947742

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947742');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947742');


-- Global-ID 3947744

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947744');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947744');


-- Global-ID 3947745

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947745');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947745');


-- Global-ID 3947746

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947746');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947746');


-- Global-ID 3947747

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947747');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947747');


-- Global-ID 3947748

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947748');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947748');


-- Global-ID 3947749

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947749');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947749');


-- Global-ID 3947750

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947750');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947750');


-- Global-ID 3947751

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947751');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947751');


-- Global-ID 3947752

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947752');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947752');


-- Global-ID 3947753

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947753');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947753');


-- Global-ID 3947754

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947754');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947754');


-- Global-ID 3947755

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947755');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947755');


-- Global-ID 3947756

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947756');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947756');


-- Global-ID 3947757

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947757');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947757');


-- Global-ID 3947758

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947758');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947758');


-- Global-ID 3947759

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947759');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947759');


-- Global-ID 3947760

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947760');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947760');


-- Global-ID 3947761

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947761');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947761');


-- Global-ID 3947762

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947762');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947762');


-- Global-ID 3947763

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947763');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947763');


-- Global-ID 3947764

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947764');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947764');


-- Global-ID 3947765

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947765');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947765');


-- Global-ID 3947766

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947766');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947766');


-- Global-ID 3947767

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947767');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947767');


-- Global-ID 3947769

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947769');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947769');


-- Global-ID 3947771

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947771');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947771');


-- Global-ID 3947772

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947772');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947772');


-- Global-ID 3947773

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947773');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947773');


-- Global-ID 3947775

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947775');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947775');


-- Global-ID 3947776

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947776');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947776');


-- Global-ID 3947784

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947784');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947784');


-- Global-ID 3947786

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947786');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947786');


-- Global-ID 3947787

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947787');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947787');


-- Global-ID 3947789

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947789');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947789');


-- Global-ID 3947790

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947790');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947790');


-- Global-ID 3947817

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947817');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947817');


-- Global-ID 3947818

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947818');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947818');


-- Global-ID 3947819

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947819');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947819');


-- Global-ID 3947821

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947821');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947821');


-- Global-ID 3947823

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947823');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947823');


-- Global-ID 3947824

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947824');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947824');


-- Global-ID 3947826

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947826');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947826');


-- Global-ID 3947827

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947827');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947827');


-- Global-ID 3947828

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947828');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947828');


-- Global-ID 3947829

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947829');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947829');


-- Global-ID 3947830

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947830');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947830');


-- Global-ID 3947832

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947832');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947832');


-- Global-ID 3947833

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947833');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947833');


-- Global-ID 3947834

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947834');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947834');


-- Global-ID 3947835

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947835');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947835');


-- Global-ID 3947836

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947836');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947836');


-- Global-ID 3947837

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947837');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947837');


-- Global-ID 3947838

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947838');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947838');


-- Global-ID 3947840

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947840');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947840');


-- Global-ID 3947842

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947842');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947842');


-- Global-ID 3947843

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947843');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947843');


-- Global-ID 3947846

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947846');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947846');


-- Global-ID 3947847

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947847');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947847');


-- Global-ID 3947848

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947848');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947848');


-- Global-ID 3947849

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947849');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947849');


-- Global-ID 3947850

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947850');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947850');


-- Global-ID 3947851

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947851');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947851');


-- Global-ID 3947852

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947852');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947852');


-- Global-ID 3947855

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947855');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947855');


-- Global-ID 3947857

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947857');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947857');


-- Global-ID 3947858

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947858');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947858');


-- Global-ID 3947859

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947859');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947859');


-- Global-ID 3947860

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947860');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947860');


-- Global-ID 3947866

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947866');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947866');


-- Global-ID 3947867

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947867');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947867');


-- Global-ID 3947868

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947868');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947868');


-- Global-ID 3947869

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947869');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947869');


-- Global-ID 3947871

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947871');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947871');


-- Global-ID 3947872

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947872');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947872');


-- Global-ID 3947873

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947873');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947873');


-- Global-ID 3947874

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947874');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947874');


-- Global-ID 3947875

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947875');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947875');


-- Global-ID 3947878

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947878');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947878');


-- Global-ID 3947880

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947880');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947880');


-- Global-ID 3947881

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947881');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947881');


-- Global-ID 3947884

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947884');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947884');


-- Global-ID 3947885

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947885');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947885');


-- Global-ID 3947886

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947886');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947886');


-- Global-ID 3947888

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947888');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947888');


-- Global-ID 3947889

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947889');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947889');


-- Global-ID 3947890

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947890');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947890');


-- Global-ID 3947891

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947891');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947891');


-- Global-ID 3947892

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947892');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947892');


-- Global-ID 3947893

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947893');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947893');


-- Global-ID 3947894

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947894');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947894');


-- Global-ID 3947895

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947895');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947895');


-- Global-ID 3947896

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947896');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947896');


-- Global-ID 3947897

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947897');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947897');


-- Global-ID 3947902

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947902');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947902');


-- Global-ID 3947904

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947904');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947904');


-- Global-ID 3947905

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947905');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947905');


-- Global-ID 3947907

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947907');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947907');


-- Global-ID 3947910

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947910');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947910');


-- Global-ID 3947911

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947911');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947911');


-- Global-ID 3947912

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947912');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947912');


-- Global-ID 3947913

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947913');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947913');


-- Global-ID 3947921

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947921');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947921');


-- Global-ID 3947923

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947923');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947923');


-- Global-ID 3947924

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947924');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947924');


-- Global-ID 3947925

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947925');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947925');


-- Global-ID 3947926

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947926');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947926');


-- Global-ID 3947927

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947927');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947927');


-- Global-ID 3947928

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947928');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947928');


-- Global-ID 3947931

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947931');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947931');


-- Global-ID 3947932

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947932');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947932');


-- Global-ID 3947933

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947933');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947933');


-- Global-ID 3947934

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947934');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947934');


-- Global-ID 3947935

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947935');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947935');


-- Global-ID 3947936

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947936');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947936');


-- Global-ID 3947937

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947937');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947937');


-- Global-ID 3947938

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947938');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947938');


-- Global-ID 3947939

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947939');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947939');


-- Global-ID 3947940

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947940');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947940');


-- Global-ID 3947941

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947941');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947941');


-- Global-ID 3947942

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947942');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947942');


-- Global-ID 3947943

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947943');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947943');


-- Global-ID 3947944

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947944');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947944');


-- Global-ID 3947945

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947945');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947945');


-- Global-ID 3947946

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947946');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947946');


-- Global-ID 3947953

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947953');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947953');


-- Global-ID 3947954

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947954');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947954');


-- Global-ID 3947957

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947957');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947957');


-- Global-ID 3947959

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947959');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947959');


-- Global-ID 3947966

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947966');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947966');


-- Global-ID 3947968

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947968');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947968');


-- Global-ID 3947985

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947985');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947985');


-- Global-ID 3947986

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947986');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947986');


-- Global-ID 3947987

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947987');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947987');


-- Global-ID 3947988

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947988');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947988');


-- Global-ID 3947989

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947989');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947989');


-- Global-ID 3947990

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947990');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947990');


-- Global-ID 3947993

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947993');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947993');


-- Global-ID 3947994

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947994');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947994');


-- Global-ID 3947995

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947995');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947995');


-- Global-ID 3947996

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947996');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947996');


-- Global-ID 3947997

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947997');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947997');


-- Global-ID 3947998

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947998');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3947998');


-- Global-ID 3948031

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948031');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948031');


-- Global-ID 3948036

UPDATE
    sample_attribute_values
SET
    int_value = 10,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948036');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948036');


-- Global-ID 3948049

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948049');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948049');


-- Global-ID 3948055

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948055');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948055');


-- Global-ID 3948056

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948056');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948056');


-- Global-ID 3948057

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948057');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948057');


-- Global-ID 3948058

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948058');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948058');


-- Global-ID 3948059

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948059');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948059');


-- Global-ID 3948060

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948060');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948060');


-- Global-ID 3948061

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948061');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948061');


-- Global-ID 3948062

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948062');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948062');


-- Global-ID 3948064

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948064');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948064');


-- Global-ID 3948348

UPDATE
    sample_attribute_values
SET
    int_value = 300,
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948348');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: 3948348');
