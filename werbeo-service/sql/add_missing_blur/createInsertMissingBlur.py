import csv
from pygments.lexers.csound import newline
from reportlab.lib.PyFontify import idRE

# Unschärfe = 21

updateUnsch = """

-- Global-ID {}

UPDATE
    sample_attribute_values
SET
    int_value = {},
    updated_on = now()
WHERE
    sample_attribute_id = 21
    AND sample_id = (
        SELECT
            sample_id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: {}');

UPDATE
    occurrences
SET
    updated_on = now()
WHERE
    id = (
        SELECT
            id
        FROM
            detail_occurrences
        WHERE
            external_key = 'Flora-MV-Global-ID: {}');
"""

with open('blur.csv', newline='') as blurFile:
    with open('update-loe2.sql', 'w') as outputFile:
        blur = csv.DictReader(blurFile)
        for row in blur:       
            globalId = row['global_id'].strip()
            unsch = row['unsch'].strip()
            outputFile.write(updateUnsch.format(globalId, unsch, globalId, globalId))
    