select
	'UPDATE indicia.sample_attribute_values set text_value = ''' || v_mtbqqq.id || ''', updated_on = now() + id * interval ''0.0001 second'' WHERE id = ' || indicia.sample_attribute_values.id || ';'
from
	indicia.sample_attribute_values
join indicia.samples on
	(indicia.samples.id = indicia.sample_attribute_values.sample_id)
join mtb.v_mtbqqq on
	(public.st_intersects( indicia.samples.geom,
	mtb.v_mtbqqq.geom))
where
	sample_attribute_id = 26
	and indicia.samples.entered_sref_system != 'mtbqqq'
	and text_value != v_mtbqqq.id;