package org.infinitenature.werbeo.service.core.test.support;

import java.time.LocalDateTime;
import java.util.UUID;

import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.common.position.impl.PositionFactoryImpl;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.Validation;

public class OccurrenceTestUtil
{

   private OccurrenceTestUtil()
   {
      throw new IllegalAccessError("Utitliy class");
   }

   public static final PositionFactory positonFacotry = new PositionFactoryImpl();

   public static Sample validSample()
   {
      Sample sample = new Sample();
      sample.setSurvey(validSurvey());
      sample.setId(UUID.randomUUID());
      sample.setDate(VagueDateFactory.create(2000));
      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(positonFacotry.createFromMTB("1943"));
      sample.setRecorder(validPerson());
      sample.getOccurrences().add(validOccurrence());
      sample.getLocality().setPrecision(100);
      return sample;
   }

   public static Person validPerson()
   {
      Person person = new Person();
      person.setId(12);
      person.setFirstName("Hans");
      person.setLastName("Bla");
      return person;
   }

   public static Survey validSurvey()
   {
      Survey survey = new Survey();
      survey.setId(33);
      survey.setName("A Survey");
      survey.setDescription("A Description");
      survey.setAvailability(Availability.RESTRICTED);
      survey.setPortal(validPortal());
      return survey;
   }

   public static Portal validPortal()
   {
      Portal portal = new Portal();
      portal.setId(1);
      portal.setTitle("Demonstatration Website");
      return portal;
   }

   public static Occurrence validOccurrence()
   {
      Occurrence occurrence = new Occurrence();
      occurrence.setId(UUID.randomUUID());
      occurrence.setDeterminer(validPerson());
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setTaxon(vaildTaxon());
      Validation validation = new Validation(ValidationStatus.VALID,
            "test@abc.de", LocalDateTime.of(2020, 10, 10, 10, 10),
            validPerson());
      validation.getValidator().setEmail("test@abc.de");
      validation.setComment("A validation comment");
      occurrence.setValidation(validation);
      return occurrence;
   }

   public static TaxonBase vaildTaxon()
   {
      TaxonBase taxon = new TaxonBase();
      taxon.setId(1);
      return taxon;
   }

}
