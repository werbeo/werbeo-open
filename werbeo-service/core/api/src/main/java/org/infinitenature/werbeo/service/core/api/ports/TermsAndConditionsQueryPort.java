package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.support.Context;

public interface TermsAndConditionsQueryPort
{
   /**
    * Returns the terms and conditions with markdown markup
    *
    * @param context
    * @return terms and conditions with markdown markup
    */
   String getTC(Context context);

}
