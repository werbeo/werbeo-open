package org.infinitenature.werbeo.service.core.api.enity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;
import org.infinitenature.werbeo.service.core.api.validation.ConfigurableSurveyField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Survey extends BaseTypeInt
{
   private Integer parentId;
   @NotBlank
   @ConfigurableSurveyField(SurveyField.DESCRIPTION)
   private String description;
   @ConfigurableSurveyField(SurveyField.OWNER)
   private Person owner;
   @NotBlank
   @ConfigurableSurveyField(SurveyField.NAME)
   private String name;
   @NotNull
   private Portal portal;
   @ConfigurableSurveyField(SurveyField.AVAILABILITY)
   private Availability availability;
   private boolean container = false;
   @ConfigurableSurveyField(SurveyField.DEPUTY_CUSTIODIANS)
   private Set<Person> deputyCustodians = new HashSet<>();
   private List<ObfuscationPolicy> obfuscationPolicies = new ArrayList<>();
   @ConfigurableSurveyField(SurveyField.WERBEO_ORIGINAL)
   private boolean werbeoOriginal = false;
   private boolean allowDataEntry = true;

   private Set<String> tags = new HashSet<>();

   public enum Availability
   {
      FREE(1), RESTRICTED(2), EMBARGO(3);

      private static Map<Integer, Availability> map = new HashMap<>();

      static
      {
         map.put(1, FREE);
         map.put(2, RESTRICTED);
         map.put(3, EMBARGO);
      }

      private int availability;

      private Availability(int avail)
      {
         this.availability = avail;
      }

      public String getStringRepresentation()
      {
         return String.valueOf(availability);
      }

      public Integer get()
      {
         return availability;
      }

      public static Availability get(Integer availability2)
      {
         return map.get(availability2);
      }

   }

   public Availability getAvailability()
   {
      return availability;
   }

   public void setAvailability(Availability availability)
   {
      this.availability = availability;
   }

   public boolean isContainer()
   {
      return container;
   }

   public void setContainer(boolean container)
   {
      this.container = container;
   }

   public Set<Person> getDeputyCustodians()
   {
      return deputyCustodians;
   }

   public void setDeputyCustodians(Set<Person> deputyCustodians)
   {
      this.deputyCustodians = deputyCustodians;
   }

   public Integer getParentId()
   {
      return parentId;
   }

   public void setParentId(Integer parentId)
   {
      this.parentId = parentId;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public Person getOwner()
   {
      return owner;
   }

   public void setOwner(Person owner)
   {
      this.owner = owner;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public Portal getPortal()
   {
      return portal;
   }

   public void setPortal(Portal portal)
   {
      this.portal = portal;
   }

   public boolean isWerbeoOriginal()
   {
      return werbeoOriginal;
   }

   public void setWerbeoOriginal(boolean werbeoOriginal)
   {
      this.werbeoOriginal = werbeoOriginal;
   }

   public boolean isAllowDataEntry()
   {
      return allowDataEntry;
   }

   public void setAllowDataEntry(boolean allowDataEntry)
   {
      this.allowDataEntry = allowDataEntry;
   }

   public Set<String> getTags()
   {
      return tags;
   }

   public void setTags(Set<String> tags)
   {
      this.tags = tags;
   }

   @Override
   public String toString()
   {
      return SurveyBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyBeanUtil.doEquals(this, obj);
   }

   public List<ObfuscationPolicy> getObfuscationPolicies()
   {
      return obfuscationPolicies;
   }

   public void setObfuscationPolicies(List<ObfuscationPolicy> obfuscationPolicies)
   {
      this.obfuscationPolicies = obfuscationPolicies;
   }

}
