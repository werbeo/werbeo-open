package org.infinitenature.werbeo.service.core.api.enity.support;

import java.util.ArrayList;
import java.util.List;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class PortalConfiguration
{
   private Integer defaultDataEntrySurveyId;
   private Double mapInitialZoom;
   private Double mapInitialLatitude;
   private Double mapInitialLongitude;
   private List<LayerName> mapOverlayLayers = new ArrayList<>();
   private int maxUploadSize;
   private List<ObfuscationPolicy> obfuscationPolicies = new ArrayList<>();
   private boolean allowAnonymousAccess = false;
   private List<CoordinateSystem> coordinateSystems = new ArrayList<>();
   private List<ExportPolicy> exportPolicies = new ArrayList<>();
   
   private StaticMapStyle staticMapStyle;

   public StaticMapStyle getStaticMapStyle()
   {
      return staticMapStyle;
   }

   public void setStaticMapStyle(StaticMapStyle staticMapStyle)
   {
      this.staticMapStyle = staticMapStyle;
   }

   public Integer getDefaultDataEntrySurveyId()
   {
      return defaultDataEntrySurveyId;
   }

   public void setDefaultDataEntrySurveyId(Integer defaultDataEntrySurveyId)
   {
      this.defaultDataEntrySurveyId = defaultDataEntrySurveyId;
   }

   public Double getMapInitialZoom()
   {
      return mapInitialZoom;
   }

   public void setMapInitialZoom(Double mapInitialZoom)
   {
      this.mapInitialZoom = mapInitialZoom;
   }

   public Double getMapInitialLatitude()
   {
      return mapInitialLatitude;
   }

   public void setMapInitialLatitude(Double mapInitialLatitude)
   {
      this.mapInitialLatitude = mapInitialLatitude;
   }

   public Double getMapInitialLongitude()
   {
      return mapInitialLongitude;
   }

   public void setMapInitialLongitude(Double mapInitialLongitude)
   {
      this.mapInitialLongitude = mapInitialLongitude;
   }

   public List<LayerName> getMapOverlayLayers()
   {
      return mapOverlayLayers;
   }

   public void setMapOverlayLayers(List<LayerName> mapOverlayLayers)
   {
      this.mapOverlayLayers = mapOverlayLayers;
   }

   public List<ObfuscationPolicy> getObfuscationPolicies()
   {
      return obfuscationPolicies;
   }

   public void setObfuscationPolicies(List<ObfuscationPolicy> obfuscationPolicies)
   {
      this.obfuscationPolicies = obfuscationPolicies == null ? new ArrayList<>()
            : obfuscationPolicies;
   }

   public List<ExportPolicy> getExportPolicies()
   {
      return exportPolicies;
   }

   public void setExportPolicies(List<ExportPolicy> exportPolicies)
   {
      this.exportPolicies = exportPolicies == null ? new ArrayList<>()
            : exportPolicies;
   }

   @Override
   public String toString()
   {
      return PortalConfigurationBeanUtil.doToString(this);
   }

   /**
    * @return max upload size in MB
    */
   public int getMaxUploadSize()
   {
      return maxUploadSize;
   }

   public void setMaxUploadSize(int maxUploadSize)
   {
      this.maxUploadSize = maxUploadSize;
   }

   public boolean isAllowAnonymousAccess()
   {
      return allowAnonymousAccess;
   }

   public void setAllowAnonymousAccess(boolean allowAnonymousAccess)
   {
      this.allowAnonymousAccess = allowAnonymousAccess;
   }

   @Override
   public int hashCode()
   {
      return PortalConfigurationBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalConfigurationBeanUtil.doEquals(this, obj);
   }

   public List<CoordinateSystem> getCoordinateSystems()
   {
      return coordinateSystems;
   }

   public void setCoordinateSystems(List<CoordinateSystem> coordinateSystems)
   {
      this.coordinateSystems = coordinateSystems;
   }
}
