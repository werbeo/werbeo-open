package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Medium
{
   private final String path;
   private final String description;
   private final MediaType type;

   public Medium(String path, String description, MediaType type)
   {
      super();
      this.path = path;
      this.description = description;
      this.type = type;
   }

   public String getPath()
   {
      return path;
   }

   public String getDescription()
   {
      return description;
   }

   public MediaType getType()
   {
      return type;
   }

   @Override
   public String toString()
   {
      return MediumBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return MediumBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return MediumBeanUtil.doEquals(this, obj);
   }
}
