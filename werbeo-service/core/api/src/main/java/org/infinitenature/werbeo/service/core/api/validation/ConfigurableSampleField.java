package org.infinitenature.werbeo.service.core.api.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;

@Retention(RUNTIME)
@Target(FIELD)
@Constraint(validatedBy = {})
@ConfigurableNotNullSampleField(value = SampleField.ID)
@ConfigurableNotAllowedSampleField(value = SampleField.ID)
@ConfigurableValuesSampleField(value = SampleField.ID)
public @interface ConfigurableSampleField
{
   String message() default "";

   Class<?>[] groups() default {};

   Class<? extends Payload>[] payload() default {};

   @OverridesAttribute(constraint = ConfigurableNotAllowedSampleField.class, name = "value")
   @OverridesAttribute(constraint = ConfigurableNotNullSampleField.class, name = "value")
   @OverridesAttribute(constraint = ConfigurableValuesSampleField.class, name = "value")
   SampleField value();
}
