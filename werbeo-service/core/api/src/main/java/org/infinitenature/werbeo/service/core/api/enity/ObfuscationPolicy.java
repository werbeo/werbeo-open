package org.infinitenature.werbeo.service.core.api.enity;


import org.infinitenature.werbeo.service.core.api.enums.Permission;
import org.infinitenature.werbeo.service.core.api.enums.Role;

public class ObfuscationPolicy
{
   private Role role;
   
   private Permission permission;
   
   public ObfuscationPolicy()
   {
      super();
   }

   public ObfuscationPolicy(String roleName, String permission)
   {
      this.role = Role.valueOf(roleName);
      this.permission = Permission.valueOf(permission);
   }

   public Role getRole()
   {
      return role;
   }

   public void setRole(Role role)
   {
      this.role = role;
   }

   public Permission getPermission()
   {
      return permission;
   }

   public void setPermission(Permission permission)
   {
      this.permission = permission;
   }
}
