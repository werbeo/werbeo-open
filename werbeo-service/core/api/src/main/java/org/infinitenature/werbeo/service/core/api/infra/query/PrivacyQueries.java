package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.werbeo.service.core.api.enity.Portal;

public interface PrivacyQueries
{
   /**
    * Returns the privacy declaration for the given portal
    *
    * @param portal
    * @return the privacy declaration formated with markdown
    */
   public String getPrivacyDeclaration(Portal portal);

   /**
    * Returns the default privacy declaration
    *
    * @return the privacy declaration formated with markdown
    */
   public String getDefaultPrivacyDeclaration();
}
