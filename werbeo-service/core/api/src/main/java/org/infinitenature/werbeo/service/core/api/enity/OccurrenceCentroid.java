package org.infinitenature.werbeo.service.core.api.enity;

import java.time.LocalDate;

import org.locationtech.jts.geom.Point;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceCentroid extends BaseTypeUUID
{
   private LocalDate date;
   // epsg: 31486
   private Point point;
   private int surveyId;

   public LocalDate getDate()
   {
      return date;
   }

   public void setDate(LocalDate date)
   {
      this.date = date;
   }

   public Point getPoint()
   {
      return point;
   }

   public void setPoint(Point point)
   {
      this.point = point;
   }

   public int getSurveyId()
   {
      return surveyId;
   }

   public void setSurveyId(int surveyId)
   {
      this.surveyId = surveyId;
   }

   @Override
   public String toString()
   {
      return OccurrenceCentroidBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceCentroidBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceCentroidBeanUtil.doEquals(this, obj);
   }
}
