package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.TaxonConfig;

public interface FieldConfigQueries
{
   SurveyConfig getSurveyConfig(Portal portal);

   OccurrenceConfig getOccurrenceConfig(Portal portal);

   SampleConfig getSampleConfig(Portal portal);

   TaxonConfig getTaxonConfig(Portal portal);
}
