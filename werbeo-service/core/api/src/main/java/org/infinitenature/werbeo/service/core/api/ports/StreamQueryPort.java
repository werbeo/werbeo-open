package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.infinitenature.werbeo.service.core.api.enity.support.StreamSlice;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface StreamQueryPort<ENTITY extends BaseType<ID>, ID>
{
   StreamSlice<ENTITY, ID> find(Context context,
         ContinuationToken continuationToken);
}
