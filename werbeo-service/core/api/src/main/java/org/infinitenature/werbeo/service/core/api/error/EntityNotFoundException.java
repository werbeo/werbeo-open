package org.infinitenature.werbeo.service.core.api.error;

public class EntityNotFoundException extends WerbeoException
{

   public EntityNotFoundException(String message)
   {
      super(message);
   }

}
