package org.infinitenature.werbeo.service.core.api.infra.query;

import java.util.UUID;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.StreamSlice;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceQueries
{
   public Slice<Occurrence, OccurrenceSortField> findOccurrences(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceSortField> offsetRequest);

   public long countOccurrences(OccurrenceFilter filter, Context context);

   public Occurrence get(UUID uuid, Context context);

   public StreamSlice<Occurrence, UUID> streamOccurrences(Context context,
         ContinuationToken continuationToken);

   public StreamSlice<Occurrence, UUID> streamOccurrencesForSinglePortal(
         Context context, ContinuationToken continuationToken);

   public Slice<OccurrenceCentroid, OccurrenceCentroidSortField> findOccurrenceCentroids(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceCentroidSortField> offsetRequest);

   public long countOcurrenceCentroids(OccurrenceFilter filter,
         Context context);

   public boolean notExists(UUID id);
}
