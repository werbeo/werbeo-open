package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.support.Context;

public interface AcceptanceQueryPort
{
   /**
    * Returns the privacy declaration with markdown markup
    *
    * @param context
    * @return privacy declaration with markdown markup
    */
   String getAcceptanceDeclaration(Context context);
}
