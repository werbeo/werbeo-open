package org.infinitenature.werbeo.service.core.api.infra.commands;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface PeopleCommands
{
   public int saveOrUpdate(Person person, Context context);

}
