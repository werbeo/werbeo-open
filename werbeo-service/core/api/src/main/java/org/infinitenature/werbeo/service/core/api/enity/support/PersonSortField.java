package org.infinitenature.werbeo.service.core.api.enity.support;

public enum PersonSortField
{
   ID, NAME
}
