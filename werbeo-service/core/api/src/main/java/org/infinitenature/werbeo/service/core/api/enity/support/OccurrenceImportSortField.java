package org.infinitenature.werbeo.service.core.api.enity.support;

public enum OccurrenceImportSortField
{
   ID, DATE
}
