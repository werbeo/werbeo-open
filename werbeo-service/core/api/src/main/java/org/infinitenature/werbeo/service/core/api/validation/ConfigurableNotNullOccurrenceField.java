package org.infinitenature.werbeo.service.core.api.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;

@Retention(RUNTIME)
@Target({ FIELD, ANNOTATION_TYPE })
@Constraint(validatedBy = WerbeoOccurrenceNotNullFieldValidator.class)
public @interface ConfigurableNotNullOccurrenceField
{
   String message() default "javax.validation.constraints.NotNull.message";

   Class<?>[] groups() default {};

   Class<? extends Payload>[] payload() default {};

   OccurrenceField value();

}
