package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface EntityQuery<ENTITY extends BaseType<?>, FILTER, ID, SORT>
{
   Slice<ENTITY, SORT> find(FILTER filter, Context context,
         OffsetRequest<SORT> offsetRequest);

   long count(FILTER filter, Context context);

   ENTITY get(ID id, Context context);
}
