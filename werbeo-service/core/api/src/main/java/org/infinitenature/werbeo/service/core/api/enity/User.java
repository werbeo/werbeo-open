package org.infinitenature.werbeo.service.core.api.enity;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.werbeo.service.core.api.enums.Role;

public class User extends BaseTypeInt
{
   /**
    * A dummy user for users hidden by obfusication process
    */
   public static final User HIDDEN_USER = new User(Integer.MIN_VALUE + 1,
         "hidden@user");
   public static final User INTERNAL_SUPER_USER = new User(Integer.MIN_VALUE,
         "internal@superuser");
   private String login;
   private Person person;

   private List<Role> roles = new ArrayList<>();

   public User()
   {
      super();
   }


   public User(String login)
   {
      super();
      this.login = login;
   }

   public User(String login, List<Role> roles)
   {
      super();
      this.login = login;
      this.roles = roles;
   }

   private User(int id, String login)
   {
      super(id);
      setLogin(login);
   }

   public Person getPerson()
   {
      return person;
   }

   public void setPerson(Person person)
   {
      this.person = person;
   }

   public String getLogin()
   {
      return login;
   }

   public void setLogin(String login)
   {
      if (login != null)
      {
         this.login = login.toLowerCase();
      } else
      {
         this.login = null;
      }
   }

   // Don't use default toString, hashCode and equals because we else get
   // circles between User and Person
   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("User [login=");
      builder.append(login);
      builder.append("]");
      return builder.toString();
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((login == null) ? 0 : login.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if(obj == null)
      {
         return false;
      }
      if (getClass() != obj.getClass())
      {
         return false;
      }
      User other = (User) obj;
      if (login == null)
      {
         if (other.login != null)
         {
            return false;
         }
      } else if (!login.equals(other.login))
      {
         return false;
      }
      return true;
   }

   public List<Role> getRoles()
   {
      return roles;
   }

   public void setRoles(List<Role> roles)
   {
      this.roles = roles;
   }

}
