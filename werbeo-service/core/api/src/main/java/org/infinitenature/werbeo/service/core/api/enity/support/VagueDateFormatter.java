package org.infinitenature.werbeo.service.core.api.enity.support;

import org.infinitenature.vaguedate.VagueDate;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class VagueDateFormatter
{
   public String format(VagueDate vagueDate)
   {
      switch (vagueDate.getType())
      {
      case DAY:
      case MONTH_IN_YEAR:
      case YEAR:
      case TO_YEAR:
         return vagueDate.getEndDate().format(
               DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.getDefault()));

      case FROM_YEAR:
         return vagueDate.getStartDate().format(
               DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.getDefault()));
      case DAYS:
      case YEARS:
      default:
         return vagueDate.getStartDate().format(
               DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.getDefault()))
               + " - " + vagueDate.getEndDate().format(
               DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.getDefault()));
      }
   }
}
