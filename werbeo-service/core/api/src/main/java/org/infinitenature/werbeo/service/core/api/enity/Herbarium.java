package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Herbarium
{
   private String code;
   private String herbary;

   public Herbarium()
   {
      super();
   }

   public Herbarium(String code, String herbary)
   {
      super();
      this.code = code;
      this.herbary = herbary;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getHerbary()
   {
      return herbary;
   }

   public void setHerbary(String herbary)
   {
      this.herbary = herbary;
   }

   @Override
   public String toString()
   {
      return HerbariumBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return HerbariumBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return HerbariumBeanUtil.doEquals(this, obj);
   }
}
