package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.werbeo.service.core.api.enity.Portal;

public interface TermsAndConditionsQueries
{

   /**
    * Returns the terms and conditions for the given portal
    *
    * @param portal
    * @return the terms and conditions formated with markdown
    */
   public String getTC(Portal portal);

   /**
    * Returns the default terms and conditions
    *
    * @return the terms and conditions formated with markdown
    */
   public String getDefaultTC();

}
