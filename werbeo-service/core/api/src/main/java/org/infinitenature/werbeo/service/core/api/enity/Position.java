package org.infinitenature.werbeo.service.core.api.enity;

import org.infintenature.mtb.MTB;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Position
{
   public enum PositionType
   {
      POINT, MTB, SHAPE, SQUARE
   }

   private PositionType type;
   private int epsg;
   private MTB mtb;
   private double[] posNW;
   private double[] posSE;
   private double[] posCenter;
   private String wkt;
   private int wktEpsg;
   private String code;
   private boolean incomplete;
   private String description;

   private boolean obfuscated = false;

   public PositionType getType()
   {
      return type;
   }

   public void setType(PositionType type)
   {
      this.type = type;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public int getEpsg()
   {
      return epsg;
   }

   public void setEpsg(int epsg)
   {
      this.epsg = epsg;
   }

   public MTB getMtb()
   {
      return mtb;
   }

   public void setMtb(MTB mtb)
   {
      this.mtb = mtb;
   }

   public double[] getPosNW()
   {
      return posNW;
   }

   public void setPosNW(double[] posNW)
   {
      this.posNW = posNW;
   }

   public double[] getPosSE()
   {
      return posSE;
   }

   public void setPosSE(double[] posSE)
   {
      this.posSE = posSE;
   }

   public double[] getPosCenter()
   {
      return posCenter;
   }

   public void setPosCenter(double[] posCenter)
   {
      this.posCenter = posCenter;
   }

   public String getWkt()
   {
      return wkt;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

   public int getWktEpsg()
   {
      return wktEpsg;
   }

   public void setWktEpsg(int wktEpsg)
   {
      this.wktEpsg = wktEpsg;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public boolean isIncomplete()
   {
      return incomplete;
   }

   public void setIncomplete(boolean incomplete)
   {
      this.incomplete = incomplete;
   }

   public boolean isObfuscated()
   {
      return obfuscated;
   }

   public void setObfuscated(boolean obfuscated)
   {
      this.obfuscated = obfuscated;
   }

   @Override
   public String toString()
   {
      return PositionBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PositionBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PositionBeanUtil.doEquals(this, obj);
   }
}
