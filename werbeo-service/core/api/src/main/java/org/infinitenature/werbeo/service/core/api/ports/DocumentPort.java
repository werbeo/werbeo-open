package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.HerbarySheet;
import org.infinitenature.werbeo.service.core.api.enums.HerbarySheetPosition;
import org.infinitenature.werbeo.service.core.api.support.Context;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

public interface DocumentPort
{
   public ByteArrayOutputStream createHerbaryPage(Context context, UUID occurrenceUuid,
         HerbarySheetPosition position, HerbarySheet sheet);
}
