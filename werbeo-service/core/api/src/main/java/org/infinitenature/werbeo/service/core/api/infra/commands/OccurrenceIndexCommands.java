package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.util.Collection;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Survey;

public interface OccurrenceIndexCommands
{
   public void index(Collection<Occurrence> occurrences, boolean enforceCommit);

   public void delete(Collection<UUID> ids);

   public void delete(UUID id);

   public void update(Survey survey);

   public void deleteBySampleId(UUID sampleId);
}
