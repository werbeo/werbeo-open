package org.infinitenature.werbeo.service.core.api.ports;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Language;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxonFilter
{
   private final String nameContains;
   private final Set<Language> languages;
   private final boolean includeSynonyms;
   private final boolean onlyUsedTaxa;
   private final LocalDate occurrencesChangedSince;
   private Set<String> externalKeys = new HashSet<>();
   private final String taxaGroup;
   private boolean onlyTaxaAvailableForInput;
   public Set<String> getExternalKeys()
   {
      return externalKeys;
   }

   public TaxonFilter(String nameContains, Set<Language> languages,
         boolean includeSynonyms, LocalDate occurrencesChangedSince,
         boolean onlyUsedTaxa, Set<String> externalKeys, String taxaGroup,
         boolean onlyTaxaAvailableForInput)
   {
      super();
      this.nameContains = nameContains;
      this.languages = Collections
            .unmodifiableSet(languages == null ? new HashSet<>() : languages);
      this.includeSynonyms = includeSynonyms;
      this.occurrencesChangedSince = occurrencesChangedSince;
      this.onlyUsedTaxa = onlyUsedTaxa;
      this.externalKeys = externalKeys;
      this.taxaGroup = taxaGroup;
      this.onlyTaxaAvailableForInput = onlyTaxaAvailableForInput;
   }

   public boolean isIncludeSynonyms()
   {
      return includeSynonyms;
   }

   public String getNameContains()
   {
      return nameContains;
   }

   public Set<Language> getLanguages()
   {
      return new HashSet<>(languages);
   }

   public LocalDate getOccurrencesChangedSince()
   {
      return occurrencesChangedSince;
   }

   public boolean isOnlyUsedTaxa()
   {
      return onlyUsedTaxa;
   }

   public String getTaxaGroup()
   {
      return taxaGroup;
   }

   @Override
   public String toString()
   {
      return TaxonFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonFilterBeanUtil.doEquals(this, obj);
   }

   public boolean isOnlyTaxaAvailableForInput()
   {
      return onlyTaxaAvailableForInput;
   }

   public void setOnlyTaxaAvailableForInput(boolean onlyTaxaAvailableForInput)
   {
      this.onlyTaxaAvailableForInput = onlyTaxaAvailableForInput;
   }
}
