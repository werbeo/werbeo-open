package org.infinitenature.werbeo.service.core.api.ports;

import java.util.Locale;
import java.util.Map;

import org.infinitenature.werbeo.service.core.api.support.Context;

public interface TranslationQueryPort
{
   String getTranslation(Context context, String key, Locale locale);

   Map<String, String> getTranslations(Context context, Locale locale);
}
