package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.ImportPart;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface ImportCommands
{
   public ImportJob storeImportfile(String importXML, Context context);
   public ImportJob storeCSVImportfile(String importCSV, Context context);

   public void saveOrUpdate(ImportPart importPart);

   public Optional<ImportPart> getNextAndSetRunning(ImportJob importJob);

   ImportJob setJobStatus(ImportJob importJob, JobStatus running,
         Context context);

   public void writeFailureReport(ImportJob importJob,
         Map<String, Set<UUID>> failures);

   public void addLogFile(ImportJob importJob, Path logFile, Context context);

   public void removeLock(ImportJob importJob);
   void updateCSV(String csv, Context context, ImportJob importJob);
   void updateProtocol(String protocol, Context context, ImportJob importJob);
   void updateErrorLog(String protocol, Context context, ImportJob importJob);

}
