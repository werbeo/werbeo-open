package org.infinitenature.werbeo.service.core.api.enity.meta.fields;

public enum OccurrenceField implements WerbeoField<OccurrenceField>
{
   ID(true), //
   DETERMINER(true), //
   RECORD_STATUS, //
   TAXON(true), //
   SAMPLE(true), //
   HABITAT(false),
   LIFE_STAGE,
   MAKROPTER,
   AMOUNT,
   SETTLEMENT_STATUS,
   AREA(),
   VITALITY,
   SETTLEMENT_STATUS_FUKAREK,
   BLOOMING_SPROUTS,
   QUANTITY,
   EXTERNAL_KEY,
   OBSERVERS,
   HERBARIUM,
   DETERMINATION_COMMENT,
   NUMERIC_AMOUNT,
   SEX(),
   REPRODUCTION,
   MEDIA,
   PUBLICATION,
   ABSENCE,
   TIME_OF_DAY, NUMERIC_AMOUNT_ACCURACY, VALIDATION, REMARK, CITE_ID, CITE_COMMENT;

   private final boolean globalMandantory;

   OccurrenceField()
   {
      this.globalMandantory = false;
   }

   OccurrenceField(boolean globalMandantory)
   {
      this.globalMandantory = globalMandantory;
   }

   @Override
   public boolean isGlobalMandantory()
   {
      return globalMandantory;
   }
}
