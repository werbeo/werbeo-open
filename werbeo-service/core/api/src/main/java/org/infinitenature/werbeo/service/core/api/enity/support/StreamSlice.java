package org.infinitenature.werbeo.service.core.api.enity.support;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.BaseType;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class StreamSlice<ENTITY extends BaseType<ID>, ID>
{
   private ContinuationToken continuationToken;
   private List<ENTITY> entities = new ArrayList<>();
   private List<ID> deletedIds = new ArrayList<>();

   public void addDeleted(ID entityId)
   {
      deletedIds.add(entityId);
   }

   public void addEntity(ENTITY entity)
   {
      entities.add(entity);
   }

   public void setContinuationToken(ContinuationToken continuationToken)
   {
      this.continuationToken = continuationToken;
   }

   public ContinuationToken getContinuationToken()
   {
      return continuationToken;
   }

   public List<ENTITY> getEntities()
   {
      return entities;
   }

   public List<ID> getDeletedIds()
   {
      return deletedIds;
   }
}
