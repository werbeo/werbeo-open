package org.infinitenature.werbeo.service.core.access;

public enum Operation
{
   CREATE, READ, UPDATE, DELETE
}
