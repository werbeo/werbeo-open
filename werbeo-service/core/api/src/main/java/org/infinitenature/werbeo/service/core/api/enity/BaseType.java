package org.infinitenature.werbeo.service.core.api.enity;

import java.time.LocalDateTime;
import java.util.Set;

import org.infinitenature.werbeo.service.core.access.Operation;

public interface BaseType<ID_TYPE>
{

   LocalDateTime getCreationDate();

   void setCreationDate(LocalDateTime creationDate);

   User getCreatedBy();

   void setCreatedBy(User createdBy);

   LocalDateTime getModificationDate();

   void setModificationDate(LocalDateTime modificationDate);

   User getModifiedBy();

   void setModifiedBy(User modifiedBy);

   ID_TYPE getId();

   void setId(ID_TYPE id);

   Set<Operation> getAllowedOperations();

   void setAllowedOperations(Set<Operation> allowedOperations);

   boolean isObfuscated();

   void setObfuscated(boolean obfuscated);

}