package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface CommandPort<ENTITY extends BaseType<ID>, ID>
{
   public ID saveOrUpdate(ENTITY entity, Context context);

   public void delete(ID entity, Context context);

}
