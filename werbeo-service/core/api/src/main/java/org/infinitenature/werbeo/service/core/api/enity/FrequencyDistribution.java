package org.infinitenature.werbeo.service.core.api.enity;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class FrequencyDistribution
{
   private final ChronoUnit gap;
   private final SortedMap<ZonedDateTime, Integer> data;

   public FrequencyDistribution(ChronoUnit gap, Map<ZonedDateTime, Integer> data)
   {
      super();
      this.gap = gap;
      this.data = Collections.unmodifiableSortedMap(new TreeMap<>(data));
   }

   public ChronoUnit getGap()
   {
      return gap;
   }

   public SortedMap<ZonedDateTime, Integer> getData()
   {
      return data;
   }

   @Override
   public String toString()
   {
      return FrequencyDistributionBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return FrequencyDistributionBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return FrequencyDistributionBeanUtil.doEquals(this, obj);
   }
}
