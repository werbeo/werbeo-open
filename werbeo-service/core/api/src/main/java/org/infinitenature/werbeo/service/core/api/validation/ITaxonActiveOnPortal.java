package org.infinitenature.werbeo.service.core.api.validation;

import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;

public interface ITaxonActiveOnPortal
{

   boolean isValid(TaxonBase taxon);

}
