package org.infinitenature.werbeo.service.core.api.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;

@Retention(RUNTIME)
@Target({ FIELD, ANNOTATION_TYPE })
@Constraint(validatedBy = WerbeoSampleNotAllowedValidator.class)
public @interface ConfigurableNotAllowedSampleField
{
   String message() default "werbeo.fieldnotactivated.message";

   Class<?>[] groups() default {};

   Class<? extends Payload>[] payload() default {};

   SampleField value();
}
