package org.infinitenature.werbeo.service.core.api.enums;

public enum Role
{
   ANONYMOUS("ANONYMOUS"), APPROVED("APPROVED"), ADMIN("ADMIN"), ACCEPTED("ACCEPTED"), VALIDATOR(
         "VALIDATOR");

   private final String roleName;

   private Role(String roleName)
   {
      this.roleName = roleName;
   }

   public String getRoleName()
   {
      return roleName;
   }

}
