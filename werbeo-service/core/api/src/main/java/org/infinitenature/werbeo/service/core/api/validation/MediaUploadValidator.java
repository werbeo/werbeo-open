package org.infinitenature.werbeo.service.core.api.validation;

public interface MediaUploadValidator
{
   public boolean sizeIsValid(int length);
}
