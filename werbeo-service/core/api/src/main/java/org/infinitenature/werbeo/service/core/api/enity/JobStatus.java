package org.infinitenature.werbeo.service.core.api.enity;

public enum JobStatus
{
   // before
   CREATING,
   INITIALIZED_CSV,
   INITIALIZED,
   PREPARED,
   // while
   RUNNING,RUNNING_CSV,
   // after
   CANCELD, FINISHED, REMOVED, FAILURE
   // running a job
}
