package org.infinitenature.werbeo.service.core.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.springframework.beans.factory.annotation.Autowired;

public class TaxonAcitveOnPortalValidator
      implements ConstraintValidator<TaxonActiveOnPortal, TaxonBase>
{

   @Autowired
   private ITaxonActiveOnPortal taxonActiveOnPortal;

   @Override
   public boolean isValid(TaxonBase taxon, ConstraintValidatorContext context)
   {
      return taxonActiveOnPortal.isValid(taxon);
   }

}
