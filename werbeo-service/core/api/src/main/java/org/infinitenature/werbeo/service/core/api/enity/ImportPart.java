package org.infinitenature.werbeo.service.core.api.enity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class ImportPart
{
   private ImportJob importJob;
   private Sample sample;
   private List<SampleComment> sampleComments = new ArrayList<>();
   private Map<Occurrence, List<OccurrenceComment>> occurrenceComments = new HashMap<>();
   private Optional<ImportFailure> failureReason = Optional.empty();
   private JobStatus status;

   public Sample getSample()
   {
      return sample;
   }

   public void setSample(Sample sample)
   {
      this.sample = sample;
   }

   public List<SampleComment> getSampleComments()
   {
      return sampleComments;
   }

   public void setSampleComments(List<SampleComment> sampleComments)
   {
      this.sampleComments = sampleComments;
   }

   public Map<Occurrence, List<OccurrenceComment>> getOccurrenceComments()
   {
      return occurrenceComments;
   }

   public void setOccurrenceComments(
         Map<Occurrence, List<OccurrenceComment>> occurrenceComments)
   {
      this.occurrenceComments = occurrenceComments;
   }

   public ImportJob getImportJob()
   {
      return importJob;
   }

   public void setImportJob(ImportJob importJob)
   {
      this.importJob = importJob;
   }

   public Optional<ImportFailure> getFailureReason()
   {
      return failureReason;
   }

   public void setFailureReason(Optional<ImportFailure> failureReason)
   {
      this.failureReason = failureReason;
   }

   public JobStatus getStatus()
   {
      return status;
   }

   public void setStatus(JobStatus status)
   {
      this.status = status;
   }
}
