package org.infinitenature.werbeo.service.core.api.enity;

import java.util.UUID;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class ImportJob extends BaseTypeUUID
{
   private JobStatus status;

   public ImportJob(UUID id, JobStatus status)
   {
      super();
      this.status = status;
      setId(id);
   }

   public JobStatus getStatus()
   {
      return status;
   }

   public void setStatus(JobStatus status)
   {
      this.status = status;
   }

   @Override
   public String toString()
   {
      return ImportJobBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ImportJobBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ImportJobBeanUtil.doEquals(this, obj);
   }
}
