package org.infinitenature.werbeo.service.core.api.enity;

public enum Language
{
   DEU, LAT, ENG, CYM, GLA, GLE
}
