package org.infinitenature.werbeo.service.core.api.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RUNTIME)
@Target({ FIELD, ANNOTATION_TYPE })
@Constraint(validatedBy = UploadNotTooBigValidator.class)
public @interface UploadNotTooBig
{
   String message() default "werbeo.uploadTooBig.message";

   Class<?>[] groups() default {};

   Class<? extends Payload>[] payload() default {};

}
