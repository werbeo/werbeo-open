package org.infinitenature.werbeo.service.core.api.enity.meta;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SampleConfig extends EntityConfig<SampleFieldConfig, SampleField>
{

   private List<SampleMethod> availableMethods = new ArrayList<>();
   public SampleConfig()
   {
      super();
   }

   public SampleConfig(Portal portal,
         Set<SampleFieldConfig> configuredFields,
         List<SampleMethod> availableMethods)
   {
      super(portal, configuredFields);
      this.availableMethods = availableMethods;
   }

   public List<SampleMethod> getAvailableMethods()
   {
      return availableMethods;
   }

   public void setAvailableMethods(List<SampleMethod> availableMethods)
   {
      this.availableMethods = availableMethods;
   }

   @Override
   public String toString()
   {
      return SampleConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleConfigBeanUtil.doEquals(this, obj);
   }
}
