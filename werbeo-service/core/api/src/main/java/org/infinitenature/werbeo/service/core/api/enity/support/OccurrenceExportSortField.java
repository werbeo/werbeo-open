package org.infinitenature.werbeo.service.core.api.enity.support;

public enum OccurrenceExportSortField
{
   ID, DATE
}
