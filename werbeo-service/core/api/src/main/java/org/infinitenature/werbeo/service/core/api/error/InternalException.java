package org.infinitenature.werbeo.service.core.api.error;

public class InternalException extends WerbeoException
{

   public InternalException(String message, Throwable cause)
   {
      super(message, cause);
   }

}
