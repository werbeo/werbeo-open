package org.infinitenature.werbeo.service.core.api.validation;

public interface WKTValidator
{

   boolean isValid(String value);

}
