package org.infinitenature.werbeo.service.core.api.enity.meta;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.WerbeoField;

public abstract class EntityConfig<T extends FieldConfig<?>, E extends WerbeoField<?>>
{
   private Portal portal;

   private Set<T> configuredFields = new HashSet<>();

   public EntityConfig()
   {
      super();
   }

   public EntityConfig(Portal portal, Set<T> configuredFields)
   {
      super();
      this.portal = portal;
      this.configuredFields = configuredFields;
   }

   public Portal getPortal()
   {
      return portal;
   }

   public void setPortal(Portal portal)
   {
      this.portal = portal;
   }

   public Set<T> getConfiguredFields()
   {
      return configuredFields;
   }

   public void setConfiguredFields(Set<T> configuredFields)
   {
      this.configuredFields = configuredFields;
   }

   public Optional<T> getFieldConfig(E field)
   {
      return configuredFields.stream().filter(fc -> fc.getField().equals(field))
            .findFirst();
   }

}
