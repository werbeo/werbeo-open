package org.infinitenature.werbeo.service.core.api.enity.support;

import org.apache.commons.lang3.Validate;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class ContinuationToken
{
   private static final String DELEMITER = "_";
   public static final ContinuationToken FIRST = new ContinuationToken(0, 0);
   private final int id;
   private final long timestamp;

   public ContinuationToken(String serializedToken)
   {
      if (serializedToken == null)
      {
         throw new IllegalArgumentException("The token may not be null!");
      }
      Validate.notBlank(serializedToken, "The token may not be empty");
      String[] split = serializedToken.split(DELEMITER);
      this.id = Integer.valueOf(split[0]);
      this.timestamp = Long.valueOf(split[1]);
   }

   public ContinuationToken(int id, long timestamp)
   {
      Validate.inclusiveBetween(0, Integer.MAX_VALUE, id);
      Validate.inclusiveBetween(0, Long.MAX_VALUE, timestamp);
      this.id = id;
      this.timestamp = timestamp;
   }

   public int getId()
   {
      return id;
   }

   public long getTimestamp()
   {
      return timestamp;
   }

   public String serialize()
   {
      return id + DELEMITER + timestamp;
   }

   @Override
   public String toString()
   {
      return ContinuationTokenBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ContinuationTokenBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ContinuationTokenBeanUtil.doEquals(this, obj);
   }
}
