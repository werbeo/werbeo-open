package org.infinitenature.werbeo.service.core.api.infra.commands;

import org.infinitenature.werbeo.service.core.api.enity.User;

public interface RolesCommands
{
   void addRole(User user, String role);
   void removeRole(User user, String role);

}
