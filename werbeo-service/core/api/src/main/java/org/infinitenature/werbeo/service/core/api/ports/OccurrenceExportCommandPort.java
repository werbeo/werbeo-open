package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.support.Context;

import java.util.UUID;

public interface OccurrenceExportCommandPort
{
   UUID create(OccurrenceFilter filter, FileFormat format, Context context);

   void delete(UUID exportId, Context context);
}
