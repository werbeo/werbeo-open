package org.infinitenature.werbeo.service.core.api.enity;

public enum MediaType
{
   /**
    * An URL to an external image
    */
   IMAGE_LINK,
   /**
    * The name of an image stored in werbeo
    */
   IMAGE_LOCAL,
   /**
    * An URL to an external audio file
    */
   AUDIO_LINK,
   /**
    * The name on an audio file stored in werbeo
    */
   AUDIO_LOCAL
}
