package org.infinitenature.werbeo.service.core.api.ports;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.CSVImport;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceImportSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceCSVImportQueryPort
      extends QueryPort<CSVImport, Void, UUID, OccurrenceImportSortField>
{
   TypedFile load(UUID importtId, Context context);
   TypedFile loadErrorLog(UUID importtId, Context context);
}
