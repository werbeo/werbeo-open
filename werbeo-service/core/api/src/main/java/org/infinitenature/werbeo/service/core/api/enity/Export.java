package org.infinitenature.werbeo.service.core.api.enity;

import java.time.LocalDateTime;
import java.util.UUID;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Export extends BaseTypeUUID
{
   private JobStatus status;
   private String fileFormat;
   
   public Export(UUID id, JobStatus jobStatus, String fileFormat, LocalDateTime createDate)
   {
      super();
      this.setId(id);
      this.status = jobStatus;
      this.fileFormat = fileFormat;
      setCreationDate(createDate);
   }

   public JobStatus getStatus()
   {
      return status;
   }

   public void setStatus(JobStatus status)
   {
      this.status = status;
   }

   public String getFileFormat()
   {
      return fileFormat;
   }

   public void setFileFormat(String fileFormat)
   {
      this.fileFormat = fileFormat;
   }

   @Override
   public String toString()
   {
      return ExportBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ExportBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ExportBeanUtil.doEquals(this, obj);
   }
}
