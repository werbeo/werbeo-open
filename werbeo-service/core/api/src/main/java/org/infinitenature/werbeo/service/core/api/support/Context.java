package org.infinitenature.werbeo.service.core.api.support;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.enity.Group;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Context
{
   private final Portal portal;
   private final User user;
   private final Set<Group> groups;
   private final String requestId;
   private final Optional<PortalConfiguration> portalConfiguration;

   /**
    * @deprecated use
    *             {@link Context#Context(Portal, User, Set, PortalConfiguration)}
    *             instead
    */
   @Deprecated
   public Context(Portal portal, User user, Set<Group> groups, String requestId)
   {
      this.portal = Validate.notNull(portal, "The portal may not be null");
      this.user = user;
      this.groups = Validate.notNull(groups,
            "Groups must not be null, use an empty collection instead");
      if (StringUtils.isNotBlank(requestId))
      {
         this.requestId = requestId;
      } else
      {
         this.requestId = "werbeo-service-generated-" + UUID.randomUUID();
      }
      this.portalConfiguration = Optional.empty();
   }

   public Context(Portal portal, User user, Set<Group> groups,
         PortalConfiguration portalConfiguration)
   {
      this(portal, user, groups, portalConfiguration, null);
   }

   public Context(Portal portal, User user, Set<Group> groups,
         PortalConfiguration portalConfiguration, String requestId)
   {
      this.portal = Validate.notNull(portal, "The portal may not be null");
      this.user = user;
      this.groups = Validate.notNull(groups,
            "Groups must not be null, use an empty collection instead");
      if (StringUtils.isNotBlank(requestId))
      {
         this.requestId = requestId;
      } else
      {
         this.requestId = "werbeo-service-generated" + UUID.randomUUID();
      }
      this.portalConfiguration = Optional.of(portalConfiguration);
   }

   public Portal getPortal()
   {
      return portal;
   }

   public User getUser()
   {
      return user;
   }

   public boolean isUserLoggedIn()
   {
      return user != null;
   }

   public boolean isAccepted()
   {
      return hasRole(Roles.ACCEPTED);
   }

   public boolean isUserApproved()
   {
      Roles role = Roles.APPROVED;
      return hasRole(role);
   }

   public boolean isAdmin()
   {
      Roles role = Roles.ADMIN;
      return hasRole(role);
   }

   /**
    * Checks if the current user has the given role for the current Portal
    *
    * @param role
    * @return
    */
   public boolean hasRole(Roles role)
   {
      return hasRoleForPortal(role, portal);
   }

   /**
    * Checks if the current user has the given role for a portal
    *
    * @param role
    * @param currentPortal
    * @return
    */
   public boolean hasRoleForPortal(Roles role, Portal currentPortal)
   {
      return getGroups().contains(new Group(
            RoleNameFactory.getKeycloakRoleName(currentPortal.getId(), role)));
   }

   public Set<Group> getGroups()
   {
      return Collections.unmodifiableSet(groups);
   }

   public String getRequestId()
   {
      return requestId;
   }

   public Optional<PortalConfiguration> getPortalConfiguration()
   {
      return portalConfiguration;
   }

   @Override
   public String toString()
   {
      return ContextBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ContextBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ContextBeanUtil.doEquals(this, obj);
   }
}
