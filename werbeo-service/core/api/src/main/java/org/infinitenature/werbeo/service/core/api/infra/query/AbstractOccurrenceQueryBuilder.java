package org.infinitenature.werbeo.service.core.api.infra.query;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter.WKTMode;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTB;

public abstract class AbstractOccurrenceQueryBuilder<T>
{

   protected abstract T initQuery(Context context);

   public T buildQuery(OccurrenceFilter filter, Context context)
   {
      T query = initQuery(context);

      query = appendWebsiteAgreements(context, query);

      if (filter.getFrom() != null)
      {
         query = appendFrom(filter.getFrom(), query);
      }
      if (filter.getTo() != null)
      {
         query = appendTo(filter.getTo(), query);
      }
      if (filter.getTaxonId() != null)
      {
         query = appendTaxon(filter.getTaxonId(), filter.isIncludeChildTaxa(),
               query);
      }
      if (filter.getMaxBlur() != null)
      {
         query = appendMaxBlur(filter.getMaxBlur(), query);
      }
      if (filter.getWkt() != null)
      {
         query = appendWkt(filter.getWkt(), filter.getWktMode(), query);
      }
      if (filter.getMtb() != null)
      {
         query = appendMTB(filter.getMtb(), query);
      }
      if (filter.getOwner() != null)
      {
         query = appendOwner(filter.getOwner(), query);
      }
      if (filter.getSurveyId() != null)
      {
         query = appendSurveyId(filter.getSurveyId(), query);
      }
      if (filter.getAbsence() != null)
      {
         query = appendAbsence(filter.getAbsence(), query);
      }
      if (filter.getHerbaryCode() != null)
      {
         query = appendHerbaryCode(filter.getHerbaryCode(), query);
      }
      if (filter.getPortalBasedFilters() != null)
      {
         query = appendPortalBasedFilters(context,
               filter.getPortalBasedFilters(), query);
      }
      if (filter.getValidationStatus() != null)
      {
         query = appendValidationStatus(filter.getValidationStatus(), query);
      }
      if (filter.getValidator() != null)
      {
         query = appendValidator(filter.getValidator(), query);
      }
      if (StringUtils.isNotBlank(filter.getOccExternalKey()))
      {
         query = appendOccurrenceExternalKey(filter.getOccExternalKey(), query);
      }
      if (filter.getModifiedSince() != null)
      {
         query=appendModifiedSince(filter.getModifiedSince(), query);
      }
      return query;
   }

   protected abstract T appendValidator(String validator, T query);

   protected abstract T appendValidationStatus(
         ValidationStatus validationStatus, T query);

   protected abstract T appendMTB(MTB mtb, T query);

   protected abstract T appendWebsiteAgreements(Context context, T query);

   protected abstract T appendOwner(String owner, T query);

   protected abstract T appendWkt(String wkt, WKTMode wktMode, T query);

   protected abstract T appendMaxBlur(int maxBlur, T query);

   protected abstract T appendTaxon(Integer taxonId, boolean includeChildTaxa,
         T query);

   protected abstract T appendTo(LocalDate to, T query);

   protected abstract T appendFrom(LocalDate from, T query);

   protected abstract T appendSurveyId(int surveyId, T query);

   protected abstract T appendAbsence(boolean absence, T query);

   protected abstract T appendHerbaryCode(String herbaryCode, T query);

   protected abstract T appendPortalBasedFilters(Context context,
         Map<Filter, Collection<Portal>> portalBasedFilters, T query);

   protected abstract T appendOccurrenceExternalKey(String occExternalKey,
         T query);

   protected abstract T appendModifiedSince(LocalDateTime modifiedSince,
         T query);
}
