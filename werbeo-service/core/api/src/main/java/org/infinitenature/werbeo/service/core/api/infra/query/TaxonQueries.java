package org.infinitenature.werbeo.service.core.api.infra.query;

import java.util.List;
import java.util.Set;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface TaxonQueries
      extends EntityQuery<Taxon, TaxonFilter, Integer, TaxonSortField>
{
   Slice<TaxonBase, TaxonSortField> findBase(TaxonFilter filter, Context context,
         OffsetRequest<TaxonSortField> offsetRequest);

   Set<TaxonBase> loadPrefferedChildren(TaxonBase taxon);

   List<Taxon> findAllSynonyms(Integer taxonId, Context context);

   Set<TaxonBase> findParrentSwarm(TaxonBase taxon);

   TaxonBase getByExternalId(int taxaListId, String externalId);
}
