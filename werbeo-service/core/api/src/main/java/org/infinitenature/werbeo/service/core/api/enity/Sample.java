package org.infinitenature.werbeo.service.core.api.enity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Sample extends SampleBase
{
   @NotNull
   @NotEmpty
   @Valid
   private List<Occurrence> occurrences = new ArrayList<>();

   public List<Occurrence> getOccurrences()
   {
      return occurrences;
   }

   public void setOccurrences(List<Occurrence> occurrences)
   {
      this.occurrences = Objects.requireNonNull(occurrences,
            "The occurrence collection may not be null");
   }

   @Override
   public String toString()
   {
      return SampleBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleBeanUtil.doEquals(this, obj);
   }

}
