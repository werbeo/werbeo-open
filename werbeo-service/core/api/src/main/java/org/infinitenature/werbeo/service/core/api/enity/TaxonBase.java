package org.infinitenature.werbeo.service.core.api.enity;

import java.util.HashSet;
import java.util.Set;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxonBase extends BaseTypeInt
{
   private String name;

   private String externalKey;

   private String group;

   private String authority;

   private boolean allowDataEntry;

   private TaxaList taxaList;

   private Set<DocumentType> documentsAvailable = new HashSet<>();

   private boolean preferred;

   public Set<DocumentType> getDocumentsAvailable()
   {
      return documentsAvailable;
   }

   public void setDocumentsAvailable(Set<DocumentType> documentsAvailable)
   {
      this.documentsAvailable = documentsAvailable;
   }

   public TaxaList getTaxaList()
   {
      return taxaList;
   }

   public void setTaxaList(TaxaList taxaList)
   {
      this.taxaList = taxaList;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public boolean isAllowDataEntry()
   {
      return allowDataEntry;
   }

   public void setAllowDataEntry(boolean allowDataEntry)
   {
      this.allowDataEntry = allowDataEntry;
   }

   public boolean isPreferred()
   {
      return preferred;
   }

   public void setPreferred(boolean preferred)
   {
      this.preferred = preferred;
   }

   @Override
   public String toString()
   {
      return TaxonBaseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonBaseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonBaseBeanUtil.doEquals(this, obj);
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public String getGroup()
   {
      return group;
   }

   public void setGroup(String group)
   {
      this.group = group;
   }

   public String getAuthority()
   {
      return authority;
   }

   public void setAuthority(String authority)
   {
      this.authority = authority;
   }
}
