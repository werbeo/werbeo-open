package org.infinitenature.werbeo.service.core.api.enity;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.validation.ConfigurableOccurrenceField;
import org.infinitenature.werbeo.service.core.api.validation.TaxonActiveOnPortal;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Occurrence extends BaseTypeUUID
{

   public enum Amount
   {
       ONE, FEW, SCATTERED, SOME, A_LOT
   }

   public enum NumericAmountAccuracy
   {
      COUNTED, MORETHAN, APPROXIMATE
   }

   public enum SettlementStatus
   {
      PLANTED, RESETTELD, WILD
   }

   public enum Area
   {
      LESS_ONE, ONE_TO_FIVE, SIX_TO_TWENTYFIVE, TWENTYSIX, FIFTY, HUNDRED, THOUSAND, TENTHOUSAND
   }

   public enum Vitality
   {
      VITAL, KUEMMERND, ABSTERBEND, ABGESTORBEN
   }

   public enum SettlementStatusFukarek
   {
      UNBEKANNT, INDIGEN, EINGEBUERGERT, UNBESTAENDIG, SYNANTHROP, ANGESIEDELT, ANGESALBT_KULTIVIERT
   }

   public enum BloomingSprouts
   {
      @Deprecated
      LESS_ONE, ONE_TO_FIVE, SIX_TO_TWENTYFIVE, TWENTYSIX, FIFTY, HUNDRED, THOUSAND, TENTHOUSAND
   }

   public enum Quantity
   {
      ONE_TO_FIVE, SIX_TO_TWENTYFIVE, TWENTYSIX, FIFTY, HUNDRED, THOUSAND, TENTHOUSAND
   }

   public enum Sex
   {
      MALE, FEMALE, UNKNOWN, BOTH
   }

   public enum Reproduction
   {
      SURE, PLAUSIBLE, UNLIKELY, UNKNOWN
   }

   public enum ValidationStatus
   {
      VALID(10), PROBABLE(20), UNKNOWN(30), IMPROBALE(40), INVALID(50);

      public final int sortOrder;

      ValidationStatus(int sortOrder)
      {
         this.sortOrder = sortOrder;
      }

   }

   public enum LifeStage
   {
      UNKNOWN, LARVE, IMAGO, LARVE_IMAGO;
   }

   public enum Makropter
   {
      UNKNOWN, YES, NO;
   }

   @NotNull
   private Person determiner;
   @NotNull
   private RecordStatus recordStatus;
   @ConfigurableOccurrenceField(OccurrenceField.LIFE_STAGE)
   private LifeStage lifeStage;
   @ConfigurableOccurrenceField(OccurrenceField.MAKROPTER)
   private Makropter makropter;
   @ConfigurableOccurrenceField(OccurrenceField.AMOUNT)
   private Amount amount;
   @ConfigurableOccurrenceField(OccurrenceField.SETTLEMENT_STATUS)
   private SettlementStatus settlementStatus;
   private SampleBase sample;
   @TaxonActiveOnPortal
   @ConfigurableOccurrenceField(OccurrenceField.TAXON)
   private TaxonBase taxon;
   @ConfigurableOccurrenceField(OccurrenceField.AREA)
   private Area coveredArea;
   @ConfigurableOccurrenceField(OccurrenceField.VITALITY)
   private Vitality vitality;
   @ConfigurableOccurrenceField(OccurrenceField.SETTLEMENT_STATUS_FUKAREK)
   private SettlementStatusFukarek settlementStatusFukarek;
   @ConfigurableOccurrenceField(OccurrenceField.BLOOMING_SPROUTS)
   private BloomingSprouts bloomingSprouts;
   @ConfigurableOccurrenceField(OccurrenceField.QUANTITY)
   private Quantity quantity;
   @ConfigurableOccurrenceField(OccurrenceField.EXTERNAL_KEY)
   private String externalKey;
   @ConfigurableOccurrenceField(OccurrenceField.OBSERVERS)
   private String observers;
   @ConfigurableOccurrenceField(OccurrenceField.HERBARIUM)
   private Herbarium herbarium;
   @ConfigurableOccurrenceField(OccurrenceField.DETERMINATION_COMMENT)
   private String determinationComment;
   @ConfigurableOccurrenceField(OccurrenceField.NUMERIC_AMOUNT)
   @Min(0)
   private Integer numericAmount;
   @ConfigurableOccurrenceField(OccurrenceField.NUMERIC_AMOUNT_ACCURACY)
   private NumericAmountAccuracy numericAmountAccuracy;
   @ConfigurableOccurrenceField(OccurrenceField.SEX)
   private Sex sex;
   @ConfigurableOccurrenceField(OccurrenceField.REPRODUCTION)
   private Reproduction reproduction;
   @ConfigurableOccurrenceField(OccurrenceField.MEDIA)
   private List<Medium> media = new ArrayList<>();
   @ConfigurableOccurrenceField(OccurrenceField.HABITAT)
   private String habitat;
   @ConfigurableOccurrenceField(OccurrenceField.ABSENCE)
   private Boolean absence;
   @ConfigurableOccurrenceField(OccurrenceField.TIME_OF_DAY)
   private String timeOfDay;
   @ConfigurableOccurrenceField(OccurrenceField.REMARK)
   private String remark;
   @ConfigurableOccurrenceField(OccurrenceField.CITE_ID)
   private String citeId;
   @ConfigurableOccurrenceField(OccurrenceField.CITE_COMMENT)
   private String citeComment;

   private Validation validation;

   public Validation getValidation()
   {
      return validation;
   }

   public void setValidation(Validation validation)
   {
      this.validation = validation;
   }

   public Reproduction getReproduction()
   {
      return reproduction;
   }

   public void setReproduction(Reproduction reproduction)
   {
      this.reproduction = reproduction;
   }

   public LifeStage getLifeStage()
   {
      return lifeStage;
   }

   public void setLifeStage(LifeStage lifeStage)
   {
      this.lifeStage = lifeStage;
   }

   public Makropter getMakropter()
   {
      return makropter;
   }

   public void setMakropter(Makropter makropter)
   {
      this.makropter = makropter;
   }

   public Sex getSex()
   {
      return sex;
   }

   public void setSex(Sex sex)
   {
      this.sex = sex;
   }

   public String getDeterminationComment()
   {
      return determinationComment;
   }

   public void setDeterminationComment(String determinationComment)
   {
      this.determinationComment = determinationComment;
   }

   public Area getCoveredArea()
   {
      return coveredArea;
   }

   public void setCoveredArea(Area area)
   {
      this.coveredArea = area;
   }

   public Person getDeterminer()
   {
      return determiner;
   }

   public void setDeterminer(Person determiner)
   {
      this.determiner = determiner;
   }

   public RecordStatus getRecordStatus()
   {
      return recordStatus;
   }

   public void setRecordStatus(RecordStatus recordStatus)
   {
      this.recordStatus = recordStatus;
   }

   public Amount getAmount()
   {
      return amount;
   }

   public void setAmount(Amount amount)
   {
      this.amount = amount;
   }

   public SettlementStatus getSettlementStatus()
   {
      return settlementStatus;
   }

   public void setSettlementStatus(SettlementStatus status)
   {
      this.settlementStatus = status;
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceBeanUtil.doEquals(this, obj);
   }

   public SampleBase getSample()
   {
      return sample;
   }

   public TaxonBase getTaxon()
   {
      return taxon;
   }

   @Override
   public int hashCode()
   {
      return OccurrenceBeanUtil.doToHashCode(this);
   }

   public void setSample(SampleBase sample)
   {
      this.sample = sample;
   }

   public void setTaxon(TaxonBase taxon)
   {
      this.taxon = taxon;
   }

   @Override
   public String toString()
   {
      return OccurrenceBeanUtil.doToString(this);
   }

   public Vitality getVitality()
   {
      return vitality;
   }

   public void setVitality(Vitality vitality)
   {
      this.vitality = vitality;
   }

   public SettlementStatusFukarek getSettlementStatusFukarek()
   {
      return settlementStatusFukarek;
   }

   public void setSettlementStatusFukarek(
         SettlementStatusFukarek occurrenceStatus)
   {
      this.settlementStatusFukarek = occurrenceStatus;
   }

   public BloomingSprouts getBloomingSprouts()
   {
      return bloomingSprouts;
   }

   public void setBloomingSprouts(BloomingSprouts bloomingSprouts)
   {
      this.bloomingSprouts = bloomingSprouts;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public Quantity getQuantity()
   {
      return quantity;
   }

   public void setQuantity(Quantity quantity)
   {
      this.quantity = quantity;
   }

   public String getObservers()
   {
      return observers;
   }

   public void setObservers(String observers)
   {
      this.observers = observers;
   }

   public Herbarium getHerbarium()
   {
      return herbarium;
   }

   public void setHerbarium(Herbarium herbarium)
   {
      this.herbarium = herbarium;
   }

   public Integer getNumericAmount()
   {
      return numericAmount;
   }

   public void setNumericAmount(Integer numericAmount)
   {
      this.numericAmount = numericAmount;
   }

   public List<Medium> getMedia()
   {
      return media;
   }

   public void setMedia(List<Medium> media)
   {
      this.media = media;
   }

   public String getHabitat()
   {
      return habitat;
   }

   public void setHabitat(String habitat)
   {
      this.habitat = habitat;
   }

   public Boolean getAbsence()
   {
      return absence;
   }

   public void setAbsence(Boolean absence)
   {
      this.absence = absence;
   }

   public String getTimeOfDay()
   {

      return timeOfDay;
   }

   public void setTimeOfDay(String timeOfDay)
   {

      this.timeOfDay = timeOfDay;
   }

   public void setNumericAmountAccuracy(
         NumericAmountAccuracy numericAmountAccuracy)
   {
      this.numericAmountAccuracy = numericAmountAccuracy;
   }

   public NumericAmountAccuracy getNumericAmountAccuracy()
   {
      return numericAmountAccuracy;
   }

   public String getRemark()
   {
      return remark;
   }

   public void setRemark(String remark)
   {
      this.remark = remark;
   }

   public String getCiteId()
   {
      return citeId;
   }

   public void setCiteId(String citeId)
   {
      this.citeId = citeId;
   }

   public String getCiteComment()
   {
      return citeComment;
   }

   public void setCiteComment(String citeComment)
   {
      this.citeComment = citeComment;
   }
}
