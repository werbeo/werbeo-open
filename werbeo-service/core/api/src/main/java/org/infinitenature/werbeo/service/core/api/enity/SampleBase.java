package org.infinitenature.werbeo.service.core.api.enity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.validation.ConfigurableSampleField;
import org.vergien.vaguedate.validation.Past;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SampleBase extends BaseTypeUUID
{
   public enum SampleMethod
   {
      FIELD_OBSERVATION, UNKNOWN, ESTIMINATION, TRANSECT_COUNT, QUADRAT, TRANSECT, NET, PITFALL_TRAP, LIGHT_TRAP, TRANSECT_SECTION, TIMED_COUNT, SWEEP_NETTING, BOX_QUADRAT, SUCTION_SAMPLER, OTHER_METHOD
   }
   @NotNull
   @Valid
   private Locality locality;
   @NotNull
   @Past
   private VagueDate date;
   @NotNull
   private Survey survey;
   @NotNull
   private Person recorder;
   @ConfigurableSampleField(SampleField.SAMPLE_METHOD)
   private SampleMethod sampleMethod;

   public SampleMethod getSampleMethod()
   {
      return sampleMethod;
   }

   public void setSampleMethod(SampleMethod sampleMethod)
   {
      this.sampleMethod = sampleMethod;
   }

   public Person getRecorder()
   {
      return recorder;
   }

   public void setRecorder(Person recorder)
   {
      this.recorder = recorder;
   }

   public VagueDate getDate()
   {
      return date;
   }

   public Survey getSurvey()
   {
      return survey;
   }

   public void setSurvey(Survey survey)
   {
      this.survey = survey;
   }

   public void setDate(VagueDate date)
   {
      this.date = date;
   }

   public Locality getLocality()
   {
      return locality;
   }

   public void setLocality(Locality locality)
   {
      this.locality = locality;
   }

   @Override
   public String toString()
   {
      return SampleBaseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleBaseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleBaseBeanUtil.doEquals(this, obj);
   }
}
