package org.infinitenature.werbeo.service.core.api.ports;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class PersonFilter
{
   private String nameContains;
   private String firstName;
   private String lastName;
   private String email;
   private String externalKey;
   private Boolean validator;

   public PersonFilter()
   {
      super();
   }

   public PersonFilter(String nameContains)
   {
      this.nameContains = nameContains;
   }

   public PersonFilter(String nameContains, String email)
   {
      this.nameContains = nameContains;
      setEmail(email);
   }

   public String getNameContains()
   {
      return nameContains;
   }

   public PersonFilter setNameContains(String nameContains)
   {
      this.nameContains = nameContains;
      return this;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public PersonFilter setFirstName(String firstName)
   {
      this.firstName = firstName;
      return this;
   }

   public String getLastName()
   {
      return lastName;
   }

   public PersonFilter setLastName(String lastName)
   {
      this.lastName = lastName;
      return this;
   }

   public String getEmail()
   {
      return email;
   }

   public PersonFilter setEmail(String email)
   {
      this.email = email == null ? null : email.toLowerCase();
      return this;
   }

   public Boolean getValidator()
   {
      return validator;
   }

   public PersonFilter setValidator(Boolean validator)
   {
      this.validator = validator;
      return this;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public PersonFilter setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
      return this;
   }

   @Override
   public String toString()
   {
      return PersonFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PersonFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PersonFilterBeanUtil.doEquals(this, obj);
   }

}
