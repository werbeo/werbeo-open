package org.infinitenature.werbeo.service.core.api.error;

public class WerbeoException extends RuntimeException
{
   public WerbeoException(String message)
   {
      super(message);
   }

   WerbeoException(String message, Throwable cause)
   {
      super(message, cause);
   }
}
