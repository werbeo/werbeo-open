package org.infinitenature.werbeo.service.core.api.infra.query;

import java.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.FrequencyDistribution;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTB;

public interface OccurrenceIndexQueries
{
   public Slice<Occurrence, OccurrenceSortField> findOccurrences(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceSortField> offsetRequest);

   public long countOccurrences(OccurrenceFilter filter, Context context);

   public Stream<UUID> findAllIds();

   public Occurrence findById(UUID id, Context context);

   public List<Occurrence> findBySampleId(UUID id, Context context);

   public Set<MTB> getCoveredMTB(int taxonId, boolean includeChildTaxa,
         Context context);

   /**
    * Creates a statistic of how many occurrences have been created the last
    * given days.
    *
    * @param context
    * @param days
    * @param timeZone
    *           the time zone, needed to get start and end of the days
    * @return
    */
   public FrequencyDistribution getOccurrencesPerDay(Context context, int days,
         ZoneId timeZone);

   /**
    * Creates a statistic of how many occurrences have been created the last
    * given weeks.
    *
    * @param context
    * @param weeks
    * @param timeZone
    *           the time zone, needed to get start and end of the days
    * @return
    */
   public FrequencyDistribution getOccurrencesPerWeek(Context context,
         int weeks, ZoneId timeZone);

   /**
    * Creates a statistic of how many occurrences have been created the last
    * given months.
    *
    * @param context
    * @param months
    * @param timeZone
    *           the time zone, needed to get start and end of the days
    * @return
    */
   public FrequencyDistribution getOccurrencesPerMonth(Context context,
         int months, ZoneId timeZone);
}
