package org.infinitenature.werbeo.service.core.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infinitenature.werbeo.service.core.api.validation.WerbeoFieldValidator.ValidationMode;
import org.springframework.beans.factory.annotation.Autowired;

public class WerbeoOccurrenceNotAllowedValidator implements
      ConstraintValidator<ConfigurableNotAllowedOccurrenceField, Object>
{
   @Autowired
   private WerbeoFieldValidator validatorImpl;

   @Override
   public void initialize(
         ConfigurableNotAllowedOccurrenceField constraintAnnotation)
   {
      validatorImpl.setField(constraintAnnotation.value());
   }

   @Override
   public boolean isValid(Object value, ConstraintValidatorContext context)
   {
      return validatorImpl.isValid(value, context,
            ValidationMode.CHECK_NOT_CONFIGURED);
   }

}
