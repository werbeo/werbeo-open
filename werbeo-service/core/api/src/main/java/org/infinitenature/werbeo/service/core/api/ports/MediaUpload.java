package org.infinitenature.werbeo.service.core.api.ports;

import javax.validation.constraints.NotBlank;

import org.infinitenature.werbeo.service.core.api.validation.UploadNotTooBig;

public class MediaUpload
{
   @UploadNotTooBig
   private byte[] mediaData;
   @NotBlank
   private String fileName;
   @NotBlank
   private String description;

   public MediaUpload(byte[] mediaData, String fileName, String description)
   {
      this.mediaData = mediaData;
      this.fileName = fileName;
      this.description = description;
   }

   public byte[] getMediaData()
   {
      return mediaData;
   }

   public void setMediaData(byte[] mediaData)
   {
      this.mediaData = mediaData;
   }

   public String getFileName()
   {
      return fileName;
   }

   public void setFileName(String fileName)
   {
      this.fileName = fileName;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }
}