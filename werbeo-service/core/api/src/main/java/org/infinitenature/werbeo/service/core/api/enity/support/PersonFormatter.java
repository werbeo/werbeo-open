package org.infinitenature.werbeo.service.core.api.enity.support;

import org.infinitenature.werbeo.service.core.api.enity.Person;

public class PersonFormatter
{
   private PersonFormatter()
   {
      throw new IllegalArgumentException("Utility class");
   }

   public static String format(Person person)
   {
      if (person == null)
      {
         return "";
      }
      return person.getFirstName() != null
            ? person.getFirstName() + " " + person.getLastName()
            : "" + person.getLastName();
   }
}
