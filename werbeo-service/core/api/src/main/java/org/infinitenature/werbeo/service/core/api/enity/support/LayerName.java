package org.infinitenature.werbeo.service.core.api.enity.support;

public enum LayerName
{
   MTB,
   ORTHOFOTO_MV,
   TOPO_MV;
}
