package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.Person;

public interface PeopleCommandPort extends CommandPort<Person, Integer>
{

}
