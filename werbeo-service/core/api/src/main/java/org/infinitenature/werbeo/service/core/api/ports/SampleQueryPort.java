package org.infinitenature.werbeo.service.core.api.ports;

import java.util.UUID;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface SampleQueryPort
{
   Sample get(UUID id, Context context);

   Slice<SampleComment, CommentSortField> getComments(UUID uuid,
         Context context, OffsetRequest<CommentSortField> offsetRequest);

   long countComments(UUID uuid, Context context);
}
