package org.infinitenature.werbeo.service.core.api.enity.meta.fields;

public enum SampleField implements WerbeoField<SampleField>
{

   ID(true), DATE(true), LOCATION(true), LOCATION_MTB(
      false), RECORDER(true), SURVEY(true), LOCATION_COMMENT(false), LOCALITY(
      false),  SAMPLE_METHOD(false), BLUR(true);

   private final boolean globalMandantory;

   private SampleField(boolean globalMandantory)
   {
      this.globalMandantory = globalMandantory;
   }

   @Override
   public boolean isGlobalMandantory()
   {
      return globalMandantory;
   }

}
