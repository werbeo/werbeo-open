package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Group
{
   private String id;

   public Group(String id)
   {
      this.id = id;
   }

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   @Override
   public String toString()
   {
      return GroupBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return GroupBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return GroupBeanUtil.doEquals(this, obj);
   }
}
