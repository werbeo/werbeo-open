package org.infinitenature.werbeo.service.core.api.enity.support;

public enum PortalSortField
{
   ID, TITLE, URL
}
