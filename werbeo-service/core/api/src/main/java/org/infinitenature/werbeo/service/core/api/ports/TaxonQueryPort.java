package org.infinitenature.werbeo.service.core.api.ports;

import java.util.List;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface TaxonQueryPort
      extends QueryPort<Taxon, TaxonFilter, Integer, TaxonSortField>
{
   Slice<TaxonBase, TaxonSortField> findBase(TaxonFilter filter, Context context,
         OffsetRequest<TaxonSortField> offsetRequest);

   public List<Taxon> findAllSynonyms(Integer id, Context context);

   public TaxonBase getByExternalId(TaxaList list, String externalId,
         Context context);
}
