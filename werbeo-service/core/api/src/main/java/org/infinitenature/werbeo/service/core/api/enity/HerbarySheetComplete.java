package org.infinitenature.werbeo.service.core.api.enity;

public class HerbarySheetComplete
{
   private String institution = null;
   private String herbary = null;
   private String latinname = null;
   private String author = null;
   private String synonym = null;
   private String book = null;
   private String locality = null;
   private String coordinates = null;
   private String epsg = null;
   private String blur = null;
   private String habitat = null;
   private String determiner = null; // determiner
   private String recorder = null;
   private String date = null;
   private String uuid = null;

   public String getInstitution()
   {
      return institution;
   }

   public void setInstitution(String institution)
   {
      this.institution = institution;
   }

   public String getHerbary()
   {
      return herbary;
   }

   public void setHerbary(String herbary)
   {
      this.herbary = herbary;
   }

   public String getLatinname()
   {
      return latinname;
   }

   public void setLatinname(String latinname)
   {
      this.latinname = latinname;
   }

   public String getAuthor()
   {
      return author;
   }

   public void setAuthor(String author)
   {
      this.author = author;
   }

   public String getSynonym()
   {
      return synonym;
   }

   public void setSynonym(String synonym)
   {
      this.synonym = synonym;
   }

   public String getBook()
   {
      return book;
   }

   public void setBook(String book)
   {
      this.book = book;
   }

   public String getLocality()
   {
      return locality;
   }

   public void setLocality(String locality)
   {
      this.locality = locality;
   }

   public String getCoordinates()
   {
      return coordinates;
   }

   public void setCoordinates(String coordinates)
   {
      this.coordinates = coordinates;
   }

   public String getEpsg()
   {
      return epsg;
   }

   public void setEpsg(String epsg)
   {
      this.epsg = epsg;
   }

   public String getBlur()
   {
      return blur;
   }

   public void setBlur(String blur)
   {
      this.blur = blur;
   }

   public String getHabitat()
   {
      return habitat;
   }

   public void setHabitat(String habitat)
   {
      this.habitat = habitat;
   }

   public String getDeterminer()
   {
      return determiner;
   }

   public void setDeterminer(String determiner)
   {
      this.determiner = determiner;
   }

   public String getRecorder()
   {
      return recorder;
   }

   public void setRecorder(String recorder)
   {
      this.recorder = recorder;
   }

   public String getDate()
   {
      return date;
   }

   public void setDate(String date)
   {
      this.date = date;
   }

   public String getUuid()
   {
      return uuid;
   }

   public void setUuid(String uuid)
   {
      this.uuid = uuid;
   }
}