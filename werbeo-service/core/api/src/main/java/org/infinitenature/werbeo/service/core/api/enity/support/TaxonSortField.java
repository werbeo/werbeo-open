package org.infinitenature.werbeo.service.core.api.enity.support;

public enum TaxonSortField
{
   ID, TAXON
}
