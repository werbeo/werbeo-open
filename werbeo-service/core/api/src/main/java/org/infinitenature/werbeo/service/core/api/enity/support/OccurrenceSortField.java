package org.infinitenature.werbeo.service.core.api.enity.support;

public enum OccurrenceSortField
{
   ID, TAXON, DETERMINER, DATE, MOD_DATE, MEDIA, TIME_OF_DAY, VALIDATION_STATUS
}
