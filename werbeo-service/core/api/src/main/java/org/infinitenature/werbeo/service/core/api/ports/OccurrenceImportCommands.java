package org.infinitenature.werbeo.service.core.api.ports;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceImportCommands
{
   ImportJob importXML(String bdeXml, Context context);
   ImportJob importCSV(String bdeXml, Context context);
   void delete(UUID imortId, Context context);
}
