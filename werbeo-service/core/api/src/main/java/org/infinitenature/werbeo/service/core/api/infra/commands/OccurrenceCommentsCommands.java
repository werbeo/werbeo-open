package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceCommentsCommands
{
   public int addComment(UUID occurrenceId, OccurrenceComment comment,
         Context context);
}
