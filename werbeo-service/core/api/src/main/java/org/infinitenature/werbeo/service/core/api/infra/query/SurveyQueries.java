package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface SurveyQueries
{
   public Slice<Survey, SurveySortField> findSurveys(String title,
         String titleContains,
         Integer parentSurveyId, Context context,
         OffsetRequest<SurveySortField> offsetRequest);

   public long countSurveys(String title, String titleContains,
         Integer parentSurveyId,
         Context context);

   public Survey load(int id, Context context);
}
