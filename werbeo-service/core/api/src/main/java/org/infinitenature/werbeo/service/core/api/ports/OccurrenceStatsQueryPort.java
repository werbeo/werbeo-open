package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.FrequencyDistribution;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceStatsQueryPort
{
   public FrequencyDistribution getOccurrencesCreatedPerDay(Context context,
         int days);

   public FrequencyDistribution getOccurrencesCreatedPerWeek(Context context,
         int weeks);

   public FrequencyDistribution getOccurrencesCreatedPerMonth(Context context,
         int months);
}
