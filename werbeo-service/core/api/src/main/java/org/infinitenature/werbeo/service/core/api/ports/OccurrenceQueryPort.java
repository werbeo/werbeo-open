package org.infinitenature.werbeo.service.core.api.ports;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTB;

public interface OccurrenceQueryPort
      extends
      QueryPort<Occurrence, OccurrenceFilter, UUID, OccurrenceSortField>,
      StreamQueryPort<Occurrence, UUID>
{
   Slice<OccurrenceCentroid, OccurrenceCentroidSortField> findOccurrenceCentroids(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceCentroidSortField> offsetRequest);

   long countOccurrenceCentroids(OccurrenceFilter filter, Context context);

   Map<Filter, Collection<Portal>> getFilter(Context context);

   Slice<OccurrenceComment, CommentSortField> getComments(UUID uuid,
         Context context, OffsetRequest<CommentSortField> offsetRequest);

   long countComments(UUID uuid, Context context);

   Set<MTB> getCoveredMTB(int taxonId, boolean incluseChildTaxa,
         Context context);

   List<Occurrence> findBySampleId(UUID uuid, Context context);
}
