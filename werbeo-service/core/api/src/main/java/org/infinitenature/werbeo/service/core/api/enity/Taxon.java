package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Taxon extends TaxonBase
{

   public enum RedListStatus
   {
      /**
       *
       *  *  nicht gefährdet
       *  nb  nicht bewertet
       *  2   stark gefährdet
       *  1   vom aussterben bedroht
       *  0  ausgestorben oder verschollen
       *  R  extrem selten
       *  3  gefährdet
       *  V  Vorwarnliste
       *  G gefährdung unbekannten ausmaßes
       *  D Daten unzureichend
       *
       */

      SAFE, NB, ZERO, ONE, TWO, THREE, V, G, D, R
   }

   private RedListStatus redListStatus;

   private Language language;

   public Language getLanguage()
   {
      return language;
   }

   public RedListStatus getRedListStatus()
   {
      return redListStatus;
   }

   public void setRedListStatus(RedListStatus redListStatus)
   {
      this.redListStatus = redListStatus;
   }

   public void setLanguage(Language language)
   {
      this.language = language;
   }

   @Override
   public String toString()
   {
      return TaxonBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonBeanUtil.doEquals(this, obj);
   }
}
