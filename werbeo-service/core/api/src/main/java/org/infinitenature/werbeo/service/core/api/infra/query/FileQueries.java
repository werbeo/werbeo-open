package org.infinitenature.werbeo.service.core.api.infra.query;

import java.io.InputStream;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.DocumentType;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface FileQueries
{
   public Set<DocumentType> getDocumentsAvailable(TaxonBase taxon, Context context);

   public InputStream loadCsvTemplate(int portalId);
   
   public InputStream loadCsvTemplateHelp(int portalId);
}
