package org.infinitenature.werbeo.service.core.api.enity.meta;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SurveyFieldConfig extends FieldConfig<SurveyField>
{

   public SurveyFieldConfig(SurveyField field, boolean mandantory)
   {
      super(field, mandantory);
   }

   @Override
   public SurveyField getField()
   {
      return super.getField();
   }

   @Override
   public String toString()
   {
      return SurveyFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyFieldConfigBeanUtil.doEquals(this, obj);
   }
}
