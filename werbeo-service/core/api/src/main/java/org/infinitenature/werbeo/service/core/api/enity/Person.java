package org.infinitenature.werbeo.service.core.api.enity;

import javax.validation.constraints.Size;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Person extends BaseTypeInt
{
   public static final Person HIDDEN_PERSON = initHidddenPerson();

   private static Person initHidddenPerson()
   {
      Person hiddenPerson = new Person(Integer.MIN_VALUE,
            "Hidden", "Hidden");
      hiddenPerson.setCreatedBy(User.HIDDEN_USER);
      hiddenPerson.setModifiedBy(User.HIDDEN_USER);
      return hiddenPerson;
   }

   private String firstName;
   private String lastName;
   @Size(max = 50)
   private String externalKey;
   private String email;

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      if (email != null)
      {
         this.email = email.toLowerCase();
      } else
      {
         this.email = null;
      }
   }

   public Person()
   {
      super();
   }

   public Person(String firstName, String lastName)
   {
      super();
      this.firstName = firstName;
      this.lastName = lastName;
   }

   public Person(Integer id, String firstName, String lastName)
   {
      super(id);
      this.firstName = firstName;
      this.lastName = lastName;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   @Override
   public String toString()
   {
      return PersonBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PersonBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PersonBeanUtil.doEquals(this, obj);
   }
}
