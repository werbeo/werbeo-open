package org.infinitenature.werbeo.service.core.api.enity;

import java.time.LocalDateTime;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Validation extends BaseTypeInt
{
   private ValidationStatus status;

   private String comment;

   private Person validator;

   public Validation()
   {
      super();
   }

   public Validation(ValidationStatus status)
   {
      super();
      this.status = status;
   }

   public Validation(ValidationStatus status, String comment)
   {
      super();
      this.status = status;
      this.comment = comment;
   }

   public Validation(ValidationStatus validationStatus, String validatedBy,
         LocalDateTime validationDate, Person validator)
   {
      this();
      this.status = validationStatus;
      this.setModificationDate(validationDate);
      this.setModifiedBy(new User());
      this.getModifiedBy().setLogin(validatedBy);
      this.validator = validator;
   }

   public ValidationStatus getStatus()
   {
      return status;
   }

   public void setStatus(ValidationStatus status)
   {
      this.status = status;
   }

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public Person getValidator()
   {
      return validator;
   }

   public void setValidator(Person validator)
   {
      this.validator = validator;
   }

   @Override
   public String toString()
   {
      return ValidationBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ValidationBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ValidationBeanUtil.doEquals(this, obj);
   }
}
