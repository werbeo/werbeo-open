package org.infinitenature.werbeo.service.core.api.ports;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Export;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceExportSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.QueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceExportQueryPort
      extends QueryPort<Export, Void, UUID, OccurrenceExportSortField>
{
   TypedFile load(UUID exportId, Context context);
}
