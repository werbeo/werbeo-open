package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;

public interface TaxaListQueryPort
      extends QueryPort<TaxaList, TaxaListFilter, Integer, TaxaListSortField>
{

}
