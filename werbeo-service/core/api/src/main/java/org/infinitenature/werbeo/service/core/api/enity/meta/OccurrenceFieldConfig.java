package org.infinitenature.werbeo.service.core.api.enity.meta;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceFieldConfig extends FieldConfig<OccurrenceField>
{
   public OccurrenceFieldConfig(OccurrenceField field, boolean mandantory)
   {
      super(field, mandantory);
   }

   @Override
   public OccurrenceField getField()
   {
      return super.getField();
   }

   @Override
   public String toString()
   {
      return OccurrenceFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceFieldConfigBeanUtil.doEquals(this, obj);
   }
}
