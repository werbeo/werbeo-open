package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.io.OutputStream;
import java.util.stream.Stream;

import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;

public interface OccurrenceExportCommands
{
   public void export(Stream<Occurrence> occurrences, OutputStream outputStream,
         OccurrenceConfig occurrenceConfig, SampleConfig sampleConfig,
         SurveyConfig surveyConfig, int portalId) throws Exception;

   public FileFormat getSupportedFileFormat();
}
