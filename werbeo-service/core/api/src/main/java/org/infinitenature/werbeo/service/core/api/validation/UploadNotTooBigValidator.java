package org.infinitenature.werbeo.service.core.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

public class UploadNotTooBigValidator
      implements ConstraintValidator<UploadNotTooBig, Object>
{
   @Autowired
   private MediaUploadValidator validatorImpl;

   @Override
   public boolean isValid(Object value, ConstraintValidatorContext context)
   {
      return validatorImpl.sizeIsValid(((byte[]) value).length);
   }

}
