package org.infinitenature.werbeo.service.core.api.enity.meta;

import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SurveyConfig extends EntityConfig<SurveyFieldConfig, SurveyField>
{
   public SurveyConfig(Portal portal,
         Set<SurveyFieldConfig> configuredFields)
   {
      super(portal, configuredFields);
   }

   @Override
   public String toString()
   {
      return SurveyConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyConfigBeanUtil.doEquals(this, obj);
   }
}
