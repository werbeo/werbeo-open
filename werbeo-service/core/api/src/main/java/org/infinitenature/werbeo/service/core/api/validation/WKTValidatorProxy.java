package org.infinitenature.werbeo.service.core.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

public class WKTValidatorProxy implements ConstraintValidator<ValidWKT, String>
{
   @Autowired
   private WKTValidator wktValidator;
   @Override
   public boolean isValid(String value, ConstraintValidatorContext context)
   {
      return wktValidator.isValid(value);
   }

}
