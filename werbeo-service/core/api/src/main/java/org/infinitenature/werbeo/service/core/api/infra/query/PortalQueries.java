package org.infinitenature.werbeo.service.core.api.infra.query;

import java.util.Set;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.ports.PortalFilter;

public interface PortalQueries
{
   public Portal load(int id);

   /**
    * @param id
    * @return all {@link Portal}s which are associated to the given one.
    *         Including the given one itself.
    */
   public Slice<Portal, PortalSortField> findAssociated(int id);

   public Set<Portal> findAll();

   public Slice<Portal, PortalSortField> find(PortalFilter filter,
         OffsetRequest<PortalSortField> offsetRequest);

   public long count(PortalFilter filter);
}
