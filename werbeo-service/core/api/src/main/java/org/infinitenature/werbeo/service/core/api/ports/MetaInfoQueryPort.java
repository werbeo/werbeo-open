package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.TaxonConfig;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface MetaInfoQueryPort
{

   SurveyConfig getSurveyConfig(Context context);

   OccurrenceConfig getOccurrenceConfig(Context context);

   SampleConfig getSampleConfig(Context context);

   TaxonConfig getTaxonConfig(Context context);
}
