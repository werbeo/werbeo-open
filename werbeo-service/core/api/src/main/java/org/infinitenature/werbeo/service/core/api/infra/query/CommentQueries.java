package org.infinitenature.werbeo.service.core.api.infra.query;

import java.util.UUID;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface CommentQueries
{

   Slice<SampleComment, CommentSortField> findSampleComments(UUID sampleId,
         Context context, OffsetRequest<CommentSortField> offsetRequest);

   long countSampleComments(UUID sampleId, Context context);

   Slice<OccurrenceComment, CommentSortField> findOccurrenceComments(
         UUID occurrenceId, Context context,
         OffsetRequest<CommentSortField> offsetRequest);

   long countOccurrenceComments(UUID occurrenceId, Context context);

}
