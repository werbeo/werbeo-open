package org.infinitenature.werbeo.service.core.api.enity.meta;

import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceConfig
      extends EntityConfig<OccurrenceFieldConfig, OccurrenceField>
{

   public OccurrenceConfig()
   {
      super();
   }

   public OccurrenceConfig(Portal portal,
         Set<OccurrenceFieldConfig> configuredFields)
   {
      super(portal, configuredFields);
   }

   @Override
   public String toString()
   {
      return OccurrenceConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceConfigBeanUtil.doEquals(this, obj);
   }
}
