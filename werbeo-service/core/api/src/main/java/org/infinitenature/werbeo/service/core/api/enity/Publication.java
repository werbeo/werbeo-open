package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Publication
{
   private String primAuthorFirstName;
   private String primAuthorLastName;
   private int year;
   private String title;
   private String citeId;

   public String getPrimAuthorFirstName()
   {
      return primAuthorFirstName;
   }

   public void setPrimAuthorFirstName(String primAuthorFirstName)
   {
      this.primAuthorFirstName = primAuthorFirstName;
   }

   public String getPrimAuthorLastName()
   {
      return primAuthorLastName;
   }

   public void setPrimAuthorLastName(String primAuthorLastName)
   {
      this.primAuthorLastName = primAuthorLastName;
   }

   public int getYear()
   {
      return year;
   }

   public void setYear(int year)
   {
      this.year = year;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getCiteId()
   {
      return citeId;
   }

   public void setCiteId(String citeId)
   {
      this.citeId = citeId;
   }

   @Override
   public String toString()
   {
      return PublicationBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PublicationBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PublicationBeanUtil.doEquals(this, obj);
   }
}
