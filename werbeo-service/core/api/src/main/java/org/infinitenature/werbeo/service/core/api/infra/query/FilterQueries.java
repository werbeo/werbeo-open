package org.infinitenature.werbeo.service.core.api.infra.query;

import java.util.Collection;
import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.infra.roles.RoleFilter;

public interface FilterQueries
{
   List<RoleFilter> getFilters(Collection<Portal> portals);
}
