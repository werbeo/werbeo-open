package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.ImportPart;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface ImportQueries
{
   Optional<ImportJob> findOneImportJobByStatus(JobStatus status);
   List<ImportJob> findImportJobsByStatus(JobStatus status);

   String getBDEXML(ImportJob importJob);
   String getCSV(ImportJob importJob);

   Context getContext(ImportJob importJob);

   Optional<ImportJob> findImportJobById(UUID id, Context context);

   boolean isPartExisting(ImportJob importJob, JobStatus... prepared);

   void forEachFailure(ImportJob importJob, Context context,
         Consumer<ImportPart> consumer);

   boolean isJobNotFinished(ImportJob importJob);
   
   String getCSVProtocol(ImportJob importJob);
   String getCSVErrors(ImportJob importJob);
}
