package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.Survey;

public interface SurveyCommandPort extends CommandPort<Survey, Integer>
{

}
