package org.infinitenature.werbeo.service.core.api.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;

@Retention(RUNTIME)
@Target(FIELD)
@Constraint(validatedBy = {})
@ConfigurableNotNullSurveyField(value = SurveyField.ID)
@ConfigurableNotAllowedSurveyField(value = SurveyField.ID)
public @interface ConfigurableSurveyField
{
   String message() default "";

   Class<?>[] groups() default {};

   Class<? extends Payload>[] payload() default {};

   @OverridesAttribute(constraint = ConfigurableNotAllowedSurveyField.class, name = "value")
   @OverridesAttribute(constraint = ConfigurableNotNullSurveyField.class, name = "value")
   SurveyField value();
}
