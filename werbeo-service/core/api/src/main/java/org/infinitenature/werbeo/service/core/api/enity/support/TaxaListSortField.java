package org.infinitenature.werbeo.service.core.api.enity.support;

public enum TaxaListSortField
{
   ID, NAME
}
