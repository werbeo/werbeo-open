package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface MediaCommands
{
   public String uploadMedia(byte[] mediaData, String fileName,
         Context context);

   public int addOccurrenceMedia(UUID occurrenceId, String path,
         String description, MediaType mediaType, Context context);

   public void deleteOccurrenceMedium(UUID occurrenceId, Medium mediumToDelete,
         Context context);
}
