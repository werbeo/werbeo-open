package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListFilter;

public interface TaxaListQueries
      extends EntityQuery<TaxaList, TaxaListFilter, Integer, TaxaListSortField>
{

}
