package org.infinitenature.werbeo.service.core.api.error;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;

public class ValidationException extends WerbeoException
{
   private final Set<ConstraintViolation<?>> constraintViolations;

   public ValidationException(String message,
         Set<? extends ConstraintViolation<?>> constraintViolations)
   {
      super(message);
      this.constraintViolations = new HashSet<>(constraintViolations);
   }

   public Set<ConstraintViolation<?>> getConstraintViolations()
   {
      return constraintViolations;
   }

   @Override
   public String getMessage()
   {
      StringBuilder sb = new StringBuilder(super.getMessage());
      sb.append("\n\n");
      for (Iterator iterator = constraintViolations.iterator(); iterator
            .hasNext();)
      {
         ConstraintViolation<?> constraintViolation = (ConstraintViolation<?>) iterator
               .next();
         sb.append(constraintViolation.getPropertyPath());
         sb.append(" - ");
         sb.append(constraintViolation.getMessage());
         if (iterator.hasNext())
         {
            sb.append("\n");
         }
      }
      return sb.toString();
   }
}
