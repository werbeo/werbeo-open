package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceComment extends AbstractComment
{
   @Override
   public String toString()
   {
      return OccurrenceCommentBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceCommentBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceCommentBeanUtil.doEquals(this, obj);
   }
}
