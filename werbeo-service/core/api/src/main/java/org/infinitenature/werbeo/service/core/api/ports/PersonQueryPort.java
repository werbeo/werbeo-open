package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;

public interface PersonQueryPort
      extends QueryPort<Person, PersonFilter, Integer, PersonSortField>
{


}
