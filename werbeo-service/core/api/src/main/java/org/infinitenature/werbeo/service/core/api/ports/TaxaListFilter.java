package org.infinitenature.werbeo.service.core.api.ports;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxaListFilter
{
   public static final TaxaListFilter ALL = new TaxaListFilter(null);
   private final String name;

   public TaxaListFilter(String name)
   {
      super();
      this.name = name;
   }

   public String getName()
   {
      return name;
   }

   @Override
   public String toString()
   {
      return TaxaListFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxaListFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxaListFilterBeanUtil.doEquals(this, obj);
   }
}
