package org.infinitenature.werbeo.service.core.api.infra.commands;

import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface SurveyCommands
{
   public int saveOrUpdate(Survey survey, Context context);

   void delete(Survey survey, Context context);
}
