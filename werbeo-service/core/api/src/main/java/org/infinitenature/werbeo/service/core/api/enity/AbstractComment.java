package org.infinitenature.werbeo.service.core.api.enity;

import java.util.UUID;

public abstract class AbstractComment extends BaseTypeInt
{
   private UUID targetId;

   private String comment;

   public UUID getTargetId()
   {
      return targetId;
   }

   public void setTargetId(UUID targetId)
   {
      this.targetId = targetId;
   }

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

}
