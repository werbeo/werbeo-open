package org.infinitenature.werbeo.service.core.api.enity;

public class TypedFile
{
   private final String fileName;
   private final FileFormat fileFormat;

   private final byte[] data;

   public TypedFile(String fileName, FileFormat fileFormat, byte[] data)
   {
      super();
      this.fileName = fileName;
      this.fileFormat = fileFormat;
      this.data = data;
   }

   public String getFileName()
   {
      return fileName;
   }

   public FileFormat getFileFormat()
   {
      return fileFormat;
   }

   public byte[] getData()
   {
      return data;
   }

}
