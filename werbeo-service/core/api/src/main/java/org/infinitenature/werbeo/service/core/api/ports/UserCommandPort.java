package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.enums.Role;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface UserCommandPort
{
   void addRole(Context context, String email, Role role);

   void removeRole(Context context, String email, Role role);
}
