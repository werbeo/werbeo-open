package org.infinitenature.werbeo.service.core.api.enity;

import java.util.ArrayList;
import java.util.List;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxaList extends BaseTypeInt
{
   private String name;
   private String description;
   private List<String> taxaGroups = new ArrayList<>();

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public List<String> getTaxaGroups()
   {
      return taxaGroups;
   }

   public void setTaxaGroups(List<String> taxaGroups)
   {
      this.taxaGroups = taxaGroups;
   }

   @Override
   public String toString()
   {
      return TaxaListBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxaListBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxaListBeanUtil.doEquals(this, obj);
   }
}
