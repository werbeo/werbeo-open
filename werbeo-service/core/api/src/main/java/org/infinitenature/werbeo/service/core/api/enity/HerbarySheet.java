package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;
import org.infinitenature.herbariorum.business.Institution;

@Bean
public class HerbarySheet
{
   private Institution institution = null;
   private String herbary = null;
   private String author = null;
   private String synonym = null;
   private String book = null;
   private String habitat = null;

   public Institution getInstitution()
   {
      return institution;
   }

   public void setInstitution(Institution institution)
   {
      this.institution = institution;
   }

   public String getHerbary()
   {
      return herbary;
   }

   public void setHerbary(String herbary)
   {
      this.herbary = herbary;
   }

   public String getSynonym()
   {
      return synonym;
   }

   public void setSynonym(String synonym)
   {
      this.synonym = synonym;
   }

   public String getBook()
   {
      return book;
   }

   public void setBook(String book)
   {
      this.book = book;
   }

   public String getHabitat()
   {
      return habitat;
   }

   public void setHabitat(String habitat)
   {
      this.habitat = habitat;
   }

   public String getAuthor()
   {
      return author;
   }

   public void setAuthor(String author)
   {
      this.author = author;
   }

}
