package org.infinitenature.werbeo.service.core.api.enity.meta;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SampleFieldConfig extends FieldConfig<SampleField>
{

   public SampleFieldConfig(SampleField field, boolean mandantory)
   {
      super(field, mandantory);
   }

   @Override
   public SampleField getField()
   {
      return super.getField();
   }

   @Override
   public String toString()
   {
      return SampleFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleFieldConfigBeanUtil.doEquals(this, obj);
   }
}
