package org.infinitenature.werbeo.service.core.api.validation;

public interface PolygonMaxSizeValidator
{

   boolean isPolygonSizeValid(String value);

}
