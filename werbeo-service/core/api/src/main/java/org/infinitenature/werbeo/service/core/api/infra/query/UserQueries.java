package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.UserSortField;

public interface UserQueries
{

   long count(String email);

   Slice<User, UserSortField> find(String email, int offset, int limit, int portalId);

}
