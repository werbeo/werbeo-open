package org.infinitenature.werbeo.service.core.api.enity;

public enum FileFormat
{
   XML_BDE("xml"), WINART("zip"), CSV("csv"), KMZ("kmz"), LOG("log");

   private final String suffix;

   private FileFormat(String suffix)
   {
      this.suffix = suffix;
   }

   public String getSuffix()
   {
      return suffix;
   }
}
