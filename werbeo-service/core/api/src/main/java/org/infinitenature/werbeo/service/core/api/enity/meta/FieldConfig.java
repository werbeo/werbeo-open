package org.infinitenature.werbeo.service.core.api.enity.meta;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.WerbeoField;

public abstract class FieldConfig<T extends WerbeoField>
{
   private final T field;
   private final boolean mandantory;

   public FieldConfig(T field, boolean mandantory)
   {
      super();
      this.field = field;
      this.mandantory = mandantory;
   }

   public T getField()
   {
      return field;
   }

   public boolean isMandantory()
   {
      return mandantory;
   }

}
