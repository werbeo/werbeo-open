package org.infinitenature.werbeo.service.core.api.ports;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceImportQueries
{

   JobStatus getStatus(UUID id, Context context);

}
