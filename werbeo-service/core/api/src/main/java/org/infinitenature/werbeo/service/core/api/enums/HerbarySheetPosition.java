package org.infinitenature.werbeo.service.core.api.enums;

public enum HerbarySheetPosition
{
    TOP_RIGHT, TOP_LEFT, BOTTOM_LEFT, BOTTOM_RIGHT
}
