package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.UserSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface UserQueryPort
{
   long count(String email, Context context);
   Slice<User, UserSortField> find (String email, int offset, int limit,Context context);
}
