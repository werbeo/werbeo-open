package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SampleComment extends AbstractComment
{
   @Override
   public String toString()
   {
      return SampleCommentBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleCommentBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleCommentBeanUtil.doEquals(this, obj);
   }
}
