package org.infinitenature.werbeo.service.core.api.enity;

import java.time.LocalDateTime;
import java.util.UUID;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class CSVImport extends BaseTypeUUID
{
   private JobStatus status;
   private String fileFormat;
   
   public CSVImport(UUID id, JobStatus jobStatus, String fileFormat, LocalDateTime createDate)
   {
      super();
      this.setId(id);
      this.status = jobStatus;
      this.fileFormat = fileFormat;
      setCreationDate(createDate);
   }

   public JobStatus getStatus()
   {
      return status;
   }

   public void setStatus(JobStatus status)
   {
      this.status = status;
   }

   public String getFileFormat()
   {
      return fileFormat;
   }

   public void setFileFormat(String fileFormat)
   {
      this.fileFormat = fileFormat;
   }

   @Override
   public String toString()
   {
      return CSVImportBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return CSVImportBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return CSVImportBeanUtil.doEquals(this, obj);
   }
}
