package org.infinitenature.werbeo.service.core.api.validation;

import javax.validation.ConstraintValidatorContext;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.WerbeoField;

public interface WerbeoFieldValidator
{
   public enum ValidationMode
   {
      CHECK_MANDANTORY, CHECK_NOT_CONFIGURED, CHECK_VALUES
   }

   void setField(WerbeoField<?> werbeoField);

   boolean isValid(Object value, ConstraintValidatorContext context,
         ValidationMode validationMode);
}
