package org.infinitenature.werbeo.service.core.api.enity.meta.fields;

public enum SurveyField implements WerbeoField<SurveyField>
{
   ID(false), PARENT_ID(false), DESCRIPTION(true), OWNER(false), NAME(
         true), PORTAL(false), AVAILABILITY(
               true), CONTAINER(
                     false), DEPUTY_CUSTIODIANS(false), WERBEO_ORIGINAL(true);

   private final boolean globalMandantory;

   private SurveyField(boolean globalMandantory)
   {
      this.globalMandantory = globalMandantory;
   }

   @Override
   public boolean isGlobalMandantory()
   {
      return globalMandantory;
   }

}
