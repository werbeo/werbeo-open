package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface SampleCommands
{
   public UUID saveOrUpdate(Sample sample, Context context);

   void delete(Sample sample, Context context);

   public int addComment(UUID sampleID, SampleComment comment, Context context);
}
