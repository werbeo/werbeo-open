package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface PortalQueryPort
{
   Portal load(int portalId);

   PortalConfiguration loadConfig(int portalId);

   Slice<Portal, PortalSortField> find(PortalFilter filter,
         Context context, OffsetRequest<PortalSortField> offsetRequest);
   long count(PortalFilter filter, Context context);
   Portal get(Integer id, Context context);

   Slice<Portal, PortalSortField> findAssociated(Context context);
}

