package org.infinitenature.werbeo.service.core.api.enity.meta.fields;

public interface WerbeoField<T extends Enum & WerbeoField<T>>
{
   public boolean isGlobalMandantory();
}
