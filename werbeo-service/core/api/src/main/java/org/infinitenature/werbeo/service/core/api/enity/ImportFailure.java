package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class ImportFailure
{
   private String failure;

   public String getFailure()
   {
      return failure;
   }

   public void setFailure(String failure)
   {
      this.failure = failure;
   }

   @Override
   public String toString()
   {
      return ImportFailureBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ImportFailureBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ImportFailureBeanUtil.doEquals(this, obj);
   }
}
