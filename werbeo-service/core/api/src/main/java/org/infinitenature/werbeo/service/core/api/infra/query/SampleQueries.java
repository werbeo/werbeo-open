package org.infinitenature.werbeo.service.core.api.infra.query;

import java.util.Optional;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface SampleQueries
{
   public Optional<Sample> get(UUID uuid, Context context);
}
