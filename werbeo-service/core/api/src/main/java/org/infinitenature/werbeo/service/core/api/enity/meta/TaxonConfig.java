package org.infinitenature.werbeo.service.core.api.enity.meta;

import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.TaxonField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxonConfig extends EntityConfig<TaxonFieldConfig, TaxonField>
{

   public TaxonConfig()
   {
      super();
   }

   public TaxonConfig(Portal portal, Set<TaxonFieldConfig> configuredFields)
   {
      super(portal, configuredFields);
   }

   @Override
   public String toString()
   {
      return TaxonConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonConfigBeanUtil.doEquals(this, obj);
   }
}