package org.infinitenature.werbeo.service.core.api.infra.roles;

import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.roles.Filter;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class RoleFilter
{
   private final Roles missingActivatingRole;
   private final Filter filter;
   private final Portal portal;

   public RoleFilter(Roles missingActivatingRole, Filter filter, Portal portal)
   {
      super();
      this.missingActivatingRole = missingActivatingRole;
      this.filter = filter;
      this.portal = portal;
   }

   public Roles getMissingActivatingRole()
   {
      return missingActivatingRole;
   }

   public Filter getFilter()
   {
      return filter;
   }

   public Portal getPortal()
   {
      return portal;
   }

   @Override
   public String toString()
   {
      return RoleFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return RoleFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return RoleFilterBeanUtil.doEquals(this, obj);
   }
}
