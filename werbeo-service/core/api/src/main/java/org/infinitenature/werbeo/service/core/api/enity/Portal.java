package org.infinitenature.werbeo.service.core.api.enity;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Portal extends BaseTypeInt
{
   public static final Portal NO_PORTAL = new Portal(Integer.MIN_VALUE,
         "No-Portal");

   private String title;
   private String url;

   public Portal()
   {
      super();
   }

   public Portal(Integer id)
   {
      super(id);
   }

   public Portal(int id, String title)
   {
      super(id);
      this.title = title;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   @Override
   public String toString()
   {
      return PortalBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PortalBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalBeanUtil.doEquals(this, obj);
   }
}
