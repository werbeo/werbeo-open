package org.infinitenature.werbeo.service.core.api.ports;

import java.util.List;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceCommandsPort
{
   public int addComment(UUID occurrenceId, OccurrenceComment comment,
         Context context);

   public List<Medium> addMedia(UUID occurrenceId, MediaUpload mediaUpload,
         Context context);

   public void deleteMedium(UUID occurrenceId, String clearPath,
         Context context);

   public void setValidationStatus(UUID uuid, Validation validation,
         Context context);
}
