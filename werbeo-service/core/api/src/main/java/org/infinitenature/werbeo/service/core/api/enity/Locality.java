package org.infinitenature.werbeo.service.core.api.enity;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.validation.ConfigurableSampleField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Locality
{
   @NotNull
   @Valid
   private Position position;
   @NotNull
   @Min(1)
   private Integer precision;
   @ConfigurableSampleField(SampleField.LOCATION_COMMENT)
   private String locationComment;
   @ConfigurableSampleField(SampleField.LOCALITY)
   private String locality;
   

   public Position getPosition()
   {
      return position;
   }

   public void setPosition(Position position)
   {
      this.position = position;
   }

   public Integer getPrecision()
   {
      return precision;
   }

   public void setPrecision(Integer precision)
   {
      this.precision = precision;
   }

   public String getLocationComment()
   {
      return locationComment;
   }

   public void setLocationComment(String locationComment)
   {
      this.locationComment = locationComment;
   }

   public String getLocality()
   {
      return locality;
   }

   public void setLocality(String locality)
   {
      this.locality = locality;
   }

   @Override
   public String toString()
   {
      return LocalityBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return LocalityBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return LocalityBeanUtil.doEquals(this, obj);
   }
}
