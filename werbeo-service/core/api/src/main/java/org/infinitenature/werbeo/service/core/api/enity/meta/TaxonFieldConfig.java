package org.infinitenature.werbeo.service.core.api.enity.meta;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.TaxonField;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxonFieldConfig extends FieldConfig<TaxonField>
{

   public TaxonFieldConfig(TaxonField field, boolean mandantory)
   {
      super(field, mandantory);
   }

   @Override
   public TaxonField getField()
   {
      return super.getField();
   }

   @Override
   public String toString()
   {
      return TaxonFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonFieldConfigBeanUtil.doEquals(this, obj);
   }
}
