package org.infinitenature.werbeo.service.core.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

public class PolygonMaxSizeValidatorProxy
      implements ConstraintValidator<PolygonMaxSize, String>
{
   @Autowired
   private PolygonMaxSizeValidator polygonMaxSizeValidator;
   @Override
   public boolean isValid(String value, ConstraintValidatorContext context)
   {
      return polygonMaxSizeValidator.isPolygonSizeValid(value);
   }

}
