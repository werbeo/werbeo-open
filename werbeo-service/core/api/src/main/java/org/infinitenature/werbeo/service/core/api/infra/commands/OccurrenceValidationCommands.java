package org.infinitenature.werbeo.service.core.api.infra.commands;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface OccurrenceValidationCommands
{

   void setValidationStatus(UUID uuid, Validation validation,
         Context context);

}
