package org.infinitenature.werbeo.service.core.api.ports;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface SampleCommandPort extends CommandPort<Sample, UUID>
{
   public UUID saveOrUpdate(Sample sample, Context context,
         List<SampleComment> sampleComments,
         Map<Occurrence, List<OccurrenceComment>> occurrenceComments);

   public int addComment(UUID sampleID, SampleComment comment, Context context);
}
