package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.werbeo.service.core.api.support.Context;

public interface RolesCommandPort
{

   boolean accept(Context context);

}
