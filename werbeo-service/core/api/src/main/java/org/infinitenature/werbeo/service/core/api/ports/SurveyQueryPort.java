package org.infinitenature.werbeo.service.core.api.ports;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface SurveyQueryPort
{

   Slice<Survey, SurveySortField> find(String title, String titleContains,
         Integer parentSurveyId,
         Context context, OffsetRequest<SurveySortField> offsetRequest);

   long count(String title, String titleContains, Integer parentSurveyId,
         Context context);

   Survey get(int id, Context context);
}
