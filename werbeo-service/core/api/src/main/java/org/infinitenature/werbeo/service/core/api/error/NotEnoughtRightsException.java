package org.infinitenature.werbeo.service.core.api.error;

public class NotEnoughtRightsException extends WerbeoException
{

   public NotEnoughtRightsException(String message)
   {
      super(message);
   }

}
