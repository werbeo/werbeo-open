package org.infinitenature.werbeo.service.core.api.enity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.service.core.access.Operation;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public abstract class BaseTypeInt implements BaseType<Integer>
{
   private Integer id;

   private LocalDateTime creationDate;
   private User createdBy;
   private LocalDateTime modificationDate;
   private User modifiedBy;
   private Set<Operation> allowedOperations = new HashSet<>();
   private boolean obfuscated = false;

   public BaseTypeInt()
   {
      super();
   }

   public BaseTypeInt(Integer id)
   {
      super();
      this.id = id;
   }

   @Override
   public LocalDateTime getCreationDate()
   {
      return creationDate;
   }

   @Override
   public void setCreationDate(LocalDateTime creationDate)
   {
      this.creationDate = creationDate;
   }

   @Override
   public User getCreatedBy()
   {
      return createdBy;
   }

   @Override
   public void setCreatedBy(User createdBy)
   {
      this.createdBy = createdBy;
   }

   @Override
   public LocalDateTime getModificationDate()
   {
      return modificationDate;
   }

   @Override
   public void setModificationDate(LocalDateTime modificationDate)
   {
      this.modificationDate = modificationDate;
   }

   @Override
   public User getModifiedBy()
   {
      return modifiedBy;
   }

   @Override
   public void setModifiedBy(User modifiedBy)
   {
      this.modifiedBy = modifiedBy;
   }

   @Override
   public Integer getId()
   {
      return id;
   }

   @Override
   public void setId(Integer id)
   {
      this.id = id;
   }

   @Override
   public Set<Operation> getAllowedOperations()
   {
      return allowedOperations;
   }

   @Override
   public void setAllowedOperations(Set<Operation> allowedOperations)
   {
      this.allowedOperations = allowedOperations;
   }

   @Override
   public boolean isObfuscated()
   {
      return obfuscated;
   }

   @Override
   public void setObfuscated(boolean obfuscated)
   {
      this.obfuscated = obfuscated;
   }

   @Override
   public String toString()
   {
      return BaseTypeIntBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return BaseTypeIntBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return BaseTypeIntBeanUtil.doEquals(this, obj);
   }

}
