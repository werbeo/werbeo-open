package org.infinitenature.werbeo.service.core.api.enity.meta.fields;

public enum TaxonField implements WerbeoField<TaxonField>
{
   ID(false), RED_LIST_STATUS(false), LANGUAGE(true), GROUP(false), AUTHORITY(
         true), NAME(true);

   private final boolean globalMandantory;

   private TaxonField(boolean globalMandantory)
   {
      this.globalMandantory = globalMandantory;
   }

   @Override
   public boolean isGlobalMandantory()
   {
      return globalMandantory;
   }

}
