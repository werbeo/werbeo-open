package org.infinitenature.werbeo.service.core.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.infinitenature.werbeo.service.core.api.validation.WerbeoFieldValidator.ValidationMode;
import org.springframework.beans.factory.annotation.Autowired;

public class WerbeoSampleNotAllowedValidator
      implements ConstraintValidator<ConfigurableNotAllowedSampleField, Object>
{

   @Autowired
   private WerbeoFieldValidator validatorImpl;

   @Override
   public void initialize(
         ConfigurableNotAllowedSampleField constraintAnnotation)
   {
      validatorImpl.setField(constraintAnnotation.value());
   }

   @Override
   public boolean isValid(Object value, ConstraintValidatorContext context)
   {
      return validatorImpl.isValid(value, context,
            ValidationMode.CHECK_NOT_CONFIGURED);
   }

}
