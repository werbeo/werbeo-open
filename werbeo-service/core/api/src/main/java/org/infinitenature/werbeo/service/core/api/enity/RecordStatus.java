package org.infinitenature.werbeo.service.core.api.enity;

public enum RecordStatus
{
   COMPLETE, IN_PROGRESS
}