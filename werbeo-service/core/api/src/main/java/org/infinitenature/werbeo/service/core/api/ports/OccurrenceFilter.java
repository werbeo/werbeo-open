package org.infinitenature.werbeo.service.core.api.ports;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.validation.PolygonMaxSize;
import org.infinitenature.werbeo.service.core.api.validation.ValidWKT;
import org.infintenature.mtb.MTB;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceFilter
{
   public enum WKTMode
   {
      WITHIN, INTERSECTS
   }

   private Integer taxonId;
   private boolean includeChildTaxa;
   private LocalDate from;
   private LocalDate to;
   private String owner;
   /**
    * the geometry in epsg:4326
    */
   @ValidWKT
   @PolygonMaxSize
   private String wkt;

   private WKTMode wktMode = WKTMode.WITHIN;

   private MTB mtb;
   private Integer maxBlur;

   private Map<Filter, Collection<Portal>> portalBasedFilters = new HashMap<>();
   private String requestingUser;
   private Integer surveyId;
   private Boolean absence = null;
   private String herbaryCode;
   private boolean groupBySample;
   private ValidationStatus validationStatus = null;
   private String validator = null;
   private String occExternalKey = null;
   private LocalDateTime modifiedSince;

   public OccurrenceFilter()
   {
      super();
   }

   public OccurrenceFilter(LocalDate from, LocalDate to)
   {
      super();
      this.from = from;
      this.to = to;
   }

   /**
    *
    * @param from
    * @param to
    * @param taxonId
    * @param wkt
    *           the geometry in epsg:4326
    * @param maxBlur
    * @param inclusiveChildTaxa
    * @param surveyId
    * @param ownerId
    */
   public OccurrenceFilter(LocalDate from, LocalDate to, Integer taxonId,
         String wkt, Integer maxBlur, boolean inclusiveChildTaxa,
         String owner, Integer surveyId)
   {
      super();
      this.from = from;
      this.to = to;
      this.taxonId = taxonId;
      this.wkt = wkt;
      this.maxBlur = maxBlur;
      this.includeChildTaxa = inclusiveChildTaxa;
      setOwner(owner);
      this.surveyId = surveyId;
   }

   public OccurrenceFilter(LocalDate from, LocalDate to, Integer taxonId,
         String wkt, Integer maxBlur, boolean inclusiveChildTaxa,
         String owner, Integer surveyId, boolean groupBySample)
   {
      super();
      this.from = from;
      this.to = to;
      this.taxonId = taxonId;
      this.wkt = wkt;
      this.maxBlur = maxBlur;
      this.includeChildTaxa = inclusiveChildTaxa;
      setOwner(owner);
      this.surveyId = surveyId;
      this.groupBySample = groupBySample;
   }

   public String getOwner()
   {
      return owner;
   }

   public void setOwner(String owner)
   {
      if (owner != null)
      {
         this.owner = owner.toLowerCase();
      } else
      {
         this.owner = owner;
      }
   }

   public OccurrenceFilter withGroupBySample(boolean groupBySample)
   {
      this.groupBySample = groupBySample;
      return this;
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceFilterBeanUtil.doEquals(this, obj);
   }

   public LocalDate getFrom()
   {
      return from;
   }

   public Integer getTaxonId()
   {
      return taxonId;
   }

   public LocalDate getTo()
   {
      return to;
   }

   /**
    *
    * @return the geometry in epsg:4326
    */
   public String getWkt()
   {
      return wkt;
   }

   @Override
   public int hashCode()
   {
      return OccurrenceFilterBeanUtil.doToHashCode(this);
   }

   public void setFrom(LocalDate from)
   {
      this.from = from;
   }

   public void setTaxonId(Integer taxonId)
   {
      this.taxonId = taxonId;
   }

   public void setTo(LocalDate to)
   {
      this.to = to;
   }

   public Integer getMaxBlur()
   {
      return maxBlur;
   }

   public void setMaxBlur(Integer maxBlur)
   {
      this.maxBlur = maxBlur;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

   public boolean isIncludeChildTaxa()
   {
      return includeChildTaxa;
   }

   public void setIncludeChildTaxa(boolean includeChildTaxa)
   {
      this.includeChildTaxa = includeChildTaxa;
   }

   public Map<Filter, Collection<Portal>> getPortalBasedFilters()
   {
      return portalBasedFilters;
   }

   public void setPortalBasedFilters(
         Map<Filter, Collection<Portal>> portalBasedFilters)
   {
      this.portalBasedFilters = portalBasedFilters;
   }

   public String getRequestingUser()
   {
      return requestingUser;
   }

   public void setRequestingUser(String requestingUser)
   {
      this.requestingUser = requestingUser;
   }

   @Override
   public String toString()
   {
      return OccurrenceFilterBeanUtil.doToString(this);
   }

   public static OccurrenceFilter taxonInclChildren(int taxonId)
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.taxonId = taxonId;
      filter.includeChildTaxa = true;
      return filter;
   }

   public Integer getSurveyId()
   {
      return surveyId;
   }

   public OccurrenceFilter setSurveyId(Integer surveyId)
   {
      this.surveyId = surveyId;
      return this;
   }

   public MTB getMtb()
   {
      return mtb;
   }

   public void setMtb(MTB mtb)
   {
      this.mtb = mtb;
   }

   public Boolean getAbsence()
   {
      return absence;
   }

   public void setAbsence(Boolean absence)
   {
      this.absence = absence;
   }

   public String getHerbaryCode()
   {
      return herbaryCode;
   }

   public void setHerbaryCode(String herbaryCode)
   {
      this.herbaryCode = herbaryCode;
   }

   public boolean isGroupBySample()
   {
      return groupBySample;
   }

   public void setGroupBySample(boolean groupBySample)
   {
      this.groupBySample = groupBySample;
   }

   public ValidationStatus getValidationStatus()
   {
      return validationStatus;
   }

   public void setValidationStatus(ValidationStatus validationStatus)
   {
      this.validationStatus = validationStatus;
   }

   public String getValidator()
   {
      return validator;
   }

   public void setValidator(String validator)
   {
      this.validator = validator;
   }

   public String getOccExternalKey()
   {
      return occExternalKey;
   }

   public void setOccExternalKey(String occExternalKey)
   {
      this.occExternalKey = occExternalKey;
   }

   public LocalDateTime getModifiedSince()
   {
      return modifiedSince;
   }

   public void setModifiedSince(LocalDateTime modifiedSince)
   {
      this.modifiedSince = modifiedSince;
   }

   public WKTMode getWktMode()
   {
      return wktMode;
   }

   public void setWktMode(WKTMode wktMode)
   {
      this.wktMode = wktMode;
   }
}