package org.infinitenature.werbeo.service.core.api.infra.query;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;

public interface PersonQueries
      extends EntityQuery<Person, PersonFilter, Integer, PersonSortField>
{

}
