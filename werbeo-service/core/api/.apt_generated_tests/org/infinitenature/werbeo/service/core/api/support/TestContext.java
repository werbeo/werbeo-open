package org.infinitenature.werbeo.service.core.api.support;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Group;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestContext
{
   private Context contextUT;

   @BeforeEach
   void setUp() throws Exception
   {

   }

   @Test
   @DisplayName("User has not accepted TC")
   void test001()
   {
      contextUT = new Context(new Portal(11), new User(),
            Collections.emptySet());
      assertThat(contextUT.isTCAccepted(), is(false));
   }

   @Test
   @DisplayName("User is not approved")
   void test002()
   {
      contextUT = new Context(new Portal(11), new User(),
            Collections.emptySet());
      assertThat(contextUT.isUserApproved(), is(false));
   }

   @Test
   @DisplayName("User is approved to other portal")
   void test003()
   {
      Set<Group> groups = new HashSet<>();
      groups.add(new Group("WERBEO_10_APPROVED"));
      contextUT = new Context(new Portal(11), new User(), groups);
      assertThat(contextUT.isUserApproved(), is(false));
   }

   @Test
   @DisplayName("User is approved")
   void test004()
   {
      Set<Group> groups = new HashSet<>();
      groups.add(new Group("WERBEO_11_APPROVED"));
      contextUT = new Context(new Portal(11), new User(), groups);
      assertThat(contextUT.isUserApproved(), is(true));
   }

   @Test
   @DisplayName("User is approved, but has not accepted TC")
   void test005()
   {
      Set<Group> groups = new HashSet<>();
      groups.add(new Group("WERBEO_11_APPROVED"));
      contextUT = new Context(new Portal(11), new User(), groups);
      assertThat(contextUT.isTCAccepted(), is(false));
   }
}
