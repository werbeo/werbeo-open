package org.infinitenature.werbeo.service.core.maps;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.common.commons.coordinate.impl.CRSHelper;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

public class PlotMap extends AbstractMap
{
   private static final Logger LOGGER = LoggerFactory.getLogger(PlotMap.class);

   public PlotMap(String shapeName, String styleName) throws IOException
   {
      super(shapeName, styleName);
   }

   private MTB[] mtbs;
   private Layer mtbLayer;

   public void setMTBs(MTB... mtbs)
   {
      this.mtbs = mtbs;
   }

   public void setMTBs(Set<MTB> mtbs)
   {
      this.mtbs = mtbs.toArray(new MTB[mtbs.size()]);
   }

   public void drawMTBLayer(int joinFactor)
         throws NoSuchAuthorityCodeException, FactoryException, ParseException
   {
      if (mtbLayer != null)
      {
         map.removeLayer(mtbLayer);
      }
      CoordinateTransformerFactory factory = new CoordinateTransformerFactory();
      CoordinateTransformer transformer = factory
            .getCoordinateTransformer(MTB.getEpsgNumber(), 4326);
      SimpleFeatureTypeBuilder b = new SimpleFeatureTypeBuilder();
      WKTReader wktreader = new WKTReader();
      b.setName("pictures");
      b.setCRS(CRSHelper.getCRS(4326));
      b.add("geom", Polygon.class);
      final SimpleFeatureType TYPE = b.buildFeatureType();
      DefaultFeatureCollection collection = new DefaultFeatureCollection("MTBs",
            TYPE);

      SimpleFeatureBuilder simpleFeatureBuilder = new SimpleFeatureBuilder(
            TYPE);
      Set<String> wkts = preparePolygons(joinFactor);
      for (String wkt : wkts)
      {
         simpleFeatureBuilder.add(wktreader.read(transformer.convert(wkt)));
         collection.add(simpleFeatureBuilder.buildFeature(wkt));
      }
      Style style = SLD.createPolygonStyle(Color.PINK, Color.PINK, .6f);

      mtbLayer = new FeatureLayer(collection, style);

      LOGGER.debug("mtb layer bounds: {} ", mtbLayer.getBounds());
      map.addLayer(mtbLayer);

      // picture location

   }

   protected Set<String> preparePolygons(int joinFactor)
   {
      Set<String> wkts = new HashSet<String>();
      {
         for (MTB mtb : mtbs)
         {
            try
            {
               wkts.add(createEnvelope(mtb, joinFactor));
            }
            catch (Exception e)
            {
               LOGGER.error(
                     "Error creating envelope from MTB: " + mtb.toString(), e);
            }
         }
      }
      return wkts;
   }

   protected String createEnvelope(MTB mtb, int joinFactor)
   {
      int y = Integer.parseInt(mtb.getMtb().substring(0, 2));
      int x = Integer.parseInt(mtb.getMtb().substring(2, 4));

      int envYmin = joinFactor * (y / joinFactor);
      int envXmin = joinFactor * (x / joinFactor);

      int envYmax = envYmin + joinFactor - 1;
      int envXmax = envXmin + joinFactor - 1;

      DecimalFormat decimalFormat = new DecimalFormat("00");
      MTB uperleft = MTBHelper.toMTB(
            decimalFormat.format(envYmin) + decimalFormat.format(envXmin));
      MTB lowerRight = MTBHelper.toMTB(
            decimalFormat.format(envYmax) + decimalFormat.format(envXmax));

      WKTReader wktReader = new WKTReader();
      try
      {
         Geometry a = wktReader.read(uperleft.toWkt());
         Geometry b = wktReader.read(lowerRight.toWkt());

         Geometry union = a.union(b);
         return union.getEnvelope().toText();
      } catch (ParseException e)
      {
         LOGGER.error("Failure creating envelope.", e);
      }

      return null;
   }
}
