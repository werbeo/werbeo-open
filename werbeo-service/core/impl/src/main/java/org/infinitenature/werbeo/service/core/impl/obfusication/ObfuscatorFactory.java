package org.infinitenature.werbeo.service.core.impl.obfusication;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.fields.NotConfiguredOccurrenceFieldsObfusicator;
import org.infinitenature.werbeo.service.core.impl.obfusication.fields.NotConfiguredSampleFieldsObfusicator;
import org.infinitenature.werbeo.service.core.impl.obfusication.nopeople.NoPersonOccurrenceObfuscator;
import org.infinitenature.werbeo.service.core.impl.obfusication.nopeople.NoPersonSampleObfuscator;
import org.infinitenature.werbeo.service.core.impl.obfusication.position.PositionOccurrenceCentroidObfuscator;
import org.infinitenature.werbeo.service.core.impl.obfusication.position.PositionOccurrenceObfuscator;
import org.infinitenature.werbeo.service.core.impl.obfusication.position.PositionSampleObfuscator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObfuscatorFactory
{
   @Autowired
   private FieldConfigQueries fieldConfigQueries;
   @Autowired
   private NoPersonOccurrenceObfuscator noPersonOccurrenceObfuscator;
   @Autowired
   private NoPersonSampleObfuscator noPersonSampleObfuscator;
   @Autowired
   private PositionSampleObfuscator positionSampleObfuscator;
   @Autowired
   private PositionOccurrenceObfuscator positionOccurrenceObfuscator;
   @Autowired
   private PositionOccurrenceCentroidObfuscator positionOccurrenceCentroidObfuscator;
   public EntityObfuscator<Occurrence> getOccurrenceObfuscator(Survey survey,
         Context context)
   {
      List<EntityObfuscator<Occurrence>> obfusicators = new ArrayList<>();
      obfusicators.add(
            new NotConfiguredOccurrenceFieldsObfusicator(fieldConfigQueries));
      obfusicators.add(noPersonOccurrenceObfuscator);
      obfusicators.add(positionOccurrenceObfuscator);
      return new ChainedEntityObfuscator<>(obfusicators);
   }

   public EntityObfuscator<OccurrenceCentroid> getOccurrenceCentroidObfusicator(
         Survey survey, Context context)
   {
      return positionOccurrenceCentroidObfuscator;
   }

   public EntityObfuscator<Sample> getSampleObfuscator(Survey survey,
         Context context)
   {
      List<EntityObfuscator<Sample>> obfusicators = new ArrayList<>();
      obfusicators
            .add(new NotConfiguredSampleFieldsObfusicator(fieldConfigQueries));
      obfusicators.add(noPersonSampleObfuscator);
      obfusicators.add(positionSampleObfuscator);
      return new ChainedEntityObfuscator<>(obfusicators);
   }

   public void setFieldConfigQueries(FieldConfigQueries fieldConfigQueries)
   {
      this.fieldConfigQueries = fieldConfigQueries;
   }

   public void setNoPersonOccurrenceObfuscator(
         NoPersonOccurrenceObfuscator noPersonOccurrenceObfuscator)
   {
      this.noPersonOccurrenceObfuscator = noPersonOccurrenceObfuscator;
   }

   public void setNoPersonSampleObfuscator(
         NoPersonSampleObfuscator noPersonSampleObfuscator)
   {
      this.noPersonSampleObfuscator = noPersonSampleObfuscator;
   }

   public void setPositionSampleObfuscator(
         PositionSampleObfuscator positionSampleObfuscator)
   {
      this.positionSampleObfuscator = positionSampleObfuscator;
   }

   public void setPositionOccurrenceObfuscator(
         PositionOccurrenceObfuscator positionOccurrenceObfuscator)
   {
      this.positionOccurrenceObfuscator = positionOccurrenceObfuscator;
   }

   public void setPositionOccurrenceCentroidObfuscator(
         PositionOccurrenceCentroidObfuscator positionOccurrenceCentroidObfuscator)
   {
      this.positionOccurrenceCentroidObfuscator = positionOccurrenceCentroidObfuscator;
   }

}
