package org.infinitenature.werbeo.service.core.impl.imports.csv;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface CSVImporter
{
   String importCSV(String csv, String protocol, ImportJob importJob, Context context);
   boolean handlesPortal(int portalId);
}
