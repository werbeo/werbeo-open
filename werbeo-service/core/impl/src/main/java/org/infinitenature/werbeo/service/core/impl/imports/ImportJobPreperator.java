package org.infinitenature.werbeo.service.core.impl.imports;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.core.PositionUtils;
import org.infinitenature.werbeo.service.core.api.enity.Herbarium;
import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.ImportPart;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.enity.Language;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatusFukarek;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Vitality;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportQueries;
import org.infinitenature.werbeo.service.core.api.ports.PeopleCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.infinitenature.werbeo.service.core.api.ports.PersonQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxonQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.vergien.bde.model.MetaDataType;
import org.vergien.bde.model.OccurrenceType;
import org.vergien.bde.model.PersonType;
import org.vergien.bde.model.SampleType;
import org.vergien.bde.model.VagueDateType;

import de.vegetweb.bde.xml.reader.StreamingXMLReader;

@Service
public class ImportJobPreperator
{
   private static final Set<Language> latinOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.LAT)));
   private final static int SURNAME_MAX_LENGTH = 49;
   private final static int FUIRSTNAME_MAX_LENGTH = 49;
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ImportJobPreperator.class);
   @Autowired
   private ImportCommands importComands;
   @Autowired
   private ImportQueries importQueries;
   @Autowired
   private SurveyQueryPort surveyQueries;
   @Autowired
   private SurveyCommandPort surveyCommandPort;
   @Autowired
   private TaxonQueryPort taxonQueries;
   @Autowired
   private PersonQueryPort personQueryPort;
   @Autowired
   private PeopleCommandPort peopleCommandPort;
   @Autowired
   private TaxaListQueryPort taxaListQueries;
   @Autowired
   private ImportFailureAggregator importFailureAggregator;

   @Scheduled(fixedDelay = 1_000)
   public void prepareJobs()
   {
      Optional<ImportJob> nextJob = importQueries
            .findOneImportJobByStatus(JobStatus.INITIALIZED);
      while (nextJob.isPresent())
      {
         String bdeXml = importQueries.getBDEXML(nextJob.get());
         Context context = importQueries.getContext(nextJob.get());
         try
         {
            Path createTempFile = Files.createTempFile(
                  "importPreparation-" + nextJob.get().getId(), ".txt");

            try (FileWriter logFile = new FileWriter(createTempFile.toFile());)
            {
               InputStream sampleIs = new ByteArrayInputStream(
                     bdeXml.getBytes(StandardCharsets.UTF_8));
               InputStream personIs = new ByteArrayInputStream(
                     bdeXml.getBytes(StandardCharsets.UTF_8));
               processImport(sampleIs, personIs, context, nextJob.get(),
                     logFile);

               logFile.close();
               importComands.addLogFile(nextJob.get(), createTempFile, context);
               importComands.setJobStatus(nextJob.get(), JobStatus.PREPARED,
                     context);
               nextJob = importQueries
                     .findOneImportJobByStatus(JobStatus.INITIALIZED);
            }
         } catch (Exception e)
         {
            LOGGER.error("Failure preaparing import", e);
         }
      }
   }

   @Scheduled(fixedDelay = 1_000)
   public void startJob()
   {
      try
      {
         Optional<ImportJob> nextJob = importQueries
               .findOneImportJobByStatus(JobStatus.RUNNING);
         if (nextJob.isPresent())
         {
            if (importQueries.isJobNotFinished(nextJob.get()))
            {
               return;
            } else
            {
               LOGGER.info("Setting job {} to status FINISHED.",
                     nextJob.get().getId());
               Context context = importQueries.getContext(nextJob.get());
               importComands.setJobStatus(nextJob.get(), JobStatus.FINISHED,
                     context);
               importFailureAggregator.collectFailures(nextJob.get(), context);
            }
         } else
         {
            nextJob = importQueries
                  .findOneImportJobByStatus(JobStatus.PREPARED);
            if (nextJob.isPresent())
            {
               LOGGER.info("Starting job {}", nextJob.get().getId());
               Context context = importQueries.getContext(nextJob.get());
               importComands.setJobStatus(nextJob.get(), JobStatus.RUNNING,
                     context);
               return;
            }
         }
      } catch (Exception e)
      {
         LOGGER.error("Something went wrong handling job status", e);
      }
   }

   private int processImport(InputStream sampleIs, InputStream personIs,
         Context context, ImportJob importJob, Writer logFile)
         throws IOException
   {
      int sampleCount = 0;
      String fileEncoding = "utf-8";
      try (Reader personXml = new InputStreamReader(personIs, fileEncoding);
            Reader sampleXml = new InputStreamReader(sampleIs, fileEncoding))
      {
         MetaDataType metaDataType = StreamingXMLReader
               .parseMetaData(personXml);
         Map<String, PersonType> personMap = new HashMap<>();
         for (PersonType personToImport : metaDataType.getPersons().getPerson())
         {
            personMap.put(personToImport.getId(), personToImport);
         }

         long counter = StreamingXMLReader.extractSamples(sampleXml,
               sampleType -> prepareSample(sampleType, context, importJob,
                     logFile),
               personMap);
         LOGGER.info("Prepared {} samples for job {}", counter, importJob);
      } catch (IOException | XMLStreamException | JAXBException
            | XPathExpressionException e)
      {

         logFile.write("Failure preparing import from file\n");
         logFile.write(ExceptionUtils.getStackTrace(e));
         logFile.append("\n");
         LOGGER.error("Failure preparing import from file", e);
      }
      return sampleCount;

   }

   private void prepareSample(SampleType sampleToImport, Context context,
         ImportJob importJob, Writer logFile)

   {
      Map<Occurrence, List<OccurrenceComment>> occurrenceComments = new HashMap<>();
      List<SampleComment> sampleComments = new ArrayList<>();

      try
      {
         Survey survey = prepareSurvey(sampleToImport, context);

         Sample sample = new Sample();
         sample.setSurvey(survey);
         if (StringUtils.isNotBlank(sampleToImport.getUuid()))
         {
            sample.setId(UUID.fromString(sampleToImport.getUuid()));
         }
         Locality locality = new Locality();
         locality.setPosition(BDEMapper.map(sampleToImport.getPosition()));

         if (sampleToImport.getPosition().getPrecision() != null)
         {
            locality.setPrecision(sampleToImport.getPosition().getPrecision());
         } else // only in case of mtb
         {
            locality.setPrecision(
                  PositionUtils.calculatePrecision(locality.getPosition()));
         }
         if (StringUtils
               .isNotBlank(sampleToImport.getPosition().getDescription()))
         {
            locality.setLocality(sampleToImport.getPosition().getDescription());
         }
         sample.setLocality(locality);
         sample.setDate(map(sampleToImport.getDate()));
         if (sampleToImport.getOwner() != null)
         {
            sample.setCreatedBy(getUser(sampleToImport.getOwner()));
         }
         for (PersonType recorder : sampleToImport.getRecorders())
         {
            Person person = getPerson(recorder, context);
            if (person == null)
            {
               throw new IllegalArgumentException(
                     "Can't load recorder for sample: "
                           + sampleToImport.getUuid());
            }
            sample.setRecorder(person);
            break;
         }

         for (OccurrenceType occurrenceToImport : sampleToImport
               .getOccurrences().getOccurrence())
         {

            prepareOccurrence(context, sample, occurrenceToImport,
                  occurrenceComments);
         }

         // do this at the end, so we can use the samples as keys
         if (sampleToImport.getPosition().getPrecision() == null)
         {
            addSampleComment(sampleComments, sample, "Precision is guessed.");
         }

         if (sampleToImport.getComments() != null)
         {
            for (String comment : sampleToImport.getComments().getComment())
            {
               addSampleComment(sampleComments, sample, comment);
            }
         }
         LOGGER.info("Prepared sample {} to import for job {}", sample.getId(),
               importJob);
         ImportPart importPart = new ImportPart();
         importPart.setImportJob(importJob);
         importPart.setSample(sample);
         importPart.setSampleComments(sampleComments);
         importPart.setOccurrenceComments(occurrenceComments);
         importPart.setStatus(JobStatus.PREPARED);

         importComands.saveOrUpdate(importPart);
      } catch (Exception e)
      {
         String message = "Failure preparing import from file";
         logToFile(logFile, message, e);

         LOGGER.error("Failure preparing import for Sample: {}", sampleToImport,
               e);
      }
   }

   private void logToFile(Writer logFile, String message, Throwable t)
   {
      try
      {
         logFile.write(message);
         logFile.append('\n');
         logFile.write(ExceptionUtils.getStackTrace(t));
         logFile.append('\n');
      } catch (IOException e)
      {
         LOGGER.error("Failure writeing to log file", e);
      }
   }

   private User getUser(PersonType owner)
   {
      User user = new User();
      user.setLogin(owner.getEmail());
      Person person = new Person(owner.getFirstName(), owner.getLastName());
      user.setPerson(person);
      return user;
   }

   private void addSampleComment(List<SampleComment> sampleComments,
         Sample sample, String comment)
   {

      SampleComment sampleComment = new SampleComment();
      sampleComment.setComment(comment);
      sampleComments.add(sampleComment);
   }

   private void prepareOccurrence(Context context, Sample sample,
         OccurrenceType occurrenceToImport,
         Map<Occurrence, List<OccurrenceComment>> occurrenceComments)
   {
      Occurrence occurrence = null;

      occurrence = new Occurrence();
      sample.getOccurrences().add(occurrence);

      if (StringUtils.isNotBlank(occurrenceToImport.getUuid()))
      {
         occurrence.setId(UUID.fromString(occurrenceToImport.getUuid()));
      }

      if (occurrenceToImport.getHerbariumSpecimen() != null)
      {
         occurrence.setHerbarium(new Herbarium(
               occurrenceToImport.getHerbariumSpecimen().getHerbarium(),
               occurrenceToImport.getHerbariumSpecimen().getCode()));
      }
      occurrence.setTaxon(mapTaxon(occurrenceToImport, context));
      occurrence.setExternalKey(occurrenceToImport.getExternalKey());
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      if (occurrenceToImport.getVitality() != null)
      {
         switch (occurrenceToImport.getVitality())
         {
         case DEAD:
            occurrence.setVitality(Vitality.ABGESTORBEN);
            break;
         case DYING_OFF:
            occurrence.setVitality(Vitality.ABSTERBEND);
            break;
         case WASTE_AWAY:
            occurrence.setVitality(Vitality.KUEMMERND);
            break;
         case VITAL:
            occurrence.setVitality(Vitality.VITAL);
            break;
         default:
            break;
         }
      }
      if (occurrenceToImport.getStatus() != null)
      {
         switch (occurrenceToImport.getStatus())
         {
         case WILD:
            occurrence
                  .setSettlementStatusFukarek(SettlementStatusFukarek.INDIGEN);
            break;
         case ESCAPED:
            occurrence.setSettlementStatusFukarek(
                  SettlementStatusFukarek.EINGEBUERGERT);
            break;
         case PLANTED:
            occurrence.setSettlementStatusFukarek(
                  SettlementStatusFukarek.SYNANTHROP);
            break;
         default:
            break;
         }
      }
      for (Object determiner : occurrenceToImport.getDeterminers())
      {
         Person person = getPerson((PersonType) determiner, context);
         if (person == null)
         {
            throw new IllegalArgumentException(
                  "Can't load determiner for occurrence: "
                        + occurrenceToImport.getUuid());
         }
         occurrence.setDeterminer(person);
      }

      if (occurrence.getDeterminer() == null)
      {
         occurrence.setDeterminer(sample.getRecorder());
         initOccurrenceComments(occurrenceComments, occurrence);
         OccurrenceComment comment = new OccurrenceComment();
         comment.setComment("Importer: Kein Bestimmer gefunden, nutze Finder");
         occurrenceComments.get(occurrence).add(comment);
      }

      if (occurrenceToImport.getComments() != null)
      {
         for (String comment : occurrenceToImport.getComments().getComment())
         {
            initOccurrenceComments(occurrenceComments, occurrence);
            OccurrenceComment occurrenceComment = new OccurrenceComment();
            occurrenceComment.setComment(comment);
            occurrenceComments.get(occurrence).add(occurrenceComment);
         }
      }
   }

   private void initOccurrenceComments(
         Map<Occurrence, List<OccurrenceComment>> occurrenceComments,
         Occurrence occurrence)
   {
      if (!occurrenceComments.containsKey(occurrence))
      {
         occurrenceComments.put(occurrence, new ArrayList<>());
      }
   }

   private TaxonBase mapTaxon(OccurrenceType occurrenceToImport,
         Context context)
   {

      // TODO: loadByExternalId
      Slice<TaxaList, TaxaListSortField> taxaListResult = taxaListQueries.find(
              new TaxaListFilter(occurrenceToImport.getTaxonRefList()), context,
            new OffsetRequestImpl<>(0, 1, SortOrder.ASC, TaxaListSortField.ID));
      if (!taxaListResult.hasContent())
      {
         throw new IllegalArgumentException("TaxonRef list: "
               + occurrenceToImport.getTaxonRefList() + " not supported.");
      }
      TaxaList taxaList = taxaListResult.getContent().get(0);
      
      
      TaxonBase taxon = null;
      
      try
      {
      taxon = taxonQueries.getByExternalId(taxaList,
            occurrenceToImport.getTaxonRef(), context);
      }
      catch(EntityNotFoundException e)
      {
    	  
      }

      if (taxon == null)
      {
         LOGGER.warn("Taxon with ref: {} not found.",
               occurrenceToImport.getTaxonRef());
         TaxonFilter taxaFilter = new TaxonFilter(occurrenceToImport.getName(),
               latinOnly, true, null, false, new HashSet<String>(), null, false);
         taxon = taxonQueries
               .find(taxaFilter, context,
                     new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
                           TaxonSortField.ID))
               .getContent().stream().findFirst().orElse(null);
         if (taxon == null)
         {
            throw new IllegalArgumentException("Taxon with ref: "
                  + occurrenceToImport.getTaxonRef() + " and name: "
                  + occurrenceToImport.getName() + " not found.");
         } else
         {
            LOGGER.warn(
                  "Taxon with name: {}, has ref: {} in import, but {} in the database.",
                  occurrenceToImport.getName(),
                  occurrenceToImport.getTaxonRef(), taxon.getExternalKey());
            return taxon;
         }
      } else if (!taxon.getName().equals(occurrenceToImport.getName()))
      {
         LOGGER.warn(
               "Taxon with ref {} and name {} does not match name in database {}",
               occurrenceToImport.getTaxonRef(), occurrenceToImport.getName(),
               taxon.getName());
      }
      return taxon;
   }

   private Survey prepareSurvey(SampleType sampleType, Context context)
   {
      String surveyName = sampleType.getProject();
      Survey survey = null;

      Slice<Survey, SurveySortField> surveys = surveyQueries.find(surveyName,
            null, null, context,
            new OffsetRequestImpl<>(0, 1, SortOrder.ASC, SurveySortField.ID));

      if (surveys.getContent().isEmpty())
      {
         LOGGER.info("Survey for project {} not yet in database.", surveyName);
         survey = new Survey();
         survey.setName(surveyName);
         survey.setAvailability(Availability.FREE);
         survey.setDescription("Dummy");
         survey.setPortal(context.getPortal());
         int surveyId = surveyCommandPort.saveOrUpdate(survey, context);
         survey = surveyQueries.get(surveyId, context);
         LOGGER.info("Created new Survey {} with id {}.", survey.getName(),
               surveyId);
      } else
      {
         LOGGER.info("Survey for project {} already in database.", surveyName);
         survey = surveys.getContent().get(0);
      }
      return survey;
   }

   private VagueDate map(VagueDateType date)
   {
      try
      {
         String type = date.getType();
         if ("Y".equals(type))
         {
            return VagueDateFactory.create(Integer.valueOf(date.getValue()));
         } else if ("D".equals(type))
         {
            return VagueDateFactory.fromDate(LocalDate.parse(date.getValue(),
                  DateTimeFormatter.ofPattern("yyyy-MM-dd")));
         } else if ("-Y".equals(type))
         {
            return VagueDateFactory.createTo(Year
                  .parse(date.getValue(), DateTimeFormatter.ofPattern("-yyyy"))
                  .getValue());
         } else if ("O".equals(type))
         {
            YearMonth yearMonth = YearMonth.parse(date.getValue(),
                  DateTimeFormatter.ofPattern("MM-yyyy"));
            return VagueDateFactory.createMonthInYear(
                  yearMonth.get(ChronoField.YEAR),
                  yearMonth.get(ChronoField.MONTH_OF_YEAR));
         } else if ("DD".equals(type))
         {
            String[] dates = date.getValue()
                  .split(getDelemiter(date.getValue()));
            DateTimeFormatter formatter = DateTimeFormatter
                  .ofPattern("yyyy-MM-dd");
            return VagueDateFactory.create(LocalDate.parse(dates[0], formatter),
                  LocalDate.parse(dates[1], formatter), "DD");
         }
      } catch (Exception e)
      {
         throw new IllegalArgumentException("Can't parse date: " + date, e);
      }
      throw new IllegalArgumentException("Can't parse date: " + date);
   }

   protected static String getDelemiter(String date)
   {
      if (date.indexOf(" - ") != -1)
      {
         return " - ";
      } else if (date.indexOf(" to ") != -1)
      {
         return " to ";
      }
      return null;
   }

   private Person getPerson(PersonType personToImport, Context context)
   {
      if (personToImport.getEmail() != null)
      {
         personToImport.setEmail(personToImport.getEmail().toLowerCase());
      }
      Person person = loadPerson(personToImport, context);
      if (person == null)
      {
         synchronized (OccurrenceImportCommandsPortImpl.class)
         {
            person = loadPerson(personToImport, context);
            if (person == null)
            {
               person = createPerson(personToImport);
               Integer id = peopleCommandPort.saveOrUpdate(person, context);
               person = personQueryPort.get(id, context);
            }
         }
      }

      if (!StringUtils.equals(person.getEmail(), personToImport.getEmail())
            && StringUtils.isNotBlank(personToImport.getEmail()))
      {
         person.setEmail(personToImport.getEmail().trim());
         Integer id = peopleCommandPort.saveOrUpdate(person, context);
         person = personQueryPort.get(id, context);
      }
      return person;
   }

   private Person loadPerson(PersonType personToImport, Context context)
   {
      Person person = null;
      if (StringUtils.isNotBlank(personToImport.getEmail()))
      {
         PersonFilter filter = new PersonFilter(null,
               personToImport.getEmail());
         LOGGER.info("Checking if person exists, by mail");
         Slice<Person, PersonSortField> response = personQueryPort.find(filter,
               context, new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
                     PersonSortField.ID));
         if (response.getContent().size() == 1)
         {
            person = response.getContent().get(0);
            LOGGER.info("Found person {} by mail", person);
         } else
         {
            LOGGER.info("Found no person by mail");
         }
      }

      if (person == null)
      {
         LOGGER.info("Checking if person exists, by name");
         PersonFilter filter = new PersonFilter()
               .setFirstName(normalize(personToImport.getFirstName(),
                     FUIRSTNAME_MAX_LENGTH))
               .setLastName(normalize(personToImport.getLastName(),
                     SURNAME_MAX_LENGTH));
         Slice<Person, PersonSortField> people = personQueryPort.find(filter,
               context, new OffsetRequestImpl<>(0, 2, SortOrder.ASC,
                     PersonSortField.NAME));

         if (people.getContent().size() == 1)
         {
            person = people.getContent().get(0);
            LOGGER.info("Found person {} by name", person);
         } else if (people.getContent().size() > 1)
         {
            LOGGER.warn("More then one person found {}", people);
            person = people.getContent().get(0);
         }

      }
      return person;
   }

   private Person createPerson(PersonType personToImport)
   {
      Person person = new Person(null,
            normalize(personToImport.getFirstName(), FUIRSTNAME_MAX_LENGTH),
            normalize(personToImport.getLastName(), SURNAME_MAX_LENGTH));
      person.setExternalKey(personToImport.getId());
      person.setEmail(personToImport.getEmail());
      if(StringUtils.isBlank(personToImport.getEmail()))
      {
			person.setEmail(generatePseudoEmail(personToImport.getFirstName(), personToImport.getLastName()));
    	  LOGGER.info("Generated Email-Address: " + person.getEmail());
      }
      return person;
   }
   
   protected String generatePseudoEmail(String firstname, String lastname)
   {
      if (StringUtils.isNotBlank(firstname) && StringUtils.isNotBlank(lastname))
      {
         return noUmlautsNoPoints(firstname) + "."
               + noUmlautsNoPoints(lastname) + "@werbeo.de";
      } else
      {
         return noUmlautsNoPoints(lastname) + "@werbeo.de";
      }
   }
   
   private String noUmlautsNoPoints(String string)
   {
      return string.trim().replace(" ", "").replace(".", "").replace("(", "").replace(")", "").replace("ä", "ae")
            .replace("ö", "oe").replace("ü", "ue").replace("ß", "ss");
   }

   public static String normalize(String value, int length)
   {
      if (StringUtils.isBlank(value))
      {
         return "unknown";
      }
      String fixedEncoding = Encoder.replace(value);
      return truncate(fixedEncoding, length).trim();
   }

   public static String truncate(String value, int length)
   {
      if (value != null && value.length() > length)
      {
         value = value.substring(0, length);
      }
      return value;
   }
}
