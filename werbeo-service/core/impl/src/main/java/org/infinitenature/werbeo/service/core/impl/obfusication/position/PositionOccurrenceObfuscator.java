package org.infinitenature.werbeo.service.core.impl.obfusication.position;

import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.EntityObfuscator;
import org.springframework.stereotype.Component;

@Component
public class PositionOccurrenceObfuscator extends AbstractPositionObfuscator
      implements EntityObfuscator<Occurrence>
{

   public PositionOccurrenceObfuscator(PositionFactory positionFactory)
   {
      super(positionFactory);
   }

   @Override
   public Occurrence obfuscate(Occurrence entity, Context context)
   {
      entity.setSample(doObfusicate(entity.getSample(), context));
      return entity;
   }

}
