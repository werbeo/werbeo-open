package org.infinitenature.werbeo.service.core.impl.validation;

import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.ports.TaxonQueryPort;
import org.infinitenature.werbeo.service.core.api.validation.ITaxonActiveOnPortal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class TaxonActiveOnPortalValidatorImpl implements ITaxonActiveOnPortal
{
   @Autowired
   private TaxonQueryPort taxonQueryPort;

   @Override
   public boolean isValid(TaxonBase taxon)
   {
      if (taxon != null)
      {
         try
         {
            // Throws exception if taxon is not in taxa list which is valid for the
            // portal
            taxonQueryPort.get(taxon.getId(), ValidationContextHolder.get());
         } catch (EntityNotFoundException e)
         {
            return false;
         }
      }
      return true;
   }
}
