package org.infinitenature.werbeo.service.core.access.checks;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.stereotype.Component;

@Component
public class OccurrenceAccessRightsCheck
      extends SimpleAccessRightCheck<Occurrence>
{

   public OccurrenceAccessRightsCheck()
   {
      super(Occurrence.class);
   }

   @Override
   protected boolean isAllowedToCreate(Occurrence entity, Context context)
   {
      return true;
   }

   @Override
   protected boolean isAllowedToFind(Class<? extends Occurrence> type,
         Context context)
   {
      return true;
   }


}
