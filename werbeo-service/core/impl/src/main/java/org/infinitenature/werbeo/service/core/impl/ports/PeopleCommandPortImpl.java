package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.infra.commands.PeopleCommands;
import org.infinitenature.werbeo.service.core.api.ports.PeopleCommandPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeopleCommandPortImpl implements PeopleCommandPort
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(PeopleCommandPortImpl.class);

   @Autowired
   private PeopleCommands peopleCommands;

   @Autowired
   private AccessRightsChecker accessRightsChecker;
   @Autowired
   private Validator validator;

   @Override
   public Integer saveOrUpdate(Person person, Context context)
   {
      if (person.getId() == null)
      {
         accessRightsChecker.isAllowedTo(Operation.CREATE, person, context);
         return create(person, context);
      } else
      {
         accessRightsChecker.isAllowedTo(Operation.UPDATE, person, context);
         return update(person, context);
      }
   }


   @Override
   public void delete(Integer personId, Context context)
   {
      throw new UnsupportedOperationException("not yet implemented");
   }

   private Integer update(Person person, Context context)
   {
      LOGGER.info("Update person {} for context {}", person, context);
      validate(person);
      return peopleCommands.saveOrUpdate(person, context);
   }

   private void validate(Person person)
   {
      Set<ConstraintViolation<Person>> constraintViolations = validator
            .validate(person);
      if (!constraintViolations.isEmpty())
      {
         LOGGER.error("Failure validating sample {}", constraintViolations);
         throw new ConstraintViolationException(constraintViolations);
      }
   }

   private Integer create(Person person, Context context)
   {
      LOGGER.info("Creating person {} for context {}", person, context);
      validate(person);
      return peopleCommands.saveOrUpdate(person, context);
   }

}
