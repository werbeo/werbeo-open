package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.TreeMap;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.ports.TranslationQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TranslationQueryPortImpl implements TranslationQueryPort
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TranslationQueryPortImpl.class);
   private static final String BUNDLE_NAME = "messages";

   @Override
   public String getTranslation(Context context, String key, Locale locale)
   {
      return getString(key, locale, context.getPortal());
   }

   @Override
   public Map<String, String> getTranslations(Context context, Locale locale)
   {
      // TODO Hacky implementation because of:
      // https://git.loe.auf.uni-rostock.de/werbeo/werbeo/issues/697#note_17399

      Map<String, String> translations = new TreeMap<>();
      ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME,
            Locale.GERMANY);
      for (String key : bundle.keySet())
      {
         translations.put(key, bundle.getString(key));
      }
      return translations;
   }

   private String getString(String key, Locale locale, Portal portal)
   {
      if (portal != null && ResourceBundle.getBundle(BUNDLE_NAME, locale)
            .containsKey(key + "." + portal.getId()))
      {
         return ResourceBundle.getBundle(BUNDLE_NAME, locale)
               .getString(key + "." + portal.getId());
      }

      try
      {
         if (locale != null)
         {
            if (!(locale.equals(Locale.GERMANY) || locale.equals(Locale.GERMAN))
                  && ResourceBundle.getBundle(BUNDLE_NAME, locale)
                        .containsKey(key))
            {
               return ResourceBundle.getBundle(BUNDLE_NAME, locale)
                     .getString(key);
            }
            return ResourceBundle.getBundle(BUNDLE_NAME, Locale.GERMANY)
                  .getString(key);
         } else
         {
            return defaultString(key, locale);
         }
      } catch (MissingResourceException e)
      {
         return defaultString(key, locale);
      }
   }

   private String defaultString(String key, Locale locale)
   {
      LOGGER.warn(
            "The key \"{}\" is not in the resource bundel for the locale \"{}\".",
            key, locale);
      return '!' + key + '!';
   }
}
