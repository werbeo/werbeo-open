package org.infinitenature.werbeo.service.core.impl.export;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Export;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceExportSortField;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceExportCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ExportFacade
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ExportFacade.class);
   private final String exportPath;
   private final List<OccurrenceExportCommands> occurrenceExports;
   private final OccurrenceQueryPort occurrenceQueries;
   private final FieldConfigQueries fieldConfigQueries;

   public ExportFacade(@Autowired OccurrenceQueryPort occurrenceQueries,
         @Autowired InstanceConfig instanceConfig,
         @Autowired(required = false) List<OccurrenceExportCommands> occurrenceExports,
         @Autowired FieldConfigQueries fieldConfigQueries)
   {
      super();
      this.occurrenceQueries = occurrenceQueries;
      this.exportPath = instanceConfig.getDir();
      this.occurrenceExports = Collections.unmodifiableList(occurrenceExports);
      this.fieldConfigQueries = fieldConfigQueries;
   }

   @Async
   public void createExport(OccurrenceFilter filter, FileFormat format,
         UUID exportId, Context context)
   {
      Validate.notNull(format, "The export format must not be null");
      Validate.notNull(filter, "The filter must not be null");
      Validate.notNull(exportId, "The exportId must not be null");
      Validate.notNull(context, "The context must not be null");

      Optional<OccurrenceExportCommands> opt = occurrenceExports.stream()
            .filter(exp -> exp.getSupportedFileFormat() == format).findFirst();
      if (!opt.isPresent())
      {
         throw new IllegalStateException(
               "No exporter for " + format + "found.");
      }
      OccurrenceExportCommands export = opt.get();
      try (OutputStream os = createOutputStream(exportId, format, context))
      {
         Stream<Occurrence> occurrenceStream = StreamSupport.stream(
               new OccurrenceSplitarator(occurrenceQueries, filter, context),
               false);
         try
         {
            export.export(occurrenceStream, os,
                  fieldConfigQueries.getOccurrenceConfig(context.getPortal()),
                  fieldConfigQueries.getSampleConfig(context.getPortal()),
                  fieldConfigQueries.getSurveyConfig(context.getPortal()),
                  context.getPortal().getId());
         } catch (Exception e)
         {
            LOGGER.error("Failure creating export with id {}.", exportId, e);
            Files.move(
                  createExportFile(exportId, format, context,
                        JobStatus.RUNNING),
                  createExportFile(exportId, format, context,
                        JobStatus.FAILURE),
                  REPLACE_EXISTING);
            return;
         }
         Files.move(
               createExportFile(exportId, format, context, JobStatus.RUNNING),
               createExportFile(exportId, format, context, JobStatus.FINISHED),
               REPLACE_EXISTING, ATOMIC_MOVE);
      } catch (IOException e)
      {
         LOGGER.error("Failure creating export with id {}.", exportId, e);
      }
   }

   public void deleteExport(UUID exportId, Context context)
   {
      try
      {
         Job job = getJob(exportId, context);
         if (job.getFileFormat() == null)
         {
            throw new EntityNotFoundException(
                  "Export job with id " + exportId + " not found");
         }
         Files.delete(
               createExportFile(exportId, job.getFileFormat(), context,
                     job.getJobStatus()));
      } catch (IOException e)
      {
         LOGGER.error("Failure deleting export with id {}.", exportId, e);
         throw new WerbeoException("Failure deleting export with id " + exportId);
      }
   }

   private OutputStream createOutputStream(UUID exportId, FileFormat format,
         Context context) throws IOException
   {
      Path exportFile = createExportFile(exportId, format, context,
            JobStatus.RUNNING);
      ensureDirExists(context);
      LOGGER.info("create export file {}", exportFile);
      return new FileOutputStream(exportFile.toFile());
   }

   private void ensureDirExists(Context context) throws IOException
   {
      for (JobStatus status : JobStatus.values())
      {
         Path path = exportDir(context, status);
         Files.createDirectories(path);
         path.toFile().mkdirs();
      }
   }

   private Path createExportFile(UUID exportId, FileFormat format,
         Context context, JobStatus status)
   {
      return exportDir(context, status)
            .resolve(exportId.toString() + "." + format.getSuffix());
   }

   private Path exportDir(Context context, JobStatus status)
   {
      return userDir(context).resolve(status.toString());
   }

   private Path userDir(Context context)
   {
      return Paths.get(exportPath, "exports",
            String.valueOf(context.getPortal().getId()),
            String.valueOf(context.getUser().getLogin()));
   }

   public Export getStatus(UUID exportId, Context context)
   {
      for (JobStatus status : JobStatus.values())
      {
         for (FileFormat format : FileFormat.values())
         {
            if (Files
                  .exists(createExportFile(exportId, format, context, status)))
            {
               LocalDateTime creation = getCreationTime(createExportFile(exportId, format, context, status));
               return new Export(exportId, status, format.getSuffix(), creation);
            }
         }
      }
      throw new EntityNotFoundException(
            "Export job with id " + exportId + " not found");
   }

   public TypedFile getFile(UUID exportId, Context context)
   {
      FileFormat formatToExport = getFileFormat(exportId, context, JobStatus.FINISHED);
      if (formatToExport == null)
      {
         throw new EntityNotFoundException(
               "Export job FINISHED with id " + exportId + " not found");
      }
      return loadFile(exportId, formatToExport, context);
   }

   private FileFormat getFileFormat(UUID exportId, Context context, JobStatus jobStatus)
   {
      FileFormat formatToExport = null;
      for (FileFormat format : FileFormat.values())
      {
         if (Files.exists(
               createExportFile(exportId, format, context, jobStatus)))
         {
            formatToExport = format;
         }
      }
      return formatToExport;
   }

   private TypedFile loadFile(UUID exportId, FileFormat format, Context context)
   {
      try (FileInputStream fis = new FileInputStream(
            createExportFile(exportId, format, context, JobStatus.FINISHED)
                  .toFile());)
      {
         return new TypedFile(exportId.toString(), format,
               IOUtils.toByteArray(fis));
      } catch (IOException e)
      {
         throw new WerbeoException(
               "Failure loading export job with id" + exportId);
      }
   }

   public Slice<Export, OccurrenceExportSortField> findExports(Context context,
         OffsetRequest<OccurrenceExportSortField> offsetRequest)
   {
      try (Stream<Path> walk = Files.walk(userDir(context)))
      {

         Stream<Export> stream = walk.filter(path -> Files.isRegularFile(path))
               .flatMap(path ->
               {
                  LOGGER.debug("Checking path {}", path);
                  String fileName = path.getName(path.getNameCount() - 1)
                        .toString();
                  LocalDateTime creation = getCreationTime(path);
                  LOGGER.debug("File name {}", fileName);
                  UUID uuid = UUID.fromString(fileName.split("\\.")[0]);
                  JobStatus status = JobStatus.valueOf(
                        path.getName(path.getNameCount() - 2).toString());
                  return Stream.of(new Export(uuid, status,
                        fileName.substring(fileName.length() - 3), creation));
               }).sorted((a, b) ->
               {
                  if (offsetRequest.getSortField()
                        .equals(OccurrenceExportSortField.DATE))
                  {
                     if (offsetRequest.getSortOrder() == SortOrder.ASC)
                     {
                        return a.getCreationDate()
                              .compareTo(b.getCreationDate());
                     } else
                     {
                        return b.getCreationDate()
                              .compareTo(a.getCreationDate());
                     }
                  } else
                  {
                     if (offsetRequest.getSortOrder() == SortOrder.ASC)
                     {
                        return a.getId().compareTo(b.getId());
                     } else
                     {
                        return b.getId().compareTo(a.getId());
                     }
                  }
               }).skip(offsetRequest.getOffset());
         ;

         if (offsetRequest.getCount() != 0)
         {
            stream = stream.limit(offsetRequest.getCount());
         }

         Slice<Export, OccurrenceExportSortField> response = new SliceImpl<>(
               stream.collect(Collectors.toList()), offsetRequest);
         walk.close();
         return response;
      } catch (NoSuchFileException e)
      {
         return new SliceImpl<>(Collections.emptyList(), offsetRequest);
      } catch (IOException e)
      {
         LOGGER.error("Failure getting exports for {}", context, e);
         throw new WerbeoException("Failure getting exports");
      } finally
      {

      }
   }

   private LocalDateTime getCreationTime(Path path)
   {
      LocalDateTime creation = null;
      try
      {
         long millis = Files.readAttributes(path, BasicFileAttributes.class).creationTime().toMillis();
         creation = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalDateTime();
      }
      catch(Exception e)
      {
         LOGGER.error("Error determining Creation-Time");
      }
      return creation;
   }

   public long coutExports(Context context)
   {
      try (Stream<Path> files = Files.walk(userDir(context))
            .filter(path -> Files.isRegularFile(path)))
      {
         long count = files.count();
         files.close();
         return count;
      } catch (NoSuchFileException e)
      {
         return 0;
      } catch (IOException e)
      {
         LOGGER.error("Failure counting exports for context {}", context, e);
         throw new WerbeoException("Failure counting exports");
      }
   }

   private Job getJob(UUID exportId, Context context)
   {
      Job job = new Job();
      FileFormat fileFormat = null;
      for (JobStatus jobStatus : JobStatus.values())
      {
         fileFormat = getFileFormat(exportId, context, jobStatus);
         if (fileFormat != null)
         {
            job.setFileFormat(fileFormat);
            job.setJobStatus(jobStatus);
            break;
         }
      }
      return job;
   }

   @PostConstruct
   public void init()
   {
      try (Stream<Path> walk = Files.walk(Paths.get(exportPath, "exports")))
      {
         walk
               .filter(path -> Files.isRegularFile(path))
               .filter(path -> path.toString().contains("RUNNING"))
               .forEach(path -> {
                  try
                  {
                     Files.delete(path);
                  } catch (IOException e)
                  {
                     LOGGER.error(
                           "Failure removing lost RUNNING file at startup "
                                 + path.getFileName(), e);
                  }
               });
      } catch (IOException e)
      {
         LOGGER.error(
               "Failure walking export files at startup to remove lost RUNNING files",
               e);
      }
   }
}
