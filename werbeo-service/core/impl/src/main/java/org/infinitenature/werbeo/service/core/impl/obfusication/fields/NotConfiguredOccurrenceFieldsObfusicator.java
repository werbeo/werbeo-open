package org.infinitenature.werbeo.service.core.impl.obfusication.fields;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.meta.FieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.error.InternalException;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.api.validation.ConfigurableOccurrenceField;
import org.infinitenature.werbeo.service.core.impl.obfusication.EntityObfuscator;

public class NotConfiguredOccurrenceFieldsObfusicator
      implements EntityObfuscator<Occurrence>
{
   private final FieldConfigQueries fieldConfigQueries;

   public NotConfiguredOccurrenceFieldsObfusicator(
         FieldConfigQueries fieldConfigQueries)
   {
      super();
      this.fieldConfigQueries = fieldConfigQueries;
   }

   @Override
   public Occurrence obfuscate(Occurrence entity, Context context)
   {
      Set<OccurrenceFieldConfig> configuredFields = fieldConfigQueries
            .getOccurrenceConfig(context.getPortal()).getConfiguredFields();
      return removeNotConfiguredFieldValues(entity, configuredFields);
   }

   private Occurrence removeNotConfiguredFieldValues(Occurrence entity,
         Set<OccurrenceFieldConfig> configuredFields)
   {
      for (Field field : Occurrence.class.getDeclaredFields())
      {
         if (field.isAnnotationPresent(ConfigurableOccurrenceField.class))
         {
            ConfigurableOccurrenceField annotation = field
                  .getAnnotation(ConfigurableOccurrenceField.class);

            Optional<FieldConfig<OccurrenceField>> fieldConfig = configuredFields
                  .stream()
                  .filter(f -> f.getField().equals(annotation.value()))
                  .map(occFC -> (FieldConfig<OccurrenceField>) occFC)
                  .findFirst();
            if (!fieldConfig.isPresent())
            {
               try
               {
                  field.setAccessible(true);
                  field.set(entity, null);
               } catch (IllegalArgumentException | IllegalAccessException e)
               {
                  String message = "Failure removing value for field " + field;
                  throw new InternalException(message, e);
               }
            }
         }
      }
      return entity;
   }
}
