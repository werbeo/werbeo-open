package org.infinitenature.werbeo.service.core.impl.obfusication.position;

import java.util.List;

import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.enums.Permission;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.nopeople.ObfuscationUtils;

public abstract class AbstractPositionObfuscator
{

   protected final PositionFactory positionFactory;

   public AbstractPositionObfuscator(PositionFactory positionFactory)
   {
      super();
      this.positionFactory = positionFactory;
   }

   protected <T extends SampleBase> T doObfusicate(T entity, Context context)
   {
      List<Permission> permissions = ObfuscationUtils
            .getObfuscationPermissions(entity.getSurvey(), context);
      
      if (entity.getAllowedOperations().contains(Operation.UPDATE)
            || permissions.contains(Permission.LOCATION_POINT))
      {
         // Admin, data-owner or configured permission, do not obfuscate
         return entity;
      }
      else
      {
         int quadrant = ObfuscationUtils.getPermittedQuadrant(permissions);
         int mtbEndIndex = quadrant == 0 ? 4 : (5 + quadrant);
         mtbEndIndex = Math.min(mtbEndIndex,
               entity.getLocality().getPosition().getMtb().getMtb().length());
   
         Locality locality = entity.getLocality();
         locality.setLocality(null);
         locality.setLocationComment(null);
         locality.setPosition(positionFactory.createFromMTB(locality.getPosition()
               .getMtb().getMtb().substring(0, mtbEndIndex)));
         entity.setObfuscated(true);
         return entity;
      }
   }

   boolean isObfuscationNeeded(Context context, Availability availability)
   {
      return !context.isUserLoggedIn()
            && availability != Availability.FREE;
   }

}