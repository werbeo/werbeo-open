package org.infinitenature.werbeo.service.core.impl.export;

import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OccurrenceSplitarator implements Spliterator<Occurrence>
{
   private static final int CHARACTERISTICS = ORDERED | SIZED | SUBSIZED
         | NONNULL | DISTINCT | IMMUTABLE;

   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceSplitarator.class);

   private final Context context;
   private final OccurrenceFilter filter;
   private final OccurrenceQueryPort queries;

   private List<Occurrence> occurrencesSlice = new ArrayList<>();
   private final long count;
   private long loaded = 0;

   public OccurrenceSplitarator(OccurrenceQueryPort queries,
         OccurrenceFilter filter, Context context)
   {
      super();
      this.queries = queries;
      this.filter = filter;
      this.context = context;

      this.count = queries.count(filter, context);
   }

   @Override
   public int characteristics()
   {
      LOGGER.debug("characteristics()");
      return CHARACTERISTICS;
   }

   @Override
   public long estimateSize()
   {
      LOGGER.debug("estimateSize() = {}", count);
      return count;
   }

   @Override
   public boolean tryAdvance(Consumer<? super Occurrence> action)
   {
      LOGGER.debug("tryAdvance(action)");
      ensureSliceIsNotEmpty();
      if (occurrencesSlice.isEmpty())
      {
         return false;
      } else
      {
         Occurrence occurrence = occurrencesSlice.get(0);
         occurrencesSlice.remove(0);
         action.accept(occurrence);
         return true;
      }
   }

   private void ensureSliceIsNotEmpty()
   {
      LOGGER.debug("ensureSliceIsNotEmpty()");
      if (occurrencesSlice.isEmpty() && loaded < count)
      {
         loadNewSlice();
      }
   }

   private void loadNewSlice()
   {
      occurrencesSlice = new ArrayList<>(queries
            .find(filter, context, new OffsetRequestImpl<>(
                  (int) loaded, 20, SortOrder.ASC, OccurrenceSortField.ID))
            .getContent());
      loaded += occurrencesSlice.size();
   }

   @Override
   public Spliterator<Occurrence> trySplit()
   {
      LOGGER.debug("trySplit()");
      ensureSliceIsNotEmpty();
      if (occurrencesSlice.isEmpty())
      {
         return null;
      } else
      {
         Spliterator<Occurrence> sliceSpliterator = Spliterators.spliterator(
               occurrencesSlice.iterator(), occurrencesSlice.size(),
               CHARACTERISTICS);
         occurrencesSlice = new ArrayList<>();
         return sliceSpliterator;
      }
   }
}
