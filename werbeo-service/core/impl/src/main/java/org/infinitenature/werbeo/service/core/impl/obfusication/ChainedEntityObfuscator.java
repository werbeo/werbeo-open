package org.infinitenature.werbeo.service.core.impl.obfusication;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.support.Context;

public class ChainedEntityObfuscator<T extends BaseType<?>>
      implements EntityObfuscator<T>
{
   private final List<EntityObfuscator<T>> obfusicators;

   @SafeVarargs
   public ChainedEntityObfuscator(EntityObfuscator<T>... entityObfusicators)
   {
      this(Arrays.asList(entityObfusicators));
   }

   public ChainedEntityObfuscator(List<EntityObfuscator<T>> obfusicators)
   {
      super();
      this.obfusicators = Collections.unmodifiableList(obfusicators);
   }

   @Override
   public T obfuscate(T entity, Context context)
   {
      T obfusicatedEntity = entity;

      for (EntityObfuscator<T> obfusicator : obfusicators)
      {
         obfusicatedEntity = obfusicator.obfuscate(obfusicatedEntity, context);
      }
      return obfusicatedEntity;
   }

   public List<EntityObfuscator<T>> getObfuscators()
   {
      return obfusicators;
   }
}
