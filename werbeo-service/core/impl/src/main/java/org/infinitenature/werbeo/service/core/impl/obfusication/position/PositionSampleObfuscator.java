package org.infinitenature.werbeo.service.core.impl.obfusication.position;

import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.EntityObfuscator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PositionSampleObfuscator extends AbstractPositionObfuscator implements EntityObfuscator<Sample>
{
   @Autowired
   public PositionSampleObfuscator(PositionFactory positionFactory)
   {
      super(positionFactory);
   }

   @Override
   public Sample obfuscate(Sample entity, Context context)
   {
      return doObfusicate(entity, context);
   }

}
