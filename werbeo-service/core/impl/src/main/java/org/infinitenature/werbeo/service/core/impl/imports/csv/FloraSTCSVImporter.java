package org.infinitenature.werbeo.service.core.impl.imports.csv;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Herbarium;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatusFukarek;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FloraSTCSVImporter extends AbstractCSVImporter implements CSVImporter
{
   
   List<Integer> supportedPortalIds = Arrays.asList(new Integer[] {11});
   
   private static final Logger LOGGER = LoggerFactory
         .getLogger(FloraSTCSVImporter.class);
   
   @Autowired
   private PositionFactory positionFactory;

   
   protected SampleWithComment map2Sample(String[] nextLine, Context context)
   {
      Sample sample = new Sample();
      String comment = null;
      
      String uuid = nextLine[0];
      String taxonName = nextLine[1];
      String authority = nextLine[2];
      String status = nextLine[3];
      String quadrant = nextLine[4];
      String timeSlot = nextLine[5];
      
      TaxonBase taxon = getTaxon(taxonName, authority, context);
      
      Occurrence occurrence = new Occurrence();
      occurrence.setTaxon(taxon);
      occurrence.setSettlementStatusFukarek(map(status));
      sample.getOccurrences().add(occurrence);
      
      sample.setId(getOrCreate(uuid));
      
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setId(UUID.randomUUID());
      
      //Finder/Determiner
      sample.setLocality(mapLocality(quadrant));
      
      
      processTimeSlot(timeSlot, sample);
      sample.setSurvey(getOrCreateSurvey("Rasterdaten_bis_2012", context));
      
      return new SampleWithComment(sample, comment);
   }


   private UUID getOrCreate(String uuid)
   {
      try
      {
         return UUID.fromString(uuid);     
      }
      catch(Exception e)
      {
         return UUID.randomUUID();
      }
   }


   private SettlementStatusFukarek map(String status)
   {
      if(StringUtils.equals("x", status))
      {
         return SettlementStatusFukarek.INDIGEN;
      }
      else if(StringUtils.equals("E", status))
      {
         return SettlementStatusFukarek.EINGEBUERGERT;
      }
      else if(StringUtils.equals("S", status))
      {
         return SettlementStatusFukarek.UNBESTAENDIG;
      }
      else if(StringUtils.equals("K", status))
      {
         return SettlementStatusFukarek.ANGESALBT_KULTIVIERT;
      }
      else return SettlementStatusFukarek.UNBEKANNT;
   }


   private TaxonBase getTaxon(String taxonName, String authority, Context context)
   {
      String taxonAddendum = "";
      if(authority != null && authority.contains("s. str."))
      {
         taxonAddendum = " s. str.";
      }
      else if(authority != null && authority.contains("s. sl."))
      {
         taxonAddendum = " s. sl.";
      }
      
      // first try to find taxon with addendum   (i.E. s. str.  OR  s. sl.)
      TaxonBase taxon = processTaxon(context, taxonName + taxonAddendum,
            null);
      
      // not found? try without addendum
      if(taxon == null)
      {
         taxon = processTaxon(context, taxonName,
               null);
      }
      
      return taxon;
   }

   
   // 1=bis 1949, 2=1950-1991, 3=1992-2009 und 4=2010-2012.

   private void processTimeSlot(String timeslot, Sample sample)
   {
      if("1".equals(timeslot))
      {
         int starYear = 1600;
         int endYear = 1949;
         sample.setDate(VagueDateFactory.create("" + starYear + "-01-01",
               "" + endYear + "-12-31", "DD"));
      }
      else if("2".equals(timeslot))
      {
         int starYear = 1950;
         int endYear = 1991;
         sample.setDate(VagueDateFactory.create("" + starYear + "-01-01",
               "" + endYear + "-12-31", "DD"));
      }
      else if("3".equals(timeslot))
      {
         int starYear = 1992;
         int endYear = 2009;
         sample.setDate(VagueDateFactory.create("" + starYear + "-01-01",
               "" + endYear + "-12-31", "DD"));
      }
      else if("4".equals(timeslot))
      {
         int starYear = 2010;
         int endYear = 2012;
         sample.setDate(VagueDateFactory.create("" + starYear + "-01-01",
               "" + endYear + "-12-31", "DD"));
      }
   }

   private void processHerbar(String herbariumCode, String belegnr,
         Occurrence occurrence)
   {
      if(StringUtils.isNotBlank(herbariumCode) && StringUtils.isNotBlank(belegnr))
      {
         Herbarium herbarium = new Herbarium(herbariumCode, belegnr); 
         occurrence.setHerbarium(herbarium);
      }
   }




   private UUID getOrCreateID(String uuidString)
   {
      if(StringUtils.isNotBlank(uuidString))
      {
         try
         {
            return UUID
                  .fromString(uuidString.replace("{", "").replace("}", ""));
         }
         catch(Exception e)
         {
            LOGGER.error("Error parsing UUID");
         }
      }
         
      return UUID.randomUUID();
   }

   /**
    * Currently only supports GK Zone 4  (EPSG: 31468)
    * @param easting
    * @param northing
    * @param mtb
    * @param mtbq
    * @return
    */
   private Locality mapLocality(String mtbq)
   {
      Locality locality = new Locality();
      locality.setPrecision(3642);
      locality.setPosition(positionFactory.createFromMTB(
               mtbq.replace("-", "/")));
      return locality;
   }



   @Override
   public boolean handlesPortal(int portalId)
   {
      return supportedPortalIds.contains(portalId);
   }

}
