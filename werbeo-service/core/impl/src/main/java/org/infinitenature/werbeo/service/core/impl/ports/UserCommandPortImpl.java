package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enums.Role;
import org.infinitenature.werbeo.service.core.api.error.NotEnoughtRightsException;
import org.infinitenature.werbeo.service.core.api.infra.commands.RolesCommands;
import org.infinitenature.werbeo.service.core.api.ports.UserCommandPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserCommandPortImpl implements UserCommandPort
{

   @Autowired
   private RolesCommands rolesCommands;

   @Override
   public void addRole(Context context, String email, Role role)
   {
      checkAdminState(context);
      rolesCommands.addRole(new User(email), RoleNameFactory
            .getKeycloakRoleName(context.getPortal().getId(),
                  role.getRoleName()));
   }

   @Override
   public void removeRole(Context context, String email, Role role)
   {
      checkAdminState(context);
      rolesCommands.removeRole(new User(email), RoleNameFactory
            .getKeycloakRoleName(context.getPortal().getId(),
                  role.getRoleName()));
   }


   private void checkAdminState(Context context)
   {
      if(!context.isAdmin())
      {
         throw new NotEnoughtRightsException("User Change only for admins");
      }
   }

}
