package org.infinitenature.werbeo.service.core.access.checks;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.infra.query.SurveyQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SampleAccessRightCheck extends SimpleAccessRightCheck<SampleBase>
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(SampleAccessRightCheck.class);

   private SurveyQueries surveyQueries;

   public SampleAccessRightCheck(SurveyQueries surveyQueries)
   {
      super(SampleBase.class);
      this.surveyQueries = surveyQueries;
   }

   @Override
   protected boolean isAllowedToFind(Class<? extends SampleBase> type,
         Context context)
   {
      return true;
   }

   @Override
   public Set<Operation> getAllowedOperations(SampleBase entity,
         Context context)
   {
      Set<Operation> allowedOperations = new HashSet<>(
            super.getAllowedOperations(entity, context));

      if (!isSureyAllowDataEntry(entity, context))
      {
         allowedOperations.remove(Operation.DELETE);
      }
      return Collections.unmodifiableSet(allowedOperations);
   }

   @Override
   protected boolean isAllowedToCreate(SampleBase entity, Context context)
   {
      return isSureyAllowDataEntry(entity, context);
   }

   boolean isSureyAllowDataEntry(SampleBase entity, Context context)
   {
      if (entity.getSurvey() != null && entity.getSurvey().getId() != null)
      {
         Survey survey = surveyQueries.load(entity.getSurvey().getId(),
               context);
         if (survey != null)
         {
            LOGGER.warn(
                  "Survey {} with id {} does not allow data entry, but user tries to",
                  survey.getName(), survey.getId());
            return survey.isAllowDataEntry();
         }
      }
      return true;
   }

}
