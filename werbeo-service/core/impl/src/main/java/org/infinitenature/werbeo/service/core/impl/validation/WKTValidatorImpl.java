package org.infinitenature.werbeo.service.core.impl.validation;

import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infinitenature.werbeo.service.core.api.validation.WKTValidator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class WKTValidatorImpl implements WKTValidator
{
   private GeometryHelper geometryHelper = new GeometryHelper();

   @Override
   public boolean isValid(String value)
   {
      if (value == null)
      {
         return true;
      }

      try
      {
         geometryHelper.parseToJts(value, 4326);
         return true;
      } catch (Exception e)
      {
         return false;
      }

   }

}
