package org.infinitenature.werbeo.service.core.impl.imports.concurent;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class BlockinigThreadPoolExecutorService extends ThreadPoolExecutor
{
   public BlockinigThreadPoolExecutorService(int maximumPoolSize,
         String poolName)
   {
      this(maximumPoolSize, maximumPoolSize, 10, TimeUnit.MINUTES,
            new ArrayBlockingQueue<>(maximumPoolSize), poolName);
   }

   public BlockinigThreadPoolExecutorService(int corePoolSize,
         int maximumPoolSize, long keepAliveTime, TimeUnit unit,
         BlockingQueue<Runnable> workQueue, String poolName)
   {
      super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
            new ThreadFactoryWithNamePrefix(poolName));
      this.semaphore = new Semaphore(workQueue.remainingCapacity());
   }

   private Semaphore semaphore;

   @Override
   public void execute(Runnable task)
   {
      boolean acquired = false;
      do
      {
         try
         {
            semaphore.acquire();
            acquired = true;
         } catch (InterruptedException e)
         {
            // wait forever!
         }
      } while (!acquired);

      try
      {
         super.execute(task);
      } catch (RuntimeException e)
      {
         // specifically, handle RejectedExecutionException
         semaphore.release();
         throw e;
      } catch (Error e)
      {
         semaphore.release();
         throw e;
      }
   }

   @Override
   protected void afterExecute(Runnable r, Throwable t)
   {
      semaphore.release();
   }
}
