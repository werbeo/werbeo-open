package org.infinitenature.werbeo.service.core.impl.imports.csv;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.CSVImport;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceImportSortField;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceExportCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.export.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImportFacade
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ImportFacade.class);
   private final String importPath;

   public ImportFacade(@Autowired OccurrenceQueryPort occurrenceQueries,
         @Autowired InstanceConfig instanceConfig,
         @Autowired(required = false) List<OccurrenceExportCommands> occurrenceExports,
         @Autowired FieldConfigQueries fieldConfigQueries)
   {
      super();
      this.importPath = instanceConfig.getDir();
   }

  
   public void deleteImport(UUID importId, Context context)
   {
      try
      {
         Job job = getJob(importId, context);
         if (job.getFileFormat() == null)
         {
            throw new EntityNotFoundException(
                  "Import job with id " + importId + " not found");
         }
         FileUtils.deleteDirectory(importDir(context, job.getJobStatus())
               .resolve(importId.toString()).toFile());
      } catch (IOException e)
      {
         LOGGER.error("Failure deleting import with id {}.", importId, e);
         throw new WerbeoException("Failure deleting import with id " + importId);
      }
   }

   private Path createImportFile(UUID importId, FileFormat format,
         Context context, JobStatus status)
   {
      return importDir(context, status)
            .resolve(importId.toString()).resolve( "import" + "." + format.getSuffix());
   }
   
   private Path createFile(UUID importId, String fileName, FileFormat format,
         Context context, JobStatus status)
   {
      return importDir(context, status)
            .resolve(importId.toString()).resolve( fileName + "." + format.getSuffix());
   }

   private Path importDir(Context context, JobStatus status)
   {
      return userDir(context).resolve(status.toString());
   }

   private Path userDir(Context context)
   {
      return Paths.get(importPath, "imports",
            String.valueOf(context.getPortal().getId()),
            String.valueOf(context.getUser().getLogin()));
   }

   public CSVImport getStatus(UUID exportId, Context context)
   {
      for (JobStatus status : JobStatus.values())
      {
         for (FileFormat format : FileFormat.values())
         {
            if (Files
                  .exists(createImportFile(exportId, format, context, status)))
            {
               LocalDateTime creation = getCreationTime(createImportFile(exportId, format, context, status));
               return new CSVImport(exportId, status, format.getSuffix(), creation);
            }
         }
      }
      throw new EntityNotFoundException(
            "Export job with id " + exportId + " not there");
   }

   public TypedFile getFile(UUID importId, Context context)
   {
      FileFormat formatToExport = getFileFormat(importId, context, JobStatus.FINISHED);
      if (formatToExport == null)
      {
         throw new EntityNotFoundException(
               "Import job FINISHED with id " + importId + " is missing");
      }
      return loadFile(importId, formatToExport, context);
   }

   private FileFormat getFileFormat(UUID exportId, Context context, JobStatus jobStatus)
   {
      FileFormat formatToExport = null;
      for (FileFormat format : FileFormat.values())
      {
         if (Files.exists(
               createImportFile(exportId, format, context, jobStatus)))
         {
            formatToExport = format;
         }
      }
      return formatToExport;
   }

   private TypedFile loadFile(UUID exportId, FileFormat format, Context context)
   {
      try (FileInputStream fis = new FileInputStream(
            createImportFile(exportId, format, context, JobStatus.FINISHED)
                  .toFile());)
      {
         return new TypedFile(exportId.toString(), format,
               IOUtils.toByteArray(fis));
      } catch (IOException e)
      {
         throw new WerbeoException(
               "Failure loading file with format " + format + ",  for job with id" + exportId);
      }
   }

   private TypedFile loadFile(UUID jobId, String fileName, FileFormat format, Context context,
         JobStatus jobStatus)
   {
      try (FileInputStream fis = new FileInputStream(
            createFile(jobId, fileName, format, context, jobStatus).toFile());)
      {
         return new TypedFile(jobId.toString(), format,
               IOUtils.toByteArray(fis));
      } catch (IOException e)
      {
         throw new WerbeoException(
               "Failure loading File " + fileName + ",   with jobid" + jobId);
      }
   }

   
   public Slice<CSVImport, OccurrenceImportSortField> findImports(Context context,
         OffsetRequest<OccurrenceImportSortField> offsetRequest)
   {
      try (Stream<Path> walk = Files.walk(userDir(context)))
      {
         Stream<CSVImport> stream = walk
               .filter(path -> path.endsWith("import.csv")).flatMap(path ->
               {
                  LOGGER.debug("Checking path {}", path);
                  String fileName = path.getName(path.getNameCount() - 2)
                        .toString();
                  LocalDateTime creation = getCreationTime(path);
                  LOGGER.debug("File name {}", fileName);
                  UUID uuid = UUID.fromString(fileName.split("\\.")[0]);
                  JobStatus status = JobStatus.valueOf(
                        path.getName(path.getNameCount() - 3).toString());
                  return Stream.of(new CSVImport(uuid, status,
                        fileName.substring(fileName.length() - 3), creation));
               }).sorted((a, b) ->
               {
                 if(offsetRequest.getSortField().equals(OccurrenceImportSortField.DATE))
                 {
                    if (offsetRequest.getSortOrder() == SortOrder.ASC)
                    {
                       return a.getCreationDate().compareTo(b.getCreationDate());
                    } else
                    {
                       return b.getCreationDate().compareTo(a.getCreationDate());
                    }
                 }
                 else
                 {
                  if (offsetRequest.getSortOrder() == SortOrder.ASC)
                  {
                     return a.getId().compareTo(b.getId());
                  } else
                  {
                     return b.getId().compareTo(a.getId());
                  }
                 }
               }).skip(offsetRequest.getOffset());
         if (offsetRequest.getCount() != 0)
         {
            stream = stream.limit(offsetRequest.getCount());
         }
         
         Slice<CSVImport, OccurrenceImportSortField>  slice = new SliceImpl<CSVImport,OccurrenceImportSortField>(stream.collect(Collectors.toList()),
               offsetRequest);
         walk.close();
         return slice;
      } catch (NoSuchFileException e)
      {
         return new SliceImpl<CSVImport,OccurrenceImportSortField>(Collections.emptyList(), offsetRequest);
      } catch (IOException e)
      {
         LOGGER.error("Failure getting imports for {}", context, e);
         throw new WerbeoException("Failure getting imports");
      }
   }

   private LocalDateTime getCreationTime(Path path)
   {
      LocalDateTime creation = null;
      try
      {
         long millis = Files.readAttributes(path, BasicFileAttributes.class).creationTime().toMillis();
         creation = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault()).toLocalDateTime();
      }
      catch(Exception e)
      {
         LOGGER.error("Error determining Creation-Time");
      }
      return creation;
   }

   public long countImports(Context context)
   {
      try (Stream<Path> files = Files.walk(userDir(context))
            .filter(path -> path.endsWith("import.csv")))
      {
         return files.count();
      } catch (NoSuchFileException e)
      {
         return 0;
      } catch (IOException e)
      {
         LOGGER.error("Failure counting imports for context {}", context, e);
         throw new WerbeoException("Failure counting imports");
      }
   }

   private Job getJob(UUID importId, Context context)
   {
      Job job = new Job();
      FileFormat fileFormat = null;
      for (JobStatus jobStatus : JobStatus.values())
      {
         fileFormat = getFileFormat(importId, context, jobStatus);
         if (fileFormat != null)
         {
            job.setFileFormat(fileFormat);
            job.setJobStatus(jobStatus);
            break;
         }
      }
      return job;
   }


   public TypedFile getErrorFile(UUID importId, Context context)
   {
      return loadFile(importId, "csv-error", FileFormat.LOG, context,
            getStatus(importId, context).getStatus());
   }

  
}
