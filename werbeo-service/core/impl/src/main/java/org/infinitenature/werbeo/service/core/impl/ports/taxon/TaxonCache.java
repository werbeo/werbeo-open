package org.infinitenature.werbeo.service.core.impl.ports.taxon;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Group;
import org.infinitenature.werbeo.service.core.api.enity.Language;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class TaxonCache
{
   private static final Set<Language> latinOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.LAT)));
   private static final Set<Language> germanOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.DEU)));

   private static final Logger LOGGER = LoggerFactory
         .getLogger(TaxonCache.class);

   @Autowired
   private InstanceConfig instanceConfig;

   @Autowired
   private TaxonQueries taxonQueries;

   @Autowired
   private PortalQueries portalQueries;

   private Map<Integer, List<TaxonBase>> allTaxaPerPortal = new HashMap<>();
   private Map<Integer, List<TaxonBase>> usedTaxaPerPortal = new HashMap<>();

   private Map<Integer, List<TaxonBase>> allTaxaPerPortalGerman = new HashMap<>();
   private Map<Integer, List<TaxonBase>> usedTaxaPerPortalGerman = new HashMap<>();

   @PostConstruct
   public void initCache()
   {
      if(!instanceConfig.isUseTaxonCache())
      {
         LOGGER.info("taxon-cache disabled!!!");
         return;
      }
      LOGGER.info("initializing of taxon-cache");

      OffsetRequest<TaxonSortField> offsetRequest = new OffsetRequest<TaxonSortField>()
      {

         @Override
         public SortOrder getSortOrder()
         {
            return SortOrder.ASC;
         }

         @Override
         public TaxonSortField getSortField()
         {
            return TaxonSortField.TAXON;
         }

         @Override
         public int getOffset()
         {
            return 0;
         }

         @Override
         public int getCount()
         {
            return Integer.MAX_VALUE;
         }
      };

      for(Portal portal : portalQueries.findAll())
      {
         LOGGER.info("initializing of taxon-cache for Portal: {}  ({})",
               portal.getId(), portal.getTitle());
         Context context = new Context(portal, null, new HashSet<Group>(),
               "taxon-cache");
         TaxonFilter filter = new TaxonFilter("", latinOnly, true, null, false,
               new HashSet<>(), null, false);
         List<TaxonBase> taxa = taxonQueries.findBase(filter, context, offsetRequest).getContent();
         allTaxaPerPortal.put(portal.getId(), taxa);

         filter = new TaxonFilter("", latinOnly, true, null,
               true,
               new HashSet<>(), null, false);
         taxa = taxonQueries.findBase(filter, context, offsetRequest).getContent();
         usedTaxaPerPortal.put(portal.getId(), taxa);

         // handle german translation for Heuschrecken??
         filter = new TaxonFilter("", germanOnly, true, null, false,
               new HashSet<>(), null, false);
         taxa = taxonQueries.findBase(filter, context, offsetRequest).getContent();
         allTaxaPerPortalGerman.put(portal.getId(), taxa);

         filter = new TaxonFilter("", germanOnly, true, null, true,
               new HashSet<>(), null, false);
         taxa = taxonQueries.findBase(filter, context, offsetRequest).getContent();
         usedTaxaPerPortalGerman.put(portal.getId(), taxa);
      }

      LOGGER.info("initializing of taxon-cache finished");
   }

   @Async
   public void refreshUsedTaxaIfNecessary(Integer portalId, List<Integer> taxonIds, Context context)
   {
      // 1st: check, whether saved taxa are already in cache
      for(TaxonBase taxon : usedTaxaPerPortal.get(portalId))
      {
         taxonIds.remove(taxon.getId());
      }

      if(taxonIds.isEmpty())
      {
         // all taxa could be found in cache, nothing to do here
         return;
      }

      // at least one taxon could not be found in usedCache, refresh it!
      TaxonFilter filter = new TaxonFilter("", latinOnly, true, null,
            true,
            new HashSet<>(), null, false);

      OffsetRequest<TaxonSortField> offsetRequest = createOffsetReuqest();
      usedTaxaPerPortal.put(portalId, taxonQueries.findBase(filter, context, offsetRequest).getContent());
   }


   private OffsetRequest<TaxonSortField> createOffsetReuqest()
   {
      OffsetRequest<TaxonSortField> offsetRequest = new OffsetRequest<TaxonSortField>()
      {

         @Override
         public SortOrder getSortOrder()
         {
            return SortOrder.ASC;
         }

         @Override
         public TaxonSortField getSortField()
         {
            return TaxonSortField.TAXON;
         }

         @Override
         public int getOffset()
         {
            return 0;
         }

         @Override
         public int getCount()
         {
            return Integer.MAX_VALUE;
         }
      };
      return offsetRequest;
   }

   public Slice<TaxonBase, TaxonSortField> find(TaxonFilter filter, Integer portalId,
         OffsetRequest<TaxonSortField> offsetRequest)
   {
      List<TaxonBase> foundTaxa = filterTaxa(filter, portalId);


      // Limit Slice
      int start = 0;
      int end = foundTaxa.size();
      if(foundTaxa.size() >= offsetRequest.getOffset())
      {
         start = offsetRequest.getOffset();
      }
      if(foundTaxa.size() >= offsetRequest.getOffset() + offsetRequest.getCount())
      {
         end = offsetRequest.getOffset() + offsetRequest.getCount();
      }

      return new SliceImpl<>(
            foundTaxa.subList(start, end), offsetRequest);
   }

   private List<TaxonBase> filterTaxa(TaxonFilter filter, Integer portalId)
   {
      // Filter
      List<TaxonBase> taxaToFilter = getTaxonList(portalId, filter);

      Stream<TaxonBase> taxaStream = taxaToFilter.stream();

      if(StringUtils.isNotBlank(filter.getNameContains())) {
         taxaStream = taxaStream.filter(filterNameContains(filter));
      }

      if (StringUtils.isNotBlank(filter.getTaxaGroup()))
      {
         taxaStream = taxaStream.filter(filterByGroup(filter));
      }

      // no species-taxa for occurrence-input
      if(filter.isOnlyTaxaAvailableForInput())
      {
         taxaStream = taxaStream.filter(TaxonBase::isAllowDataEntry);
      }

      if(!filter.getExternalKeys().isEmpty()) {
         taxaStream = taxaStream.filter(filterByExternalKey(filter));
      }

      if (!filter.isIncludeSynonyms())
      {
         taxaStream = taxaStream.filter(TaxonBase::isPreferred);
      }
      return taxaStream.collect(Collectors.toList());
   }

   private Predicate<TaxonBase> filterByExternalKey(TaxonFilter filter)
   {
      return taxa -> filter.getExternalKeys().contains(taxa.getExternalKey());
   }

   private Predicate<TaxonBase> filterByGroup(TaxonFilter filter)
   {
      return taxa -> taxa.getGroup().equals(filter.getTaxaGroup());
   }

   private List<TaxonBase> getTaxonList(Integer portalId, TaxonFilter filter)
   {
      if (filter.getLanguages() != null
            && filter.getLanguages().contains(Language.DEU))
      {
         return filter.isOnlyUsedTaxa() ? usedTaxaPerPortalGerman.get(portalId)
               : allTaxaPerPortalGerman.get(portalId);
      } else
      {
         return filter.isOnlyUsedTaxa() ? usedTaxaPerPortal.get(portalId)
               : allTaxaPerPortal.get(portalId);
      }
   }

   private Predicate<TaxonBase> filterNameContains(TaxonFilter filter)
   {
      return (TaxonBase t) -> matches(t.getName(), filter.getNameContains());
   }

   protected boolean matches(String name, String search)
   {
      if(StringUtils.isBlank(search))
      {
         return true;
      }
      String[] nameSplit = name.split(" ");
      String[] searchSplit = search.split(" ", -1);

      // more search keywords than taxon.name
      if(searchSplit.length > nameSplit.length)
      {
         return false;
      }

      int minLength = Math.min(nameSplit.length, searchSplit.length);
      boolean found = true;
      for(int i = 0; i < minLength; i++)
      {
         if (!nameSplit[i].toLowerCase()
               .startsWith(searchSplit[i].toLowerCase()))
         {
            found = false;
            break;
         }
      }

      return found;
   }

   public long count(TaxonFilter filter, Integer portalId)
   {
      // Filter
      List<TaxonBase> foundTaxa = filterTaxa(filter, portalId);
      return foundTaxa.size();
   }


}
