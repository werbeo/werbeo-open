package org.infinitenature.werbeo.service.core.impl.meta;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.TaxonConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.TaxonFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.TaxonField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.WerbeoField;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.togglz.core.manager.FeatureManager;

@Service
public class FieldConfigQuieresImpl implements FieldConfigQueries
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(FieldConfigQuieresImpl.class);

   private final FeatureManager featureManager;

   @Autowired
   public FieldConfigQuieresImpl(FeatureManager featureManager)
   {
      this.featureManager = featureManager;
   }

   @Override
   public TaxonConfig getTaxonConfig(Portal portal)
   {
      Set<TaxonFieldConfig> fields = globalMandatoryFields(TaxonField.class)
            .stream().map(sf -> new TaxonFieldConfig(sf, true))
            .collect(Collectors.toSet());
      fields.add(new TaxonFieldConfig(TaxonField.ID, false));
      fields.add(new TaxonFieldConfig(TaxonField.GROUP, false));
      if (portal.getTitle().equalsIgnoreCase("heuschrecken-bund"))
      {
         fields.add(new TaxonFieldConfig(TaxonField.RED_LIST_STATUS, false));
      }
      return new TaxonConfig(portal, fields);
   }

   @Override
   public SurveyConfig getSurveyConfig(Portal portal)
   {
      Set<SurveyFieldConfig> fields = globalMandatoryFields(SurveyField.class)
            .stream().map(sf -> new SurveyFieldConfig(sf, true))
            .collect(Collectors.toSet());
      fields.add(new SurveyFieldConfig(SurveyField.ID, false));
      return new SurveyConfig(portal, fields);
   }

   @Override
   public OccurrenceConfig getOccurrenceConfig(Portal portal)
   {
      Set<OccurrenceFieldConfig> fields = globalMandatoryFields(
            OccurrenceField.class).stream()
                  .map(of -> new OccurrenceFieldConfig(of, true))
                  .collect(Collectors.toSet());
      fields.add(
               new OccurrenceFieldConfig(OccurrenceField.REMARK, false));
      
      if (featureManager.isActive(WerBeoFeatures.VALIDATION_FIELD))
      {
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.VALIDATION, false));
      }
      if (isFloraMV(portal))
      {
         fields.add(new OccurrenceFieldConfig(OccurrenceField.AMOUNT, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.CITE_ID, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.CITE_COMMENT, false));
      }
      if (isFloristic(portal))
      {
         fields.add(new OccurrenceFieldConfig(OccurrenceField.MEDIA, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.OBSERVERS, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.AREA, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.BLOOMING_SPROUTS,
               false));

         if (featureManager.isActive(WerBeoFeatures.LEGACY_QUANTITY_SUPPORT))
         {
            fields.add(
                  new OccurrenceFieldConfig(OccurrenceField.QUANTITY, false));
         }
         fields.add(new OccurrenceFieldConfig(OccurrenceField.NUMERIC_AMOUNT,
               false));
         fields.add(new OccurrenceFieldConfig(
               OccurrenceField.NUMERIC_AMOUNT_ACCURACY, false));

         //  FUKAREK-Status for all Floras
         fields.add(new OccurrenceFieldConfig(
                  OccurrenceField.SETTLEMENT_STATUS_FUKAREK, false));

         fields.add(new OccurrenceFieldConfig(OccurrenceField.VITALITY, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.HERBARIUM, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.EXTERNAL_KEY, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.HABITAT, false));
      } else if (isLocust(portal))
      {
         fields.add(new OccurrenceFieldConfig(OccurrenceField.MEDIA, false));
         fields.add(new OccurrenceFieldConfig(
               OccurrenceField.DETERMINATION_COMMENT, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.NUMERIC_AMOUNT,
               false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.LIFE_STAGE, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.MAKROPTER, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.SEX, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.REPRODUCTION, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.EXTERNAL_KEY, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.HABITAT, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.TIME_OF_DAY, false));
      } else if (portal.getTitle().equals("dermaptera"))
      {
         fields.add(new OccurrenceFieldConfig(OccurrenceField.MEDIA, false));
         fields.add(new OccurrenceFieldConfig(
               OccurrenceField.DETERMINATION_COMMENT, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.NUMERIC_AMOUNT,
               false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.SEX, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.REPRODUCTION, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.EXTERNAL_KEY, false));
         fields.add(new OccurrenceFieldConfig(OccurrenceField.HABITAT, false));
         fields.add(
               new OccurrenceFieldConfig(OccurrenceField.TIME_OF_DAY, false));
      }
      return new OccurrenceConfig(portal, fields);

   }

   boolean isFloraMV(Portal portal)
   {
      return StringUtils.equalsIgnoreCase(portal.getTitle(), "flora-mv");
   }

   /**
    * Is the portal keeping grass hopper (locust) data
    *
    * @param portal
    * @return
    */
   private boolean isLocust(Portal portal)
   {
      return portal.getTitle().equals("heuschrecken-bund")
            || portal.getTitle().equals("heuschrecken-rlp")
            || portal.getTitle().equals("heuschrecken-nrw");
   }

   private boolean isFloristic(Portal portal)
   {
      return isFloraMV(portal)
            || portal.getTitle().equalsIgnoreCase("Demonstration Website") // that
                                                                           // is
                                                                           // portalId=1
                                                                           // and
                                                                           // is
                                                                           // used
                                                                           // in
                                                                           // some
                                                                           // tests
            || portal.getTitle().equalsIgnoreCase("floristische datenbanken") // that
                                                                              // means
                                                                              // flora-bb
                                                                              // on
                                                                              // test
                                                                              // and
                                                                              // live
            || portal.getTitle().equalsIgnoreCase("flora-bb") // that is
                                                              // flora-bb
                                                              // locally
            || portal.getTitle().equalsIgnoreCase("flora-sn")

            || portal.getTitle().equalsIgnoreCase("flora-st");

   }

   @Override
   public SampleConfig getSampleConfig(Portal portal)
   {
      Set<SampleFieldConfig> fields = globalMandatoryFields(SampleField.class)
            .stream().map(sf -> new SampleFieldConfig(sf, true))
            .collect(Collectors.toSet());
      List<SampleMethod> availableSampleMethods = new ArrayList<>();

      if (isFloristic(portal))
      {
         fields.add(new SampleFieldConfig(SampleField.LOCATION_MTB, false));
         fields.add(new SampleFieldConfig(SampleField.LOCATION_COMMENT, false));
         fields.add(new SampleFieldConfig(SampleField.LOCALITY, false));

      } else if (isLocust(portal))
      {
         fields.add(new SampleFieldConfig(SampleField.LOCATION_MTB, false));
         fields.add(new SampleFieldConfig(SampleField.SAMPLE_METHOD, false));
         fields.add(new SampleFieldConfig(SampleField.LOCALITY, false));
         availableSampleMethods.addAll(Arrays.asList(SampleMethod.ESTIMINATION,
               SampleMethod.TRANSECT_COUNT, SampleMethod.SWEEP_NETTING,
               SampleMethod.BOX_QUADRAT, SampleMethod.SUCTION_SAMPLER,
               SampleMethod.OTHER_METHOD, SampleMethod.FIELD_OBSERVATION));
      } else if (portal.getTitle().equals("dermaptera"))
      {
         fields.add(new SampleFieldConfig(SampleField.LOCALITY, false));
      }
      return new SampleConfig(portal, fields, availableSampleMethods);
   }

   private <T extends Enum<T> & WerbeoField<T>> Set<T> globalMandatoryFields(
         Class<T> type)
   {
      try
      {
         Method method = type.getMethod("values");
         List<T> list = Arrays.asList((T[]) method.invoke(null));
         Set<T> result = new HashSet<>();
         for (T t : list)
         {
            if (t.isGlobalMandantory())
            {
               LOGGER.debug("{} is global mandantory", t);
               result.add(t);
            }
         }
         return result;
      } catch (NoSuchMethodException | SecurityException
            | IllegalAccessException | IllegalArgumentException
            | InvocationTargetException e)
      {
         throw new RuntimeException(e);
      }

   }

}
