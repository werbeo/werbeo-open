package org.infinitenature.werbeo.service.core.impl.obfusication.nopeople;

import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enums.Permission;
import org.infinitenature.werbeo.service.core.api.support.Context;

public abstract class AbstractNoPersonObfuscator
{
   protected boolean isObfuscationNecessary(SampleBase sampleBase,
         Context context)
   {
      // NO Obfuscation for :  Admin, Data-Owner OR User with configured permission
      return !sampleBase.getAllowedOperations().contains(Operation.UPDATE)
            && !ObfuscationUtils.getObfuscationPermissions(sampleBase.getSurvey(), context)
                  .contains(Permission.PERSONS);
   }
}
