package org.infinitenature.werbeo.service.core.impl.validation;

import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.validation.MediaUploadValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class MediaUploadValidatorImpl implements MediaUploadValidator
{
   @Autowired
   private PortalQueryPort portalQueryPort;


   @Override
   public boolean sizeIsValid(int length)
   {
      int maxUploadSizeMB = portalQueryPort
            .loadConfig(ValidationContextHolder.get().getPortal().getId())
            .getMaxUploadSize();
      return maxUploadSizeMB * 1024 * 1024 >= length;
   }
}
