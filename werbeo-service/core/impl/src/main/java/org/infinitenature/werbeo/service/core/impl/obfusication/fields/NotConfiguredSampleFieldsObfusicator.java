package org.infinitenature.werbeo.service.core.impl.obfusication.fields;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.meta.FieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.error.InternalException;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.api.validation.ConfigurableSampleField;
import org.infinitenature.werbeo.service.core.impl.obfusication.EntityObfuscator;

public class NotConfiguredSampleFieldsObfusicator
      implements EntityObfuscator<Sample>
{
   private final FieldConfigQueries fieldConfigQueries;

   public NotConfiguredSampleFieldsObfusicator(
         FieldConfigQueries fieldConfigQueries)
   {
      this.fieldConfigQueries = fieldConfigQueries;
   }

   @Override
   public Sample obfuscate(Sample entity, Context context)
   {
      Set<SampleFieldConfig> configuredFields = fieldConfigQueries
            .getSampleConfig(context.getPortal()).getConfiguredFields();
      removeNotConfiguredFieldValues(entity, configuredFields);

      NotConfiguredOccurrenceFieldsObfusicator occurrenceFieldsObfusicator = new NotConfiguredOccurrenceFieldsObfusicator(
            fieldConfigQueries);
      entity.getOccurrences().forEach(occurrence -> occurrenceFieldsObfusicator
            .obfuscate(occurrence, context));
      return entity;
   }

   private void removeNotConfiguredFieldValues(Sample entity,
         Set<SampleFieldConfig> configuredFields)
   {
      for (Field field : Sample.class.getDeclaredFields())
      {
         if (field.isAnnotationPresent(ConfigurableSampleField.class))
         {
            ConfigurableSampleField annotation = field
                  .getAnnotation(ConfigurableSampleField.class);

            Optional<FieldConfig<SampleField>> fieldConfig = configuredFields
                  .stream().filter(f -> f.getField().equals(annotation.value()))
                  .map(occFC -> (FieldConfig<SampleField>) occFC).findFirst();
            if (!fieldConfig.isPresent())
            {
               try
               {
                  field.setAccessible(true);
                  field.set(entity, null);
               } catch (IllegalArgumentException | IllegalAccessException e)
               {
                  String message = "Failure removing value for field " + field;
                  throw new InternalException(message, e);
               }
            }

         }
      }
   }

}
