package org.infinitenature.werbeo.service.core.impl.export;

import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;

public class Job
{
   private FileFormat fileFormat;
   private JobStatus jobStatus;

   public FileFormat getFileFormat()
   {
      return fileFormat;
   }

   public void setFileFormat(FileFormat fileFormat)
   {
      this.fileFormat = fileFormat;
   }

   public JobStatus getJobStatus()
   {
      return jobStatus;
   }

   public void setJobStatus(JobStatus jobStatus)
   {
      this.jobStatus = jobStatus;
   }
}
