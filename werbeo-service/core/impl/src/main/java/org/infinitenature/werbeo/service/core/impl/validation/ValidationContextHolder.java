package org.infinitenature.werbeo.service.core.impl.validation;

import org.infinitenature.werbeo.service.core.api.support.Context;

public class ValidationContextHolder
{
   private static final ThreadLocal<Context> contextHolder = new ThreadLocal<>();

   private ValidationContextHolder()
   {
      throw new IllegalAccessError("Utitily class");
   }

   public static final void setThreadLocalContext(Context context)
   {
      contextHolder.set(context);
   }

   public static final void removeThreadLocalContext()
   {
      contextHolder.remove();
   }

   public static final Context get()
   {
      return contextHolder.get();
   }

   public static final void set(Context context)
   {
      contextHolder.set(context);
   }
}
