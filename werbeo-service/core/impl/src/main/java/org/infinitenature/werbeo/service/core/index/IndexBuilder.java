package org.infinitenature.werbeo.service.core.index;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.infinitenature.werbeo.service.core.api.enity.support.StreamSlice;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceIndexCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.togglz.core.manager.FeatureManager;

@Service
public class IndexBuilder
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(IndexBuilder.class);
   private final PortalQueries portalQueries;
   private final OccurrenceQueries occurrenceQueries;
   private final FeatureManager featureManager;
   private final Path indexStateDir;
   private final OccurrenceIndexCommands occurrenceIndexCommands;
   private List<Integer> portalIdsWithoutIndex = Arrays.asList(1,3,4);
   public IndexBuilder(PortalQueries portalQueries,
         OccurrenceQueries occurrenceQueries, FeatureManager featureManager,
         @Autowired InstanceConfig instanceConfig,
         OccurrenceIndexCommands occurrenceIndexCommands)
   {
      this.portalQueries = portalQueries;
      this.occurrenceQueries = occurrenceQueries;
      this.featureManager = featureManager;
      this.indexStateDir = Paths.get(instanceConfig.getDir(), "index-state");
      this.occurrenceIndexCommands = occurrenceIndexCommands;
   }

   @PostConstruct
   public void setup() throws IOException
   {
      Files.createDirectories(indexStateDir);
   }

   @Scheduled(fixedDelay = 5000)
   public void updateIndex()
   {
      if (featureManager.isActive(WerBeoFeatures.BUILD_OCCURRENCE_INDEX))
      {
         for (Portal portal : portalQueries.findAll())
         {
            if(portalIdsWithoutIndex.contains(portal.getId()))
            {
               continue;
               
            }
            try
            {
               updateIndex(portal);
            } catch (Exception e)
            {
               LOGGER.error("Failure update index for portal: {} - {}",
                     portal.getId(), portal.getTitle(), e);
            }
         }
      } else
      {
         LOGGER.debug("Update index is not active");
      }
   }

   private void updateIndex(Portal portal) throws IOException
   {

      Integer portalId = portal.getId();
      ContinuationToken continuationToken = getContinationToken(portalId);

      StreamSlice<Occurrence, UUID> streamOccurrences = occurrenceQueries
            .streamOccurrencesForSinglePortal(new Context(portal,
                  User.INTERNAL_SUPER_USER,
                  Collections.emptySet(), "index-builder-" + UUID.randomUUID()),
                  continuationToken);
      ContinuationToken nextToken = streamOccurrences.getContinuationToken();
      if (LOGGER.isInfoEnabled() && (streamOccurrences.getEntities().size()
            + streamOccurrences.getDeletedIds().size()) > 0)
      {
         LOGGER.info("Update Index for portal {} - {} - {}", portalId, portal.getTitle(),
               continuationToken);
         LOGGER.info("Going to index {} and delete {} occurrences",
               streamOccurrences.getEntities().size(),
               streamOccurrences.getDeletedIds().size());
         LOGGER.info("Next token for portal {} is {}", portalId, nextToken);
      }
      occurrenceIndexCommands.index(streamOccurrences
            .getEntities().stream().filter(o -> o.getSample().getSurvey()
                  .getPortal().getId() == portalId)
            .collect(Collectors.toList()), false);

      occurrenceIndexCommands.delete(streamOccurrences.getDeletedIds());

      if (!continuationToken.equals(nextToken))
      {
         persistToken(portalId, nextToken);
      }
   }

   private void persistToken(int portalId, ContinuationToken continuationToken)
         throws IOException
   {
      Path tokenFileName = createTokenPath(portalId);
      try (FileWriter fileWriter = new FileWriter(tokenFileName.toFile());)
      {
         IOUtils.write(continuationToken.serialize(), fileWriter);
      }
      catch (IOException e)
      {
         LOGGER.error("Faiure persiting token {} for portal {}",
               continuationToken, portalId, e);
         throw e;
      }
   }

   private ContinuationToken getContinationToken(int portalId)
         throws IOException
   {
      Path tokeFileName = createTokenPath(portalId);
      ContinuationToken token;
      if (Files.exists(tokeFileName))
      {
         String tokenString = IOUtils.toString(tokeFileName.toUri(), "UTF-8");
         try
         {
            token = new ContinuationToken(tokenString);
         } catch (Exception e)
         {
            LOGGER.info(
                  "Failure parsing token string {}, start from the beginning",
                  tokenString);
            token = ContinuationToken.FIRST;
         }
      } else
      {
         token = ContinuationToken.FIRST;
      }

      return token;
   }

   private Path createTokenPath(int portalId)
   {
      return indexStateDir.resolve("STATE-" + portalId);
   }

}
