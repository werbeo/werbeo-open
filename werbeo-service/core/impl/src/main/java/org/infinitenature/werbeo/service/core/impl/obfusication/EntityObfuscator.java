package org.infinitenature.werbeo.service.core.impl.obfusication;

import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.support.Context;

@FunctionalInterface
public interface EntityObfuscator<T extends BaseType<?>>
{
   T obfuscate(T entity, Context context);
}
