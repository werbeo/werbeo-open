package org.infinitenature.werbeo.service.core.impl.imports.csv;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Herbarium;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FloraCSVImporter extends AbstractCSVImporter implements CSVImporter
{
   
   // TODO : move to Configuration 
   List<Integer> supportedPortalIds = Arrays.asList(new Integer[] {2,4,5});
   
   private static final Logger LOGGER = LoggerFactory
         .getLogger(FloraCSVImporter.class);
   
   
   @Autowired
   private PositionFactory positionFactory;

   
   protected SampleWithComment map2Sample(String[] nextLine, Context context)
   {
      Sample sample = new Sample();
      String comment = null;
      
      String taxonName = nextLine[2];
      String taxonExternalKey = nextLine[3];
      
      TaxonBase taxon = processTaxon(context, taxonName,
            taxonExternalKey);
      
      Occurrence occurrence = new Occurrence();
      occurrence.setTaxon(taxon);
      sample.getOccurrences().add(occurrence);
      
      String uuidString = nextLine[0];
      sample.setId(getOrCreateID(uuidString));
      
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setId(UUID.randomUUID());
      
      //Finder/Determiner
      String finderString = nextLine[6];
      String determinerString = nextLine[5];
      sample.setRecorder(toPerson(finderString, context));
      occurrence.setDeterminer(toPerson(determinerString, context));
      // no determiner given? use finder!
      if(occurrence.getDeterminer() == null)
      {
         occurrence.setDeterminer(sample.getRecorder());
      }
      
      occurrence.setObservers(nextLine[8]);
      
      
      // column 24 for Quarasek citeId
      if(StringUtils.isNotBlank(nextLine[24]))
      {
         occurrence.setCiteId(nextLine[24]);
         occurrence.setCiteComment(nextLine[19]);
      }
      else if(StringUtils.isNotBlank(nextLine[18]))
      {
         occurrence.setCiteId("floralit_" + nextLine[18]);
         occurrence.setCiteComment(nextLine[19]);
      }
      
      if (StringUtils.isNotBlank(nextLine[20])
            && StringUtils.isNotBlank(nextLine[21]))
      {
         occurrence.setHerbarium(new Herbarium(nextLine[21], nextLine[20]));
      }
      
      sample.setLocality(mapLocality(nextLine[11], nextLine[12], nextLine[9],
            nextLine[10], nextLine[13], nextLine[14]));
      
      
      String amountText = nextLine[4];
      String remarkText = StringUtils.isNotBlank(nextLine[16]) ? nextLine[16] : "";
      occurrence.setRemark(remarkText + (StringUtils.isNotBlank(amountText)
            ? "; Mengenangabe:" + amountText
            : ""));
      
      processDate(nextLine[1], sample);
      sample.setSurvey(getOrCreateSurvey("CSV-Importe", context));
      
      processHerbar(nextLine[20], nextLine[21], occurrence);
      
      
      return new SampleWithComment(sample, comment);
   }

   private void processHerbar(String herbariumCode, String belegnr,
         Occurrence occurrence)
   {
      if(StringUtils.isNotBlank(herbariumCode) && StringUtils.isNotBlank(belegnr))
      {
         Herbarium herbarium = new Herbarium(herbariumCode, belegnr); 
         occurrence.setHerbarium(herbarium);
      }
   }

   private UUID getOrCreateID(String uuidString)
   {
      if(StringUtils.isNotBlank(uuidString))
      {
         try
         {
            return UUID.fromString(uuidString);
         }
         catch(Exception e)
         {
            LOGGER.debug("Error parsing UUID");
         }
      }
         
      return UUID.randomUUID();
   }

   /**
    * Currently only supports GK Zone 4  (EPSG: 31468)
    * @param easting
    * @param northing
    * @param mtb
    * @param mtbq
    * @return
    */
   private Locality mapLocality(String easting, String northing,
          String mtb, String mtbq, String blur, String localityString)
   {
      int epsg = getEpsgFromCoordinate(easting);
      
      Locality locality = new Locality();
      if(StringUtils.isNotBlank(easting) && StringUtils.isNotBlank(northing))
      {
         locality.setPrecision(Integer.valueOf(blur));
         locality.setPosition(positionFactory.create(Double.valueOf(easting),
               Double.valueOf(northing), epsg));
         locality.setLocality(localityString);
      }
      else
      {
         locality.setPosition(positionFactory.createFromMTB(
               mtb + (StringUtils.isNotBlank(mtbq) ? "/" + mtbq : "")));
      }
      
      return locality;
   }


   private int getEpsgFromCoordinate(String easting)
   {
      int epsg = 31468;
      if(StringUtils.isNotBlank(easting) )
      {
         if(easting.startsWith("3"))
         {
            epsg = 31467;
         }
         if(easting.startsWith("4"))
         {
            epsg = 31468;
         }
         if(easting.startsWith("5"))
         {
            epsg = 31469;
         }
      }
      return epsg;
   }

   @Override
   public boolean handlesPortal(int portalId)
   {
      return supportedPortalIds.contains(portalId);
   }

}
