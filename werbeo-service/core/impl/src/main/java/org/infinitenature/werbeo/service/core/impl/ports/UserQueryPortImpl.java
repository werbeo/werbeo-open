package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.UserSortField;
import org.infinitenature.werbeo.service.core.api.error.NotEnoughtRightsException;
import org.infinitenature.werbeo.service.core.api.infra.query.UserQueries;
import org.infinitenature.werbeo.service.core.api.ports.UserQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserQueryPortImpl implements UserQueryPort
{

   @Autowired
   private UserQueries userQueries;

   @Override
   public long count(String email, Context context)
   {
      checkAdminState(context);
      return userQueries.count(email);
   }

   @Override
   public Slice<User, UserSortField> find(String email, int offset, int limit,
         Context context)
   {
      checkAdminState(context);
      return userQueries.find(email, offset, limit,
            context.getPortal().getId());
   }

   private void checkAdminState(Context context)
   {
      if (!context.isAdmin())
      {
         throw new NotEnoughtRightsException(
               "User Query and Change only for admins");
      }
   }

}
