package org.infinitenature.werbeo.service.core.impl.ports;

import java.time.ZoneId;

import org.infinitenature.werbeo.service.core.api.enity.FrequencyDistribution;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceIndexQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceStatsQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceStatsQueryPortImpl implements OccurrenceStatsQueryPort
{
   @Autowired
   private OccurrenceIndexQueries occurrenceIndexQueries;

   @Override
   @Cacheable(cacheNames = "occ-stats", keyGenerator = "nameAwareKeyGenerator")
   public FrequencyDistribution getOccurrencesCreatedPerDay(Context context,
         int days)
   {
      return occurrenceIndexQueries.getOccurrencesPerDay(context, days,
            ZoneId.of("Europe/Berlin"));
   }

   @Override
   @Cacheable(cacheNames = "occ-stats", keyGenerator = "nameAwareKeyGenerator")
   public FrequencyDistribution getOccurrencesCreatedPerWeek(Context context,
         int weeks)
   {
      return occurrenceIndexQueries.getOccurrencesPerWeek(context, weeks,
            ZoneId.of("Europe/Berlin"));
   }

   @Override
   @Cacheable(cacheNames = "occ-stats", keyGenerator = "nameAwareKeyGenerator")
   public FrequencyDistribution getOccurrencesCreatedPerMonth(Context context,
         int months)
   {
      // We need UTC here because else month calculation for solr fails -
      // hopefully nobody notices
      return occurrenceIndexQueries.getOccurrencesPerMonth(context, months,
            ZoneId.of("UTC"));
   }

}
