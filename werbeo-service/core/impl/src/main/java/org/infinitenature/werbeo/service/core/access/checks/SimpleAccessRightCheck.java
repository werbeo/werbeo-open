package org.infinitenature.werbeo.service.core.access.checks;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.access.AccessRightsCheck;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.support.Context;

public abstract class SimpleAccessRightCheck<T extends BaseType<?>>
      implements AccessRightsCheck<T>
{

   private final Class<T> type;

   public SimpleAccessRightCheck(Class<T> type)
   {
      super();
      this.type = type;
   }

   @Override
   public Set<Operation> getAllowedOperations(T entity, Context context)
   {
      if (context.hasRole(Roles.ADMIN))
      {
         return Collections.unmodifiableSet(operationForAdmin());
      } else if (context.isUserLoggedIn()
            && context.getUser().equals(entity.getCreatedBy()))
      {
         return Collections.unmodifiableSet(operationForOwner());
      } else if (context.isUserLoggedIn())
      {
         return Collections.unmodifiableSet(operationForLoggedIn());
      } else
      {
         return Collections.unmodifiableSet(operationsForAnonymous());
      }
   }

   @Override
   public boolean isAllowedTo(Operation operation, Class<? extends T> type,
         Context context)
   {
      switch (operation)
      {
      case READ:
         if (anoymousAccessIsAllowed(context))
         {
            return isAllowedToFind(type, context);
         } else
         {
         return userIsProperLoggedIn(context)
                  && isAllowedToFind(type, context);
         }
      default:
         return false;
      }
   }

   private boolean anoymousAccessIsAllowed(Context context)
   {
      if (context.getPortalConfiguration().isPresent())
      {
         return context.getPortalConfiguration().get().isAllowAnonymousAccess();
      } else
      {
         return false;
      }
   }

   private boolean userIsProperLoggedIn(Context context)
   {
      return isAccepted(context) && isPrivacyAccepted(context);
   }

   @Override
   public boolean isAllowedTo(Operation operation, T entity, Context context)
   {
      switch (operation)
      {
      case CREATE:
         return userIsProperLoggedIn(context)
               && isAllowedToCreate(entity, context);
      case UPDATE:
      case DELETE:
         return userIsProperLoggedIn(context)
               && isAllowedToChange(entity, context, operation);

      default:
         return false;
      }

   }

   protected boolean isAllowedToChange(T entity, Context context,
         Operation operation)
   {
      return entity.getAllowedOperations().contains(operation);
   }

   protected abstract boolean isAllowedToCreate(T entity, Context context);

   protected abstract boolean isAllowedToFind(Class<? extends T> type,
         Context context);

   @Override
   public boolean isResponsibleFor(Class<?> type, Context context)
   {
      return this.type.isAssignableFrom(type);
   }

   protected boolean isAccepted(Context context)
   {
      return context.isAccepted();
   }

   protected boolean isPrivacyAccepted(Context context)
   {
      return true;
   }

   protected boolean isUserLoggedId(Context context)
   {
      return context.isUserLoggedIn();
   }

   protected Set<Operation> operationForAdmin()
   {
      return new HashSet<>(
            Arrays.asList(Operation.DELETE, Operation.READ, Operation.UPDATE));
   }

   protected Set<Operation> operationForLoggedIn()
   {
      return new HashSet<>(Arrays.asList(Operation.READ));
   }

   protected Set<Operation> operationForOwner()
   {
      return new HashSet<>(
            Arrays.asList(Operation.DELETE, Operation.READ, Operation.UPDATE));
   }

   protected Set<Operation> operationsForAnonymous()
   {
      return Collections.emptySet();
   }
}
