package org.infinitenature.werbeo.service.core.impl.imports;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportCommands;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceImportCommands;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.imports.csv.ImportFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceImportCommandsPortImpl
      implements OccurrenceImportCommands
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceImportCommandsPortImpl.class);
   private ImportCommands importCommands;
   
   @Autowired
   private ImportFacade importFacade;
   @Autowired
   private AccessRightsChecker accessRightsChecker;

   public OccurrenceImportCommandsPortImpl(
         @Autowired ImportCommands importCommands)
   {
      this.importCommands = importCommands;
   }

   @Override
   public ImportJob importXML(String bdeXml, Context context)
   {
      return importCommands.storeImportfile(bdeXml, context);
   }

   @Override
   public ImportJob importCSV(String csvContent, Context context)
   {
      return importCommands.storeCSVImportfile(csvContent, context);
   }
   
   @Override
   public void delete(UUID importId, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class, context);
      importFacade.deleteImport(importId, context);
   }

}
