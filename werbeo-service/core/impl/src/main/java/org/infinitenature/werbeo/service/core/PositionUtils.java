package org.infinitenature.werbeo.service.core;

import org.infinitenature.werbeo.service.core.api.enity.Position;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiLineString;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

public class PositionUtils
{
   public static int calculatePrecision(Position positon)
   {
      return calculatePrecision(positon.getWkt());
   }

   public static int calculatePrecision(String wkt)
   {
      try
      {
         GeometryFactory geometryFactory = new GeometryFactory();
         WKTReader reader = new WKTReader(geometryFactory);
         Geometry geom = reader.read(wkt);

         return calculatePrecision(geom);
      } catch (ParseException e)
      {
         throw new RuntimeException("Failure parsing wkt", e);
      }
   }

   public static int calculatePrecision(Geometry geom)
   {
      double area = 0;

      if (geom instanceof Polygon)
      {
         Polygon polygon = (Polygon) geom;
         area = polygon.getArea() * 6371 * 1000000d;
      } 
      else if (geom instanceof org.locationtech.jts.geom.Point )
      {
    	  return 1000;
      }
      else if (geom instanceof MultiPolygon)
      {
         MultiPolygon multiPolygon = (MultiPolygon) geom;
         area = multiPolygon.getArea();
      } else
      {
         MultiLineString multiPolygon = (MultiLineString) geom;
         area = multiPolygon.getArea();
      }

      double length = (Math.sqrt(area));
      // pythagoras!
      double diagonal = Math.sqrt(length * length + length * length);
      // half the diagonal
      int precision = (int) (diagonal / 2);
      return precision;
   }
}
