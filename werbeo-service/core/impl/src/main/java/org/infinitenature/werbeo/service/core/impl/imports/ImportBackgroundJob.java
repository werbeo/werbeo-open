package org.infinitenature.werbeo.service.core.impl.imports;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadPoolExecutor;

import javax.annotation.PostConstruct;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.ImportFailure;
import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.ImportPart;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportQueries;
import org.infinitenature.werbeo.service.core.api.ports.SampleCommandPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.imports.concurent.BlockinigThreadPoolExecutorService;
import org.infinitenature.werbeo.service.core.impl.imports.csv.CSVImportProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.thoughtworks.xstream.XStream;

@Service
public class ImportBackgroundJob
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ImportBackgroundJob.class);

   private static XStream xstream = new XStream();

   @Autowired
   private ImportQueries importQueries;
   @Autowired
   private ImportCommands importCommands;
   @Autowired
   private SampleCommandPort sampleCommandPort;
   @Autowired
   private InstanceConfig instanceConfig;
   private ThreadPoolExecutor excecutor;

   @PostConstruct
   public void init()
   {
      excecutor = new BlockinigThreadPoolExecutorService(
            instanceConfig.getImporterThreads(), "ImporterPool");
   }

   @Scheduled(fixedDelay = 1_000)
   public void importParts()
   {

      Optional<ImportJob> importJob = importQueries
            .findOneImportJobByStatus(JobStatus.RUNNING);
      if (importJob.isPresent())
      {

         Optional<ImportPart> importPart = importCommands
               .getNextAndSetRunning(importJob.get());

         while (importPart.isPresent())
         {
            ImportPart thisPart = importPart.get();
            excecutor.execute(() -> importPart(thisPart));
            importPart = importCommands.getNextAndSetRunning(importJob.get());
         }
         importCommands.removeLock(importJob.get());
      } else
      {
         LOGGER.debug("No found to create ImportParts for");
      }
   }

   private void importPart(ImportPart importPart)
   {
      try
      {
         Context context = importQueries.getContext(importPart.getImportJob());
         Sample sample = importPart.getSample();
         UUID sampleID = sampleCommandPort.saveOrUpdate(sample, context,
               importPart.getSampleComments(),
               importPart.getOccurrenceComments());
         importPart.setStatus(JobStatus.FINISHED);
         LOGGER.info("Imported sample with id {} successfull for job {}",
               sampleID, importPart.getImportJob().getId());
      } catch (Exception e)
      {
         importPart.setStatus(JobStatus.FAILURE);
         importPart.setFailureReason(Optional.ofNullable(toImportFailure(e)));
      }
      importCommands.saveOrUpdate(importPart);
   }

   private ImportFailure toImportFailure(Exception e)
   {
      ImportFailure importFailure = new ImportFailure();
      importFailure.setFailure(xstream.toXML(e));
      return importFailure;
   }
}
