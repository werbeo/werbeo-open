package org.infinitenature.werbeo.service.core.aspects;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class CorePortsPerformance
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CorePortsPerformance.class);

   @Pointcut("execution(public * org.infinitenature.werbeo.service.core.api.ports.*.*(..))")
   void portMethod()
   {
   }

   @Around("portMethod()")
   public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable
   {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      Object retVal = pjp.proceed();
      stopWatch.stop();
      String requestId = getRequestId(pjp.getArgs());
      LOGGER.info("Request: {}, method: {} took {}ms", requestId,
            pjp.getSignature(), stopWatch.getTime(TimeUnit.MILLISECONDS));

      return retVal;
   }

   private String getRequestId(Object[] args)
   {
      for (Object arg : args)
      {
         if (arg instanceof Context)
         {
            return ((Context) arg).getRequestId();
         }
      }
      return null;
   }
}
