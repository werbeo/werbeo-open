package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.infinitenature.werbeo.service.core.api.infra.query.SurveyQueries;
import org.infinitenature.werbeo.service.core.api.ports.SurveyQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SurveyQueryPortImpl implements SurveyQueryPort
{
   @Autowired
   private SurveyQueries surveyQueries;

   @Autowired
   private AccessRightsChecker accessRightsChecker;

   @Override
   public Slice<Survey, SurveySortField> find(String title,
         String titleContains,
         Integer parentSurveyId, Context context,
         OffsetRequest<SurveySortField> offsetRequest)
   {
      Slice<Survey, SurveySortField> surveys = surveyQueries.findSurveys(title,
            titleContains, parentSurveyId, context, offsetRequest);
      accessRightsChecker.addAllowedOperations(surveys, context);
      return surveys;
   }

   @Override
   public long count(String title, String titleContains, Integer parentSurveyId,
         Context context)
   {
      return surveyQueries.countSurveys(title, titleContains, parentSurveyId,
            context);
   }

   @Override
   public Survey get(int id, Context context)
   {
      Survey survey = surveyQueries.load(id, context);
      if (survey != null)
      {
         accessRightsChecker.addAllowedOperations(survey, context);
      }
      return survey;
   }

}
