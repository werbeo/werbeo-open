package org.infinitenature.werbeo.service.core.impl.imports.csv;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Herbarium;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FloraSachsenCSVImporter extends AbstractCSVImporter implements CSVImporter
{
   
   List<Integer> supportedPortalIds = Arrays.asList(new Integer[] {10});
   
   private static final Logger LOGGER = LoggerFactory
         .getLogger(FloraSachsenCSVImporter.class);
   
   @Autowired
   private PositionFactory positionFactory;

   
   protected SampleWithComment map2Sample(String[] nextLine, Context context)
   {
      Sample sample = new Sample();
      String comment = null;
      
      String taxonName = nextLine[2];
      String taxonExternalKey = nextLine[3];
      
      TaxonBase taxon = processTaxon(context, taxonName,
            taxonExternalKey);
      
      Occurrence occurrence = new Occurrence();
      occurrence.setTaxon(taxon);
      sample.getOccurrences().add(occurrence);
      
      String uuidString = nextLine[0];
      sample.setId(getOrCreateID(uuidString));
      
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setId(UUID.randomUUID());
      
      //Finder/Determiner
      String finderString = nextLine[6];
      String determinerString = nextLine[5];
      sample.setRecorder(toPerson(finderString, context));
      occurrence.setDeterminer(toPerson(determinerString, context));
      // no determiner given? use finder!
      if(occurrence.getDeterminer() == null)
      {
         occurrence.setDeterminer(sample.getRecorder());
      }
      
      occurrence.setObservers(nextLine[8]);
      
      sample.setLocality(mapLocality(nextLine[11], nextLine[12], nextLine[9],
            nextLine[10], nextLine[13], nextLine[14],nextLine[17]));
      
      comment = nextLine[16];
      
      processWolfYear(nextLine[1], sample);
      sample.setSurvey(getOrCreateSurvey("CSV-Importe", context));
      
      processHerbar(nextLine[20], nextLine[21], occurrence);
      
      
      return new SampleWithComment(sample, comment);
   }


   private void processWolfYear(String string, Sample sample)
   {
      int starYear = Integer.parseInt(string.split("/")[0]);
      int endYear = Integer.parseInt(string.split("/")[1]);
      sample.setDate(VagueDateFactory.create("" + starYear + "-05-01",
            "" + endYear + "-04-30", "DD"));
   }


   private void processHerbar(String herbariumCode, String belegnr,
         Occurrence occurrence)
   {
      if(StringUtils.isNotBlank(herbariumCode) && StringUtils.isNotBlank(belegnr))
      {
         Herbarium herbarium = new Herbarium(herbariumCode, belegnr); 
         occurrence.setHerbarium(herbarium);
      }
   }




   private UUID getOrCreateID(String uuidString)
   {
      if(StringUtils.isNotBlank(uuidString))
      {
         try
         {
            return UUID
                  .fromString(uuidString.replace("{", "").replace("}", ""));
         }
         catch(Exception e)
         {
            LOGGER.error("Error parsing UUID");
         }
      }
         
      return UUID.randomUUID();
   }

   /**
    * Currently only supports GK Zone 4  (EPSG: 31468)
    * @param easting
    * @param northing
    * @param mtb
    * @param mtbq
    * @return
    */
   private Locality mapLocality(String easting, String northing,
          String mtb, String mtbq, String blur, String locationComment, String epsgString)
   {
      int epsg = Integer.valueOf(epsgString);
      
      Locality locality = new Locality();
      if(StringUtils.isNotBlank(easting) && StringUtils.isNotBlank(northing))
      {
         // use MTB blur by default
         locality.setPrecision(
               StringUtils.isNotBlank(blur) ? Integer.valueOf(blur) : 7285);
         locality.setPosition(positionFactory.create(Double.valueOf(easting),
               Double.valueOf(northing), epsg));
         locality.setLocationComment(locationComment);
      }
      else
      {
         locality.setPosition(positionFactory.createFromMTB(
               mtb + (StringUtils.isNotBlank(mtbq) ? "/" + mtbq : "")));
      }
      
      return locality;
   }



   @Override
   public boolean handlesPortal(int portalId)
   {
      return supportedPortalIds.contains(portalId);
   }

}
