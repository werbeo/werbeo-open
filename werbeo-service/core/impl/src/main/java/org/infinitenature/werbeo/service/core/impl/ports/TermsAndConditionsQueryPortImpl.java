package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.query.TermsAndConditionsQueries;
import org.infinitenature.werbeo.service.core.api.ports.TermsAndConditionsQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TermsAndConditionsQueryPortImpl
      implements TermsAndConditionsQueryPort
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TermsAndConditionsQueryPortImpl.class);
   @Autowired
   private TermsAndConditionsQueries queries;

   @Override
   public String getTC(Context context)
   {
      try
      {
         return queries.getTC(context.getPortal());
      } catch (EntityNotFoundException e)
      {
         LOGGER.warn(
               "No terms and conditions found for portal {},\n\tuseing default ones",
               context.getPortal());
         return queries.getDefaultTC();
      }
   }


}
