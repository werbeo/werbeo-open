package org.infinitenature.werbeo.service.core.impl.obfusication.nopeople;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.EntityObfuscator;
import org.springframework.stereotype.Component;

@Component
public class NoPersonOccurrenceObfuscator
      extends AbstractNoPersonObfuscator implements EntityObfuscator<Occurrence>
{
   @Override
   public Occurrence obfuscate(Occurrence occurrence, Context context)
   {
      if (isObfuscationNecessary(occurrence.getSample(), context))
      {
         return doObfuscate(occurrence);
      }
      return occurrence;
   }

   private Occurrence doObfuscate(Occurrence occurrence)
   {
      ObfuscationUtils.obfuscatePeople(occurrence);
      ObfuscationUtils.obfuscatePeople(occurrence.getSample());
      ObfuscationUtils.obfuscatePeople(occurrence.getSample().getSurvey());
      return occurrence;
   }
}
