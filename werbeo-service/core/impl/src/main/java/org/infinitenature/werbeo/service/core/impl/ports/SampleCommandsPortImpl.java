package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.BaseTypeUUID;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.NotEnoughtRightsException;
import org.infinitenature.werbeo.service.core.api.error.ValidationException;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceIndexCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.PeopleCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.SampleCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceCommandsPort;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.infinitenature.werbeo.service.core.api.ports.SampleCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.SampleQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.ports.taxon.TaxonCache;
import org.infinitenature.werbeo.service.core.impl.validation.ValidationContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.togglz.core.manager.FeatureManager;

@Service
public class SampleCommandsPortImpl
      implements SampleCommandPort
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SampleCommandsPortImpl.class);

   @Autowired
   private SampleCommands sampleCommands;
   @Autowired
   private OccurrenceCommandsPort occurrenceCommandsPort;
   @Autowired
   private AccessRightsChecker accesRightsChecker;
   @Autowired
   private SampleQueryPort sampleQueries;
   @Autowired
   private OccurrenceIndexCommands occurrenceIndexCommands;
   @Autowired
   private FeatureManager featureManager;
   @Autowired
   private PeopleCommands peopleCommands;
   @Autowired
   private PersonQueries personQueries;
   @Autowired
   private Validator validator;
   @Autowired
   private SurveyQueryPort surveyQueryPort;
   @Autowired
   private FieldConfigQueries fieldConfigQueries;
   @Autowired
   private LegacyQuantitySync legacyQuantiySync;
   @Autowired
   private TaxonCache taxonCache;


   @Override
   public UUID saveOrUpdate(Sample sample, Context context)
   {
      ValidationContextHolder.setThreadLocalContext(context);

      if (sample.getId() == null)
      {
         accesRightsChecker.isAllowedTo(Operation.CREATE, sample, context);
         createNeedetUUIDs(sample);
      } else
      {
         Sample persistedSample = null;
         try
         {
            persistedSample = sampleQueries.get(sample.getId(), context);
         } catch (EntityNotFoundException e)
         {
            // is ok, because an uuid might be assigned form another system
         }
         if (persistedSample != null)
         {
            accesRightsChecker.isAllowedTo(Operation.UPDATE, persistedSample,
                  context);
         } else
         {
            accesRightsChecker.isAllowedTo(Operation.CREATE, sample, context);
         }
      }
      legacyQuantiySync.syncQuantiyFieldsOnSave(sample, context);

      sample.getOccurrences().forEach(this::createNeedetUUIDs);
      sample.getOccurrences().forEach(occ -> validate(occ, context));

      validate(sample, context);

      // find or create unidentified determiner/recorder

      sample.setRecorder(findOrCreate(sample.getRecorder(), context));
      for(Occurrence occurrence : sample.getOccurrences())
      {
         occurrence.setDeterminer(findOrCreate(occurrence.getDeterminer(), context));
      }

      UUID sampleId = sampleCommands.saveOrUpdate(sample, context);

      index(sampleId, context);

      refreshTaxonCache(sample, context);

      return sampleId;
   }


   private Person findOrCreate(Person person, Context context)
   {
      if(person != null)
      {
         if(person.getId() != null && person.getId() > 0)
         {
            return person;
         }
         else
         {
            if(StringUtils.isNotBlank(person.getEmail()))
            {
               PersonFilter filter = new PersonFilter("", person.getEmail());
               Slice<Person, PersonSortField> slice = personQueries.find(filter,
                     context, new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
                     PersonSortField.ID));
               if(slice.hasContent())
               {
                  return slice.getContent().get(0);
               }
               else
               {
                  Person newPerson = new Person();
                  newPerson.setFirstName(person.getFirstName());
                  newPerson.setLastName(person.getLastName());
                  newPerson.setEmail(person.getEmail());
                  int personId = peopleCommands.saveOrUpdate(newPerson, context);
                  return personQueries.get(personId, context);
               }
            }
         }
      }
      return null;
   }

   protected void validate(Sample sample, Context context)
   {
      Set<ConstraintViolation<Sample>> constraintViolations = validator
            .validate(sample);
      if (!constraintViolations.isEmpty())
      {
         LOGGER.error("Failure validating sample {}", constraintViolations);
         throw new ValidationException("Failure validating sample",
               constraintViolations);
      }
      if (surveyQueryPort.get(sample.getSurvey().getId(), context) == null)
      {
         String message = "Survey with id "
               + sample.getSurvey().getId() + " not found for portal "
               + context.getPortal().getId();
         LOGGER.error(message);
         throw new ValidationException(message,
               Collections.emptySet());
      }
      Validate.notNull(sample.getId(), "The sample id must not be null");

      sample.getOccurrences().forEach(occ -> validate(occ, context));
   }

   private void validate(Occurrence occurrence, Context context)
   {
      Validate.notNull(occurrence.getId(),
            "The occurrence id must not be null");
      if(occurrence.getTaxon().getId() == null)
      {
         throw new ValidationException("The taxon id must not be null",
               Collections.emptySet());
      }
      if (occurrence.getNumericAmount() != null && fieldConfigQueries
            .getOccurrenceConfig(context.getPortal())
            .getFieldConfig(OccurrenceField.NUMERIC_AMOUNT_ACCURACY).isPresent()
            && occurrence.getNumericAmountAccuracy() == null)
      {
         throw new ValidationException(
               "The numieric amount accuray must not be null, because a numeric accouracy is given",
               Collections.emptySet());
      }
   }

   private void createNeedetUUIDs(BaseTypeUUID uuidType)
   {
      if (uuidType.getId() == null)
      {
         UUID newUUID = UUID.randomUUID();
         uuidType.setId(newUUID);
         LOGGER.info("Added new UUID {} to {}", newUUID,
               uuidType.getClass().getSimpleName());
      }
   }

   @Override
   public void delete(UUID sampleId, Context context)
   {

      Sample persistedSample = sampleQueries.get(sampleId, context);
      accesRightsChecker.isAllowedTo(Operation.DELETE, persistedSample,
            context);
      sampleCommands.delete(persistedSample, context);

      if (featureManager.isActive(WerBeoFeatures.UPDATE_INDEX_ON_SAVE))
      {
         occurrenceIndexCommands.deleteBySampleId(sampleId);
      }
   }

   @Override
   public int addComment(UUID sampleId, SampleComment comment, Context context)
   {
      if (comment.getId() != null)
      {
         throw new NotEnoughtRightsException(
               "Updateing comments is not alloewd");
      }
      sampleQueries.get(sampleId, context); // throws Exception if
                                                // user tries to comment
                                                // on a occurrence without
                                                // the right to read it
      return sampleCommands.addComment(sampleId, comment, context);
   }

   @Override
   public UUID saveOrUpdate(Sample sample, Context context,
         List<SampleComment> sampleComments,
         Map<Occurrence, List<OccurrenceComment>> occurrenceComments)
   {
      ValidationContextHolder.setThreadLocalContext(context);
      // TODO check access rights
      createNeedetUUIDs(sample);
      sample.getOccurrences().forEach(this::createNeedetUUIDs);
      sample.getOccurrences().forEach(occ -> validate(occ, context));
      UUID sampleId = sampleCommands.saveOrUpdate(sample, context);
      sampleComments.forEach(
            sampleComment -> addComment(sampleId, sampleComment, context));
      for (Entry<Occurrence, List<OccurrenceComment>> entry : occurrenceComments
            .entrySet())
      {
         entry.getValue()
               .forEach(occurrcneComment -> occurrenceCommandsPort.addComment(
                     entry.getKey().getId(),
                     occurrcneComment, context));
      }

      index(sampleId, context);
      refreshTaxonCache(sample, context);
      return sampleId;

   }

   private void refreshTaxonCache(Sample sample, Context context)
   {
      taxonCache.refreshUsedTaxaIfNecessary(
            sample.getSurvey().getPortal().getId(),
            getTaxonIds(sample.getOccurrences()), context);

   }


   private List<Integer> getTaxonIds(List<Occurrence> occurrences)
   {
      List<Integer> taxonIds = new ArrayList<>();
      occurrences.forEach(occ ->
      {
         taxonIds.add(occ.getTaxon().getId());
      });
      return taxonIds;
   }


   private void index(UUID sampleID, Context context)
   {
      if (featureManager.isActive(WerBeoFeatures.UPDATE_INDEX_ON_SAVE))
      {
         CompletableFuture.supplyAsync(() ->
         {
            Sample sample = sampleQueries.get(sampleID, context);
            occurrenceIndexCommands.index(sample.getOccurrences(), true);
            return true;
         });
      }
   }
}
