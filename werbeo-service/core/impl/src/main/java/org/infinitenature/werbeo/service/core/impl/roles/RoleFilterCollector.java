package org.infinitenature.werbeo.service.core.impl.roles;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.infra.roles.RoleFilter;
import org.infinitenature.werbeo.service.core.api.roles.Filter;

public class RoleFilterCollector implements
      Collector<RoleFilter, Map<Filter, Collection<Portal>>, Map<Filter, Collection<Portal>>>
{
   @Override
   public BiConsumer<Map<Filter, Collection<Portal>>, RoleFilter> accumulator()
   {
      return new BiConsumer<Map<Filter, Collection<Portal>>, RoleFilter>()
      {

         @Override
         public void accept(Map<Filter, Collection<Portal>> t, RoleFilter u)
         {
            if (!t.containsKey(u.getFilter()))
            {
               t.put(u.getFilter(), new HashSet<>());
            }
            t.get(u.getFilter()).add(u.getPortal());
         }
      };
   }

   @Override
   public Set<Characteristics> characteristics()
   {
      Set<Characteristics> characteristics = new HashSet<>();
      characteristics.add(Characteristics.UNORDERED);
      characteristics.add(Characteristics.IDENTITY_FINISH);
      return characteristics;
   }

   @Override
   public BinaryOperator<Map<Filter, Collection<Portal>>> combiner()
   {
      return new BinaryOperator<Map<Filter, Collection<Portal>>>()
      {

         @Override
         public Map<Filter, Collection<Portal>> apply(
               Map<Filter, Collection<Portal>> t,
               Map<Filter, Collection<Portal>> u)
         {
            HashMap<Filter, Collection<Portal>> combined = new HashMap<>(t);
            u.entrySet().forEach(entry ->
            {
               if (!combined.containsKey(entry.getKey()))
               {
                  combined.put(entry.getKey(), new HashSet<>());
               }
               combined.get(entry.getKey()).addAll(entry.getValue());
            });
            return combined;
         }
      };
   }

   @Override
   public Function<Map<Filter, Collection<Portal>>, Map<Filter, Collection<Portal>>> finisher()
   {
      return new Function<Map<Filter, Collection<Portal>>, Map<Filter, Collection<Portal>>>()
      {

         @Override
         public Map<Filter, Collection<Portal>> apply(
               Map<Filter, Collection<Portal>> t)
         {
            return t;
         }
      };
   }

   @Override
   public Supplier<Map<Filter, Collection<Portal>>> supplier()
   {
      return HashMap::new;
   }
}