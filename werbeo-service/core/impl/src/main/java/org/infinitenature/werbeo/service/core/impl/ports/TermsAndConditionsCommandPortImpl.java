package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.error.NotEnoughtRightsException;
import org.infinitenature.werbeo.service.core.api.infra.commands.RolesCommands;
import org.infinitenature.werbeo.service.core.api.ports.RolesCommandPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TermsAndConditionsCommandPortImpl implements RolesCommandPort
{
   @Autowired
   private RolesCommands commands;

   @Override
   public boolean accept(Context context)
   {
      if (!context.isUserLoggedIn())
      {
         throw new NotEnoughtRightsException(
               "Only logged in users can accept terms and conditions");
      }

      commands.addRole(context.getUser(), RoleNameFactory
            .getKeycloakRoleName(context.getPortal().getId(), Roles.ACCEPTED));
      return true;
   }

}
