package org.infinitenature.werbeo.service.core.impl.roles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.infra.query.FilterQueries;
import org.infinitenature.werbeo.service.core.api.infra.roles.RoleFilter;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Hard coded implementation of the {@link FilterQueries} interface
 *
 * @author dve
 *
 */
@Service
public class FilterQueriesImpl implements FilterQueries
{
   private final PortalQueryPort portalQueries;

   @Autowired
   public FilterQueriesImpl(PortalQueryPort portalQueries)
   {
      super();
      this.portalQueries = portalQueries;
   }

   @Override
   public List<RoleFilter> getFilters(Collection<Portal> portals)
   {
      List<RoleFilter> result = new ArrayList<>();
      portals.forEach(portal ->
      {
         PortalConfiguration portalConfiguration = portalQueries
               .loadConfig(portal.getId());
         if (!portalConfiguration.isAllowAnonymousAccess())
         {
            result.add(
                  new RoleFilter(Roles.APPROVED, Filter.ONLY_OWN_DATA, portal));
         }
      });
      return result;
   }

}
