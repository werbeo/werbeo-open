package org.infinitenature.werbeo.service.core.impl.imports.csv;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.vaguedate.VagueDate.Type;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.Language;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.infra.commands.PeopleCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.SampleCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.SurveyCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.SurveyQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;

import freemarker.template.utility.StringUtil;

public abstract class AbstractCSVImporter
{

   @Autowired
   private SampleCommands sampleCommands;
   @Autowired
   private SurveyQueries surveyQueries;
   @Autowired
   private SurveyCommands surveyCommands;
   @Autowired
   private InstanceConfig instanceConfig;
   @Autowired
   private TaxonQueries taxonQueries;
   @Autowired
   private PersonQueries personQueries;
   @Autowired
   private PeopleCommands peopleCommands;
   @Autowired
   private ProtocolHandler protocolHandler;
   @Autowired
   private ErrorHandler errorHandler;

   private static final Logger LOGGER = LoggerFactory
         .getLogger(AbstractCSVImporter.class);

   public String importCSV(String csv, String protocol, ImportJob importJob, Context context)
   {
      try
      {
         // for every already imported sample, we got a protocol-line, containing the UUID of the sample,
         // or "error" if previous import failured
         String[] protocolLines = fetchProtocolLines(protocol);

         CSVReader reader = setupCSVReader(csv);
         StringWriter stringWriter = new StringWriter();
         CSVWriter writer = new CSVWriter(stringWriter, ';', '"',
               CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

         String [] nextLine;
         boolean firstline = true;
         int lineIndex = 0;
         while ((nextLine = reader.readNext()) != null)
         {
            // skip header
            if(firstline)
            {
               firstline = false;
               writer.writeNext(nextLine); // resulting csv must contain the header, due to re-import possibility
               continue;
            }
            processSingleCSVLine(importJob, context, protocolLines, nextLine,
                  lineIndex);
            lineIndex++;
            writer.writeNext(nextLine);
         }

         reader.close();
         writer.close();
         return stringWriter.toString();
      }
      catch(Exception e)
      {
         e.printStackTrace();
      }
      return null;
   }

   private CSVReader setupCSVReader(String csv)
   {
      com.opencsv.CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
      CSVReader reader = new CSVReaderBuilder(new StringReader(csv)).withCSVParser(parser)
            .build();
      return reader;
   }

   private void processSingleCSVLine(ImportJob importJob, Context context,
         String[] protocolLines, String[] nextLine, int lineIndex)
   {
      try
      {
         // check whether line was already imported
         if(protocolLines.length > lineIndex)
         {
            getUUIDFromProtocol(protocolLines, nextLine, lineIndex);
         }
         else // not already imported
         {
            // write received sample UUID to 1st column (index=0) of CSV
            saveSampleAndWriteProtocol(importJob, context, nextLine);
         }
      }
      catch(Exception e)
      {
         LOGGER.error("Error importing sample", e);
         protocolHandler.writeProtocolLine( "error", importJob, context);
         errorHandler.writeError(e, lineIndex + 2, importJob, context);
      }
   }

   private void saveSampleAndWriteProtocol(ImportJob importJob, Context context,
         String[] nextLine)
   {
      SampleWithComment sampleWithComment = map2Sample(nextLine, context);
      nextLine[0] = sampleCommands.saveOrUpdate(sampleWithComment.getSample(), context).toString();
      if(StringUtils.isNotBlank(sampleWithComment.getComment()))
      {
         SampleComment comment = new SampleComment();
         comment.setComment(sampleWithComment.getComment());
         sampleCommands.addComment(UUID.fromString(nextLine[0]),
               comment, context);
      }
      protocolHandler.writeProtocolLine( nextLine[0], importJob, context);
   }

   private void getUUIDFromProtocol(String[] protocolLines, String[] nextLine,
         int lineIndex)
   {
      if(!"error".equals(protocolLines[lineIndex]))
      {
         // set the UUID from the already imported sample, if succeeded, else: leave it blank
         nextLine[0] = protocolLines[lineIndex];
      }
   }

   private String[] fetchProtocolLines(String protocol)
   {
      String[] protocolLines;
      if(StringUtils.isNotBlank(protocol))
      {
         protocolLines = protocol.split(System.getProperty("line.separator"));
      }
      else
      {
         protocolLines = new String[0];
      }
      return protocolLines;
   }

   protected TaxonBase processTaxon(Context context,
          String taxonName,
         String taxonExternalKey)
   {

      TaxonBase taxon;
      if(StringUtils.isNotBlank(taxonName))
      {
         taxon = chooseTaxon(taxonQueries.findBase(
               new TaxonFilter(taxonName,
                     new HashSet<Language>(
                           Arrays.asList(new Language[] { Language.LAT })),
                     true, null, false, new HashSet<String>(), null, true),
               context, getOffsetRequest()).getContent(), taxonName);
      }
      else
      {
         Set<String> taxonExternalKeys = new HashSet<>();
         taxonExternalKeys.add(taxonExternalKey);

         taxon = taxonQueries.findBase(
               new TaxonFilter("",
                     new HashSet<Language>(
                           Arrays.asList(new Language[] { Language.LAT })),
                     true, null, false, taxonExternalKeys, null, true),
               context, getOffsetRequest()).getContent().get(0);
      }
      return taxon;
   }

   protected TaxonBase chooseTaxon(List<TaxonBase> content, String taxonName)
   {
      TaxonBase taxon = null;
      if(content.size() == 1)
      {
         taxon = content.get(0);
      }
      else if(content.size() > 1)
      {
         for(TaxonBase resultTaxon : content)
         {
            // check for exact name match
            if(StringUtils.equalsIgnoreCase(taxonName, resultTaxon.getName()))
            {
               taxon = resultTaxon;
               break;
            }
         }
         if(taxon == null)
         {
            taxon = content.get(0);
         }
      }
      
      return taxon;
   }

   private OffsetRequest<TaxonSortField> getOffsetRequest()
   {
      OffsetRequest<TaxonSortField> offsetRequest = new OffsetRequest<TaxonSortField>()
      {

         @Override
         public SortOrder getSortOrder()
         {
            return SortOrder.ASC;
         }

         @Override
         public TaxonSortField getSortField()
         {
            return TaxonSortField.TAXON;
         }

         @Override
         public int getOffset()
         {
            return 0;
         }

         @Override
         public int getCount()
         {
            return Integer.MAX_VALUE;
         }
      };
      return offsetRequest;
   }

   protected Survey getOrCreateSurvey(String name, Context context)
   {
      List<Survey> existingSurveys = surveyQueries.findSurveys(name,name, null, context, new OffsetRequest<SurveySortField>()
      {
         @Override
         public SortOrder getSortOrder()
         {
            return SortOrder.DESC;
         }

         @Override
         public SurveySortField getSortField()
         {
            return SurveySortField.ID;
         }

         @Override
         public int getOffset()
         {
            // TODO Auto-generated method stub
            return 0;
         }

         @Override
         public int getCount()
         {
            // TODO Auto-generated method stub
            return 1;
         }
      }).getContent();
      if(existingSurveys.isEmpty())
      {
         Survey survey = new Survey();
         survey.setName(name);
         survey.setAvailability(Availability.RESTRICTED);
         survey.setId(surveyCommands.saveOrUpdate(survey, context));
         return survey;
      }
      else
      {
         return existingSurveys.get(0);
      }
   }

   /**
    * Find or Create a person
    * @param name
    * @return
    */

   protected Person toPerson(String name, Context context)
   {
      if(StringUtils.isNotBlank(name))
      {
         PersonFilter filter = new PersonFilter();
         String[] names = name.split(" ");
         if(names.length == 1)
         {
            filter.setLastName(name);
         }
         else if(names.length > 1)
         {
            filter.setFirstName(names[0]);
            filter.setLastName(name.substring(name.indexOf(" ")).trim());
         }

         Slice<Person, PersonSortField> slice = personQueries.find(filter,
               context, new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
               PersonSortField.ID));
         if(slice.hasContent())
         {
            return slice.getContent().get(0);
         }
         else
         {
            Person newPerson = new Person();
            newPerson.setFirstName(filter.getFirstName());
            newPerson.setLastName(filter.getLastName());
            setPseudoEmail(newPerson);
            int personId = peopleCommands.saveOrUpdate(newPerson, context);
            return personQueries.get(personId, context);
         }
      }
      return null;
   }

   protected void setPseudoEmail(Person person)
   {
      if (StringUtils.isNotBlank(person.getFirstName()))
      {
         person.setEmail(noUmlautsNoPoints(person.getFirstName()) + "."
               + noUmlautsNoPoints(person.getLastName()) + "@werbeo.de");
      } else
      {
         person.setEmail(noUmlautsNoPoints(person.getLastName()) + "@werbeo.de");
      }
   }

   private String noUmlautsNoPoints(String string)
   {
      return string.trim().replace(" ", "-").replace(".", "").replace("ä", "ae")
            .replace("ö", "oe").replace("ü", "ue").replace("ß", "ss");
   }

   /**
    * Supported Dates:
    *
    * 00.00.YEAR
    *
    * 00.MM.YEAR
    *
    * DD.MM.YEAR
    *
    * @param nextLine
    * @param sample
    */
   protected void processDate(String dateString, Sample sample)
   {

      if(dateString.startsWith("00.00."))
      {
         // YEAR  00.00.1988
         sample.setDate(VagueDateFactory
               .create(Integer.valueOf(dateString.substring(6))));
      }
      else if(dateString.length() == 4)
      {
         // YEAR  1988
         sample.setDate(VagueDateFactory
               .create(Integer.valueOf(dateString)));
      }
      else if(dateString.length() == 9 && dateString.charAt(4) == '-')
      {
         // YEARS  1988-1999
         sample.setDate(VagueDateFactory.create(
               Integer.valueOf(dateString.substring(0, 4)),
               Integer.valueOf(dateString.substring(5))));
      }
      else if(dateString.startsWith("00."))
      {
         // MONTH
         int year = Integer.valueOf(dateString.substring(6));
         int month = Integer.valueOf(dateString.substring(3 , 5));
         sample.setDate(VagueDateFactory
               .create(YearMonth.of(year, month), Type.MONTH_IN_YEAR));
      }
      else
      {
         // DATE
         try
         {
            Date date = DateUtils.parseDate(dateString, "dd.MM.yy");
            sample.setDate(VagueDateFactory.fromDate(date.toInstant()
                  .atZone(ZoneId.systemDefault())
                  .toLocalDate()));
         }
         catch(ParseException e)
         {
            e.printStackTrace();
         }
      }
   }

   protected abstract SampleWithComment map2Sample(String[] nextLine, Context context);
}
