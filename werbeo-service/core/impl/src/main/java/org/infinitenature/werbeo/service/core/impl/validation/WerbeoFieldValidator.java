package org.infinitenature.werbeo.service.core.impl.validation;

import java.util.Collection;
import java.util.Optional;

import javax.validation.ConstraintValidatorContext;

import org.infinitenature.werbeo.service.core.api.enity.meta.FieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.WerbeoField;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class WerbeoFieldValidator implements
      org.infinitenature.werbeo.service.core.api.validation.WerbeoFieldValidator
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(WerbeoFieldValidator.class);

   private WerbeoField<?> werbeoField;

   @Autowired
   private FieldConfigQueries fieldConfigQueries;

   @Override
   public boolean isValid(Object value, ConstraintValidatorContext context,
         ValidationMode validationMode)
   {
      Optional<FieldConfig> fieldConfig = null;
      if (werbeoField instanceof OccurrenceField)
      {
         fieldConfig = fieldConfigQueries
               .getOccurrenceConfig(ValidationContextHolder.get().getPortal())
               .getConfiguredFields().stream()
               .filter(field -> field.getField().equals(werbeoField))
               .map(occFC -> (FieldConfig) occFC).findFirst();
      } else if (werbeoField instanceof SurveyField)
      {
         fieldConfig = fieldConfigQueries
               .getSurveyConfig(ValidationContextHolder.get().getPortal())
               .getConfiguredFields().stream()
               .filter(field -> field.getField().equals(werbeoField))
               .map(occFC -> (FieldConfig) occFC).findFirst();
      } else if (werbeoField instanceof SampleField)
      {
         fieldConfig = fieldConfigQueries
               .getSampleConfig(ValidationContextHolder.get().getPortal())
               .getConfiguredFields().stream()
               .filter(field -> field.getField().equals(werbeoField))
               .map(occFC -> (FieldConfig) occFC).findFirst();
      }

      switch (validationMode)
      {
      case CHECK_MANDANTORY:
         if (fieldConfig.isPresent() && fieldConfig.get().isMandantory())
         {
            return value != null;
         }
         break;
      case CHECK_NOT_CONFIGURED:
         if (!fieldConfig.isPresent())
         {
            if (value instanceof Collection<?>)
            {
               return value == null || ((Collection<?>) value).isEmpty();
            } else
            {
               return value == null;
            }
         }
         break;
      case CHECK_VALUES:
         return checkValues(value);

      default:
         LOGGER.error("Not handeld validation mode: {}", validationMode);
         break;
      }

      return true;
   }

   private boolean checkValues(Object value)
   {
      return true;
   }


   @Override
   public void setField(WerbeoField<?> werbeoField)
   {
      this.werbeoField = werbeoField;
   }
}
