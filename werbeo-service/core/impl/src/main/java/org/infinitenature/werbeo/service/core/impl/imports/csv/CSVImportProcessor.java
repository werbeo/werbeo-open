package org.infinitenature.werbeo.service.core.impl.imports.csv;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CSVImportProcessor implements ProtocolHandler, ErrorHandler
{
   
   @Autowired
   private ImportQueries importQueries;
   
   @Autowired
   private ImportCommands importComands;
   
   @Autowired
   private List<CSVImporter> csvImporters = new ArrayList<>();
   
   @Scheduled(initialDelay = 10_000, fixedDelay=Long.MAX_VALUE)
   public void processRunningImports()
   {
      for(ImportJob nextJob : importQueries.findImportJobsByStatus(JobStatus.RUNNING_CSV))
      {
         Context context = importQueries.getContext(nextJob);
         String csv = importQueries.getCSV(nextJob);
         String protocol = importQueries.getCSVProtocol(nextJob);
         // the returned csv contains the internal UUIDs
         csv = importCSV(csv, protocol, nextJob, context);
         importComands.updateCSV(csv, context,nextJob);
         importComands.setJobStatus(nextJob, JobStatus.FINISHED, context);
         
         importComands.updateProtocol("", context, nextJob);
      }
   }
   
   @Scheduled(fixedDelay = 1_000, initialDelay = 60_000)
   public void processImports()
   {
      Optional<ImportJob> nextJob = importQueries
            .findOneImportJobByStatus(JobStatus.INITIALIZED_CSV);
      
      if(nextJob.isPresent())
      {
         Context context = importQueries.getContext(nextJob.get());
         importComands.setJobStatus(nextJob.get(), JobStatus.RUNNING_CSV, context);
         String csv = importQueries.getCSV(nextJob.get());
         // initially create protocol file
         importComands.updateProtocol("", context, nextJob.get());
         // initially create errorlog
         importComands.updateErrorLog("", context, nextJob.get());
         
         // the returned csv contains the internal UUIDs
         csv = importCSV(csv, "", nextJob.get(), context);
         importComands.updateCSV(csv, context,nextJob.get());
         importComands.setJobStatus(nextJob.get(), JobStatus.FINISHED, context);
      }
   }

   private String importCSV(String csv, String protocol, ImportJob importJob, Context context)
   {
      for(CSVImporter importer : csvImporters)
      {
         if(importer.handlesPortal(context.getPortal().getId()))
         {
            return importer.importCSV(csv, protocol, importJob, context);
         }
      }
      return csv;
   }
   
   @Override
   public void writeError(Exception e, int lineIndex, ImportJob importJob, Context context)
   {
      String errorLog = importQueries.getCSVErrors(importJob);
      StackTraceElement[] elements = e.getStackTrace();
      String trace = null;
      for (StackTraceElement element : elements)
      {
         trace = (trace == null) ? element.toString()
               : trace + "\n" + element.toString();
      }
      trace = trace + "\n";
      errorLog = errorLog + "Zeile:" + lineIndex + "\n";
      errorLog = errorLog + e.toString() + "\n";
      errorLog = errorLog + trace;
      importComands.updateErrorLog(errorLog, context, importJob);
   }

   @Override
   public void writeProtocolLine(String line, ImportJob importJob, Context context)
   {
      String protocol = importQueries.getCSVProtocol(importJob);
      protocol = protocol + line + "\n";
      importComands.updateProtocol(protocol, context, importJob);
   }
}
