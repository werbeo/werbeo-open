package org.infinitenature.werbeo.service.core.access.checks;

import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.stereotype.Component;

@Component
public class SurveyAccessRightCheck extends SimpleAccessRightCheck<Survey>
{

   public SurveyAccessRightCheck()
   {
      super(Survey.class);
   }

   @Override
   protected boolean isAllowedToCreate(Survey entity, Context context)
   {
      return currentUserIsAdmin(context) || currentUserIsOwner(entity, context);
   }

   @Override
   protected boolean isAllowedToFind(Class<? extends Survey> type,
         Context context)
   {
      return true;
   }

   private boolean currentUserIsAdmin(Context context)
   {
      return context.hasRole(Roles.ADMIN);
   }

   private boolean currentUserIsOwner(Survey entity, Context context)
   {
      return entity.getOwner() != null && entity.getOwner().getId()
            .equals(context.getUser().getPerson().getId());
   }

}
