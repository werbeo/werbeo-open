package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.query.*;
import org.infinitenature.werbeo.service.core.api.ports.*;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcceptanceQueryPortImpl implements AcceptanceQueryPort
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(AcceptanceQueryPortImpl.class);

   @Autowired
   private AcceptanceQueries queries;

   @Override
   public String getAcceptanceDeclaration(Context context)
   {
      try
      {
         return queries.getAcceptanceDeclaration(context.getPortal());
      } catch (EntityNotFoundException e)
      {
         LOGGER.warn(
               "No privacy declaration found for portal {},\n\tuseing default ones",
               context.getPortal());
         return queries.getDefaultAcceptanceDeclaration();
      }
   }
}
