package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceExportCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.export.ExportFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class OccurrenceExportCommandPortImpl
      implements OccurrenceExportCommandPort
{
   @Autowired
   private AccessRightsChecker accessRightsChecker;
   @Autowired
   private ExportFacade exportFacade;

   @Override
   public UUID create(OccurrenceFilter filter, FileFormat format,
         Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class, context);
      UUID exportId = UUID.randomUUID();
      exportFacade.createExport(filter, format, exportId, context);
      return exportId;
   }

   @Override
   public void delete(UUID exportId, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class, context);
      exportFacade.deleteExport(exportId, context);
   }
}
