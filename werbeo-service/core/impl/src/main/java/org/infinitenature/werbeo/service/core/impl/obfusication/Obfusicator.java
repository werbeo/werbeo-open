package org.infinitenature.werbeo.service.core.impl.obfusication;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Obfusicator
{
   @Autowired
   private ObfuscatorFactory obfuscatorFactory;

   public void obfuscateOccurrences(
         Slice<Occurrence, OccurrenceSortField> slice, Context context)
   {
      slice.getContent().forEach(entity -> obfuscate(entity, context));
   }

   public void obfuscate(Occurrence entity, Context context)
   {
      EntityObfuscator<Occurrence> obfusicator = obfuscatorFactory
            .getOccurrenceObfuscator(entity.getSample().getSurvey(), context);
      obfusicator.obfuscate(entity, context);
   }

   public void obfuscate(Sample entity, Context context)
   {
      EntityObfuscator<Sample> obfusicator = obfuscatorFactory
            .getSampleObfuscator(entity.getSurvey(), context);
      obfusicator.obfuscate(entity, context);
   }

   public void obfuscateOccurrenceCentroids(
         Slice<OccurrenceCentroid, OccurrenceCentroidSortField> slice,
         Context context)
   {
      slice.getContent().forEach(entity -> obfuscate(entity, context));
   }

   private void obfuscate(OccurrenceCentroid entity, Context context)
   {
      Survey survey = new Survey();
      survey.setId(entity.getSurveyId());
      EntityObfuscator<OccurrenceCentroid> obfusicator = obfuscatorFactory
            .getOccurrenceCentroidObfusicator(
                  survey, context);
      obfusicator.obfuscate(entity, context);
   }
}
