package org.infinitenature.werbeo.service.core.impl.obfusication.nopeople;

import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.EntityObfuscator;
import org.springframework.stereotype.Component;

@Component
public class NoPersonSampleObfuscator extends AbstractNoPersonObfuscator
      implements EntityObfuscator<Sample>
{
   @Override
   public Sample obfuscate(Sample sample, Context context)
   {
      if (isObfuscationNecessary(sample, context))
      {
         return doObfuscate(sample);
      }
      return sample;
   }

   private Sample doObfuscate(Sample sample)
   {
      sample.getOccurrences().forEach(
            occurrence -> ObfuscationUtils.obfuscatePeople(occurrence));
      ObfuscationUtils.obfuscatePeople(sample);
      ObfuscationUtils.obfuscatePeople(sample.getSurvey());
      return sample;
   }
}
