package org.infinitenature.werbeo.service.core.maps;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.geotools.referencing.CRS;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Language;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.StaticMapStyle;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTB;
import org.locationtech.jts.io.ParseException;
import org.opengis.referencing.FactoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TaxonMapsCreator
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TaxonMapsCreator.class);

   @Autowired
   private OccurrenceQueries queries;

   private PlotMap plotMap;
   private final int mapsFileWith = 600;
   private final int thumbsFileWith = 50;

   @Autowired
   private InstanceConfig instanceConfig;

   @Autowired
   private PortalQueries portalQueries;

   @Autowired
   private TaxonQueries taxonQueries;

   @Value("${werbeo.service.dir}")
   private String directory;

   private Set<SampleBase> findSampleBasesOfOccurrences(TaxonBase taxon,
         Context context) throws FactoryException, ParseException
   {
      Slice<Occurrence, OccurrenceSortField> occurrencesSlice = queries
            .findOccurrences(
                  new OccurrenceFilter(null, null, taxon.getId(), null, null,
                        false, null, null),
                  context, new OffsetRequestImpl<>(0, Integer.MAX_VALUE,
                        SortOrder.ASC, OccurrenceSortField.ID));

      List<Occurrence> occurrenceList = occurrencesSlice.getContent();
      Set<SampleBase> sampleBaseSet = new HashSet<>();
      for (Occurrence occurrence : occurrenceList)
      {
         sampleBaseSet.add(occurrence.getSample());
      }
      return sampleBaseSet;
   }

   private void createTaxonMap(Set<MTB> mtbs, int portalID, TaxonBase taxon)
         throws FactoryException, ParseException
   {
      plotMap.setMTBs(mtbs);
      plotMap.drawMTBLayer(1);
      File dir = new File(directory + "/" + portalID + "/maps/vegetweb-style/");
      dir.mkdirs();
      String taxonName = taxon.getName().replace("/", "");
      plotMap.saveImage(directory + "/" + portalID + "/maps/vegetweb-style/"
            + taxonName + ".png", mapsFileWith, true);

      plotMap.drawMTBLayer(12);
      plotMap.saveImage(directory + "/" + portalID + "/maps/vegetweb-style/"
            + taxonName + "_thumb.png", thumbsFileWith, false);

      plotMap.saveImage(directory + "/" + portalID + "/maps/vegetweb-style/"
            + taxonName + "_thumb_l.png", thumbsFileWith, false);
   }

   @Scheduled(cron = "0 0 0 * * *")
   public void updateMaps()
   {
      try
      {
         initPlotMap();
      } catch (Exception e)
      {
         LOGGER.error("Error initializing plotMap", e);
         return;
      }
      for (Portal portal : portalQueries.findAll())
      {
         if (instanceConfig.getStaticMapStyle().get(portal.getId()) != null
               && instanceConfig.getStaticMapStyle().get(portal.getId())
                     .equals(StaticMapStyle.VEGETWEB))
         {
            LOGGER.info("Update Taxon Maps for portal: {} - {}", portal.getId(),
                  portal.getTitle());
            try
            {
               createMaps(portal);
            } catch (Exception e)
            {
               LOGGER.error("Failure updateTaxon Maps for  portal: {} - {}",
                     portal.getId(), portal.getTitle(), e);
            }
         }
      }
   }

   private void createMaps(Portal portal)
   {
      TaxonFilter filter = new TaxonFilter("",
            new HashSet(Arrays.asList(Language.LAT)), true, null, true,
            new HashSet<String>(), null, false);
      for (TaxonBase taxon : taxonQueries.findBase(filter,
            new Context(
                  portal, User.INTERNAL_SUPER_USER, Collections.emptySet(),
                  this.getClass().getSimpleName() + "-" + UUID.randomUUID()),
            new OffsetRequestImpl<TaxonSortField>(0, 100000, SortOrder.ASC,
                  TaxonSortField.TAXON)))
      {
         try
         {
            createTaxonMap(
                  getCoveredMTBs(findSampleBasesOfOccurrences(taxon,
                        new Context(portal, User.INTERNAL_SUPER_USER,
                              Collections.emptySet(),
                              this.getClass().getSimpleName() + "-"
                                    + UUID.randomUUID()))),
                  portal.getId(), taxon);
         } catch (Exception e)
         {
            LOGGER.error("Error creating Map for:" + portal.getTitle() + ", "
                  + taxon.getName(), e);
         }
      }
   }

   private Set<MTB> getCoveredMTBs(Set<SampleBase> sampleBaseSet)
   {
      Set<MTB> mtbSet = new HashSet<>();
      for (SampleBase sample : sampleBaseSet)
      {
         mtbSet.add(sample.getLocality().getPosition().getMtb());
      }
      return mtbSet;
   }

   private void initPlotMap() throws FactoryException, IOException
   {
      plotMap = new PlotMap("Hoehenstufen Deutschland_WGS84.shp",
            "DE_HOEHEN.sld");
      plotMap.setCRS(CRS.decode("EPSG:900913"));
   }

   // for testing
   void setDirectory(String directory)
   {
      this.directory = directory;
   }
}
