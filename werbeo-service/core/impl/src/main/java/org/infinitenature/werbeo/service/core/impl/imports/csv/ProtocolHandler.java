package org.infinitenature.werbeo.service.core.impl.imports.csv;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface ProtocolHandler
{
   void writeProtocolLine(String line, ImportJob importJob, Context context);
}
