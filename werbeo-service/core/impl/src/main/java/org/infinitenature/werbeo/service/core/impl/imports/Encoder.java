package org.infinitenature.werbeo.service.core.impl.imports;

import java.util.*;

public class Encoder
{

   private static Map<String, String> replacements = new HashMap<String, String>();

   static
   {
      replacements.put("Ã¤", "ä");
      replacements.put("&auml", "ä");
      replacements.put("Ã¶", "ö");
      replacements.put("&Ouml", "ö");
      replacements.put("Ã¼", "ü");
      replacements.put("&uuml", "ü");
      replacements.put("ÃŸ", "ß");
      replacements.put("&szlig", "ß");
      replacements.put("Ã„", "Ä");
      replacements.put("&Auml", "Ä");
      replacements.put("Ã–", "Ö");
      replacements.put("&Ouml", "Ö");
      replacements.put("Ãœ", "Ü");
      replacements.put("&Uuml", "Ü");
      replacements.put("Ã©", "é");

      replacements.put("&amp;", "&");
      replacements.put("&lt;", "<");
      replacements.put("&gt;", ">");
      replacements.put("&quot;", "-");
   }

   public static String replace(String string)
   {
      for (String wrongChar : replacements.keySet())
      {
	 string = string.replace(wrongChar, replacements.get(wrongChar));
      }
      return string;
   }
}
