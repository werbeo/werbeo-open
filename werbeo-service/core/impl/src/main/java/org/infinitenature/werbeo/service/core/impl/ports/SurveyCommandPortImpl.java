package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceIndexCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.SurveyCommands;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.validation.ValidationContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SurveyCommandPortImpl implements SurveyCommandPort
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyCommandPortImpl.class);

   @Autowired
   private SurveyCommands surveyCommands;
   @Autowired
   private SurveyQueryPort surveyQueryPort;
   @Autowired
   private Validator validator;
   @Autowired
   private OccurrenceIndexCommands occurrenceIndexCommands;
   @Autowired
   private AccessRightsChecker accesRightsChecker;
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;

   @Override
   public Integer saveOrUpdate(Survey survey, Context context)
   {
      if (survey.getId() == null)
      {
         accesRightsChecker.isAllowedTo(Operation.CREATE, survey, context);
         return create(survey, context);
      } else
      {
         Survey persistedSurvey = surveyQueryPort.get(survey.getId(), context);
         accesRightsChecker.isAllowedTo(Operation.UPDATE, persistedSurvey,
               context);
         return update(survey, context);
      }

   }

   private Integer update(Survey survey, Context context)
   {
      LOGGER.info("Update survey {} for context {}", survey, context);
      validate(survey, context);
      int surveyId = surveyCommands.saveOrUpdate(survey, context);
      occurrenceIndexCommands.update(survey);
      return surveyId;
   }

   private void validate(Survey survey, Context context)
   {
      validateContainer(survey, context);

      ValidationContextHolder.setThreadLocalContext(context);

      Set<ConstraintViolation<Survey>> constraintViolations = validator
            .validate(survey);
      if (!constraintViolations.isEmpty())
      {
         LOGGER.error("Failure validating sample {}", constraintViolations);
         throw new ConstraintViolationException(constraintViolations);
      }

      ValidationContextHolder.removeThreadLocalContext();
   }

   private void validateContainer(
         Survey survey, Context context)
   {
      if (survey.getId() != null && survey.isContainer())
      {

         OccurrenceFilter filter = new OccurrenceFilter()
               .setSurveyId(survey.getId());
         boolean surveyHasOccurrences = occurrenceQueryPort.count(filter,
               context) > 0;
         if (surveyHasOccurrences)
         {

            throw new ValidationException(
                  "Container surveys may not include observation data");
         }
      }
   }

   private Integer create(Survey survey, Context context)
   {
      LOGGER.info("Creating survey {} for context {}", survey, context);
      validate(survey, context);
      return surveyCommands.saveOrUpdate(survey, context);
   }

   @Override
   public void delete(Integer surveyId, Context context)
   {
      Survey survey = surveyQueryPort.get(surveyId, context);
      accesRightsChecker.isAllowedTo(Operation.DELETE, survey, context);
      surveyCommands.delete(survey, context);
   }

   public void setSurveyCommands(SurveyCommands surveyCommands)
   {
      this.surveyCommands = surveyCommands;
   }


   public void setValidator(Validator validator)
   {
      this.validator = validator;
   }

   public void setOccurrenceIndexCommands(
         OccurrenceIndexCommands occurrenceIndexCommands)
   {
      this.occurrenceIndexCommands = occurrenceIndexCommands;
   }

   public void setAccesRightsChecker(AccessRightsChecker accesRightsChecker)
   {
      this.accesRightsChecker = accesRightsChecker;
   }

   public void setSurveyQueryPort(SurveyQueryPort surveyQueryPort)
   {
      this.surveyQueryPort = surveyQueryPort;
   }

   public void setOccurrenceQueryPort(OccurrenceQueryPort occurrenceQueryPort)
   {
      this.occurrenceQueryPort = occurrenceQueryPort;
   }
}
