package org.infinitenature.werbeo.service.core.impl.obfusication.position;

import java.util.List;

import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enums.Permission;
import org.infinitenature.werbeo.service.core.api.error.InternalException;
import org.infinitenature.werbeo.service.core.api.ports.SurveyQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.EntityObfuscator;
import org.infinitenature.werbeo.service.core.impl.obfusication.nopeople.ObfuscationUtils;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.springframework.stereotype.Component;

import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;

@Component
public class PositionOccurrenceCentroidObfuscator extends AbstractPositionObfuscator
      implements EntityObfuscator<OccurrenceCentroid>
{
   private final SurveyQueryPort surveyQueries;
   private final GeometryHelper geometryHelper = new GeometryHelper();
   private final CoordinateTransformer coordinateTransformer;

   public PositionOccurrenceCentroidObfuscator(PositionFactory positionFactory,
         SurveyQueryPort surveyQueries,
         CoordinateTransformerFactory coordinateTransformerFactory)
   {
      super(positionFactory);
      this.surveyQueries = surveyQueries;
      this.coordinateTransformer = coordinateTransformerFactory
            .getCoordinateTransformer(MTB.getEpsgNumber(), 31468);
   }

   @Override
   public OccurrenceCentroid obfuscate(OccurrenceCentroid entity,
         Context context)
   {
      Survey survey = surveyQueries.get(entity.getSurveyId(), context);
      
      List<Permission> permissions = ObfuscationUtils.getObfuscationPermissions(survey, context);
      
      if (context.isAdmin()
            || permissions.contains(Permission.LOCATION_POINT))
      {
         // Admin  or configured permission, do not obfuscate
         return entity;
      }
      else
      {
         int quadrant = ObfuscationUtils.getPermittedQuadrant(permissions);
         pointToMTBToPoint(entity, quadrant);
         return entity;
      }
   }

   private void pointToMTBToPoint(OccurrenceCentroid entity, int quadrant)
   {
      Position position = positionFactory.create(entity.getPoint().toText(),
            31468);
      
      int mtbEndIndex = quadrant == 0 ? 4 : (5 + quadrant);
      
      MTB mtb = MTBHelper.toMTB(position.getMtb().getMtb().substring(0, mtbEndIndex));
      String centerWkt = mtb.getCenterWkt();
      Point point;
      try
      {
         point = (Point) geometryHelper.parseToJts(centerWkt,
               MTB.getEpsgNumber());
         Point projected = (Point) coordinateTransformer.convert(point);
         entity.setPoint(projected);
      } catch (ParseException e)
      {
         throw new InternalException(
               "Failure obfuscating occurrence centroid " + entity, e);
      }
   }

}
