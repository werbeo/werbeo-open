package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.infinitenature.werbeo.service.core.api.ports.PersonQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonQueryPortImpl implements PersonQueryPort
{
   @Autowired
   private PersonQueries personQueries;
   @Autowired
   private AccessRightsChecker accessRightsChecker;

   @Override
   public Person get(Integer id, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Person.class, context);
      return personQueries.get(id, context);
   }

   @Override
   public Slice<Person, PersonSortField> find(PersonFilter filter,
         Context context, OffsetRequest<PersonSortField> offsetRequest)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Person.class, context);
      return personQueries.find(filter, context, offsetRequest);
   }

   @Override
   public long count(PersonFilter filter, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Person.class, context);
      return personQueries.count(filter, context);
   }

}
