package org.infinitenature.werbeo.service.core.impl.meta;

import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.TaxonConfig;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.ports.MetaInfoQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MetaInfoQueryPortImpl implements MetaInfoQueryPort
{
   @Autowired
   private FieldConfigQueries fieldConfigQueries;

   @Override
   public SurveyConfig getSurveyConfig(Context context)
   {
      return fieldConfigQueries.getSurveyConfig(context.getPortal());
   }

   @Override
   public OccurrenceConfig getOccurrenceConfig(Context context)
   {
      return fieldConfigQueries.getOccurrenceConfig(context.getPortal());
   }

   @Override
   public SampleConfig getSampleConfig(Context context)
   {
      return fieldConfigQueries.getSampleConfig(context.getPortal());
   }

   @Override
   public TaxonConfig getTaxonConfig(Context context)
   {
      return fieldConfigQueries.getTaxonConfig(context.getPortal());
   }
}
