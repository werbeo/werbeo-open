package org.infinitenature.werbeo.service.core.impl.validation;

import javax.measure.MetricPrefix;
import javax.measure.Quantity;
import javax.measure.Unit;
import javax.measure.quantity.Area;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.validation.PolygonMaxSizeValidator;
import org.locationtech.jts.algorithm.Orientation;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import si.uom.SI;
import tech.units.indriya.quantity.Quantities;
import tech.units.indriya.unit.ProductUnit;

@Component
@Scope("prototype")
public class PolygonMaxSizeValidatorImpl implements PolygonMaxSizeValidator
{
   private final GeometryHelper geometryHelper = new GeometryHelper();
   public static final Unit<Area> SQUARE_KILOMETER = (Unit<Area>) ProductUnit
         .ofPow(MetricPrefix.KILO(SI.METRE), 2);
   private final double maxSize;
   @Autowired
   public PolygonMaxSizeValidatorImpl(InstanceConfig instanceConfig)
   {
      this.maxSize = instanceConfig.getAreaFilterMaxSize();
   }

   @Override
   public boolean isPolygonSizeValid(String value)
   {
      try
      {
         Geometry geometry = geometryHelper.parseToJts(value, 4326);
         if (geometry instanceof Polygon)
         {
            Polygon p = (Polygon) geometry;

            // We assume that counter clock wise polygons are far too big
            if (!Orientation.isCCW(p.getExteriorRing().getCoordinates()))
            {
               return false;
            }
            Point centroid = p.getCentroid();

            String code = "AUTO:42001," + centroid.getX() + ","
                  + centroid.getY();
            CoordinateReferenceSystem auto = CRS.decode(code);

            MathTransform transform = CRS
                  .findMathTransform(DefaultGeographicCRS.WGS84, auto);

            Polygon projed = (Polygon) JTS.transform(p, transform);
            Quantity<Area> measureM2 = Quantities.getQuantity(projed.getArea(),
                  SI.SQUARE_METRE);

            return measureM2.to(SQUARE_KILOMETER).getValue()
                  .doubleValue() <= maxSize;
         } else
         {
            return true;
         }
      } catch (Exception e)
      {
         return true;
      }
   }

}
