package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.NumericAmountAccuracy;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Quantity;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.togglz.core.manager.FeatureManager;

@Component
public class LegacyQuantitySync
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(LegacyQuantitySync.class);
   @Autowired
   private FeatureManager featureManager;
   @Autowired
   private FieldConfigQueries fieldConfigQueries;

   public void syncQuantiyFieldsOnSave(Sample sample, Context context)
   {
      if (featureManager.isActive(WerBeoFeatures.LEGACY_QUANTITY_SUPPORT))
      {
         syncQuantiyFields(sample, context);
      }
   }

   private void syncQuantiyFields(Sample sample, Context context)
   {
      OccurrenceConfig occurrenceConfig = fieldConfigQueries
            .getOccurrenceConfig(context.getPortal());
      if (occurrenceConfig
            .getFieldConfig(OccurrenceField.NUMERIC_AMOUNT_ACCURACY).isPresent()
            && occurrenceConfig.getFieldConfig(OccurrenceField.NUMERIC_AMOUNT)
                  .isPresent()
            && occurrenceConfig.getFieldConfig(OccurrenceField.QUANTITY)
                  .isPresent())
      {
         sample.getOccurrences().forEach(this::syncQuantiyFields);
      }
   }

   private void syncQuantiyFields(Occurrence occurrence)
   {
      setNumericAmount(occurrence);
      occurrence.setQuantity(null);
   }

   void setQuantiy(Occurrence occurrence)
   {
      int numericAmount = occurrence.getNumericAmount();
      if (numericAmount <= 5)
      {
         occurrence.setQuantity(Quantity.ONE_TO_FIVE);
      } else if (numericAmount <= 25)
      {
         occurrence.setQuantity(Quantity.SIX_TO_TWENTYFIVE);
      } else if (numericAmount <= 50)
      {
         occurrence.setQuantity(Quantity.TWENTYSIX);
      } else if (numericAmount > 50)
      {
         occurrence.setQuantity(Quantity.FIFTY);
      } else if (numericAmount > 100)
      {
         occurrence.setQuantity(Quantity.HUNDRED);
      } else if (numericAmount > 1000)
      {
         occurrence.setQuantity(Quantity.THOUSAND);
      } else if (numericAmount > 10000)
      {
         occurrence.setQuantity(Quantity.TENTHOUSAND);
      }
   }

   void setNumericAmount(Occurrence occurrence)
   {
      if (occurrence.getQuantity() != null
            && occurrence.getNumericAmount() == null
            && occurrence.getNumericAmountAccuracy() == null)
      {
         switch (occurrence.getQuantity())
         {
         case ONE_TO_FIVE:
            occurrence.setNumericAmount(3);
            occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.APPROXIMATE);
            break;
         case SIX_TO_TWENTYFIVE:
            occurrence.setNumericAmount(15);
            occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.APPROXIMATE);
            break;
         case TWENTYSIX:
            occurrence.setNumericAmount(30);
            occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.APPROXIMATE);
            break;
         case FIFTY:
            occurrence.setNumericAmount(50);
            occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.MORETHAN);
            break;
         case HUNDRED:
            occurrence.setNumericAmount(100);
            occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.MORETHAN);
            break;
         case THOUSAND:
            occurrence.setNumericAmount(1000);
            occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.MORETHAN);
            break;
         case TENTHOUSAND:
            occurrence.setNumericAmount(10000);
            occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.MORETHAN);
            break;
         default:
            LOGGER.warn("Unsupported Quantiy value {}",
                  occurrence.getQuantity());
            break;
         }
      }
   }

   public void syncQuantiyFieldsOnLoad(Sample sample, Context context)
   {
      sample.getOccurrences()
            .forEach(occ -> syncQuantiyFieldsOnLoad(occ, context));
   }

   public void syncQuantiyFieldsOnLoad(Occurrence occurrence, Context context)
   {
      if (featureManager.isActive(WerBeoFeatures.LEGACY_QUANTITY_SUPPORT))
      {
         if (occurrence.getQuantity() != null
               && occurrence.getNumericAmount() == null)
         {
            setNumericAmount(occurrence);
         } else if (occurrence.getNumericAmount() != null)
         {
            setQuantiy(occurrence);
         }
      }
   }
}
