package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.query.PrivacyQueries;
import org.infinitenature.werbeo.service.core.api.ports.PrivacyQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrivacyQueryPortImpl implements PrivacyQueryPort
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PrivacyQueryPortImpl.class);

   @Autowired
   private PrivacyQueries queries;

   @Override
   public String getPrivacyDeclaration(Context context)
   {
      try
      {
         return queries.getPrivacyDeclaration(context.getPortal());
      } catch (EntityNotFoundException e)
      {
         LOGGER.warn(
               "No privacy declaration found for portal {},\n\tuseing default ones",
               context.getPortal());
         return queries.getDefaultPrivacyDeclaration();
      }
   }
}
