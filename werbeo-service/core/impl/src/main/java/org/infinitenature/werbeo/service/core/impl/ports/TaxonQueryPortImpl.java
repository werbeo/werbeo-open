package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.error.ValidationException;
import org.infinitenature.werbeo.service.core.api.infra.query.FileQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxonQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.ports.taxon.TaxonCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaxonQueryPortImpl implements TaxonQueryPort
{
   @Autowired
   private TaxonQueries queries;

   @Autowired
   private FileQueries fileQueries;

   @Autowired
   private TaxonCache taxonCache;

   @Autowired
   private InstanceConfig instanceConfig;

   @Override
   public Taxon get(Integer id, Context context)
   {
      Taxon taxon =  queries.get(id, context);
      taxon.setDocumentsAvailable(fileQueries.getDocumentsAvailable(taxon, context));
      return taxon;
   }

   private void validateTaxonGroups(TaxonFilter filter, Context context)
   {
      Set<String> configuredGroups = instanceConfig.getTaxonGroupsPerPortal()
            .getOrDefault(context.getPortal().getId(), Collections.emptySet());
      if (configuredGroups.isEmpty())
      {
         return;
      } else if (StringUtils.isBlank(filter.getTaxaGroup()))
      {
         return;
      } else if (configuredGroups.contains(filter.getTaxaGroup()))
      {
         return;
      } else
      {
         throw new ValidationException(
               filter.getTaxaGroup()
                     + " is not a configured taxa group. Available groups are: "
                     + StringUtils.join(configuredGroups, ", "),
               new HashSet<>());
      }
   }

   @Override
   public Slice<TaxonBase, TaxonSortField> findBase(TaxonFilter filter,
         Context context,
         OffsetRequest<TaxonSortField> offsetRequest)
   {
      validateTaxonGroups(filter, context);
      // Taxon-Cache does not 
      if(instanceConfig.isUseTaxonCache() && filter.getOccurrencesChangedSince() == null)
      {
         return taxonCache.find(filter, context.getPortal().getId(), offsetRequest);
      }
      return queries.findBase(filter, context, offsetRequest);
   }


   @Override
   public List<Taxon> findAllSynonyms(Integer id, Context context)
   {
      return queries.findAllSynonyms(id, context);
   }

   @Override
   public long count(TaxonFilter filter, Context context)
   {
      validateTaxonGroups(filter, context);
      if(instanceConfig.isUseTaxonCache() && filter.getOccurrencesChangedSince() == null)
      {
         return taxonCache.count(filter, context.getPortal().getId());
      }
      return queries.count(filter, context);
   }

   @Override
   public Slice<Taxon, TaxonSortField> find(TaxonFilter filter, Context context,
         OffsetRequest<TaxonSortField> offsetRequest)
   {
      validateTaxonGroups(filter, context);
      return queries.find(filter, context, offsetRequest);
   }

   @Override
   public TaxonBase getByExternalId(TaxaList list, String externalId,
         Context context)
   {
      return queries.getByExternalId(list.getId(), externalId);
   }
}
