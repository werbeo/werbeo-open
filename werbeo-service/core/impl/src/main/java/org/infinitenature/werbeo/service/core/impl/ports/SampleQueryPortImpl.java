package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.Optional;
import java.util.UUID;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.query.CommentQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.SampleQueries;
import org.infinitenature.werbeo.service.core.api.ports.SampleQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.Obfusicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampleQueryPortImpl implements SampleQueryPort
{
   @Autowired
   private SampleQueries queries;
   @Autowired
   private CommentQueries commentQueries;
   @Autowired
   private AccessRightsChecker accessRightsChecker;
   @Autowired
   private Obfusicator obfusicator;
   @Autowired
   private LegacyQuantitySync legacyQuantiySync;
   @Override
   public Sample get(UUID id, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Sample.class, context);
      Optional<Sample> optional = queries.get(id, context);
      if (optional.isPresent())
      {
         Sample sample = optional.get();
         legacyQuantiySync.syncQuantiyFieldsOnLoad(sample, context);
         accessRightsChecker.addAllowedOperations(sample, context);
         obfusicator.obfuscate(sample, context);

         return sample;
      } else
      {
         throw new EntityNotFoundException(
               "Sample with uuid " + id + " not found");
      }
   }

   @Override
   public Slice<SampleComment, CommentSortField> getComments(UUID sampleId,
         Context context, OffsetRequest<CommentSortField> offsetRequest)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Sample.class, context);
      return commentQueries.findSampleComments(sampleId, context,
            offsetRequest);
   }

   @Override
   public long countComments(UUID sampleId, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Sample.class, context);
      return commentQueries.countSampleComments(sampleId, context);
   }
}
