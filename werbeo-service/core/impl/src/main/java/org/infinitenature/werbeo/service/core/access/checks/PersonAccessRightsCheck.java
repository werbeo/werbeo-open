package org.infinitenature.werbeo.service.core.access.checks;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.stereotype.Component;

@Component
public class PersonAccessRightsCheck
      extends SimpleAccessRightCheck<Person>
{
   public PersonAccessRightsCheck()
   {
      super(Person.class);
   }

   @Override
   protected boolean isAllowedToCreate(Person entity, Context context)
   {
      return context.isUserLoggedIn();
   }

   @Override
   protected boolean isAllowedToFind(Class<? extends Person> type,
         Context context)
   {
      return context.isUserLoggedIn();
   }

   @Override
   protected Set<Operation> operationForOwner()
   {
      return new HashSet<>(Arrays.asList(Operation.READ));
   }

}
