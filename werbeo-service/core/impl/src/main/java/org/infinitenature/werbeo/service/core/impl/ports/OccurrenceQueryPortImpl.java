package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.common.logging.Logger;
import org.infinitenature.werbeo.service.common.logging.LoggerFactory;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.StreamSlice;
import org.infinitenature.werbeo.service.core.api.error.ValidationException;
import org.infinitenature.werbeo.service.core.api.infra.query.CommentQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceIndexQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.Obfusicator;
import org.infinitenature.werbeo.service.core.impl.roles.RoleFilterFactory;
import org.infintenature.mtb.MTB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.togglz.core.manager.FeatureManager;

@Service
public class OccurrenceQueryPortImpl implements OccurrenceQueryPort
{
   private static Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceQueryPortImpl.class);

   @Autowired
   private OccurrenceQueries occurrenceQueries;
   @Autowired
   private AccessRightsChecker accessRightsChecker;
   @Autowired(required = false)
   private OccurrenceIndexQueries occurrenceIndexQueries;
   @Autowired
   private FeatureManager featureManager;
   @Autowired
   private RoleFilterFactory roleFilterFactory;
   @Autowired
   private CommentQueries commentQueries;
   @Autowired
   private Obfusicator obfusicator;
   @Autowired
   private Validator validator;
   @Autowired
   private LegacyQuantitySync legacyQuantitySync;

   @Override
   public Slice<Occurrence, OccurrenceSortField> find(OccurrenceFilter filter,
         Context context, OffsetRequest<OccurrenceSortField> offsetRequest)
   {
      validate(filter, context);
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      filter = applayPortalFilters(filter, context);
      Slice<Occurrence, OccurrenceSortField> result;
      if (useIndex(context, filter))
      {
         LOGGER.info(context, "Using index to find occurrences");
         result = occurrenceIndexQueries.findOccurrences(filter, context,
               offsetRequest);
      } else
      {
         LOGGER.info(context, "Not using index to find occurrences");
         result = occurrenceQueries.findOccurrences(filter, context,
               offsetRequest);
      }
      result.forEach(
            occ -> legacyQuantitySync.syncQuantiyFieldsOnLoad(occ, context));
      accessRightsChecker.addAllowedOperations(result, context);
      obfusicator.obfuscateOccurrences(result, context);
      LOGGER.info(context, "Returning {} occurrences",
            result.getContent().size());
      return result;
   }

   private boolean requestSizeExceeded(OccurrenceFilter filter)
   {
      return StringUtils.isNotBlank(filter.getWkt()) && filter.getWkt().length() > 100;
   }

   @Override
   public Set<MTB> getCoveredMTB(int taxonId, boolean includeChildTaxa,
         Context context)
   {
      return occurrenceIndexQueries.getCoveredMTB(taxonId, includeChildTaxa,
            context);
   }

   private boolean useIndex(Context context, OccurrenceFilter filter)
   {
      return featureManager.isActive(WerBeoFeatures.USE_OCCURRENCE_INDEX) && !requestSizeExceeded(filter);
   }

   @Override
   public StreamSlice<Occurrence, UUID> find(Context context,
         ContinuationToken continuationToken)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      StreamSlice<Occurrence, UUID> result = occurrenceQueries
            .streamOccurrences(context, continuationToken);
      result.getEntities().forEach(
            occ -> legacyQuantitySync.syncQuantiyFieldsOnLoad(occ, context));
      return result;
   }

   @Override
   public long count(OccurrenceFilter filter, Context context)
   {
      validate(filter, context);
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      filter = applayPortalFilters(filter, context);
      long result;
      if (useIndex(context, filter))
      {
         LOGGER.info(context, "Using index to count occurrences");
         result = occurrenceIndexQueries.countOccurrences(filter, context);
      } else
      {
         LOGGER.info(context, "Not using index to count occurrences");
         result = occurrenceQueries.countOccurrences(filter, context);
      }
      LOGGER.info(context, "Counted {} occurrences", result);
      return result;
   }

   private void validate(OccurrenceFilter occurrenceFilter, Context context)
   {
      Set<ConstraintViolation<OccurrenceFilter>> constraintViolations = validator
            .validate(occurrenceFilter);
      if (!constraintViolations.isEmpty())
      {
         LOGGER.error(context, "Failure validating occurrence filter {}",
               constraintViolations);
         throw new ValidationException("Failure validating occurrence filter",
               constraintViolations);
      }
   }

   @Override
   public Occurrence get(UUID uuid, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      Occurrence occurrence = occurrenceQueries.get(uuid, context);
      accessRightsChecker.addAllowedOperations(occurrence, context);
      obfusicator.obfuscate(occurrence, context);
      return occurrence;
   }

   @Override
   public Slice<OccurrenceCentroid, OccurrenceCentroidSortField> findOccurrenceCentroids(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceCentroidSortField> offsetRequest)
   {
      validate(filter, context);
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      filter = applayPortalFilters(filter, context);
      Slice<OccurrenceCentroid, OccurrenceCentroidSortField> result = occurrenceQueries
            .findOccurrenceCentroids(filter, context, offsetRequest);
      obfusicator.obfuscateOccurrenceCentroids(result, context);
      return result;
   }

   @Override
   public long countOccurrenceCentroids(OccurrenceFilter filter,
         Context context)
   {
      validate(filter, context);
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      filter = applayPortalFilters(filter, context);
      return occurrenceQueries.countOcurrenceCentroids(filter, context);
   }

   private OccurrenceFilter applayPortalFilters(OccurrenceFilter filter,
         Context context)
   {
      Map<Filter, Collection<Portal>> activeFilter = roleFilterFactory
            .findActiveFilter(context);
      LOGGER.info(context, "Add filter: {}", activeFilter);
      filter.setPortalBasedFilters(activeFilter);
      return filter;
   }

   @Override
   public Map<Filter, Collection<Portal>> getFilter(Context context)
   {
      return roleFilterFactory.findActiveFilter(context);
   }

   @Override
   public Slice<OccurrenceComment, CommentSortField> getComments(
         UUID occurrenceId, Context context,
         OffsetRequest<CommentSortField> offsetRequest)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      return commentQueries.findOccurrenceComments(occurrenceId, context,
            offsetRequest);
   }

   @Override
   public long countComments(UUID occurrenceId, Context context)
   {
      accessRightsChecker.isAllowedTo(Operation.READ, Occurrence.class,
            context);
      return commentQueries.countOccurrenceComments(occurrenceId, context);
   }

   @Override
   public List<Occurrence> findBySampleId(UUID uuid, Context context)
   {
      List<Occurrence> occurrences = occurrenceIndexQueries.findBySampleId(uuid,
            context);
      occurrences
            .forEach(occurrence -> obfusicator.obfuscate(occurrence, context));
      occurrences.forEach(
            occ -> legacyQuantitySync.syncQuantiyFieldsOnLoad(occ, context));
      return occurrences;
   }
}
