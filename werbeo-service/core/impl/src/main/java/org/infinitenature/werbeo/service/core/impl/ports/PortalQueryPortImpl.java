package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.StaticMapStyle;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.ports.PortalFilter;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortalQueryPortImpl implements PortalQueryPort
{

   private InstanceConfig instanceConfig;

   private PortalQueries portalQueries;

   public PortalQueryPortImpl(@Autowired InstanceConfig instanceConfig,
         @Autowired PortalQueries portalQueries)
   {
      super();
      this.instanceConfig = instanceConfig;
      this.portalQueries = portalQueries;
   }

   @Override
   public Portal load(int portalId)
   {
      return portalQueries.load(portalId);
   }

   @Override
   public Slice<Portal, PortalSortField> findAssociated(Context context)
   {
      return portalQueries.findAssociated(context.getPortal().getId());
   }

   @Override
   public PortalConfiguration loadConfig(int portalId)
   {
      portalQueries.load(portalId); // throws exception if portal does not
      // exists.

      PortalConfiguration portalConfiguration = new PortalConfiguration();

      portalConfiguration.setDefaultDataEntrySurveyId(
            instanceConfig.getDefaultDataEntrySurveys().get(portalId));

      portalConfiguration.setMapInitialZoom(
            instanceConfig.getMapInitialZoom().get(portalId));
      portalConfiguration.setMapInitialLatitude(
            instanceConfig.getMapInitialLatitude().get(portalId));
      portalConfiguration.setMapInitialLongitude(
            instanceConfig.getMapInitialLongitude().get(portalId));
      portalConfiguration.setMapOverlayLayers(
            instanceConfig.getMapOverlayLayersPerPortal().get(portalId));
      portalConfiguration.setMaxUploadSize(
            instanceConfig.getMaxUploadSize().getOrDefault(portalId, 3));
      portalConfiguration
            .setDefaultDataEntrySurveyId(
                  instanceConfig.getDefaultDataEntrySurveys().get(portalId));
      portalConfiguration.setStaticMapStyle(instanceConfig.getStaticMapStyle()
            .getOrDefault(portalId, StaticMapStyle.FLORA_MV));

      portalConfiguration.setObfuscationPolicies(
            instanceConfig.getObfuscationPoliciesPerPortal().get(portalId));
      portalConfiguration.setExportPolicies(
            instanceConfig.getExportPoliciesPerPortal().get(portalId));
      portalConfiguration.setAllowAnonymousAccess(
            instanceConfig.getAnonymousAccessAllowedPerPortal()
                  .getOrDefault(portalId, false));
      portalConfiguration.setCoordinateSystems(
            (instanceConfig.getCoordinateSystemsPerPortal().get(portalId)));

      return portalConfiguration;
   }

   
   @Override
   public Slice<Portal, PortalSortField> find(PortalFilter filter,
         Context context, OffsetRequest<PortalSortField> offsetRequest)
   {
      return portalQueries.find(filter, offsetRequest);
   }

   @Override
   public long count(PortalFilter filter, Context context)
   {
      return portalQueries.count(filter);
   }

   @Override
   public Portal get(Integer id, Context context)
   {
      return portalQueries.load(id);
   }
}
