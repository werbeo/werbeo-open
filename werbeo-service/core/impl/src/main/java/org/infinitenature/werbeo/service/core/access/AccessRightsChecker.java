package org.infinitenature.werbeo.service.core.access;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.error.NotEnoughtRightsException;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessRightsChecker
{
   private final List<AccessRightsCheck<BaseType<?>>> checks;

   @SuppressWarnings({ "rawtypes", "unchecked" })
   public AccessRightsChecker(
         @Autowired List<AccessRightsCheck> accessRightsChecks)
   {
      // Hack to satisfy spring autowire by my not so great generics knowledge
      List<AccessRightsCheck<BaseType<?>>> checks = new ArrayList<>();
      for (AccessRightsCheck c : accessRightsChecks)
      {
         checks.add(c);
      }
      this.checks = Collections.unmodifiableList(checks);
   }

   public void isAllowedTo(Operation operation, BaseType<?> entity,
         Context context)
   {
      boolean operationIsAllowed = checks.stream()
            .filter(check -> check.isResponsibleFor(entity.getClass(), context))
            .map(check -> check.isAllowedTo(operation, entity, context))
            .filter(isAllowed -> isAllowed.equals(false)).count() == 0;

      if (!operationIsAllowed)
      {
         throw new NotEnoughtRightsException("Operation " + operation
               + " on entity " + entity.getClass().getSimpleName() + " with id "
               + entity.getId() + " is not allowed.");
      }
   }

   public void isAllowedTo(Operation operation,
         Class<? extends BaseType<?>> type, Context context)
   {
      boolean operationIsAllowed = checks.stream()
            .filter(check -> check.isResponsibleFor(type, context))
            .map(check -> check.isAllowedTo(operation, type, context))
            .filter(isAllowed -> isAllowed.equals(false)).count() == 0;

      if (!operationIsAllowed)
      {
         throw new NotEnoughtRightsException("Operation " + operation
               + " on entity " + type + " is not allowed.");
      }
   }

   public void addAllowedOperations(Slice<? extends BaseType<?>, ?> entites,
         Context context)
   {
      entites.getContent()
            .forEach(entity -> addAllowedOperations(entity, context));
   }

   public void addAllowedOperations(BaseType<?> entity, Context context)
   {
      Set<Operation> operations = new HashSet<>(
            Arrays.asList(Operation.values()));

      checks.stream()
            .filter(check -> check.isResponsibleFor(entity.getClass(), context))
            .map(check -> check.getAllowedOperations(entity, context))
            .forEach(ops -> operations.retainAll(ops));
      entity.setAllowedOperations(new HashSet<>(operations));

      // We need the allowed operation on the sample, because obfuscation works
      // on this
      if (entity instanceof Occurrence)
      {
         SampleBase sample = ((Occurrence) entity).getSample();
         addAllowedOperations(sample, context);
      }

   }
}
