package org.infinitenature.werbeo.service.core.impl.ports;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.Export;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceExportSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceExportQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.export.ExportFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class OccurrenceExportQueryPortImpl implements OccurrenceExportQueryPort
{
   @Autowired
   private ExportFacade exportFacade;

   @Override
   public Export get(UUID exportId, Context context)
   {
      return exportFacade.getStatus(exportId, context);
   }

   @Override
   public TypedFile load(UUID exportId, Context context)
   {
      return exportFacade.getFile(exportId, context);
   }

   @Override
   public Slice<Export, OccurrenceExportSortField> find(Void filter,
         Context context,
         OffsetRequest<OccurrenceExportSortField> offsetRequest)
   {
      return exportFacade.findExports(context, offsetRequest);
   }

   @Override
   public long count(Void filter, Context context)
   {
      return exportFacade.coutExports(context);
   }
}
