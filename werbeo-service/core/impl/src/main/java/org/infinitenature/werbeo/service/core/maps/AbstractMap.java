package org.infinitenature.werbeo.service.core.maps;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.renderer.GTRenderer;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.xml.styling.SLDParser;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract super class for creating maps.
 *
 * @author dve
 */
public abstract class AbstractMap
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(AbstractMap.class);
   private final StyleFactory styleFactory = CommonFactoryFinder
         .getStyleFactory();
   protected final MapContent map = new MapContent();
   protected final Layer background;

   public AbstractMap(String shapeName, String styleName) throws IOException
   {
      // making background layer
      URL backgroundURL = this.getClass().getResource(shapeName);
      FileDataStore store = FileDataStoreFinder.getDataStore(backgroundURL);
      SimpleFeatureSource featureSource = store.getFeatureSource();

      InputStream inputStream = this.getClass().getResourceAsStream(styleName);
      Style style = createFromSLD(inputStream);
      background = new FeatureLayer(featureSource, style);
      LOGGER.debug("background layer bounds: {}", background.getBounds());
      map.addLayer(background);
   }

   public void setCRS(CoordinateReferenceSystem newCRS)
   {
      map.getViewport().setCoordinateReferenceSystem(newCRS);
   }

   public void setTitle(String title)
   {
      map.setTitle(title);
   }

   /**
    * Saves the map to an image. <b>After this operation the object can not be
    * used anymore!<b/>
    *
    * @param file
    * @param imageWidth
    */
   public void saveImage(final String file, final int imageWidth, boolean includeTimestamp)
   {
      LOGGER.trace("Entering saveImage({},{})", file, imageWidth);
      GTRenderer renderer = new StreamingRenderer();
      renderer.setMapContent(map);

      Rectangle imageBounds = null;
      ReferencedEnvelope mapBounds = null;
      try
      {
         mapBounds = map.getMaxBounds();
         double heightToWidth = mapBounds.getSpan(1) / mapBounds.getSpan(0);
         imageBounds = new Rectangle(0, 0, imageWidth,
               (int) Math.round(imageWidth * heightToWidth));

      } catch (Exception e)
      {
         LOGGER.debug("Failure saving map file {} with width {}", file,
               imageWidth, e);
         // failed to access map layers
         throw new RuntimeException(e);
      }

      BufferedImage image = new BufferedImage(imageBounds.width,
            imageBounds.height, BufferedImage.TYPE_INT_RGB);
      LOGGER.trace("Created BufferedImage");
      Graphics2D gr = image.createGraphics();
      RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
      gr.setRenderingHints(rh);
      gr.setPaint(Color.WHITE);
      gr.fill(imageBounds);
      if(includeTimestamp)
      {
         gr.setColor(Color.BLACK);
         gr.setFont(new Font("Arial", Font.PLAIN, 15));
         gr.drawString(new SimpleDateFormat("dd-MM-yyyy").format(new Date()), 10, 25);
      }
      try
      {
         renderer.paint(gr, imageBounds, mapBounds);
         LOGGER.trace("Painted");
         File fileToSave = new File(file);
         LOGGER.trace("Created file object");
         ImageIO.write(image, "png", fileToSave);
         LOGGER.debug("Saved image {}", fileToSave);

      } catch (IOException e)
      {
         LOGGER.error("Failure saving map file {} with width {}", file,
               imageWidth, e);
         throw new RuntimeException(e);
      }
   }

   /**
    * This needs to be called before garbage collection is happening to prevent
    * memory leaks.
    */
   public void dispose()
   {
      map.dispose();
   }

   /**
    * Create a Style object from a definition in a SLD document
    */
   protected Style createFromSLD(InputStream sld)
   {
      try
      {
         SLDParser stylereader = new SLDParser(styleFactory, sld);
         Style[] style = stylereader.readXML();
         return style[0];

      } catch (Exception e)
      {
         LOGGER.error("Failure reading style", e);
      }
      return null;
   }

   public MapContent getMap()
   {
      return map;
   }
}
