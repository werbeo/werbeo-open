package org.infinitenature.werbeo.service.core.access;

import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.support.Context;

public interface AccessRightsCheck<T extends BaseType<?>>
{
   boolean isResponsibleFor(Class<?> type, Context context);

   boolean isAllowedTo(Operation operation, T entity, Context context);

   boolean isAllowedTo(Operation operation, Class<? extends T> type,
         Context context);

   Set<Operation> getAllowedOperations(T entity, Context context);
}
