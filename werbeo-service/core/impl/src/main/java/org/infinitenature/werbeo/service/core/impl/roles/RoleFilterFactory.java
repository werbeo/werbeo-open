package org.infinitenature.werbeo.service.core.impl.roles;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.service.core.api.enity.Group;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.infra.query.FilterQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.infra.roles.RoleFilter;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoleFilterFactory
{
   private final FilterQueries filterQueries;
   private final PortalQueries portalQueries;

   public RoleFilterFactory(@Autowired FilterQueries filterQueries,
         @Autowired PortalQueries portalQueries)
   {
      this.filterQueries = filterQueries;
      this.portalQueries = portalQueries;
   }

   public Map<Filter, Collection<Portal>> findActiveFilter(Context context)
   {
      List<Portal> portals = portalQueries
            .findAssociated(context.getPortal().getId()).getContent();
      return filterQueries.getFilters(portals).stream()
            .filter(roleFilter -> isActive(roleFilter, context))
            .collect(new RoleFilterCollector());
   }

   protected boolean isActive(RoleFilter roleFilter, Context context)
   {
      Group missingActivatingGroup = new Group(
            RoleNameFactory.getKeycloakRoleName(roleFilter.getPortal().getId(),
                  roleFilter.getMissingActivatingRole()));
      return !context.getGroups().contains(missingActivatingGroup);
   }
}
