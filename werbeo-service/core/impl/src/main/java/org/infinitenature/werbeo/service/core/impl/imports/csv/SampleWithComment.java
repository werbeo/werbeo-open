package org.infinitenature.werbeo.service.core.impl.imports.csv;

import org.infinitenature.werbeo.service.core.api.enity.Sample;

public class SampleWithComment
{
   public SampleWithComment(Sample sample, String comment)
   {
      super();
      this.sample = sample;
      this.comment = comment;
   }
   private Sample sample;
   private String comment;
   public String getComment()
   {
      return comment;
   }
   public void setComment(String comment)
   {
      this.comment = comment;
   }
   public Sample getSample()
   {
      return sample;
   }
   public void setSample(Sample sample)
   {
      this.sample = sample;
   }
}
