package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.UUID;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.core.api.enity.CSVImport;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceImportSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceCSVImportQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.imports.csv.ImportFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceCSVImportQueryPortImpl implements OccurrenceCSVImportQueryPort
{
   @Autowired
   private ImportFacade importFacade;

   @Override
   public CSVImport get(UUID importId, Context context)
   {
      return importFacade.getStatus(importId, context);
   }

   @Override
   public TypedFile load(UUID importId, Context context)
   {
      return importFacade.getFile(importId, context);
   }
   
   @Override
   public TypedFile loadErrorLog(UUID importId, Context context)
   {
      return importFacade.getErrorFile(importId, context);
   }

   @Override
   public Slice<CSVImport, OccurrenceImportSortField> find(Void filter,
         Context context,
         OffsetRequest<OccurrenceImportSortField> offsetRequest)
   {
      return importFacade.findImports(context, offsetRequest);
   }

   @Override
   public long count(Void filter, Context context)
   {
      return importFacade.countImports(context);
   }
}
