package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.tika.Tika;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.NotEnoughtRightsException;
import org.infinitenature.werbeo.service.core.api.error.ValidationException;
import org.infinitenature.werbeo.service.core.api.infra.commands.MediaCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceCommentsCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceIndexCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceValidationCommands;
import org.infinitenature.werbeo.service.core.api.ports.MediaUpload;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceCommandsPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.validation.ValidationContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.togglz.core.manager.FeatureManager;

@Service
public class OccurrensCommandsPortImpl implements OccurrenceCommandsPort
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrensCommandsPortImpl.class);

   @Autowired
   private AccessRightsChecker accessRightsChecker;
   @Autowired
   private OccurrenceCommentsCommands occurrenceCommentCommands;
   @Autowired
   private MediaCommands mediaComands;
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;
   @Autowired
   private OccurrenceIndexCommands occurrenceIndexCommands;
   @Autowired
   private FeatureManager featureManager;
   @Autowired
   private OccurrenceValidationCommands occurrenceValidationCommands;
   @Autowired
   private Validator validator;

   @Override
   public int addComment(UUID occurrenceId, OccurrenceComment comment,
         Context context)
   {
      if (comment.getId() != null)
      {
         throw new NotEnoughtRightsException(
               "Updateing comments is not alloewd");
      }
      occurrenceQueryPort.get(occurrenceId, context); // throws Exception if
                                                      // user tries to comment
                                                      // on a occurrence without
                                                      // the right to read it
      return occurrenceCommentCommands.addComment(occurrenceId, comment,
            context);
   }

   @Override
   public void deleteMedium(UUID occurrenceId, String clearPath,
         Context context)
   {
      Occurrence occurrence = occurrenceQueryPort.get(occurrenceId, context);
      accessRightsChecker.isAllowedTo(Operation.UPDATE, occurrence, context);

      Medium mediumToDelete = occurrence.getMedia().stream()
            .filter(m -> m.getPath().equals(clearPath)).findAny()
            .orElseThrow(() -> new EntityNotFoundException("No medium "
                  + clearPath + " found for occurrence " + occurrenceId));

      mediaComands.deleteOccurrenceMedium(occurrenceId,
            mediumToDelete,
            context);

      index(occurrenceId, context);
   }

   private void index(UUID occurrenceId, Context context)
   {
      if (featureManager.isActive(WerBeoFeatures.UPDATE_INDEX_ON_SAVE))
      {
         CompletableFuture.supplyAsync(() ->
         {
            occurrenceIndexCommands
                  .index(
                        new ArrayList<>(Arrays.asList(
                              occurrenceQueryPort.get(occurrenceId, context))),
                        true);
            return true;
         });
      }
   }

   @Override
   public List<Medium> addMedia(UUID occurrenceId, MediaUpload mediaUpload,
         Context context)
   {
      Occurrence occurrence = occurrenceQueryPort.get(occurrenceId, context);
      accessRightsChecker.isAllowedTo(Operation.UPDATE, occurrence, context);
      ValidationContextHolder.setThreadLocalContext(context);
      validate(mediaUpload);
      ValidationContextHolder.removeThreadLocalContext();
      MediaType mediaType = checkMediaType(mediaUpload.getMediaData());
      String uploadedFileName = mediaComands.uploadMedia(
            mediaUpload.getMediaData(), mediaUpload.getFileName(), context);
      List<Medium> uploadedMedia = new ArrayList<>();

      uploadedMedia.add(new Medium(uploadedFileName,
            mediaUpload.getDescription(), mediaType));

      mediaComands.addOccurrenceMedia(occurrenceId,
            uploadedFileName,
            mediaUpload.getDescription(), mediaType, context);

      if (featureManager.isActive(WerBeoFeatures.UPDATE_INDEX_ON_SAVE))
      {
         CompletableFuture.supplyAsync(() ->
         {
            occurrenceIndexCommands
                  .index(new ArrayList<>(Arrays.asList(occurrence)), true);
            return true;
         });
      }

      return uploadedMedia;
   }

   @Override
   public void setValidationStatus(UUID occurrenceId,
         Validation validationStatus,
         Context context)
   {
      if (!context.hasRole(Roles.VALIDATOR))
      {
         throw new NotEnoughtRightsException(
               "Current user is not allowed up validate occurrences in portal "
                     + context.getPortal().getId());
      }

      occurrenceQueryPort.get(occurrenceId, context); // throws Exception if
                                                      // user tries to set
                                                      // validation status on a
                                                      // occurrence without the
                                                      // right to read it
      occurrenceValidationCommands.setValidationStatus(occurrenceId,
            validationStatus,
            context);

      if (featureManager.isActive(WerBeoFeatures.UPDATE_INDEX_ON_SAVE))
      {
         CompletableFuture.supplyAsync(() ->
         {
            Occurrence occurrence = occurrenceQueryPort.get(occurrenceId,
                  context);
            occurrenceIndexCommands
                  .index(new ArrayList<>(Arrays.asList(occurrence)), true);
            return true;
         });
      }

   }

   private void validate(MediaUpload mediaUpload)
   {
      Set<ConstraintViolation<MediaUpload>> constraintViolations = validator
            .validate(mediaUpload);
      if (!constraintViolations.isEmpty())
      {
         LOGGER.error("Failure validating sample {}", constraintViolations);
         throw new ValidationException("Failure validating sample",
               constraintViolations);
      }
   }

   private MediaType checkMediaType(byte[] mediaData)
   {
      String mimeType = getMimeType(mediaData).toLowerCase();

      switch (mimeType)
      {
      case "image/jpeg":
      case "image/png":
         return MediaType.IMAGE_LOCAL;
      case "audio/vnd.wave":
      case "audio/mpeg":
         return MediaType.AUDIO_LOCAL;
      default:
         throw new ValidationException("Unknown media type " + mimeType,
               Collections.emptySet());

      }

   }

   private String getMimeType(byte[] data)
   {
      return new Tika().detect(data);
   }

}
