package org.infinitenature.werbeo.service.core.impl.imports.csv;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.LifeStage;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Makropter;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Reproduction;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Sex;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocustCSVImporter extends AbstractCSVImporter
      implements CSVImporter
{

   private static final String UNBEKANNT = "unbekannt";
   // TODO : move to Configuration
   List<Integer> supportedPortalIds = Arrays
         .asList(new Integer[] { 6, 7, 8, 9 });
   @Autowired
   private PositionFactory positionFactory;

   protected SampleWithComment map2Sample(String[] nextLine, Context context)
   {
      Sample sample = new Sample();

      String uuid = nextLine[0];
      if(StringUtils.isNotBlank(uuid))
      {
         sample.setId(UUID.fromString(uuid));
      }
      else
      {
         sample.setId(UUID.randomUUID());
      }
      
      
      TaxonBase taxon = processTaxon(context, nextLine[1],
            null);

      Occurrence occurrence = new Occurrence();
      occurrence.setTaxon(taxon);
      sample.getOccurrences().add(occurrence);
      
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setId(UUID.randomUUID());
      String finderString = nextLine[2];
      String determinerString = nextLine[3];
      sample.setRecorder(toPerson(finderString, context));
      occurrence.setDeterminer(toPerson(determinerString, context));
      
      if(occurrence.getDeterminer() == null)
      {
         if(sample.getRecorder() == null)
         {
            sample.setRecorder(toPerson("keine Angabe", context));
         }
         occurrence.setDeterminer(sample.getRecorder());
      }
      
      processDate(nextLine[4], sample);
      sample.setLocality(mapLocality(nextLine[6], nextLine[7], nextLine[8],
            nextLine[9], nextLine[5], null));
      
      
      occurrence.setHabitat(nextLine[10]);
      sample.getLocality().setLocality(nextLine[11]);
      if(StringUtils.isNotBlank(nextLine[12]))
      {
         occurrence.setNumericAmount(Integer.valueOf(nextLine[12]));
      }
      
      occurrence.setSex(mapSex(nextLine[13]));
      occurrence.setReproduction(mapReproduction(nextLine[14]));
      sample.setSampleMethod(mapSampleMethod(nextLine[15]));
      occurrence.setLifeStage(mapLifeStage(nextLine[16]));
      occurrence.setMakropter(mapMakropter(nextLine[17]));
      occurrence.setDeterminationComment(nextLine[18]);
      sample.setSurvey(getOrCreateSurvey(nextLine[19], context));
      return new SampleWithComment(sample, null);
   }

   private Locality mapLocality(String latitude, String longitude, String epsg,
         String blur, String mtb, String mtbq)
   {
      Locality locality = new Locality();
      if (StringUtils.isNotBlank(latitude) && StringUtils.isNotBlank(longitude)
            && StringUtils.isNotBlank(epsg))
      {
         locality.setPrecision(Integer.valueOf(blur));
         locality.setPosition(positionFactory.create(Double.valueOf(latitude.replace(",", ".")),
               Double.valueOf(longitude.replace(",", ".")), Integer.valueOf(epsg)));
      } else
      {
         locality.setPrecision(StringUtils.isNotBlank(mtbq) ? 3642 : 7285);
         locality.setPosition(positionFactory.createFromMTB(
               mtb + (StringUtils.isNotBlank(mtbq) ? "/" + mtbq : "")));
      }

      return locality;
   }

   private Reproduction mapReproduction(String string)
   {
      if ("sicher".contentEquals(string))
      {
         return Reproduction.SURE;
      } else if ("plausibel".contentEquals(string))
      {
         return Reproduction.PLAUSIBLE;
      } else if ("unwahrscheinlich".contentEquals(string))
      {
         return Reproduction.UNLIKELY;
      } else if (UNBEKANNT.contentEquals(string))
      {
         return Reproduction.UNKNOWN;
      }
      return null;
   }
   
   private SampleMethod mapSampleMethod(String string)
   {
      if ("Schätzung".contentEquals(string) || "S".contentEquals(string))
      {
         return SampleMethod.ESTIMINATION;
      } else if ("Transektbegehung".contentEquals(string) || "T".contentEquals(string))
      {
         return SampleMethod.TRANSECT_COUNT;
      } else if ("Kescherfänge".contentEquals(string)|| "K".contentEquals(string))
      {
         return SampleMethod.SWEEP_NETTING;
      }
      else if ("Isolationsquadrate".contentEquals(string) || "I".contentEquals(string))
      {
         return SampleMethod.BOX_QUADRAT;
      }
      else if ("Motorsauger".contentEquals(string) || "M".contentEquals(string))
      {
         return SampleMethod.SUCTION_SAMPLER;
      }
      else if ("Andere Methode".contentEquals(string) || "a".contentEquals(string) || "A".contentEquals(string))
      {
         return SampleMethod.OTHER_METHOD;
      }
      else if ("Beobachtung".contentEquals(string) || "B".contentEquals(string))
      {
         return SampleMethod.FIELD_OBSERVATION;
      }
      return null;
   }

   private Sex mapSex(String string)
   {
      if ("weiblich".contentEquals(string) || "W".contentEquals(string))
      {
         return Sex.FEMALE;
      } else if ("männlich".contentEquals(string) || "M".contentEquals(string))
      {
         return Sex.MALE;
      } else if ("männlich+weiblich".contentEquals(string) || "MW".contentEquals(string))
      {
         return Sex.BOTH;
      } else if (UNBEKANNT.contentEquals(string) || "u".contentEquals(string) || "U".contentEquals(string))
      {
         return Sex.UNKNOWN;
      }
      return null;
   }
   
   private Makropter mapMakropter(String string)
   {
      if (UNBEKANNT.contentEquals(string))
      {
         return Makropter.UNKNOWN;
      } else if ("ja".contentEquals(string))
      {
         return Makropter.YES;
      } 
      else if ("nein".contentEquals(string))
      {
         return Makropter.NO;
      } 
      return null;
   }



   private LifeStage mapLifeStage(String string)
   {
      if (UNBEKANNT.contentEquals(string))
      {
         return LifeStage.UNKNOWN;
      } else if ("Larve".contentEquals(string))
      {
         return LifeStage.LARVE;
      } 
      else if ("Imago".contentEquals(string))
      {
         return LifeStage.IMAGO;
      } 
      else if ("Larve und Imago".contentEquals(string))
      {
         return LifeStage.LARVE_IMAGO;
      } 
      return null;
   }




   private OffsetRequest<TaxonSortField> getOffsetRequest()
   {
      OffsetRequest<TaxonSortField> offsetRequest = new OffsetRequest<TaxonSortField>()
      {

         @Override
         public SortOrder getSortOrder()
         {
            return SortOrder.ASC;
         }

         @Override
         public TaxonSortField getSortField()
         {
            return TaxonSortField.TAXON;
         }

         @Override
         public int getOffset()
         {
            return 0;
         }

         @Override
         public int getCount()
         {
            return Integer.MAX_VALUE;
         }
      };
      return offsetRequest;
   }

   @Override
   public boolean handlesPortal(int portalId)
   {
      return supportedPortalIds.contains(portalId);
   }

}
