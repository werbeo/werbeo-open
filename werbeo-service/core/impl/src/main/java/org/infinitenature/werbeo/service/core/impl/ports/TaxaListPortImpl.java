package org.infinitenature.werbeo.service.core.impl.ports;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxaListQueries;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaxaListPortImpl implements TaxaListQueryPort
{

   private final TaxaListQueries taxaListQueries;

   private final InstanceConfig instanceConfig;

   public TaxaListPortImpl(@Autowired TaxaListQueries taxaListQueries,
         @Autowired InstanceConfig instanceConfig)
   {
      super();
      this.taxaListQueries = taxaListQueries;
      this.instanceConfig = instanceConfig;
   }

   @Override
   public Slice<TaxaList, TaxaListSortField> find(TaxaListFilter filter,
         Context context, OffsetRequest<TaxaListSortField> offsetRequest)
   {
      return taxaListQueries.find(filter, context, offsetRequest);
   }

   @Override
   public long count(TaxaListFilter filter, Context context)
   {
      return taxaListQueries.count(filter, context);
   }

   @Override
   public TaxaList get(Integer id, Context context)
   {
      TaxaList taxaList = taxaListQueries.get(id, context);
      adjustGroups(taxaList, context);
      return taxaList;
   }

   private void adjustGroups(TaxaList taxaList, Context context)
   {
      if (instanceConfig.getTaxonGroupsPerPortal()
            .containsKey(context.getPortal().getId()))
      {
         Collection<String> adjustedGroups = CollectionUtils
               .intersection(
               taxaList.getTaxaGroups(), instanceConfig.getTaxonGroupsPerPortal()
                     .get(context.getPortal().getId()));
         taxaList.setTaxaGroups(new ArrayList<>(adjustedGroups));
      }
   }

}
