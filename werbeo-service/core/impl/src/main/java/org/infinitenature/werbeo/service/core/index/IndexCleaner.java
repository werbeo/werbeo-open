package org.infinitenature.werbeo.service.core.index;

import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceIndexCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceIndexQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.togglz.core.manager.FeatureManager;

@Service
public class IndexCleaner
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(IndexCleaner.class);
   @Autowired
   private OccurrenceIndexQueries occurrenceIndexQueries;
   @Autowired
   private OccurrenceQueries occurrenceQueries;
   @Autowired
   private OccurrenceIndexCommands occurrenceIndexCommands;
   @Autowired
   private FeatureManager featureManager;

   @Scheduled(fixedDelay = 5000)
   public void cleanUpIndex()
   {
      if (featureManager.isActive(WerBeoFeatures.CLEAN_INDEX))
      {
         LOGGER.info("Start cleaning index");
         occurrenceIndexQueries.findAllIds()
               .filter(id -> occurrenceQueries.notExists(id))
               .forEach(id -> occurrenceIndexCommands.delete(id));
      }
   }
}
