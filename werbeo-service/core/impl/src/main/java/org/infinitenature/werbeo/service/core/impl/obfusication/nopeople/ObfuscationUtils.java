package org.infinitenature.werbeo.service.core.impl.obfusication.nopeople;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonFormatter;
import org.infinitenature.werbeo.service.core.api.enums.Permission;
import org.infinitenature.werbeo.service.core.api.enums.Role;
import org.infinitenature.werbeo.service.core.api.support.Context;

public class ObfuscationUtils
{

   private ObfuscationUtils()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static void obfuscateMetaUsers(BaseType<?> entity)
   {
      entity.setCreatedBy(User.HIDDEN_USER);
      entity.setModifiedBy(User.HIDDEN_USER);
      entity.setObfuscated(true);
   }

   public static void obfuscatePeople(Survey survey)
   {
      obfuscateMetaUsers(survey);
      survey.setDeputyCustodians(new HashSet<>());
   }

   public static void obfuscatePeople(Occurrence entity)
   {
      obfuscateMetaUsers(entity);
      if (entity.getDeterminer() != null)
      {
         entity.setDeterminer(Person.HIDDEN_PERSON);
      }
      if (StringUtils.isNotBlank(entity.getObservers()))
      {
         entity.setObservers(PersonFormatter.format(Person.HIDDEN_PERSON));
      }
   }

   public static void obfuscatePeople(SampleBase sample)
   {

      obfuscateMetaUsers(sample);
      if (sample.getRecorder() != null)
      {
         sample.setRecorder(Person.HIDDEN_PERSON);
      }

   }
   
   public static List<Permission> getObfuscationPermissions(Survey survey,
         Context context)
   {
      List<Permission> permissions = new ArrayList<>();
      for (ObfuscationPolicy policy : survey.getObfuscationPolicies())
      {
         if (Role.ANONYMOUS.equals(policy.getRole()) || context
               .hasRole(Roles.valueOf(policy.getRole().getRoleName())))
         {
            permissions.add(policy.getPermission());
         }
      }
      return permissions;
   }
   
   public static int getPermittedQuadrant(List<Permission> permissions)
   {
      int quadrant = 0;
      if(permissions.contains(Permission.LOCATION_MTBQQQ))
      {
         quadrant = 3;
      }
      else if(permissions.contains(Permission.LOCATION_MTBQQ))
      {
         quadrant = 2;
      }
      else if(permissions.contains(Permission.LOCATION_MTBQ))
      {
         quadrant = 1;
      }
      else
      {
          quadrant = 0;
      }
      return quadrant;
   }
}
