package org.infinitenature.werbeo.service.core.impl.imports;

import java.util.Optional;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceImportQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceImportQueryPortImpl implements OccurrenceImportQueries
{
   @Autowired
   private ImportQueries importQueries;

   @Override
   public JobStatus getStatus(UUID id, Context context)
   {
      // TODO check user rights
      Optional<ImportJob> importJob = importQueries.findImportJobById(id,
            context);
      if (importJob.isPresent())
      {
         return importJob.get().getStatus();
      } else
      {
         throw new EntityNotFoundException(
               "no import job with id " + id + " found");
      }
   }

}
