package org.infinitenature.werbeo.service.core.impl.imports;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.ImportPart;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ImportFailureAggregator
{
   @Autowired
   private ImportQueries importQueries;
   @Autowired
   private ImportCommands importCommands;
   @Async
   public void collectFailures(ImportJob importJob, Context context)
   {
      Map<String, Set<UUID>> failures = new HashMap<>();
      importQueries.forEachFailure(importJob, context,
            part -> addFailure(part, failures));
      importCommands.writeFailureReport(importJob, failures);
   }

   private void addFailure(ImportPart part, Map<String, Set<UUID>> failures)
   {
      if (part.getFailureReason().isPresent())
      {
         String failure = part.getFailureReason().get().getFailure();
         if(!failures.containsKey(failure)) {
            failures.put(failure, new HashSet<>());
         }
         failures.get(failure).add(part.getSample().getId());
      }
   }

}
