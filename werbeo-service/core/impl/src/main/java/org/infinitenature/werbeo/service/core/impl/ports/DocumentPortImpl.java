package org.infinitenature.werbeo.service.core.impl.ports;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateFormatter;
import org.infinitenature.werbeo.service.core.api.enity.HerbarySheet;
import org.infinitenature.werbeo.service.core.api.enity.HerbarySheetComplete;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.support.VagueDateFormatter;
import org.infinitenature.werbeo.service.core.api.enums.HerbarySheetPosition;
import org.infinitenature.werbeo.service.core.api.ports.DocumentPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
public class DocumentPortImpl implements DocumentPort
{

   public static final String EMPTY = "";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(DocumentPortImpl.class);
   private static final String PATH = "/templates/odt/";

   private final OccurrenceQueryPort occurrenceQueryPort;

   public DocumentPortImpl(@Autowired OccurrenceQueryPort occurrenceQueryPort)
   {
      super();
      this.occurrenceQueryPort = occurrenceQueryPort;
   }

   @Override
   public ByteArrayOutputStream createHerbaryPage(Context context,
         UUID occurrenceUuid, HerbarySheetPosition position, HerbarySheet sheet)
   {
      String file = getTemplateFileName(position);
      Occurrence occurrence = occurrenceQueryPort.get(occurrenceUuid, context);
      if (occurrence == null)
      {
         LOGGER.error("Error fetching occurrence for HerbarySheet");
         return null;
      }
      return makePdf(mapToHerbarySheetComplete(occurrence, sheet), file);
   }

   protected String getTemplateFileName(HerbarySheetPosition position)
   {
      String file = null;
      switch (position)
      {
      case TOP_LEFT:
         file = "herbarysheet_topleft.odt";
         break;
      case TOP_RIGHT:
         file = "herbarysheet_topright.odt";
         break;
      case BOTTOM_LEFT:
         file = "herbarysheet_bottomleft.odt";
         break;
      case BOTTOM_RIGHT:
         file = "herbarysheet_bottomright.odt";
         break;
      default:
         LOGGER.error("Error producing herbarysheet, case missing");
      }
      return PATH + file;
   }

   protected ByteArrayOutputStream makePdf(Object sheet, String file)
   {
      final java.util.logging.Logger app = java.util.logging.Logger
            .getLogger("org.odftoolkit.odfdom.pkg.OdfXMLFactory");
      app.setLevel(java.util.logging.Level.WARNING);

      try (InputStream in = DocumentPortImpl.class.getResourceAsStream(file);
            ByteArrayOutputStream out = new ByteArrayOutputStream())
      {
         IXDocReport report = XDocReportRegistry.getRegistry()
               .loadReport(in, TemplateEngineKind.Freemarker);
         IContext context = report.createContext();
         context.put("h", sheet);

         Options options = Options
               .getFrom(fr.opensagres.xdocreport.core.document.DocumentKind.ODT)
               .to(ConverterTypeTo.PDF);

         report.convert(context, options, out);
         return out;
      } catch (IOException | XDocReportException e)
      {
         LOGGER.error("Error producing pdf", e);
         return null;
      }
   }

   private HerbarySheetComplete mapToHerbarySheetComplete(Occurrence occurrence,
         HerbarySheet herbarySheet)
   {
      HerbarySheetComplete herbarySheetComplete = new HerbarySheetComplete();

      herbarySheetComplete.setInstitution(
            herbarySheet.getInstitution() == null ?
                  EMPTY :
                  herbarySheet.getInstitution().getOrganization() == null ?
                        EMPTY :
                        herbarySheet.getInstitution().getOrganization() + " ");

      herbarySheetComplete.setHerbary(herbarySheet.getHerbary() == null ?
            EMPTY :
            herbarySheet.getHerbary());

      herbarySheetComplete.setLatinname(occurrence.getTaxon().getName());

      herbarySheetComplete.setAuthor(
            occurrence.getTaxon().getAuthority() == null ?
                  EMPTY :
                  occurrence.getTaxon().getAuthority());

      herbarySheetComplete.setSynonym(herbarySheet.getSynonym() == null ?
            EMPTY :
            herbarySheet.getSynonym());

      herbarySheetComplete.setCoordinates(
            formatCoordinates(occurrence));

      herbarySheetComplete.setEpsg(formatEpsg(occurrence));

      if (occurrence.getSample().getLocality().getPrecision() != null)
      {
         if (occurrence.getSample().getLocality().getPosition().getType()
               .compareTo(Position.PositionType.MTB) == 0)
         {
            herbarySheetComplete.setBlur(EMPTY);
         } else
         {
            herbarySheetComplete.setBlur(
                  "+/- " + String.valueOf(occurrence.getSample().getLocality().getPrecision())
                        + " m");
         }
      } else
      {
         herbarySheetComplete.setBlur(EMPTY);
      }

      herbarySheetComplete.setHabitat(herbarySheet.getHabitat() == null ?
            EMPTY :
            herbarySheet.getHabitat());

      if (occurrence.getDeterminer() != null)
      {
         herbarySheetComplete.setDeterminer(
               occurrence.getDeterminer().getFirstName() + " " + occurrence
                     .getDeterminer().getLastName());
      } else
      {
         herbarySheetComplete.setDeterminer(EMPTY);
      }

      if (occurrence.getSample().getRecorder() != null)
      {
         herbarySheetComplete.setRecorder(
               occurrence.getSample().getRecorder().getFirstName() + " "
                     + occurrence.getSample().getRecorder().getLastName());
      } else
      {
         herbarySheetComplete.setRecorder(EMPTY);
      }

      VagueDateFormatter vagueDateFormatter = new VagueDateFormatter();

      herbarySheetComplete.setDate(
            vagueDateFormatter.format(occurrence.getSample().getDate()));

      herbarySheetComplete.setUuid(occurrence.getId().toString());

      return herbarySheetComplete;
   }

   private String formatEpsg(Occurrence occurrence)
   {
      if (occurrence.getSample().getLocality().getPosition().getType()
            == Position.PositionType.MTB)
      {
         return "Messtischblatt";
      }

      return "EPSG: " + String
            .valueOf(occurrence.getSample().getLocality().getPosition().getEpsg());
   }

   private String formatCoordinates(Occurrence occurrence)
   {

      if (Position.PositionType.MTB.compareTo(occurrence.getSample().getLocality().getPosition().getType()) == 0)
      {
         return "MTB " +occurrence.getSample().getLocality().getPosition().getMtb().getMtb();
      }

      return CoordinateFormatter.format(occurrence.getSample().getLocality().getPosition().getEpsg(),
            occurrence.getSample().getLocality().getPosition().getPosCenter()[0],
            occurrence.getSample().getLocality().getPosition().getPosCenter()[1]);
   }
}


