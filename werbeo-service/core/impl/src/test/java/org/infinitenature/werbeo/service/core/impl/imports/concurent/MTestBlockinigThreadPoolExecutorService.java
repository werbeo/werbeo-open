package org.infinitenature.werbeo.service.core.impl.imports.concurent;

import java.util.concurrent.ExecutorService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class MTestBlockinigThreadPoolExecutorService
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(MTestBlockinigThreadPoolExecutorService.class);

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   void test()
   {
      ExecutorService es = new BlockinigThreadPoolExecutorService(20,
            "Test-Pool");

      for (int i = 0; i <= 100; i++)
      {
         es.execute(() ->
         {
            LOGGER.info("Starting...");
            try
            {
               Thread.sleep(1000);
            } catch (InterruptedException e)
            {
               e.printStackTrace();
            }
            LOGGER.info("... finished.");
         });
      }
   }

}
