package org.infinitenature.werbeo.service.core.impl.obfusication;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Collections;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.obfusication.nopeople.NoPersonOccurrenceObfuscator;
import org.infinitenature.werbeo.service.core.impl.obfusication.nopeople.NoPersonSampleObfuscator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestObfuscatorFactory
{
   private ObfuscatorFactory obfuscatorFactory;
   private Context context = new Context(new Portal(-1), new User(),
         Collections.emptySet(), "test");
   private Survey survey = new Survey();
   private PortalQueryPort portalQueryPort;

   @BeforeEach
   public void setUp() throws Exception
   {
      obfuscatorFactory = new ObfuscatorFactory();
      obfuscatorFactory.setNoPersonOccurrenceObfuscator(
            new NoPersonOccurrenceObfuscator());
      obfuscatorFactory.setNoPersonOccurrenceObfuscator(
            new NoPersonOccurrenceObfuscator());
      obfuscatorFactory
            .setNoPersonSampleObfuscator(new NoPersonSampleObfuscator());
      obfuscatorFactory
            .setNoPersonSampleObfuscator(new NoPersonSampleObfuscator());
   }

   @Test
   @DisplayName("occurrence and PortalConfiguration contains PERSONS_FOR_APPROVED_USERS ObfuscationPolicy")
   public void test001()
   {
      ChainedEntityObfuscator<?> obfuscator = (ChainedEntityObfuscator<?>) obfuscatorFactory
            .getOccurrenceObfuscator(survey, context);

      assertThat(obfuscator.getObfuscators().get(1),
            instanceOf(NoPersonOccurrenceObfuscator.class));
   }

   @Test
   @DisplayName("occurrence and PortalConfiguration does not contain PERSONS_FOR_APPROVED_USERS ObfuscationPolicy")
   public void test002()
   {
      ChainedEntityObfuscator<?> obfuscator = (ChainedEntityObfuscator<?>) obfuscatorFactory
            .getOccurrenceObfuscator(survey, context);

      assertThat(obfuscator.getObfuscators().get(1),
            instanceOf(NoPersonOccurrenceObfuscator.class));
   }

   @Test
   @DisplayName("sample and PortalConfiguration contains PERSONS_FOR_APPROVED_USERS ObfuscationPolicy")
   public void test003()
   {
      ChainedEntityObfuscator<?> obfuscator = (ChainedEntityObfuscator<?>) obfuscatorFactory
            .getSampleObfuscator(survey, context);

      assertThat(obfuscator.getObfuscators().get(1),
            instanceOf(NoPersonSampleObfuscator.class));
   }

   @Test
   @DisplayName("sample and PortalConfiguration does not contain PERSONS_FOR_APPROVED_USERS ObfuscationPolicy")
   public void test004()
   {
      ChainedEntityObfuscator<?> obfuscator = (ChainedEntityObfuscator<?>) obfuscatorFactory
            .getSampleObfuscator(survey, context);

      assertThat(obfuscator.getObfuscators().get(1),
            instanceOf(NoPersonSampleObfuscator.class));
   }
}
