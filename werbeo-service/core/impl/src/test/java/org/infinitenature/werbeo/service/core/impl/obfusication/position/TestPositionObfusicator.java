package org.infinitenature.werbeo.service.core.impl.obfusication.position;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.Collections;

import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.common.position.impl.PositionFactoryImpl;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestPositionObfusicator
{

   private PositionSampleObfuscator obfusicatorUT;

   private PositionFactory positionFactory = new PositionFactoryImpl();
   private Context anonymousContext = new Context(new Portal(), null,
         Collections.emptySet(), "test");
   private Context loggedInContext = new Context(new Portal(), new User(),
         Collections.emptySet(), "test");

   @BeforeEach
   void setUp() throws Exception
   {
      obfusicatorUT = new PositionSampleObfuscator(positionFactory);
   }

   @Test
   @DisplayName("Check that anonymous access to samples obfusicates position")
   void test001()
   {
      SampleBase sample = obfusicatorUT.obfuscate(createSample(),
            anonymousContext);

      assertThat(sample.isObfuscated(), is(true));

      Locality locality = sample.getLocality();

      assertThat(locality.getLocality(), is(nullValue()));
      assertThat(locality.getLocationComment(), is(nullValue()));
      assertThat(locality.getPosition().getMtb(), is(MTBHelper.toMTB("2943")));
      assertThat(locality.getPrecision(), is(300));
   }

   @Test
   @DisplayName("Check that logged in access to samples does not obfusicates position")
   void test002()
   {
      SampleBase sample = obfusicatorUT.obfuscate(createSample(),
            loggedInContext);

      assertThat(sample.isObfuscated(), is(true));

      Locality locality = sample.getLocality();

      assertEquals(locality.getLocality(), null);
      assertEquals(locality.getLocationComment(),null);
      assertThat(locality.getPosition().getMtb(),
            is(MTBHelper.toMTB("2943")));
      assertThat(locality.getPrecision(), is(300));
   }

   @Test
   @DisplayName("Check that logged in access to  free samples does not obfusicates position")
   void test003()
   {
      SampleBase sample = obfusicatorUT
            .obfuscate(createSample(Availability.FREE), loggedInContext);

      assertThat(sample.isObfuscated(), is(true));

      Locality locality = sample.getLocality();

      assertEquals(locality.getLocality(), null);
      assertEquals(locality.getLocationComment(),null);
      assertThat(locality.getPosition().getMtb(),
            is(MTBHelper.toMTB("2943")));
      assertThat(locality.getPrecision(), is(300));
   }

   @Test
   @DisplayName("Check that anonymous access to free samples does not obfusicates position")
   void test004()
   {
      SampleBase sample = obfusicatorUT
            .obfuscate(createSample(Availability.FREE), anonymousContext);

      assertThat(sample.isObfuscated(), is(true));

      Locality locality = sample.getLocality();

      assertEquals(locality.getLocality(), null);
      assertEquals(locality.getLocationComment(),null);
      assertThat(locality.getPosition().getMtb(),
            is(MTBHelper.toMTB("2943")));
      assertThat(locality.getPrecision(), is(300));
   }

   private Sample createSample()
   {
      Availability availability = Availability.RESTRICTED;
      return createSample(availability);
   }

   private Sample createSample(Availability availability)
   {
      Locality locality = new Locality();

      locality.setLocality("locality");
      locality.setLocationComment("locationComment");
      locality.setPosition(positionFactory.createFromMTB("2943/123"));
      locality.setPrecision(300);
      Sample sample = new Sample();
      sample.setLocality(locality);
      sample.setSurvey(new Survey());
      sample.getSurvey().setAvailability(availability);
      return sample;
   }

}
