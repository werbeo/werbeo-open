package org.infinitenature.werbeo.service.core.impl.ports;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.ports.TranslationQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestTranslationQueryPortImpl
{
   private TranslationQueryPort queryPortUT = new TranslationQueryPortImpl();

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   void testGetTranslation()
   {
      String message = queryPortUT.getTranslation(
            new Context(new Portal(1, "Test"), User.INTERNAL_SUPER_USER,
                  Collections.emptySet(), new PortalConfiguration()),
            "message.export.csv", Locale.GERMANY);

      assertThat(message, is("csv Export"));
   }

   @Test
   void testGetTranslations()
   {
      Map<String, String> translations = queryPortUT.getTranslations(
            new Context(new Portal(1, "Test"), User.INTERNAL_SUPER_USER,
                  Collections.emptySet(), new PortalConfiguration()),
            Locale.GERMANY);

      assertThat(translations.size(), is(greaterThan(0)));
   }
}
