package org.infinitenature.werbeo.service.core.maps;

import java.util.Collections;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;

public abstract class AbstractTest
{
   protected User getUser(String login)
   {
      switch (login)
      {
      case UserTestUtil.USER_LOGIN_A:
         Person person = new Person("Cthrine", "Tomala");
         User user = new User();
         user.setLogin(UserTestUtil.USER_LOGIN_A);
         user.setPerson(person);
         return user;
      case UserTestUtil.USER_LOGIN_B:
         Person personB = new Person("Sarge", "Ellins");
         User userB = new User();
         userB.setLogin(UserTestUtil.USER_LOGIN_B);
         userB.setPerson(personB);
         return userB;
      default:
         throw new IllegalArgumentException("Unknown user login " + login);
      }
   }

   protected Context createContext(User user)
   {
      return new Context(new Portal(PortalTestUtil.DEMO_ID), user,
            Collections.EMPTY_SET, "test");
   }

}
