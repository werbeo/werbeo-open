package org.infinitenature.werbeo.service.core.access.checks;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Set;
import java.util.UUID;

import org.hamcrest.core.IsNot;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.infra.query.SurveyQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestSampleAccessRightCheck
{
   private SampleAccessRightCheck checkUT;
   private Context context = mock(Context.class);
   private SurveyQueries surveyQueries = mock(SurveyQueries.class);

   @BeforeEach
   void setUp() throws Exception
   {
      Survey surveyAllowDataEntry = new Survey();
      Survey surveyDontAllowDataEntry = new Survey();
      surveyDontAllowDataEntry.setAllowDataEntry(false);
      when(surveyQueries.load(eq(1), any(Context.class)))
            .thenReturn(surveyAllowDataEntry);
      when(surveyQueries.load(eq(2), any(Context.class)))
            .thenReturn(surveyDontAllowDataEntry);
      checkUT = new SampleAccessRightCheck(surveyQueries);
   }

   @Test
   void test001()
   {
      assertThat(checkUT.isResponsibleFor(Sample.class, null), is(true));
   }

   @Test
   void test002()
   {
      assertThat(checkUT.isResponsibleFor(SampleBase.class, null), is(true));
   }

   @Test
   @DisplayName("Save new sample - non admin")
   void test003()
   {
      Sample sample = sampleSurveyAllowDataEntry();
      when(context.isAccepted()).thenReturn(true);
      when(context.isUserLoggedIn()).thenReturn(true);
      assertThat(checkUT.isAllowedTo(Operation.CREATE, sample, context),
            is(true));
   }

   Sample sampleSurveyAllowDataEntry()
   {
      return sampleSurveyAllowDataEntry(true);
   }

   Sample sampleSurveyAllowDataEntry(boolean allowDataEntry)
   {
      Sample sample = new Sample();
      Survey survey = new Survey();
      survey.setAllowDataEntry(allowDataEntry);
      survey.setId(allowDataEntry ? 1 : 2);
      sample.setSurvey(survey);
      return sample;
   }

   @Test
   @DisplayName("Save new sample - none admin - Privacy not accepted")
   void test003a()
   {
      Sample sample = sampleSurveyAllowDataEntry();
      when(context.isAccepted()).thenReturn(false);
      when(context.isUserLoggedIn()).thenReturn(true);

      assertThat(checkUT.isAllowedTo(Operation.CREATE, sample, context),
            is(false));
   }

   @Test
   @DisplayName("Save new sample - none admin - User not logged in")
   void test003b()
   {
      Sample sample = sampleSurveyAllowDataEntry();
      when(context.isAccepted()).thenReturn(false);
      when(context.isUserLoggedIn()).thenReturn(false);

      assertThat(checkUT.isAllowedTo(Operation.CREATE, sample, context),
            is(false));
   }

   @Test
   @DisplayName("Update sample - admin")
   void test004()
   {
      UUID id = UUID.randomUUID();

      Sample persistedSample = sampleSurveyAllowDataEntry();
      persistedSample.setId(id);
      User user = new User();
      user.setLogin("bla@bla.de");
      persistedSample.setCreatedBy(user);

      when(context.isAccepted()).thenReturn(true);
      when(context.hasRole(Roles.ADMIN)).thenReturn(true);
      when(context.isUserLoggedIn()).thenReturn(true);

      assertThat(checkUT.getAllowedOperations(persistedSample, context),
            hasItem(Operation.UPDATE));
   }

   @Test
   @DisplayName("Update sample - non admin - but own")
   void test005()
   {
      UUID id = UUID.randomUUID();

      Sample persistedSample = sampleSurveyAllowDataEntry();
      persistedSample.setId(id);
      User user = new User();
      user.setLogin("bla@bla.de");
      persistedSample.setCreatedBy(user);

      when(context.isAccepted()).thenReturn(true);
      when(context.hasRole(Roles.ADMIN)).thenReturn(false);
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.getUser()).thenReturn(user);

      assertThat(checkUT.getAllowedOperations(persistedSample, context),
            hasItem(Operation.UPDATE));
   }

   @Test
   @DisplayName("Update sample - non admin - not own - TC - Privacy - logged in")
   void test005a()
   {
      UUID id = UUID.randomUUID();

      Sample persistedSample = sampleSurveyAllowDataEntry();
      persistedSample.setId(id);
      User user = new User();
      user.setLogin("bla@bla.de");
      user.setId(0);
      persistedSample.setCreatedBy(user);
      User user1 = new User();
      user1.setLogin("blub@blub.de");
      user1.setId(1);

      when(context.isAccepted()).thenReturn(true);
      when(context.hasRole(Roles.ADMIN)).thenReturn(false);
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.getUser()).thenReturn(user1);

      assertThat(checkUT.getAllowedOperations(persistedSample, context),
            IsNot.not(hasItem(Operation.UPDATE)));
   }

   @Test
   @DisplayName("Save new sample - non admin, survey don't allow dataEntry")
   void test006()
   {
      Sample sample = sampleSurveyAllowDataEntry(false);
      when(context.isAccepted()).thenReturn(true);
      when(context.isUserLoggedIn()).thenReturn(true);
      assertThat(checkUT.isAllowedTo(Operation.CREATE, sample, context),
            is(false));
   }

   @Test
   @DisplayName("Delete sample - non admin, survey don't allow dataEntry")
   void test007()
   {
      Sample sample = sampleSurveyAllowDataEntry(false);
      when(context.isAccepted()).thenReturn(true);
      when(context.isUserLoggedIn()).thenReturn(true);
      assertThat(checkUT.isAllowedTo(Operation.DELETE, sample, context),
            is(false));
   }

   @Test
   @DisplayName("get allowed operations - survey don't allow dataEntry")
   void test008()
   {
      Sample sample = sampleSurveyAllowDataEntry(false);
      when(context.isAccepted()).thenReturn(true);
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.hasRole(Roles.ADMIN)).thenReturn(true);
      Set<Operation> allowedOperations = checkUT.getAllowedOperations(sample,
            context);
      assertThat(allowedOperations,
            containsInAnyOrder(Operation.READ, Operation.UPDATE));
   }

}
