package org.infinitenature.werbeo.service.core.access.checks;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestPersonAccessRightsCheck
{
   private PersonAccessRightsCheck checkUT;

   @BeforeEach
   void setUp() throws Exception
   {
      checkUT = new PersonAccessRightsCheck();
   }

   @Test
   @DisplayName("Check is responsible for people in any context")
   void test_001()
   {
      assertThat(checkUT.isResponsibleFor(Person.class, null), is(true));
   }

   @Test
   @DisplayName("Check is not responsible for surveys in any context")
   void test_002()
   {
      assertThat(checkUT.isResponsibleFor(Survey.class, null), is(false));
   }

   @Test
   @DisplayName("Anonymous users are not allowed to read")
   void test_003()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(false);
      when(context.getPortalConfiguration())
            .thenReturn(Optional.of(new PortalConfiguration()));
      assertThat(checkUT.isAllowedTo(Operation.READ, Person.class, context),
            is(false));
   }

   @Test
   @DisplayName("Anonymous users are allowed to read in general, but not people")
   void test_003a()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(false);
      PortalConfiguration portalConfiguration = new PortalConfiguration();
      portalConfiguration.setAllowAnonymousAccess(true);
      when(context.getPortalConfiguration())
            .thenReturn(Optional.of(portalConfiguration));
      assertThat(checkUT.isAllowedTo(Operation.READ, Person.class, context),
            is(false));
   }

   @Test
   @DisplayName("Anonymous users are not allowed to create")
   void test_004()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(false);
      assertThat(checkUT.isAllowedTo(Operation.CREATE, new Person(), context),
            is(false));
   }

   @Test
   @DisplayName("Anonymous users are not allowed to update")
   void test_005()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(false);
      assertThat(checkUT.isAllowedTo(Operation.UPDATE, new Person(), context),
            is(false));
   }

   @Test
   @DisplayName("Anonymous users are not allowed to delete")
   void test_006()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(false);
      assertThat(checkUT.isAllowedTo(Operation.DELETE, new Person(), context),
            is(false));
   }

   @Test
   @DisplayName("Logged in users are allowed to read")
   void test_007()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.isAccepted()).thenReturn(true);
      assertThat(checkUT.isAllowedTo(Operation.READ, Person.class, context),
            is(true));
   }

   @Test
   @DisplayName("Logged in users are not allowed to read - no TC - no Privacy")
   void test_007a()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.isAccepted()).thenReturn(false);
      assertThat(checkUT.isAllowedTo(Operation.READ, Person.class, context),
            is(false));
   }

   @Test
   @DisplayName("ADMIN users are allowed to create")
   void test_008()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.isAccepted()).thenReturn(true);
      when(context.hasRole(Roles.ADMIN)).thenReturn(true);
      assertThat(checkUT.isAllowedTo(Operation.CREATE, new Person(), context),
            is(true));
   }

   @Test
   @DisplayName("ADMIN users are not allowed to create -  no Privacy")
   @Disabled
   void test_008a()
   {
      Context context = mock(Context.class);
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.isAccepted()).thenReturn(true);
      when(context.hasRole(Roles.ADMIN)).thenReturn(true);
      assertThat(checkUT.isAllowedTo(Operation.CREATE, new Person(), context),
            is(false));
   }
}
