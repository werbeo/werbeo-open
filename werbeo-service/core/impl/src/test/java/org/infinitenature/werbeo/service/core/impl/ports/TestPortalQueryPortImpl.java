package org.infinitenature.werbeo.service.core.impl.ports;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestPortalQueryPortImpl
{
   private PortalQueryPort queriesUT;
   private InstanceConfig instanceConfig;

   @BeforeEach
   void setUp() throws Exception
   {
      instanceConfig = new InstanceConfig();
      queriesUT = new PortalQueryPortImpl(instanceConfig,
            mock(PortalQueries.class));
   }

   @Test
   @DisplayName("Default upload size")
   void test001()
   {
      PortalConfiguration portalConfiguration = queriesUT.loadConfig(22);
      assertThat(portalConfiguration.getMaxUploadSize(), is(3));
   }

   @Test
   @DisplayName("Portal sepcific upload size")
   void test002()
   {
      instanceConfig.getMaxUploadSize().put(22, 5);
      PortalConfiguration portalConfiguration = queriesUT.loadConfig(22);
      assertThat(portalConfiguration.getMaxUploadSize(), is(5));
   }
}
