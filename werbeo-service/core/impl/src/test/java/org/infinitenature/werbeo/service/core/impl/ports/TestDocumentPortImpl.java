package org.infinitenature.werbeo.service.core.impl.ports;

import org.apache.commons.io.FileUtils;
import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.common.position.impl.PositionFactoryImpl;
import org.infinitenature.werbeo.service.core.api.enity.HerbarySheet;
import org.infinitenature.werbeo.service.core.api.enity.HerbarySheetComplete;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enums.HerbarySheetPosition;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.pdfbox.pdmodel.PDDocument;
import org.pdfbox.util.PDFTextStripper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDocumentPortImpl
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TestDocumentPortImpl.class);
   public static final String INSTITUTION_TEST = "institutionTest";
   public static final String AUTHOR_TEST = "authorTest";
   public static final String GERMANNAME_TEST = "germannameTest";
   public static final String HABITAT_TEST = "habitatTest";
   public static final String HERBARY_TEST = "herbaryTest";
   public static final String PERSON_TEST = "personTest";
   public static final String RECORDER_TEST = "recorderTest";
   public static final String LATINNAME_TEST = "latinnameTest";
   public static final String EPSG_TEST = "epsgTest";
   public static final String DATE_TEST = "dateTest";
   public static final String COORDINATES_TEST = "coordinatesTest";
   public static final String BLUR_TEST = "blurTest";
   public static final String UUID_TEXT = "uuidText";

   private static final UUID mtbOccurrenceId = UUID.randomUUID();
   private DocumentPortImpl documentPort;

   @BeforeEach
   void setUp()
   {
      PositionFactory positionFactory = new PositionFactoryImpl();
      OccurrenceQueryPort occurrenceQueryPortMock = mock(
            OccurrenceQueryPort.class);
      Occurrence mtbOccurrence = new Occurrence();
      TaxonBase taxon = new TaxonBase();
      taxon.setName("Abies alba");
      mtbOccurrence.setTaxon(taxon);
      SampleBase sample = new SampleBase();
      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(positionFactory.createFromMTB("1943/123"));
      sample.setDate(VagueDateFactory.create(LocalDate.of(1990, 11, 2),
            LocalDate.of(1990, 11, 5),
            VagueDate.Type.DAYS.getStringRepresentation()));
      mtbOccurrence.setSample(sample);
      mtbOccurrence.setId(mtbOccurrenceId);
      when(occurrenceQueryPortMock.get(eq(mtbOccurrenceId), any()))
            .thenReturn(mtbOccurrence);
      documentPort = new DocumentPortImpl(occurrenceQueryPortMock);
   }

   @Test
   void testMakePdf() throws IOException
   {
      HerbarySheetComplete herbarySheetComplete;
      String file = documentPort
            .getTemplateFileName(HerbarySheetPosition.TOP_RIGHT);
      herbarySheetComplete = new HerbarySheetComplete();
      herbarySheetComplete.setInstitution(INSTITUTION_TEST);
      herbarySheetComplete.setAuthor(AUTHOR_TEST);
      herbarySheetComplete.setSynonym(GERMANNAME_TEST);
      herbarySheetComplete.setHabitat(HABITAT_TEST);
      herbarySheetComplete.setHerbary(HERBARY_TEST);
      herbarySheetComplete.setDeterminer(PERSON_TEST);
      herbarySheetComplete.setRecorder(RECORDER_TEST);
      herbarySheetComplete.setLatinname(LATINNAME_TEST);
      herbarySheetComplete.setEpsg(EPSG_TEST);
      herbarySheetComplete.setDate(DATE_TEST);
      herbarySheetComplete.setCoordinates(COORDINATES_TEST);
      herbarySheetComplete.setBlur(BLUR_TEST);
      herbarySheetComplete.setUuid(UUID_TEXT);
      ByteArrayOutputStream byteArrayOutputStream = documentPort
            .makePdf(herbarySheetComplete, file);
      String text = null;

      PDDocument pdfDocument = PDDocument.load(
            new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
      text = new PDFTextStripper().getText(pdfDocument);
      pdfDocument.close();

      assertTrue(text.contains(INSTITUTION_TEST));
      assertTrue(text.contains(AUTHOR_TEST));
      assertTrue(text.contains(GERMANNAME_TEST));
      assertTrue(text.contains(HABITAT_TEST));
      assertTrue(text.contains(HERBARY_TEST));
      assertTrue(text.contains(PERSON_TEST));
      assertTrue(text.contains(RECORDER_TEST));
      assertTrue(text.contains(LATINNAME_TEST));
      assertTrue(text.contains(EPSG_TEST));
      assertTrue(text.contains(DATE_TEST));
      assertTrue(text.contains(COORDINATES_TEST));
      assertTrue(text.contains(BLUR_TEST));
      assertTrue(text.contains(UUID_TEXT));
   }

   @Test
   void test002() throws IOException
   {
      HerbarySheet sheet = new HerbarySheet();
      File file = File.createTempFile("test", ".pdf");
      System.out.println(file);
      byte[] byteArray = documentPort.createHerbaryPage(null,
            mtbOccurrenceId, HerbarySheetPosition.BOTTOM_RIGHT, sheet).toByteArray();

      String text = null;

      PDDocument pdfDocument = PDDocument
            .load(new ByteArrayInputStream(byteArray));
      text = new PDFTextStripper().getText(pdfDocument);
      pdfDocument.close();
      FileUtils.writeByteArrayToFile(file, byteArray);
      LOGGER.info("Pdf written to file: {}", file);
      assertThat(text.contains("Koordinaten: MTB 1943/123"), is(true));
      assertThat(text.contains("Koordinatenrefsys: Messtischblatt"),
            is(true));
      LOGGER.error("TestDocumentPortImpl: text = " + text);
      assertThat(text.contains("Funddatum: 02.11.1990 - 05.11.1990"),
            is(true));

   }
}
