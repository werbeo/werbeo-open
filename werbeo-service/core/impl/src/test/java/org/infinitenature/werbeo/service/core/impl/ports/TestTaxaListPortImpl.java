package org.infinitenature.werbeo.service.core.impl.ports;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxaListQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestTaxaListPortImpl
{
   private TaxaListPortImpl portUT;

   @BeforeEach
   void setUp() throws Exception
   {

      TaxaListQueries repo = mock(TaxaListQueries.class);

      TaxaList taxaList = new TaxaList();
      taxaList.getTaxaGroups().add("GROUP_A");
      taxaList.getTaxaGroups().add("GROUP_B");
      when(repo.get(eq(1), any(Context.class))).thenReturn(taxaList);
      InstanceConfig instanceConfig = new InstanceConfig();
      Set<String> groups = new HashSet<>();
      groups.add("GROUP_A");
      groups.add("GROUP_C");
      instanceConfig.getTaxonGroupsPerPortal().put(1, groups);
      portUT = new TaxaListPortImpl(repo, instanceConfig);
   }

   @Test
   void testGet_filteredGroups()
   {
      TaxaList taxaList = portUT.get(1, createContext(1));
      assertThat(taxaList.getTaxaGroups(), hasSize(1));
      assertThat(taxaList.getTaxaGroups(), contains("GROUP_A"));
   }

   @Test
   void testGet_unfilteredGroups()
   {
      TaxaList taxaList = portUT.get(1, createContext(2));
      assertThat(taxaList.getTaxaGroups(), hasSize(2));
      assertThat(taxaList.getTaxaGroups(),
            containsInAnyOrder("GROUP_A", "GROUP_B"));
   }

   private Context createContext(int portalId)
   {
      return new Context(new Portal(portalId), User.INTERNAL_SUPER_USER,
            Collections.emptySet(), "test");
   }

}
