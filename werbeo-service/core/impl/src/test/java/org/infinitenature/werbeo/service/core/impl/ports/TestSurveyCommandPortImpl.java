package org.infinitenature.werbeo.service.core.impl.ports;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ValidationException;
import javax.validation.Validator;

import org.infinitenature.werbeo.service.core.access.AccessRightsCheck;
import org.infinitenature.werbeo.service.core.access.AccessRightsChecker;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceIndexCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.SurveyCommands;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestSurveyCommandPortImpl
{
   private SurveyCommandPortImpl portUT;
   private Validator validatorMock;
   private OccurrenceIndexCommands occurrenceIndexCommandsMock;
   private SurveyCommands surveyCommandsMock;
   private SurveyQueryPort surveyQueriesMock;
   private OccurrenceQueryPort occurrenceQueryPortMock;

   @BeforeEach
   void setUp() throws Exception
   {
      portUT = new SurveyCommandPortImpl();
      validatorMock = mock(Validator.class);
      portUT.setValidator(validatorMock);
      occurrenceIndexCommandsMock = mock(OccurrenceIndexCommands.class);
      portUT.setOccurrenceIndexCommands(occurrenceIndexCommandsMock);
      surveyCommandsMock = mock(SurveyCommands.class);
      portUT.setSurveyCommands(surveyCommandsMock);
      surveyQueriesMock = mock(SurveyQueryPort.class);
      portUT.setSurveyQueryPort(surveyQueriesMock);
      List<AccessRightsCheck> accessRightsChecks = new ArrayList<>();
      portUT.setAccesRightsChecker(new AccessRightsChecker(accessRightsChecks));
      occurrenceQueryPortMock = mock(OccurrenceQueryPort.class);
      portUT.setOccurrenceQueryPort(occurrenceQueryPortMock);
   }

   @Test
   @DisplayName("Update survey")
   void test001()
   {
      Survey survey = new Survey();
      survey.setId(22);

      Context context = mock(Context.class);
      portUT.saveOrUpdate(survey, context);

      verify(validatorMock).validate(survey);
      verify(surveyCommandsMock).saveOrUpdate(survey, context);
      verify(occurrenceIndexCommandsMock).update(survey);
   }

   @Test
   @DisplayName("new surveys may be container")
   void test002()
   {
      Survey survey = new Survey();
      survey.setContainer(true);

      Context context = mock(Context.class);
      portUT.saveOrUpdate(survey, context);

      verify(validatorMock).validate(survey);
      verify(surveyCommandsMock).saveOrUpdate(survey, context);
      verify(occurrenceIndexCommandsMock, times(0)).update(survey);
   }

   @Test
   @DisplayName("empty survey may be container")
   void test003()
   {
      when(occurrenceQueryPortMock.count(any(), any())).thenReturn(0l);
      Survey survey = new Survey();
      survey.setId(123);
      survey.setContainer(true);

      Context context = mock(Context.class);
      portUT.saveOrUpdate(survey, context);

      verify(validatorMock).validate(survey);
      verify(surveyCommandsMock).saveOrUpdate(survey, context);
      verify(occurrenceIndexCommandsMock, times(1)).update(survey);
   }

   @Test
   @DisplayName("non-empty survey must not be container")
   void test004()
   {
      when(occurrenceQueryPortMock.count(any(), any())).thenReturn(1l);
      Survey survey = new Survey();
      survey.setId(123);
      survey.setContainer(true);

      Context context = mock(Context.class);
      ValidationException exception = assertThrows(ValidationException.class,
            () -> portUT.saveOrUpdate(survey, context));

      assertThat(exception.getMessage(),
            is("Container surveys may not include observation data"));
   }
}
