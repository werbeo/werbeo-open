package org.infinitenature.werbeo.service.core.impl.export;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Export;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceExportSortField;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceExportCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestExportFacade
{
   private ExportFacade exportFacadeUT;

   @BeforeEach
   void setUp() throws Exception
   {
      Path exportPath = Files
            .createTempDirectory(TestExportFacade.class.getSimpleName());
      InstanceConfig instanceConfig = new InstanceConfig();
      instanceConfig.setDir(exportPath.toString());

      OccurrenceQueryPort occurrenceQueries = mock(OccurrenceQueryPort.class);
      List<OccurrenceExportCommands> occurrenceExports = new ArrayList<>();
      OccurrenceExportCommands command = mock(OccurrenceExportCommands.class);
      when(command.getSupportedFileFormat()).thenReturn(FileFormat.CSV);
      occurrenceExports.add(command);

      FieldConfigQueries fieldConfigQueries = mock(FieldConfigQueries.class);

      exportFacadeUT = new ExportFacade(occurrenceQueries,
            instanceConfig, occurrenceExports, fieldConfigQueries);
   }

   @Test
   @DisplayName("Test getting exports where no exports exist")
   void test001()
   {
      Context context = createContext(1);

      assertThat(exportFacadeUT
            .findExports(context,
                  unlimitedResults())
            .getContent().size(), is(0));
   }

   private OffsetRequestImpl<OccurrenceExportSortField> unlimitedResults()
   {
      return new OffsetRequestImpl<>(0, Integer.MAX_VALUE,
            SortOrder.ASC, OccurrenceExportSortField.ID);
   }

   @Test
   @DisplayName("Test getting exports where one finished exports exist")
   void test002()
   {
      Context context = createContext(1);

      UUID exportID = UUID.randomUUID();
      exportFacadeUT.createExport(new OccurrenceFilter(), FileFormat.CSV,
            exportID, context);

      List<Export> exports = exportFacadeUT
            .findExports(context, unlimitedResults()).getContent();

      assertThat(exports.size(), is(1));

      Export newExport = new Export(exportID, JobStatus.FINISHED, FileFormat.CSV.getSuffix(), null);
      assertThat(exports.get(0).getId().compareTo(newExport.getId()), is(0));
      assertThat(exports.get(0).getFileFormat().equals(newExport.getFileFormat()), is(true));
   }

   @Test
   @DisplayName("Test portal separation")
   void test003()
   {
      Context contextPortal1 = createContext(1);

      UUID exportID = UUID.randomUUID();
      exportFacadeUT.createExport(new OccurrenceFilter(), FileFormat.CSV,
            exportID, contextPortal1);

      List<Export> exports = exportFacadeUT
            .findExports(contextPortal1, unlimitedResults()).getContent();

      assertThat(exports.size(), is(1));

      Export newExport = new Export(exportID, JobStatus.FINISHED, FileFormat.CSV.getSuffix(), null);
      assertThat(exports.get(0).getId().compareTo(newExport.getId()), is(0));
      assertThat(exports.get(0).getFileFormat().equals(newExport.getFileFormat()), is(true));

      Context contextPortal2 = createContext(2);
      List<Export> exportsPortal2 = exportFacadeUT
            .findExports(contextPortal2, unlimitedResults()).getContent();

      assertThat(exportsPortal2.size(), is(0));
   }

   private Context createContext(int portalId)
   {
      User user = new User();
      user.setLogin("test@test.de");
      Portal portal = new Portal();
      portal.setId(portalId);
      return new Context(portal, user, Collections.emptySet(), "test");
   }

}
