package org.infinitenature.werbeo.service.core;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestPositionUtils
{

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   void test_001()
   {
      MTB mtb = MTBHelper.toMTB("1943");

      String wkt = mtb.toWkt();
      int precision = PositionUtils.calculatePrecision(wkt);
      assertThat(precision, is(7286));
   }

   @Test
   void test_002()
   {
      MTB mtb = MTBHelper.toMTB("1943/1");
      String wkt = mtb.toWkt();
      int precision = PositionUtils.calculatePrecision(wkt);
      assertThat(precision, is(3643));
   }

   @Test
   void test_003()
   {
      MTB mtb = MTBHelper.toMTB("1943/11");
      String wkt = mtb.toWkt();
      int precision = PositionUtils.calculatePrecision(wkt);
      assertThat(precision, is(1821));
   }

   @Test
   void test_004()
   {
      MTB mtb = MTBHelper.toMTB("1943/111");
      String wkt = mtb.toWkt();
      int precision = PositionUtils.calculatePrecision(wkt);
      assertThat(precision, is(910));
   }

}
