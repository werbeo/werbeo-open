package org.infinitenature.werbeo.service.core.impl.validation;


import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.TaxonQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.meta.FieldConfigQuieresImpl;
import org.infinitenature.werbeo.service.core.impl.ports.PortalQueryPortImpl;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.togglz.core.manager.FeatureManager;

@SpringBootApplication
public class TestApp
{
   @Bean
   public InstanceConfig instanceConfg()
   {
      return new InstanceConfig();
   }

   @Bean
   public FieldConfigQueries fieldConfigQueries()
   {
      return new FieldConfigQuieresImpl(mock(FeatureManager.class));
   }

   @Bean
   public PortalQueryPort portalQueryPort()
   {
      return new PortalQueryPortImpl(new InstanceConfig(),
            mock(PortalQueries.class));
   }

   @Bean
   public TaxonQueryPort taxonQuerPort()
   {
      TaxonQueryPort taxonQueryPortMock = mock(TaxonQueryPort.class);
      when(taxonQueryPortMock.get(not(eq(1)), any(Context.class)))
            .thenThrow(EntityNotFoundException.class);
      return taxonQueryPortMock;
   }
}
