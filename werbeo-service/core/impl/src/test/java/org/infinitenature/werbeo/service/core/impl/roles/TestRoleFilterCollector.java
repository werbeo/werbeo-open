package org.infinitenature.werbeo.service.core.impl.roles;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;

import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.infra.roles.RoleFilter;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.junit.jupiter.api.Test;

class TestRoleFilterCollector
{
   private RoleFilterCollector collectorUT = new RoleFilterCollector();
   private Supplier<Map<Filter, Collection<Portal>>> supplierUT = collectorUT
         .supplier();
   private BiConsumer<Map<Filter, Collection<Portal>>, RoleFilter> accumulatorUT = collectorUT
         .accumulator();
   private BinaryOperator<Map<Filter, Collection<Portal>>> combinerUT = collectorUT
         .combiner();

   @Test
   void test_001()
   {
      assertThat(supplierUT.get(), is(new HashMap<>()));
   }

   @Test
   void test_002()
   {
      Map<Filter, Collection<Portal>> map = supplierUT.get();
      accumulatorUT.accept(map, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(1)));
      assertThat(map.size(), is(1));
      assertThat(map.containsKey(Filter.ONLY_OWN_DATA), is(true));
      assertThat(map.get(Filter.ONLY_OWN_DATA).size(), is(1));
      assertThat(map.get(Filter.ONLY_OWN_DATA).contains(new Portal(1)),
            is(true));
   }

   @Test
   void test_003()
   {
      Map<Filter, Collection<Portal>> map = supplierUT.get();
      accumulatorUT.accept(map, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(1)));
      accumulatorUT.accept(map, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(2)));
      assertThat(map.size(), is(1));
      assertThat(map.containsKey(Filter.ONLY_OWN_DATA), is(true));
      assertThat(map.get(Filter.ONLY_OWN_DATA).size(), is(2));
      assertThat(map.get(Filter.ONLY_OWN_DATA).contains(new Portal(1)),
            is(true));
      assertThat(map.get(Filter.ONLY_OWN_DATA).contains(new Portal(2)),
            is(true));
   }

   @Test
   void test_004()
   {
      assertThat(combinerUT.apply(supplierUT.get(), supplierUT.get()),
            is(new HashMap<>()));
   }

   @Test
   void test_005()
   {
      Map<Filter, Collection<Portal>> map = new HashMap<>();
      accumulatorUT.accept(map, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(1)));

      assertThat(combinerUT.apply(supplierUT.get(), map), is(map));
   }

   @Test
   void test_006()
   {
      Map<Filter, Collection<Portal>> map = new HashMap<>();
      accumulatorUT.accept(map, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(1)));

      assertThat(combinerUT.apply(map, supplierUT.get()), is(map));
   }

   @Test
   void test_007()
   {
      Map<Filter, Collection<Portal>> map = new HashMap<>();
      accumulatorUT.accept(map, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(1)));

      assertThat(combinerUT.apply(map, map), is(map));
   }

   @Test
   void test_008()
   {
      Map<Filter, Collection<Portal>> map1 = new HashMap<>();
      accumulatorUT.accept(map1, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(1)));
      Map<Filter, Collection<Portal>> map2 = new HashMap<>();
      accumulatorUT.accept(map2, new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(2)));
      Map<Filter, Collection<Portal>> resultMap = combinerUT.apply(map1, map2);
      assertThat(resultMap.keySet(), hasSize(1));
   }
}
