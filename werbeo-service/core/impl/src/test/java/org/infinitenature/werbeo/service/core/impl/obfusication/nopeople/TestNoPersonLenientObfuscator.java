package org.infinitenature.werbeo.service.core.impl.obfusication.nopeople;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestNoPersonLenientObfuscator
{
   private Context context;
   private AbstractNoPersonObfuscator noPersonLenientObfuscator;

   @BeforeEach
   public void setUp() throws Exception
   {
      context = mock(Context.class);
      noPersonLenientObfuscator = new NoPersonSampleObfuscator();
   }

   @Test
   @DisplayName("user not logged in")
   public void test001()
   {
      when(context.isUserLoggedIn()).thenReturn(false);
      assertThat(
            noPersonLenientObfuscator.isObfuscationNecessary(createSample(),
                  context),
            is(true));
   }

   @Test
   @DisplayName("user logged in, user is allowed to edit")
   public void test002()
   {
      when(context.isUserLoggedIn()).thenReturn(true);
      Sample sampleBase = createSample();
      sampleBase.getAllowedOperations().add(Operation.UPDATE);
      assertThat(
            noPersonLenientObfuscator.isObfuscationNecessary(sampleBase,
                  context),
            is(false));
   }

   @Test
   @DisplayName("user logged in, user is not allowed to edit, user is approved")
   public void test003()
   {
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.isUserApproved()).thenReturn(true);
      assertThat(
            noPersonLenientObfuscator.isObfuscationNecessary(createSample(),
                  context),
            is(true));
   }

   @Test
   @DisplayName("user logged in, user is not allowed to edit, user is not approved")
   public void test004()
   {
      when(context.isUserLoggedIn()).thenReturn(true);
      when(context.isUserApproved()).thenReturn(false);
      assertThat(
            noPersonLenientObfuscator.isObfuscationNecessary(createSample(),
                  context),
            is(true));
   }
   
   private Sample createSample()
   {
      Locality locality = new Locality();

      locality.setLocality("locality");
      locality.setLocationComment("locationComment");
      locality.setPrecision(300);
      Sample sample = new Sample();
      sample.setLocality(locality);
      sample.setSurvey(new Survey());
      return sample;
   }
}
