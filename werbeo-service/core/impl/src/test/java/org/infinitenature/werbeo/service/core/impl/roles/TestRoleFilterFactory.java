package org.infinitenature.werbeo.service.core.impl.roles;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.common.commons.roles.RoleNameFactory;
import org.infinitenature.werbeo.common.commons.roles.Roles;
import org.infinitenature.werbeo.service.core.api.enity.Group;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.infra.roles.RoleFilter;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestRoleFilterFactory
{
   private RoleFilterFactory roleFilterFactoryUT;
   private PortalQueries portalQueriesMock;

   @BeforeEach
   void setUp() throws Exception
   {
      portalQueriesMock = mock(PortalQueries.class);
      List<Portal> assosiatedTo1 = new ArrayList<>();
      assosiatedTo1.add(new Portal(1));
      assosiatedTo1.add(new Portal(2));
      when(portalQueriesMock.findAssociated(1)).thenReturn(new SliceImpl<>(
            assosiatedTo1,
            new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                  PortalSortField.ID)));

      List<Portal> assosiatedTo2 = new ArrayList<>();
      assosiatedTo2.add(new Portal(2));
      when(portalQueriesMock.findAssociated(2)).thenReturn(
            new SliceImpl<>(assosiatedTo2, new OffsetRequestImpl<>(0,
                  Integer.MAX_VALUE, SortOrder.ASC, PortalSortField.ID)));

      PortalQueryPort portalQueryPortMock = mock(PortalQueryPort.class);
      when(portalQueryPortMock.loadConfig(anyInt()))
            .thenReturn(new PortalConfiguration());
      roleFilterFactoryUT = new RoleFilterFactory(
            new FilterQueriesImpl(portalQueryPortMock),
            portalQueriesMock);
   }

   @Test
   void test_001()
   {
      RoleFilter roleFilter = new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(2));
      Set<Group> groups = new HashSet<>();
      groups.add(new Group(RoleNameFactory.getKeycloakRoleName(2, Roles.APPROVED)));
      Context context = new Context(new Portal(), new User(), groups, "test");

      assertThat(roleFilterFactoryUT.isActive(roleFilter, context), is(false));
   }

   @Test
   void test_002()
   {
      RoleFilter roleFilter = new RoleFilter(Roles.APPROVED,
            Filter.ONLY_OWN_DATA, new Portal(2));
      Set<Group> groups = new HashSet<>();
      Context context = new Context(new Portal(), new User(), groups, "test");

      assertThat(roleFilterFactoryUT.isActive(roleFilter, context), is(true));
   }

   @Test
   void test_003()
   {
      Context context = new Context(new Portal(1), new User(),
            Collections.emptySet(), "test");
      Map<Filter, Collection<Portal>> activeFilter = roleFilterFactoryUT
            .findActiveFilter(context);

      assertThat(activeFilter.size(), is(1));
      assertThat(activeFilter.get(Filter.ONLY_OWN_DATA), hasSize(2));
   }
}
