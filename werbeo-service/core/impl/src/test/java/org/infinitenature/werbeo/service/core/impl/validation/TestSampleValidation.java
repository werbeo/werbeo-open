package org.infinitenature.werbeo.service.core.impl.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.test.support.OccurrenceTestUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestApp.class)
public class TestSampleValidation
{
   @Autowired
   private Validator validator;

   @BeforeEach()
   void initContext()
   {
      Portal portal = new Portal(PortalTestUtil.FLORA_MV_ID);
      portal.setTitle("flora-mv");
      ValidationContextHolder.setThreadLocalContext(
            new Context(portal, new User(), new HashSet<>(), "test"));
   }

   @AfterEach
   void cleanUp()
   {
      ValidationContextHolder.removeThreadLocalContext();
   }

   @Test
   @DisplayName("Sample has no UUID")
   void testValidateSample001()
   {

      Sample sample = OccurrenceTestUtil.validSample();
      sample.setId(null);
      Set<ConstraintViolation<Sample>> validationResult = validator
            .validate(sample);
      assertThat(validationResult, hasSize(1));
      ConstraintViolation<?> v = validationResult.iterator().next();
      assertThat(v.getPropertyPath().toString(), is("id"));
   }

   @Test
   @DisplayName("Valid sample")
   void testValidateSample002()
   {
      Sample sample = OccurrenceTestUtil.validSample();

      assertThat(validator.validate(sample), hasSize(0));

   }

   @Test
   @DisplayName("Sampel without precision")
   void testValidateSample003()
   {

      Sample sample = OccurrenceTestUtil.validSample();
      sample.getLocality().setPrecision(null);

      Set<ConstraintViolation<Sample>> validationResult = validator
            .validate(sample);

      assertThat(validationResult.size(), is(1));
      ConstraintViolation<?> v = validationResult.iterator().next();
      assertThat(v.getPropertyPath().toString(), is("locality.precision"));

   }

   @Test
   @DisplayName("Check mandantory field")
   void test()
   {
      Occurrence occ = OccurrenceTestUtil.validOccurrence();
      occ.setTaxon(null);
      Set<ConstraintViolation<Occurrence>> result = validator.validate(occ);

      assertThat(result, hasSize(1));

      ConstraintViolation<?> violation = result.iterator().next();

      assertThat(violation.getMessageTemplate(),
            is("javax.validation.constraints.NotNull.message"));
      assertThat(violation.getPropertyPath().toString(), is("taxon"));
   }

   @Test
   @DisplayName("Check not configured field")
   void test002()
   {

      Occurrence occ = OccurrenceTestUtil.validOccurrence();

      // not allowd field for folra-mv
      occ.setSettlementStatus(SettlementStatus.PLANTED);
      Set<ConstraintViolation<Occurrence>> result = validator.validate(occ);

      assertThat(result, hasSize(1));

      ConstraintViolation<?> violation = result.iterator().next();

      assertThat(violation.getMessageTemplate(),
            is("werbeo.fieldnotactivated.message"));
      assertThat(violation.getPropertyPath().toString(),
            is("settlementStatus"));
   }

   @Test
   @DisplayName("Taxon not acitve in portal")
   void test003()
   {
      Occurrence occurrence = OccurrenceTestUtil.validOccurrence();
      occurrence.getTaxon().setId(99999);
      Set<ConstraintViolation<Occurrence>> result = validator
            .validate(occurrence);

      assertThat(result, hasSize(1));

      ConstraintViolation<?> violation = result.iterator().next();

      assertThat(violation.getMessageTemplate(),
            is("werbeo.taxonnotactiveonportal.message"));
      assertThat(violation.getPropertyPath().toString(), is("taxon"));
   }
}
