package org.infinitenature.werbeo.service.core.impl.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.ports.MediaUpload;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TestApp.class)
public class TestMediaUploadValidation
{
   @Autowired
   private Validator validator;

   @BeforeEach()
   void initContext()
   {
      Portal portal = new Portal(PortalTestUtil.FLORA_MV_ID);
      portal.setTitle("flora-mv");

      ValidationContextHolder.setThreadLocalContext(
            new Context(portal, new User(), new HashSet<>(), "test"));
   }

   @AfterEach
   void cleanUp()
   {
      ValidationContextHolder.removeThreadLocalContext();
   }

   @Test
   @DisplayName("Valid upload")
   void test001()
   {
      MediaUpload mediaUpload = new MediaUpload(new byte[34], "a name",
            "a description");
      Set<ConstraintViolation<MediaUpload>> validationResult = validator
            .validate(mediaUpload);
      assertThat(validationResult, hasSize(0));
   }

   @Test
   @DisplayName("upload data too big")
   void test002()
   {
      MediaUpload mediaUpload = new MediaUpload(new byte[4 * 1014 * 1024],
            "a name", "a description");
      Set<ConstraintViolation<MediaUpload>> validationResult = validator
            .validate(mediaUpload);
      assertThat(validationResult, hasSize(1));
      ConstraintViolation<?> v = validationResult.iterator().next();
      assertThat(v.getPropertyPath().toString(), is("mediaData"));
   }

   @Test
   @DisplayName("Valid no name")
   void test003()
   {
      MediaUpload mediaUpload = new MediaUpload(new byte[34], "",
            "a description");
      Set<ConstraintViolation<MediaUpload>> validationResult = validator
            .validate(mediaUpload);
      assertThat(validationResult, hasSize(1));
      ConstraintViolation<?> v = validationResult.iterator().next();
      assertThat(v.getPropertyPath().toString(), is("fileName"));
   }

   @Test
   @DisplayName("no description")
   void test004()
   {
      MediaUpload mediaUpload = new MediaUpload(new byte[4], "a name", null);
      Set<ConstraintViolation<MediaUpload>> validationResult = validator
            .validate(mediaUpload);
      assertThat(validationResult, hasSize(1));
      ConstraintViolation<?> v = validationResult.iterator().next();
      assertThat(v.getPropertyPath().toString(), is("description"));
   }
}
