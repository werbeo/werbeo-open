package org.infinitenature.werbeo.service.core.impl.validation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbeo.service.core.api.validation.WKTValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestWKTValidatorImpl
{

   private WKTValidator validatorUT = new WKTValidatorImpl();

   @Test
   @DisplayName("Test that no given value is a valid one")
   void test001()
   {
      assertThat(validatorUT.isValid(null), is(true));
   }

   @Test
   @DisplayName("Valid WKT")
   void test002()
   {
      assertThat(
            validatorUT.isValid(GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326),
            is(true));
   }

   @Test
   @DisplayName("Invalid WKT - wrong name")
   void test003()
   {
      assertThat(
            validatorUT.isValid(
                  GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326.substring(1)),
            is(false));
   }
}
