package org.infinitenature.werbeo.service.core.impl.ports.taxon;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

class TestTaxonCache
{
   private TaxonCache taxonCache = new TaxonCache();
   @Test
   void test()
   {
      assertThat(taxonCache.matches("Abies alba", "ab al"), is(true));
   }

   void test002()
   {
      assertThat(taxonCache.matches("Pseudochorthippus blbl", "Chorthippus"),
            is(false));
   }

   void test003()
   {
      assertThat(taxonCache.matches("Abies alba", " al"), is(true));
   }

}
