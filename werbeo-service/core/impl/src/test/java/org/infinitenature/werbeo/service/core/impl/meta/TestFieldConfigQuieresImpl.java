package org.infinitenature.werbeo.service.core.impl.meta;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.mock;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.togglz.core.manager.FeatureManager;

class TestFieldConfigQuieresImpl
{
   private FieldConfigQueries quieresUT;

   @BeforeEach
   void setUp() throws Exception
   {

      quieresUT = new FieldConfigQuieresImpl(mock(FeatureManager.class));
   }

   @Test
   @DisplayName("Survey fields")
   void test001()
   {
      Portal portal = new Portal(1);
      portal.setTitle("Demonstration Website");
      SurveyConfig surveyFieldConfig = quieresUT.getSurveyConfig(portal);

      assertAll(
            () -> assertThat(surveyFieldConfig.getConfiguredFields(),
                  hasSize(5)),
            () -> assertThat(surveyFieldConfig.getConfiguredFields(),
                  containsInAnyOrder(
                        new SurveyFieldConfig(SurveyField.AVAILABILITY, true),
                        new SurveyFieldConfig(SurveyField.ID, false),
                        new SurveyFieldConfig(SurveyField.DESCRIPTION, true),
                        new SurveyFieldConfig(SurveyField.NAME, true),
                        new SurveyFieldConfig(SurveyField.WERBEO_ORIGINAL,
                              true))));
   }

}
