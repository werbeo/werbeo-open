package org.infinitenature.werbeo.service.core.impl.obfusication;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestChainedEntityObfusicator
{

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   @DisplayName("Test that entity is returned")
   void test001()
   {
      Context context = mock(Context.class);
      ChainedEntityObfuscator<Sample> obfusicatorUT = new ChainedEntityObfuscator<>();

      Sample sample = new Sample();

      assertThat(obfusicatorUT.obfuscate(sample, context), is(sample));
   }

}
