package org.infinitenature.werbeo.service.core.impl.obfusication.fields;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Collections;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.impl.meta.FieldConfigQuieresImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.togglz.core.manager.FeatureManager;

class TestNotConfiguredOccurrenceFieldsObfusicator
{
   private NotConfiguredOccurrenceFieldsObfusicator obfusicatorUT;

   @BeforeEach
   void setUp() throws Exception
   {
      obfusicatorUT = new NotConfiguredOccurrenceFieldsObfusicator(
            new FieldConfigQuieresImpl(mock(FeatureManager.class)));
   }

   @Test
   void test()
   {
      Occurrence occ = new Occurrence();
      occ.setAbsence(true);
      Portal portal = new Portal(PortalTestUtil.FLORA_MV_ID);
      portal.setTitle("flora-mv");
      obfusicatorUT.obfuscate(occ,
            new Context(portal, User.HIDDEN_USER, Collections.emptySet(),
                  "test"));
      assertThat(occ.getAbsence(), is(nullValue()));
   }

}
