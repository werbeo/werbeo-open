package org.infinitenature.werbeo.service.core.impl.validation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.validation.PolygonMaxSizeValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestPolygonMaxSizeValidatorImpl
{
   private PolygonMaxSizeValidator validatorUT = new PolygonMaxSizeValidatorImpl(
         new InstanceConfig());

   @Test
   @DisplayName("WKT ist counter clock wise")
   void test001()
   {
      assertThat(validatorUT.isPolygonSizeValid(
            GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326_CCW), is(false));
   }

   @Test
   @DisplayName("Has valid size")
   void test002()
   {
      assertThat(validatorUT.isPolygonSizeValid(
            GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326), is(true));
   }

   @Test
   @DisplayName("Has too big size")
   void test003()
   {
      assertThat(validatorUT.isPolygonSizeValid(GeomTestUtil.WKT_DE_APROX_4326),
            is(false));
   }
}
