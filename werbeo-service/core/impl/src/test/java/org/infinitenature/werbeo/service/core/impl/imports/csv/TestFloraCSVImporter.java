package org.infinitenature.werbeo.service.core.impl.imports.csv;

import static org.junit.jupiter.api.Assertions.*;

import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.junit.jupiter.api.Test;

public class TestFloraCSVImporter
{
   @Test
   void testProcessDate() throws Exception
   {
      FloraCSVImporter csvImporter = new FloraCSVImporter();
      
      Sample sample = new Sample();

      csvImporter.processDate("18.06.1982", sample);
      assertEquals("1982-06-18", sample.getDate().toString());
      
      csvImporter.processDate("00.06.1982", sample);
      assertEquals("1982-06", sample.getDate().toString());
      
      csvImporter.processDate("00.00.1982", sample);
      assertEquals("1982", sample.getDate().toString());
   }
}
