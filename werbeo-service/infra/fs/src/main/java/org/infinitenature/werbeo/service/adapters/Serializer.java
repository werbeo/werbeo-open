package org.infinitenature.werbeo.service.adapters;

import java.io.File;
import java.nio.file.Path;

import com.thoughtworks.xstream.XStream;

public class Serializer
{
   private static XStream xstream = new XStream();

   private Serializer()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static String serialize(Object obj)
   {
      return xstream.toXML(obj);
   }

   public static Object deserialize(File xml)
   {
      return xstream.fromXML(xml);
   }

   public static Object deserialize(Path path)
   {
      return deserialize(path.toFile());
   }

   public static Object deserialize(String xml)
   {
      return xstream.fromXML(xml);
   }
}
