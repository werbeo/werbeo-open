package org.infinitenature.werbeo.service.adapters;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.infinitenature.werbeo.service.core.api.infra.query.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class AcceptanceQueriesAdapter implements AcceptanceQueries
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(AcceptanceQueriesAdapter.class);

   private final Path acceptanceDir;

   public AcceptanceQueriesAdapter(@Autowired
         InstanceConfig instanceConfig)
   {
      acceptanceDir = Paths.get(instanceConfig.getDir(), "acceptance");
   }

   @Override
   public String getAcceptanceDeclaration(Portal portal)
   {
      Path privacyFile = acceptanceDir.resolve(portal.getId() + ".MD");
      if (!Files.exists(privacyFile))
      {
         throw new EntityNotFoundException(
               "Acceptance declaration file " + privacyFile.toString() + " not found.");
      }
      try
      {
         return FileUtils.readFileToString(privacyFile.toFile(),
               Charset.forName("utf8"));
      } catch (IOException e)
      {
         LOGGER.error("Failure reading file", e);
         throw new RuntimeException("Failure reading privacy declaration file",
               e);
      }
   }

   @Override
   public String getDefaultAcceptanceDeclaration()
   {
      try (InputStream is = getClass().getResourceAsStream("/default-acceptance.MD");)
      {
         return IOUtils.toString(is, Charset.forName("utf8"));
      } catch (IOException e)
      {
         throw new WerbeoException("Failure loading default privacy declaration");
      }
   }
}
