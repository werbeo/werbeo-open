package org.infinitenature.werbeo.service.adapters;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.DocumentType;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.infra.query.FileQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class FileQueriesAdapter implements FileQueries
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(FileQueriesAdapter.class);
   private final String dir;

   public FileQueriesAdapter(@Autowired InstanceConfig instanceConfig)
   {
      dir = instanceConfig.getDir();
   }

   @Override
   public Set<DocumentType> getDocumentsAvailable(TaxonBase taxon,
         Context context)
   {
      Set<DocumentType> documentsAvailable = new HashSet<>();

      for (DocumentType documentType : DocumentType.values())
      {
         if (Files.exists(createFileName(documentType,
               taxon, context.getPortal())))
         {
            documentsAvailable.add(documentType);
         }
      }

      return documentsAvailable;
   }

   private Path createFileName(DocumentType fileType, TaxonBase taxon,
         Portal portal)
   {
      switch (fileType)
      {
      case MAP_TAXON_VEGETWEB_STYLE:
         return Paths.get(dir, String.valueOf(portal.getId()), "maps",
               "vegetweb-style", taxon.getName() + ".png");
      case MAP_TAXON_VEGETWEB_STYLE_THUMB:
         return Paths.get(dir, String.valueOf(portal.getId()), "maps",
               "vegetweb-style", taxon.getName() + "_thumb.png");
      case MAP_TAXON_VEGETWEB_STYLE_THUMB_L:
         return Paths.get(dir, String.valueOf(portal.getId()), "maps",
               "vegetweb-style", taxon.getName() + "_thumb_l.png");
      case MAP_TAXON_FLORA_MV_STYLE:
         return Paths.get("/var/www/html/maps/", String.valueOf(portal.getId()),
               taxon.getGroup().substring(0, 1), taxon.getName() + ".png");
      case MAP_TAXON_FLORA_MV_STYLE_THUMB:
         return Paths.get("/var/www/html/maps/", String.valueOf(portal.getId()),
               taxon.getGroup().substring(0, 1), "thumbs",
               "thumb_" + taxon.getName() + ".png");
      default:
         LOGGER.error("Unknown file type: {}", fileType);
         return Paths.get(UUID.randomUUID().toString()); // return random not
                                                         // existing path name
      }
   }

   @Override
   public InputStream loadCsvTemplate(int portalId)
   {
      try
      {
         Resource resource = new ClassPathResource(
               "csvTemplates/" + portalId + "_template.csv");
         return resource.getInputStream();
      }
      catch(Exception e)
      {
         LOGGER.error("Error loading csv Template for portal "+ portalId, e);
         return null;
      }
   }
   
   @Override
   public InputStream loadCsvTemplateHelp(int portalId)
   {
      try
      {
         Resource resource = new ClassPathResource(
               "csvTemplates/" + portalId + "_template_help.txt");
         return resource.getInputStream();
      }
      catch(Exception e)
      {
         LOGGER.error("Error loading csv Template for portal "+ portalId, e);
         return null;
      }
   }

}
