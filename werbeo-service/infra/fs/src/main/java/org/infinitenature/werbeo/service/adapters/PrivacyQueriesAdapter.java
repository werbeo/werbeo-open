package org.infinitenature.werbeo.service.adapters;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.infinitenature.werbeo.service.core.api.infra.query.PrivacyQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class PrivacyQueriesAdapter implements PrivacyQueries
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PrivacyQueriesAdapter.class);

   private final Path privacyDir;

   public PrivacyQueriesAdapter(@Autowired
         InstanceConfig instanceConfig)
   {
      privacyDir = Paths.get(instanceConfig.getDir(), "privacy");
   }

   @Override
   public String getPrivacyDeclaration(Portal portal)
   {
      Path privacyFile = privacyDir.resolve(portal.getId() + ".MD");
      if (!Files.exists(privacyFile))
      {
         throw new EntityNotFoundException(
               "Privacy declaration file " + privacyFile.toString() + " not found.");
      }
      try
      {
         return FileUtils.readFileToString(privacyFile.toFile(),
               Charset.forName("utf8"));
      } catch (IOException e)
      {
         LOGGER.error("Failure reading file", e);
         throw new RuntimeException("Failure reading privacy declaration file",
               e);
      }
   }

   @Override
   public String getDefaultPrivacyDeclaration()
   {
      try (InputStream is = getClass().getResourceAsStream("/default-privacy.MD");)
      {
         return IOUtils.toString(is, Charset.forName("utf8"));
      } catch (IOException e)
      {
         throw new WerbeoException("Failure loading default privacy declaration");
      }
   }
}
