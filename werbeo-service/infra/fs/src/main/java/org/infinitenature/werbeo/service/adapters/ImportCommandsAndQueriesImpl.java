package org.infinitenature.werbeo.service.adapters;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.ImportPart;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.InternalException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.ImportQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImportCommandsAndQueriesImpl
      implements ImportCommands, ImportQueries
{
   private static final String RUNNING_LOCK = "running.lock";
   private static final String IMPORT_SOURCE_FILE_NAME = "bde.xml";
   private static final String IMPORT_SOURCE_FILE_NAME_CSV = "import.csv";
   private static final String IMPORT_PROTOCOL_FILE_NAME = "protocol.csv";
   private static final String IMPORT_ERROR_FILE_NAME = "csv-error.log";
   private static final String CONTEXT_FILE_NAME = "context.xml";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ImportCommandsAndQueriesImpl.class);
   private final Path importPath;

   public ImportCommandsAndQueriesImpl(@Autowired InstanceConfig instanceConfig)
   {
      importPath = Paths.get(instanceConfig.getDir(), "imports");
      try
      {
         Files.createDirectories(importPath);
      } catch (IOException e)
      {
         throw new RuntimeException("Failure creating dir for imports", e);
      }
   }

   @Override
   public ImportJob storeImportfile(String importXML, Context context)
   {
      UUID importJobId = UUID.randomUUID();
      ImportJob importJob = new ImportJob(importJobId, JobStatus.CREATING);
      try
      {
         Files.createDirectories(jobDir(context, importJob));
         writeToFile(importXML, IMPORT_SOURCE_FILE_NAME, context, importJob);
         writeToFile(Serializer.serialize(context), CONTEXT_FILE_NAME, context,
               importJob);

         JobStatus newStatus = JobStatus.INITIALIZED;
         return setJobStatus(importJob, newStatus, context);

      } catch (IOException e)
      {
         throw new RuntimeException("Failure creating dir for import job", e);
      }
   }

   private void writeToFile(String content, String fileName, Context context,
         ImportJob importJob) throws IOException
   {
      File file = jobDir(context, importJob).resolve(fileName).toFile();
      FileUtils.writeStringToFile(file, content, Charset.defaultCharset());
      LOGGER.info("Wrote to file {}", file);
   }

   @Override
   public void writeFailureReport(ImportJob importJob,
         Map<String, Set<UUID>> failures)
   {
      Context context = getContext(importJob);
      Path jobDir = jobDir(context, importJob);
      TreeSet<FailureEntry> orderedFailures = new TreeSet<>();
      failures.entrySet().stream()
            .map(entry -> new FailureEntry(entry.getKey(), entry.getValue()))
            .forEach(orderedFailures::add);

      try (PrintWriter printWriter = new PrintWriter(
            new FileWriter(jobDir.resolve("summary.xml").toFile())))
      {
         for (FailureEntry e : orderedFailures)
         {
            printWriter.println(e.getAmount());
            printWriter.println(e.getFailure());
            printWriter.println(e.getSampleIds());
         }
      } catch (IOException e)
      {
         LOGGER.error("Failure writing summary for job {}", importJob.getId(),
               e);
      }
   }

   private Path userDir(Context context)
   {
      return importPath.resolve(String.valueOf(context.getPortal().getId()))
            .resolve(String.valueOf(context.getUser().getLogin()));
   }

   private Path jobDir(Context context, ImportJob importJob)
   {
      JobStatus status = importJob.getStatus();
      UUID id = importJob.getId();
      return jobDir(id, status, context);
   }

   private Path jobDir(UUID id, JobStatus status, Context context)
   {
      return userDir(context).resolve(status.toString()).resolve(id.toString());
   }

   @Override
   public Optional<ImportJob> findOneImportJobByStatus(JobStatus status)
   {
      List<UUID> uuids = new ArrayList<>(1);// hack so it is virtual final...
      try
      {
         Files.walkFileTree(importPath, Collections.emptySet(), 4,
               new SimpleFileVisitor<Path>()
               {
                  @Override
                  public FileVisitResult visitFile(Path file,
                        BasicFileAttributes attrs) throws IOException
                  {
                     LOGGER.debug("Checking if {} is a initalize job", file);
                     if (file.getName(file.getNameCount() - 2).toString()
                           .equals(status.toString())
                           && isUUID(file.getName(file.getNameCount() - 1)))
                     {
                        UUID uuid = UUID.fromString(
                              file.getName(file.getNameCount() - 1).toString());
                        LOGGER.info("Found importJob {} in state {}", uuid,
                              status);
                        uuids.add(uuid);
                        return FileVisitResult.TERMINATE;
                     }
                     return super.visitFile(file, attrs);
                  }

                  private boolean isUUID(Path name)
                  {
                     try
                     {
                        UUID.fromString(name.toString());
                        return true;
                     } catch (Exception e)
                     {
                        return false;
                     }
                  }
               });
      } catch (IOException e)
      {
         LOGGER.error("Failure finding one job by status {}", status, e);
         return Optional.empty();
      }
      if (uuids.isEmpty())
      {
         return Optional.empty();
      } else
      {
         return Optional.of(new ImportJob(uuids.get(0), status));
      }
   }
   
   @Override
   public List<ImportJob> findImportJobsByStatus(JobStatus status)
   {
      List<ImportJob> jobs = new ArrayList<>();
      try
      {
         Files.walkFileTree(importPath, Collections.emptySet(), 4,
               new SimpleFileVisitor<Path>()
               {
                  @Override
                  public FileVisitResult visitFile(Path file,
                        BasicFileAttributes attrs) throws IOException
                  {
                     LOGGER.debug("Checking if {} is a initalize job", file);
                     if (file.getName(file.getNameCount() - 2).toString()
                           .equals(status.toString())
                           && isUUID(file.getName(file.getNameCount() - 1)))
                     {
                        UUID uuid = UUID.fromString(
                              file.getName(file.getNameCount() - 1).toString());
                        LOGGER.info("Found importJob {} in state {}", uuid,
                              status);
                        jobs.add(new ImportJob(uuid, status));
                     }
                     return super.visitFile(file, attrs);
                  }

                  private boolean isUUID(Path name)
                  {
                     try
                     {
                        UUID.fromString(name.toString());
                        return true;
                     } catch (Exception e)
                     {
                        return false;
                     }
                  }
               });
      } catch (IOException e)
      {
         LOGGER.error("Failure finding one job by status {}", status, e);
      }
      return jobs;
   }

   @Override
   public Context getContext(ImportJob importJob)
   {
      return (Context) Serializer
            .deserialize(loadFileForJob(importJob, CONTEXT_FILE_NAME));
   }

   @Override
   public String getBDEXML(ImportJob importJob)
   {
      String fileName = IMPORT_SOURCE_FILE_NAME;
      return loadFileForJob(importJob, fileName);
   }

   @Override
   public String getCSV(ImportJob importJob)
   {
      String fileName = IMPORT_SOURCE_FILE_NAME_CSV;
      return loadFileForJob(importJob, fileName);
   }

   @Override
   public String getCSVProtocol(ImportJob importJob)
   {
      String fileName = IMPORT_PROTOCOL_FILE_NAME;
      return loadFileForJob(importJob, fileName);
   }

   @Override
   public String getCSVErrors(ImportJob importJob)
   {
      String fileName = IMPORT_ERROR_FILE_NAME;
      return loadFileForJob(importJob, fileName);
   }


   private Optional<Path> findOneJobLevelFile(Predicate<Path> accecpt)
   {
      return findOneFile(accecpt, 4, importPath);
   }

   private Optional<Path> findOneFile(Predicate<Path> accept,
         int maxDepth, Path root)
   {
      List<Path> files = new ArrayList<>(1);// hack so it is effective final...
      try
      {
         Files.walkFileTree(root, Collections.emptySet(), maxDepth,
               new SimpleFileVisitor<Path>()
               {
                  @Override
                  public FileVisitResult visitFile(Path file,
                        BasicFileAttributes attrs) throws IOException
                  {
                     if (accept.test(file))
                     {
                        files.add(file);
                        return FileVisitResult.TERMINATE;
                     } else
                     {
                        return super.visitFile(file, attrs);
                     }
                  }
               });

      } catch (IOException e)
      {
         LOGGER.error("Error finding one file", e);
         return Optional.empty();
      }
      if (files.isEmpty())
      {
         return Optional.empty();
      } else
      {
         return Optional.of(files.get(0));
      }

   }

   private String loadFileForJob(ImportJob importJob, String fileName)
   {
      Optional<Path> result = findOneJobLevelFile(
            file -> file.getName(file.getNameCount() - 1).toString()
                  .equals(importJob.getId().toString()));
      if (!result.isPresent())
      {
         throw new EntityNotFoundException(
               "No job with uuid " + importJob.getId() + " found.");
      } else
      {
         try
         {
            return (FileUtils.readFileToString(
                  result.get().resolve(fileName).toFile(),
                  Charset.defaultCharset()));
         } catch (IOException e)
         {
            throw new InternalException("Failure reading file", e);
         }
      }
   }

   @Override
   public void addLogFile(ImportJob importJob, Path logFile, Context context)
   {
      Path jobDir = jobDir(context, importJob);

      try
      {
         FileUtils.moveFileToDirectory(logFile.toFile(), jobDir.toFile(),
               false);
      } catch (IOException e)
      {
         LOGGER.error("Failure adding logfile", e);
      }
   }

   @Override
   public void saveOrUpdate(ImportPart importPart)
   {
      String xml = Serializer.serialize(importPart);
      Context context = (Context) Serializer.deserialize(
            loadFileForJob(importPart.getImportJob(), CONTEXT_FILE_NAME));
      try
      {
         Optional<Path> existingPart = findImportPath(importPart, context);
         if (existingPart.isPresent())
         {
            Files.delete(existingPart.get());
         }

         Path importPartPath = importPartPath(importPart, context);
         Files.createDirectories(importPartPath.getParent());
         Files.write(importPartPath, xml.getBytes());
      } catch (IOException e)
      {
         LOGGER.error("Failure writing file to disk", e);
         throw new WerbeoException("Failure writing file to disk");
      }
   }

   private Optional<Path> findImportPath(ImportPart importPart, Context context)
   {
      for (JobStatus jobStatus : JobStatus.values())
      {
         Path pathToCheck = importPartPath(importPart, context, jobStatus);
         if (Files.exists(pathToCheck))
         {
            return Optional.of(pathToCheck);
         }
      }
      return Optional.empty();
   }

   private Path importPartPath(ImportPart importPart, Context context)
   {
      JobStatus status = importPart.getStatus();
      return importPartPath(importPart, context, status);
   }

   private Path importPartPath(ImportPart importPart, Context context,
         JobStatus status)
   {
      return jobDir(context, importPart.getImportJob())
            .resolve(status.toString())
            .resolve(importPart.getSample().getId() + ".xml");
   }

   @Override
   public boolean isPartExisting(ImportJob importJob, JobStatus... partStatus)
   {
      Context context = getContext(importJob);
      Optional<Path> pathToPart = findPathToPart(importJob, context,
            partStatus);
      return pathToPart.isPresent();
   }

   @Override
   public boolean isJobNotFinished(ImportJob importJob)
   {
      Context context = getContext(importJob);
      Path jobDir = jobDir(context, importJob);
      Path lock = jobDir.resolve(RUNNING_LOCK);
      return Files.exists(lock);
   }

   @Override
   public void removeLock(ImportJob importJob)
   {
      Context context = getContext(importJob);
      Path jobDir = jobDir(context, importJob);
      Optional<Path> pathToPart = findPathToPart(importJob, context,
            JobStatus.RUNNING);
      if (!pathToPart.isPresent())
      {
         Path lock = jobDir.resolve(RUNNING_LOCK);
         try
         {
            Files.delete(lock);
         } catch (IOException e)
         {
            LOGGER.error("Failure removing lock for job {}", importJob.getId(),
                  e);
         }
      }
   }

   @Override
   public Optional<ImportPart> getNextAndSetRunning(ImportJob importJob)
   {
      Validate.isTrue(importJob.getStatus() == JobStatus.RUNNING,
            "The job status must be RUNNING but was {}", importJob.getStatus());
      Context context = getContext(importJob);
      Optional<Path> pathToPart = findPathToPart(importJob, context,
            JobStatus.PREPARED);

      if (!pathToPart.isPresent())
      {
         return Optional.empty();
      } else
      {
         ImportPart importPart = (ImportPart) Serializer
               .deserialize(pathToPart.get());
         importPart.setStatus(JobStatus.RUNNING);
         importPart.setImportJob(importJob); // old state is serialized on disk
         try
         {
            Files.delete(pathToPart.get());
            Files.createDirectories(
                  importPartPath(importPart, context).getParent());
            Files.write(importPartPath(importPart, context),
                  Serializer.serialize(importPart).getBytes());
         } catch (IOException e)
         {
            LOGGER.error("failure", e);
         }

         return Optional.of(importPart);
      }
   }

   private Optional<Path> findPathToPart(ImportJob importJob, Context context,
         JobStatus... jobPartStatus)
   {
      Path jobDir = jobDir(context, importJob);
      return findOneFile(path ->
      {
         boolean statusMatch = false;
         for (JobStatus status : jobPartStatus)
         {
            statusMatch = statusMatch || path.getName(path.getNameCount() - 2)
                  .toString().equals(status.toString());
         }
         return statusMatch && path.getName(path.getNameCount() - 1).toString()
               .endsWith(".xml");
      }, Integer.MAX_VALUE, jobDir);
   }

   @Override
   public synchronized ImportJob setJobStatus(ImportJob importJob,
         JobStatus newStatus, Context context)
   {
      try
      {
         UUID id = importJob.getId();
         JobStatus oldStatus = importJob.getStatus();
         FileUtils.moveDirectory(jobDir(id, oldStatus, context).toFile(),
               jobDir(id, newStatus, context).toFile());
         importJob.setStatus(newStatus);
         if (JobStatus.RUNNING.equals(newStatus))
         {
            FileUtils.touch(
                  jobDir(context, importJob).resolve(RUNNING_LOCK).toFile());
         }
         LOGGER.info("Set status for job {} from {} to {}", id, oldStatus,
               newStatus);
         return importJob;
      } catch (IOException e)
      {
         throw new WerbeoException("Failure changeing import job state");
      }
   }

   @Override
   public Optional<ImportJob> findImportJobById(UUID id, Context context)
   {
      for (JobStatus jobStatus : JobStatus.values())
      {
         if (Files.exists(jobDir(id, jobStatus, context)))
         {
            return Optional.of(new ImportJob(id, jobStatus));
         }
      }
      return Optional.empty();
   }

   @Override
   public void forEachFailure(ImportJob importJob, Context context,
         Consumer<ImportPart> consumer)
   {
      Path jobDir = jobDir(context, importJob)
            .resolve(JobStatus.FAILURE.toString());
      try (Stream<Path> walk = Files.walk(jobDir, 1))
      {
         walk.filter(path -> path.getName(path.getNameCount() - 1).toString()
               .endsWith(".xml"))
               .map(path -> (ImportPart) Serializer.deserialize(path))
               .forEach(consumer);
      } catch (IOException e)
      {
         LOGGER.error("Failure walking throw failures of {}", importJob);
      }
   }

   @Override
   public ImportJob storeCSVImportfile(String importCSV, Context context)
   {
      UUID importJobId = UUID.randomUUID();
      ImportJob importJob = new ImportJob(importJobId, JobStatus.CREATING);
      try
      {
         Files.createDirectories(jobDir(context, importJob));
         writeToFile(importCSV, IMPORT_SOURCE_FILE_NAME_CSV, context, importJob);
         writeToFile(Serializer.serialize(context), CONTEXT_FILE_NAME, context,
               importJob);

         JobStatus newStatus = JobStatus.INITIALIZED_CSV;
         return setJobStatus(importJob, newStatus, context);

      } catch (IOException e)
      {
         throw new RuntimeException("Failure creating dir for import job", e);
      }
   }

   @Override
   public void updateCSV(String csv, Context context, ImportJob importJob)
   {
      try
      {
         writeToFile(csv, IMPORT_SOURCE_FILE_NAME_CSV, context, importJob);
      }
      catch(Exception e)
      {
         LOGGER.error("Error updating CSV-Importfile", e);
      }
   }

   @Override
   public void updateProtocol(String protocol, Context context, ImportJob importJob)
   {
      try
      {
         writeToFile(protocol, IMPORT_PROTOCOL_FILE_NAME, context, importJob);
      }
      catch(Exception e)
      {
         LOGGER.error("Error updating CSV-Protocol-file", e);
      }
   }

   @Override
   public void updateErrorLog(String errors, Context context, ImportJob importJob)
   {
      try
      {
         writeToFile(errors, IMPORT_ERROR_FILE_NAME, context, importJob);
      }
      catch(Exception e)
      {
         LOGGER.error("Error updating CSV-Error-file", e);
      }
   }

}
