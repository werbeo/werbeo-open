package org.infinitenature.werbeo.service.adapters;

import java.util.Set;
import java.util.UUID;

public class FailureEntry implements Comparable<FailureEntry>
{
   private final Integer amount;
   private final String failure;
   private final Set<UUID> sampleIds;

   public FailureEntry(String failure, Set<UUID> sampleIds)
   {
      super();
      this.failure = failure;
      this.sampleIds = sampleIds;
      this.amount = sampleIds.size();
   }

   public Integer getAmount()
   {
      return amount;
   }

   public String getFailure()
   {
      return failure;
   }

   public Set<UUID> getSampleIds()
   {
      return sampleIds;
   }

   @Override
   public int compareTo(FailureEntry o)
   {
      return o.amount.compareTo(amount);
   }

}
