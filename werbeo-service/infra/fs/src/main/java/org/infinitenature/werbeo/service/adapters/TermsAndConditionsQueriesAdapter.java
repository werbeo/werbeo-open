package org.infinitenature.werbeo.service.adapters;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.infinitenature.werbeo.service.core.api.infra.query.TermsAndConditionsQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TermsAndConditionsQueriesAdapter
      implements TermsAndConditionsQueries
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TermsAndConditionsQueriesAdapter.class);
   private final Path tcDir;

   public TermsAndConditionsQueriesAdapter(
         @Autowired InstanceConfig instanceConfig)
   {
      tcDir = Paths.get(instanceConfig.getDir(), "tc");
   }

   @Override
   public String getTC(Portal portal)
   {
      Path tcFile = tcDir.resolve(portal.getId() + ".MD");
      if (!Files.exists(tcFile))
      {
         throw new EntityNotFoundException(
               "TC file " + tcFile.toString() + " not found.");
      }
      try
      {
         return FileUtils.readFileToString(tcFile.toFile(),
               Charset.forName("utf8"));
      } catch (IOException e)
      {
         LOGGER.error("Failure reading file", e);
         throw new RuntimeException("Failure reading terms and conditions file",
               e);
      }
   }

   @Override
   public String getDefaultTC()
   {
      try (InputStream is = getClass().getResourceAsStream("/default-tc.MD");)
      {
         return IOUtils.toString(is, Charset.forName("utf8"));
      } catch (IOException e)
      {
         throw new WerbeoException("Failure loading default tc");
      }

   }

}
