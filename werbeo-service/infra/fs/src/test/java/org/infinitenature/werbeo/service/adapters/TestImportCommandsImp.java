package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.ImportJob;
import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestImportCommandsImp
{
   private InstanceConfig instanceConfig;
   private ImportCommandsAndQueriesImpl implUT;

   @BeforeEach
   void setUp() throws Exception
   {
      instanceConfig = new InstanceConfig();
      instanceConfig.setDir(Files
            .createTempDirectory(TestImportCommandsImp.class.getSimpleName())
            .toString());

      implUT = new ImportCommandsAndQueriesImpl(instanceConfig);
   }

   @Test
   @DisplayName("ensure files are writen to disk")
   void test_001() throws IOException
   {
      Context context = context();
      ImportJob importJob = implUT.storeImportfile("Hello, World!", context);

      assertThat(importJob.getId(), is(notNullValue()));
      assertThat(importJob.getStatus(), is(JobStatus.INITIALIZED));

      assertThat(FileUtils.readFileToString(Paths
            .get(instanceConfig.getDir(), "imports", "2", "test@abc.de",
                  "INITIALIZED", importJob.getId().toString(), "bde.xml")
            .toFile(), Charset.defaultCharset()), is("Hello, World!"));

      Context deserializeContext = (Context) Serializer.deserialize(Paths
            .get(instanceConfig.getDir(), "imports", "2", "test@abc.de",
                  "INITIALIZED", importJob.getId().toString(), "context.xml")
            .toFile());
      assertThat(deserializeContext, equalTo(context));
   }

   @Test
   @DisplayName("find one initalized")
   void test_002() throws IOException
   {
      Context context = context();
      ImportJob importJob = implUT.storeImportfile("Hello, World!", context);

      Optional<ImportJob> loadedJob = implUT
            .findOneImportJobByStatus(JobStatus.INITIALIZED);

      assertThat(loadedJob.isPresent(), is(true));
      assertThat(loadedJob.get().getId(), equalTo(importJob.getId()));
   }
   
   @Test
   @DisplayName("find 3 initalized")
   void test_005() throws IOException
   {
      Context context = context();
      ImportJob importJob1 = implUT.storeImportfile("Hello, World!1", context);
      ImportJob importJob2 = implUT.storeImportfile("Hello, World!2", context);
      ImportJob importJob3 = implUT.storeImportfile("Hello, World!3", context);

      List<ImportJob> jobs = implUT
            .findImportJobsByStatus(JobStatus.INITIALIZED);

      assertTrue(jobs.size() == 3);
      System.out.println();
   }

   private Context context()
   {
      User user = new User();
      user.setLogin("test@abc.de");
      Context context = new Context(new Portal(2), user,
            Collections.emptySet(), "test");
      return context;
   }

   @Test
   @DisplayName("set job running")
   void test_004()
   {
      Context context = context();
      ImportJob importJob = implUT.storeImportfile("Hello, World!", context);

      ImportJob updatedJob = implUT.setJobStatus(importJob, JobStatus.RUNNING,
            context);

      assertThat(updatedJob.getStatus(), is(JobStatus.RUNNING));
   }

   @Test
   @DisplayName("load bde.xml from job")
   void test_003() throws IOException
   {

      Context context = context();
      ImportJob importJob = implUT.storeImportfile("Hello, World!", context);

      String xml = implUT.getBDEXML(importJob);

      assertThat(xml, is("Hello, World!"));
   }
}
