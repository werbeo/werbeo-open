package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.nio.file.Files;
import java.nio.file.Path;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestTermsAndConditionsQueriesAdapter
{
   private TermsAndConditionsQueriesAdapter adapterUT;

   @BeforeEach
   void setUp() throws Exception
   {
      Path tempDir = Files.createTempDirectory(this.getClass().getSimpleName());
      InstanceConfig instanceConfig = new InstanceConfig();
      instanceConfig.setDir(tempDir.toString());
      adapterUT = new TermsAndConditionsQueriesAdapter(instanceConfig);
   }

   @Test
   @DisplayName("Test get default terms and conditions")
   void test_001()
   {
      assertThat(adapterUT.getDefaultTC().startsWith("Nutzungsbedingungen"),
            is(true));
   }

}
