package org.infinitenature.werbeo.service.adapters;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.commands.RolesCommands;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RoleCommandsImpl implements RolesCommands
{
   @Value("${keycloak.realm}")
   private String keyloakRealm;
   @Value("${keycloak.resource}")
   private String keycloakResource;
   @Value("${keycloak.auth-server-url}")
   private String keylcoakURL;
   @Autowired
   private InstanceConfig instanceConfig;
   private RealmResource realm;

   @PostConstruct
   public void inti()
   {
      Keycloak kc = KeycloakBuilder.builder().serverUrl(keylcoakURL)
            .realm(instanceConfig.getKeycloakAdmin().getRealm())
            .username(instanceConfig.getKeycloakAdmin().getUser())
            .password(instanceConfig.getKeycloakAdmin().getPassword())
            .clientId(instanceConfig.getKeycloakAdmin().getClientId())
            .resteasyClient(
                  new ResteasyClientBuilder().connectionPoolSize(10).build())
            .build();

      realm = kc.realm(keyloakRealm);

   }

   @Override
   public void addRole(User user, String roleName)
   {
      List<UserRepresentation> search = realm.users().search(user.getLogin());
      if (search.isEmpty())
      {
         throw new EntityNotFoundException(
               "User with login " + user.getLogin() + " not found in keycloak");
      }
      UserResource userResource = realm.users().get(search.get(0).getId());

      for (RoleRepresentation roleRepresentation : userResource.roles()
            .realmLevel().listAvailable())
      {
         if (roleRepresentation.getName().equals(roleName))
         {
            userResource.roles().realmLevel()
                  .add(Arrays.asList(roleRepresentation));
            break;
         }
      }
   }
   
   @Override
   public void removeRole(User user, String roleName)
   {
      List<UserRepresentation> search = realm.users().search(user.getLogin());
      if (search.isEmpty())
      {
         throw new EntityNotFoundException(
               "User with login " + user.getLogin() + " not found in keycloak");
      }
      UserResource userResource = realm.users().get(search.get(0).getId());

      for (RoleRepresentation roleRepresentation : userResource.roles()
            .realmLevel().listAll())
      {
         if (roleRepresentation.getName().equals(roleName))
         {
            userResource.roles().realmLevel()
                  .remove(Arrays.asList(roleRepresentation));
            break;
         }
      }
   }
}
