package org.infinitenature.werbeo.service.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.infinitenature.werbeo.service.core.api.enums.Role;

public class RoleMapper
{
   private RoleMapper()
   {
      throw new IllegalAccessError("Utitlity class");
   }

   public static Optional<Role> map(String roleName)
   {
      for (Role role : Role.values())
      {
         if (role.getRoleName().equals(roleName))
         {
            return Optional.of(role);
         }
      }
      return Optional.empty();
   }

   public static List<Role> map(List<String> roleNames)
   {
      List<Role> roles = new ArrayList<>();

      for (String roleName : roleNames)
      {
         RoleMapper.map(roleName).ifPresent(role -> roles.add(role));
      }
      return roles;
   }
}
