package org.infinitenature.werbeo.service.adapters;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.UserSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.UserQueries;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserQueriesImpl implements UserQueries
{
   @Value("${keycloak.realm}")
   private String keyloakRealm;
   @Value("${keycloak.resource}")
   private String keycloakResource;
   @Value("${keycloak.auth-server-url}")
   private String keylcoakURL;
   @Autowired
   private InstanceConfig instanceConfig;
   private RealmResource realm;

   @PostConstruct
   public void init()
   {
      Keycloak kc = KeycloakBuilder.builder().serverUrl(keylcoakURL)
            .realm(instanceConfig.getKeycloakAdmin().getRealm())
            .username(instanceConfig.getKeycloakAdmin().getUser())
            .password(instanceConfig.getKeycloakAdmin().getPassword())
            .clientId(instanceConfig.getKeycloakAdmin().getClientId())
            .resteasyClient(
                  new ResteasyClientBuilder().connectionPoolSize(10).build())
            .build();

      realm = kc.realm(keyloakRealm);

   }

   @Override
   public long count(String email)
   {
      return realm.users().search(email).size();
   }

   @Override
   public Slice<User, UserSortField> find(String email, int offset, int limit,
         int portalId)
   {
      return new SliceImpl<>(
            mapUserRepresentations(realm.users().search(email, offset, limit),
                  portalId),
            new OffsetRequestImpl<UserSortField>(offset, limit));
   }

   private List<User> mapUserRepresentations(List<UserRepresentation> search,
         int portalId)
   {
      List<User> users = new ArrayList<>();
      for (UserRepresentation keycloakUser : search)
      {
         String prefix = "WERBEO_" + portalId + "_";
         UserResource userResource = realm.users().get(keycloakUser.getId());

         List<String> roles = userResource.roles().realmLevel().listEffective()
               .stream().map(RoleRepresentation::getName)
               .filter(roleName -> roleName.startsWith(prefix))
               .map(roleName -> roleName.substring(prefix.length()))
               .collect(Collectors.toList());

         users.add(new User(keycloakUser.getEmail(), RoleMapper.map(roles)));
      }
      return users;
   }

}
