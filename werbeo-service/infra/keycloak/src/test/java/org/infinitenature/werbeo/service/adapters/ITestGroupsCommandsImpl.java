package org.infinitenature.werbeo.service.adapters;

import org.infinitenature.werbeo.service.core.api.enity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ITestGroupsCommandsImpl
{
   @Autowired
   private RoleCommandsImpl groupCommands;
   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   void contextLoads()
   {
   }



   @Test
   void test_002()
   {
      User user = new User();
      user.setLogin("ctomalaa@ucoz.com");
      groupCommands.addRole(user, "4275_admin");
   }
}
