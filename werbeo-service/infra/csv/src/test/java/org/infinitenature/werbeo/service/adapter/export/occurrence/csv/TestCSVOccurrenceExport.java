package org.infinitenature.werbeo.service.adapter.export.occurrence.csv;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.common.position.impl.PositionFactoryImpl;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Quantity;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Description;

class TestCSVOccurrenceExport
{
   private CSVOccurrenceExport exportUT;
   private PositionFactory positionFacotry = new PositionFactoryImpl();
   @BeforeEach
   void setUp()
   {
      exportUT = new CSVOccurrenceExport();
   }

   @Test
   @Description("Test empty export")
   void test001() throws IOException
   {
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      OccurrenceConfig occurrenceConfig = occurrenceConfig();
      exportUT.export(new ArrayList<Occurrence>().stream(), bos,
            occurrenceConfig, sampleConfig(), surveyConfg(), 1);
      String result = bos.toString("ISO-8859-1");

      assertAll(() -> assertThat(result.contains("UUID"), is(true)),
            () -> assertThat(result.contains("Taxon"), is(true)),
            () -> assertThat(result.contains("Bestimmer"), is(false)),
            () -> assertThat(result.contains("Beobachter"), is(true)),
            () -> assertThat(result.contains("Mitbeobachter"), is(true)),
            () -> assertThat(result.contains("Vorkommens-Status"), is(true)),
            () -> assertThat(result.contains("Anzahl"), is(true)),
            () -> assertThat(result.contains("Habitat"), is(true)),
            () -> assertThat(result.contains("Fundort"), is(true)),
            () -> assertThat(result.contains("Breite"), is(true)),
            () -> assertThat(result.contains("Koordinatensystem EPSG"),
                  is(true)),
            () -> assertThat(
                  result.contains("Ungenauigkeit der Ortsangabe in m"),
                  is(true)),
            () -> assertThat(result.contains("Herbarbeleg"), is(true)),
            () -> assertThat(result.contains("Datum"), is(true)),
            () -> assertThat(result.contains("Uhrzeit"), is(true)),
            () -> assertThat(result.contains("Projekt"), is(true)),
            () -> assertThat(result.contains("Projekt Beschreibung"), is(true)),
            () -> assertThat(result.contains("Vitalität"), is(true)),
            () -> assertThat(result.contains("Fundortbeschreibung"), is(true)),
            () -> assertThat(result.contains("Validator"), is(true)),
            () -> assertThat(result.contains("Validierungsstatus"), is(true)),
            () -> assertThat("Validierungsdatum is missing",
                  result.contains("Validierungsdatum"), is(true)),
            () -> assertThat("WerbeoOriginal ist missing", result,
                  containsString("Werbeo Datensatz")));
   }

   private SurveyConfig surveyConfg()
   {
      SurveyConfig surveyConfig = new SurveyConfig(new Portal(4),
            new HashSet<>());
      surveyConfig.getConfiguredFields()
            .add(new SurveyFieldConfig(SurveyField.WERBEO_ORIGINAL, true));
      return surveyConfig;
   }

   private OccurrenceConfig occurrenceConfig()
   {
      OccurrenceConfig occurrenceConfig = new OccurrenceConfig();
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.ID, true));
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.TAXON, true));
      occurrenceConfig.getConfiguredFields().add(new OccurrenceFieldConfig(
            OccurrenceField.SETTLEMENT_STATUS_FUKAREK, true));
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.QUANTITY, true));
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.HERBARIUM, false));
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.VITALITY, false));
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.HABITAT, false));
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.OBSERVERS, false));
      occurrenceConfig.getConfiguredFields()
      .add(new OccurrenceFieldConfig(OccurrenceField.TIME_OF_DAY, false));
      occurrenceConfig.getConfiguredFields()
            .add(new OccurrenceFieldConfig(OccurrenceField.VALIDATION, false));
      return occurrenceConfig;
   }

   private SampleConfig sampleConfig()
   {
      SampleConfig sampleConfig = new SampleConfig();
      sampleConfig.getConfiguredFields()
            .add(new SampleFieldConfig(SampleField.LOCALITY, false));
      sampleConfig.getConfiguredFields()
            .add(new SampleFieldConfig(SampleField.LOCATION, true));
      sampleConfig.getConfiguredFields()
            .add(new SampleFieldConfig(SampleField.BLUR, true));
      sampleConfig.getConfiguredFields()
            .add(new SampleFieldConfig(SampleField.DATE, true));
      sampleConfig.getConfiguredFields()
            .add(new SampleFieldConfig(SampleField.SURVEY, true));
      sampleConfig.getConfiguredFields()
            .add(new SampleFieldConfig(SampleField.RECORDER, true));
      sampleConfig.getConfiguredFields()
            .add(new SampleFieldConfig(SampleField.LOCATION_COMMENT, true));
      return sampleConfig;
   }

   @Test
   @Description("Test correct file format")
   void test002()
   {
      assertThat(exportUT.getSupportedFileFormat(), is(FileFormat.CSV));
   }

   @Test
   @Description("Test mock data")
   void test003() throws IOException
   {
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      List<Occurrence> occurrences = new ArrayList<>();
      Occurrence occ = new Occurrence();
      occ.setQuantity(Quantity.ONE_TO_FIVE);
      occ.setId(UUID.randomUUID());
      Taxon taxon = new Taxon();
      taxon.setName("Bla");
      occ.setTaxon(taxon);
      occ.setTimeOfDay("07:35");
      Validation validation = new Validation();
      validation.setStatus(ValidationStatus.VALID);
      validation.setModifiedBy(new User("admin@werbeo.de"));
      validation.setValidator(new Person("xxx_Validator", "xxx"));
      validation.setModificationDate(LocalDateTime.of(2021, 04, 21, 20, 58));
      occ.setValidation(validation);
      occ.setDeterminer(new Person(1, "Hans", "Wurst"));
      Sample sample = new Sample();
      occ.setSample(sample);
      sample.setDate(VagueDateFactory.create(1920));
      sample.setModificationDate(LocalDateTime.of(2019, 12, 24, 14, 10));
      Survey survey = new Survey();
      survey.setName("A test survey");
      survey.setDescription("Ein Beschreibung mit Umlauten äöüß");
      sample.setSurvey(survey);
      Locality locality = new Locality();
      locality.setPosition(positionFacotry.createFromMTB("1943"));
      locality.setPrecision(33);
      sample.setLocality(locality);
      occurrences.add(occ);
      exportUT.export(occurrences.stream(), bos, occurrenceConfig(),
            sampleConfig(), surveyConfg(), 1);
      String result = bos.toString("ISO-8859-1");

      assertAll(() -> assertThat("Change date is missing",
            result.contains("2019-12-24"), is(true)),
            () -> assertThat("Valdiator is missing", result,
                  containsString("xxx_Validator xxx")),
            () -> assertThat("Validation status is missing", result,
                  containsString("Sicher")),
            () -> assertThat("Validation date is missing", result,
                  containsString("2021-04-21")));

      Path path = Files.createTempFile("occ-texport", ".csv");
      Files.write(path, result.getBytes("ISO-8859-1"));
      System.out.println(path);
      System.out.println(result);
   }

}
