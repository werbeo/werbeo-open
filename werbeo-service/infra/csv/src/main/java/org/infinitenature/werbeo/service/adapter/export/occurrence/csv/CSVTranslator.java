package org.infinitenature.werbeo.service.adapter.export.occurrence.csv;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class CSVTranslator
{
   private ResourceBundle messages;

   public CSVTranslator()
   {
      try
      {
         messages = ResourceBundle.getBundle("messages", Locale.GERMANY);
      } catch (Exception e)
      {

      }
   }

   public String getTranslation(Enum<?> someEnum)
   {
      if (someEnum == null)
      {
         return "";
      }
      try
      {
         return "=\""
               + messages.getString(
                     someEnum.getClass().getSimpleName() + "." + someEnum)
               + "\"";
      } catch (MissingResourceException e)
      {
         return someEnum.toString();
      }
   }

   /*
    * add a PLAIN-suffix, for symbol-translations
    */
   public String getTranslationPlain(Enum<?> someEnum)
   {
      if (someEnum == null)
      {
         return "";
      }
      try
      {
         return "=\""
               + messages.getString(
                     someEnum.getClass().getSimpleName() + "." + someEnum + ".PLAIN")
               + "\"";
      } catch (MissingResourceException e)
      {
         // Fallback: no plain-translation found, use default

         try
         {
            return "=\""
                  + messages.getString(
                        someEnum.getClass().getSimpleName() + "." + someEnum)
                  + "\"";
         } catch (MissingResourceException ex)
         {
            return someEnum.toString();
         }
      }
   }
}
