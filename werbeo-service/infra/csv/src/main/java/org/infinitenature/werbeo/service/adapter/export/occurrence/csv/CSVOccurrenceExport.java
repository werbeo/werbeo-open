package org.infinitenature.werbeo.service.adapter.export.occurrence.csv;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonFormatter;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceExportCommands;
import org.infinitenature.werbeo.service.core.api.ports.PersonQueryPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CSVOccurrenceExport implements OccurrenceExportCommands
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CSVOccurrenceExport.class);
   
   private CoordinateTransformerFactory coordinateTransformerFactory = new CoordinateTransformerFactory();

   private final static String COORD_FORMAT = "#.000000";

   private DefaultMTBFieldFormatter defaultMTBFieldFormatter = new DefaultMTBFieldFormatter();

   @Autowired
   private List<MTBFieldFormatter> mtbFieldFormatters = new ArrayList<>();

   @Autowired
   private PersonQueryPort personQueries;

   @Override
   public void export(Stream<Occurrence> occurrences, OutputStream outputStream,
         OccurrenceConfig occurrenceConfig, SampleConfig sampleConfig,
         SurveyConfig surveyConfig, int portalId)
   {
      CSVTranslator translator = new CSVTranslator();

      Writer out = new BufferedWriter(
            new OutputStreamWriter(outputStream, StandardCharsets.ISO_8859_1));

      Occurrence occurrence = null;
      try (CSVPrinter printer = CSVFormat.EXCEL.withDelimiter(';')

            .print(out))
      {

         printer.printRecord(createHeader(occurrenceConfig, sampleConfig,
               surveyConfig, portalId, translator));

         Iterator<Occurrence> it = occurrences.iterator();
         while (it.hasNext())
         {
            occurrence = it.next();
            printer.printRecord(createRecord(translator, occurrence,
                  occurrenceConfig, sampleConfig, surveyConfig, portalId));
         }

      } catch (IOException e)
      {
         LOGGER.error("error exporting csv, occurrence " + occurrence);
      }
   }

   private Object[] createRecord(CSVTranslator translator,
         Occurrence occurrence, OccurrenceConfig occurrenceConfig,
         SampleConfig sampleConfig, SurveyConfig surveyConfig, int portalId)
   {
      NumberFormat nf = new DecimalFormat(COORD_FORMAT,
            DecimalFormatSymbols.getInstance(Locale.GERMANY));
      Locality locality;
      Position position;
      locality = occurrence.getSample().getLocality();
      position = locality.getPosition();
      transformToWGS84(position);
      
      String latitude = position.getPosCenter() == null ?
            "" :
            nf.format(position.getPosCenter()[0]);
      String longitude = position.getPosCenter() == null ?
            "" :
            nf.format(position.getPosCenter()[1]);
      FieldAppender fa = new FieldAppender(occurrenceConfig, sampleConfig,
            surveyConfig);
      fa.append(OccurrenceField.ID, occurrence.getId())
            .append(OccurrenceField.TAXON, occurrence.getTaxon().getName())
            .append(OccurrenceField.DETERMINATION_COMMENT,
                  occurrence.getDeterminationComment())
            .append(OccurrenceField.SETTLEMENT_STATUS_FUKAREK, translator
                  .getTranslation(occurrence.getSettlementStatusFukarek()))
            .append(OccurrenceField.SETTLEMENT_STATUS,
                  translator.getTranslation(occurrence.getSettlementStatus()))
            .append(OccurrenceField.VITALITY,
                  translator.getTranslation(occurrence.getVitality()))
            .append(OccurrenceField.AREA,
                  translator.getTranslation(occurrence.getCoveredArea()))
            .append(OccurrenceField.AMOUNT,
                  translator.getTranslation(occurrence.getAmount()))
            .append(OccurrenceField.QUANTITY,
                  translator.getTranslation(occurrence.getQuantity()))
            .append(OccurrenceField.NUMERIC_AMOUNT,
                  occurrence.getNumericAmount())
            .append(OccurrenceField.BLOOMING_SPROUTS,
                  translator.getTranslation(occurrence.getBloomingSprouts()))
            .append(OccurrenceField.SEX,
                  translator.getTranslationPlain(occurrence.getSex()))
            .append(OccurrenceField.REPRODUCTION,
                  translator.getTranslation(occurrence.getReproduction()))
            .append(SampleField.SAMPLE_METHOD, translator
                  .getTranslation(occurrence.getSample().getSampleMethod()))
            .append(OccurrenceField.HABITAT, occurrence.getHabitat() == null ?
                  "" :
                  occurrence.getHabitat())
            .append(SampleField.LOCATION_COMMENT,
            locality.getLocationComment() == null ?
                  "" :
                  locality.getLocationComment())
            .append(SampleField.LOCALITY,
            locality.getLocality() == null ? "" : locality.getLocality())
            .append(SampleField.LOCATION, latitude)
            .append(SampleField.LOCATION, longitude)
            .append(SampleField.LOCATION, locality.getPosition().getEpsg())
            .append(SampleField.BLUR, locality.getPrecision() == null ?
                  "" :
                  locality.getPrecision());
      getMTBFieldFormatter(portalId).appendContent(fa,
            occurrence.getSample().getLocality().getPosition().getMtb()
                  .getMtb());
      fa.append(OccurrenceField.HERBARIUM, occurrence.getHerbarium() == null ?
            "" :
            occurrence.getHerbarium().getCode())
            .append(OccurrenceField.HERBARIUM,
                  occurrence.getHerbarium() == null ?
                        "" :
                        occurrence.getHerbarium().getHerbary())
            .append(OccurrenceField.EXTERNAL_KEY, occurrence.getExternalKey())
            
            
            .append(SampleField.DATE,
                  occurrence.getSample().getDate().getStartDate() != null
                        ? DateTimeFormatter.ofPattern("yyyy-MM-dd").format(
                              occurrence.getSample().getDate().getStartDate())
                        : "")
            .append(SampleField.DATE,
                  occurrence.getSample().getDate().getEndDate() != null
                        ? DateTimeFormatter.ofPattern("yyyy-MM-dd").format(
                              occurrence.getSample().getDate().getEndDate())
                        : "")
            
            
            
            .append(OccurrenceField.TIME_OF_DAY, occurrence.getTimeOfDay())
            .append(DateTimeFormatter.ofPattern("yyyy-MM-dd")
                  .format(occurrence.getSample().getModificationDate()))
            .append(SampleField.SURVEY,
                  occurrence.getSample().getSurvey().getName())
            .append(SampleField.SURVEY,
                  occurrence.getSample().getSurvey().getDescription() == null ?
                        "" :
                        occurrence.getSample().getSurvey().getDescription())
            .append(SampleField.RECORDER, PersonFormatter.format(occurrence.getSample().getRecorder()))
            .append(OccurrenceField.OBSERVERS, occurrence.getObservers())
            .append(OccurrenceField.DETERMINER,
                  PersonFormatter.format(occurrence.getDeterminer()))
            .append(OccurrenceField.VALIDATION,
                  occurrence.getValidation() == null ? ""
                        : translator.getTranslation(
                              occurrence.getValidation().getStatus()))
            .append(OccurrenceField.VALIDATION,
                  occurrence.getValidation() == null ? ""
                        : getValidator(occurrence.getValidation()))
            .append(OccurrenceField.VALIDATION,
                  occurrence.getValidation() == null ? ""
                        : formatDate(
                              occurrence.getValidation().getModificationDate()))
            .append(SurveyField.WERBEO_ORIGINAL,
                  occurrence.getSample().getSurvey().isWerbeoOriginal());
      return fa.getRecord();
   }

   private void transformToWGS84(Position position)
   {
      if(position.getPosCenter() != null)
      {
         position.setPosCenter(coordinateTransformerFactory
               .getCoordinateTransformer(position.getEpsg(), 4326)
               .convert(position.getPosCenter()));
         position.setEpsg(4326);
      }
   }

   private String getValidator(Validation validation)
   {
      return PersonFormatter.format(validation.getValidator());
   }

   private String formatDate(LocalDateTime date)
   {
      if (date == null)
      {
         return "";
      } else
      {
         return DateTimeFormatter.ofPattern("yyyy-MM-dd").format(date);
      }
   }
   private MTBFieldFormatter getMTBFieldFormatter(int portalId)
   {
      for(MTBFieldFormatter formatter : mtbFieldFormatters)
      {
         if(formatter.getPortalId() != null && portalId == formatter.getPortalId())
         {
            return formatter;
         }
      }
      return defaultMTBFieldFormatter;
   }

   private Object[] createHeader(OccurrenceConfig occurrenceConfig,
         SampleConfig sampleConfig, SurveyConfig surveyConfig, int portalId,
         CSVTranslator translator)
   {
      // TODO: i18n
      FieldAppender fa = new FieldAppender(occurrenceConfig, sampleConfig,
            surveyConfig);
      fa.append(OccurrenceField.ID, "UUID")
            .append(OccurrenceField.TAXON, "Taxon")
            .append(OccurrenceField.DETERMINATION_COMMENT,
                  "Bemerkung zur Bestimmung")
            .append(OccurrenceField.SETTLEMENT_STATUS_FUKAREK,
                  "Vorkommens-Status Fukarek")
            .append(OccurrenceField.SETTLEMENT_STATUS, "Vorkommens-Status")
            .append(OccurrenceField.VITALITY, "Vitalität")
            .append(OccurrenceField.AREA, "Bedeckte Fläche (qm)")
            .append(OccurrenceField.AMOUNT, "Häufigkeit")
            .append(OccurrenceField.QUANTITY, "Anzahl")
            .append(OccurrenceField.NUMERIC_AMOUNT, "Anzahl")
            .append(OccurrenceField.BLOOMING_SPROUTS, "Fertile Sprosse")
            .append(OccurrenceField.SEX, "Geschlecht")
            .append(OccurrenceField.REPRODUCTION, "Reproduktion")
            .append(SampleField.SAMPLE_METHOD, "Methode")
            .append(OccurrenceField.HABITAT, "Habitat")
            .append(SampleField.LOCATION_COMMENT, "Fundortbeschreibung")
            .append(SampleField.LOCALITY, "Fundort")
            .append(SampleField.LOCATION, "Länge")
            .append(SampleField.LOCATION, "Breite")
            .append(SampleField.LOCATION, "Koordinatensystem EPSG")
            .append(SampleField.BLUR, "Ungenauigkeit der Ortsangabe in m");
      getMTBFieldFormatter(portalId).appendHeader(fa);
      fa.append(OccurrenceField.HERBARIUM, "Code Index Herbariorum")
            .append(OccurrenceField.HERBARIUM, "Herbarbeleg")
            .append(OccurrenceField.EXTERNAL_KEY, "externer Schlüssel")
            .append(SampleField.DATE, "Datum - Start")
            .append(SampleField.DATE, "Datum - Ende")//
            .append(OccurrenceField.TIME_OF_DAY, "Uhrzeit")
            .append("Änderungsdatum")
            .append(SampleField.SURVEY, "Projekt")
            .append(SampleField.SURVEY, "Projekt Beschreibung")
            .append(SampleField.RECORDER, "Finder/Beobachter")
            .append(OccurrenceField.OBSERVERS, "Mitbeobachter")
            .append(OccurrenceField.DETERMINER, "Bestimmer")
            .append(OccurrenceField.VALIDATION, "Validierungsstatus")
            .append(OccurrenceField.VALIDATION, "Validator")
            .append(OccurrenceField.VALIDATION, "Validierungsdatum")
            .append(SurveyField.WERBEO_ORIGINAL, "Werbeo Datensatz");
      return fa.getRecord();
   }

   @Override
   public FileFormat getSupportedFileFormat()
   {
      return FileFormat.CSV;
   }

}
