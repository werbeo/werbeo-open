package org.infinitenature.werbeo.service.adapter.export.occurrence.csv;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField;

public class FieldAppender
{
   private final Set<OccurrenceField> occurrenceFields;
   private final Set<SampleField> sampleFields;
   private final Set<SurveyField> surveyFields;
   private final List<Object> record = new ArrayList<>();

   public FieldAppender(OccurrenceConfig occurrenceConfig,
         SampleConfig sampleConfig, SurveyConfig surveyConfig)
   {
      occurrenceFields = Collections
            .unmodifiableSet(occurrenceConfig.getConfiguredFields().stream()
                  .map(ocf -> ocf.getField()).collect(Collectors.toSet()));
      sampleFields = Collections
            .unmodifiableSet(sampleConfig.getConfiguredFields().stream()
                  .map(scf -> scf.getField()).collect(Collectors.toSet()));
      surveyFields = Collections
            .unmodifiableSet(surveyConfig.getConfiguredFields().stream()
                  .map(scf -> scf.getField()).collect(Collectors.toSet()));
   }

   public FieldAppender append(Object value)
   {
      record.add(value);
      return this;
   }

   public FieldAppender append(SurveyField field, Object value) {
      if (surveyFields.contains(field))
      {
         record.add(value);
      }
      return this;
   }
   public FieldAppender append(OccurrenceField field, Object value)
   {
      if (occurrenceFields.contains(field))
      {
         record.add(value);
      }
      return this;
   }

   public FieldAppender append(SampleField field, Object value)
   {
      if (sampleFields.contains(field))
      {
         record.add(value);
      }
      return this;
   }

   public Object[] getRecord()
   {
      return record.toArray();
   }
}
