package org.infinitenature.werbeo.service.adapter.export.occurrence.csv;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.springframework.stereotype.*;

@Service
public class SeparateMTBFieldFormatter implements MTBFieldFormatter
{

   @Override
   public void appendHeader(FieldAppender appender)
   {
      appender.append(SampleField.LOCATION_MTB, "MTB");
      appender.append(SampleField.LOCATION_MTB, "Quadranten");
   }

   @Override
   public void appendContent(FieldAppender appender, String mtb)
   {
      appender.append(SampleField.LOCATION_MTB,
            mtb.substring(0,4));
      appender.append(SampleField.LOCATION_MTB,
            extractMtbQuadrants(mtb)); 
   }
   
   private String extractMtbQuadrants(String mtb)
   {
      if(mtb.length() == 4)
      {
         return "";
      }
      return mtb.substring(4).replace("/","");
   }

   @Override
   public Integer getPortalId()
   {
      return 6;
   }

}
