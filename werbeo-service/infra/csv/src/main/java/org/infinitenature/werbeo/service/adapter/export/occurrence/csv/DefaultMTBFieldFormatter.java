package org.infinitenature.werbeo.service.adapter.export.occurrence.csv;

import org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField;
import org.springframework.stereotype.*;


public class DefaultMTBFieldFormatter implements MTBFieldFormatter
{

   @Override
   public void appendHeader(FieldAppender appender)
   {
      appender.append(SampleField.LOCATION_MTB, "MTB");
   }

   @Override
   public Integer getPortalId()
   {
      return null;
   }

   @Override
   public void appendContent(FieldAppender appender, String mtb)
   {
      appender.append(SampleField.LOCATION_MTB,
            mtb);
   }

}
