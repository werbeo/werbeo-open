package org.infinitenature.werbeo.service.adapter.export.occurrence.csv;

public interface MTBFieldFormatter
{
   public void appendHeader(FieldAppender appender);
   public void appendContent(FieldAppender appender, String mtb);
   public Integer getPortalId();
}
