Erläuterungen zum Ausfüllen der CSV-Import-Datei

UUID --> Feld bitte freilassen: wird automatisch ausgefüllt

Taxon --> hier bitte korrekten Artnamen eintragen

Finder --> Name, bei einer Literaturauswertung: Literatur

Bestimmer --> Name, s.o.

Datum --> genaues Datum 01.02.2014 oder 2014

MTB --> z.B. 6606

Laenge --> z.B. 8,27488

Breite --> z.B. 50,908318

Referenzsystem --> 4326 (= WGS84)

Unschaerfe --> z.B. 25 (m)

Habitat/Wuchsort --> z.B. Flussufer (Text)

Fundort --> z.B. Seltrupper Heide (Text)

Anzahl --> hier eine Zahl eintragen oder Feld nicht ausfüllen (keine Null!)

Geschlecht (Codierung):

leere Zelle = keine Angabe
M = männlich
W = weiblich 
MW = männlich und weiblich
u = unbekannt

Reproduktion (Codierung):

leere Zelle = keine Angabe
sicher = sicher (Larvenfund)
plausibel = plausibel (Paarung, Eiablage, mehrere Tiere im typischen Lebensraum)
unwahrscheinlich = unwahrscheinlich (eingeflogenes Einzeltier)
unbekannt = unbekannt

Methode (Codierung):

leere Zelle = keine Angabe
T = Transektbegehung
K = Kescherfänge
I = Isolationsquadrate
M = Motorsauger
A = Andere Methode
B = Beobachtung

Entwicklungsstadium (Einträge):

leere Zelle = keine Angabe
unbekannt
Larve
Imago
Larve und Imago

Makroptere Exemplare

leere Zelle = keine Angabe
unbekannt
ja
nein

Anmerkungen zur Bestimmung (Textfeld)

z.B. Quelle der Literaturangabe oder Besonderheiten/Schwierigkeiten bei der Bestimmung

Projekt --> Hier bitte einen einheitlichen Projektnamen vergeben: z.B. Literaturauswertung1




