package org.infinitenature.werbeo.service.adapters.index;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.core.api.enity.FrequencyDistribution;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceIndexQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.FacetQuery;
import org.springframework.data.solr.core.query.GroupOptions;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleField;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.core.query.result.FieldStatsResult;
import org.springframework.data.solr.core.query.result.GroupPage;
import org.springframework.data.solr.core.query.result.ScoredPage;
import org.springframework.data.solr.core.query.result.StatsPage;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceIndexQueryAdapter implements OccurrenceIndexQueries
{
   private static final String COLLECTION = "occurrences";

   @Autowired
   private SolrTemplate solrTemplate;
   @Autowired
   private RequestMapper requestMapper;
   @Autowired
   private CacheOccurrenceMapper cacheOccurrenceMapper;
   @Autowired
   private SolrOccurrenceQueryBuilder queryBuilder;
   @Autowired
   private OccurrenceCacheRepository repo;

   @Override
   public Slice<Occurrence, OccurrenceSortField> findOccurrences(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceSortField> offsetRequest)
   {
      Query query = perpareQuery(filter, context);
      query.setPageRequest(requestMapper.map(offsetRequest));
      if(filter.isGroupBySample())
      {
         query.addGroupByField(new SimpleField("sampleId"));
      }

      ScoredPage<CacheOccurrence> resultPage = solrTemplate
            .queryForPage(COLLECTION, query, CacheOccurrence.class);
      return new SliceImpl<>(
            cacheOccurrenceMapper.mapToCore(resultPage.getContent(),
                  context),
            offsetRequest);

   }


   private Query perpareQuery(OccurrenceFilter filter, Context context)
   {
      return queryBuilder.buildQuery(filter, context);
   }

   @Override
   public long countOccurrences(OccurrenceFilter filter, Context context)
   {
      Query query = perpareQuery(filter, context);
      query.setPageRequest(requestMapper.map(new OffsetRequestImpl<>(0, 0,
            SortOrder.ASC, OccurrenceSortField.MOD_DATE)));
      if(filter.isGroupBySample())
      {
         SimpleField sampleIdField = new SimpleField("sampleId");
         query.setGroupOptions(
               new GroupOptions().setTotalCount(true).setLimit(0).setOffset(0)
                     .addGroupByField(sampleIdField));

         GroupPage<CacheOccurrence> resultPage = solrTemplate
               .queryForGroupPage(COLLECTION, query, CacheOccurrence.class);
         return resultPage.getGroupResult("sample_id_s").getGroupsCount();
      }
      else
      {
      ScoredPage<CacheOccurrence> resultPage = solrTemplate
            .queryForPage(COLLECTION, query, CacheOccurrence.class);
         return resultPage.getTotalElements();
      }
   }

   @Override
   public Stream<UUID> findAllIds()
   {
      return repo.findAllIds().stream().map(CacheOccurrence::getId)
            .map(s -> UUID.fromString(s));
   }

   @Override
   public Occurrence findById(UUID id, Context context)
   {
      return cacheOccurrenceMapper.map(repo.findById(id.toString()), context);
   }

   @Override
   public List<Occurrence> findBySampleId(UUID id, Context context)
   {
      return new ArrayList<>(cacheOccurrenceMapper
            .mapToCore(repo.findBySampleId(id.toString()), context));
   }

   @Override
   public Set<MTB> getCoveredMTB(int taxonId, boolean includeChildTaxa,
         Context context)
   {
      Query query = queryBuilder.createCoveredMTBsQuery(taxonId,
            includeChildTaxa, context);

      StatsPage<CacheOccurrence> result = solrTemplate
            .queryForStatsPage(COLLECTION, query, CacheOccurrence.class);
      FieldStatsResult fieldStatsResult = result.getFieldStatsResult("mtb_s");
      Collection<Object> distinctValues = fieldStatsResult.getDistinctValues();
      return reduceToUniqueMTBQs(distinctValues);
   }

   @Override
   public FrequencyDistribution getOccurrencesPerDay(Context context, int days, ZoneId timeZone)
   {
      FacetQuery query = queryBuilder.createQueryOccurrencesPerDay(context,
            days, timeZone);
      FacetPage<CacheOccurrence> page = solrTemplate.queryForFacetPage(
            CacheOccurrence.COLLECTION, query, CacheOccurrence.class);
      return createStats(page, timeZone, ChronoUnit.DAYS);
   }

   @Override
   public FrequencyDistribution getOccurrencesPerWeek(Context context,
         int weeks, ZoneId timeZone)
   {
      FacetQuery query = queryBuilder.createQueryOccurrencesPerWeek(context,
            weeks, timeZone);
      FacetPage<CacheOccurrence> page = solrTemplate.queryForFacetPage(
            CacheOccurrence.COLLECTION, query, CacheOccurrence.class);
      return createStats(page, timeZone, ChronoUnit.WEEKS);
   }

   @Override
   public FrequencyDistribution getOccurrencesPerMonth(Context context,
         int months, ZoneId timeZone)
   {
      FacetQuery query = queryBuilder
            .createQueryOccurrencesCreatedPerMonth(context,
            months, timeZone);
      FacetPage<CacheOccurrence> page = solrTemplate.queryForFacetPage(
            CacheOccurrence.COLLECTION, query, CacheOccurrence.class);
      return createStats(page, timeZone, ChronoUnit.MONTHS);
   }

   private FrequencyDistribution createStats(FacetPage<CacheOccurrence> page,
         ZoneId timeZone, ChronoUnit chronoUnit)
   {
      final Map<ZonedDateTime, Integer> collect = page
            .getRangeFacetResultPage("creation_date_dt")
            .get()
            .collect(Collectors.toMap(
                  facetFieldEntry -> ZonedDateTime
                        .parse(facetFieldEntry.getValue()).toInstant()
                        .atZone(timeZone),
                  facetFieldEntry -> ((int) facetFieldEntry.getValueCount())));

      return new FrequencyDistribution(chronoUnit, collect);
   }

   protected Set<MTB> reduceToUniqueMTBQs(Collection<Object> distinctValues)
   {
      return removeNulls(distinctValues.stream()
            .map(o -> o.toString())
            .map(mtb -> mtb.length() == 4 ? mtb : mtb.substring(0, 6))
            .distinct()
            .map(mtb -> {
               try
               {
                  return MTBHelper.toMTB(mtb);
               }
               catch(Exception e)
               {
                  return null;
               }
               }).collect(Collectors.toSet()));
   }

   private Set<MTB> removeNulls(Set<MTB> collect)
   {
      Iterator<MTB> it = collect.iterator();
      while(it.hasNext())
      {
         MTB next = it.next();
            if(next == null)
            {
               it.remove();
            }
      }
      return collect;
   }

}
