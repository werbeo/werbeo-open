package org.infinitenature.werbeo.service.adapters.index;

import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.OCCURRENCE_ABSENCE;
import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.OCCURRENCE_EXTERNAL_KEY;
import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.OCCURRENCE_MODIFICATION_DATE;
import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.PROJECT_ID;
import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.SAMPLE_BLUR;
import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.SAMPLE_FROM;
import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.SAMPLE_MTB;
import static org.infinitenature.werbeo.service.adapters.index.CacheOccurrence.SAMPLE_TO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.common.commons.DateTimeUtils;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.infra.query.AbstractOccurrenceQueryBuilder;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter.WKTMode;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTB;
import org.opengis.feature.IllegalAttributeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.FacetOptions;
import org.springframework.data.solr.core.query.FacetQuery;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleFacetQuery;
import org.springframework.data.solr.core.query.SimpleFilterQuery;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.SimpleStringCriteria;
import org.springframework.data.solr.core.query.StatsOptions;
import org.springframework.stereotype.Component;
@Component
public class SolrOccurrenceQueryBuilder
      extends AbstractOccurrenceQueryBuilder<Query>
{
   @Autowired
   private PortalQueries portalQueries;

   @Override
   protected Query initQuery(Context context)
   {
      Query query = new SimpleQuery();
      return query.addCriteria(new Criteria("id").isNotNull());
   }

   @Override
   protected Query appendWebsiteAgreements(Context context, Query query)
   {
      return query.addFilterQuery(
            new SimpleFilterQuery(new Criteria("portal_i").in(map(context))));
   }

   private Set<Integer> map(Context context)
   {
      List<Portal> loadAssociated = portalQueries
            .findAssociated(context.getPortal().getId()).getContent();
      return loadAssociated.stream().map(Portal::getId)
            .collect(Collectors.toSet());
   }

   @Override
   protected Query appendOwner(String owner, Query query)
   {
      return query.addFilterQuery(
            new SimpleFilterQuery(new Criteria("owner_s").is(owner)));
   }

   @Override
   protected Query appendOccurrenceExternalKey(String occExternalKey,
         Query query)
   {
      occExternalKey = StringUtils.replace(occExternalKey, ":", "\\:");
      occExternalKey = StringUtils.replace(occExternalKey, " ", "\\ ");
      return query.addFilterQuery(new SimpleFilterQuery(
            new Criteria(OCCURRENCE_EXTERNAL_KEY)
                  .expression("*" + occExternalKey + "*")));
   }

   @Override
   protected Query appendWkt(String wkt, WKTMode wktMode, Query query)
   {// "Intersects(POLYGON((11.666666666666666 53.85,11.666666666666666
    // 53.9,11.75 53.9,11.75 53.85,11.666666666666666 53.85))) distErrPct=0.00"

      if (wktMode == WKTMode.WITHIN)
      {
         return query
               .addFilterQuery(new SimpleFilterQuery(new Criteria("geom_geom")
                     .expression("\"Within(" + wkt + ") distErrPct=0.01\"")));
      } else if (wktMode == WKTMode.INTERSECTS)
      {
         return query.addFilterQuery(
               new SimpleFacetQuery(new Criteria("geom_geom").expression(
                     "\"Intersects(" + wkt + ") distErrPct=0.00\"")));
      } else
      {
         throw new IllegalAttributeException(
               "Illegal value for WKTMode. Must be INTERSETCS or WITHIN. Value was: "
                     + wktMode);
      }
   }

   @Override
   protected Query appendMaxBlur(int maxBlur, Query query)
   {
      return query.addFilterQuery(new SimpleFilterQuery(
            new Criteria(SAMPLE_BLUR).lessThanEqual(maxBlur)));
   }

   @Override
   protected Query appendMTB(MTB mtb, Query query)
   {
      return query.addFilterQuery(new SimpleFilterQuery(
            new Criteria(SAMPLE_MTB).startsWith(mtb.getMtb())));
   }

   @Override
   protected Query appendSurveyId(int surveyId, Query query)
   {
      return query.addFilterQuery(
            new SimpleFilterQuery(new Criteria(PROJECT_ID).is(surveyId)));
   }

   @Override
   protected Query appendValidator(String validator, Query query)
   {
      return query.addFilterQuery(
            new SimpleFilterQuery(
                  new Criteria(CacheOccurrence.VALIDATED_BY).is(validator)));
   }

   @Override
   protected Query appendValidationStatus(ValidationStatus validationStatus,
         Query query)
   {
      return query.addFilterQuery(new SimpleFilterQuery(
            new Criteria(CacheOccurrence.VALIDATION_STATUS)
                  .is(validationStatus)));
   }

   @Override
   protected Query appendTaxon(Integer taxonId, boolean includeChildTaxa,
         Query query)
   {

      if (includeChildTaxa)
      {
         return query.addFilterQuery(new SimpleFilterQuery(
               new Criteria("taxon_swarm_is").in(taxonId)));
      } else
      {
         return query.addFilterQuery(new SimpleFilterQuery(
               new Criteria("taxon_synonym_is").in(taxonId)));
      }
   }

   @Override
   protected Query appendTo(LocalDate to, Query query)
   {
      return query.addFilterQuery(
            new SimpleFilterQuery(new Criteria(SAMPLE_TO).lessThanEqual(
                  DateTimeFormatter.ISO_LOCAL_DATE.format(to) + "T00:00:00Z")));
   }

   @Override
   protected Query appendFrom(LocalDate from, Query query)
   {
      return query
            .addFilterQuery(new SimpleFilterQuery(new Criteria(SAMPLE_FROM)
            .greaterThanEqual(DateTimeFormatter.ISO_LOCAL_DATE.format(from)
                  + "T00:00:00Z")));
   }

   @Override
   protected Query appendPortalBasedFilters(Context context,
         Map<Filter, Collection<Portal>> portalBasedFilters, Query query)
   {
      Collection<Portal> onlyOwnDataPortals = portalBasedFilters
            .getOrDefault(Filter.ONLY_OWN_DATA, Collections.emptySet());

      for (Portal portal : onlyOwnDataPortals)
      {
         String criteria = "(NOT portal_i:" + portal.getId() + ")"
               + " OR owner_s:" + context.getUser().getLogin();
         criteria += " OR project_availability_s:FREE";
         query.addFilterQuery(
               new SimpleFilterQuery(new SimpleStringCriteria(criteria)));
      }
      return query;
   }

   @Override
   protected Query appendAbsence(boolean absence, Query query)
   {
      return query.addFilterQuery(
            new SimpleFilterQuery(
                  new Criteria(OCCURRENCE_ABSENCE).is(absence)));
   }

   @Override
   protected Query appendHerbaryCode(String herbaryCode, Query query)
   {
      return query.addFilterQuery(new SimpleFilterQuery(
            new Criteria("herbary_code_s").is(herbaryCode)));
   }

   @Override
   protected Query appendModifiedSince(LocalDateTime modifiedSince, Query query)
   {
      return query.addFilterQuery(
            new SimpleFilterQuery(new Criteria(OCCURRENCE_MODIFICATION_DATE)
                  .greaterThanEqual(DateTimeFormatter.ISO_LOCAL_DATE_TIME
                        .format(modifiedSince) + "Z")));
   }
   public Query createCoveredMTBsQuery(int taxonId, boolean includeChildTaxa,
         Context context)
   {
      Query query = initQuery(context);
      appendWebsiteAgreements(context, query);
      appendTaxon(taxonId, includeChildTaxa, query);
      query.setRows(0);
      StatsOptions statsOptions = new StatsOptions().addField("mtb_s")
            .setSelectiveCalcDistinct(true);
      query.setStatsOptions(statsOptions);
      return query;
   }

   public FacetQuery createQueryOccurrencesPerDay(Context context, int days,
         ZoneId timeZone)
   {
      LocalDateTime now = LocalDateTime.now();

      return createQueryOccurrencesPerDay(context, days, timeZone, now);
   }

   FacetQuery createQueryOccurrencesPerDay(Context context,
         int days,
         ZoneId timeZone, LocalDateTime now)
   {
      ZonedDateTime startDate = createDayStartDate(days, timeZone, now);
      ZonedDateTime endDate = createDayResEndDate(timeZone, now);

      return createOccurrencesCreatedPerTimeFacetQuery(context, startDate,
            endDate, "+1DAY");
   }

   ZonedDateTime createDayStartDate(int days, ZoneId timeZone,
         LocalDateTime now)
   {
      return now.minusDays(days - 1l).atZone(timeZone).with(LocalTime.MIN);
   }

   ZonedDateTime createDayResEndDate(ZoneId timeZone, LocalDateTime now)
   {
      return now.plusDays(1).atZone(timeZone).with(LocalTime.MIN);
   }

   public FacetQuery createQueryOccurrencesPerWeek(Context context, int weeks,
         ZoneId timeZone)
   {
      LocalDateTime now = LocalDateTime.now();

      return createQueryOccurrencesPerWeek(context, weeks, timeZone, now);
   }

   FacetQuery createQueryOccurrencesPerWeek(Context context, int weeks,
         ZoneId timeZone, LocalDateTime now)
   {
      ZonedDateTime endDate = createWeekEndDate(timeZone, now);

      ZonedDateTime startDate = createWeekStartDate(weeks, timeZone, now);

      return createOccurrencesCreatedPerTimeFacetQuery(context, startDate,
            endDate, "+7DAY");
   }

   ZonedDateTime createWeekStartDate(int weeks, ZoneId timeZone,
         LocalDateTime now)
   {
      LocalDate startOfCurrentWeek = DateTimeUtils.getStartOfWeek(now);
      return startOfCurrentWeek.minusWeeks(weeks - 1l).atStartOfDay(timeZone);
   }

   ZonedDateTime createWeekEndDate(ZoneId timeZone, LocalDateTime now)
   {
      LocalDate endOfWeek = DateTimeUtils.getEndOfWeek(now);
      return endOfWeek.plusDays(1).atStartOfDay(timeZone);
   }

   public FacetQuery createQueryOccurrencesCreatedPerMonth(
         Context context,
         int months, ZoneId timeZone)
   {
      return createQueryOccurrencesCreatedPerMonth(context, months, timeZone,
            LocalDateTime.now());
   }

   FacetQuery createQueryOccurrencesCreatedPerMonth(Context context,
         int months,
         ZoneId timeZone, LocalDateTime now)
   {
      ZonedDateTime endOfThisMonth = now.atZone(timeZone).plusMonths(1)
            .withDayOfMonth(1).with(LocalTime.MIN);
      ZonedDateTime startDate = now.atZone(timeZone)
            .minusMonths(months - 1).withDayOfMonth(1).with(LocalTime.MIN);
      return createOccurrencesCreatedPerTimeFacetQuery(context,
            startDate, endOfThisMonth, "+1MONTH");
   }

   public FacetQuery createOccurrencesCreatedPerTimeFacetQuery(
         Context context,
         ZonedDateTime startDate, ZonedDateTime endDate, String gap)
   {
      Date start = Date.from(startDate.toInstant());
      Date end = Date.from(endDate.toInstant());
      FacetQuery query = new SimpleFacetQuery();

      query.addCriteria(new Criteria("id").isNotNull());
      appendWebsiteAgreements(context, query);
      query.setRows(0);

      query.setFacetOptions(new FacetOptions("creation_date_dt")
            .setFacetLimit(-1).setFacetMinCount(0).addFacetByRange(
                  new FacetOptions.FieldWithDateRangeParameters(
                  "creation_date_dt", start, end, gap)));
      return query;
   }
}
