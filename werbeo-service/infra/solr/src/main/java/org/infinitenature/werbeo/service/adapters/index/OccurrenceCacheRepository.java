package org.infinitenature.werbeo.service.adapters.index;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;

public interface OccurrenceCacheRepository
      extends SolrCrudRepository<CacheOccurrence, UUID>
{
   @Query(value = "id:*", fields = { "id" })
   List<CacheOccurrence> findAllIds();

   @Query(value = "project_i:?0", fields = { "id" })
   List<CacheOccurrence> findIdsByProjectId(int projcetId);

   CacheOccurrence findById(String id);

   Collection<CacheOccurrence> findBySampleId(String id);

   void deleteBySampleId(String sampleId);
}
