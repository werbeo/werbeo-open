package org.infinitenature.werbeo.service.adapters.index;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.solr.client.solrj.beans.Field;
import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Position.PositionType;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Dynamic;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import net.vergien.beanautoutils.annotation.Bean;

@SolrDocument(collection = CacheOccurrence.COLLECTION)
@Bean
public class CacheOccurrence
{
   public static final String PORTAL = "portal_i";
   public static final String OCCURRENCE_EXTERNAL_KEY = "external_key_s";
   public static final String SAMPLE_MTB = "mtb_s";
   public static final String SAMPLE_BLUR = "blur_i";
   public static final String SAMPLE_TO = "to_dt";
   public static final String SAMPLE_FROM = "from_dt";
   public static final String OCCURRENCE_ABSENCE = "absence_b";
   public static final String OCCURRENCE_MODIFICATION_DATE = "modification_date_dt";
   public static final String PROJECT_TAG = "project_tag_ss";
   public static final String VALIDATION_STATUS = "validation_status_s";
   public static final String VALIDATION_STATUS_SORT_ORDER = "validation_status_sort_order_i";
   public static final String VALIDATED_BY = "validated_by_s";
   public static final String COLLECTION = "occurrences";
   public static final String PROJECT_ID = "project_i";
   public static final String PROJECT_NAME = "project_s";
   public static final String PROJECT_DESCRIPTION = "project_description_s";
   public static final String PROJECT_OBFUSCATION_POLICIES = "project_obfuscation_policies_s";
   public static final String PROJECT_AVAILABILITY = "project_availability_s";
   public static final String PROJECT_WERBEO_ORIGINAL = "project_werboe_original_b";
   public static final String PROJECT_ALLOW_DATA_ENTRY = "project_allow_data_entry_b";

   @Id
   private String id;
   @Indexed
   @Field("creation_date_dt")
   private LocalDateTime occurrenceCreationDate;
   @Indexed
   @Field("created_by_s")
   private String occurrenceCreatedBy;
   @Indexed
   @Field(OCCURRENCE_MODIFICATION_DATE)
   private LocalDateTime occurrenceModificationDate;
   @Indexed
   @Field("modified_by_s")
   private String occurrenceModifiedBy;

   @Indexed
   @Field("geom_geom")
   private String wkt;
   @Indexed
   @Field("wkt_orig_text")
   private String wktOrig;
   @Indexed
   @Field("wkt_orig_s")
   private String wktOrigOld;
   @Indexed
   @Field("sref_orig_i")
   private int srefOrig;
   @Indexed
   @Field(SAMPLE_BLUR)
   private int blur;
   @Indexed
   @Field(SAMPLE_MTB)
   private String mtb;
   @Indexed
   @Field("postion_type_s")
   private PositionType positionType;
   @Indexed
   @Field(OCCURRENCE_EXTERNAL_KEY)
   private String externalKey;
   @Indexed
   @Field(PORTAL)
   private int portalId;
   @Indexed
   @Field("portal_creation_date_dt")
   private LocalDateTime portalCreationDate;
   @Indexed
   @Field("portal_created_by_s")
   private String portalCreatedBy;
   @Indexed
   @Field("portal_modification_date_dt")
   private LocalDateTime portalModificationDate;
   @Indexed
   @Field("portal_modified_by_s")
   private String portalModifiedBy;
   @Indexed
   @Field("portal_s")
   private String portalName;
   @Indexed
   @Field("portal_url_s")
   private String portalUrl;

   @Field(PROJECT_ID)
   @Indexed
   private int projectId;
   @Indexed
   @Field("project_creation_date_dt")
   private LocalDateTime projectCreationDate;
   @Indexed
   @Field("project_created_by_s")
   private String projectCreatedBy;
   @Indexed
   @Field("project_modification_date_dt")
   private LocalDateTime projectModificationDate;
   @Indexed
   @Field("project_modified_by_s")
   private String projectModifiedBy;
   @Field(PROJECT_NAME)
   @Indexed
   private String projectName;
   @Field(PROJECT_AVAILABILITY)
   @Indexed
   private Availability projectAvailability;
   @Field(PROJECT_DESCRIPTION)
   @Indexed
   private String projectDescription;
   @Field(PROJECT_OBFUSCATION_POLICIES)
   @Indexed
   private String projectObfuscationPolicies;
   @Field(PROJECT_WERBEO_ORIGINAL)
   @Indexed
   private Boolean projectWerbeoOriginal;
   @Field(PROJECT_ALLOW_DATA_ENTRY)
   @Indexed
   private Boolean projectAllowDataEntry;
   @Indexed
   @Field("determiner_i")
   private String determinerId;
   @Indexed
   @Field("determiner_first_name_s")
   private String determinerFirstName;
   @Indexed
   @Field("determiner_email_s")
   private String determinerEmail;
   @Indexed
   @Field("determiner_last_name_s")
   private String determinerLastName;
   @Indexed
   @Field("determiner_external_key_s")
   private String determinerExternalKey;

   @Indexed
   @Field("recorder_i")
   private String recorderId;
   @Indexed
   @Field("recorder_first_name_s")
   private String recorderFirstName;
   @Indexed
   @Field("recorder_last_name_s")
   private String recorderLastName;
   @Indexed
   @Field("recorder_email_s")
   private String recorderEmail;
   @Indexed
   @Field("recorder_external_key_s")
   private String recorderExternalKey;

   @Indexed
   @Field("observers_s")
   private String observers;

   @Indexed
   @Field("taxon_i")
   private int taxonId;
   @Indexed
   @Field("taxon_external_key_s")
   private String taxonExternalKey;
   @Indexed
   @Field("taxon_group_s")
   private String taxonGroup;
   @Indexed
   @Field("taxon_authority_s")
   private String taxonAuthority;
   @Indexed
   @Field("taxon_s")
   private String taxon;
   @Indexed
   @Field("taxon_swarm_is")
   private Set<Integer> taxonIdSwarm;
   @Indexed
   @Field("taxon_synonym_is")
   private Set<Integer> taxonSynonymIds;

   @Indexed
   @Field("owner_s")
   private String owner;

   @Indexed
   @Field(SAMPLE_FROM)
   private String from;
   @Indexed
   @Field(SAMPLE_TO)
   private String to;
   @Indexed
   @Field("date_type_s")
   private VagueDate.Type dateType;
   @Indexed
   @Field("record_status_s")
   private RecordStatus recordStatus;
   @Indexed
   @Field("amount_s")
   private Amount amount;
   @Indexed
   @Field("settelment_status_s")
   private SettlementStatus settlementStatus;
   @Indexed
   @Field(VALIDATION_STATUS)
   private ValidationStatus validationStatus;
   @Indexed
   @Field(VALIDATION_STATUS_SORT_ORDER)
   private Integer validationStatusSortOrder;
   @Indexed
   @Field("validation_date_dt")
   private LocalDateTime validationDate;
   @Indexed
   @Field(VALIDATED_BY)
   private String validatedBy;
   @Indexed
   @Field("validator_i")
   private String validatorId;
   @Field("validator_first_name_s")
   private String validatorFirstName;
   @Indexed
   @Field("validator_last_name_s")
   private String validatorLastName;
   @Indexed
   @Field("validator_external_key_s")
   private String validatorExternalKey;
   @Indexed
   @Field("validator_email_s")
   private String validatorEmail;
   @Indexed
   @Field("validation_comment_s")
   private String validationComment;
   @Indexed
   @Field("sample_id_s")
   private String sampleId;
   @Indexed
   @Field("sample_creation_date_dt")
   private LocalDateTime sampleCreationDate;
   @Indexed
   @Field("sample_created_by_s")
   private String sampleCreatedBy;
   @Indexed
   @Field("sample_modification_date_dt")
   private LocalDateTime sampleModificationDate;
   @Indexed
   @Field("sample_modified_by_s")
   private String sampleModifiedBy;
   @Indexed
   @Field("vitality_s")
   private Occurrence.Vitality vitality;
   @Indexed
   @Field("status_fukarek_s")
   private Occurrence.SettlementStatusFukarek statusFukarek;
   @Indexed
   @Field("coveredArea_s")
   private Occurrence.Area coveredArea;
   @Indexed
   @Field("bloomingSprouts_s")
   private Occurrence.BloomingSprouts bloomingSprouts;
   @Indexed
   @Field("quantity_s")
   private Occurrence.Quantity quantity;
   @Indexed
   @Field("determination_comment_txt_de")
   private String determinationComment;
   @Indexed
   @Field("numeric_amount_i")
   private Integer numericAmount;
   @Indexed
   @Field("numeric_amount_accuracy_s")
   private Occurrence.NumericAmountAccuracy numericAmountAccuracy;
   @Indexed
   @Field("sex_s")
   private Occurrence.Sex sex;
   @Indexed
   @Field("life_stage_s")
   private Occurrence.LifeStage lifeStage;
   @Indexed
   @Field("makropter_s")
   private Occurrence.Makropter makropter;
   @Indexed
   @Field("repoduction_s")
   private Occurrence.Reproduction reproduction;
   @Indexed
   @Field("sample_sample_method_s")
   private Sample.SampleMethod sampleMethod;
   @Indexed
   @Field("locality_s")
   private String locality;
   @Indexed
   @Field("location_comment_s")
   private String locationComment;
   @Indexed
   @Field("habitat_s")
   private String habitat;
   @Indexed
   @Field("herbary_code_s")
   private String herbaryCode;
   @Indexed
   @Field("herbary_info_s")
   private String herbaryInfo;
   @Indexed
   @Dynamic
   @Field("*_media_type_s")
   private Map<String, String> mediaTypes = new HashMap<>();
   @Indexed
   @Dynamic
   @Field("*_media_path_s")
   private Map<String, String> mediaPaths = new HashMap<>();
   @Indexed
   @Dynamic
   @Field("*_media_description_s")
   private Map<String, String> mediaDescriptions = new HashMap<>();
   @Indexed
   @Field(OCCURRENCE_ABSENCE)
   private Boolean absence;
   @Indexed
   @Field("time_of_day_s")
   private String timeOfDay;
   @Indexed
   @Field("remark_s")
   private String remark;
   @Indexed
   @Field(PROJECT_TAG)
   private Set<String> projectTags;
   @Indexed
   @Field("citeId_s")
   private String citeId;
   @Indexed
   @Field("citeComment_s")
   private String citeComment;

   public String getHerbaryCode()
   {
      return herbaryCode;
   }

   public void setHerbaryCode(String herbaryCode)
   {
      this.herbaryCode = herbaryCode;
   }

   public String getHerbaryInfo()
   {
      return herbaryInfo;
   }

   public void setHerbaryInfo(String herbaryInfo)
   {
      this.herbaryInfo = herbaryInfo;
   }

   public String getHabitat()
   {
      return habitat;
   }

   public void setHabitat(String habitat)
   {
      this.habitat = habitat;
   }

   public String getObservers()
   {
      return observers;
   }

   public void setObservers(String observers)
   {
      this.observers = observers;
   }

   public String getLocationComment()
   {
      return locationComment;
   }

   public void setLocationComment(String locationComment)
   {
      this.locationComment = locationComment;
   }

   public String getLocality()
   {
      return locality;
   }

   public void setLocality(String locality)
   {
      this.locality = locality;
   }

   public Occurrence.Sex getSex()
   {
      return sex;
   }

   public void setSex(Occurrence.Sex sex)
   {
      this.sex = sex;
   }

   public Occurrence.LifeStage getLifeStage()
   {
      return lifeStage;
   }

   public void setLifeStage(Occurrence.LifeStage lifeStage)
   {
      this.lifeStage = lifeStage;
   }

   public Occurrence.Makropter getMakropter()
   {
      return makropter;
   }

   public void setMakropter(Occurrence.Makropter makropter)
   {
      this.makropter = makropter;
   }

   public Occurrence.Reproduction getReproduction()
   {
      return reproduction;
   }

   public void setReproduction(Occurrence.Reproduction reproduction)
   {
      this.reproduction = reproduction;
   }

   public Integer getNumericAmount()
   {
      return numericAmount;
   }

   public void setNumericAmount(Integer numericAmount)
   {
      this.numericAmount = numericAmount;
   }

   public Occurrence.Vitality getVitality()
   {
      return vitality;
   }

   public void setVitality(Occurrence.Vitality vitality)
   {
      this.vitality = vitality;
   }

   public Occurrence.SettlementStatusFukarek getStatusFukarek()
   {
      return statusFukarek;
   }

   public void setStatusFukarek(
         Occurrence.SettlementStatusFukarek statusFukarek)
   {
      this.statusFukarek = statusFukarek;
   }

   public Occurrence.Area getCoveredArea()
   {
      return coveredArea;
   }

   public void setCoveredArea(Occurrence.Area coveredArea)
   {
      this.coveredArea = coveredArea;
   }

   public Occurrence.BloomingSprouts getBloomingSprouts()
   {
      return bloomingSprouts;
   }

   public void setBloomingSprouts(Occurrence.BloomingSprouts bloomingSprouts)
   {
      this.bloomingSprouts = bloomingSprouts;
   }

   public Occurrence.Quantity getQuantity()
   {
      return quantity;
   }

   public void setQuantity(Occurrence.Quantity quantity)
   {
      this.quantity = quantity;
   }

   public PositionType getPositionType()
   {
      return positionType;
   }

   public void setPositionType(PositionType positionType)
   {
      this.positionType = positionType;
   }

   public String getSampleId()
   {
      return sampleId;
   }

   public void setSampleId(String sampleId)
   {
      this.sampleId = sampleId;
   }

   public String getDeterminerExternalKey()
   {
      return determinerExternalKey;
   }

   public void setDeterminerExternalKey(String determinerExternalKey)
   {
      this.determinerExternalKey = determinerExternalKey;
   }

   public String getRecorderId()
   {
      return recorderId;
   }

   public void setRecorderId(String recorderId)
   {
      this.recorderId = recorderId;
   }

   public String getRecorderFirstName()
   {
      return recorderFirstName;
   }

   public void setRecorderFirstName(String recorderFirstName)
   {
      this.recorderFirstName = recorderFirstName;
   }

   public String getRecorderLastName()
   {
      return recorderLastName;
   }

   public void setRecorderLastName(String recorderLastName)
   {
      this.recorderLastName = recorderLastName;
   }

   public String getRecorderExternalKey()
   {
      return recorderExternalKey;
   }

   public void setRecorderExternalKey(String recorderExternalKey)
   {
      this.recorderExternalKey = recorderExternalKey;
   }

   public String getTaxonExternalKey()
   {
      return taxonExternalKey;
   }

   public void setTaxonExternalKey(String taxonExternalKey)
   {
      this.taxonExternalKey = taxonExternalKey;
   }

   public String getTaxonGroup()
   {
      return taxonGroup;
   }

   public void setTaxonGroup(String taxonGroup)
   {
      this.taxonGroup = taxonGroup;
   }

   public String getTaxonAuthority()
   {
      return taxonAuthority;
   }

   public void setTaxonAuthority(String taxonAuthority)
   {
      this.taxonAuthority = taxonAuthority;
   }

   public VagueDate.Type getDateType()
   {
      return dateType;
   }

   public void setDateType(VagueDate.Type dateType)
   {
      this.dateType = dateType;
   }

   public RecordStatus getRecordStatus()
   {
      return recordStatus;
   }

   public void setRecordStatus(RecordStatus recordStatus)
   {
      this.recordStatus = recordStatus;
   }

   public Amount getAmount()
   {
      return amount;
   }

   public void setAmount(Amount amount)
   {
      this.amount = amount;
   }

   public SettlementStatus getSettlementStatus()
   {
      return settlementStatus;
   }

   public void setSettlementStatus(SettlementStatus settlementStatus)
   {
      this.settlementStatus = settlementStatus;
   }

   public String getFrom()
   {
      return from;
   }

   public void setFrom(String from)
   {
      this.from = from;
   }

   public String getTo()
   {
      return to;
   }

   public void setTo(String to)
   {
      this.to = to;
   }

   public String getMtb()
   {
      return mtb;
   }

   public void setMtb(String mtb)
   {
      this.mtb = mtb;
   }

   public int getBlur()
   {
      return blur;
   }

   public void setBlur(int blur)
   {
      this.blur = blur;
   }

   public String getOwner()
   {
      return owner;
   }

   public void setOwner(String owner)
   {
      this.owner = owner;
   }

   public Set<Integer> getTaxonSynonymIds()
   {
      return taxonSynonymIds;
   }

   public void setTaxonSynonymIds(Set<Integer> taxonSynonymIds)
   {
      this.taxonSynonymIds = taxonSynonymIds;
   }

   public int getTaxonId()
   {
      return taxonId;
   }

   public void setTaxonId(int taxonId)
   {
      this.taxonId = taxonId;
   }

   public String getTaxon()
   {
      return taxon;
   }

   public void setTaxon(String taxon)
   {
      this.taxon = taxon;
   }

   public Set<Integer> getTaxonIdSwarm()
   {
      return taxonIdSwarm;
   }

   public void setTaxonIdSwarm(Set<Integer> taxaSwarm)
   {
      this.taxonIdSwarm = taxaSwarm;
   }

   public String getDeterminerId()
   {
      return determinerId;
   }

   public void setDeterminerId(String determinerId)
   {
      this.determinerId = determinerId;
   }

   public String getDeterminerFirstName()
   {
      return determinerFirstName;
   }

   public void setDeterminerFirstName(String dterminerFirstName)
   {
      this.determinerFirstName = dterminerFirstName;
   }

   public String getDeterminerLastName()
   {
      return determinerLastName;
   }

   public void setDeterminerLastName(String dterminerLastName)
   {
      this.determinerLastName = dterminerLastName;
   }

   public int getPortalId()
   {
      return portalId;
   }

   public void setPortalId(int portalId)
   {
      this.portalId = portalId;
   }

   public String getPortalName()
   {
      return portalName;
   }

   public void setPortalName(String portalName)
   {
      this.portalName = portalName;
   }

   public String getWkt()
   {
      return wkt;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public int getProjectId()
   {
      return projectId;
   }

   public void setProjectId(int projectId)
   {
      this.projectId = projectId;
   }

   public String getProjectName()
   {
      return projectName;
   }

   public void setProjectName(String projectName)
   {
      this.projectName = projectName;
   }

   public Availability getProjectAvailability()
   {
      return projectAvailability;
   }

   public void setProjectAvailability(Availability projectAvailability)
   {
      this.projectAvailability = projectAvailability;
   }

   public String getWktOrig()
   {
      return wktOrig;
   }

   public void setWktOrig(String wktOrig)
   {
      this.wktOrig = wktOrig;
   }

   public int getSrefOrig()
   {
      return srefOrig;
   }

   public void setSrefOrig(int srefOrig)
   {
      this.srefOrig = srefOrig;
   }

   public LocalDateTime getOccurrenceCreationDate()
   {
      return occurrenceCreationDate;
   }

   public void setOccurrenceCreationDate(LocalDateTime creationDate)
   {
      this.occurrenceCreationDate = creationDate;
   }

   public String getOccurrenceCreatedBy()
   {
      return occurrenceCreatedBy;
   }

   public void setOccurrenceCreatedBy(String createdBy)
   {
      this.occurrenceCreatedBy = createdBy;
   }

   public LocalDateTime getOccurrenceModificationDate()
   {
      return occurrenceModificationDate;
   }

   public void setOccurrenceModificationDate(LocalDateTime modificationDate)
   {
      this.occurrenceModificationDate = modificationDate;
   }

   public String getOccurrenceModifiedBy()
   {
      return occurrenceModifiedBy;
   }

   public void setOccurrenceModifiedBy(String modifiedBy)
   {
      this.occurrenceModifiedBy = modifiedBy;
   }

   public LocalDateTime getPortalCreationDate()
   {
      return portalCreationDate;
   }

   public void setPortalCreationDate(LocalDateTime portalCreationDate)
   {
      this.portalCreationDate = portalCreationDate;
   }

   public String getPortalCreatedBy()
   {
      return portalCreatedBy;
   }

   public void setPortalCreatedBy(String portalCreatedBy)
   {
      this.portalCreatedBy = portalCreatedBy;
   }

   public LocalDateTime getPortalModificationDate()
   {
      return portalModificationDate;
   }

   public void setPortalModificationDate(LocalDateTime portalModificationDate)
   {
      this.portalModificationDate = portalModificationDate;
   }

   public String getPortalModifiedBy()
   {
      return portalModifiedBy;
   }

   public void setPortalModifiedBy(String portalModifiedBy)
   {
      this.portalModifiedBy = portalModifiedBy;
   }

   public LocalDateTime getProjectCreationDate()
   {
      return projectCreationDate;
   }

   public void setProjectCreationDate(LocalDateTime projectCreationDate)
   {
      this.projectCreationDate = projectCreationDate;
   }

   public String getProjectCreatedBy()
   {
      return projectCreatedBy;
   }

   public void setProjectCreatedBy(String projectCreatedBy)
   {
      this.projectCreatedBy = projectCreatedBy;
   }

   public LocalDateTime getProjectModificationDate()
   {
      return projectModificationDate;
   }

   public void setProjectModificationDate(LocalDateTime projectModificationDate)
   {
      this.projectModificationDate = projectModificationDate;
   }

   public String getProjectModifiedBy()
   {
      return projectModifiedBy;
   }

   public void setProjectModifiedBy(String projectModifiedBy)
   {
      this.projectModifiedBy = projectModifiedBy;
   }

   public LocalDateTime getSampleCreationDate()
   {
      return sampleCreationDate;
   }

   public void setSampleCreationDate(LocalDateTime sampleCreationDate)
   {
      this.sampleCreationDate = sampleCreationDate;
   }

   public String getSampleCreatedBy()
   {
      return sampleCreatedBy;
   }

   public void setSampleCreatedBy(String sampleCreatedBy)
   {
      this.sampleCreatedBy = sampleCreatedBy;
   }

   public LocalDateTime getSampleModificationDate()
   {
      return sampleModificationDate;
   }

   public void setSampleModificationDate(LocalDateTime sampleModificationDate)
   {
      this.sampleModificationDate = sampleModificationDate;
   }

   public String getSampleModifiedBy()
   {
      return sampleModifiedBy;
   }

   public void setSampleModifiedBy(String sampleModifiedBy)
   {
      this.sampleModifiedBy = sampleModifiedBy;
   }

   public String getPortalUrl()
   {
      return portalUrl;
   }

   public void setPortalUrl(String portalUrl)
   {
      this.portalUrl = portalUrl;
   }

   public String getProjectDescription()
   {
      return projectDescription;
   }

   public void setProjectDescription(String projectDescription)
   {
      this.projectDescription = projectDescription;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public String getWktOrigOld()
   {
      return wktOrigOld;
   }

   public void setWktOrigOld(String wktOrigOld)
   {
      this.wktOrigOld = wktOrigOld;
   }

   public String getDeterminationComment()
   {
      return determinationComment;
   }

   public void setDeterminationComment(String determinationComment)
   {
      this.determinationComment = determinationComment;
   }

   public Sample.SampleMethod getSampleMethod()
   {
      return sampleMethod;
   }

   public void setSampleMethod(Sample.SampleMethod sampleMethod)
   {
      this.sampleMethod = sampleMethod;
   }

   public Map<String, String> getMediaTypes()
   {
      return mediaTypes;
   }

   public void setMediaTypes(Map<String, String> mediaTypes)
   {
      this.mediaTypes = mediaTypes;
   }

   public Map<String, String> getMediaPaths()
   {
      return mediaPaths;
   }

   public void setMediaPaths(Map<String, String> mediaPaths)
   {
      this.mediaPaths = mediaPaths;
   }

   public Boolean getAbsence()
   {
      return absence;
   }

   public void setAbsence(Boolean absence)
   {
      this.absence = absence;
   }

   public Map<String, String> getMediaDescriptions()
   {
      return mediaDescriptions;
   }

   public void setMediaDescriptions(Map<String, String> mediaDescriptions)
   {
      this.mediaDescriptions = mediaDescriptions;
   }

   public ValidationStatus getValidationStatus()
   {
      return validationStatus;
   }

   public void setValidationStatus(ValidationStatus validationStatus)
   {
      this.validationStatus = validationStatus;
   }

   public LocalDateTime getValidationDate()
   {
      return validationDate;
   }

   public void setValidationDate(LocalDateTime validationDate)
   {
      this.validationDate = validationDate;
   }

   public String getValidatedBy()
   {
      return validatedBy;
   }

   public void setValidatedBy(String validatedBy)
   {
      this.validatedBy = validatedBy;
   }

   public String getValidatorFirstName()
   {
      return validatorFirstName;
   }

   public void setValidatorFirstName(String validatorFirstName)
   {
      this.validatorFirstName = validatorFirstName;
   }

   public String getValidatorLastName()
   {
      return validatorLastName;
   }

   public void setValidatorLastName(String validatorLastName)
   {
      this.validatorLastName = validatorLastName;
   }

   public String getValidatorExternalKey()
   {
      return validatorExternalKey;
   }

   public void setValidatorExternalKey(String validatorExternalKey)
   {
      this.validatorExternalKey = validatorExternalKey;
   }

   public String getValidatorId()
   {
      return validatorId;
   }

   public void setValidatorId(String validatorId)
   {
      this.validatorId = validatorId;
   }

   public String getValidatorEmail()
   {
      return validatorEmail;
   }

   public void setValidatorEmail(String validatorEmail)
   {
      this.validatorEmail = validatorEmail;
   }

   public String getDeterminerEmail()
   {
      return determinerEmail;
   }

   public void setDeterminerEmail(String determinerEmail)
   {
      this.determinerEmail = determinerEmail;
   }

   public String getRecorderEmail()
   {
      return recorderEmail;
   }

   public void setRecorderEmail(String recorderEmail)
   {
      this.recorderEmail = recorderEmail;
   }

   public String getTimeOfDay()
   {
      return timeOfDay;
   }

   public void setTimeOfDay(String timeOfDay)
   {
      this.timeOfDay = timeOfDay;
   }

   public Occurrence.NumericAmountAccuracy getNumericAmountAccuracy()
   {
      return numericAmountAccuracy;
   }

   public void setNumericAmountAccuracy(
         Occurrence.NumericAmountAccuracy numericAmountAccuracy)
   {
      this.numericAmountAccuracy = numericAmountAccuracy;
   }

   public String getValidationComment()
   {
      return validationComment;
   }

   public void setValidationComment(String validationComment)
   {
      this.validationComment = validationComment;
   }

   public Integer getValidationStatusSortOrder()
   {
      return validationStatusSortOrder;
   }

   public void setValidationStatusSortOrder(Integer validationStatusSortOrder)
   {
      this.validationStatusSortOrder = validationStatusSortOrder;
   }

   public Boolean getProjectWerbeoOriginal()
   {
      return projectWerbeoOriginal;
   }

   public void setProjectWerbeoOriginal(Boolean projectWerbeoOriginal)
   {
      this.projectWerbeoOriginal = projectWerbeoOriginal;
   }

   public Boolean getProjectAllowDataEntry()
   {
      return projectAllowDataEntry;
   }

   public void setProjectAllowDataEntry(Boolean projectAllowDataEntry)
   {
      this.projectAllowDataEntry = projectAllowDataEntry;
   }

   public Set<String> getProjectTags()
   {
      return projectTags;
   }

   public void setProjectTags(Set<String> tags)
   {
      this.projectTags = tags;
   }

   @Override
   public String toString()
   {
      return CacheOccurrenceBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return CacheOccurrenceBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return CacheOccurrenceBeanUtil.doEquals(this, obj);
   }

   public String getProjectObfuscationPolicies()
   {
      return projectObfuscationPolicies;
   }

   public void setProjectObfuscationPolicies(String projectObfuscationPolicies)
   {
      this.projectObfuscationPolicies = projectObfuscationPolicies;
   }

   public String getRemark()
   {
      return remark;
   }

   public void setRemark(String remark)
   {
      this.remark = remark;
   }

   public String getCiteId()
   {
      return citeId;
   }

   public void setCiteId(String citeId)
   {
      this.citeId = citeId;
   }

   public String getCiteComment()
   {
      return citeComment;
   }

   public void setCiteComment(String citeComment)
   {
      this.citeComment = citeComment;
   }
}
