package org.infinitenature.werbeo.service.adapters.index;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.solr.SolrProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@Configuration
@EnableSolrRepositories
public class SolrConfig
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SolrConfig.class);
   @Autowired
   private SolrProperties properties;

   @Bean
   @Primary
   public SolrClient solrClient()
   {
      return new HttpSolrClient.Builder(this.properties.getHost()).build();
   }

   @Bean
   public ConcurrentUpdateSolrClient updateSolrClient()
   {
      return new ConcurrentUpdateSolrClient.Builder(this.properties.getHost())
            .build();
   }

   @Bean("updateSolrTemplate")
   public SolrTemplate updateSolrTemplate(
         @Autowired ConcurrentUpdateSolrClient client)
   {
      return new SolrTemplate(client);
   }

   @Bean
   public SolrTemplate solrTemplate(@Autowired SolrClient client)
   {
      LOGGER.info("Create solr template with client {}", client);
      return new SolrTemplate(client);
   }

}
