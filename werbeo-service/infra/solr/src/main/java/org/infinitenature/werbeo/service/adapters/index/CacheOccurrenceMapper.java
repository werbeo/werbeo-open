package org.infinitenature.werbeo.service.adapters.index;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring", imports = UUID.class)
public abstract class CacheOccurrenceMapper
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(CacheOccurrenceMapper.class);

   @Autowired
   private PositionFactory positionFactory;
   @Autowired
   protected TaxonQueries taxonQueries;
   @Autowired
   private CoordinateTransformerFactory coordinateTransformerFactory;

   @Mapping(target = "validationComment", source = "validation.comment")
   @Mapping(target = "validatedBy", source = "validation.modifiedBy.login")
   @Mapping(target = "validationDate", source = "validation.modificationDate")
   @Mapping(target = "validationStatus", source = "validation.status")
   @Mapping(target = "validationStatusSortOrder", source = "validation.status")
   @Mapping(target = "validatorEmail", source = "validation.validator.email")
   @Mapping(target = "validatorFirstName", source = "validation.validator.firstName")
   @Mapping(target = "validatorLastName", source = "validation.validator.lastName")
   @Mapping(target = "validatorId", source = "validation.validator.id")
   @Mapping(target = "validatorExternalKey", source = "validation.validator.externalKey")
   @Mapping(target = "mediaDescriptions", ignore = true) // mapped with
                                                         // @AfterMapping
   @Mapping(target = "mediaPaths", ignore = true) // mapped with @AfterMapping
   @Mapping(target = "mediaTypes", ignore = true) // mapped with @AfterMapping
   @Mapping(target = "coveredArea", source = "coveredArea")
   @Mapping(target = "statusFukarek", source = "settlementStatusFukarek")
   @Mapping(target = "wktOrigOld", ignore = true)
   @Mapping(target = "vitality", source = "vitality")
   @Mapping(target = "portalUrl", source = "sample.survey.portal.url")
   @Mapping(target = "occurrenceCreationDate", source = "creationDate")
   @Mapping(target = "occurrenceModificationDate", source = "modificationDate")
   @Mapping(target = "portalCreatedBy", source = "sample.survey.portal.createdBy.login")
   @Mapping(target = "portalCreationDate", source = "sample.survey.portal.creationDate")
   @Mapping(target = "portalModificationDate", source = "sample.survey.portal.modificationDate")
   @Mapping(target = "portalModifiedBy", source = "sample.survey.portal.modifiedBy.login")

   @Mapping(target = "projectCreatedBy", source = "sample.survey.createdBy.login")
   @Mapping(target = "projectCreationDate", source = "sample.survey.creationDate")
   @Mapping(target = "projectModificationDate", source = "sample.survey.modificationDate")
   @Mapping(target = "projectModifiedBy", source = "sample.survey.modifiedBy.login")
   @Mapping(target = "projectWerbeoOriginal", source = "sample.survey.werbeoOriginal")
   @Mapping(target = "projectAllowDataEntry", source = "sample.survey.allowDataEntry")
   @Mapping(target = "projectDescription", source = "sample.survey.description")
   @Mapping(target = "projectObfuscationPolicies", expression = "java(mapObfuscationPolicies(occurrence.getSample().getSurvey().getObfuscationPolicies()))")
   @Mapping(target = "projectId", source = "sample.survey.id")
   @Mapping(target = "projectName", source = "sample.survey.name")
   @Mapping(target = "projectTags", source = "sample.survey.tags")

   @Mapping(target = "sampleCreatedBy", source = "sample.createdBy.login")
   @Mapping(target = "sampleCreationDate", source = "sample.creationDate")
   @Mapping(target = "sampleModificationDate", source = "sample.modificationDate")
   @Mapping(target = "sampleModifiedBy", source = "sample.modifiedBy.login")
   @Mapping(target = "srefOrig", source = "sample.locality.position.epsg")
   @Mapping(target = "wktOrig", source = "sample.locality.position.wkt")
   @Mapping(target = "positionType", source = "sample.locality.position.type")
   @Mapping(target = "recorderExternalKey", source = "sample.recorder.externalKey")
   @Mapping(target = "recorderFirstName", source = "sample.recorder.firstName")
   @Mapping(target = "recorderId", source = "sample.recorder.id")
   @Mapping(target = "recorderLastName", source = "sample.recorder.lastName")
   @Mapping(target = "recorderEmail", source = "sample.recorder.email")
   @Mapping(target = "taxonId", source = "taxon.id")
   @Mapping(target = "taxon", source = "taxon.name")
   @Mapping(target = "taxonIdSwarm", ignore = true) // done in mapTaxa
   @Mapping(target = "taxonSynonymIds", ignore = true) // done in mapTaxa
   @Mapping(target = "taxonAuthority", source = "taxon.authority")
   @Mapping(target = "taxonExternalKey", source = "taxon.externalKey")
   @Mapping(target = "taxonGroup", source = "taxon.group")
   @Mapping(target = "blur", source = "sample.locality.precision")
   @Mapping(target = "id", expression = "java(occurrence.getId().toString())")
   @Mapping(target = "sampleId", expression = "java(occurrence.getSample().getId().toString())")
   @Mapping(target = "determinerId", source = "determiner.id")
   @Mapping(target = "determinerFirstName", source = "determiner.firstName")
   @Mapping(target = "determinerLastName", source = "determiner.lastName")
   @Mapping(target = "determinerEmail", source = "determiner.email")
   @Mapping(target = "determinerExternalKey", source = "determiner.externalKey")
   @Mapping(target = "wkt", ignore = true) // done in convertWKT
   @Mapping(target = "portalId", source = "sample.survey.portal.id")
   @Mapping(target = "portalName", source = "sample.survey.portal.title")
   @Mapping(target = "owner", source = "createdBy.login")
   @Mapping(target = "from", source = "sample.date.startDate")
   @Mapping(target = "to", source = "sample.date.endDate")
   @Mapping(target = "dateType", source = "sample.date.type")
   @Mapping(target = "mtb", source = "sample.locality.position.mtb.mtb")
   @Mapping(target = "projectAvailability", source = "sample.survey.availability")
   @Mapping(target = "occurrenceModifiedBy", source = "modifiedBy.login")
   @Mapping(target = "occurrenceCreatedBy", source = "createdBy.login")
   @Mapping(target = "sampleMethod", source = "sample.sampleMethod")
   @Mapping(target = "locality", source = "sample.locality.locality")
   @Mapping(target = "locationComment", source = "sample.locality.locationComment")
   @Mapping(target = "observers", source = "observers")
   @Mapping(target = "habitat", source = "habitat")
   @Mapping(target = "remark", source = "remark")
   @Mapping(target = "citeId", source = "citeId")
   @Mapping(target = "citeComment", source = "citeComment")
   @Mapping(target = "herbaryCode", source = "herbarium.code")
   @Mapping(target = "herbaryInfo", source = "herbarium.herbary")
   abstract CacheOccurrence map(Occurrence occurrence);

   abstract Collection<CacheOccurrence> map(Collection<Occurrence> occurrences);

   @AfterMapping
   void mapMedia(@MappingTarget CacheOccurrence cacheOccurrence,
         Occurrence occurrence)
   {
      int i = 0;
      for (Medium medium : occurrence.getMedia())
      {
         String key = String.valueOf(i);
         cacheOccurrence.getMediaTypes().put(key, medium.getType().name());
         cacheOccurrence.getMediaPaths().put(key, medium.getPath());
         cacheOccurrence.getMediaDescriptions().put(key,
               medium.getDescription());
         i++;
      }
   }

   @AfterMapping
   void mapTaxa(@MappingTarget CacheOccurrence cacheOccurrence,
         Occurrence occurrence)
   {
      cacheOccurrence.setTaxonIdSwarm(
            taxonQueries.findParrentSwarm(occurrence.getTaxon()).stream()
                  .map(TaxonBase::getId).collect(Collectors.toSet()));

      cacheOccurrence.setTaxonSynonymIds(
            taxonQueries.findAllSynonyms(occurrence.getTaxon().getId(), null)
                  .stream().map(Taxon::getId).collect(Collectors.toSet()));
   }


   public String mapObfuscationPolicies(List<ObfuscationPolicy> policies)
   {
      List<String> policyStrings = new ArrayList<>();
      policies.forEach(policy -> policyStrings
            .add(policy.getRole() + "_" + policy.getPermission()));
      return StringUtils.join(policyStrings, ",");
   }

   @AfterMapping
   void convertWkt(@MappingTarget CacheOccurrence cacheOccurrence,
         Occurrence occurrence)

   {
      CoordinateTransformer transformer = coordinateTransformerFactory
            .getCoordinateTransformer(
                  occurrence.getSample().getLocality().getPosition().getEpsg(),
                  4326);
      cacheOccurrence.setWkt(transformer.convert(
            occurrence.getSample().getLocality().getPosition().getWkt()));
   }

   @Mapping(target = "allowedOperations", ignore = true)
   @Mapping(target = "obfuscated", ignore = true)
   @Mapping(target = "media", ignore = true) // mapped with @AfterMapping
   @Mapping(target = "creationDate", source = "occurrenceCreationDate")
   @Mapping(target = "modificationDate", source = "occurrenceModificationDate")
   @Mapping(target = "determiner.id", source = "determinerId")
   @Mapping(target = "determiner.firstName", source = "determinerFirstName")
   @Mapping(target = "determiner.lastName", source = "determinerLastName")
   @Mapping(target = "determiner.email", source = "determinerEmail")
   @Mapping(target = "determiner.externalKey", source = "determinerExternalKey")
   @Mapping(target = "id", expression = "java(UUID.fromString(cacheOccurrence.getId()))")
   @Mapping(target = "modifiedBy.login", source = "occurrenceModifiedBy")
   @Mapping(target = "taxon.taxaList", expression = "java(taxonQueries.get(cacheOccurrence.getTaxonId(), context).getTaxaList())")
   @Mapping(target = "taxon.id", source = "taxonId")
   @Mapping(target = "taxon.name", source = "taxon")
   @Mapping(target = "taxon.externalKey", source = "taxonExternalKey")
   @Mapping(target = "taxon.group", source = "taxonGroup")
   @Mapping(target = "taxon.authority", source = "taxonAuthority")
   @Mapping(target = "createdBy.login", source = "owner")
   @Mapping(target = "sample", expression = "java(mapSampleBase(cacheOccurrence))")
   @Mapping(target = "herbarium.code", source = "herbaryCode")
   @Mapping(target = "herbarium.herbary", source = "herbaryInfo")
   @Mapping(target = "settlementStatusFukarek", source = "statusFukarek")
   @Mapping(target = "validation.status", source = "validationStatus")
   @Mapping(target = "validation.modificationDate", source = "validationDate")
   @Mapping(target = "validation.modifiedBy.login", source = "validatedBy")
   @Mapping(target = "validation.validator.firstName", source = "validatorFirstName")
   @Mapping(target = "validation.validator.lastName", source = "validatorLastName")
   @Mapping(target = "validation.validator.externalKey", source = "validatorExternalKey")
   @Mapping(target = "validation.validator.email", source = "validatorEmail")
   @Mapping(target = "validation.validator.id", source = "validatorId")
   @Mapping(target = "validation.comment", source = "validationComment")
   @Mapping(target = "remark", source = "remark")
   abstract Occurrence map(CacheOccurrence cacheOccurrence,
         @org.mapstruct.Context Context context);

   @AfterMapping
   void mapMedia(@MappingTarget Occurrence occurrence,
         CacheOccurrence cacheOccurrence)
   {
      for (int i = 0; i < cacheOccurrence.getMediaPaths().size(); i++)
      {
         String key = String.valueOf(i);
         occurrence.getMedia()
               .add(new Medium(
                     cacheOccurrence.getMediaPaths().get(key),
                     cacheOccurrence.getMediaDescriptions()
                           .get(key),
                     MediaType
                           .valueOf(cacheOccurrence.getMediaTypes().get(key))));
      }
   }
   @Mapping(target = "allowedOperations", ignore = true)
   @Mapping(target = "obfuscated", ignore = true)
   @Mapping(target = "survey", expression = "java(mapSurvey(cacheOccurrence))")
   @Mapping(target = "createdBy.login", source = "sampleCreatedBy")
   @Mapping(target = "creationDate", source = "sampleCreationDate")
   @Mapping(target = "modificationDate", source = "sampleModificationDate")
   @Mapping(target = "modifiedBy.login", source = "sampleModifiedBy")
   @Mapping(target = "locality.position", expression = "java(mapPosition(cacheOccurrence))")
   @Mapping(target = "locality.precision", source = "blur")
   @Mapping(target = "locality.locality", source = "locality")
   @Mapping(target = "locality.locationComment", source = "locationComment")
   @Mapping(target = "recorder.id", source = "recorderId")
   @Mapping(target = "recorder.firstName", source = "recorderFirstName")
   @Mapping(target = "recorder.lastName", source = "recorderLastName")
   @Mapping(target = "recorder.email", source = "recorderEmail")
   @Mapping(target = "recorder.externalKey", source = "recorderExternalKey")
   @Mapping(target = "id", expression = "java(UUID.fromString(cacheOccurrence.getSampleId()))")
   @Mapping(target = "date", expression = "java(mapVagueDate(cacheOccurrence))")
   abstract SampleBase mapSampleBase(CacheOccurrence cacheOccurrence);

   @Mapping(target = "werbeoOriginal", source = "projectWerbeoOriginal", defaultValue = "true")
   @Mapping(target = "allowDataEntry", source = "projectAllowDataEntry", defaultValue = "true")
   @Mapping(target = "allowedOperations", ignore = true)
   @Mapping(target = "obfuscated", ignore = true)
   @Mapping(target = "createdBy.login", source = "projectCreatedBy")
   @Mapping(target = "creationDate", source = "projectCreationDate")
   @Mapping(target = "modificationDate", source = "projectModificationDate")
   @Mapping(target = "modifiedBy.login", source = "projectModifiedBy")
   @Mapping(target = "availability", source = "projectAvailability")
   @Mapping(target = "obfuscationPolicies", expression = "java(mapObfuscationPoliciesToCore(cacheOccurrence.getProjectObfuscationPolicies()))")
   @Mapping(target = "container", ignore = true)
   @Mapping(target = "deputyCustodians", ignore = true)
   @Mapping(target = "description", source = "projectDescription")
   @Mapping(target = "parentId", ignore = true)
   @Mapping(target = "owner", ignore = true)
   @Mapping(target = "id", source = "projectId")
   @Mapping(target = "name", source = "projectName")
   @Mapping(target = "portal", expression = "java(mapPortal(cacheOccurrence))")
   @Mapping(target = "tags", source = "projectTags")
   abstract Survey mapSurvey(CacheOccurrence cacheOccurrence);

   @Mapping(target = "allowedOperations", ignore = true)
   @Mapping(target = "obfuscated", ignore = true)
   @Mapping(target = "url", source = "portalUrl")
   @Mapping(target = "createdBy.login", source = "portalCreatedBy")
   @Mapping(target = "modifiedBy.login", source = "portalModifiedBy")
   @Mapping(target = "modificationDate", source = "portalModificationDate")
   @Mapping(target = "creationDate", source = "portalCreationDate")
   @Mapping(target = "id", source = "portalId")
   @Mapping(target = "title", source = "portalName")
   abstract Portal mapPortal(CacheOccurrence cacheOccurrence);

   abstract List<Occurrence> mapToCore(
         Collection<CacheOccurrence> cacheOccurrences,
         @org.mapstruct.Context Context context);

   Position mapPosition(CacheOccurrence cacheOccurrence)
   {
      switch (cacheOccurrence.getPositionType())
      {
      case MTB:
         return positionFactory.createFromMTB(cacheOccurrence.getMtb());
      default:
         String wktOrig = cacheOccurrence.getWktOrig();
         if (StringUtils.isBlank(wktOrig))
         {
            return positionFactory.create(cacheOccurrence.getWktOrigOld(),
                  cacheOccurrence.getSrefOrig());
         } else
         {
            return positionFactory.create(wktOrig,
                  cacheOccurrence.getSrefOrig());
         }
      }
   }

   public List<ObfuscationPolicy> mapObfuscationPoliciesToCore(
         String obfuscationPolicies)
   {
      List<ObfuscationPolicy> policies = new ArrayList<>();
      if (StringUtils.isNotBlank(obfuscationPolicies))
      {
         for (String policy : obfuscationPolicies.split(","))
         {
            String[] policySplit = policy.split("_");
            policies.add(new ObfuscationPolicy(policySplit[0],
                  policy.substring(policy.indexOf("_") + 1)));
         }
      }
      return policies;
   }

   VagueDate mapVagueDate(CacheOccurrence cacheOccurrence)
   {
      try
      {
         LocalDate fromDate = parseDate(cacheOccurrence.getFrom());
         LocalDate toDate = parseDate(cacheOccurrence.getTo());
         return VagueDateFactory.create(fromDate, toDate,
               cacheOccurrence.getDateType().getStringRepresentation());
      } catch (Exception e)
      {
         LOGGER.error(
               "Failure mapping date for cacheOccurrence with id {}, set dummy date",
               cacheOccurrence.getId(), e);

         return VagueDateFactory.create(500);
      }
   }

   protected LocalDate parseDate(String date)
   {
      // remove day of the week, because it fails with quite old dates
      // see https://git.loe.auf.uni-rostock.de/werbeo/werbeo/issues/133
      return LocalDate.parse(date.substring(4, date.length()), DateTimeFormatter
            .ofPattern("MMM dd kk:mm:ss z yyyy").withLocale(Locale.ENGLISH));
   }

   protected int toSortOrder(ValidationStatus validationStatus)
   {

      return validationStatus == null ? 0 : validationStatus.sortOrder;
   }
}
