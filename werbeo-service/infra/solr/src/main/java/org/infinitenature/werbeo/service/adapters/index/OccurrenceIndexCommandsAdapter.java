package org.infinitenature.werbeo.service.adapters.index;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceIndexCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.PartialUpdate;
import org.springframework.data.solr.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceIndexCommandsAdapter implements OccurrenceIndexCommands
{
   private Duration updateCommitTime = Duration.ofSeconds(100);
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceIndexCommandsAdapter.class);
   @Autowired
   private OccurrenceCacheRepository repo;
   @Autowired
   private CacheOccurrenceMapper mapper;
   @Autowired
   @Qualifier("updateSolrTemplate")
   private SolrTemplate solrTemplate;

   @Override
   public void index(Collection<Occurrence> occurrences, boolean enforceCommit)
   {
      try
      {
         indexBatch(occurrences);
      } catch (Exception e)
      {
         LOGGER.error("Failure indexing occurrences in batch, tring one by one",
               e);
         occurrences.forEach(o -> indexSingle(o));
      }
      if (enforceCommit)
      {
         solrTemplate.softCommit(CacheOccurrence.COLLECTION);
      }
   }

   private void indexBatch(Collection<Occurrence> occurrences)
   {
      if (!occurrences.isEmpty())
      {
         StopWatch stopWatch = StopWatch.createStarted();
         Collection<CacheOccurrence> cacheOccurrences = mapper.map(occurrences);
         solrTemplate.saveBeans(CacheOccurrence.COLLECTION, cacheOccurrences,
               updateCommitTime);
         stopWatch.stop();

         LOGGER.info("Indexed {} occurrences in {}ms.", occurrences.size(),
               stopWatch.getTime());
      }
      if (LOGGER.isDebugEnabled())
      {
         occurrences.forEach(
               o -> LOGGER.debug("index occurrence with id {}", o.getId()));
      }
   }

   private void indexSingle(Occurrence occurrence)
   {
      try
      {
         repo.save(mapper.map(occurrence));
         LOGGER.info("index occurrence with id {}", occurrence.getId());
      } catch (Exception e1)
      {
         LOGGER.error("Failure indexing occurrence {}", occurrence);
         LOGGER.error("Exception: {}", e1);
         LOGGER.error("This occurrence will not be visible in the index!");
      }
   }

   @Override
   public void delete(Collection<UUID> ids)
   {
      ids.forEach(id ->
      {
         delete(id);
      });
   }

   @Override
   public void delete(UUID id)
   {
      repo.deleteById(id);
      LOGGER.info("Delete occurrence with id {}", id);
   }

   @Override
   public void deleteBySampleId(UUID sampleId)
   {
      LOGGER.info("Delete occurrence with sampleId {}", sampleId);
      repo.deleteBySampleId(sampleId.toString());
      solrTemplate.commit(CacheOccurrence.COLLECTION);
   }

   @Override
   @Async
   public void update(Survey survey)
   {
      String surveyName = survey.getName();
      Integer surveyId = survey.getId();
      LOGGER.info("Update occurrences wich belong to survey \"{}\" with id {}",
            surveyName, surveyId);

      List<CacheOccurrence> occurrenceIds = repo
            .findIdsByProjectId(surveyId);
      LOGGER.info("Found {} occrrences in survey {} to update",
            occurrenceIds.size(), surveyId);
      List<Update> updates = occurrenceIds.stream().map(CacheOccurrence::getId)
            .map(id -> createPartialUpdate(id, survey))
            .collect(Collectors.toList());
      LOGGER.info("Created partial updates for survey {}", surveyId);
      if (!updates.isEmpty()) // solr complains on empty collection
      {
         List<List<Update>> partitions = ListUtils.partition(updates, 1000);
         int i = 0;
         for (List<Update> partition : partitions)
         {
            solrTemplate.saveBeans(CacheOccurrence.COLLECTION, partition, Duration.ofSeconds(3));
            LOGGER.info(
                  "Send partition {} of updates to solr for survey \"{}\" with id {}",
                  i, surveyName, surveyId);
            i++;
            solrTemplate.commit(CacheOccurrence.COLLECTION);
         }
         
      }
   }

   private PartialUpdate createPartialUpdate(String occurrenceId, Survey survey)
   {
      PartialUpdate update = new PartialUpdate("id", occurrenceId);
      update.setValueOfField(CacheOccurrence.PROJECT_TAG, survey.getTags());
      update.setValueOfField(CacheOccurrence.PROJECT_AVAILABILITY,
            survey.getAvailability().toString());
      update.setValueOfField(CacheOccurrence.PROJECT_DESCRIPTION,
            survey.getDescription());
      update.setValueOfField(CacheOccurrence.PROJECT_NAME, survey.getName());
      update.setValueOfField(CacheOccurrence.PROJECT_WERBEO_ORIGINAL,
            survey.isWerbeoOriginal());
      update.setValueOfField(CacheOccurrence.PROJECT_OBFUSCATION_POLICIES,
            mapper.mapObfuscationPolicies(survey.getObfuscationPolicies()));
      update.setValueOfField(CacheOccurrence.PROJECT_ALLOW_DATA_ENTRY,
            survey.isAllowDataEntry());
      return update;
   }

   public void setUpdateCommitTime(Duration updateCommitTime)
   {
      this.updateCommitTime = updateCommitTime;
   }
}
