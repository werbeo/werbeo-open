package org.infinitenature.werbeo.service.adapters.index;

import org.springframework.data.solr.core.query.FacetOptions.FieldWithRangeParameters;

public class FieldWithStringRangeParameters extends
      FieldWithRangeParameters<FieldWithStringRangeParameters, String, String>
{

   public FieldWithStringRangeParameters(String name, String start, String end,
         String gap)
   {
      super(name, start, end, gap);
   }

}
