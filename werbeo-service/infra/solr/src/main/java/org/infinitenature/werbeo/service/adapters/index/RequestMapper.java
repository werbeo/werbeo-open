package org.infinitenature.werbeo.service.adapters.index;

import java.util.function.Function;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.spring.data.domain.OffsetPageable;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Mapper(componentModel = "spring")
public interface RequestMapper
{
   @Mapping(source = "count", target = "limit")
   @Mapping(target = "sort", expression = "java(mapSort(offsetRequest))")
   OffsetPageable map(OffsetRequest<OccurrenceSortField> offsetRequest);

   default Sort mapSort(OffsetRequest<OccurrenceSortField> offsetRequest)
   {
      return Sort.by(map(offsetRequest.getSortOrder()),
            getSortFieldMapper().apply(offsetRequest.getSortField()));
   }

   default Direction map(SortOrder sortOrder)
   {
      if (sortOrder == SortOrder.ASC)
      {
         return Direction.ASC;
      } else
      {
         return Direction.DESC;
      }
   }

   default Function<OccurrenceSortField, String[]> getSortFieldMapper()
   {

      return sortField ->
      {
         switch (sortField)
         {
         case MOD_DATE:
            return new String[] { "modification_date_dt" };
         case TAXON:
            return new String[] { "taxon_s" };
         case DETERMINER:
            return new String[] { "determiner_last_name_s",
                  "determiner_first_name_s" };
         case DATE:
            return new String[] { "from_dt", "to_dt" };
         case MEDIA:
            return new String[] {"0_media_type_s"};
         case TIME_OF_DAY:
            return new String[] {"time_of_day_s"};
         case VALIDATION_STATUS:
            return new String[] {
                  CacheOccurrence.VALIDATION_STATUS_SORT_ORDER };
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
