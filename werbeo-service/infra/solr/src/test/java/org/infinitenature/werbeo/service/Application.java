package org.infinitenature.werbeo.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = "org.infinitenature.werbeo")
public class Application
{

   @Bean
   public TaxonQueries taxonQueries()
   {
      TaxonQueries mock = mock(TaxonQueries.class);
      Taxon taxon = new Taxon();
      TaxaList taxaList = new TaxaList();
      taxaList.setId(1);
      taxaList.setName("German SL 1.2");

      taxon.setTaxaList(taxaList);
      when(mock.get(any(Integer.class), any())).thenReturn(taxon);
      return mock;
   }

   @Bean
   public PortalQueries portalQueries()
   {
      PortalQueries portalQueriesMock = mock(PortalQueries.class);

      when(portalQueriesMock.findAssociated(PortalTestUtil.FLORA_BB_ID))
            .thenReturn(new SliceImpl<>(
                  Arrays.asList(new Portal(PortalTestUtil.FLORA_BB_ID)),
                  new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                        PortalSortField.ID)));

      when(portalQueriesMock.findAssociated(PortalTestUtil.FLORA_MV_ID))
            .thenReturn(new SliceImpl<>(
                  Arrays.asList(new Portal(PortalTestUtil.FLORA_MV_ID)),
                  new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                        PortalSortField.ID)));

      when(portalQueriesMock.findAssociated(PortalTestUtil.REST_SRV_ID))
            .thenReturn(new SliceImpl<>(
                  Arrays.asList(new Portal(PortalTestUtil.FLORA_BB_ID),
                        new Portal(PortalTestUtil.FLORA_MV_ID)),
                  new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                        PortalSortField.ID)));

      when(portalQueriesMock.findAssociated(PortalTestUtil.HEUSCHRECKEN_D_ID))
            .thenReturn(new SliceImpl<>(
                  Arrays.asList(new Portal(PortalTestUtil.HEUSCHRECKEN_D_ID)),
                  new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                        PortalSortField.ID)));
      return portalQueriesMock;
   }
}
