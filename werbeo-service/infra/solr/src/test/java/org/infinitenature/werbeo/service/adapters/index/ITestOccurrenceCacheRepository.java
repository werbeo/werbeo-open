package org.infinitenature.werbeo.service.adapters.index;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ITestOccurrenceCacheRepository
{
   @Autowired
   private OccurrenceCacheRepository repoUT;

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   @DisplayName("Find ids by project id")
   void test001()
   {
      List<CacheOccurrence> ids = repoUT.findIdsByProjectId(68); // DE-MV-Freie
                                                                 // Daten
      assertThat(ids, hasSize(38));
   }

   @Test
   void test()
   {
      List<CacheOccurrence> ids = repoUT.findAllIds();
      assertThat(ids, is(not(empty())));
   }

}
