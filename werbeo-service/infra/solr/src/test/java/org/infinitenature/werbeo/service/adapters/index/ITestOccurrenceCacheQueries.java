package org.infinitenature.werbeo.service.adapters.index;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.FacetQuery;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ITestOccurrenceCacheQueries
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ITestOccurrenceCacheQueries.class);

   @Autowired
   private SolrTemplate solrTemplate;

   @Autowired
   private SolrOccurrenceQueryBuilder queryBuilder;

   @Test
   @DisplayName("Get occurrences created per day")
   void test001()
   {
      ZoneId timeZone = ZoneId.of("Europe/Berlin");
      LocalDateTime now = LocalDateTime.of(2020, 4, 16, 10, 0);
      FacetQuery query = queryBuilder.createQueryOccurrencesPerDay(
            createContext(PortalTestUtil.FLORA_MV_ID), 3, timeZone, now);


      FacetPage<CacheOccurrence> page = solrTemplate.queryForFacetPage(
            CacheOccurrence.COLLECTION, query, CacheOccurrence.class);
      List<FacetFieldEntry> content = page
            .getRangeFacetResultPage("creation_date_dt").getContent();
      content.forEach(e ->
      {
         LOGGER.debug("{} - {}", e.getValue(), e.getValueCount());
      });
      assertThat(content, hasSize(3));
      assertThat(content.get(0).getValue(), is("2020-04-13T22:00:00Z"));
      assertThat(content.get(0).getValueCount(), is(0l));
      assertThat(content.get(1).getValue(), is("2020-04-14T22:00:00Z"));
      assertThat(content.get(1).getValueCount(), is(38l));
      assertThat(content.get(2).getValue(), is("2020-04-15T22:00:00Z"));
      assertThat(content.get(2).getValueCount(), is(0l));
   }

   @Test
   @DisplayName("Get occurrences created per week")
   void test002()
   {
      ZoneId timeZone = ZoneId.of("Europe/Berlin");
      LocalDateTime now = LocalDateTime.of(2020, 4, 16, 10, 0);

      FacetQuery query = queryBuilder.createQueryOccurrencesPerWeek(
            createContext(PortalTestUtil.FLORA_MV_ID), 2, timeZone, now);

      FacetPage<CacheOccurrence> page = solrTemplate.queryForFacetPage(
            CacheOccurrence.COLLECTION, query, CacheOccurrence.class);
      List<FacetFieldEntry> content = page
            .getRangeFacetResultPage("creation_date_dt").getContent();

      content.forEach(e ->
      {
         LOGGER.debug("{} - {}", e.getValue(), e.getValueCount());
      });
      assertThat(content, hasSize(2));
      assertThat(content.get(0).getValue(), is("2020-04-05T22:00:00Z"));
      assertThat(content.get(0).getValueCount(), is(0l));
      assertThat(content.get(1).getValue(), is("2020-04-12T22:00:00Z"));
      assertThat(content.get(1).getValueCount(), is(38l));
   }

   @Test
   @DisplayName("Get occurrences created per month")
   void test003()
   {
      // we assume here that utc is the requested zone, because else moth ar not
      // calculated correct
      ZoneId timeZone = ZoneId.of("UTC");
      LocalDateTime now = LocalDateTime.of(2020, 4, 16, 10, 0);

      FacetQuery query = queryBuilder
            .createQueryOccurrencesCreatedPerMonth(
            createContext(PortalTestUtil.FLORA_MV_ID), 2, timeZone, now);

      FacetPage<CacheOccurrence> page = solrTemplate.queryForFacetPage(
            CacheOccurrence.COLLECTION, query, CacheOccurrence.class);
      List<FacetFieldEntry> content = page
            .getRangeFacetResultPage("creation_date_dt").getContent();

      content.forEach(e ->
      {
         LOGGER.debug("{} - {}", ZonedDateTime.parse(e.getValue()).toInstant()
               .atZone(timeZone),
               e.getValueCount());
      });
      assertThat(content, hasSize(2));
      assertThat(content.get(0).getValue(), is("2020-03-01T00:00:00Z"));
      assertThat(content.get(0).getValueCount(), is(0l));
      assertThat(content.get(1).getValue(), is("2020-04-01T00:00:00Z"));
      assertThat(content.get(1).getValueCount(), is(38l));
   }
   private Context createContext(int fId)
   {
      return new Context(new Portal(fId), null, Collections.emptySet(),
            new PortalConfiguration());
   }
}