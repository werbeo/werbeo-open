package org.infinitenature.werbeo.service.adapters.index;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.core.test.support.OccurrenceTestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ITestOccurrenceIndexComandsAdapter
{

   @Autowired
   private OccurrenceIndexCommandsAdapter adapterUT;
   @Autowired
   private OccurrenceIndexQueryAdapter queries;
   @Autowired
   private SolrTemplate solrTemplate;
   @BeforeEach
   void setUp() throws Exception
   {
      // adapterUT.setUpdateCommitTime(Duration.ZERO);
   }

   @Test
   @DisplayName("Test update survey")
   void test001()
   {
      // GIVEN
      Sample sample = OccurrenceTestUtil.validSample();
      sample.setId(UUID.randomUUID());
      sample.getSurvey().getPortal().setId(123456);
      sample.getSurvey().getTags().add("TAG_1");
      sample.getSurvey().getTags().add("TAG_2");
      sample.getSurvey().getTags().add("TAG_3");
      List<Occurrence> occurrences = sample
            .getOccurrences();
      occurrences.forEach(occ -> occ.setId(UUID.randomUUID()));
      occurrences.forEach(occ -> occ.setSample(sample));
      adapterUT.index(occurrences, true);

      // THEN

      Survey toUpdate = OccurrenceTestUtil.validSurvey();
      toUpdate.setAvailability(Availability.FREE);
      toUpdate.setName("A new name");
      toUpdate.setWerbeoOriginal(false);
      toUpdate.setAllowDataEntry(false);
      toUpdate.getTags().add("TAG_1");
      toUpdate.getTags().add("TAG_4");

      adapterUT.update(toUpdate);

      solrTemplate.commit(CacheOccurrence.COLLECTION); // enforce a commit

      // THAT

      List<Occurrence> found = queries.findBySampleId(sample.getId(),
            new Context(OccurrenceTestUtil.validPortal(), null,
                  Collections.emptySet(), "test"));

      assertThat(found, hasSize(1));

      Survey foundSurvey = found.get(0).getSample().getSurvey();

      assertAll(
            () -> assertThat("availability not updated",
                  foundSurvey.getAvailability(),
                  is(Availability.FREE)),
            () -> assertThat("name not updated", foundSurvey.getName(),
                  is("A new name")),
            () -> assertThat("werbeoOriginal not updated",
                  foundSurvey.isWerbeoOriginal(), is(false)),
            () -> assertThat("allowDataEntry not updated",
                  foundSurvey.isAllowDataEntry(), is(false)),
            () -> assertThat("tags not updated", foundSurvey.getTags(),
                  containsInAnyOrder("TAG_1", "TAG_4")));

      assertThat(found.get(0).getSample().getId(), is(sample.getId()));
   }

   @Test
   @DisplayName("Test delete sample")
   void test002()
   {
      // GIVEN
      Sample sample = OccurrenceTestUtil.validSample();
      sample.setId(UUID.randomUUID());
      sample.getSurvey().getPortal().setId(123456);
      List<Occurrence> occurrences = sample.getOccurrences();
      occurrences.forEach(occ -> occ.setId(UUID.randomUUID()));
      occurrences.forEach(occ -> occ.setSample(sample));
      adapterUT.index(occurrences, true);

      // THEN

      adapterUT.deleteBySampleId(sample.getId());
      solrTemplate.commit(CacheOccurrence.COLLECTION); // enforce a commit

      // THAT

      List<Occurrence> found = queries.findBySampleId(sample.getId(),
            new Context(OccurrenceTestUtil.validPortal(), null,
                  Collections.emptySet(), "test"));

      assertThat(found, hasSize(0));

   }
}
