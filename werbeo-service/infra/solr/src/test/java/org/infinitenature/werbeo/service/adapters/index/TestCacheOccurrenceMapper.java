package org.infinitenature.werbeo.service.adapters.index;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.time.LocalDate;
import java.time.Month;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestCacheOccurrenceMapper
{
   private CacheOccurrenceMapper mapperUT;

   @BeforeEach
   void setUp() throws Exception
   {
      mapperUT = new CacheOccurrenceMapperImpl();
   }

   @Test
   void testParseDate()
   {
      LocalDate date = mapperUT.parseDate("Tue Dec 31 01:00:00 CET 1499");

      assertThat(date.getYear(), is(1499));
      assertThat(date.getMonth(), is(Month.DECEMBER));
      assertThat(date.getDayOfMonth(), is(31));
   }

}
