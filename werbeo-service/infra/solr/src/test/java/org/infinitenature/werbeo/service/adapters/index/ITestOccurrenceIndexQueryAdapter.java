package org.infinitenature.werbeo.service.adapters.index;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.LifeStage;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Makropter;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter.WKTMode;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ITestOccurrenceIndexQueryAdapter
{
   @Autowired
   private OccurrenceIndexQueryAdapter adapterUT;
   @Autowired
   private CoordinateTransformerFactory coordinateTransformerFactory;

   @Test
   @DisplayName("filter by start date and taxa")
   void test009()
   {
      User user = null;
      Slice<Occurrence, OccurrenceSortField> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(LocalDate.of(2018, 3, 29), null, 25244,
                        null, null, false, null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.ID));

      Occurrence occurrence = occurrences.getContent().get(0);

      assertAll(() -> assertThat(occurrences.getNumberOfElements(), is(1)),
            () -> assertThat(occurrence.getId(),
                  is(UUID.fromString("41a53785-2092-42a3-95d3-b4dea6f099bc"))),
            () -> assertThat(occurrence.getTaxon().getName(),
                  is("Stigmidium fuscatae")));
   }

   @Test
   @DisplayName("Count flora-bb occurrences")
   void test001()
   {
      User user = null;
      long count = adapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_BB_ID, user));

      assertThat(count, is(0l));
   }

   @Test
   @DisplayName("Count flora-mv occurrences")
   void test002()
   {
      User user = null;
      long count = adapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(greaterThan(700l)));
   }

   @Test
   @DisplayName("WKT without any data")
   void test003()
   {
      User user = null;
      String wkt = "POINT (1 1)";
      long count = adapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, wkt, null, false, null,
                  null),
            createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(0l));
   }

   @Test
   @DisplayName("WKT with data")
   void test004()
   {
      User user = null;
      String wkt = "POLYGON((13.0 54.3,13.0 54.35,13.083333333333334 54.35,13.083333333333334 54.3,13.0 54.3))";
      long count = adapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, wkt, null, false, null,
                  null),
            createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(greaterThan(700l)));
   }

   @Test
   @DisplayName("10m max blur")
   void test005()
   {
      User user = null;
      long count = adapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, null, 10, false, null, null),
            createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("200m max blur")
   void test006()
   {
      User user = null;
      long count = adapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, null, 200, false, null,
                  null),
            createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(7426L));
   }

   @Test
   @DisplayName("before 1700")
   void test007()
   {
      User user = null;
      long count = adapterUT.countOccurrences(
            new OccurrenceFilter(null, LocalDate.of(1699, 12, 31), null, null,
                  null, false, null, null),
            createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("before today")
   void test008()
   {
      User user = null;
      long count = adapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, LocalDate.now(), null, null, null,
                        false, null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(7426L));
   }

   @Test
   @DisplayName("sort filtered by change date ascending")
   void test010()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, 49403, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.MOD_DATE))
            .getContent();

      assertAll(() -> assertThat(occurrences.size(), is(3)),
            () -> assertThat(occurrences.get(0).getId(),
                  is(UUID.fromString("867c6266-bff3-4a96-aeb5-cb1f10915c5c"))),
            () -> assertThat(occurrences.get(0).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(1).getId(),
                  is(UUID.fromString("b55251f4-fabc-4132-9094-358443f6de60"))),
            () -> assertThat(occurrences.get(1).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(2).getId(),
                  is(UUID.fromString("3ae96c47-5eda-4ae9-aac9-5ce902f1d831"))),
            () -> assertThat(occurrences.get(0).getTaxon().getName(),
                  is("Abies alba")));
   }

   @Test
   @DisplayName("sort by change date descending")
   void test011()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, 49403, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.DESC,
                        OccurrenceSortField.MOD_DATE))
            .getContent();

      assertAll(() -> assertThat(occurrences.size(), is(3)),
            () -> assertThat(occurrences.get(0).getId(),
                  is(UUID.fromString("3ae96c47-5eda-4ae9-aac9-5ce902f1d831"))),
            () -> assertThat(occurrences.get(0).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(1).getId(),
                  is(UUID.fromString("b55251f4-fabc-4132-9094-358443f6de60"))),
            () -> assertThat(occurrences.get(1).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(2).getId(),
                  is(UUID.fromString("867c6266-bff3-4a96-aeb5-cb1f10915c5c"))),
            () -> assertThat(occurrences.get(2).getTaxon().getName(),
                  is("Abies alba")));
   }

   @Test
   @DisplayName("sort by date ascending")
   void test012()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, 49403, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.DATE))
            .getContent();

      assertAll(() -> assertThat(occurrences.size(), is(3)),
            () -> assertThat(occurrences.get(2).getId(),
                  is(UUID.fromString("3ae96c47-5eda-4ae9-aac9-5ce902f1d831"))),
            () -> assertThat(occurrences.get(2).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(1).getId(),
                  is(UUID.fromString("b55251f4-fabc-4132-9094-358443f6de60"))),
            () -> assertThat(occurrences.get(1).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(0).getId(),
                  is(UUID.fromString("867c6266-bff3-4a96-aeb5-cb1f10915c5c"))),
            () -> assertThat(occurrences.get(0).getTaxon().getName(),
                  is("Abies alba")));
   }

   @Test
   @DisplayName("sort by date descending")
   void test013()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, 49403, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.DESC,
                        OccurrenceSortField.DATE))
            .getContent();

      assertAll(() -> assertThat(occurrences.size(), is(3)),
            () -> assertThat(occurrences.get(0).getId(),
                  is(UUID.fromString("3ae96c47-5eda-4ae9-aac9-5ce902f1d831"))),
            () -> assertThat(occurrences.get(0).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(1).getId(),
                  is(UUID.fromString("b55251f4-fabc-4132-9094-358443f6de60"))),
            () -> assertThat(occurrences.get(1).getTaxon().getName(),
                  is("Abies alba")),
            () -> assertThat(occurrences.get(2).getId(),
                  is(UUID.fromString("867c6266-bff3-4a96-aeb5-cb1f10915c5c"))),
            () -> assertThat(occurrences.get(2).getTaxon().getName(),
                  is("Abies alba")));
   }

   @Test
   @DisplayName("sort by taxa asc")
   void test014()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
                        OccurrenceSortField.TAXON))
            .getContent();

      assertThat(occurrences.get(0).getTaxon().getName(), is("Abies"));
   }

   @Test
   @DisplayName("sort by taxa desc")
   void test015()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 1, SortOrder.DESC,
                        OccurrenceSortField.TAXON))
            .getContent();

      assertThat(occurrences.get(0).getTaxon().getName(), is("x Pseudorhiza"));
   }

   @Test
   @DisplayName("sort by determiner assc")
   void test016()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
                        OccurrenceSortField.DETERMINER))
            .getContent();

      assertThat(occurrences.get(0).getDeterminer().getLastName(),
            is("Abrahami"));
   }

   @Test
   @DisplayName("sort by determiner desc")
   void test017()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 1, SortOrder.DESC,
                        OccurrenceSortField.DETERMINER))
            .getContent();

      assertThat(occurrences.get(0).getDeterminer().getLastName(),
            is("Yeoman"));
   }

   @Test
   @DisplayName("count abies spec without child taxa")
   void test018()
   {
      User user = null;
      long count = adapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, null, 49401, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(1L));
   }

   @Test
   @DisplayName("count abies spec with child taxa")
   void test019()
   {
      User user = null;
      long count = adapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, null, 49401, null, null, true,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(6L));
   }

   @Test
   @DisplayName("count by owner")
   void test020()
   {
      User user = null;
      long count = adapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, null, null, true,
                  "cTomalaa@ucoz.com", null),
            createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(7426L));
   }

   @Test
   @DisplayName("sort by change date ascending")
   void test021()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.MOD_DATE))
            .getContent();

      assertThat(occurrences.size(), is(10));
      assertThat(occurrences.get(0).getSample().getId(),
            is(UUID.fromString("bdec3695-98ca-483f-9bb2-260891ffb1ec")));
   }

   @Test
   @DisplayName("count all with ownly own data filter and own data")
   void test022()
   {

      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, null);
      Map<Filter, Collection<Portal>> portalBasedFilters = new HashMap<>();
      portalBasedFilters.put(Filter.ONLY_OWN_DATA, new HashSet<>());
      portalBasedFilters.get(Filter.ONLY_OWN_DATA)
            .add(new Portal(PortalTestUtil.FLORA_MV_ID));
      filter.setPortalBasedFilters(portalBasedFilters);
      long occurrences = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(occurrences, is(7426l));
   }

   @Test
   @DisplayName("count all with ownly own data filter but with no own data")
   void test023()
   {

      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, null);
      Map<Filter, Collection<Portal>> portalBasedFilters = new HashMap<>();
      portalBasedFilters.put(Filter.ONLY_OWN_DATA, new HashSet<>());
      portalBasedFilters.get(Filter.ONLY_OWN_DATA)
            .add(new Portal(PortalTestUtil.FLORA_MV_ID));
      filter.setPortalBasedFilters(portalBasedFilters);
      long occurrences = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.REST_SRV_ID, getUser(UserTestUtil.USER_LOGIN_B)));

      assertThat(occurrences, is(38l));
   }

   @Test
   @DisplayName("Filter by not existing survey")
   void test024()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, 9999);
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("Filter by survey")
   void test025()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, 16);
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(7388L));
   }

   @Test
   @DisplayName("filter by mtb - no data")
   void test026()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setMtb(MTBHelper.toMTB("2222"));
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("filter by mtb - data")
   void test027()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setMtb(MTBHelper.toMTB("1643"));
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(804L));
   }

   @Test
   @DisplayName("filter by mtbq - data")
   void test028()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setMtb(MTBHelper.toMTB("1643/2"));
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(124L));
   }

   @Test
   @DisplayName("filter by wkt - geom of mtb 1943")
   void test029()
   {
      String mtb = "1943";
      String grimmen4326 = mtbToWKT4326(mtb);

      User user = null;
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null,
            grimmen4326, null, false, null, null);
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(filter,
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 1000, SortOrder.ASC,
                        OccurrenceSortField.ID))
            .getContent();
      System.out.println(occurrences.size());
      assertThat(occurrences.size(), is(greaterThan(0)));
      for (Occurrence occurrence : occurrences)
      {
         assertThat(occurrence.getSample().getLocality().getPosition().getMtb()
               .getMtb(), startsWith(mtb));
      }
   }

   @Test
   @DisplayName("find mtbs by taxon")
   void test030()
   {
      Set<MTB> coveredMTB = adapterUT.getCoveredMTB(37319, true,
            createContext(PortalTestUtil.FLORA_MV_ID, null));

      assertThat(coveredMTB, containsInAnyOrder(MTBHelper.toMTB("1944/4"),
            MTBHelper.toMTB("1643/3"), MTBHelper.toMTB("2043/3")));
   }

   @Test
   @DisplayName("reduce to unique mtbqs")
   void test031()
   {
      Set<MTB> coveredMTB = adapterUT.reduceToUniqueMTBQs(
            Arrays.asList("1994/4", "1943/123", "1944", "1994/43"));

      assertThat(coveredMTB, containsInAnyOrder(MTBHelper.toMTB("1994/4"),
            MTBHelper.toMTB("1943/1"), MTBHelper.toMTB("1944")));
   }

   @Test
   @DisplayName("filter for herbary")
   void test032()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setHerbaryCode("GFW");
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(1L));
   }

   @Test
   @DisplayName("Count flora-mv occurrences, group by sample")
   void test033()
   {
      User user = null;
      long count = adapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, null, null, false, null,
                  null).withGroupBySample(true),
            createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(count, is(1030l));
   }

   @Test
   @DisplayName("sort by time ascending")
   void test034()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.HEUSCHRECKEN_D_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.TIME_OF_DAY))
            .getContent();

      for (Occurrence o : occurrences)
      {
         System.out.println(
               "ASC " + o.getTaxon().getName() + " " + o.getTimeOfDay());
      }

      assertThat(occurrences.get(0).getTaxon().getName(),
            is("Bicolorana bicolor"));
      assertThat(occurrences.get(0).getTimeOfDay(), is("06:25"));
      assertThat(occurrences.get(1).getTaxon().getName(),
            is("Oedaleus decorus"));
      assertThat(occurrences.get(1).getTimeOfDay(), is("16:11"));
      assertNull(occurrences.get(2).getTimeOfDay());
   }

   @Test
   @DisplayName("Find by non existing validator")
   void test035()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setValidator("abc");
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("Find by existing validator")
   void test036()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setValidator(UserTestUtil.USER_LOGIN_A);
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(2L));
   }

   @Test
   @DisplayName("sort by time decending")
   void test037()
   {
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.HEUSCHRECKEN_D_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.DESC,
                        OccurrenceSortField.TIME_OF_DAY))
            .getContent();

      for (Occurrence o : occurrences)
      {
         System.out.println(
               "DESC " + o.getTaxon().getName() + " " + o.getTimeOfDay());
      }

      assertThat(occurrences.get(0).getTaxon().getName(),
            is("Oedaleus decorus"));
      assertThat(occurrences.get(0).getTimeOfDay(), is("16:11"));
      assertThat(occurrences.get(1).getTaxon().getName(),
            is("Bicolorana bicolor"));
      assertThat(occurrences.get(1).getTimeOfDay(), is("06:25"));
      assertNull(occurrences.get(2).getTimeOfDay());
   }

   @Test
   @DisplayName("Count by validation status")
   void test038()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setValidationStatus(ValidationStatus.VALID);
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(1L));
   }

   @Test
   @DisplayName("check on Bicolorana bicolor: LifeStage, Makropter")
   void test039()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setTaxonId(19113);
      User user = null;
      List<Occurrence> occurrences = adapterUT
            .findOccurrences(filter,
                  createContext(PortalTestUtil.HEUSCHRECKEN_D_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.MOD_DATE)) // sorting: default
                                                       // values
            .getContent();

      assertThat(occurrences.size(), is(1));
      assertThat(occurrences.get(0).getTaxon().getName(),
            is("Bicolorana bicolor"));
      assertThat(occurrences.get(0).getLifeStage(), is(LifeStage.LARVE_IMAGO));
      assertThat(occurrences.get(0).getMakropter(), is(Makropter.YES));
   }

   @Test
   @DisplayName("count - filter by point")
   public void test040()
   {

      String glewitz4745 = MTBHelper.toMTB("1943/1").getCenterWkt();

      CoordinateTransformer ct = coordinateTransformerFactory
            .getCoordinateTransformer(4745, 4326);

      String glewitz4326 = ct.convert(glewitz4745);

      User user = null;
      long result = adapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, null, null, glewitz4326, null,
                        false, null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(result, is(0l)); // POINTS are never "WITHIN"
   }

   @Test
   @DisplayName("filter by external key - match")
   void test041()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setOccExternalKey("Flora-MV-Global-ID: 1234");
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(1L));
   }

   @Test
   @DisplayName("Count by external key - case sesitive no match")
   void test042()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setOccExternalKey("GLOBAL");
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("Count by external key - case sesitive match")
   void test043()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setOccExternalKey("Global");
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(2L));
   }

   @Test
   @DisplayName("Count by modified since - no data because date in the future")
   void test044()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setModifiedSince(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("Count by modified since - all data because date long in the past")
   void test045()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setModifiedSince(LocalDateTime.of(2000, 1, 1, 10, 0));
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(7426L));
   }

   @Test
   @DisplayName("Count by wkt intersect")
   void test046()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setWkt(GeomTestUtil.WKT_INERSECTS_1944_1_AND_2);
      filter.setWktMode(WKTMode.INTERSECTS);
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(287l));
   }

   @Test
   @DisplayName("Count by wkt within")
   void test047()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setWkt(GeomTestUtil.WKT_INERSECTS_1944_1_AND_2);
      filter.setWktMode(WKTMode.WITHIN);
      long count = adapterUT.countOccurrences(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   private User getUser(String login)
   {
      switch (login)
      {
      case UserTestUtil.USER_LOGIN_A:
         Person person = new Person("Cthrine", "Tomala");
         User user = new User();
         user.setLogin(UserTestUtil.USER_LOGIN_A);
         user.setPerson(person);
         return user;
      case UserTestUtil.USER_LOGIN_B:
         Person personB = new Person("Sarge", "Ellins");
         User userB = new User();
         userB.setLogin(UserTestUtil.USER_LOGIN_B);
         userB.setPerson(personB);
         return userB;
      default:
         throw new IllegalArgumentException("Unknown user login " + login);
      }

   }

   private Context createContext(int portalId, User user)
   {
      Portal portal = new Portal();
      portal.setId(portalId);
      return new Context(portal, user, Collections.emptySet(), "test");
   }

   private String mtbToWKT4326(String mtbString)
   {
      String wgt1943 = MTBHelper.toMTB(mtbString).toWkt();

      CoordinateTransformer ct = coordinateTransformerFactory
            .getCoordinateTransformer(4745, 4326);

      String munich4326 = ct.convert(wgt1943);
      return munich4326;
   }

}
