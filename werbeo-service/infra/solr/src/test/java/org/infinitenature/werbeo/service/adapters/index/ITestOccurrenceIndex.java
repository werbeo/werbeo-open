package org.infinitenature.werbeo.service.adapters.index;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infinitenature.werbeo.service.Application;
import org.infinitenature.werbeo.service.core.access.Operation;
import org.infinitenature.werbeo.service.core.api.enity.Herbarium;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.LifeStage;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Makropter;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.locationtech.jts.geom.Geometry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@Import(Application.class)
class ITestOccurrenceIndex
{
   public static final String DIVIDER = "|#|#|";
   @Autowired
   private OccurrenceIndexQueryAdapter occurrenceIndexQueryAdapter;

   @Autowired
   private OccurrenceIndexCommandsAdapter occurrenceIndexCommandsAdapter;

   private Occurrence occurrence;
   private UUID occurrenceId = UUID.randomUUID();
   private UUID sampleId = UUID.randomUUID();
   private GeometryHelper geometryHelper = new GeometryHelper();
   private StringBuilder builder = new StringBuilder();

   @BeforeEach
   public void setUp() throws Exception
   {
      Position position = new Position();
      position.setWkt(
            "MULTIPOLYGON (((40 40, 20 45, 45 30, 40 40)), ((20 35, 10 30, 10 10, 30 5, 45 20, 20 35), (30 20, 20 15, 20 25, 30 20)))");
      position.setWktEpsg(4326);
      position.setEpsg(4326);
      position.setType(Position.PositionType.SHAPE);
      position.setCode("code");
      position.setDescription("description");
      position.setIncomplete(true);
      position.setMtb(MTBHelper.nullMTB);
      position.setObfuscated(true);
      position.setPosCenter(geometryHelper
            .getCenter(position.getWkt(), position.getEpsg()).getCoordinate());
      Geometry geom = geometryHelper.parseToJts(position.getWkt(),
            position.getEpsg());
      position.setPosNW(geometryHelper.findPosNW(geom));
      position.setPosSE(geometryHelper.findPosSE(geom));
      position.setObfuscated(true);

      Locality locality = new Locality();
      locality.setPosition(position);
      locality.setLocationComment("comment");
      locality.setLocality("locality");
      locality.setPrecision(1);

      Set<Operation> allowedOperations = new HashSet<>();
      allowedOperations.add(Operation.CREATE);
      allowedOperations.add(Operation.DELETE);
      allowedOperations.add(Operation.UPDATE);
      allowedOperations.add(Operation.READ);

      Person personAdmin = new Person();
      personAdmin.setFirstName("core");
      personAdmin.setLastName("admin");
      personAdmin.setEmail("admin@example.com");
      personAdmin.setObfuscated(false);
      personAdmin.setId(1);
      personAdmin.setObfuscated(true);

      User userAdmin = new User();
      userAdmin.setPerson(personAdmin);
      userAdmin.setId(1);
      userAdmin.setObfuscated(true);
      userAdmin.setLogin(personAdmin.getEmail());

      Person person = new Person(); // fiktiv
      person.setFirstName("Beeke");
      person.setLastName("Heidefrau");
      person.setEmail("beeke@heidefrau.de");
      person.setExternalKey("externalKey");
      person.setObfuscated(true);
      person.setId(44);
      person.setAllowedOperations(allowedOperations);
      person.setCreatedBy(userAdmin);
      person.setModifiedBy(userAdmin);
      person.setCreationDate(
            LocalDateTime.of(2018, Month.FEBRUARY, 4, 4, 4, 4));
      person.setModificationDate(
            LocalDateTime.of(2018, Month.FEBRUARY, 4, 4, 4, 4));

      Person personClemMeadows = new Person();
      personClemMeadows.setFirstName("Clen");
      personClemMeadows.setLastName("Meadows");
      personClemMeadows.setEmail("cmeadows0@prweb.com");
      personClemMeadows.setCreationDate(
            LocalDateTime.of(2018, Month.MARCH, 2, 12, 47, 45));
      personClemMeadows.setModificationDate(
            LocalDateTime.of(2018, Month.MARCH, 2, 12, 47, 45));
      person.setObfuscated(true);
      personClemMeadows.setId(3);

      User user = new User();
      user.setPerson(person);
      user.setId(12);
      user.setLogin(person.getEmail());
      user.setObfuscated(true);

      User userAutoImport = new User();
      userAutoImport.setId(3);
      userAutoImport.setObfuscated(true);

      Portal portal = new Portal();
      portal.setTitle("title");
      portal.setUrl("url");
      portal.setId(99);
      portal.setCreatedBy(userAdmin);
      portal.setCreationDate(
            LocalDateTime.of(2010, Month.APRIL, 1, 11, 11, 11));
      portal.setModificationDate(
            LocalDateTime.of(2011, Month.APRIL, 1, 11, 11, 11));
      portal.setModifiedBy(userAdmin);
      portal.setUrl("url");
      portal.setAllowedOperations(allowedOperations);
      portal.setObfuscated(true);

      Survey survey = new Survey();
      survey.setAvailability(Survey.Availability.FREE);
      survey.setContainer(true);
      survey.setDescription("description");
      survey.setName("name");
      survey.setOwner(person);
      survey.setPortal(portal);
      survey.setParentId(1);
      survey.setId(2);
      survey.setCreatedBy(userAutoImport);
      survey.setCreationDate(LocalDateTime.of(2019, Month.JANUARY, 1, 9, 9, 9));
      survey.setModifiedBy(userAutoImport);
      survey.setModificationDate(
            LocalDateTime.of(2019, Month.JANUARY, 1, 10, 10, 10));
      survey.setAllowedOperations(allowedOperations);
      survey.setObfuscated(true);

      SampleBase sampleBase = new SampleBase();
      sampleBase.setLocality(locality);
      sampleBase.setDate(new VagueDate(LocalDate.of(2019, Month.JULY, 7),
            LocalDate.of(2019, Month.JULY, 31), VagueDate.Type.DAYS));
      sampleBase.setRecorder(person);
      sampleBase.setSampleMethod(SampleBase.SampleMethod.SUCTION_SAMPLER);
      sampleBase.setSurvey(survey);
      sampleBase.setId(sampleId);
      sampleBase
            .setCreationDate(LocalDateTime.of(2019, Month.JULY, 7, 7, 7, 7));
      sampleBase.setCreatedBy(user);
      sampleBase
            .setModificationDate(LocalDateTime.of(2019, Month.JULY, 8, 1, 1));
      sampleBase.setModifiedBy(user);
      sampleBase.setAllowedOperations(allowedOperations);
      sampleBase.setObfuscated(true);

      Herbarium herbarium = new Herbarium();
      herbarium.setCode("code");
      herbarium.setHerbary("herbary");

      TaxonBase taxonBase = new TaxonBase();
      taxonBase.setAuthority("Roth");
      taxonBase.setExternalKey("829");
      taxonBase.setName("Betula pendula");
      taxonBase.setGroup("Spermatophyta");
      taxonBase.setId(35198);
      taxonBase.setCreationDate(
            LocalDateTime.of(2018, Month.FEBRUARY, 21, 17, 33, 9));
      taxonBase.setCreatedBy(userAutoImport);
      taxonBase.setObfuscated(true);
      taxonBase.setObfuscated(true);

      List<Medium> media = new ArrayList<>();
      media.add(new Medium("path0", "description0", MediaType.AUDIO_LINK));
      media.add(new Medium("path1", "description1", MediaType.AUDIO_LOCAL));
      media.add(new Medium("path2", "description2", MediaType.IMAGE_LINK));
      media.add(new Medium("path3", "description3", MediaType.IMAGE_LOCAL));

      occurrence = new Occurrence();
      occurrence.setId(occurrenceId);
      occurrence
            .setCreationDate(LocalDateTime.of(2019, Month.JULY, 7, 7, 7, 7));
      occurrence.setCreatedBy(user);
      occurrence
            .setModificationDate(LocalDateTime.of(2019, Month.JULY, 8, 1, 1));
      occurrence.setModifiedBy(user);
      occurrence.setDeterminationComment("determinationComment");
      occurrence.setAmount(Occurrence.Amount.FEW);
      occurrence.setBloomingSprouts(Occurrence.BloomingSprouts.ONE_TO_FIVE);
      occurrence.setCoveredArea(Occurrence.Area.FIFTY);
      occurrence.setDeterminer(person);
      occurrence.setExternalKey("externalKey");
      occurrence.setHabitat("habitat");
      occurrence.setHerbarium(herbarium);
      occurrence.setObservers("observers");
      occurrence.setTaxon(taxonBase);
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setAmount(Occurrence.Amount.A_LOT);
      occurrence.setSettlementStatus(Occurrence.SettlementStatus.PLANTED);
      occurrence.setSettlementStatusFukarek(
            Occurrence.SettlementStatusFukarek.SYNANTHROP);
      occurrence.setVitality(Occurrence.Vitality.VITAL);
      occurrence.setCoveredArea(Occurrence.Area.HUNDRED);
      occurrence.setBloomingSprouts(Occurrence.BloomingSprouts.FIFTY);
      occurrence.setQuantity(Occurrence.Quantity.THOUSAND);
      occurrence.setDeterminationComment("determinationComment");
      occurrence.setNumericAmount(1000);
      occurrence.setSex(Occurrence.Sex.BOTH);
      occurrence.setReproduction(Occurrence.Reproduction.SURE);
      occurrence.setMedia(media);
      occurrence.setSample(sampleBase);
      occurrence.setAllowedOperations(allowedOperations);
      occurrence.setObfuscated(true);
      occurrence.setTimeOfDay("16:00");
      Person validator = new Person(1, "xxx", "xxx-xxx");
      validator.setEmail("xxx@xxx.net");
      occurrence.setValidation(new Validation(ValidationStatus.INVALID,
            "xxx@xxx.net", LocalDateTime.of(2020, 8, 27, 15, 27),
            validator));
      occurrence.setLifeStage(LifeStage.IMAGO);
      occurrence.setMakropter(Makropter.NO);
   }

   @Test
   void testSaveAndRetrive()
   {
      occurrenceIndexCommandsAdapter.index(Arrays.asList(occurrence), true);
      Occurrence result = occurrenceIndexQueryAdapter.findById(occurrenceId,
            createContext(PortalTestUtil.FLORA_BB_ID, null));

      assertAndReport(occurrence, result);
   }

   private void assertAndReport(Occurrence occurrence, Occurrence result)
   {
      assertAll( //
            () -> assertEquals(occurrence.getId(), result.getId()),
            () -> assertEquals(occurrence.getCreationDate(),
                  result.getCreationDate()),
            () -> assertEquals(occurrence.getModificationDate(),
                  result.getModificationDate()),
            () -> assertEquals(occurrence.getRecordStatus(),
                  result.getRecordStatus()),
            () -> assertEquals(occurrence.getAmount(), result.getAmount()),
            () -> assertEquals(occurrence.getSettlementStatus(),
                  result.getSettlementStatus()),
            () -> assertEquals(occurrence.getCoveredArea(),
                  result.getCoveredArea()),
            () -> assertEquals(occurrence.getVitality(), result.getVitality()),
            () -> assertEquals(occurrence.getSettlementStatusFukarek(),
                  result.getSettlementStatusFukarek()),
            () -> assertEquals(occurrence.getBloomingSprouts(),
                  result.getBloomingSprouts()),
            () -> assertEquals(occurrence.getQuantity(), result.getQuantity()),
            () -> assertEquals(occurrence.getExternalKey(),
                  result.getExternalKey()),
            () -> assertEquals(occurrence.getObservers(),
                  result.getObservers()),
            () -> assertEquals(occurrence.getDeterminationComment(),
                  result.getDeterminationComment()),
            () -> assertEquals(occurrence.getNumericAmount(),
                  result.getNumericAmount()),
            () -> assertEquals(occurrence.getSex(), result.getSex()),
            () -> assertEquals(occurrence.getReproduction(),
                  result.getReproduction()),
            () -> assertEquals(occurrence.getHabitat(), result.getHabitat()),
            () -> assertEquals(occurrence.getMedia(), result.getMedia()),
            () -> assertEquals(occurrence.getTimeOfDay(), result.getTimeOfDay()),
            () -> assertEquals(occurrence.getValidation(),
                  result.getValidation()),
            () -> assertEquals(occurrence.getLifeStage(),
                  result.getLifeStage()),
            () -> assertEquals(occurrence.getMakropter(), result.getMakropter()


      ));



      assertAndReport("from Occurrence created by", occurrence.getCreatedBy(),
            result.getCreatedBy());
      builder.append("\n");
      assertAndReport("from Occurrence modified by", occurrence.getModifiedBy(),
            result.getModifiedBy());
      builder.append("\n");
      assertAndReport("from Occurrence determiner", occurrence.getDeterminer(),
            result.getDeterminer());
      builder.append("\n");
      assertAndReport(occurrence.getHerbarium(), result.getHerbarium());
      builder.append("\n");
      assertAndReport(occurrence.getMedia(), result.getMedia());
      builder.append("\n");
      assertAndReport(occurrence.getSample(), result.getSample());
      builder.append("\n");
      assertAndReport(occurrence.getTaxon(), result.getTaxon());
      builder.append("\n");
   }

   private void assertAndReport(SampleBase sampleBase, SampleBase result)
   {
      builder.append("SampleBase\n");
      try
      {
         assertNotEquals(null, sampleBase);
      } catch (AssertionError e)
      {
         builder.append("sampleBase is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("id=");
      builder.append(sampleBase.getId());
      builder.append("\t");
      builder.append(result.getId());
      builder.append("\n");
      try
      {
         assertNotEquals("", sampleBase.getId());
         assertNotEquals("", result.getId());
         assertEquals(sampleBase.getId(), result.getId());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("creationDate=");
      builder.append(sampleBase.getCreationDate());
      builder.append("\t");
      builder.append(result.getCreationDate());
      builder.append("\n");
      try
      {
         assertNotEquals("", sampleBase.getCreationDate());
         assertNotEquals("", result.getCreationDate());
         assertEquals(sampleBase.getCreationDate(), result.getCreationDate());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("modificationDate=");
      builder.append(sampleBase.getModificationDate());
      builder.append("\t");
      builder.append(result.getModificationDate());
      builder.append("\n");
      try
      {
         assertNotEquals("", sampleBase.getModificationDate());
         assertNotEquals("", result.getModificationDate());
         assertEquals(sampleBase.getModificationDate(),
               result.getModificationDate());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT IN SOLR allowedOperations=");
      builder.append(sampleBase.getAllowedOperations());
      builder.append("\t");
      builder.append(result.getAllowedOperations());
      builder.append("\n");
      builder.append("NOT IN SOLR obfusicated=");
      builder.append(sampleBase.isObfuscated());
      builder.append("\t");
      builder.append(result.isObfuscated());
      builder.append("\n");
      builder.append("date=");
      builder.append(sampleBase.getDate());
      builder.append("\t");
      builder.append(result.getDate());
      builder.append("\n");
      try
      {
         assertNotEquals("", sampleBase.getDate());
         assertNotEquals("", result.getDate());
         assertEquals(sampleBase.getDate(), result.getDate());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("sampleMethod=");
      builder.append(sampleBase.getSampleMethod());
      builder.append("\t");
      builder.append(result.getSampleMethod());
      builder.append("\n");
      try
      {
         assertNotEquals("", sampleBase.getSampleMethod());
         assertNotEquals("", result.getSampleMethod());
         assertEquals(sampleBase.getSampleMethod(), result.getSampleMethod());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      assertAndReport(sampleBase.getSurvey(), result.getSurvey());
      builder.append("\n");
      assertAndReport(sampleBase.getLocality(), result.getLocality());
      builder.append("\n");

      assertAndReport("from SampleBase created by", sampleBase.getCreatedBy(),
            result.getCreatedBy());
      builder.append("\n");
      assertAndReport("from SampleBase modified by", sampleBase.getModifiedBy(),
            result.getModifiedBy());
      builder.append("\n");

      assertAndReport("from SampleBase recorder", sampleBase.getRecorder(),
            result.getRecorder());
   }

   private void assertAndReport(Locality locality, Locality result)
   {
      builder.append("Locality\n");
      try
      {
         assertNotEquals(null, locality);
      } catch (AssertionError e)
      {
         builder.append("localtity is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("precision=");
      builder.append(locality.getPrecision());
      builder.append("\t");
      builder.append(result.getPrecision());
      builder.append("\n");
      try
      {
         assertNotEquals("", locality.getPrecision());
         assertNotEquals("", result.getPrecision());
         assertEquals(locality.getPrecision(), result.getPrecision());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("locationComment=");
      builder.append(locality.getLocationComment());
      builder.append("\t");
      builder.append(result.getLocationComment());
      builder.append("\n");
      try
      {
         assertNotEquals("", locality.getLocationComment());
         assertNotEquals("", result.getLocationComment());
         assertEquals(locality.getLocationComment(),
               result.getLocationComment());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("locality=");
      builder.append(locality.getLocality());
      builder.append("\t");
      builder.append(result.getLocality());
      builder.append("\n");
      try
      {
         assertNotEquals("", locality.getLocality());
         assertNotEquals("", result.getLocality());
         assertEquals(locality.getLocality(), result.getLocality());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      assertAndReport(locality.getPosition(), result.getPosition());
      builder.append("\n");
   }

   private void assertAndReport(Position position, Position result)
   {
      builder.append("\n");
      builder.append("Position\n");
      try
      {
         assertNotEquals(null, position);
      } catch (AssertionError e)
      {
         builder.append("position is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("type=");
      builder.append(position.getType());
      builder.append("\t");
      builder.append(result.getType());
      builder.append("\n");
      try
      {
         assertNotEquals("", position.getType());
         assertNotEquals("", result.getType());
         assertEquals(position.getType(), result.getType());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("epsg=");
      builder.append(position.getEpsg());
      builder.append("\t");
      builder.append(result.getEpsg());
      builder.append("\n");
      try
      {
         assertNotEquals("", position.getEpsg());
         assertNotEquals("", result.getEpsg());
         assertEquals(position.getEpsg(), result.getEpsg());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("mtb=");
      builder.append(position.getMtb());
      builder.append("\t");
      builder.append(result.getMtb());
      builder.append("\n");
      try
      {
         assertNotEquals("", position.getMtb());
         assertNotEquals("", result.getMtb());
         assertEquals(position.getMtb(), result.getMtb());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT CREATED posNW=");
      builder.append(Arrays.toString(position.getPosNW()));
      builder.append("\t");
      builder.append(Arrays.toString(result.getPosNW()));
      builder.append("\n");
      builder.append("NOT CREATED posSE=");
      builder.append(Arrays.toString(position.getPosSE()));
      builder.append("\t");
      builder.append(Arrays.toString(result.getPosSE()));
      builder.append("\n");
      builder.append("posCenter=");
      builder.append(Arrays.toString(position.getPosCenter()));
      builder.append("\t");
      builder.append(Arrays.toString(result.getPosCenter()));
      builder.append("\n");
      try
      {
         assertNotEquals(null, position.getPosCenter());
         assertNotEquals(null, result.getPosCenter());
         assertEquals(Arrays.toString(position.getPosCenter()),
               Arrays.toString(result.getPosCenter()));
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("wkt=");
      builder.append("\n");
      builder.append(position.getWkt());
      builder.append("\n");
      builder.append(result.getWkt());
      builder.append("\n");
      try
      {
         assertNotEquals("", position.getWkt());
         assertNotEquals("", result.getWkt());
         assertEquals(position.getWkt(), result.getWkt());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("wktEpsg=");
      builder.append(position.getWktEpsg());
      builder.append("\t");
      builder.append(result.getWktEpsg());
      builder.append("\n");
      try
      {
         assertNotEquals("", position.getWktEpsg());
         assertNotEquals("", result.getWktEpsg());
         assertEquals(position.getWktEpsg(), result.getWktEpsg());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT CREATED code=");
      builder.append(position.getCode());
      builder.append("\t");
      builder.append(result.getCode());
      builder.append("\n");
      builder.append("NOT CREATED incomplete=");
      builder.append(position.isIncomplete());
      builder.append("\t");
      builder.append(result.isIncomplete());
      builder.append("\n");
      builder.append("NOT CREATED description=");
      builder.append(position.getDescription());
      builder.append("\t");
      builder.append(result.getDescription());
      builder.append("\n");
      builder.append("NOT CREATED/NOT IN SOLR obfusicated=");
      builder.append(position.isObfuscated());
      builder.append("\t");
      builder.append(result.isObfuscated());
   }

   private void assertAndReport(TaxonBase taxonBase, TaxonBase result)
   {
      builder.append("TaxonBase\n");
      try
      {
         assertNotEquals(null, taxonBase);
      } catch (AssertionError e)
      {
         builder.append("taxonBase is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("id=");
      builder.append(taxonBase.getId());
      builder.append("\t");
      builder.append(result.getId());
      builder.append("\n");
      try
      {
         assertNotEquals("", taxonBase.getId());
         assertNotEquals("", result.getId());
         assertEquals(taxonBase.getId(), result.getId());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT IN SOLR creationDate=");
      builder.append(taxonBase.getCreationDate());
      builder.append("\t");
      builder.append(result.getCreationDate());
      builder.append("\n");
      builder.append("NOT IN SOLR modificationDate=");
      builder.append(taxonBase.getModificationDate());
      builder.append("\t");
      builder.append(result.getModificationDate());
      builder.append("\n");
      builder.append("NOT IN SOLR allowedOperations=");
      builder.append(taxonBase.getAllowedOperations());
      builder.append("\t");
      builder.append(result.getAllowedOperations());
      builder.append("\n");
      builder.append("NOT IN SOLR obfusicated=");
      builder.append(taxonBase.isObfuscated());
      builder.append("\t");
      builder.append(result.isObfuscated());
      builder.append("\n");
      builder.append("name=");
      builder.append(taxonBase.getName());
      builder.append("\t");
      builder.append(result.getName());
      builder.append("\n");
      try
      {
         assertNotEquals("", taxonBase.getName());
         assertNotEquals("", result.getName());
         assertEquals(taxonBase.getName(), result.getName());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("externalKey=");
      builder.append(taxonBase.getExternalKey());
      builder.append("\t");
      builder.append(result.getExternalKey());
      builder.append("\n");
      try
      {
         assertNotEquals("", taxonBase.getExternalKey());
         assertNotEquals("", result.getExternalKey());
         assertEquals(taxonBase.getExternalKey(), result.getExternalKey());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("group=");
      builder.append(taxonBase.getGroup());
      builder.append("\t");
      builder.append(result.getGroup());
      builder.append("\n");
      try
      {
         assertNotEquals("", taxonBase.getGroup());
         assertNotEquals("", result.getGroup());
         assertEquals(taxonBase.getGroup(), result.getGroup());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      assertEquals(taxonBase.getGroup(), result.getGroup());
      builder.append("authority=");
      builder.append(taxonBase.getAuthority());
      builder.append("\t");
      builder.append(result.getAuthority());
      builder.append("\n");
      try
      {
         assertNotEquals("", taxonBase.getAuthority());
         assertNotEquals("", result.getAuthority());
         assertEquals(taxonBase.getAuthority(), result.getAuthority());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("from TaxonBase NOT IN SOLR documentsAvailable");
      builder.append("from TaxonBase NOT IN SOLR created by");
      builder.append("\n");
      builder.append("from TaxonBase NOT IN SOLR modified by");
      builder.append("\n");
   }

   private void assertAndReport(Survey survey, Survey result)
   {
      builder.append("Survey\n");
      try
      {
         assertNotEquals(null, survey);
      } catch (AssertionError e)
      {
         builder.append("survey is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("id=");
      builder.append(survey.getId());
      builder.append("\t");
      builder.append(result.getId());
      builder.append("\n");
      try
      {
         assertNotEquals("", survey.getId());
         assertNotEquals("", result.getId());
         assertEquals(survey.getId(), result.getId());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("creationDate=");
      builder.append(survey.getCreationDate());
      builder.append("\t");
      builder.append(result.getCreationDate());
      builder.append("\n");
      try
      {
         assertNotEquals("", survey.getCreationDate());
         assertNotEquals("", result.getCreationDate());
         assertEquals(survey.getCreationDate(), result.getCreationDate());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("modificationDate=");
      builder.append(survey.getModificationDate());
      builder.append("\t");
      builder.append(result.getModificationDate());
      builder.append("\n");
      try
      {
         assertNotEquals("", survey.getModificationDate());
         assertNotEquals("", result.getModificationDate());
         assertEquals(survey.getModificationDate(),
               result.getModificationDate());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT IN SOLR allowedOperations=");
      builder.append(survey.getAllowedOperations());
      builder.append("\t");
      builder.append(result.getAllowedOperations());
      builder.append("\n");
      builder.append("NOT IN SOLR obfusicated=");
      builder.append(survey.isObfuscated());
      builder.append("\t");
      builder.append(result.isObfuscated());
      builder.append("\n");
      builder.append("IGNORE parentId=");
      builder.append(survey.getParentId());
      builder.append("\t");
      builder.append(result.getParentId());
      builder.append("\n");
      builder.append("description=");
      builder.append(survey.getDescription());
      builder.append("\t");
      builder.append(result.getDescription());
      builder.append("\n");
      try
      {
         assertNotEquals("", survey.getDescription());
         assertNotEquals("", result.getDescription());
         assertEquals(survey.getDescription(), result.getDescription());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("name=");
      builder.append(survey.getName());
      builder.append("\t");
      builder.append(result.getName());
      builder.append("\n");
      try
      {
         assertNotEquals("", survey.getName());
         assertNotEquals("", result.getName());
         assertEquals(survey.getName(), result.getName());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("availability=");
      builder.append(survey.getAvailability());
      builder.append("\t");
      builder.append(result.getAvailability());
      builder.append("\n");
      try
      {
         assertNotEquals("", survey.getAvailability());
         assertNotEquals("", result.getAvailability());
         assertEquals(survey.getAvailability(), result.getAvailability());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT IN SOLR container=");
      builder.append(survey.isContainer());
      builder.append("\t");
      builder.append(result.isContainer());
      builder.append("\n");
      assertAndReport(survey.getPortal(), result.getPortal());
      builder.append("\n");
      assertAndReport("from Survey created by", survey.getCreatedBy(),
            result.getCreatedBy());
      builder.append("\n");
      assertAndReport("from Survey modified by", survey.getModifiedBy(),
            result.getModifiedBy());
      builder.append("\n");
      builder.append("from Survey IGNORE owner");
      builder.append("\n");
      builder.append("from Survey IGNORE Deputy Custodians");
      builder.append("\n");
   }

   private void assertAndReport(Portal portal, Portal result)
   {
      builder.append("Portal\n");
      try
      {
         assertNotEquals(null, portal);
      } catch (AssertionError e)
      {
         builder.append("portal is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("id=");
      builder.append(portal.getId());
      builder.append("\t");
      builder.append(result.getId());
      builder.append("\n");
      try
      {
         assertNotEquals("", portal.getId());
         assertNotEquals("", result.getId());
         assertEquals(portal.getId(), result.getId());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("creationDate=");
      builder.append(portal.getCreationDate());
      builder.append("\t");
      builder.append(result.getCreationDate());
      builder.append("\n");
      try
      {
         assertNotEquals("", portal.getCreationDate());
         assertNotEquals("", result.getCreationDate());
         assertEquals(portal.getCreationDate(), result.getCreationDate());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("modificationDate=");
      builder.append(portal.getModificationDate());
      builder.append("\t");
      builder.append(result.getModificationDate());
      builder.append("\n");
      try
      {
         assertNotEquals("", portal.getModificationDate());
         assertNotEquals("", result.getModificationDate());
         assertEquals(portal.getModificationDate(),
               result.getModificationDate());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT IN SOLR allowedOperations=");
      builder.append(portal.getAllowedOperations());
      builder.append("\t");
      builder.append(result.getAllowedOperations());
      builder.append("\n");
      builder.append("NOT IN SOLR obfusicated=");
      builder.append(portal.isObfuscated());
      builder.append("\t");
      builder.append(result.isObfuscated());
      builder.append("\n");
      builder.append("title=");
      builder.append(portal.getTitle());
      builder.append("\t");
      builder.append(result.getTitle());
      builder.append("\n");
      try
      {
         assertNotEquals("", portal.getTitle());
         assertNotEquals("", result.getTitle());
         assertEquals(portal.getTitle(), result.getTitle());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("url=");
      builder.append(portal.getUrl());
      builder.append("\t");
      builder.append(result.getUrl());
      builder.append("\n");
      try
      {
         assertNotEquals("", portal.getUrl());
         assertNotEquals("", result.getUrl());
         assertEquals(portal.getUrl(), result.getUrl());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      assertAndReport("from Portal created by", portal.getCreatedBy(),
            result.getCreatedBy());
      builder.append("\n");
      assertAndReport("from Portal modified by", portal.getModifiedBy(),
            result.getModifiedBy());
      builder.append("\n");
   }

   private void assertAndReport(String info, Set<Person> people,
         Set<Person> result)
   {
      builder.append(info + " People\n");
      try
      {
         assertNotEquals(null, people);
      } catch (AssertionError e)
      {
         builder.append("people are null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("number of people=");
      builder.append(people.size());
      builder.append("\t");
      builder.append(result.size());
      try
      {
         assertNotEquals(0, people.size());
         assertNotEquals(0, result.size());
         assertEquals(people.size(), result.size());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("\n");
      List<Person> personList = new ArrayList<>(people);
      List<Person> resultList = new ArrayList<>(result);
      Collections.sort(personList, Comparator.comparing(Person::getEmail));
      Collections.sort(resultList, Comparator.comparing(Person::getEmail));
      for (int i = 0; i < personList.size(); i++)
      {
         builder.append(i + "\n");
         assertAndReport(info, personList.get(i), resultList.get(i));
      }
   }

   private void assertAndReport(String info, Person person, Person result)
   {
      builder.append(info + " Person\n");
      try
      {
         assertNotEquals(null, person);
      } catch (AssertionError e)
      {
         builder.append("person is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("id=");
      builder.append(person.getId());
      builder.append("\t");
      builder.append(result.getId());
      builder.append("\n");
      try
      {
         assertEquals(person.getId(), result.getId());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT IN SOLR creationDate=");
      builder.append(person.getCreationDate());
      builder.append("\t");
      builder.append(result.getCreationDate());
      builder.append("\n");
      builder.append("NOT IN SOLR modificationDate=");
      builder.append(person.getModificationDate());
      builder.append("\t");
      builder.append(result.getModificationDate());
      builder.append("\n");
      builder.append("NOT IN SOLR allowedOperations=");
      builder.append(person.getAllowedOperations());
      builder.append("\t");
      builder.append(result.getAllowedOperations());
      builder.append("\n");
      builder.append("NOT IN SOLR obfusicated=");
      builder.append(person.isObfuscated());
      builder.append("\t");
      builder.append(result.isObfuscated());
      builder.append("\n");
      builder.append("firstName=");
      builder.append(person.getFirstName());
      builder.append("\t");
      builder.append(result.getFirstName());
      builder.append("\n");
      try
      {
         assertNotEquals("", person.getFirstName());
         assertNotEquals("", result.getFirstName());
         assertEquals(person.getFirstName(), result.getFirstName());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("lastName=");
      builder.append(person.getLastName());
      builder.append("\t");
      builder.append(result.getLastName());
      builder.append("\n");
      try
      {
         assertNotEquals("", person.getLastName());
         assertNotEquals("", result.getLastName());
         assertEquals(person.getLastName(), result.getLastName());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("externalKey=");
      builder.append(person.getExternalKey());
      builder.append("\t");
      builder.append(result.getExternalKey());
      builder.append("\n");
      try
      {
         assertNotEquals("", person.getExternalKey());
         assertNotEquals("", result.getExternalKey());
         assertEquals(person.getExternalKey(), result.getExternalKey());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("NOT IN SOLR email=");
      builder.append(person.getEmail());
      builder.append("\t");
      builder.append(result.getEmail());
      builder.append("\n");
      builder.append(info + " created by NOT IN SOLR");
      builder.append("\n");
      builder.append(info + " modified by NOT IN SOLR");
      builder.append("\n");
   }

   private void assertAndReport(String info, User user, User result)
   {
      builder.append(info + " User\n");
      try
      {
         assertNotEquals(null, user);
      } catch (AssertionError e)
      {
         builder.append("user is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("login=");
      builder.append(user.getLogin());
      builder.append("\t");
      builder.append(result.getLogin());
      try
      {
         assertNotEquals("", user.getLogin());
         assertNotEquals("", result.getLogin());
         assertEquals(user.getLogin(), result.getLogin());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
   }

   private void assertAndReport(Herbarium herbarium, Herbarium result)
   {
      builder.append("Herbarium\n");
      try
      {
         assertNotEquals(null, herbarium);
      } catch (AssertionError e)
      {
         builder.append("herbarium is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      try
      {
         assertNotEquals(null, result);
      } catch (AssertionError e)
      {
         builder.append("result is null");
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("code=");
      builder.append(herbarium.getCode());
      builder.append("\t");
      builder.append(result.getCode());
      builder.append("\n");
      try
      {
         assertNotEquals("", herbarium.getCode());
         assertNotEquals("", result.getCode());
         assertEquals(herbarium.getCode(), result.getCode());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
      builder.append("herbary=");
      builder.append(herbarium.getHerbary());
      builder.append("\t");
      builder.append(result.getHerbary());
      builder.append("\n");
      try
      {
         assertNotEquals("", herbarium.getHerbary());
         assertNotEquals("", result.getHerbary());
         assertEquals(herbarium.getHerbary(), result.getHerbary());
      } catch (AssertionError e)
      {
         System.out.println(builder.toString());
         throw new AssertionError(e);
      }
   }

   private void assertAndReport(List<Medium> media, List<Medium> result)
   {
      builder.append("NO MAPPING Media\n");
      if (media == null)
      {
         builder.append("media is null");
         builder.append("\n");
         return;
      }
      if (result == null)
      {
         builder.append("result is null");
         builder.append("\n");
         return;
      }
      builder.append("number of media=");
      builder.append("\n");
      builder.append(media.size());
      builder.append("\t");
      builder.append(result.size());
      if (media.size() == result.size())
      {
         for (int i = 0; i < media.size(); i++)
         {
            builder.append("\n");
            builder.append(i + " path=");
            builder.append(media.get(i).getPath());
            builder.append("\t");
            builder.append(result.get(i).getPath());
            builder.append("\n");
            builder.append(i + " description=");
            builder.append(media.get(i).getDescription());
            builder.append("\t");
            builder.append(result.get(i).getDescription());
            builder.append("\n");
            builder.append(i + " type=");
            builder.append(media.get(i).getType());
            builder.append("\t");
            builder.append(result.get(i).getType());
         }
      }
   }

   private Context createContext(int portalId, User user)
   {
      Portal portal = new Portal();
      portal.setId(portalId);
      return new Context(portal, user, Collections.emptySet(), "test");
   }
}
