package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDate.Type;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.Position.PositionType;

public class WinArtDateFormater
{

   private WinArtDateFormater()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static String format(VagueDate vagueDate)
   {
      if (vagueDate.getType() == Type.DAY)
      {
         return vagueDate.getStartDate().format(
               DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMANY));
      } else
      {
         return vagueDate.getStartDate().format(
               DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMANY)) + "-"
               + vagueDate.getEndDate().format(
                     DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.GERMANY));
      }
   }

   public static String getCoordString(Position position)
   {
      if (position.getType() != PositionType.MTB)
      {
         return null;
      } else
      {
         switch (position.getMtb().getSize())
         {
         case MTB:
            return "M";
         case MTBQ:
            return "Q";
         case MTBQQ:
            return "V";
         case MTBQQQ:
            return "S";
         default:
            throw new IllegalArgumentException(
                  "Unknows MTB size " + position.getMtb().getSize());
         }
      }
   }
}
