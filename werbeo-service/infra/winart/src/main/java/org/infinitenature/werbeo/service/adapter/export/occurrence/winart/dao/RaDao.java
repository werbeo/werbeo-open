package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;

import com.linuxense.javadbf.DBFField;

public class RaDao extends AbstractDao<Occurrence>
{
   private static final String FILENAME = "GEXPO1RA.DB7";

   public RaDao(Path tempDir) throws IOException
   {
      super(Files.createFile(tempDir.resolve(FILENAME)));
   }

   @Override
   protected Object[] createRowData(Occurrence person)
   {
      return new Object[13];
   }

   @Override
   protected DBFField[] createFields()
   {
      DBFField fields[] = new DBFField[13];
      fields[0] = createCharDBFField("GEBIET", 10);
      fields[1] = createCharDBFField("TAXNR", 5);
      fields[2] = createCharDBFField("HAEUFIG", 1);
      fields[2] = createCharDBFField("ART", 70);
      fields[3] = createCharDBFField("AUTOR", 60);
      fields[4] = createCharDBFField("MARKE", 1);
      fields[5] = createCharDBFField("ZUSTAND", 3);
      fields[6] = createCharDBFField("FLAG", 1);
      fields[7] = createCharDBFField("SYN", 2);
      fields[8] = createCharDBFField("RL_KAT", 1);
      fields[9] = createCharDBFField("SIPNR", 5);
      fields[10] = createCharDBFField("AGGNR", 5);
      fields[11] = createCharDBFField("GUID", 36);
      fields[12] = createCharDBFField("VERSION", 3);
      return fields;
   }

   @Override
   public String getFileName()
   {
      return FILENAME;
   }

}
