package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.infinitenature.werbeo.service.core.api.enity.Person;

import com.linuxense.javadbf.DBFField;

public class PersonDao extends AbstractDao<Person>
{
   private static final String FILENAME = "GEXPO1AD.DB7";

   public PersonDao(Path tempDir) throws IOException
   {
      super(Files.createFile(tempDir.resolve(FILENAME)));
   }

   @Override
   protected DBFField[] createFields()
   {
      DBFField fields[] = new DBFField[11];
      fields[0] = createNumDBFField("NR", 7, 0);
      fields[1] = createCharDBFField("NAMECODE", 6);
      fields[2] = createCharDBFField("ANREDE", 10);
      fields[3] = createCharDBFField("NNAME", 40);
      fields[4] = createCharDBFField("VORNAME", 20);
      fields[5] = createCharDBFField("PLZ", 5);
      fields[6] = createCharDBFField("ORT", 30);
      fields[7] = createCharDBFField("STRASSE", 30);
      fields[8] = createCharDBFField("TEL", 20);
      fields[9] = createCharDBFField("FAX", 20);
      fields[10] = createCharDBFField("FIRMA", 60);
      return fields;
   }


   @Override
   public Object[] createRowData(Person person)
   {
      Object rowData[] = new Object[11];
      rowData[0] = Integer.valueOf(0);
      rowData[1] = String.valueOf(person.getId());
      rowData[3] = person.getLastName();
      rowData[4] = person.getFirstName();
      return rowData;
   }

   @Override
   public String getFileName()
   {
      return FILENAME;
   }

}
