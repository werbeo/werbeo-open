package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.infinitenature.werbeo.service.core.api.enity.Publication;

import com.linuxense.javadbf.DBFField;

public class LIDao extends AbstractDao<Publication>
{
   private static final String FILENAME = "GEXPO1LI.DB7";

   public LIDao(Path tempDir) throws IOException
   {
      super(Files.createFile(tempDir.resolve(FILENAME)));
   }

   @Override
   protected Object[] createRowData(Publication person)
   {
      return new Object[11];
   }

   @Override
   protected DBFField[] createFields()
   {

      DBFField fields[] = new DBFField[10];
      fields[0] = createCharDBFField("LIT", 6);
      fields[1] = createCharDBFField("AUTOR", 65);
      fields[2] = createCharDBFField("JAHR", 10);
      fields[3] = createCharDBFField("TITEL", 200);
      fields[4] = createCharDBFField("ZEITSCHR", 60);
      fields[5] = createCharDBFField("NUMMER", 7);
      fields[6] = createCharDBFField("SEITEN", 9);
      fields[7] = createCharDBFField("AUFLAGE", 2);
      fields[8] = createCharDBFField("ERSCHORT", 25);
      fields[9] = createCharDBFField("VERLAG", 40);

      return fields;
   }

   @Override
   public String getFileName()
   {
      return FILENAME;
   }

}
