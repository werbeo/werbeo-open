package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Collection;

import com.linuxense.javadbf.DBFDataType;
import com.linuxense.javadbf.DBFField;
import com.linuxense.javadbf.DBFWriter;

public abstract class AbstractDao<T>
{
   private final DBFWriter writer;

   public AbstractDao(Path tempDir)
   {
      writer = new DBFWriter(tempDir.toFile(), Charset.forName("IBM437"));
      writer.setFields(createFields());
   }

   protected DBFField createCharDBFField(String name, int length)
   {
      final DBFField fld = new DBFField();
      fld.setName(name);
      fld.setType(DBFDataType.CHARACTER);
      fld.setLength(length);

      return fld;
   }

   protected DBFField createDateDBFField(String name)
   {
      final DBFField fld = new DBFField();
      fld.setName(name);
      fld.setType(DBFDataType.DATE);
      return fld;
   }

   protected DBFField createNumDBFField(String name, int length, int decimal)
   {
      final DBFField fld = new DBFField();
      fld.setName(name);
      fld.setType(DBFDataType.NUMERIC);
      fld.setLength(length);
      fld.setDecimalCount(decimal);
      return fld;
   }

   public void addData(T data)
   {
      writer.addRecord(createRowData(data));
   }

   public void writeAll(Collection<T> data)
   {
      data.forEach(this::addData);
   }

   protected abstract Object[] createRowData(T person);

   protected abstract DBFField[] createFields();

   public abstract String getFileName();

   public void close()
   {
      writer.close();
   }
}
