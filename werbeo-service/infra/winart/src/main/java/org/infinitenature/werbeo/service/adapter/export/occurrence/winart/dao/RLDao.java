package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;

import com.linuxense.javadbf.DBFField;

public class RLDao extends AbstractDao<TaxonBase>
{
   private static final String FILENAME = "GEXPO1RL.DB7";

   public RLDao(Path tempDir) throws IOException
   {
      super(Files.createFile(tempDir.resolve(FILENAME)));
   }

   @Override
   protected Object[] createRowData(TaxonBase person)
   {
      return new Object[26];
   }

   @Override
   protected DBFField[] createFields()
   {
      DBFField fields[] = new DBFField[26];

      fields[0] = createCharDBFField("TAXNR", 5);
      fields[1] = createCharDBFField("SIPNR", 5);
      fields[2] = createCharDBFField("AGGNR", 5);
      fields[3] = createCharDBFField("GUELT", 2);
      fields[4] = createCharDBFField("EHRD", 6);
      fields[5] = createCharDBFField("EPI1", 30);
      fields[6] = createCharDBFField("EPI2", 50);
      fields[7] = createCharDBFField("RANG", 3);
      fields[8] = createCharDBFField("EPI3", 30);
      fields[9] = createCharDBFField("HYB", 1);
      fields[10] = createCharDBFField("AUTOR_BASI", 55);
      fields[11] = createCharDBFField("AUTOR_KOMB", 45);
      fields[12] = createCharDBFField("SENSU", 20);
      fields[13] = createCharDBFField("SENSUAUTOR", 50);
      fields[14] = createCharDBFField("TAX_ZUS", 50);
      fields[15] = createCharDBFField("ART", 70);
      fields[16] = createCharDBFField("TRIVIAL1", 60);
      fields[17] = createCharDBFField("FLOR", 1);
      fields[18] = createCharDBFField("NON_WISS", 1);
      fields[19] = createCharDBFField("NO_PUB", 1);
      fields[20] = createCharDBFField("KZ", 1);
      fields[21] = createCharDBFField("VORANG", 1);
      fields[22] = createCharDBFField("KONVSAT", 1);
      fields[23] = createCharDBFField("RL_KAT", 1);
      fields[24] = createCharDBFField("STATUS_ST", 3);
      fields[25] = createCharDBFField("EING_STATU", 3);

      return fields;
   }

   @Override
   public String getFileName()
   {
      return FILENAME;
   }

}
