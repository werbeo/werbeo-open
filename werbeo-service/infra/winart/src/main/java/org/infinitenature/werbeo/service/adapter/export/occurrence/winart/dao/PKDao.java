package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import com.linuxense.javadbf.DBFField;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Position.PositionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PKDao extends AbstractDao<Occurrence>
{
   private static final Logger LOGGER = LoggerFactory.getLogger(PKDao.class);

   private static final String FILENAME = "GEXPO1PK.DB7";

   private final CoordinateTransformerFactory coordinateTransformerFactory;

   public PKDao(Path tempDir,
         CoordinateTransformerFactory coordinateTransformerFactory)
         throws IOException
   {
      super(Files.createFile(tempDir.resolve(FILENAME)));
      this.coordinateTransformerFactory = coordinateTransformerFactory;
   }

   @Override
   protected DBFField[] createFields()
   {
      DBFField fields[] = new DBFField[44];
      fields[0] = createCharDBFField("GEBIET", 10);
      fields[1] = createCharDBFField("TAXNR", 5);
      fields[2] = createCharDBFField("ART", 70);
      fields[3] = createCharDBFField("AUTOR", 60);
      fields[4] = createCharDBFField("ZUSTAND", 3);
      fields[5] = createCharDBFField("BEO_DATUM", 21);
      fields[6] = createCharDBFField("ORTLOKAL", 50);
      fields[7] = createCharDBFField("ORTSNAME", 35);
      fields[8] = createCharDBFField("ANZAHL", 3);
      fields[9] = createCharDBFField("BLUE_SPRO", 3);
      fields[10] = createCharDBFField("BED_FLAE", 3);
      fields[11] = createCharDBFField("KO_BEZ", 1);
      fields[12] = createNumDBFField("GK_R", 7, 0);
      fields[13] = createNumDBFField("GK_H", 7, 0);
      fields[14] = createNumDBFField("UTM489_R", 7, 0);
      fields[15] = createNumDBFField("UTM489_H", 7, 0);
      fields[16] = createCharDBFField("TOLERANZ", 4);
      fields[17] = createCharDBFField("BIOTOP", 9);
      fields[18] = createCharDBFField("BEMERKUNG", 140);
      fields[19] = createCharDBFField("RL_KAT", 1);
      fields[20] = createCharDBFField("MARKE", 1);
      fields[21] = createCharDBFField("KREIS", 3);
      fields[22] = createCharDBFField("QUADR", 8);
      fields[23] = createCharDBFField("HOEHE", 9);
      fields[24] = createCharDBFField("SCHU_GEB", 10);
      fields[25] = createCharDBFField("QUELLE", 2);
      fields[26] = createCharDBFField("FLAG", 1);
      fields[27] = createCharDBFField("SYN", 2);
      fields[28] = createCharDBFField("BEOBACHT", 6);
      fields[29] = createCharDBFField("ERFNAME", 6);
      fields[30] = createDateDBFField("ERFDATUM");
      fields[31] = createCharDBFField("LIT", 6);
      fields[32] = createCharDBFField("FREI1", 20);
      fields[33] = createNumDBFField("FREI2", 6, 0);
      fields[34] = createCharDBFField("SIPNR", 5);
      fields[35] = createCharDBFField("AGGNR", 5);
      fields[36] = createCharDBFField("WEITERGABE", 1);
      fields[37] = createCharDBFField("SENS", 1);
      fields[38] = createCharDBFField("KONTROLLE", 1);
      fields[39] = createCharDBFField("ID_TIME", 20);
      fields[40] = createCharDBFField("OK", 1);
      fields[41] = createCharDBFField("GUID", 36);
      fields[42] = createCharDBFField("REF_GUID", 36);
      fields[43] = createCharDBFField("VERSION", 3);
      return fields;
   }

   @Override
   public Object[] createRowData(Occurrence occurrence)
   {
      Object rowData[] = new Object[44];
      // rowData[0] = Integer.valueOf(0); // GEBIET
      rowData[1] = occurrence.getTaxon().getExternalKey(); // TAXNR
      rowData[2] = occurrence.getTaxon().getName(); // ART
      // AUTOR
      rowData[4] = "x"; // ZUSTAND
      rowData[5] = WinArtDateFormater.format(occurrence.getSample().getDate()); // BEO_DATUM

      rowData[11] = WinArtDateFormater
            .getCoordString(occurrence.getSample().getLocality().getPosition()); // KO_BEZ

      CoordinateTransformer gKCoordinateTransformer = coordinateTransformerFactory
            .getCoordinateTransformer(
                  occurrence.getSample().getLocality().getPosition().getEpsg(), 31468);
      double[] centerGK = gKCoordinateTransformer
            .convert(occurrence.getSample().getLocality().getPosition().getPosCenter());
      rowData[12] = Math.round(centerGK[0]);
      rowData[13] = Math.round(centerGK[1]);

      CoordinateTransformer uTM489CoordinateTransformer = coordinateTransformerFactory
            .getCoordinateTransformer(
                  occurrence.getSample().getLocality().getPosition().getEpsg(), 25832);
      double[] centerUTM489 = uTM489CoordinateTransformer
            .convert(occurrence.getSample().getLocality().getPosition().getPosCenter());
      rowData[14] = Math.round(centerUTM489[0]);
      rowData[15] = Math.round(centerUTM489[1]);

      rowData[16] = occurrence.getSample().getLocality().getPosition()
            .getType() == PositionType.MTB ? ""
                  : String.valueOf(occurrence.getSample().getLocality().getPrecision()); // TOLERANZ

      rowData[22] = occurrence.getSample().getLocality().getPosition().getMtb().getMtb()
            .replace("/", "-");
      if(occurrence.getDeterminer() != null)
      {
    	  rowData[28] = String.valueOf(occurrence.getDeterminer().getId());
      }
      return rowData;
   }

   @Override
   public String getFileName()
   {
      return FILENAME;
   }

}
