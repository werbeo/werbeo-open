package org.infinitenature.werbeo.service.adapter.export.occurrence.winart;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao.KoDao;
import org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao.LIDao;
import org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao.PKDao;
import org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao.PersonDao;
import org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao.RLDao;
import org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao.RaDao;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceExportCommands;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WinArtOccurrenceExport implements OccurrenceExportCommands
{

   private CoordinateTransformerFactory coordianteTransformerFacktory;

   public WinArtOccurrenceExport(
         @Autowired CoordinateTransformerFactory coordianteTransformerFacktory)
   {
      super();
      this.coordianteTransformerFacktory = coordianteTransformerFacktory;
   }

   @Override
   public void export(Stream<Occurrence> occurrences, OutputStream outputStream,
         OccurrenceConfig occurrenceConfig, SampleConfig sampleConfig,
         SurveyConfig surveyConfig, int portalId) throws Exception
   {
      Path tempDir = Files.createTempDirectory("winart-export");
      Set<Person> uniquePeople = new HashSet<>();

      PersonDao adDao = new PersonDao(tempDir);
      PKDao pkDao = new PKDao(tempDir, coordianteTransformerFacktory);
      LIDao liDao = new LIDao(tempDir);
      KoDao koDao = new KoDao(tempDir);
      RLDao rlDao = new RLDao(tempDir);
      RaDao raDao = new RaDao(tempDir);

      Iterator<Occurrence> iterator = occurrences.iterator();

      while (iterator.hasNext())
      {
         Occurrence occurrence = iterator.next();

         uniquePeople.add(occurrence.getDeterminer());
         uniquePeople.add(occurrence.getSample().getRecorder());
         pkDao.addData(occurrence);

      }

      adDao.writeAll(uniquePeople);

      adDao.close();
      pkDao.close();
      liDao.close();
      koDao.close();
      rlDao.close();
      raDao.close();

      ZipOutputStream zos = new ZipOutputStream(outputStream);

      addEntry(zos, adDao.getFileName(), tempDir);
      addEntry(zos, pkDao.getFileName(), tempDir);
      addEntry(zos, liDao.getFileName(), tempDir);
      addEntry(zos, koDao.getFileName(), tempDir);
      addEntry(zos, rlDao.getFileName(), tempDir);
      addEntry(zos, raDao.getFileName(), tempDir);

      zos.close();

   }

   private void addEntry(ZipOutputStream zos, String fileName, Path tempDir)
         throws IOException
   {
      zos.putNextEntry(new ZipEntry(fileName));
      zos.write(
            FileUtils.readFileToByteArray(tempDir.resolve(fileName).toFile()));
      zos.closeEntry();
   }

   @Override
   public FileFormat getSupportedFileFormat()
   {
      return FileFormat.WINART;
   }

}
