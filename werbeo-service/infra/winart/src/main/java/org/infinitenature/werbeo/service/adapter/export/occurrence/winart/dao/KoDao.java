package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.infinitenature.werbeo.service.core.api.enity.Sample;

import com.linuxense.javadbf.DBFField;

public class KoDao extends AbstractDao<Sample>
{
   private static final String FILENAME = "GEXPO1KO.DB7";

   public KoDao(Path tempDir) throws IOException
   {
      super(Files.createFile(tempDir.resolve(FILENAME)));
   }

   @Override
   protected Object[] createRowData(Sample person)
   {
      return new Object[30];
   }

   @Override
   protected DBFField[] createFields()
   {
      DBFField fields[] = new DBFField[30];
      fields[0] = createCharDBFField("GEBIET", 10);
      fields[1] = createNumDBFField("GK_R", 7, 0);
      fields[2] = createNumDBFField("GK_H", 7, 0);
      fields[3] = createNumDBFField("UTM489_R", 7, 0);
      fields[4] = createNumDBFField("UTM489_H", 7, 0);
      fields[5] = createCharDBFField("ORTLOKAL", 50);
      fields[6] = createCharDBFField("ORTSNAME", 35);
      fields[7] = createCharDBFField("KREIS", 3);
      fields[8] = createCharDBFField("QUADR", 8);
      fields[9] = createCharDBFField("TOLERANZ", 4);
      fields[10] = createCharDBFField("HOEHE", 9);
      fields[11] = createCharDBFField("KO_BEZ", 1);
      fields[12] = createCharDBFField("BEO_DATUM", 21);
      fields[13] = createCharDBFField("SCHU_GEB", 10);
      fields[14] = createCharDBFField("BIOTOP", 9);
      fields[15] = createCharDBFField("QUELLE", 2);
      fields[16] = createCharDBFField("FLAG", 1);
      fields[17] = createCharDBFField("BEOBACHT", 6);
      fields[18] = createCharDBFField("ERFNAME", 6);
      fields[19] = createDateDBFField("ERFDATUM");
      fields[20] = createCharDBFField("LIT", 6);
      fields[21] = createCharDBFField("BEMERKUNG", 140);
      fields[22] = createCharDBFField("FREI1", 20);
      fields[23] = createNumDBFField("FREI2", 6, 0);
      fields[24] = createCharDBFField("WEITERGABE", 1);
      fields[25] = createCharDBFField("SENS", 1);
      fields[26] = createCharDBFField("KONTROLLE", 1);
      fields[27] = createCharDBFField("ID_TIME", 20);
      fields[28] = createCharDBFField("GUID", 36);
      fields[29] = createCharDBFField("VERSION", 3);

      return fields;
   }

   @Override
   public String getFileName()
   {
      return FILENAME;
   }

}
