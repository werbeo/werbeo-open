package org.infinitenature.werbeo.service.adapter.export.occurrence.winart;

import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class MTestWinArtOccurrenceExport
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(MTestWinArtOccurrenceExport.class);

   private WinArtOccurrenceExport exportUT;

   @BeforeEach
   void setUp() throws Exception
   {
      exportUT = new WinArtOccurrenceExport(new CoordinateTransformerFactory());
   }

   @Test
   void test() throws Exception
   {
      Path file = Files.createTempFile(
            MTestWinArtOccurrenceExport.class.getSimpleName(), ".zip");
      FileOutputStream fos = new FileOutputStream(file.toFile());
      LOGGER.info("Filename for test is {}", file.toString());
      Occurrence occ = new Occurrence();
      occ.setDeterminer(new Person(1, "Hans", "Wurst"));

      SampleBase sample = new Sample();
      sample.setRecorder(new Person(1, "Hans", "Wurst"));
      sample.setDate(VagueDateFactory.create(2018, 10, 10));
      Position position = new Position();

      position.setEpsg(4745);
      position.setMtb(MTBHelper.toMTB("1643/3"));
      position.setWkt(
            "POLYGON((12.833333333333334 54.3,12.833333333333334 54.35,12.916666666666666 54.35,12.916666666666666 54.3,12.833333333333334 54.3))");
      position.setWktEpsg(4745);
      position.setPosCenter(new double[] { 12.875, 54.325 });
      sample.getLocality().setPosition(position);
      occ.setSample(sample);

      TaxonBase taxon = new TaxonBase();
      taxon.setExternalKey("1234");
      taxon.setName("Blaue blume");
      occ.setTaxon(taxon);

      List<Occurrence> occurrneses = new ArrayList<>();
      occurrneses.add(occ);

      exportUT.export(occurrneses.stream(), fos, new OccurrenceConfig(),
            new SampleConfig(),
            new SurveyConfig(new Portal(), new HashSet<>()), 1);
      fos.close();
   }

}
