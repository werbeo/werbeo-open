package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import java.nio.file.Files;
import java.nio.file.Path;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MTestPersonDao
{
   private PersonDao personDaoUT;

   @BeforeEach
   void setUp() throws Exception
   {
      Path tempDir = Files
            .createTempDirectory(MTestPersonDao.class.getSimpleName());
      personDaoUT = new PersonDao(tempDir);
   }

   @Test
   void testAddData()
   {
      personDaoUT.addData(new Person(1, "Hans", "Wurst"));
      personDaoUT.close();
   }

}
