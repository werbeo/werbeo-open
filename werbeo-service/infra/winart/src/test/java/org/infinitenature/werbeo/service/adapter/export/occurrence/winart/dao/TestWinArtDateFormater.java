package org.infinitenature.werbeo.service.adapter.export.occurrence.winart.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.vaguedate.VagueDateFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestWinArtDateFormater
{

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   @DisplayName("Test date with day and month smaler than 10")
   void test001()
   {
      assertThat(WinArtDateFormater.format(VagueDateFactory.create(2018, 8, 9)),
            is("09.08.2018"));
   }

   @Test
   @DisplayName("Test date with day and month greater than 10")
   void test002()
   {
      assertThat(
            WinArtDateFormater.format(VagueDateFactory.create(2018, 10, 30)),
            is("30.10.2018"));
   }

   @Test
   @DisplayName("Test range date ")
   void test003()
   {
      assertThat(WinArtDateFormater.format(VagueDateFactory.create(2018)),
            is("01.01.2018-31.12.2018"));
   }
}
