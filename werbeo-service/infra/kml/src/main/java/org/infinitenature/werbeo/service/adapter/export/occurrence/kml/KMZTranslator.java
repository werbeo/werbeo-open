package org.infinitenature.werbeo.service.adapter.export.occurrence.kml;

import java.util.Map;

class KMZTranslator
{
   private Map<Enum<?>, String> translationMap;

   KMZTranslator(Map<Enum<?>, String> translationMap)
   {
      this.translationMap = translationMap;
   }

   public String getTranslation(Enum<?> someEnum)
   {
      if (someEnum == null)
      {
         return "";
      }

      return translationMap.get(someEnum);
   }
}
