package org.infinitenature.werbeo.service.adapter.export.occurrence.kml;

public class LatLonAltBox
{
   private String mtb;
   private Integer index;
   private ExtendOfArea extendOfArea;

   public String getMtb()
   {
      return mtb;
   }

   public void setMtb(String mtb)
   {
      this.mtb = mtb;
   }

   public Integer getIndex()
   {
      return index;
   }

   public void setIndex(Integer index)
   {
      this.index = index;
   }

   public ExtendOfArea getExtendOfArea()
   {
      return extendOfArea;
   }

   public void setExtendOfArea(ExtendOfArea extendOfArea)
   {
      this.extendOfArea = extendOfArea;
   }
}
