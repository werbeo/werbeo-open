package org.infinitenature.werbeo.service.adapter.export.occurrence.kml;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.geotools.data.DataUtilities;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.*;

class AbstractHandleKMLForExport extends AbstractHandleDataForExport
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(AbstractHandleKMLForExport.class);

   private ExtendOfArea totalAreaOfAllOccurrences = new ExtendOfArea();

   SimpleFeatureCollection createSampleFeatures(
         Collection<Occurrence> occurrences)
   {
      SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
      typeBuilder.setName("occurrences");
      typeBuilder.add("geometry", Geometry.class, DefaultGeographicCRS.WGS84);
      typeBuilder.add("name", String.class);
      typeBuilder.add("description", Object.class);
      SimpleFeatureType type = typeBuilder.buildFeatureType();

      SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(type);
      List<SimpleFeature> simpleFeatures = new ArrayList<>();
      CoordinateTransformerFactory factory = new CoordinateTransformerFactory();

      for (Occurrence occurrence : occurrences)
      {
         String wkt;
         if (occurrence.getSample().getLocality().getPosition().getWktEpsg()
               == 4326)
         {
            wkt = occurrence.getSample().getLocality().getPosition().getWkt();
         }
         else
         {
            CoordinateTransformer transformer = factory
                  .getCoordinateTransformer(
                        occurrence.getSample().getLocality().getPosition()
                              .getWktEpsg(), 4326);
            wkt = transformer.convert(
                  occurrence.getSample().getLocality().getPosition().getWkt());
         }

         WKTReader wktReader = new WKTReader();
         try
         {
            Geometry geom = wktReader.read(wkt);
            featureBuilder.add(geom);
            totalAreaOfAllOccurrences = findExtendOfArea(
                  geom.getCentroid().getX(), geom.getCentroid().getY(),
                  totalAreaOfAllOccurrences);

            featureBuilder.add(occurrence.getTaxon().getName());

            StringBuilder descriptionBuilder = new StringBuilder();
            descriptionBuilder
                  .append(occurrence.getSample().getSurvey().getName());
            descriptionBuilder.append(" ");
            descriptionBuilder.append(occurrence.getSample().getDate());
            descriptionBuilder.append(" Unschärfe: ");
            descriptionBuilder.append(occurrence.getSample().getLocality().getPrecision());
            descriptionBuilder.append("m Fund-UUID: ");
            descriptionBuilder.append(occurrence.getId());

            featureBuilder.add(descriptionBuilder.toString());
            simpleFeatures.add(featureBuilder
                  .buildFeature("uuid." + occurrence.getId().toString()));

         } catch (ParseException e)
         {
            LOGGER.error("Error in KMZ export, wkt of occurrence with ID {} could not be parsed",
                  occurrence.getId());
         }
      }

      return DataUtilities.collection(simpleFeatures);
   }

   String createParentFile(Map<String, LatLonAltBox> networkLinks,
         int totalOccurrences)
   {
      String west = String.valueOf(totalAreaOfAllOccurrences.getWest());
      String south = String.valueOf(totalAreaOfAllOccurrences.getSouth());
      String north = String.valueOf(totalAreaOfAllOccurrences.getNorth());
      String east = String.valueOf(totalAreaOfAllOccurrences.getEast());

      StringBuilder builder = new StringBuilder();
      builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
      builder.append("<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n");
      builder.append("<Document>\n");
      builder.append("<name>Gesamtgebiet ").append(totalOccurrences)
            .append(" Funde</name>\n");

      if(totalOccurrences > 0)
      {
         builder.append("<Region>\n");
         builder.append("<LatLonAltBox>\n");
         builder.append("<north>").append(north).append("</north>\n");
         builder.append("<south>").append(south).append("</south>\n");
         builder.append("<east>").append(east).append("</east>\n");
         builder.append("<west>").append(west).append("</west>\n");
         builder.append("</LatLonAltBox>\n");
         builder.append("</Region>\n");

         builder.append("<Placemark>\n");
         builder.append("<visibility>1</visibility>\n");
         builder.append("<name>Fundgebiet</name>\n");
         builder.append("<LineString>\n");
         builder.append("<tessellate>1</tessellate>\n");
         builder.append("<coordinates>\n");
         builder.append(west).append(",").append(south).append(",0\n");
         builder.append(east).append(",").append(south).append(",0\n");
         builder.append(east).append(",").append(north).append(",0\n");
         builder.append(west).append(",").append(north).append(",0\n");
         builder.append(west).append(",").append(south).append(",0\n");
         builder.append("</coordinates>\n");
         builder.append("</LineString>\n");
         builder.append("</Placemark>\n");

         List<String> sortedNetworkLinksList = new ArrayList(networkLinks.keySet());
         sortedNetworkLinksList.sort((o1, o2) -> {
            LatLonAltBox box1 = networkLinks.get(o1);
            LatLonAltBox box2 = networkLinks.get(o2);
            return box1.getIndex().compareTo(box2.getIndex());
         });

         for (String file : sortedNetworkLinksList)
         {
            createNetworkLink(builder, file, networkLinks.get(file));
         }
      }
      builder.append("</Document>\n");
      builder.append("</kml>\n");
      return builder.toString();
   }

   private void createNetworkLink(StringBuilder builder, String childFile,
         LatLonAltBox box)
   {
      builder.append("<NetworkLink>\n");
      builder.append("<name>Ebene_").append(box.getMtb()).append("</name>\n");

      builder.append("<Region>\n");
      builder.append("<LatLonAltBox>\n");
      builder.append("<north>").append(box.getExtendOfArea().getNorth())
            .append("</north>\n");
      builder.append("<south>").append(box.getExtendOfArea().getSouth())
            .append("</south>\n");
      builder.append("<east>").append(box.getExtendOfArea().getEast())
            .append("</east>\n");
      builder.append("<west>").append(box.getExtendOfArea().getWest())
            .append("</west>\n");
      builder.append("</LatLonAltBox>\n");
      builder.append("</Region>\n");

      builder.append("<Placemark>\n");
      builder.append("<visibility>0</visibility>\n");
      builder.append("<name>Region</name>\n");
      builder.append("<LineString>\n");
      builder.append("<tessellate>1</tessellate>\n");
      builder.append("<coordinates>\n");
      builder.append(box.getExtendOfArea().getWest()).append(",")
            .append(box.getExtendOfArea().getSouth()).append(",0\n");
      builder.append(box.getExtendOfArea().getEast()).append(",")
            .append(box.getExtendOfArea().getSouth()).append(",0\n");
      builder.append(box.getExtendOfArea().getEast()).append(",")
            .append(box.getExtendOfArea().getNorth()).append(",0\n");
      builder.append(box.getExtendOfArea().getWest()).append(",")
            .append(box.getExtendOfArea().getNorth()).append(",0\n");
      builder.append(box.getExtendOfArea().getWest()).append(",")
            .append(box.getExtendOfArea().getSouth()).append(",0\n");
      builder.append("</coordinates>");
      builder.append("</LineString>\n");
      builder.append("</Placemark>\n");

      builder.append("<Link>\n");
      builder.append("<href>").append(childFile).append("</href>\n");
      builder.append("<viewRefreshMode>onRegion</viewRefreshMode>\n");
      builder.append("</Link>\n");

      builder.append("</NetworkLink>\n");
   }

   // Workaround for https://trac.osgeo.org/gdal/ticket/6981
   // remove after fix is published for recent gdal and qgis versions

   void deleteRootElement(OutputStream os, ByteArrayOutputStream tempOs)
         throws TransformerFactoryConfigurationError, TransformerException
   {
      TransformerFactory factory = TransformerFactory.newInstance();
      Source xslt = new StreamSource(
            this.getClass().getResourceAsStream("/removeNs.xslt"));
      Transformer transformer = factory.newTransformer(xslt);
      Source text = new StreamSource(
            new ByteArrayInputStream(tempOs.toByteArray()));
      transformer.transform(text, new StreamResult(os));
   }

   ExtendOfArea getTotalAreaOfAllOccurrences()
   {
      return totalAreaOfAllOccurrences;
   }
}
