package org.infinitenature.werbeo.service.adapter.export.occurrence.kml;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.kml.KML;
import org.geotools.kml.KMLConfiguration;
import org.geotools.xsd.Encoder;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceExportCommands;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class KMZOccurrenceExport extends AbstractHandleKMLForExport
      implements OccurrenceExportCommands
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(KMZOccurrenceExport.class);

   @Override
   public void export(Stream<Occurrence> occurrences, OutputStream outputStream,
         OccurrenceConfig occurrenceConfig, SampleConfig sampleConfig,
         SurveyConfig surveyConfig, int portalId)
   {
      if (occurrences.count() == 0)
      {
         try (ZipOutputStream zos = new ZipOutputStream(outputStream))
         {
            ZipEntry ze = new ZipEntry("doc.kml");
            zos.putNextEntry(ze);
            PrintStream ps = new PrintStream(zos);
            ps.println(createParentFile(Collections.emptyMap(), 0));
            ps.flush();
            zos.closeEntry();
            zos.flush();
         } catch (IOException | TransformerFactoryConfigurationError e)
         {
            LOGGER.error("Failure to create parent file for empty KMZ."
                  + e.getMessage());
         }
         return;
      }
      Occurrence[] sortedOccurrences = occurrences
            .sorted(Comparator.comparing(this::getMesstischblatt))
            .toArray(Occurrence[]::new);

      Set<List<Occurrence>> occurrenceLists = divideUpDataIntoMTBs(
            sortedOccurrences);

      List<List<Occurrence>> sortedOccurrenceLists = new ArrayList<>(
            occurrenceLists);
      sortedOccurrenceLists.sort(Comparator.comparing(this::getMesstischblatt));

      try (ZipOutputStream zos = new ZipOutputStream(outputStream))
      {
         startingValuesForFindingFullArea(sortedOccurrenceLists.get(0).get(0));

         Map<String, LatLonAltBox> networkLinks = new HashMap<>();

         ZipEntry ze0 = new ZipEntry("regions/");
         zos.putNextEntry(ze0);
         zos.closeEntry();

         int index = 0;

         for (List<Occurrence> occurrenceList : sortedOccurrenceLists)
         {
            String link = "regions/region" + index + "_"
                  + getMesstischblatt(occurrenceList) + ".kml";

            SimpleFeatureCollection features = createSampleFeatures(
                  occurrenceList);
            Encoder encoder = new Encoder(new KMLConfiguration());

            try (ByteArrayOutputStream tempOs = new ByteArrayOutputStream();
                  ByteArrayOutputStream baos = new ByteArrayOutputStream())
            {
               encoder.encode(features, KML.kml, tempOs);
               deleteRootElement(baos, tempOs);
               ZipEntry ze = new ZipEntry(link);
               zos.putNextEntry(ze);
               PrintStream ps = new PrintStream(zos);
               ps.println(baos.toString());
               ps.flush();
               zos.closeEntry();
               networkLinks.put(link, readNetworkLink(occurrenceList, index));
               index++;
            } catch (IOException | TransformerFactoryConfigurationError
                  | TransformerException e)
            {
               LOGGER.error(
                     "Failure to encode occurrences as KMZ at occurrenceList."
                           + e.getMessage());
            }
         }

         ZipEntry ze = new ZipEntry("doc.kml");
         zos.putNextEntry(ze);
         PrintStream ps = new PrintStream(zos);
         ps.println(createParentFile(networkLinks, sortedOccurrences.length));
         ps.flush();
         zos.closeEntry();

         zos.flush();
      } catch (IOException | TransformerFactoryConfigurationError
            | ParseException e)
      {
         LOGGER.error(
               "Failure to create parent file for KMZ." + e.getMessage());
      }
   }

   private void startingValuesForFindingFullArea(Occurrence occurrence)
         throws ParseException
   {
      Point point = getCentroidPoint(occurrence);
      getTotalAreaOfAllOccurrences().setEast(point.getX());
      getTotalAreaOfAllOccurrences().setWest(point.getX());
      getTotalAreaOfAllOccurrences().setNorth(point.getY());
      getTotalAreaOfAllOccurrences().setSouth(point.getY());
   }

   @Override
   public FileFormat getSupportedFileFormat()
   {
      return FileFormat.KMZ;
   }
}
