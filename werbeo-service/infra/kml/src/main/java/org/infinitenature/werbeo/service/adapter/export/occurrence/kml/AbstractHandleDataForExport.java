package org.infinitenature.werbeo.service.adapter.export.occurrence.kml;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

abstract class AbstractHandleDataForExport
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(AbstractHandleDataForExport.class);

   private static final int THRESHHOLD = 1000;

   Set<List<Occurrence>> divideUpDataIntoMTBs(Occurrence[] sortedOccurrences)
   {
      Occurrence firstOccurrence = sortedOccurrences[0];

      Set<List<Occurrence>> allMesstischblattSorted = new HashSet<>();
      List<Occurrence> oneMesstischblattSorted = new ArrayList<>();
      int currentMTB = getMesstischblatt(firstOccurrence);
      allMesstischblattSorted.add(oneMesstischblattSorted);

      for (Occurrence occurrence : sortedOccurrences)
      {
         if (currentMTB == getMesstischblatt(occurrence))
         {
            oneMesstischblattSorted.add(occurrence);
         }
         else
         {
            oneMesstischblattSorted = new ArrayList<>();
            allMesstischblattSorted.add(oneMesstischblattSorted);
            oneMesstischblattSorted.add(occurrence);
            currentMTB = getMesstischblatt(occurrence);
         }
      }

      Set<List<Occurrence>> markedForBreakupIntoQuadrants = new HashSet<>();
      Set<List<Occurrence>> noFurtherBreakup = new HashSet<>();
      for (List<Occurrence> list : allMesstischblattSorted)
      {
         if (list.size() > THRESHHOLD)
         {
            markedForBreakupIntoQuadrants.add(list);
         }
         else
         {
            noFurtherBreakup.add(list);
         }
      }

      for (List<Occurrence> list : markedForBreakupIntoQuadrants)
      {
         noFurtherBreakup.addAll(breakIntoQuadrants(6, list));
      }

      return noFurtherBreakup;
   }

   private Collection<? extends List<Occurrence>> breakIntoQuadrants(
         int lengthMTB, List<Occurrence> occurrences)
   {
      Set<List<Occurrence>> result = new HashSet<>();
      if (lengthMTB > 8) // stop recursion
      {
         if (!occurrences.isEmpty())
         {
            result.add(occurrences);
         }
         return result;
      }

      List<Occurrence> withoutQuadrants = new ArrayList<>();
      List<Occurrence> quadrant1 = new ArrayList<>();
      List<Occurrence> quadrant2 = new ArrayList<>();
      List<Occurrence> quadrant3 = new ArrayList<>();
      List<Occurrence> quadrant4 = new ArrayList<>();

      for (Occurrence occurrence : occurrences) // sorting
      {
         String mtb = occurrence.getSample().getLocality().getPosition().getMtb()
               .getMtb();
         if (mtb.length() < lengthMTB)
         {
            withoutQuadrants.add(occurrence);
         }
         else
         {
            String quadrant = mtb.substring(lengthMTB - 1, lengthMTB);
            switch (quadrant)
            {
            case "1":
               quadrant1.add(occurrence);
               break;
            case "2":
               quadrant2.add(occurrence);
               break;
            case "3":
               quadrant3.add(occurrence);
               break;
            case "4":
               quadrant4.add(occurrence);
               break;
            default:
               LOGGER.error(
                     "Error in KMZ export, occurrence with ID {} could not be added to export",
                     occurrence.getId());
               break;
            }
         }
      }

      // checking results of sorting:

      if (!withoutQuadrants.isEmpty())
      {
         result.add(withoutQuadrants);
      }

      if (quadrant1.size() > THRESHHOLD) // recursion
      {
         result.addAll(breakIntoQuadrants(lengthMTB + 1, quadrant1));
      }
      else if (!quadrant1.isEmpty()) // else no recursion
      {
         result.add(quadrant1);
      }

      if (quadrant2.size() > THRESHHOLD) // recursion
      {
         result.addAll(breakIntoQuadrants(lengthMTB + 1, quadrant2));
      }
      else if (!quadrant2.isEmpty()) // else no recursion
      {
         result.add(quadrant2);
      }

      if (quadrant3.size() > THRESHHOLD) // recursion
      {
         result.addAll(breakIntoQuadrants(lengthMTB + 1, quadrant3));
      }
      else if (!quadrant3.isEmpty()) // else no recursion
      {
         result.add(quadrant3);
      }

      if (quadrant4.size() > THRESHHOLD) // recursion
      {
         result.addAll(breakIntoQuadrants(lengthMTB + 1, quadrant4));
      }
      else if (!quadrant4.isEmpty()) // else no recursion
      {
         result.add(quadrant4);
      }
      return result;
   }

   LatLonAltBox readNetworkLink(List<Occurrence> occurrenceList, int index)
         throws ParseException
   {
      Position position = occurrenceList.get(0).getSample().getLocality().getPosition();
      String mtb = position.getMtb().getMtb();
      ExtendOfArea extendOfArea = new ExtendOfArea(position.getPosCenter()[0], position.getPosCenter()[1]);

      LatLonAltBox box = new LatLonAltBox();
      box.setExtendOfArea(extendOfArea);
      box.setIndex(index);
      box.setMtb(mtb);

      for (Occurrence occurrence : occurrenceList)
      {
         Point point = getCentroidPoint(occurrence);
         extendOfArea = findExtendOfArea(point.getX(), point.getY(), extendOfArea);
         if (mtb.length() > occurrence.getSample().getLocality().getPosition()
               .getMtb().getMtb().length())
         {
            mtb = occurrence.getSample().getLocality().getPosition().getMtb()
                  .getMtb();
         }
      }

      return box;
   }

   Point getCentroidPoint(Occurrence occurrence) throws ParseException
   {
      CoordinateTransformerFactory factory = new CoordinateTransformerFactory();
      CoordinateTransformer transformer = factory.getCoordinateTransformer(
            occurrence.getSample().getLocality().getPosition().getWktEpsg(),
            4326);
      String wkt = transformer.convert(
            occurrence.getSample().getLocality().getPosition().getWkt());
      WKTReader wktReader = new WKTReader();

      Geometry geom = wktReader.read(wkt);
      return geom.getCentroid();
   }

   ExtendOfArea findExtendOfArea(double westEast, double northSouth,
         ExtendOfArea extendOfArea)
   {
      if (northSouth > extendOfArea.getNorth())
      {
         extendOfArea.setNorth(northSouth);
      }
      if (northSouth < extendOfArea.getSouth())
      {
         extendOfArea.setSouth(northSouth);
      }
      if (westEast > extendOfArea.getEast())
      {
         extendOfArea.setEast(westEast);
      }
      if (westEast < extendOfArea.getWest())
      {
         extendOfArea.setWest(westEast);
      }
      return extendOfArea;
   }

   Integer getMesstischblatt(Occurrence o1)
   {
      return Integer.valueOf(
            o1.getSample().getLocality().getPosition().getMtb().getMtb()
                  .substring(0, 4));
   }

   Integer getMesstischblatt(List<Occurrence> list)
   {
      return getMesstischblatt(list.get(0));
   }
}
