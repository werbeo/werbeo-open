package org.infinitenature.werbeo.service.adapter.export.occurrence.kml;

public class ExtendOfArea
{
   private double west;
   private double east;
   private double north;
   private double south;

   public ExtendOfArea()
   {

   }

   public ExtendOfArea(double startingValueWestEast, double startingValueNorthSouth)
   {
      west = startingValueWestEast;
      east = startingValueWestEast;
      north = startingValueNorthSouth;
      south = startingValueNorthSouth;
   }

   public double getWest()
   {
      return west;
   }

   public void setWest(double west)
   {
      this.west = west;
   }

   public double getEast()
   {
      return east;
   }

   public void setEast(double east)
   {
      this.east = east;
   }

   public double getNorth()
   {
      return north;
   }

   public void setNorth(double north)
   {
      this.north = north;
   }

   public double getSouth()
   {
      return south;
   }

   public void setSouth(double south)
   {
      this.south = south;
   }
}
