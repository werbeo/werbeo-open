package org.infinitenature.werbeo.service.adapter.export.occurrence.kml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Description;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTWriter;

public class MTestKMZExport
{
   private KMZOccurrenceExport kmzOccurrenceExport;
   private List<Occurrence> occurrenceList;
   private Set<String> messtischblaetter;
   private List<MTB> mtbs;
   private List<Position> positions;
   private List<Occurrence> occurrences;

   private String[] messtischblaetterElse = { "1345", "1346", "1444", "1445",
         "1446", "1447", "1541", "1542", "1543", "1545", "1546", "1547", "1640",
         "1641", "1642", "1643", "1644", "1645", "1646", "1647", "1648", "1739",
         "1740", "1741", "1742", "1743", "1744", "1745", "1746", "1836", "1837",
         "1838", "1839", "1840", "1841", "1842", "1843", "1844", "1846", "1848",
         "2550", "2551", "2742", "2743", "2744", "2834" };
   private Integer[] messtischblaetter19 = { 1935, 1949 };
   private Integer[] messtischblaetter20 = { 2031, 2050 };
   private Integer[] messtischblaetter21 = { 2131, 2149 };
   private Integer[] messtischblaetter22 = { 2231, 2251 };
   private Integer[] messtischblaetter23 = { 2332, 2351 };
   private Integer[] messtischblaetter24 = { 2431, 2451 };
   private Integer[] messtischblaetter25 = { 2530, 2547 };
   private Integer[] messtischblaetter26a = { 2631, 2637 };
   private Integer[] messtischblaetter26b = { 2640, 2646 };
   private Integer[] messtischblaetter27 = { 2732, 2736 };

   @BeforeEach
   void setUp()
   {
      kmzOccurrenceExport = new KMZOccurrenceExport();

      occurrenceList = new ArrayList<>();
      messtischblaetter = new HashSet<>();
      makeSimpleMesstischblaetter();
      makeQuadarants();
      mtbs = new ArrayList<>();
      makeMTBs();
      positions = new ArrayList<>();
      makePositions();
      occurrences = new ArrayList<>();
      makeOccurrences();
   }

   @Test
   @Description("test >200000 occurrences")
   void test001() throws FileNotFoundException
   {
      File file = new File("C:/tmp/testForGoogleEarth.kmz");
      FileOutputStream fileOutputStream = new FileOutputStream(file);
      kmzOccurrenceExport.export(occurrences.stream(), fileOutputStream,
            new OccurrenceConfig(), new SampleConfig(),
            new SurveyConfig(new Portal(1), new HashSet<>()), 1);
   }

   private void makeOccurrences()
   {
      Random randomgenerator = new Random();
      for (Position position : positions)
      {
         Occurrence occurrence = new Occurrence();
         SampleBase sample = new SampleBase();
         Locality locality = new Locality();
         locality.setPosition(position);
         sample.setLocality(locality);
         Survey survey = new Survey();
         survey.setName("survey" + randomgenerator.nextInt(100));
         sample.setSurvey(survey);
         int year = randomgenerator.nextInt(100) + 1918;
         int month = randomgenerator.nextInt(12) + 1;
         int day = randomgenerator.nextInt(28) + 1;
         VagueDate date = new VagueDate(LocalDate.of(year, month, day),
               LocalDate.of(year + 1, month, day), VagueDate.Type.DAYS);
         sample.setDate(date);
         occurrence.setSample(sample);
         occurrence.setId(UUID.randomUUID());
         TaxonBase taxon = new TaxonBase();
         taxon.setName("taxon" + randomgenerator.nextInt(400));
         occurrence.setTaxon(taxon);
         occurrences.add(occurrence);
      }
   }



   private void makePositions()
   {
      for (MTB mtb : mtbs)
      {
         Position position = new Position();
         position.setMtb(mtb);
         position.setEpsg(4326);

         Point point;
         GeometryHelper geometryHelper = new GeometryHelper();
         Geometry geometry = null;
         try
         {
            geometry = geometryHelper
                  .parseToJts(mtb.toWkt(), 4326);
         } catch (ParseException e)
         {

         }
         point = geometry.getInteriorPoint();
         double[] latlong = new double[2];
         latlong[0] = point.getX();
         latlong[1] = point.getY();
         position.setPosCenter(latlong);
         WKTWriter wktWriter = new WKTWriter();
         position.setWkt(wktWriter.write(point));
         position.setWktEpsg(4326);
         position.setType(Position.PositionType.POINT);
         positions.add(position);
      }
   }

   private void makeMTBs()
   {
      for (String mtbString : messtischblaetter)
      {
         MTB mtb = MTBHelper.toMTB(mtbString);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
         mtbs.add(mtb);
      }
   }

   private void makeQuadarants()
   {
      Set<String> result4thQuadrants = new HashSet<>();
      for (String mtb : messtischblaetter)
      {
         result4thQuadrants.add(mtb + "/1");
         result4thQuadrants.add(mtb + "/2");
         result4thQuadrants.add(mtb + "/3");
         result4thQuadrants.add(mtb + "/4");
      }

      Set<String> result16thQuadrants = new HashSet<>();
      for (String mtb : result4thQuadrants)
      {
         result16thQuadrants.add(mtb + "1");
         result16thQuadrants.add(mtb + "2");
         result16thQuadrants.add(mtb + "3");
         result16thQuadrants.add(mtb + "4");
      }

      Set<String> result64thQuadrants = new HashSet<>();
      for (String mtb : result16thQuadrants)
      {
         result64thQuadrants.add(mtb + "1");
         result64thQuadrants.add(mtb + "2");
         result64thQuadrants.add(mtb + "3");
         result64thQuadrants.add(mtb + "4");
      }
      messtischblaetter.addAll(result4thQuadrants);
      messtischblaetter.addAll(result16thQuadrants);
      messtischblaetter.addAll(result64thQuadrants);
   }

   private void makeSimpleMesstischblaetter()
   {
      Collections.addAll(messtischblaetter, messtischblaetterElse);
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter19));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter20));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter21));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter22));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter23));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter24));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter25));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter26a));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter26b));
      messtischblaetter.addAll(makeMesstischblaetter(messtischblaetter27));
   }

   private Collection<? extends String> makeMesstischblaetter(
         Integer[] messtischblaetter)
   {
      Set<String> result = new HashSet<>();
      for (int i = messtischblaetter[0]; i <= messtischblaetter[1]; i++)
      {
         result.add(String.valueOf(i));
      }
      return result;
   }

}
