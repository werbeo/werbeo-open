package org.infinitenature.werbeo.service.adapters;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class AdditionalTaxonAttributeConfig
{
   private int redListStatusId;


   public int getRedListStatusId()
   {
      return redListStatusId;
   }

   public void setRedListStatusId(int redListStatusId)
   {
      this.redListStatusId = redListStatusId;
   }

   @Override
   public String toString()
   {
      return AdditionalTaxonAttributeConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return AdditionalTaxonAttributeConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return AdditionalTaxonAttributeConfigBeanUtil.doEquals(this, obj);
   }
}
