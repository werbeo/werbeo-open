package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleAttributeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SampleAttributeValueRepository
      extends JpaRepository<DBSampleAttributeValue, Integer>,
      QuerydslPredicateExecutor<DBSampleAttributeValue>
{

}
