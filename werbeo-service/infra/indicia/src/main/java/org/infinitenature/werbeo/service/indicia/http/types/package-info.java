// evry java.util.Date class in the types pacage should be processed by DateAdapter
@XmlJavaTypeAdapter(value = DateAdapter.class, type = Date.class)
package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
