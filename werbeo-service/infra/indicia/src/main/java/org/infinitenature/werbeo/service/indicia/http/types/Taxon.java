package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "taxon")
public class Taxon
{

   private int id;

   private String taxonName;

   @XmlAttribute
   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   @XmlValue
   public String getTaxonName()
   {
      return taxonName;
   }

   public void setTaxonName(String taxon)
   {
      this.taxonName = taxon;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Taxon [id=");
      builder.append(id);
      builder.append(", taxonName=");
      builder.append(taxonName);
      builder.append("]");
      return builder.toString();
   }

}
