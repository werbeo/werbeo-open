package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigIndiciaJPA.class, uses = {
      SampleBaseMapperIndicia.class, PersonMapperIndicia.class,
      MediaMapperIndicia.class, TaxonBaseMapperIndicia.class })
public abstract class OccurrenceMapperIndicia
{
   @Autowired
   private OccurrenceMappingHelper mappingHelper;

   @Mapping(target = "numericAmountAccuracy", ignore = true) // handled by
                                                             // additional
                                                             // attribute
   @Mapping(target = "validation", ignore = true)
   @Mapping(target = "absence", source = "zeroAbundance")
   @Mapping(target = "habitat", ignore = true) // handled by additional
                                               // attribute
   @Mapping(target = "allowedOperations", ignore = true) // set later by
                                                         // core-imp
   @Mapping(target = "amount", ignore = true) // handled by additional attribute
   @Mapping(target = "bloomingSprouts", ignore = true) // handled by additional
                                                       // attribute
   @Mapping(target = "coveredArea", ignore = true) // handled by additional
                                                   // attribute
   @Mapping(target = "determinationComment", ignore = true) // handled by
                                                            // additional
                                                            // attribute
   @Mapping(target = "herbarium", ignore = true) // handled by additional
                                                 // attribute
   @Mapping(target = "media", source = "occurrenceMedia")
   @Mapping(target = "numericAmount", ignore = true) // handled by additional
                                                     // attribute
   @Mapping(target = "observers", ignore = true) // handled by additional
                                                 // attribute
   @Mapping(target = "quantity", ignore = true) // handled by additional
                                                // attribute
   @Mapping(target = "reproduction", ignore = true) // handled by additional
                                                    // attribute
   @Mapping(target = "settlementStatus", ignore = true) // handled by additional
                                                        // attribute
   @Mapping(target = "settlementStatusFukarek", ignore = true) // handled by
                                                               // additional
                                                               // attribute
   @Mapping(target = "sex", ignore = true) // handled by additional attribute
   @Mapping(target = "taxon", source = "taxaTaxonList.taxon")
   @Mapping(target = "vitality", ignore = true) // handled by additional
                                                // attribute
   @Mapping(target = "id", ignore = true) // handled by additional attribute
   @Mapping(target = "timeOfDay", ignore = true) // handled by additional attribute
   @Mapping(target = "lifeStage", ignore = true) // handled by additional attribute
   @Mapping(target = "remark", ignore = true) // handled by additional attribute
   @Mapping(target = "makropter", ignore = true) // handled by additional attribute
   public abstract Occurrence map(DBOccurrence jpaOccurrence,
         @org.mapstruct.Context Context context);

   public abstract List<Occurrence> map(List<DBOccurrence> content,
         @org.mapstruct.Context Context context);

   protected RecordStatus map(Character statusCode)
   {
      if (statusCode == null)
      {
         return null;
      } else if (IndiciaOccurrence.COMPLETED.equals(statusCode.toString()))
      {
         return RecordStatus.COMPLETE;
      } else if (IndiciaOccurrence.IN_PROGRESS.equals(statusCode.toString()))
      {
         return RecordStatus.IN_PROGRESS;
      } else
      {
         throw new IllegalArgumentException(
               "Record status code " + statusCode + " not supported");
      }
   }

   @AfterMapping
   public void setAttributes(@MappingTarget Occurrence occurrence,
         DBOccurrence jpaOccurrence, @org.mapstruct.Context Context context)
   {

      mappingHelper.mapAdditionalAttributes(occurrence, jpaOccurrence);
      mappingHelper.addValidator(occurrence.getValidation(), context);
   }

}
