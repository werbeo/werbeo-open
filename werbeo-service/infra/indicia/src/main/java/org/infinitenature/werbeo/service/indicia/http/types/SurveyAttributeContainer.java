package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "survey_attributes")
@XmlAccessorType(XmlAccessType.FIELD)
public class SurveyAttributeContainer
{
   @XmlElement(name = "survey_attribute")
   private List<SurveyAttribute> surveyAttributes = new ArrayList<>();

   public List<SurveyAttribute> getSurveyAttributes()
   {
      return surveyAttributes;
   }

   public void setSurveyAttributes(List<SurveyAttribute> surveyAttributes)
   {
      this.surveyAttributes = surveyAttributes;
   }
}