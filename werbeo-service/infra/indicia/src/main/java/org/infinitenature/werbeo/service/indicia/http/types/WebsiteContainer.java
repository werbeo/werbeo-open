package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "websites")
@XmlAccessorType(XmlAccessType.FIELD)
public class WebsiteContainer
{
   @XmlElement(name = "website")
   private List<Website> websites = new ArrayList<>();

   public List<Website> getWebsites()
   {
      return websites;
   }
}
