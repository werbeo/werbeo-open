package org.infinitenature.werbeo.service.adapters;

import java.util.UUID;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.CommentQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrenceComment;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSampleComment;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.CommentMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.CommentRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceCommentRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SampleCommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.dsl.BooleanExpression;

@Service
public class CommentQueriesAdapter implements CommentQueries
{
   @Autowired
   private SampleCommentRepository sampleCommentRepository;
   @Autowired
   private OccurrenceCommentRepository occurrenceCommentRepository;
   @Autowired
   private CommentMapperIndicia commentMapper;
   @Autowired
   private CommentRequestMapperIndicia requestMapper;
   @Autowired
   private IdQueries idQueries;

   @Transactional(readOnly = true)
   @Override
   public Slice<SampleComment, CommentSortField> findSampleComments(
         UUID sampleId, Context context,
         OffsetRequest<CommentSortField> offsetRequest)
   {
      int indiciaSampleId = idQueries.getIndiciaSampleId(sampleId);
      BooleanExpression predicate = prepareSampleCommentsQuery(indiciaSampleId,
            context);

      return new SliceImpl<>(
            commentMapper.mapSampleComments(sampleCommentRepository
                  .findAll(predicate, requestMapper.map(offsetRequest))
                  .getContent(), sampleId),
            offsetRequest);
   }

   @Transactional(readOnly = true)
   @Override
   public long countSampleComments(UUID sampleId, Context context)
   {
      int indiciaSampleId = idQueries.getIndiciaSampleId(sampleId);
      BooleanExpression predicate = prepareSampleCommentsQuery(indiciaSampleId,
            context);
      return sampleCommentRepository.count(predicate);
   }

   @Transactional(readOnly = true)
   @Override
   public long countOccurrenceComments(UUID occurrenceId, Context context)
   {
      int indiciaOccurrenceId = idQueries.getIndiciaOccurrenceId(occurrenceId);
      return occurrenceCommentRepository.count(
            prepareOccurrenceCommentsQuery(indiciaOccurrenceId, context));
   }

   @Transactional(readOnly = true)
   @Override
   public Slice<OccurrenceComment, CommentSortField> findOccurrenceComments(
         UUID occurrenceId, Context context,
         OffsetRequest<CommentSortField> offsetRequest)
   {
      int indiciaOccurrenceId = idQueries.getIndiciaOccurrenceId(occurrenceId);
      return new SliceImpl<>(
            commentMapper
                  .mapOccurrenceComments(
                        occurrenceCommentRepository
                              .findAll(
                                    prepareOccurrenceCommentsQuery(
                                          indiciaOccurrenceId, context),
                                    requestMapper.map(offsetRequest))
                              .getContent(),
                        occurrenceId),
            offsetRequest);
   }

   private BooleanExpression prepareSampleCommentsQuery(int indiciaSampleId,
         Context context)
   {
      QDBSampleComment dbsamplecomment = QDBSampleComment.dBSampleComment;
      return dbsamplecomment.sampleId.eq(indiciaSampleId)
            .and(dbsamplecomment.deleted.isFalse());
   }

   private BooleanExpression prepareOccurrenceCommentsQuery(
         int indiciaOccurrenceId, Context context)
   {
      QDBOccurrenceComment dboccurrencecomment = QDBOccurrenceComment.dBOccurrenceComment;
      return dboccurrencecomment.occurrenceId.eq(indiciaOccurrenceId)
            .and(dboccurrencecomment.deleted.isFalse());
   }

}
