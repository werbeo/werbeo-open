package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_media")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleMediaContainer
{
   @XmlElement(name = "sample_medium")
   private List<SampleMedium> sampleMedia = new ArrayList<>();

   public List<SampleMedium> getSampleMedia()
   {
      return sampleMedia;
   }

   public void setSampleMedia(List<SampleMedium> sampleMedia)
   {
      this.sampleMedia = sampleMedia;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SampleMediaContainer@");
      builder.append(System.identityHashCode(this));
      builder.append(" [");
      if (sampleMedia != null)
      {
         builder.append("sampleMedia=");
         builder.append(sampleMedia);
      }
      builder.append("]");
      return builder.toString();
   }
}