package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBWebsite;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.Mapper;

@Mapper(config = CentralConfigIndiciaJPA.class)
public interface PortalMapperIndicia
{
   Portal map(DBWebsite jpaWebsite);

   Set<Portal> map(Iterable<DBWebsite> jpaWebsites);

   List<Portal> mapList(Iterable<DBWebsite> content);
}
