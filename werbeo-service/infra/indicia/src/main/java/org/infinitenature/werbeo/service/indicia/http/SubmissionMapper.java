package org.infinitenature.werbeo.service.indicia.http;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaType;
import org.infinitenature.werbeo.service.indicia.http.types.Location;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceComment;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceMedium;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceSubmissionContainer;
import org.infinitenature.werbeo.service.indicia.http.types.Person;
import org.infinitenature.werbeo.service.indicia.http.types.SampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.http.types.SampleComment;
import org.infinitenature.werbeo.service.indicia.http.types.SampleMedium;
import org.infinitenature.werbeo.service.indicia.http.types.SampleSubmissionContainer;
import org.infinitenature.werbeo.service.indicia.http.types.SurveyMedium;
import org.infinitenature.werbeo.service.indicia.http.types.SurveySubmissionContrainer;
import org.infinitenature.werbeo.service.indicia.http.types.submission.Field;
import org.infinitenature.werbeo.service.indicia.http.types.submission.Model;
import org.infinitenature.werbeo.service.indicia.http.types.submission.SubModel;

public class SubmissionMapper
{
   private static final String FIELD_DESCRIPTION = "description";
   private static final String FIELD_COMMENT = "comment";
   private static final String FIELD_OCCURRENCE_ID = "occurrence_id";
   private static final String FIELD_SAMPLE_ID = "sample_id";
   private static final String FIELD_CAPTION = "caption";
   private static final String FIELD_TITLE = "title";
   private static final String FIELD_OWNER_ID = "owner_id";
   private static final String FIELD_PARENT_ID = "parent_id";
   private static final String SURVEY_ID = "survey_id";

   public static Model createModel(Location location)
   {
      Set<Field> fields = new HashSet<>();
      if (location.getId() != 0)
      {
         fields.add(new Field("id", location.getId()));
      }
      if (location.getLocationTypeId() != 0)
      {
         fields.add(
               new Field("location_type_id", location.getLocationTypeId()));
      }
      fields.add(new Field("name", location.getName()));
      fields.add(new Field("code", location.getCode()));
      fields.add(new Field("boundary_geom", location.getBoundaryGeom()));
      return new Model("location", fields);
   }

   public static Model createModel(
         SurveySubmissionContrainer surveySubmissionContrainer, int webSiteId)
   {
      Model surveyModel = SubmissionMapper.createModel(
            surveySubmissionContrainer.getSurvey(),
            surveySubmissionContrainer.getAdditionalAttributes(), webSiteId);
      List<SubModel> subModels = new ArrayList<>();
      for (SurveyMedium sampleMedium : surveySubmissionContrainer.getMedia())
      {
         Model mediaModel = SubmissionMapper.createModel(sampleMedium);
         SubModel mediaSubModel = new SubModel(SURVEY_ID, mediaModel);
         subModels.add(mediaSubModel);
      }
      surveyModel.setSubModels(subModels);
      if (surveySubmissionContrainer.getAdditionalAttributes() != null)
      {
         for (String index : surveySubmissionContrainer
               .getAdditionalAttributes().keySet())
         {
            surveyModel.getFields()
                  .add(new Field("srvAttr:" + index, surveySubmissionContrainer
                        .getAdditionalAttributes().get(index).toString()));
         }
      }

      return surveyModel;
   }

   private static Model createModel(IndiciaSurvey indiciaSurvey,
         Map<String, String> additionalAttributes, int webSiteId)
   {
      Set<Field> fields = new HashSet<>();
      SubmissionMapper.addBaseFields(indiciaSurvey, fields);
      fields.add(new Field(IndiciaApi.KEY_WEBSITE_ID, webSiteId));
      fields.add(new Field(FIELD_TITLE, indiciaSurvey.getTitle()));

      if (StringUtils.isNotBlank(indiciaSurvey.getDescription()))
      {

         fields.add(
               new Field(FIELD_DESCRIPTION, indiciaSurvey.getDescription()));
      }
      if (indiciaSurvey.getParentId() != 0)
      {
         fields.add(new Field(FIELD_PARENT_ID, indiciaSurvey.getParentId()));
      }
      if (indiciaSurvey.getOwner() != null)
      {
         fields.add(
               new Field(FIELD_OWNER_ID, indiciaSurvey.getOwner().getId()));
      }
      return new Model("survey", fields);

   }

   private static Model createModel(SurveyMedium surveyMedium)
   {
      Set<Field> fields = new HashSet<>();
      addBaseFields(surveyMedium, fields);
      fields.add(new Field(FIELD_CAPTION, surveyMedium.getCaption()));
      fields.add(new Field(SURVEY_ID, surveyMedium.getSurveyId()));
      fields.add(new Field("path", surveyMedium.getPath()));
      fields.add(
            new Field("media_type_id", surveyMedium.getMediaType().getId()));
      return new Model("survey_medium", fields);
   }

   public static Model createModel(
         SampleSubmissionContainer sampleSubmissionContainer)
   {
      Model sampleModel = SubmissionMapper.createModel(
            sampleSubmissionContainer.getSample(),
            sampleSubmissionContainer.getAdditionAtributes());
      List<SubModel> subModels = new ArrayList<>();
      for (SampleComment sampleComment : sampleSubmissionContainer
            .getComments())
      {
         Model commentModel = SubmissionMapper.createModel(sampleComment);
         SubModel commentSubModel = new SubModel(FIELD_SAMPLE_ID, commentModel);
         subModels.add(commentSubModel);
      }
      for (SampleMedium sampleMedium : sampleSubmissionContainer.getMedia())
      {
         Model mediaModel = SubmissionMapper.createModel(sampleMedium);
         SubModel mediaSubModel = new SubModel(FIELD_SAMPLE_ID, mediaModel);
         subModels.add(mediaSubModel);
      }
      for (OccurrenceSubmissionContainer occurrenceSubmissionContainer : sampleSubmissionContainer
            .getOccurrenceSubmissionContainers())
      {
         Model occurrenceModel = createModel(occurrenceSubmissionContainer);
         SubModel occurrenceSubModel = new SubModel(FIELD_SAMPLE_ID,
               occurrenceModel);
         subModels.add(occurrenceSubModel);
      }
      sampleModel.setSubModels(subModels);
      return sampleModel;
   }

   public static Model createModel(
         OccurrenceSubmissionContainer occurrenceSubmissionContainer)
   {
      Model occurrenceModel = createModel(
            occurrenceSubmissionContainer.getOccurrence(),
            occurrenceSubmissionContainer.getAdditionAtributes());
      List<SubModel> subModels = new ArrayList<>();
      for (OccurrenceComment occurrenceComment : occurrenceSubmissionContainer
            .getComments())
      {
         Model commentModel = SubmissionMapper.createModel(occurrenceComment);
         SubModel commentSubModel = new SubModel(FIELD_OCCURRENCE_ID,
               commentModel);
         subModels.add(commentSubModel);
      }
      for (OccurrenceMedium occurrenceMedium : occurrenceSubmissionContainer
            .getMedia())
      {
         Model mediaModel = SubmissionMapper.createModel(occurrenceMedium);
         SubModel mediaSubModel = new SubModel(FIELD_OCCURRENCE_ID, mediaModel);
         subModels.add(mediaSubModel);
      }
      occurrenceModel.setSubModels(subModels);
      return occurrenceModel;
   }

   public static Model createModel(IndiciaOccurrence occurrence,
         Map<String, String> additionAtributes)
   {
      Set<Field> fields = new HashSet<>();
      if (occurrence.getWebsiteId() != 0)
      {
         fields.add(new Field("website_id", occurrence.getWebsiteId()));
      }
      addBaseFields(occurrence, fields);
      if (occurrence.getTaxaTaxonListId() != 0)
      {
         fields.add(new Field("taxa_taxon_list_id",
               occurrence.getTaxaTaxonListId()));
      }
      fields.add(new Field(FIELD_SAMPLE_ID, occurrence.getSampleId()));

      if (occurrence.getRecordStatus() != null)
      {
         fields.add(new Field("record_status", occurrence.getRecordStatus()));
      }
      if (occurrence.getDeterminerId() != 0)
      {
         fields.add(new Field("determiner_id", occurrence.getDeterminerId()));
      }
      if (occurrence.getExternalKey() != null)
      {
         fields.add(new Field("external_key", occurrence.getExternalKey()));
      }
      if (occurrence.getComment() != null)
      {
         fields.add(new Field(FIELD_COMMENT, occurrence.getComment()));
      }
      if (additionAtributes != null)
      {
         for (String index : additionAtributes.keySet())
         {
            fields.add(new Field("occAttr:" + index,
                  additionAtributes.get(index)));
         }
      }
      return new Model("occurrence", fields);

   }

   public static Model createModel(OccurrenceComment occurrenceComment)
   {
      Set<Field> fields = new HashSet<>();
      addBaseFields(occurrenceComment, fields);
      fields.add(new Field(FIELD_COMMENT, occurrenceComment.getComment()));
      fields.add(new Field(FIELD_OCCURRENCE_ID,
            occurrenceComment.getOccurrenceId()));
      return new Model("occurrence_comment", fields);

   }

   public static Model createModel(OccurrenceMedium occurrenceMedium)
   {
      Set<Field> fields = new HashSet<>();
      addBaseFields(occurrenceMedium, fields);
      fields.add(new Field(FIELD_CAPTION, occurrenceMedium.getCaption()));
      fields.add(
            new Field(FIELD_OCCURRENCE_ID, occurrenceMedium.getOccurrenceId()));
      fields.add(new Field("path", occurrenceMedium.getPath()));
      fields.add(new Field("media_type_id",
            occurrenceMedium.getMediaType().getId()));
      return new Model("occurrence_medium", fields);
   }

   public static Model createModel(SampleComment sampleComment)
   {
      Set<Field> fields = new HashSet<>();
      addBaseFields(sampleComment, fields);
      fields.add(new Field(FIELD_COMMENT, sampleComment.getComment()));
      fields.add(new Field(FIELD_SAMPLE_ID, sampleComment.getSampleId()));
      return new Model("sample_comment", fields);
   }

   private static Model createModel(SampleMedium sampleMedium)
   {
      Set<Field> fields = new HashSet<>();
      addBaseFields(sampleMedium, fields);
      fields.add(new Field(FIELD_CAPTION, sampleMedium.getCaption()));
      fields.add(new Field(FIELD_SAMPLE_ID, sampleMedium.getSampleId()));
      fields.add(new Field("path", sampleMedium.getPath()));
      return new Model("sample_medium", fields);

   }

   public static Model createModel(Person person,
         Map<String, String> additionalAttributes)
   {
      Set<Field> fields = new HashSet<>();
      addBaseFields(person, fields);
      fields.add(new Field("first_name", person.getFirstName()));
      fields.add(new Field("surname", person.getLastName()));
      if (StringUtils.isNotBlank(person.getEmailAddress()))
      {
         fields.add(new Field("email_address", person.getEmailAddress()));
      }
      if (StringUtils.isNotBlank(person.getInitials()))
      {
         fields.add(new Field("initials", person.getInitials()));
      }
      if (StringUtils.isNotBlank(person.getAddress()))
      {
         fields.add(new Field("address", person.getAddress()));
      }
      if (StringUtils.isNotBlank(person.getExternalKey()))
      {
         fields.add(new Field("external_key", person.getExternalKey()));
      }

      if (additionalAttributes != null)
      {
         for (String index : additionalAttributes.keySet())
         {
            fields.add(new Field("psnAttr:" + index,
                  additionalAttributes.get(index).toString()));
         }
      }
      return new Model("person", fields);
   }

   public static Model createModel(IndiciaSample sample,
         Map<String, String> additionAtributes)
   {
      Set<Field> fields = new HashSet<>();
      addBaseFields(sample, fields);
      fields.add(new Field("website_id", sample.getWebsiteId()));
      fields.add(new Field("record_status", "C"));

      if (StringUtils.isBlank(sample.getDate()))
      {
         if (StringUtils.isNotBlank(sample.getDateType()))
         {
            fields.add(new Field("date_type", sample.getDateType()));
         }
         if (StringUtils.isBlank(sample.getDateType()) || StringUtils
               .equals(IndiciaApi.VAGUE_DATE_DAY, sample.getDateType()))
         {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                  IndiciaApi.INDICIA_DATE_FORMAT);
            fields.add(
                  new Field("date", dateFormat.format(sample.getStartDate())));
         } else if (StringUtils.equals(IndiciaApi.VAGUE_DATE_YEAR,
               sample.getDateType()))
         {
            fields.add(new Field("date", sample.getYear()));
         } else if (StringUtils.equals(IndiciaApi.VAGUE_DATE_MONTH_IN_YEAR,
               sample.getDateType()))
         {

            fields.add(new Field("date",
                  sample.getYear() + "-" + sample.getMonth()));
         } else if (StringUtils.equals(IndiciaApi.VAGUE_DATE_DAYS,
               sample.getDateType()))
         {
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                  IndiciaApi.INDICIA_DATE_FORMAT);
            fields.add(
                  new Field("date", dateFormat.format(sample.getStartDate())
                        + " to " + dateFormat.format(sample.getEndDate())));
         }
      } else
      {
         fields.add(new Field("date", sample.getDate()));
         if (StringUtils.isNotBlank(sample.getDateType()))
         {
            fields.add(new Field("date_type", sample.getDateType()));
         }
      }
      if (StringUtils.isNotBlank(sample.getEnteredSref()))
      {
         fields.add(new Field("entered_sref", sample.getEnteredSref()));
      }
      if (sample.getGeom() != null)
      {
         fields.add(new Field("geom", sample.getGeom()));
      }
      if (StringUtils.isNotBlank(sample.getEnteredSrefSystem()))
      {
         fields.add(
               new Field("entered_sref_system", sample.getEnteredSrefSystem()));
      }
      fields.add(new Field(SURVEY_ID, sample.getSurveyId()));
      if (StringUtils.isNotBlank(sample.getComment()))
      {
         fields.add(new Field(FIELD_COMMENT, sample.getComment()));
      }
      if (StringUtils.isNotBlank(sample.getRecorderNames()))
      {
         fields.add(new Field("recorder_names", sample.getRecorderNames()));
      }
      if (sample.getLocationId() != 0)
      {
         fields.add(new Field("location_id", sample.getLocationId()));
      }
      if (sample.getParentId() != 0)
      {
         fields.add(new Field(FIELD_PARENT_ID, sample.getParentId()));
      }
      if (sample.getSampleMethodId() != 0)
      {
         fields.add(new Field("sample_method_id", sample.getSampleMethodId()));
      }
      if (additionAtributes != null)
      {
         for (String index : additionAtributes.keySet())
         {
            fields.add(new Field("smpAttr:" + index,
                  additionAtributes.get(index).toString()));
         }
      }
      return new Model("sample", fields);
   }

   static void addBaseFields(IndiciaType indiciatype, Set<Field> fields)
   {
      if (indiciatype.getId() != 0)
      {
         fields.add(new Field("id", indiciatype.getId()));
      }
      if (indiciatype.getCreatedById() != 0)
      {
         fields.add(new Field("created_by_id", indiciatype.getCreatedById()));
      }
      if (indiciatype.getUpdatedById() != 0)
      {
         fields.add(new Field("updated_by_id", indiciatype.getUpdatedById()));
      }
      if (indiciatype.isDeleted())
      {
         fields.add(new Field("deleted", "true"));
      }
   }

   public static Model createModel(IndiciaSurvey survey, int webSiteId)
   {
      Set<Field> fields = new HashSet<>();
      SubmissionMapper.addBaseFields(survey, fields);
      fields.add(new Field(IndiciaApi.KEY_WEBSITE_ID, webSiteId));
      fields.add(new Field(FIELD_TITLE, survey.getTitle()));

      if (StringUtils.isNotBlank(survey.getDescription()))
      {
         fields.add(new Field(FIELD_DESCRIPTION, survey.getDescription()));
      }
      if (survey.getParentId() != 0)
      {
         fields.add(new Field(FIELD_PARENT_ID, survey.getParentId()));
      }
      if (survey.getOwner() != null)
      {
         fields.add(new Field(FIELD_OWNER_ID, survey.getOwner().getId()));
      }
      return new Model("survey", fields);
   }

   public static Model createModel(SampleAttributeValue sampleAttributeValue)
   {
      Set<Field> fields = new HashSet<>();
      fields.add(
            new Field(FIELD_SAMPLE_ID, sampleAttributeValue.getSampleId()));
      fields.add(new Field("sample_attribute_id",
            sampleAttributeValue.getSampleAttributeId()));
      fields.add(new Field("value", sampleAttributeValue.getValue()));
      return new Model("sample_attribute_value", fields);
   }

   public static Model createModelInt(
         OccurrenceAttributeValue occurrenceAttributeValue)
   {
      Set<Field> fields = new HashSet<>();
      if (occurrenceAttributeValue.getId() != 0)
      {
         fields.add(new Field("id", occurrenceAttributeValue.getId()));
      }
      fields.add(new Field(FIELD_OCCURRENCE_ID,
            occurrenceAttributeValue.getOccurrenceId()));
      fields.add(new Field("occurrence_attribute_id",
            occurrenceAttributeValue.getOccurrenceAttributeId()));
      if (occurrenceAttributeValue.isIntValue())
      {
         fields.add(new Field("int_value", occurrenceAttributeValue.getValue()));
      } else
      {
         fields.add(
               new Field("text_value", occurrenceAttributeValue.getValue()));
      }

      return new Model("occurrence_attribute_value", fields);
   }

   public static Model createModel(
         OccurrenceAttributeValue occurrenceAttributeValue)
   {
      Set<Field> fields = new HashSet<>();
      if (occurrenceAttributeValue.getId() != 0)
      {
         fields.add(new Field("id", occurrenceAttributeValue.getId()));
      }
      fields.add(new Field(FIELD_OCCURRENCE_ID,
            occurrenceAttributeValue.getOccurrenceId()));
      fields.add(new Field("occurrence_attribute_id",
            occurrenceAttributeValue.getOccurrenceAttributeId()));
      fields.add(new Field("value", occurrenceAttributeValue.getValue()));
      return new Model("occurrence_attribute_value", fields);
   }

}
