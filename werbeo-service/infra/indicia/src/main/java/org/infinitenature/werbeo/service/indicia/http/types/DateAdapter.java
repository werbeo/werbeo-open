package org.infinitenature.werbeo.service.indicia.http.types;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.lang3.time.DateUtils;

public class DateAdapter extends XmlAdapter<String, Date>
{
   private static final String pattern = "yyyy-MM-dd";
   private static final String patternWithTime = "yyyy-MM-dd HH:mm:ss";

   private static final String[] patterns = { patternWithTime, pattern };

   @Override
   public String marshal(Date date) throws Exception
   {
      return new SimpleDateFormat(pattern).format(date);
   }

   @Override
   public Date unmarshal(String dateString) throws Exception
   {
      return DateUtils.parseDate(dateString, patterns);
   }
}