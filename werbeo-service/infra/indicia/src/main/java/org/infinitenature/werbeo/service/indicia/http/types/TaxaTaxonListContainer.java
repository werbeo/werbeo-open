package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "taxa_taxon_lists")
public class TaxaTaxonListContainer
{

   private List<TaxaTaxonList> taxaTaxonLists = new ArrayList<>();

   @XmlElement(name = "taxa_taxon_list")
   public List<TaxaTaxonList> getTaxaTaxonLists()
   {
      return taxaTaxonLists;
   }

   public void setTaxaTaxonLists(List<TaxaTaxonList> taxaTaxonLists)
   {
      this.taxaTaxonLists = taxaTaxonLists;
   }

}
