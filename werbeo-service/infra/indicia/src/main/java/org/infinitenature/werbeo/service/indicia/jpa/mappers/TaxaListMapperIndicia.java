package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxaList;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(config = CentralConfigIndiciaJPA.class)
public interface TaxaListMapperIndicia
{
   TaxaList map(DBTaxaList jpaTaxaList);

   List<TaxaList> map(List<DBTaxaList> content);

   @AfterMapping
   default void mapGroups(@MappingTarget TaxaList taxaList,
         DBTaxaList jpaTaxaList)
   {

   }
}
