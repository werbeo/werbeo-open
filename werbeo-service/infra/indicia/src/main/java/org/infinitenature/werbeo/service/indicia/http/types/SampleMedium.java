package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_medium")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleMedium extends IndiciaTypeImpl implements MediumType
{
   @XmlElement(name = "sample_id")
   private int sampleId;
   @XmlElement
   private String path;
   @XmlElement
   private String caption;
   @XmlElement(name = "media_type")
   private MediaTypeAsSubelement mediaType;

   public SampleMedium()
   {
      super();
   }

   public SampleMedium(String path, String caption)
   {
      super();
      this.path = path;
      this.caption = caption;
   }

   public int getSampleId()
   {
      return sampleId;
   }

   public void setSampleId(int sampleId)
   {
      this.sampleId = sampleId;
   }

   @Override
   public String getPath()
   {
      return path;
   }

   public void setPath(String path)
   {
      this.path = path;
   }

   @Override
   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   @Override
   public MediaTypeAsSubelement getMediaType()
   {
      return mediaType;
   }

   public void setMediaType(MediaTypeAsSubelement mediaType)
   {
      this.mediaType = mediaType;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SampleMedium@");
      builder.append(System.identityHashCode(this));
      builder.append(" [sampleId=");
      builder.append(sampleId);
      builder.append(", ");
      if (path != null)
      {
         builder.append("path=");
         builder.append(path);
         builder.append(", ");
      }
      if (caption != null)
      {
         builder.append("caption=");
         builder.append(caption);
         builder.append(", ");
      }
      if (mediaType != null)
      {
         builder.append("mediaType=");
         builder.append(mediaType);
      }
      builder.append("]");
      return builder.toString();
   }

   @Override
   public String getModelName()
   {
      return "sample_medium";
   }

   @Override
   public int getTargetId()
   {
      return getSampleId();
   }

}