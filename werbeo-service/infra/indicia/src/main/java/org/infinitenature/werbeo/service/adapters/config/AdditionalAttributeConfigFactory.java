package org.infinitenature.werbeo.service.adapters.config;

import org.infinitenature.AdditionalAttributeNames;
import org.infinitenature.werbeo.service.adapters.AdditionalOccurrenceAttributeConfig;
import org.infinitenature.werbeo.service.adapters.AdditionalPersonAttributeConfig;
import org.infinitenature.werbeo.service.adapters.AdditionalSampleAttributeConfig;
import org.infinitenature.werbeo.service.adapters.AdditionalSurveyAttributeConfig;
import org.infinitenature.werbeo.service.adapters.AdditionalTaxonAttributeConfig;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBPersonAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurveyAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxonAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrenceAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBPersonAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSampleAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSurveyAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBTaxonAttribute;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceAttributeRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.PersonAttributesRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SampleAttributeRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SurveyAttributesRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.TaxonAttributeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AdditionalAttributeConfigFactory
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(AdditionalAttributeConfigFactory.class);
   @Autowired
   private SurveyAttributesRepository surveyAttributesRepository;
   @Autowired
   private SampleAttributeRepository sampleAttributeRepository;
   @Autowired
   private OccurrenceAttributeRepository occurrenceAttributeRepository;
   @Autowired
   private TaxonAttributeRepository taxonAttributeRepository;
   @Autowired
   private PersonAttributesRepository personAttributesRepository;

   @Cacheable("indicia-occurrence-attribute")
   public AdditionalOccurrenceAttributeConfig getOccurrenceConfig()
   {
      AdditionalOccurrenceAttributeConfig config = new AdditionalOccurrenceAttributeConfig();
      Iterable<DBOccurrenceAttribute> attributes = occurrenceAttributeRepository
            .findAll(QDBOccurrenceAttribute.dBOccurrenceAttribute.deleted
                  .isFalse());
      attributes.forEach(attribute ->
      {
         Integer attributeId = attribute.getId();
         switch (attribute.getCaption())
         {
         case "Kartennummer":
            config.setCartNumber(attributeId);
            break;
         case "LiteraturCode":
            config.setLitCode(attributeId);
            break;
         case "Aussterbedatum":
            config.setExtinctionDate(attributeId);
            break;
         case "Menge":
            config.setAmount(attributeId);
            break;
         case "LiteraturBemerkung":
            config.setLitRemark(attributeId);
            break;
         case "Status":
            config.setStatus(attributeId);
            break;
         case "Legnam":
            config.setLegName(attributeId);
            break;
         case "UUID":
            config.setUuid(attributeId);
            break;
         case "fk_deck":
            config.setFkDeck(attributeId);
            break;
         case "Coverage min":
            config.setCoverageMin(attributeId);
            break;
         case "Coverage max":
            config.setCoverageMax(attributeId);
            break;
         case "Coverage mean":
            config.setCoverageMean(attributeId);
            break;
         case "layer":
            config.setLayer(attributeId);
            break;
         case "Coverage code":
            config.setCoverageCode(attributeId);
            break;
         case "BedeckteFlaeche":
            config.setCoveredArea(attributeId);
            break;
         case "Vitalitaet":
            config.setVitality(attributeId);
            break;
         case "VorkStatus":
            config.setOccurrenceStatus(attributeId);
            break;
         case "BluehendeSprosse":
            config.setBloomingSprouts(attributeId);
            break;
         case "Anzahl":
            config.setQuantity(attributeId);
            break;
         case "Mitbeobachter":
            config.setObservers(attributeId);
            break;
         case "Herbarium Code":
            config.setHerbariumCode(attributeId);
            break;
         case "Herbarium Nummer":
            config.setHerbary(attributeId);
            break;
         case "determination_comment":
            config.setDeterminationComment(attributeId);
            break;
         case "numeric_amount":
            config.setNumericAmount(attributeId);
            break;
         case "sex":
            config.setSex(attributeId);
            break;
         case "life_stage":
            config.setLifeStage(attributeId);
            break;
         case "makropter":
            config.setMakropter(attributeId);
            break;
         case "reproduction":
            config.setReproduction(attributeId);
            break;
         case "habitat":
            config.setHabitat(attributeId);
            break;
         case "time_of_day":
            config.setTimeOfDay(attributeId);
            break;
         case "validation":
            config.setValidationStatus(attributeId);
            break;
         case "numeric_amount_accuracy":
            config.setNumericAmountAccuracy(attributeId);
            break;
         case "validation_comment":
            config.setValidationComment(attributeId);
            break;
         case "remark":
            config.setRemark(attributeId);
            break;
         case "citeId":
            config.setCiteId(attributeId);
            break;
         case "citeComment":
            config.setCiteComment(attributeId);
            break;
         default:
            LOGGER.info("Ignoring indicia occurrence attribute {} with id {}",
                  attribute.getCaption(), attributeId);
            break;
         }
      });
      return config;
   }

   @Cacheable("indicia-sample-attribute")
   public AdditionalSampleAttributeConfig getSampleConfig()
   {
      AdditionalSampleAttributeConfig config = new AdditionalSampleAttributeConfig();
      Iterable<DBSampleAttribute> attributes = sampleAttributeRepository
            .findAll(QDBSampleAttribute.dBSampleAttribute.deleted.isFalse());
      attributes.forEach(attribute ->
      {
         Integer attributeId = attribute.getId();
         switch (attribute.getCaption())
         {
         case "Unschärfe":
            config.setPrecision(attributeId);
            break;
         case "Status":
            config.setStatus(attributeId);
            break;
         case "Legnam":
            config.setLegnam(attributeId);
            break;
         case "Briefdatum":
            config.setLetterDate(attributeId);
            break;
         case "Ortsbeschreibung":
            config.setLocationDescription(attributeId);
            break;
         case "MTB":
            config.setMtb(attributeId);
            break;
         case "UUID":
            config.setUuid(attributeId);
            break;
         case "Availability":
            config.setAvailability(attributeId);
            break;
         case "TurbovegAttribute":
            config.setTurboeVegAttribute(attributeId);
            break;
         case "Area":
            config.setArea(attributeId);
            break;
         case "Area Min":
            config.setAreaMin(attributeId);
            break;
         case "Area Max":
            config.setAreaMax(attributeId);
            break;
         case "Cover Scale Code":
            config.setCoverScaleCode(attributeId);
            break;
         case "Cover Scale Name":
            config.setCoverScaleName(attributeId);
            break;
         case  "Fundort":
            config.setLocality(attributeId);
            break;
         default:
            break;
         }
      });
      return config;
   }

   @Cacheable("indicia-survey-attribute")
   public AdditionalSurveyAttributeConfig getSurveyConfig()
   {
      AdditionalSurveyAttributeConfig config = new AdditionalSurveyAttributeConfig();
      Iterable<DBSurveyAttribute> attributes = surveyAttributesRepository
            .findAll(QDBSurveyAttribute.dBSurveyAttribute.deleted.isFalse());
      attributes.forEach(attribute ->
      {
         switch (attribute.getCaption())
         {
         case AdditionalAttributeNames.AVAILABILITY:
            config.setAvailabitlityId(attribute.getId());
            break;
         case AdditionalAttributeNames.CONTAINER:
            config.setContainerId(attribute.getId());
            break;
         case AdditionalAttributeNames.OBFUSCATION_POLICIES:
             config.setObfuscationPoliciesId(attribute.getId());
             break;
         case AdditionalAttributeNames.WERBEO_ORIGINAL:
            config.setWerbeoOriginalId(attribute.getId());
            break;
         case AdditionalAttributeNames.ALLOW_DATA_ENTRY:
            config.setAllowDataEntryId(attribute.getId());
            break;
         case AdditionalAttributeNames.TAG:
            config.setTagId(attribute.getId());
            break;
         default:
            break;
         }
      });
      return config;
   }

   @Cacheable("indicia-taxon-attribute")
   public AdditionalTaxonAttributeConfig getTaxonConfig()
   {
      AdditionalTaxonAttributeConfig config = new AdditionalTaxonAttributeConfig();
      Iterable<DBTaxonAttribute> attributes = taxonAttributeRepository
            .findAll(QDBTaxonAttribute.dBTaxonAttribute.deleted.isFalse());
      attributes.forEach(attribute ->
      {
         switch (attribute.getCaption())
         {
         case "red-list-status":
            config.setRedListStatusId(attribute.getId());
            break;
         default:
            break;
         }
      });
      return config;
   }

   public AdditionalPersonAttributeConfig getPersonConfig()
   {
      AdditionalPersonAttributeConfig config = new AdditionalPersonAttributeConfig();
      Iterable<DBPersonAttribute> attributes = personAttributesRepository
            .findAll(QDBPersonAttribute.dBPersonAttribute.deleted.isFalse());
      attributes.forEach(attribute ->
      {
         switch (attribute.getCaption())
         {
         case "Job":
            config.setJobId(attribute.getId());
            break;
         case "Wirkzeitraum":
            config.setWorkingPeriodId(attribute.getId());
            break;
         default:
            break;
         }
      });
      return config;
   }
}
