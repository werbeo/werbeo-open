package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PersonRequestMapperIndicia
      extends RequestMapperIndicia<PersonSortField>
{
   @Override
   default Function<PersonSortField, String[]> getSortFieldMapper()
{
      return personSortField ->
      {
         switch (personSortField)
         {
         case NAME:
            return new String[] { "lastName", "firstName" };
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
