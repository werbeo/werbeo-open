package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigIndiciaJPA.class)
public abstract class ValidationStatusMapperIndicia
{
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;

   @Mapping(target = "status", ignore = true)
   abstract Validation map(DBOccurrenceAttributeValue attributeValue,
         @MappingTarget Validation validation);

   @AfterMapping
   public void setStatus(
         @MappingTarget Validation validation,
         DBOccurrenceAttributeValue attributeValue)
   {
      validation.setStatus(termlistConfigFactory.getTermConfig()
            .getValidationStatus(attributeValue.getIntValue()));
   }
}
