package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBPerson;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigIndiciaJPA.class)
public interface PersonMapperIndicia
{
   @Mapping(target = "email", source = "emailAddress")
   Person map(DBPerson jpaPerson);

   List<Person> map(List<DBPerson> jpaPeople);
}
