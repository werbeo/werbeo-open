package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.GenericGenerator;

import com.querydsl.core.annotations.QueryInit;

@MappedSuperclass
@Filter(name = "notDeletedFilter", condition = "deleted = false")
public abstract class Base implements Serializable
{
   /**
    *
    */
   private static final long serialVersionUID = -826060321557050386L;
   @Id
   @GenericGenerator(name = "indicia-sequence", strategy = "org.infinitenature.werbeo.service.indicia.jpa.IdGenerator")
   @GeneratedValue(generator = "indicia-sequence", strategy = GenerationType.SEQUENCE)
   @Basic(optional = false)
   protected Integer id;
   @Basic(optional = false)
   @NotNull
   @Column(name = "created_on")
   @Temporal(TemporalType.TIMESTAMP)
   protected Date createdOn;
   @Basic(optional = false)
   @NotNull
   @Column(name = "updated_on")
   @Temporal(TemporalType.TIMESTAMP)
   protected Date updatedOn;
   @Basic(optional = false)
   @NotNull
   protected boolean deleted;
   @JoinColumn(name = "created_by_id", referencedColumnName = "id")
   @ManyToOne(optional = false, fetch = FetchType.LAZY)
   @QueryInit("person.emailAddress")
   protected DBUser createdById;
   @JoinColumn(name = "updated_by_id", referencedColumnName = "id")
   @ManyToOne(optional = false, fetch = FetchType.LAZY)
   protected DBUser updatedById;

   public Integer getId()
   {
      return id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   public Date getCreatedOn()
   {
      return createdOn;
   }

   public void setCreatedOn(Date createdOn)
   {
      this.createdOn = createdOn;
   }

   public Date getUpdatedOn()
   {
      return updatedOn;
   }

   public void setUpdatedOn(Date updatedOn)
   {
      this.updatedOn = updatedOn;
   }

   public boolean isDeleted()
   {
      return deleted;
   }

   public void setDeleted(boolean deleted)
   {
      this.deleted = deleted;
   }

   public DBUser getCreatedById()
   {
      return createdById;
   }

   public void setCreatedById(DBUser createdById)
   {
      this.createdById = createdById;
   }

   public DBUser getUpdatedById()
   {
      return updatedById;
   }

   public void setUpdatedById(DBUser updatedById)
   {
      this.updatedById = updatedById;
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((id == null) ? 0 : id.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (obj == null)
      {
         return false;
      }
      if (getClass() != obj.getClass())
      {
         return false;
      }
      Base other = (Base) obj;
      if (id == null)
      {
         if (other.id != null)
         {
            return false;
         }
      } else if (!id.equals(other.id))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append(this.getClass().getName());
      builder.append("@");
      builder.append(System.identityHashCode(this));
      builder.append("[id=");
      builder.append(id);
      builder.append("]");
      return builder.toString();
   }

}