package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.locationtech.jts.geom.Geometry;

@Entity
@Table(name = "samples")
public class DBSample extends Base
      implements HasVagueDate, HasSampleAttributeValues, HasPosition
{

   @OneToMany(mappedBy = "sample")
   private Collection<DBOccurrence> occurrences;

   @JoinColumn(name = "survey_id", referencedColumnName = "id")
   @ManyToOne
   private DBSurvey survey;

   @OneToMany(mappedBy = "sample")
   private Set<DBSampleAttributeValue> sampleAttributeValues;
   @Column(name = "recorder_names")
   private String recorderNames;
   @Column(name = "entered_sref")
   private String enteredSref;
   @Column(name = "entered_sref_system")
   private String enteredSrefSystem;
   @Column(name = "geom")
   private Geometry geometry;
   @Column(name = "date_start")
   private LocalDate startDate;
   @Column(name = "date_end")
   private LocalDate endDate;
   @Column(name = "date_type")
   private String dateType;
   @Column(name = "sample_method_id")
   private Integer sampleMethodId;

   public DBSurvey getSurvey()
   {
      return survey;
   }

   @Override
   public LocalDate getStartDate()
   {
      return startDate;
   }

   public void setStartDate(LocalDate startDate)
   {
      this.startDate = startDate;
   }

   @Override
   public LocalDate getEndDate()
   {
      return endDate;
   }

   public void setEndDate(LocalDate endDate)
   {
      this.endDate = endDate;
   }

   @Override
   public String getDateType()
   {
      return dateType;
   }

   public void setDateType(String dateType)
   {
      this.dateType = dateType;
   }

   public void setSurvey(DBSurvey survey)
   {
      this.survey = survey;
   }

   public Collection<DBOccurrence> getOccurrences()
   {
      return occurrences;
   }

   public void setOccurrences(Collection<DBOccurrence> occurrences)
   {
      this.occurrences = occurrences;
   }

   @Override
   public Set<DBSampleAttributeValue> getSampleAttributeValues()
   {
      return sampleAttributeValues;
   }

   public void setSampleAttributeValues(
         Set<DBSampleAttributeValue> sampleAttributeValues)
   {
      this.sampleAttributeValues = sampleAttributeValues;
   }

   public String getRecorderNames()
   {
      return recorderNames;
   }

   public void setRecorderNames(String recorderNames)
   {
      this.recorderNames = recorderNames;
   }

   @Override
   public String getEnteredSref()
   {
      return enteredSref;
   }

   public void setEnteredSref(String enteredSref)
   {
      this.enteredSref = enteredSref;
   }

   @Override
   public String getEnteredSrefSystem()
   {
      return enteredSrefSystem;
   }

   public void setEnteredSrefSystem(String enteredSrefSystem)
   {
      this.enteredSrefSystem = enteredSrefSystem;
   }

   @Override
   public Geometry getGeometry()
   {
      return geometry;
   }

   public void setGeometry(Geometry geometry)
   {
      this.geometry = geometry;
   }

   public Integer getSampleMethodId()
   {
      return sampleMethodId;
   }

   public void setSampleMethodId(Integer sampleMethodId)
   {
      this.sampleMethodId = sampleMethodId;
   }

}
