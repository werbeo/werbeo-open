package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "languages")
public class DBLanguage extends Base
{
   private String iso;
   private String language;

   public String getIso()
   {
      return iso;
   }

   public void setIso(String iso)
   {
      this.iso = iso;
   }

   public String getLanguage()
   {
      return language;
   }

   public void setLanguage(String language)
   {
      this.language = language;
   }
}
