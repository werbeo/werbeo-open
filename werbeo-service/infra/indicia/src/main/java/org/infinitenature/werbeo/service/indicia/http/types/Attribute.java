package org.infinitenature.werbeo.service.indicia.http.types;

public interface Attribute
{
   public int getId();

   public String getCaption();

}
