package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "taxon_attributes_websites")
public class DBTaxonAttributesWebsites extends DBAbstractAttributesWebsites
{
   private static final long serialVersionUID = 1L;
   @JoinColumn(name = "taxon_attribute_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private DBTaxonAttribute taxonAttribute;
   public DBTaxonAttribute getTaxonAttribute()
   {
      return taxonAttribute;
   }
   public void setTaxonAttribute(DBTaxonAttribute taxonAttribute)
   {
      this.taxonAttribute = taxonAttribute;
   }


}