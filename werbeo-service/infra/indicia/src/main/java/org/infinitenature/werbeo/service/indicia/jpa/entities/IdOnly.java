package org.infinitenature.werbeo.service.indicia.jpa.entities;

public class IdOnly
{
   private Integer id;

   public Integer getId()
   {
      return id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("IdOnly [id=").append(id).append("]");
      return builder.toString();
   }

}
