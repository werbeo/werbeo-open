package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrence_comments")
@XmlAccessorType(XmlAccessType.FIELD)
public class OccurrenceCommentContainer
{
   @XmlElement(name = "occurrence_comment")
   private List<OccurrenceComment> occurrenceComments = new ArrayList<>();

   public List<OccurrenceComment> getOccurrenceComments()
   {
      return occurrenceComments;
   }

   public void setOccurrenceComments(List<OccurrenceComment> occurrenceComments)
   {
      this.occurrenceComments = occurrenceComments;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("OccurrenceCommentContainer@");
      builder.append(System.identityHashCode(this));
      builder.append(" [occurrenceComments=");
      builder.append(occurrenceComments);
      builder.append("]");
      return builder.toString();
   }
}