package org.infinitenature.werbeo.service.adapters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

/**
 * http://indicia-docs.readthedocs.io/en/latest/developing/web-services/submission-format.html#sending-custom-attributes
 * 
 * @author dve
 *
 */
public class CustomAttributeMerger
{

   public static Map<String, String> merge(Map<String, String> newAttributes,
         Map<String, String> existingAttriubtes)
   {

      List<Attribute> flatNewAttr = newAttributes.entrySet().stream()
            .map(Attribute::new).collect(Collectors.toList());
      List<Attribute> flatExistingAttr = existingAttriubtes.entrySet().stream()
            .map(Attribute::new).collect(Collectors.toList());
      HashMap<String, String> mergedAttributes = new HashMap<>();

      flatNewAttr.forEach(newAttr ->
      {
         // has not changed - same attributeId and value exists
         Optional<Attribute> sameAttribute = flatExistingAttr.stream()
               .filter(existingAttr -> StringUtils.equals(
                     existingAttr.getAttributeId(), newAttr.getAttributeId())
                     && StringUtils.equals(existingAttr.getValue(),
                           newAttr.getValue()))
               .findFirst();
         if (sameAttribute.isPresent())
         {
            mergedAttributes.put(sameAttribute.get().getKey(),
                  sameAttribute.get().getValue());
            flatExistingAttr.remove(sameAttribute.get());
         } else
         {
            Optional<Attribute> idToOverride = flatExistingAttr.stream()
                  .filter(existingAttr -> StringUtils.equals(
                        existingAttr.getAttributeId(),
                        newAttr.getAttributeId()))
                  .findFirst();
            if (idToOverride.isPresent())
            {
               mergedAttributes.put(idToOverride.get().getKey(),
                     newAttr.getValue());
               flatExistingAttr.remove(idToOverride.get());
            } else
            {
               mergedAttributes.put(newAttr.getKey(), newAttr.getValue());
            }
         }
      });
      flatExistingAttr
            .forEach(toDelete -> mergedAttributes.put(toDelete.getKey(), ""));
      return mergedAttributes;
   }
}
