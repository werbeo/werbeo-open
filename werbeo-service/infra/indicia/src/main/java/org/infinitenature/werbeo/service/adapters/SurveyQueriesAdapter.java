package org.infinitenature.werbeo.service.adapters;

import static org.infinitenature.werbeo.service.adapters.QueriesUtil.createWebsiteAgreementQuery;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.infinitenature.werbeo.service.core.api.infra.query.SurveyQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSurvey;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.SurveyMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.SurveyRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class SurveyQueriesAdapter implements SurveyQueries
{
   @Autowired
   private SurveyRepository repository;
   @Autowired
   private SurveyRequestMapperIndicia requestMapper;
   @Autowired
   private SurveyMapperIndicia surveyMapper;

   @Transactional(readOnly = true)
   @Override
   public long countSurveys(String title, String titleContains,
         Integer parentSurveyId,
         Context context)
   {
      return repository
            .count(prepareQuery(title, titleContains, parentSurveyId, context));
   }

   @Override
   @Transactional(readOnly = true)
   @Cacheable(cacheNames = "surveys")
   public Slice<Survey, SurveySortField> findSurveys(String title,
         String titleContains,
         Integer parentSurveyId,
         Context context, OffsetRequest<SurveySortField> offsetRequest)
   {
      BooleanExpression predicate = prepareQuery(title, titleContains,
            parentSurveyId,
            context);

      return new SliceImpl<>(surveyMapper.map(repository
            .findAll(predicate, requestMapper.map(offsetRequest)).getContent()),
            offsetRequest);
   }

   @Override
   @Transactional(readOnly = true)
   @Cacheable(cacheNames = "surveys")
   public Survey load(int id, Context context)
   {
      QDBSurvey qSurveys = QDBSurvey.dBSurvey;
      return surveyMapper.map(repository.findOne(qSurveys.id.eq(id)
            .and(qSurveys.deleted.isFalse().and(qSurveys.website
                  .in(createWebsiteAgreementQuery(context)))))
            .orElse(null));
   }

   protected BooleanExpression prepareQuery(String title,
         String titleContains, Integer parentSurveyId, Context context)
   {
      QDBSurvey qSurveys = QDBSurvey.dBSurvey;
      BooleanExpression predicate = qSurveys.deleted.isFalse()
            .and(qSurveys.website.in(createWebsiteAgreementQuery(context)));

      if (StringUtils.isNotBlank(titleContains))
      {
         BooleanExpression expression = qSurveys.title
               .containsIgnoreCase(titleContains);
         predicate = predicate.and(expression);
      }
      if (StringUtils.isNotBlank(title))
      {
         BooleanExpression expression = qSurveys.title.eq(title);
         predicate = predicate.and(expression);
      }
      if (parentSurveyId != null)
      {
         predicate = predicate.and(qSurveys.parentId.id.eq(parentSurveyId));
      }
      return predicate;
   }

}
