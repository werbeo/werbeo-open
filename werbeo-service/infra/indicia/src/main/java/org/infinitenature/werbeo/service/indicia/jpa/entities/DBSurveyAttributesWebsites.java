package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "survey_attributes_websites")
public class DBSurveyAttributesWebsites extends DBAbstractAttributesWebsites
      implements Serializable
{

   private static final long serialVersionUID = 1L;
   @JoinColumn(name = "survey_attribute_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private DBSurveyAttribute surveyAttribute;

   public DBSurveyAttributesWebsites()
   {
   }

   public DBSurveyAttributesWebsites(Integer id)
   {
      this.id = id;
   }

   public DBSurveyAttributesWebsites(Integer id, Date createdOn,
         boolean deleted, int weight)
   {
      this.id = id;
      this.createdOn = createdOn;
      this.deleted = deleted;
      setWeight(weight);
   }

   public DBSurveyAttribute getSurveyAttribute()
   {
      return surveyAttribute;
   }

   public void setSurveyAttribute(DBSurveyAttribute surveyAttribute)
   {
      this.surveyAttribute = surveyAttribute;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBSurveyAttributesWebsites))
      {
         return false;
      }
      DBSurveyAttributesWebsites other = (DBSurveyAttributesWebsites) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.SurveyAttributesWebsites[ id=" + id
            + " ]";
   }

}
