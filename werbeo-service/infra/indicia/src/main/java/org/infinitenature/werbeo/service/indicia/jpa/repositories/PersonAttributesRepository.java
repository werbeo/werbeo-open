package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBPersonAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PersonAttributesRepository
      extends JpaRepository<DBPersonAttribute, Integer>,
      QuerydslPredicateExecutor<DBPersonAttribute>
{

}
