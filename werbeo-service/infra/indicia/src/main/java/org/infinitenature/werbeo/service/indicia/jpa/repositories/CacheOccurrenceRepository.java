package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.util.List;

import org.infinitenature.werbeo.service.indicia.jpa.entities.cache.CacheOccurrence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CacheOccurrenceRepository
      extends JpaRepository<CacheOccurrence, Integer>,
      QuerydslPredicateExecutor<CacheOccurrence>
{
   public List<CacheOccurrence> findBySampleId(int sampleId);
}
