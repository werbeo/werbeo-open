package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "taxa_taxon_list_attribute_values")
public class DBTaxonAttributeValue extends DBAbstractAttributeValue
      implements Serializable
{
   private static final long serialVersionUID = 1L;

   @JoinColumn(name = "taxa_taxon_list_attribute_id", referencedColumnName = "id")
   @ManyToOne
   private DBTaxonAttribute taxonAttribute;
   @JoinColumn(name = "taxa_taxon_list_id", referencedColumnName = "id")
   @ManyToOne
   private DBTaxaTaxonList taxaTaxonList;


   public DBTaxaTaxonList getTaxaTaxonList()
   {
      return taxaTaxonList;
   }

   public void setTaxaTaxonList(DBTaxaTaxonList taxaTaxonList)
   {
      this.taxaTaxonList = taxaTaxonList;
   }

   public DBTaxonAttribute getTaxonAttribute()
   {
      return taxonAttribute;
   }

   public void setTaxonAttribute(DBTaxonAttribute taxonAttribute)
   {
      this.taxonAttribute = taxonAttribute;
   }

   @Override
   public Base getEntity()
   {
      return getTaxaTaxonList();
   }

   @Override
   public DBAbstractAttribute getAttribute()
   {
      return getTaxonAttribute();
   }
}
