package org.infinitenature.werbeo.service.indicia.http.http.crypt;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.LoggerFactory;

/**
 * En- and decrpyts messages for the indicia user auth servcie
 *
 * @author dve
 *
 */
public class Crypter
{
   private static final org.slf4j.Logger logger = LoggerFactory
         .getLogger(Crypter.class);

   public String seal(String secret, String websitePassword)
   {
      String plain = PhpWrapper.sha1(secret) + ":" + secret;
      return encrypt(plain, PhpWrapper.sha1(websitePassword));
   }

   /**
    *
    * @param secret
    * @param key
    * @return BASE64 encoded and encrypted String
    */
   String encrypt(String secret, String key)
   {
      if (key.length() > 16)
      {
         key = key.substring(0, 16);
      }
      PhpRijndael128 mcrypt = new PhpRijndael128(key.getBytes());

      return Base64.encodeBase64String(mcrypt.encrypt(secret.getBytes()))
            .trim();
   }

   public String unseal(String encrypted, String websitePassword)
   {
      String plain = decrypt(encrypted, PhpWrapper.sha1(websitePassword));
      int index = plain.indexOf(":");
      String hash = plain.substring(0, index);
      String json = plain.substring(index + 1);
      if (!hash.equals(PhpWrapper.sha1(json)))
      {
         logger.error("Hash not correct. Message corrupted");
      }
      logger.debug("Unseald Message: " + json);
      return json;
   }

   /**
    *
    * @param encrypted
    *           BASE64 encoded and encrypted String
    * @param key
    * @return
    */
   String decrypt(String encrypted, String key)
   {
      if (key.length() > 16)
      {
         key = key.substring(0, 16);
      }
      PhpRijndael128 mcrypt = new PhpRijndael128(key.getBytes());
      return new String(mcrypt.decrypt(Base64.decodeBase64(encrypted))).trim();
   }
}
