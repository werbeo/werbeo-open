package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBWebsite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PortalRepository extends JpaRepository<DBWebsite, Integer>,
      QuerydslPredicateExecutor<DBWebsite>
{

}
