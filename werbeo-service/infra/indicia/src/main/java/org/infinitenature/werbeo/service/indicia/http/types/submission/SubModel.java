package org.infinitenature.werbeo.service.indicia.http.types.submission;

import external.org.json.JSONException;
import external.org.json.JSONObject;

public class SubModel
{
   private String fkId;
   private Model model;

   public SubModel(String fkId, Model model)
   {
      super();
      this.fkId = fkId;
      this.model = model;
   }

   public JSONObject getJsonObject() throws JSONException
   {
      JSONObject subModel = new JSONObject();
      subModel.put("fkId", fkId);
      subModel.put("model", model.getJsonObject());
      return subModel;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SubModel [fkId=");
      builder.append(fkId);
      builder.append(", model=");
      builder.append(model);
      builder.append("]");
      return builder.toString();
   }
}
