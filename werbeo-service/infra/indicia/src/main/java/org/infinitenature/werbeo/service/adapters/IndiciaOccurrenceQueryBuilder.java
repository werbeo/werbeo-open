package org.infinitenature.werbeo.service.adapters;

import static org.infinitenature.werbeo.service.adapters.QueriesUtil.createWebsiteAgreementQuery;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.error.ValidationException;
import org.infinitenature.werbeo.service.core.api.infra.query.AbstractOccurrenceQueryBuilder;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter.WKTMode;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSurveyAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBTaxaTaxonList;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.TaxonRepository;
import org.infintenature.mtb.MTB;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.opengis.feature.IllegalAttributeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.togglz.core.manager.FeatureManager;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;

@Component
public class IndiciaOccurrenceQueryBuilder
      extends AbstractOccurrenceQueryBuilder<BooleanExpression>
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(IndiciaOccurrenceQueryBuilder.class);

   private QDBOccurrence qOccurrences = QDBOccurrence.dBOccurrence;

   @Autowired
   private TaxonRepository taxonRepository;
   @Autowired
   private AdditionalAttributeConfigFactory configFactory;
   @Autowired
   private TermlistConfigFactory termlistConfigFacotry;
   @Autowired
   private FeatureManager featureManager;

   @Override
   protected BooleanExpression appendFrom(LocalDate from,
         BooleanExpression query)
   {
      BooleanExpression expression = qOccurrences.sample.startDate
            .after(from.minusDays(1));
      return query.and(expression);
   }

   @Override
   protected BooleanExpression appendMaxBlur(int maxBlur,
         BooleanExpression query)
   {
      int blurAttributeId = configFactory.getSampleConfig().getPrecision();

      QDBSampleAttributeValue sampleAttribute = qOccurrences.sample.sampleAttributeValues
            .any();
      return query.and(sampleAttribute.intValue.lt(maxBlur + 1)
            .and(sampleAttribute.deleted.isFalse())
            .and(sampleAttribute.sampleAttribute.id.eq(blurAttributeId)));
   }

   @Override
   protected BooleanExpression appendHerbaryCode(String herbaryCode,
         BooleanExpression query)
   {
      int herbaryCodeId = configFactory.getOccurrenceConfig()
            .getHerbariumCode();
      QDBOccurrenceAttributeValue occurrenceAttributeValue = qOccurrences.occurrenceAttributeValues
            .any();
      return query.and(occurrenceAttributeValue.textValue.eq(herbaryCode)
            .and(occurrenceAttributeValue.deleted.isFalse())
            .and(occurrenceAttributeValue.occurrenceAttribute.id
                  .eq(herbaryCodeId)));
   }

   @Override
   protected BooleanExpression appendValidationStatus(
         ValidationStatus validationStatus, BooleanExpression query)
   {
      int validationStatusId = configFactory.getOccurrenceConfig()
            .getValidationStatus();

      QDBOccurrenceAttributeValue occurrenceAttributeValue = qOccurrences.occurrenceAttributeValues
            .any();
      return query.and(occurrenceAttributeValue.intValue
            .eq(termlistConfigFacotry.getTermConfig().getId(validationStatus))
            .and(occurrenceAttributeValue.deleted.isFalse())
            .and(occurrenceAttributeValue.occurrenceAttribute.id
                  .eq(validationStatusId)));
   }

   @Override
   protected BooleanExpression appendValidator(String validator,
         BooleanExpression query)
   {
      int validationStatusId = configFactory.getOccurrenceConfig()
            .getValidationStatus();

      QDBOccurrenceAttributeValue occurrenceAttributeValue = qOccurrences.occurrenceAttributeValues
            .any();
      return query.and(occurrenceAttributeValue.createdById.person.emailAddress
            .eq(validator).and(occurrenceAttributeValue.deleted.isFalse())
            .and(occurrenceAttributeValue.occurrenceAttribute.id
                  .eq(validationStatusId)));
   }

   @Override
   protected BooleanExpression appendOwner(String owner,
         BooleanExpression query)
   {
      return query.and(qOccurrences.createdById.person.emailAddress.eq(owner));
   }

   @Override
   protected BooleanExpression appendOccurrenceExternalKey(
         String occExternalKey, BooleanExpression query)
   {
      return query
            .and(qOccurrences.externalKey.contains(occExternalKey));
   }

   @Override
   protected BooleanExpression appendTaxon(Integer taxonId,
         boolean includeChildTaxa, BooleanExpression query)
   {
      BooleanExpression expression;
      if (!includeChildTaxa)
      {
         QDBTaxaTaxonList qTaxaTaxonList = new QDBTaxaTaxonList("ttl");
         JPQLQuery<Integer> taxaMeaningIdsClause = JPAExpressions
               .select(qTaxaTaxonList.taxonMeaningId).from(qTaxaTaxonList)
               .where(qTaxaTaxonList.id.eq(taxonId));
         expression = qOccurrences.taxaTaxonList.taxonMeaningId
               .in(taxaMeaningIdsClause);
      } else
      {
         expression = qOccurrences.taxaTaxonList.taxonMeaningId
               .in(taxonRepository.findChildTaxonMeaningIds(taxonId));
      }
      return query.and(expression);
   }

   @Override
   protected BooleanExpression appendTo(LocalDate to, BooleanExpression query)
   {
      BooleanExpression expression = qOccurrences.sample.endDate
            .before(to.plusDays(1));
      return query.and(expression);
   }

   @Override
   protected BooleanExpression appendWebsiteAgreements(Context context,
         BooleanExpression query)
   {
      return query.and(qOccurrences.sample.survey.website
            .in(createWebsiteAgreementQuery(context)));
   }

   @Override
   protected BooleanExpression appendWkt(String wkt, WKTMode wktMode,
         BooleanExpression query)
   {
      try
      {
         WKTReader parser = new WKTReader(
               new GeometryFactory(new PrecisionModel(), 4326));
         if (wktMode == WKTMode.WITHIN)
         {
            return query.and(qOccurrences.sample.geometry.transform(4326)
                  .within(parser.read(wkt)));
         } else if (wktMode == WKTMode.INTERSECTS)
         {
            return query.and(qOccurrences.sample.geometry.transform(4326)
                  .intersects(parser.read(wkt)));
         } else
         {
            throw new IllegalAttributeException(
                  "Illegal value for WKTMode. Must be INTERSETCS or WITHIN. Value was: "
                        + wktMode);
         }
      } catch (ParseException e)
      {
         LOGGER.error("Failure creating WHERE clause for WKT {}", wkt, e);

         throw new ValidationException("Failure parsinng WKT " + wkt,
               Collections.emptySet());
      }
   }

   @Override
   protected BooleanExpression appendSurveyId(int surveyId,
         BooleanExpression query)
   {
      BooleanExpression expression = qOccurrences.sample.survey.id.eq(surveyId);
      return query.and(expression);
   }

   @Override
   protected BooleanExpression appendPortalBasedFilters(Context context,
         Map<Filter, Collection<Portal>> portalBasedFilters,
         BooleanExpression query)
   {
      Collection<Portal> onlyOwnDataPortals = portalBasedFilters
            .getOrDefault(Filter.ONLY_OWN_DATA, Collections.emptySet());
      int availabitlityAttributeId = configFactory.getSurveyConfig()
            .getAvailabitlityId();
      for (Portal portal : onlyOwnDataPortals)
      {
         QDBSurveyAttributeValue surveyAttValue = QDBSurveyAttributeValue.dBSurveyAttributeValue;
         JPQLQuery<Integer> where = JPAExpressions.select(surveyAttValue.id)
               .from(surveyAttValue)
               .where(surveyAttValue.deleted.isFalse()
                     .and(surveyAttValue.surveyAttribute.id
                           .eq(availabitlityAttributeId))
                     .and(surveyAttValue.textValue.eq("FREE")));

         BooleanExpression fromProtalOnlyOwnData = qOccurrences.sample.survey.website.id
               .ne(portal.getId())
               .or(qOccurrences.createdById.person.emailAddress
                     .eq(context.getUser().getLogin()));

         fromProtalOnlyOwnData = fromProtalOnlyOwnData
               .or(qOccurrences.sample.survey.surveyAttributeValuesCollection
                     .any().id.in(where));

         query = query.and(fromProtalOnlyOwnData);
      }
      return query;
   }

   @Override
   protected BooleanExpression appendMTB(MTB mtb, BooleanExpression query)
   {
      int mtbAttributeId = configFactory.getSampleConfig().getMtb();

      QDBSampleAttributeValue sampleAttribute = qOccurrences.sample.sampleAttributeValues
            .any();
      return query.and(sampleAttribute.textValue.startsWith(mtb.getMtb())
            .and(sampleAttribute.deleted.isFalse()
                  .and(sampleAttribute.sampleAttribute.id.eq(mtbAttributeId))));
   }

   @Override
   protected BooleanExpression appendAbsence(boolean absence,
         BooleanExpression query)
   {
      return query.and(qOccurrences.zeroAbundance.eq(absence));
   }

   @Override
   protected BooleanExpression appendModifiedSince(LocalDateTime modifiedSince,
         BooleanExpression query)
   {
      java.util.Date date = Date.from(
            modifiedSince.atZone(ZoneId.of("Europe/Berlin")).toInstant());
      return query.and(qOccurrences.updatedOn.after(date)
            .or(qOccurrences.updatedOn.eq(date)));
   }

   @Override
   protected BooleanExpression initQuery(Context context)
   {
      return qOccurrences.deleted.isFalse();
   }

}
