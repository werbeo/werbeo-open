package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryInit;

@Entity
@Table(name = "occurrences")
@NamedEntityGraph(name = "graph.occurrenceFind", attributeNodes = {
      @NamedAttributeNode(value = "createdById", subgraph = "user"),
      @NamedAttributeNode(value = "updatedById", subgraph = "user"),
      @NamedAttributeNode(value = "determiner", subgraph = "determiner"),
      @NamedAttributeNode(value = "occurrenceAttributeValues", subgraph = "occurrenceAttributeValues"),
      @NamedAttributeNode(value = "taxaTaxonList"),
      @NamedAttributeNode(value = "sample", subgraph = "sample") }, subgraphs = {
            @NamedSubgraph(name = "occurrenceAttributeValues", attributeNodes = {
                  @NamedAttributeNode(value = "occurrenceAttribute") }),
            @NamedSubgraph(name = "determiner", attributeNodes = {
                  @NamedAttributeNode(value = "createdById", subgraph = "user") }),
            @NamedSubgraph(name = "user", attributeNodes = {
                  @NamedAttributeNode("person") }),
            @NamedSubgraph(name = "sample", attributeNodes = {
                  @NamedAttributeNode(value = "survey", subgraph = "survey"),
                  @NamedAttributeNode(value = "sampleAttributeValues", subgraph = "sampleAttributeValues") }),
            @NamedSubgraph(name = "sampleAttributeValues", attributeNodes = {
                  @NamedAttributeNode(value = "sampleAttribute") }),
            @NamedSubgraph(name = "survey", attributeNodes = {
                  @NamedAttributeNode("website"),
                  @NamedAttributeNode(value = "surveyAttributeValuesCollection", subgraph = "surveyAttributeValuesCollection") }),
            @NamedSubgraph(name = "surveyAttributeValuesCollection", attributeNodes = {
                  @NamedAttributeNode(value = "surveyAttribute") }),
            @NamedSubgraph(name = "taxaTaxonList", attributeNodes = {
                  @NamedAttributeNode(value = "taxon") }) })
public class DBOccurrence extends Base implements HasOccurrenceAttributeValues
{

   private String comment;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "determiner_id", referencedColumnName = "id")
   private DBPerson determiner;

   private String externalKey;

   private Character recordStatus;

   @JoinColumn(name = "sample_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   @QueryInit("survey.website")
   private DBSample sample;

   @JoinColumn(name = "taxa_taxon_list_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   @QueryInit("taxon")
   private DBTaxaTaxonList taxaTaxonList;

   @OneToMany(mappedBy = "occurrence", fetch = FetchType.LAZY)
   private Set<DBOccurrenceAttributeValue> occurrenceAttributeValues;

   @OneToMany(mappedBy = "occurrence")
   private Set<DBOccurrenceMedia> occurrenceMedia;

   private boolean zeroAbundance = false;

   public String getComment()
   {
      return comment;
   }

   public DBPerson getDeterminer()
   {
      return determiner;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public Character getRecordStatus()
   {
      return recordStatus;
   }

   public DBSample getSample()
   {
      return sample;
   }

   public DBTaxaTaxonList getTaxaTaxonList()
   {
      return taxaTaxonList;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public void setDeterminer(DBPerson determiner)
   {
      this.determiner = determiner;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public void setRecordStatus(Character recordStatus)
   {
      this.recordStatus = recordStatus;
   }

   public void setSample(DBSample sample)
   {
      this.sample = sample;
   }

   public void setTaxaTaxonList(DBTaxaTaxonList taxaTaxonList)
   {
      this.taxaTaxonList = taxaTaxonList;
   }

   @Override
   public Set<DBOccurrenceAttributeValue> getOccurrenceAttributeValues()
   {
      return occurrenceAttributeValues;
   }

   public void setOccurrenceAttributeValues(
         Set<DBOccurrenceAttributeValue> occurrenceAttributeValues)
   {
      this.occurrenceAttributeValues = occurrenceAttributeValues;
   }

   public Set<DBOccurrenceMedia> getOccurrenceMedia()
   {
      return occurrenceMedia;
   }

   public void setOccurrenceMedia(Set<DBOccurrenceMedia> occurrenceMedia)
   {
      this.occurrenceMedia = occurrenceMedia;
   }

   public boolean isZeroAbundance()
   {
      return zeroAbundance;
   }

   public void setZeroAbundance(boolean zeroAbundance)
   {
      this.zeroAbundance = zeroAbundance;
   }

}
