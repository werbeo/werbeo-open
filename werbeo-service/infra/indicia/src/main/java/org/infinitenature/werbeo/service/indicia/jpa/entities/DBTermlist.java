package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "termlists")
public class DBTermlist extends Base
{
   @Column(name = "title")
   private String title;
   @Column(name = "description")
   private String description;

   @OneToMany(mappedBy = "termlist")
   private List<DBTermlistTerm> termListTerms = new ArrayList<>();

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public List<DBTermlistTerm> getTermListTerms()
   {
      return termListTerms;
   }

   public void setTermListTerms(List<DBTermlistTerm> termListTerms)
   {
      this.termListTerms = termListTerms;
   }

}
