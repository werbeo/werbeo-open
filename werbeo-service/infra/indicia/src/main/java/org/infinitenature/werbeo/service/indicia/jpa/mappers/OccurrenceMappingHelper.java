package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbeo.service.adapters.AdditionalOccurrenceAttributeConfig;
import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.adapters.config.TermConfig;
import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Herbarium;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasOccurrenceAttributeValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OccurrenceMappingHelper
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceMappingHelper.class);

   @Autowired
   private AdditionalAttributeConfigFactory attributeConfigFactory;
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;
   @Autowired
   private ValidationStatusMapperIndicia validationStatusMapper;
   @Autowired
   private PersonQueries personQueries;
   public void mapAdditionalAttributes(Occurrence occurrence,
         HasOccurrenceAttributeValues jpaOccurrcence)
   {
      TermConfig termConfig = termlistConfigFactory.getTermConfig();

      AdditionalOccurrenceAttributeConfig occurrenceConfig = attributeConfigFactory
            .getOccurrenceConfig();

      List<DBOccurrenceAttributeValue> attributeValues = jpaOccurrcence
            .getOccurrenceAttributeValues().stream()
            .filter(value -> !value.isDeleted()
                  && !value.getAttribute().isDeleted())
            .collect(Collectors.toList());

      attributeValues.forEach(attributeValue ->
      {
         Integer attributeId = attributeValue.getAttribute().getId();
         if (occurrenceConfig.getUuid() == attributeId)
         {
            occurrence.setId(UUID.fromString(attributeValue.getTextValue()));
         } else if (occurrenceConfig.getAmount() == attributeId)
         {
            occurrence.setAmount(
                  termConfig.getAmount(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getStatus() == attributeId)
         {
            occurrence.setSettlementStatus(
                  termConfig.getStatus(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getCoveredArea() == attributeId)
         {
            occurrence.setCoveredArea(
                  termConfig.getArea(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getVitality() == attributeId)
         {
            occurrence.setVitality(
                  termConfig.getVitality(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getOccurrenceStatus() == attributeId)
         {
            occurrence.setSettlementStatusFukarek(termConfig
                  .getOccurrenceStatusFukarek(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getBloomingSprouts() == attributeId)
         {
            occurrence.setBloomingSprouts(
                  termConfig.getBloomingSprouts(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getQuantity() == attributeId)
         {
            occurrence.setQuantity(
                  termConfig.getQuantity(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getObservers() == attributeId)
         {
            occurrence.setObservers(attributeValue.getTextValue());
         } else if (occurrenceConfig.getHerbariumCode() == attributeId)
         {
            if (occurrence.getHerbarium() == null)
            {
               occurrence.setHerbarium(new Herbarium());
            }
            occurrence.getHerbarium().setCode(attributeValue.getTextValue());
         } else if (occurrenceConfig.getHerbary() == attributeId)
         {
            if (occurrence.getHerbarium() == null)
            {
               occurrence.setHerbarium(new Herbarium());
            }
            occurrence.getHerbarium().setHerbary(attributeValue.getTextValue());
         } else if (occurrenceConfig.getDeterminationComment() == attributeId)
         {
            occurrence.setDeterminationComment(attributeValue.getTextValue());
         } else if (occurrenceConfig.getNumericAmount() == attributeId)
         {
            occurrence.setNumericAmount(attributeValue.getIntValue());
         } else if (occurrenceConfig.getReproduction() == attributeId)
         {
            occurrence.setReproduction(
                  termConfig.getReproduction(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getSex() == attributeId)
         {
            occurrence.setSex(termConfig.getSex(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getLifeStage() == attributeId)
         {
            occurrence.setLifeStage(
                  termConfig.getLifeStage(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getMakropter() == attributeId)
         {
            occurrence.setMakropter(
                  termConfig.getMakropter(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getHabitat() == attributeId)
         {
            occurrence.setHabitat(attributeValue.getTextValue());
         }
         else if (occurrenceConfig.getCiteId() == attributeId)
         {
            occurrence.setCiteId(attributeValue.getTextValue());
         }
         else if (occurrenceConfig.getCiteComment() == attributeId)
         {
            occurrence.setCiteComment(attributeValue.getTextValue());
         }
         else if (occurrenceConfig.getTimeOfDay() == attributeId)
         {
            occurrence.setTimeOfDay(attributeValue.getTextValue());
         }
         else if (occurrenceConfig.getRemark() == attributeId)
         {
            occurrence.setRemark(attributeValue.getTextValue());
         }
         else if (occurrenceConfig.getValidationStatus() == attributeId)
         {
            if (occurrence.getValidation() == null)
            {
               occurrence.setValidation(new Validation());
            }
            occurrence
                  .setValidation(validationStatusMapper.map(attributeValue,
                        occurrence.getValidation()));
         } else if (occurrenceConfig.getNumericAmountAccuracy() == attributeId)
         {
            occurrence.setNumericAmountAccuracy(termConfig
                  .getNumericAmountAccuracy(attributeValue.getIntValue()));
         } else if (occurrenceConfig.getValidationComment() == attributeId)
         {
            if (occurrence.getValidation() == null)
            {
               occurrence.setValidation(new Validation());
            }
            occurrence.getValidation()
                  .setComment(attributeValue.getTextValue());
         }
      });
   }

   public void addValidator(Validation validation, Context context)
   {
      if (validation != null&& validation.getModifiedBy()!=null)
      {
         PersonFilter filter = new PersonFilter();
         filter.setEmail(validation.getModifiedBy().getLogin());
         Slice<Person, PersonSortField> people = personQueries.find(filter,
               context, new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
                     PersonSortField.ID));
         if (!people.getContent().isEmpty())
         {
            validation.setValidator(people.getContent().get(0));
         }
      }
   }
}
