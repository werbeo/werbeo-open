/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dve
 */
@Entity
@Table(name = "surveys")
@NamedEntityGraph(name = "SurveyOwner", attributeNodes = {
      @NamedAttributeNode(value = "ownerId") })
public class DBSurvey extends Base implements Serializable
{

   private static final long serialVersionUID = 1L;

   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 100)
   private String title;
   @Size(max = 2147483647)
   private String description;

   @JoinColumn(name = "owner_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBPerson ownerId;

   @JoinColumn(name = "website_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBWebsite website;

   @JoinColumn(name = "parent_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBSurvey parentId;

   @OneToMany(mappedBy = "survey")
   private Collection<DBSurveyAttributeValue> surveyAttributeValuesCollection;


   public DBWebsite getWebsite()
   {
      return website;
   }

   public void setWebsite(DBWebsite website)
   {
      this.website = website;
   }

   @Override
   public Integer getId()
   {
      return id;
   }

   @Override
   public void setId(Integer id)
   {
      this.id = id;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   @Override
   public Date getUpdatedOn()
   {
      return updatedOn;
   }

   @Override
   public void setUpdatedOn(Date updatedOn)
   {
      this.updatedOn = updatedOn;
   }

   public DBPerson getOwnerId()
   {
      return ownerId;
   }

   public void setOwnerId(DBPerson ownerId)
   {
      this.ownerId = ownerId;
   }

   public DBSurvey getParentId()
   {
      return parentId;
   }

   public void setParentId(DBSurvey parentId)
   {
      this.parentId = parentId;
   }

   @Override
   public DBUser getCreatedById()
   {
      return createdById;
   }

   @Override
   public void setCreatedById(DBUser createdById)
   {
      this.createdById = createdById;
   }

   @Override
   public DBUser getUpdatedById()
   {
      return updatedById;
   }

   @Override
   public void setUpdatedById(DBUser updatedById)
   {
      this.updatedById = updatedById;
   }

   @XmlTransient
   public Collection<DBSurveyAttributeValue> getSurveyAttributeValuesCollection()
   {
      return surveyAttributeValuesCollection;
   }

   public void setSurveyAttributeValuesCollection(
         Collection<DBSurveyAttributeValue> surveyAttributeValuesCollection)
   {
      this.surveyAttributeValuesCollection = surveyAttributeValuesCollection;
   }
}
