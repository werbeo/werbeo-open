package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "occurrence_comments")
public class DBOccurrenceComment extends Base
{
   @Column(name = "comment")
   private String comment;
   @Column(name = "occurrence_id")
   private int occurrenceId;

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public int getOccurrenceId()
   {
      return occurrenceId;
   }

   public void setOccurrenceId(int sampleId)
   {
      this.occurrenceId = sampleId;
   }
}
