package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.*;

import com.querydsl.core.annotations.QueryInit;

@Entity
@Table(name = "taxon_lists")
public class DBTaxaList extends Base
{
   @Column(name = "title")
   private String name;
   @Column(name = "description")
   private String description;
   
   @JoinColumn(name = "website_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBWebsite website;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public DBWebsite getWebsite()
   {
      return website;
   }

   public void setWebsite(DBWebsite website)
   {
      this.website = website;
   }

}
