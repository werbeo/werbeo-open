package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.util.Optional;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<DBUser, Integer>,
      QueryDslPredicateAndProjectionExecutor<DBUser, Integer>
{

   Optional<DBUser> findOneByUsername(String username);

}
