package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SurveySubmissionContrainer
{
   private final IndiciaSurvey survey;
   private final Set<SurveyMedium> media;
   private final Map<String, String> additionalAttributes;

   public SurveySubmissionContrainer(IndiciaSurvey survey,
         Set<SurveyMedium> media, Map<String, String> additionalAttributes)
   {
      super();
      this.survey = survey;
      this.media = media;
      this.additionalAttributes = additionalAttributes;
   }

   public IndiciaSurvey getSurvey()
   {
      return survey;
   }

   public Set<SurveyMedium> getMedia()
   {
      return Collections.unmodifiableSet(media);
   }

   public Map<String, String> getAdditionalAttributes()
   {
      return Collections.unmodifiableMap(additionalAttributes);
   }

   @Override
   public String toString()
   {
      return SurveySubmissionContrainerBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveySubmissionContrainerBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveySubmissionContrainerBeanUtil.doEquals(this, obj);
   }
}