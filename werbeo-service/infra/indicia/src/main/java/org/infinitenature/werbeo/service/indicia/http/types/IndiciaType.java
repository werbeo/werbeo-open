package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Date;

public interface IndiciaType
{

   public abstract int getId();

   public abstract void setId(int id);

   public abstract Date getCreatedOn();

   public abstract void setCreatedOn(Date createdOn);

   public abstract Date getUpdatedOn();

   public abstract void setUpdatedOn(Date updatedOn);

   public abstract long getUpdatedById();

   public abstract String getUpdatedByName();

   public abstract long getCreatedById();

   public abstract String getCreatedByName();

   public abstract void setCreatedBy(int userId);

   public abstract void setUpdatedBy(int userId);

   public abstract User getCreatedBy();

   public abstract void setCreatedBy(UserAsSubelement createdBy);

   public abstract User getUpdatedBy();

   public abstract void setUpdatedBy(UserAsSubelement updatedBy);

   public abstract String getModelName();

   boolean isDeleted();

   void setDeleted(boolean deleted);
}