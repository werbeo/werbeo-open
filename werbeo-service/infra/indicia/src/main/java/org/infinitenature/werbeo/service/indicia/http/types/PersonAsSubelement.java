package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonAsSubelement
{
   @XmlAttribute
   private int id;
   @XmlValue
   private String lastName;

   public PersonAsSubelement()
   {

   }

   public PersonAsSubelement(int id)
   {
      this.id = id;
   }

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("PersonAsSubelement@");
      builder.append(System.identityHashCode(this));
      builder.append(" [id=");
      builder.append(id);
      builder.append(", lastName=");
      builder.append(lastName);
      builder.append("]");
      return builder.toString();
   }
}