package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "person_attribute")
public class PersonAttribute extends AbstractAttribute implements Attribute
{
}