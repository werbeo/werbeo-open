package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.infinitenature.werbeo.service.adapters.AdditionalTaxonAttributeConfig;
import org.infinitenature.werbeo.service.adapters.config.*;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.indicia.jpa.entities.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaxonMappingHelper
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TaxonMappingHelper.class);

   @Autowired
   private AdditionalAttributeConfigFactory attributeConfigFactory;
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;

   public void mapAdditionalAttributes(Taxon taxon,
         HasTaxonAttributeValues jpaTaxon)
   {
      TermConfig termConfig = termlistConfigFactory.getTermConfig();

      AdditionalTaxonAttributeConfig taxonConfig = attributeConfigFactory
            .getTaxonConfig();

      List<DBTaxonAttributeValue> attributeValues = jpaTaxon
            .getTaxonAttributeValues().stream()
            .filter(value -> !value.isDeleted()
                  && !value.getAttribute().isDeleted())
            .collect(Collectors.toList());

      attributeValues.forEach(attributeValue ->
      {
         Integer attributeId = attributeValue.getAttribute().getId();
         if (taxonConfig.getRedListStatusId() == attributeId)
         {
            taxon.setRedListStatus(termConfig.getRedListStatus(attributeValue.getIntValue()));
         } 
      });
   }
}
