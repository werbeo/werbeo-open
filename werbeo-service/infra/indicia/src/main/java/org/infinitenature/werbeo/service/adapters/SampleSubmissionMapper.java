package org.infinitenature.werbeo.service.adapters;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Position.PositionType;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class SampleSubmissionMapper
{
   private static final String DECIMAL_FORMAT_PATTERN = "#0.########";

   @Autowired
   private CoordinateTransformerFactory coordinateTransformerFactory;
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;

   protected String createEnteredSrefString(Sample sample)
   {
      NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
      DecimalFormat df = (DecimalFormat) nf;
      df.applyPattern(DECIMAL_FORMAT_PATTERN);
      String enterdSref;
      if (31468 == sample.getLocality().getPosition().getEpsg()
            || 31469 == sample.getLocality().getPosition().getEpsg()
            || 25832 == sample.getLocality().getPosition().getEpsg()
            || 25833 == sample.getLocality().getPosition().getEpsg()
            || 4258 == sample.getLocality().getPosition().getEpsg()
            || 3398 == sample.getLocality().getPosition().getEpsg()
            || 900913 == sample.getLocality().getPosition().getEpsg()
            || 31467 == sample.getLocality().getPosition().getEpsg()
            || 5650 == sample.getLocality().getPosition().getEpsg())
      {
         enterdSref = df.format(sample.getLocality().getPosition().getPosCenter()[0]) + ", "
               + df.format(sample.getLocality().getPosition().getPosCenter()[1]);
      } else  // 4326, 258337
      {
         enterdSref = df.format(sample.getLocality().getPosition().getPosCenter()[1]) + ", "
               + df.format(sample.getLocality().getPosition().getPosCenter()[0]);
      }
      return enterdSref;
   }

   @Mapping(target = "deleted", ignore = true)
   @Mapping(target = "comment", ignore = true)
   @Mapping(target = "dateType", ignore = true)
   @Mapping(target = "determiner", ignore = true)
   @Mapping(target = "determinerId", source = "determiner.id")
   @Mapping(target = "determinerName", ignore = true)
   @Mapping(target = "endDate", ignore = true)
   @Mapping(target = "enteredSref", ignore = true)
   @Mapping(target = "enteredSrefSystem", ignore = true)
   @Mapping(target = "geom", ignore = true)
   @Mapping(target = "occurrenceAttributes", ignore = true)
   @Mapping(target = "sampleAttributes", ignore = true)
   @Mapping(target = "sampleId", ignore = true)
   @Mapping(target = "startDate", ignore = true)
   @Mapping(target = "survey", ignore = true)
   @Mapping(target = "surveyId", ignore = true)
   @Mapping(target = "taxaTaxonListId", source = "taxon.id")
   @Mapping(target = "taxonMeaningId", ignore = true)
   @Mapping(target = "websiteId", ignore = true)
   @Mapping(target = "wkt", ignore = true)
   @Mapping(target = "createdOn", ignore = true)
   @Mapping(target = "updatedBy", source = "modifiedBy.id")
   @Mapping(target = "createdBy", source = "createdBy.id")
   @Mapping(target = "taxon", source = "taxon.name")
   @Mapping(target = "updatedOn", ignore = true)
   @Mapping(target = "id", ignore = true)

   abstract IndiciaOccurrence map(Occurrence occurrence,
         @org.mapstruct.Context Context context);

   @Mapping(target = "deleted", ignore = true)
   @Mapping(target = "comment", ignore = true)
   @Mapping(target = "surveyId", source = "survey.id")
   @Mapping(target = "recorderNames", expression = "java( createRecorderNames(sample.getRecorder()) )")
   @Mapping(target = "dateType", ignore = true)
   @Mapping(target = "endDate", ignore = true)
   @Mapping(target = "startDate", ignore = true)
   @Mapping(target = "enteredSref", ignore = true)
   @Mapping(target = "enteredSrefSystem", ignore = true)
   @Mapping(target = "geom", ignore = true)
   @Mapping(target = "wkt", ignore = true)
   @Mapping(target = "parentId", ignore = true)
   @Mapping(target = "locationId", ignore = true)
   @Mapping(target = "month", ignore = true)
   @Mapping(target = "createdOn", ignore = true)
   @Mapping(target = "updatedBy", source = "modifiedBy.id")
   @Mapping(target = "createdBy", source = "createdBy.id")
   @Mapping(target = "updatedOn", ignore = true)
   @Mapping(target = "websiteId", ignore = true)
   @Mapping(target = "id", ignore = true)
   @Mapping(target = "year", ignore = true)
   @Mapping(target = "sampleMethodId", source = "sampleMethod")
   abstract IndiciaSample map(Sample sample,
         @org.mapstruct.Context Context context);

   String map(VagueDate date)
   {
      return date.toString();
   }

   public int map(SampleMethod sampleMethod)
   {
      if (sampleMethod == null)
      {
         return 0;
      } else
      {
         return termlistConfigFactory.getTermConfig().getSampleMethodMap()
               .get(sampleMethod);
      }
   }
   public String map(RecordStatus recordStatus)
   {
      switch (recordStatus)
      {
      case COMPLETE:
         return IndiciaOccurrence.COMPLETED;
      case IN_PROGRESS:
         return IndiciaOccurrence.IN_PROGRESS;
      default:
         throw new IllegalArgumentException(
               "RecordStatus: " + recordStatus + " not supported");
      }
   }

   @AfterMapping
   void mapBase(@MappingTarget IndiciaOccurrence indiciaOccurrence,
         Occurrence occurrence, @org.mapstruct.Context Context context)
   {
      indiciaOccurrence.setWebsiteId(context.getPortal().getId());
   }

   @AfterMapping
   void mapBase(@MappingTarget IndiciaSample indiciaSample, Sample sample,
         @org.mapstruct.Context Context context)
   {
      indiciaSample.setWebsiteId(context.getPortal().getId());
   }

   protected String createRecorderNames(Person recorder)
   {
      return recorder != null ? "#id-" + recorder.getId() + "#" : null;
   }

   @AfterMapping
   void mapPosition(@MappingTarget IndiciaSample indiciaSample, Sample sample)
   {
      if (sample.getLocality().getPosition().getType() == PositionType.POINT)
      {
         indiciaSample.setEnteredSrefSystem(
               String.valueOf(sample.getLocality().getPosition().getEpsg()));

         String enterdSref = createEnteredSrefString(sample);
         indiciaSample.setEnteredSref(enterdSref);
      } else if (sample.getLocality().getPosition().getType() == PositionType.MTB)
      {
         indiciaSample.setEnteredSrefSystem("mtbqqq");
         indiciaSample.setEnteredSref(sample.getLocality().getPosition().getMtb().getMtb());
      } else if (sample.getLocality().getPosition().getType() == PositionType.SHAPE)
      {
         CoordinateTransformer coordinateTransformer = coordinateTransformerFactory
               .getCoordinateTransformer(sample.getLocality().getPosition().getWktEpsg(),
                     900913);
         indiciaSample.setGeom(
               coordinateTransformer.convert(sample.getLocality().getPosition().getWkt()));
         NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
         DecimalFormat df = (DecimalFormat) nf;
         df.applyPattern(DECIMAL_FORMAT_PATTERN);
         indiciaSample.setEnteredSrefSystem(
               String.valueOf(sample.getLocality().getPosition().getEpsg()));
         indiciaSample.setEnteredSref(
               df.format(sample.getLocality().getPosition().getPosCenter()[1]) + ", "
                     + df.format(sample.getLocality().getPosition().getPosCenter()[0]));
      }
   }
}
