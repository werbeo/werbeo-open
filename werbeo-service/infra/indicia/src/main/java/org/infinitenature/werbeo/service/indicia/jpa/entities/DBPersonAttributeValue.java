/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author hfr
 */
@Entity
@Table(name = "person_attribute_values")
public class DBPersonAttributeValue extends DBAbstractAttributeValue
      implements Serializable
{

   private static final long serialVersionUID = 1L;

   @JoinColumn(name = "person_attribute_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBPersonAttribute personAttribute;
   @JoinColumn(name = "person_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBPerson person;

   public DBPersonAttributeValue()
   {
   }

   @Override
   public Base getEntity()
   {
      return getPerson();
   }

   @Override
   public DBAbstractAttribute getAttribute()
   {
      return getPersonAttribute();
   }

   public DBPersonAttributeValue(Integer id)
   {
      this.id = id;
   }

   public DBPersonAttributeValue(Integer id, Date createdOn, Date updatedOn,
         boolean deleted)
   {
      this.id = id;
      this.createdOn = createdOn;
      this.updatedOn = updatedOn;
      this.deleted = deleted;
   }

   public DBPersonAttribute getPersonAttribute()
   {
      return personAttribute;
   }

   public void setPersonAttribute(DBPersonAttribute personAttribute)
   {
      this.personAttribute = personAttribute;
   }

   public DBPerson getPerson()
   {
      return person;
   }

   public void setPersony(DBPerson person)
   {
      this.person = person;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBPersonAttributeValue))
      {
         return false;
      }
      DBPersonAttributeValue other = (DBPersonAttributeValue) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.PersonAttributeValues[ id=" + id
            + " ]";
   }

}
