package org.infinitenature.werbeo.service.adapters;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.IdOnly;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceAttributeValueRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SampleAttributeValueRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class IdQueries
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(IdQueries.class);
   @Autowired
   private SampleAttributeValueRepository sampleAttributeValuesRepository;
   @Autowired
   private OccurrenceAttributeValueRepository occurrenceAttributeValueRepository;
   @Autowired
   private OccurrenceRepository occurrenceRepository;
   public int getIndiciaSampleId(UUID sampleUUID)
   {
      QDBSampleAttributeValue qdbSampleAttributeValue = QDBSampleAttributeValue.dBSampleAttributeValue;

      BooleanExpression expression = qdbSampleAttributeValue.deleted.isFalse()
            .and(qdbSampleAttributeValue.sampleAttribute.caption.eq("UUID"))
            .and(qdbSampleAttributeValue.textValue.eq(sampleUUID.toString()))
            .and(qdbSampleAttributeValue.sample.deleted.isFalse());
      DBSampleAttributeValue sampleAttributeValue = sampleAttributeValuesRepository
            .findOne(expression).orElse(null);
      int sampleId = sampleAttributeValue != null
            ? sampleAttributeValue.getSample().getId()
            : 0;
      LOGGER.info("Got indicia id {} for sample uuid {}", sampleId, sampleUUID);
      return sampleId;
   }

   public int getIndiciaOccurrenceId(UUID occurrenceUUID)
   {

      QDBOccurrenceAttributeValue qdbOccurrenceAttributeValue = QDBOccurrenceAttributeValue.dBOccurrenceAttributeValue;

      BooleanExpression expression = qdbOccurrenceAttributeValue.deleted
            .isFalse()
            .and(qdbOccurrenceAttributeValue.occurrenceAttribute.caption
                  .eq("UUID"))
            .and(qdbOccurrenceAttributeValue.textValue
                  .eq(occurrenceUUID.toString()))
            .and(qdbOccurrenceAttributeValue.occurrence.deleted.isFalse());

      DBOccurrenceAttributeValue occurrenceAttributeValue = occurrenceAttributeValueRepository
            .findOne(expression).orElse(null);
      return occurrenceAttributeValue != null
            ? occurrenceAttributeValue.getOccurrence().getId()
            : 0;
   }

   public Set<Integer> getIndiciaOccurrenceIds(int sampleId)
   {
      if (sampleId == 0)
      {
         return Collections.emptySet();
      }
      else
      {
         QDBOccurrence qOccurrence = QDBOccurrence.dBOccurrence;
         List<IdOnly> sampleIds = occurrenceRepository.findWithProjection(
               Projections.bean(IdOnly.class, qOccurrence.id),
               qOccurrence.sample.id.eq(sampleId)
                     .and(qOccurrence.deleted.isFalse()));
         return sampleIds.stream().map(IdOnly::getId)
               .collect(Collectors.toSet());
      }

   }

}
