package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxaTaxonList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TaxaTaxonListRepository
      extends JpaRepository<DBTaxaTaxonList, Integer>,
      QueryDslPredicateAndProjectionExecutor<DBTaxaTaxonList, Integer>
{
   @Query("SELECT MAX(taxonMeaningId) FROM DBTaxaTaxonList WHERE deleted = false")
   int getMaxTaxonMeaningId();
}
