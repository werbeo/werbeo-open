package org.infinitenature.werbeo.service.indicia.jpa.entities.index;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBWebsite;

@Entity
@Table(name = "index_websites_website_agreements")
public class WebsiteAgreementsIndex
{
   @Id
   @Column(name = "id")
   private int id;

   @JoinColumn(name = "from_website_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private DBWebsite fromWebsite;

   @JoinColumn(name = "to_website_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private DBWebsite toWebsite;

   @Column(name = "provide_for_reporting")
   private boolean provideForReporting;

   @Column(name = "receive_for_reporting")
   private boolean receiveForReporting;

   @Column(name = "provide_for_peer_review")
   private boolean provideForPeerReview;

   @Column(name = "receive_for_peer_review")
   private boolean receiveForPeerReview;

   @Column(name = "provide_for_verification")
   private boolean provideForVerification;

   @Column(name = "receive_for_verification")
   private boolean receiveForVerification;

   @Column(name = "provide_for_data_flow")
   private boolean provideForDataFlow;

   @Column(name = "receive_for_data_flow")
   private boolean receiveForDataFlow;

   @Column(name = "provide_for_moderation")
   private boolean provideForModeration;

   @Column(name = "receive_for_moderation")
   private boolean receiveForModeration;

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public DBWebsite getFromWebsite()
   {
      return fromWebsite;
   }

   public void setFromWebsite(DBWebsite fromWebsite)
   {
      this.fromWebsite = fromWebsite;
   }

   public DBWebsite getToWebsite()
   {
      return toWebsite;
   }

   public void setToWebsite(DBWebsite toWebsite)
   {
      this.toWebsite = toWebsite;
   }

   public boolean isProvideForReporting()
   {
      return provideForReporting;
   }

   public void setProvideForReporting(boolean provideForReporting)
   {
      this.provideForReporting = provideForReporting;
   }

   public boolean isReceiveForReporting()
   {
      return receiveForReporting;
   }

   public void setReceiveForReporting(boolean receiveForReporting)
   {
      this.receiveForReporting = receiveForReporting;
   }

   public boolean isProvideForPeerReview()
   {
      return provideForPeerReview;
   }

   public void setProvideForPeerReview(boolean provideForPeerReview)
   {
      this.provideForPeerReview = provideForPeerReview;
   }

   public boolean isReceiveForPeerReview()
   {
      return receiveForPeerReview;
   }

   public void setReceiveForPeerReview(boolean receiveForPeerReview)
   {
      this.receiveForPeerReview = receiveForPeerReview;
   }

   public boolean isProvideForVerification()
   {
      return provideForVerification;
   }

   public void setProvideForVerification(boolean provideForVerification)
   {
      this.provideForVerification = provideForVerification;
   }

   public boolean isReceiveForVerification()
   {
      return receiveForVerification;
   }

   public void setReceiveForVerification(boolean receiveForVerification)
   {
      this.receiveForVerification = receiveForVerification;
   }

   public boolean isProvideForDataFlow()
   {
      return provideForDataFlow;
   }

   public void setProvideForDataFlow(boolean provideForDataFlow)
   {
      this.provideForDataFlow = provideForDataFlow;
   }

   public boolean isReceiveForDataFlow()
   {
      return receiveForDataFlow;
   }

   public void setReceiveForDataFlow(boolean receiveForDataFlow)
   {
      this.receiveForDataFlow = receiveForDataFlow;
   }

   public boolean isProvideForModeration()
   {
      return provideForModeration;
   }

   public void setProvideForModeration(boolean provideForModeration)
   {
      this.provideForModeration = provideForModeration;
   }

   public boolean isReceiveForModeration()
   {
      return receiveForModeration;
   }

   public void setReceiveForModeration(boolean receiveForModeration)
   {
      this.receiveForModeration = receiveForModeration;
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + id;
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (obj == null)
      {
         return false;
      }
      if (getClass() != obj.getClass())
      {
         return false;
      }
      WebsiteAgreementsIndex other = (WebsiteAgreementsIndex) obj;
      if (id != other.id)
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("WebsiteAgreementsIndex@");
      builder.append("@");
      builder.append(System.identityHashCode(this));
      builder.append("[id=");
      builder.append(id);
      builder.append("]");
      return builder.toString();
   }

}
