package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxonMeaning;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaxonMeaningRepository
      extends JpaRepository<DBTaxonMeaning, Integer>
{

}
