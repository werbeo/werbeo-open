package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "locations")
public class LocationContainer
{
   private List<Location> locations = new ArrayList<>();

   @XmlElement(name = "location")
   public List<Location> getLocations()
   {
      return locations;
   }

   public void setLocations(List<Location> locations)
   {
      this.locations = locations;
   }
}