package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sample_comments")
public class DBSampleComment extends Base
{
   @Column(name = "comment")
   private String comment;
   @Column(name = "sample_id")
   private int sampleId;

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public int getSampleId()
   {
      return sampleId;
   }

   public void setSampleId(int sampleId)
   {
      this.sampleId = sampleId;
   }
}
