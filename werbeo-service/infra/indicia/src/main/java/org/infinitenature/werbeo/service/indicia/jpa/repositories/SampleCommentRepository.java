package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleComment;

public interface SampleCommentRepository
      extends QueryDslPredicateAndProjectionExecutor<DBSampleComment, Integer>
{

}
