package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSample;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PosionMapperIndiciaImpl
{
   @Autowired
   private PositionFactory positionFactory;

   @Autowired
   private CoordinateTransformerFactory coordinateTransformerFactory;

   public Position map(DBSample jpaSample)
   {
      return map2Position(jpaSample.getEnteredSrefSystem(),
            jpaSample.getEnteredSref(), jpaSample.getGeometry(), null);
   }

   protected Position map2Position(String enteredSrefSystem, String enteredSref,
         Geometry geom, String code)
   {
      Position position;// = new Position();

      if ("mtbqqq".equals(enteredSrefSystem))
      {
         position = positionFactory.createFromMTB(enteredSref);
      } else if (geom instanceof Point)
      {
         // format of enteredSref can be : "54.04454N 13.711E",
         // "5192989, 3133647" or "52.6665, 13.0549"
         Integer srefSystemCode = Integer.valueOf(enteredSrefSystem);
         if (enteredSref.contains(","))
         {
            String[] pos = enteredSref.split(", ");
            if (srefSystemCode == 4326 || srefSystemCode == 258337)
            {
               position = positionFactory.create(Double.valueOf(pos[1]),
                     Double.valueOf(pos[0]), srefSystemCode);
            } else
            {
               position = positionFactory.create(Double.valueOf(pos[0]),
                     Double.valueOf(pos[1]), srefSystemCode);
            }
         } else
         {
            String[] pos = enteredSref.split(" ");
            String northing = pos[0].contains("N")
                  ? pos[0].substring(0, pos[0].length() - 1)
                  : pos[1].substring(0, pos[1].length() - 1);
            String easting = pos[0].contains("E")
                  ? pos[0].substring(0, pos[0].length() - 1)
                  : pos[1].substring(0, pos[1].length() - 1);
            position = positionFactory.create(Double.valueOf(easting),
                  Double.valueOf(northing), srefSystemCode);
         }
      } else
      {
         // convert shape to enteredSrefSystem, if other then 900913
         if(!"900913".contentEquals(enteredSrefSystem))
         {
            CoordinateTransformer coordinateTransformer = coordinateTransformerFactory
                  .getCoordinateTransformer( 900913,
                        Integer.valueOf(enteredSrefSystem));

            Geometry transformedGeometry = coordinateTransformer.convert(geom);
            position = positionFactory.create(transformedGeometry.toText(),
                  Integer.valueOf(enteredSrefSystem));
         }
         else
         {
            position = positionFactory.create(geom.toText(),
                  Integer.valueOf(900913));
         }
      }

      position.setCode(code);
      return position;
   }
}
