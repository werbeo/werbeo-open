package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceMedia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigIndiciaJPA.class)
public class MediaMapperIndicia
{
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;

   public Medium map(DBOccurrenceMedia jpaMedia,
         @org.mapstruct.Context Context context)
   {
      if (jpaMedia == null)
      {
         return null;
      }
      return new Medium(jpaMedia.getPath(), jpaMedia.getCaption(),
            termlistConfigFactory.getTermConfig()
                  .getMediaType(jpaMedia.getMediaType().getId()));

   }

   public List<Medium> map(
         Set<DBOccurrenceMedia> set, @org.mapstruct.Context Context context)
   {
      if (set == null)
      {
         return null;
      }

      List<Medium> list = new ArrayList<>(set.size());
      for (DBOccurrenceMedia dBOccurrenceMedia : set)
      {
         if (!dBOccurrenceMedia.isDeleted())
         {
            list.add(map(dBOccurrenceMedia, context));
         }
      }

      return list;
   }
}
