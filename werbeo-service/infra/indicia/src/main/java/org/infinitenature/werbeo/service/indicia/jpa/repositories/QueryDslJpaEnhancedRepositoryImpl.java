package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.JPQLQuery;

public class QueryDslJpaEnhancedRepositoryImpl<T, ID extends Serializable>
      extends QuerydslJpaRepository<T, ID>
      implements QueryDslPredicateAndProjectionExecutor<T, ID>
{

   // All instance variables are available in super, but they are private
   private static final EntityPathResolver DEFAULT_ENTITY_PATH_RESOLVER = SimpleEntityPathResolver.INSTANCE;

   private final EntityPath<T> path;
   private final PathBuilder<T> builder;
   private final Querydsl querydsl;
   public QueryDslJpaEnhancedRepositoryImpl(
         JpaEntityInformation<T, ID> entityInformation,
         EntityManager entityManager)
   {
      this(entityInformation, entityManager, DEFAULT_ENTITY_PATH_RESOLVER);
   }

   public QueryDslJpaEnhancedRepositoryImpl(
         JpaEntityInformation<T, ID> entityInformation,
         EntityManager entityManager, EntityPathResolver resolver)
   {

      super(entityInformation, entityManager, resolver);
      this.path = resolver.createPath(entityInformation.getJavaType());
      this.builder = new PathBuilder<>(path.getType(), path.getMetadata());
      this.querydsl = new Querydsl(entityManager, builder);
   }



   @Override
   public List<T> findWithoutCount(Predicate predicate, Pageable pageable)
   {
      return querydsl
            .applyPagination(pageable, createQuery(predicate).select(path))
            .fetch();
   }

   @Override
   public <PROJ> Page<PROJ> findWithProjection(
         FactoryExpression<PROJ> factoryExpression, Predicate predicate,
         Pageable pageable)
   {

      JPQLQuery<PROJ> query = querydsl.applyPagination(pageable,
            createQuery(predicate).select(factoryExpression));

      List<PROJ> content = query.fetch();

      return new PageImpl<>(content, pageable, 0);
   }

   @Override
   public <PROJ> List<PROJ> findDistinctWithProjection(
         FactoryExpression<PROJ> factoryExpression, Predicate predicate)
   {
      JPQLQuery<PROJ> query = createQuery(predicate).select(factoryExpression)
            .distinct();
      return query.fetch();
   }

   @Override
   public <PROJ> List<PROJ> findWithProjection(
         FactoryExpression<PROJ> factoryExpression, Predicate predicate)
   {
      JPQLQuery<PROJ> query = createQuery(predicate).select(factoryExpression);
      return query.fetch();
   }
}