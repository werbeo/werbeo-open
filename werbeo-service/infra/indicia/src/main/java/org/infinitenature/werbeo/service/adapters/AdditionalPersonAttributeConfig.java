package org.infinitenature.werbeo.service.adapters;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class AdditionalPersonAttributeConfig
{
   private int jobId;

   public int getJobId()
   {
      return jobId;
   }

   public void setJobId(int jobId)
   {
      this.jobId = jobId;
   }

   public int getWorkingPeriodId()
   {
      return workingPeriodId;
   }

   public void setWorkingPeriodId(int workingPeriodId)
   {
      this.workingPeriodId = workingPeriodId;
   }

   private int workingPeriodId;



   @Override
   public String toString()
   {
      return AdditionalPersonAttributeConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return AdditionalPersonAttributeConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return AdditionalPersonAttributeConfigBeanUtil.doEquals(this, obj);
   }
}
