package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sample_attributes_websites")
public class DBSampleAttributesWebsites extends DBAbstractAttributesWebsites
{
   private static final long serialVersionUID = 1L;
   @JoinColumn(name = "sample_attribute_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private DBSampleAttribute sampleAttribute;

   public DBSampleAttribute getSampleAttribute()
   {
      return sampleAttribute;
   }

   public void setSampleAttribute(DBSampleAttribute sampleAttributeId)
   {
      this.sampleAttribute = sampleAttributeId;
   }

}
