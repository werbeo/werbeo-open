package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTermlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface TermlistRepository extends JpaRepository<DBTermlist, Integer>,
      QuerydslPredicateExecutor<DBTermlist>
{
   public DBTermlist findOneByTitleAndDeletedIsFalse(String title);
}
