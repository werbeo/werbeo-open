package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceComment;

public interface OccurrenceCommentRepository extends
      QueryDslPredicateAndProjectionExecutor<DBOccurrenceComment, Integer>
{

}
