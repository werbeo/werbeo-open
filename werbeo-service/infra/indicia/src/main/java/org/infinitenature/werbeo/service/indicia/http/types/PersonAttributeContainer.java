package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "person_attributes")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonAttributeContainer
{
   @XmlElement(name = "person_attribute")
   private List<PersonAttribute> personAttributes = new ArrayList<>();

   public List<PersonAttribute> getPersonAttributes()
   {
      return personAttributes;
   }

   public void setPersonAttributes(List<PersonAttribute> personAttributes)
   {
      this.personAttributes = personAttributes;
   }
}