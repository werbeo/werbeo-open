package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.time.LocalDate;

public interface HasVagueDate
{
   public String getDateType();

   public LocalDate getEndDate();

   public LocalDate getStartDate();
}
