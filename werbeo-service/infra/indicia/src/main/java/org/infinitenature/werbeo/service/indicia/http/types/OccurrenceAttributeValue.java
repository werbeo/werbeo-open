package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "occurrence_attribute_value")
@XmlAccessorType(XmlAccessType.FIELD)
public class OccurrenceAttributeValue extends IndiciaTypeImpl
{

   @XmlElement(name = "occurrence_id")
   private int occurrenceId;
   @XmlElement(name = "occurrence_attribute_id")
   private int occurrenceAttributeId;
   @XmlElement(name = "data_type")
   private String dataType;
   @XmlElement(name = "caption")
   private String caption;
   @XmlElement(name = "value")
   private String value;
   @XmlElement(name = "website_id")
   private int websiteId;

   @XmlTransient
   private boolean intValue = true;

   public int getOccurrenceId()
   {
      return occurrenceId;
   }

   public void setOccurrenceId(int occurrenceId)
   {
      this.occurrenceId = occurrenceId;
   }

   public int getOccurrenceAttributeId()
   {
      return occurrenceAttributeId;
   }

   public void setOccurrenceAttributeId(int occurrenceAttributeId)
   {
      this.occurrenceAttributeId = occurrenceAttributeId;
   }

   public String getDataType()
   {
      return dataType;
   }

   public void setDataType(String dataType)
   {
      this.dataType = dataType;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   public int getWebsiteId()
   {
      return websiteId;
   }

   public void setWebsiteId(int websiteId)
   {
      this.websiteId = websiteId;
   }

   public boolean isIntValue()
   {
      return intValue;
   }

   public void setIntValue(boolean intValue)
   {
      this.intValue = intValue;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("OccurrenceAttributeValue [id=");
      builder.append(id);
      builder.append(", occurrenceId=");
      builder.append(occurrenceId);
      builder.append(", occurrenceAttributeId=");
      builder.append(occurrenceAttributeId);
      builder.append(", dataType=");
      builder.append(dataType);
      builder.append(", caption=");
      builder.append(caption);
      builder.append(", value=");
      builder.append(value);
      builder.append(", websiteId=");
      builder.append(websiteId);
      builder.append("]");
      return builder.toString();
   }

   @Override
   public String getModelName()
   {
      return "occurrence_attribute_value";
   }

}
