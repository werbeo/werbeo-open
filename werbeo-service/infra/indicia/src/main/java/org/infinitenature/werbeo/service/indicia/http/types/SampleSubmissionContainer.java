package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SampleSubmissionContainer
{
   private IndiciaSample sample;
   private Map<String, String> additionAtributes;
   private Set<SampleComment> comments;
   private Set<SampleMedium> media;
   private Set<OccurrenceSubmissionContainer> occurrenceSubmissionContainers;

   public SampleSubmissionContainer()
   {
      super();
      this.additionAtributes = new HashMap<>();
      this.comments = new HashSet<>();
      this.media = new HashSet<>();
      this.occurrenceSubmissionContainers = new HashSet<>();
   }

   public SampleSubmissionContainer(IndiciaSample sample,
         Map<String, String> additionAtributes, Set<SampleComment> comments,
         Set<OccurrenceSubmissionContainer> occurrenceSubmissionContainers,
         Set<SampleMedium> sampleMedia)
   {
      super();
      this.sample = sample;
      this.additionAtributes = additionAtributes;
      this.comments = comments;
      this.occurrenceSubmissionContainers = occurrenceSubmissionContainers;
      this.media = sampleMedia;
   }

   public IndiciaSample getSample()
   {
      return sample;
   }

   public void setSample(IndiciaSample sample)
   {
      this.sample = sample;
   }

   public Map<String, String> getAdditionAtributes()
   {
      return additionAtributes;
   }

   public void setAdditionAtributes(Map<String, String> additionAtributes)
   {
      this.additionAtributes = additionAtributes;
   }

   public Set<SampleComment> getComments()
   {
      return comments;
   }

   public void setComments(Set<SampleComment> comments)
   {
      this.comments = comments;
   }

   public Set<OccurrenceSubmissionContainer> getOccurrenceSubmissionContainers()
   {
      return occurrenceSubmissionContainers;
   }

   public void setOccurrenceSubmissionContainers(
         Set<OccurrenceSubmissionContainer> occurrenceSubmissionContainers)
   {
      this.occurrenceSubmissionContainers = occurrenceSubmissionContainers;
   }

   public Set<SampleMedium> getMedia()
   {
      return media;
   }

   public void setMedia(Set<SampleMedium> media)
   {
      this.media = media;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SampleSubmissionContainer@");
      builder.append(System.identityHashCode(this));
      builder.append(" [");
      if (sample != null)
      {
         builder.append("sample=");
         builder.append(sample);
         builder.append(", ");
      }
      if (additionAtributes != null)
      {
         builder.append("additionAtributes=");
         builder.append(additionAtributes);
         builder.append(", ");
      }
      if (comments != null)
      {
         builder.append("comments=");
         builder.append(comments);
         builder.append(", ");
      }
      if (media != null)
      {
         builder.append("media=");
         builder.append(media);
         builder.append(", ");
      }
      if (occurrenceSubmissionContainers != null)
      {
         builder.append("occurrenceSubmissionContainers=");
         builder.append(occurrenceSubmissionContainers);
      }
      builder.append("]");
      return builder.toString();
   }
}