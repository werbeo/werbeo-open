package org.infinitenature.werbeo.service.indicia.jpa;

import java.io.Serializable;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import org.infinitenature.werbeo.service.indicia.jpa.entities.Base;

public class IdGenerator implements IdentifierGenerator, Configurable
{
   private String sequenceCallSyntax;

   @Override
   public void configure(Type type, Properties params,
         ServiceRegistry serviceRegistry) throws MappingException
   {
      JdbcEnvironment jdbcEnvironment = serviceRegistry
            .getService(JdbcEnvironment.class);
      Dialect dialect = jdbcEnvironment.getDialect();
      final String defaultSequenceName = params.getProperty("target_table")
            + "_id_seq";
      sequenceCallSyntax = dialect.getSequenceNextValString(
            ConfigurationHelper.getString(SequenceStyleGenerator.SEQUENCE_PARAM,
                  params, defaultSequenceName));
   }

   @Override
   public Serializable generate(SharedSessionContractImplementor session,
         Object obj) throws HibernateException
   {
      if (obj instanceof Base)
      {
         Base entity = (Base) obj;
         Integer id = entity.getId();
         if (id != null)
         {
            return id;
         }
      }
      return ((Number) ((Session) session).createNativeQuery(sequenceCallSyntax)
            .uniqueResult()).intValue();
   }

}
