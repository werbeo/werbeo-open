package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrence_attribute_values")
@XmlAccessorType(XmlAccessType.FIELD)
public class OccurrenceAttributeValueContainer
{
   @XmlElement(name = "occurrence_attribute_value")
   private List<OccurrenceAttributeValue> occurrenceAttributeValues = new ArrayList<>();

   public List<OccurrenceAttributeValue> getOccurrenceAttributeValues()
   {
      return occurrenceAttributeValues;
   }

   public void setOccurrenceAttributeValues(
         List<OccurrenceAttributeValue> occurrenceAttributeValues)
   {
      this.occurrenceAttributeValues = occurrenceAttributeValues;
   }
}
