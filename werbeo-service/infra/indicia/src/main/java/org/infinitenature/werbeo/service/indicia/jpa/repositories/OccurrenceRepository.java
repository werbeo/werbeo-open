package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrence;

import java.util.List;

public interface OccurrenceRepository
      extends QueryDslPredicateAndProjectionExecutor<DBOccurrence, Integer>
{
   public List<DBOccurrence> findBySampleId(int sampleId);

   // @EntityGraph(value = "graph.occurrenceFind") // why is this slower???
   public List<DBOccurrence> findDistinctByIdIn(List<Integer> id);

}
