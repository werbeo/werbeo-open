package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "terms")
public class DBTerm extends Base
{
   @Column(name = "term")
   private String term;

   @OneToMany(mappedBy = "term")
   private List<DBTermlistTerm> termListTerms = new ArrayList<>();

   public DBTerm()
   {
      super();
   }

   public DBTerm(int id, String term)
   {
      super();
      this.id = id;
      this.term = term;
   }

   public List<DBTermlistTerm> getTermListTerms()
   {
      return termListTerms;
   }

   public void setTermListTerms(List<DBTermlistTerm> termListTerms)
   {
      this.termListTerms = termListTerms;
   }

   public String getTerm()
   {
      return term;
   }

   public void setTerm(String term)
   {
      this.term = term;
   }

}
