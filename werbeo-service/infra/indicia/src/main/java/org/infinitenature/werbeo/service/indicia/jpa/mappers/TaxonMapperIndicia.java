package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.Collection;
import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxon;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigIndiciaJPA.class, imports = org.infinitenature.werbeo.service.core.api.enity.Language.class, uses = TaxaListMapperIndicia.class)
public abstract class TaxonMapperIndicia
{
   @Autowired
   private TaxonMappingHelper mappingHelper;

   @Mapping(target = "documentsAvailable", ignore = true)
   @Mapping(target = "name", source = "taxon")
   @Mapping(target = "group", source = "taxonGroup.title")
   @Mapping(target = "id", source = "taxaTaxonList.id")
   @Mapping(target = "language", expression = "java(Language.valueOf(taxon.getLanguage().getIso().toUpperCase()))")
   @Mapping(target = "allowDataEntry", source = "taxaTaxonList.allowDataEntry")
   @Mapping(target = "redListStatus", ignore = true)
   @Mapping(target = "taxaList", source = "taxaTaxonList.taxaList")
   @Mapping(target = "preferred", source = "taxaTaxonList.preferred")
   public abstract Taxon map(DBTaxon taxon);

   public abstract List<Taxon> map(Collection<DBTaxon> taxa);

   @AfterMapping
   public void setAttributes(@MappingTarget Taxon taxon,
         DBTaxon jpaTaxon)
   {

      mappingHelper.mapAdditionalAttributes(taxon, jpaTaxon);
   }
}
