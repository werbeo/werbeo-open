package org.infinitenature.werbeo.service.indicia.jpa.mappers.config;

import org.infinitenature.werbeo.service.core.api.enity.BaseTypeInt;
import org.infinitenature.werbeo.service.core.api.enity.BaseTypeUUID;
import org.infinitenature.werbeo.service.indicia.jpa.entities.Base;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.UserMapperIndicia;
import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;
import org.mapstruct.MappingInheritanceStrategy;

@MapperConfig(componentModel = "spring", mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_FROM_CONFIG, uses = {
      UserMapperIndicia.class })
public interface CentralConfigIndiciaJPA
{
   @Mapping(target = "creationDate", source = "createdOn")
   @Mapping(target = "modificationDate", source = "updatedOn")
   @Mapping(target = "createdBy", source = "createdById")
   @Mapping(target = "modifiedBy", source = "updatedById")
   @Mapping(target = "obfuscated", ignore = true) // set late by bussines logic
   @Mapping(target = "allowedOperations", ignore = true) // set in core-impl
   BaseTypeInt baseMapping(Base base);

   @Mapping(target = "creationDate", source = "createdOn")
   @Mapping(target = "modificationDate", source = "updatedOn")
   @Mapping(target = "createdBy", source = "createdById")
   @Mapping(target = "modifiedBy", source = "updatedById")
   @Mapping(target = "obfuscated", ignore = true) // set late by bussines logic
   @Mapping(target = "allowedOperations", ignore = true) // set in core-impl
   BaseTypeUUID baseMappingUUID(Base base);
}
