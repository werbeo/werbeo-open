package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "taxon_groups")
public class DBTaxonGroup extends Base
{

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   private String title;


}
