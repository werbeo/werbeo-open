package org.infinitenature.werbeo.service.indicia.http.http;

public class ReadAuthToken extends AuthToken
{
   private final ReadNonce readNonce;

   public ReadAuthToken(ReadNonce readNonce, String password)
   {
      super(password);
      this.readNonce = readNonce;
   }

   @Override
   public String getToken()
   {
      return createAuthToken(readNonce.getReadNonce());
   }

   @Override
   public String getNonce()
   {
      return readNonce.getReadNonce();
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result
            + ((readNonce == null) ? 0 : readNonce.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
         return true;
      if (!super.equals(obj))
         return false;
      if (getClass() != obj.getClass())
         return false;
      ReadAuthToken other = (ReadAuthToken) obj;
      if (readNonce == null)
      {
         if (other.readNonce != null)
            return false;
      } else if (!readNonce.equals(other.readNonce))
         return false;
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("ReadAuthToken@");
      builder.append(System.identityHashCode(this));
      builder.append(" [readNonce=");
      builder.append(readNonce);
      builder.append("]");
      return builder.toString();
   }
}