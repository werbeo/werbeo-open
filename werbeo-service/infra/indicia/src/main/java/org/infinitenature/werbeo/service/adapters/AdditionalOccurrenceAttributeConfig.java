package org.infinitenature.werbeo.service.adapters;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class AdditionalOccurrenceAttributeConfig
{
   private int cartNumber;
   private int litCode;
   private int extinctionDate;
   private int litRemark;
   private int amount;
   private int status;
   private int legName;
   private int uuid;
   private int fkDeck;
   private int coverageMin;
   private int coverageMax;
   private int coverageMean;
   private int layer;
   private int coverageCode;
   private int coveredArea;
   private int vitality;
   private int occurrenceStatus;
   private int bloomingSprouts;
   private int quantity;
   private int observers;
   private int herbariumCode;
   private int herbary;
   private int determinationComment;
   private int numericAmount;
   private int sex;
   private int lifeStage;
   private int makropter;
   private int reproduction;
   private int habitat;
   private int timeOfDay;
   private int validationStatus;
   private int numericAmountAccuracy;
   private int validationComment;
   private int remark;
   private int citeId;
   private int citeComment;

   public int getNumericAmountAccuracy()
   {
      return numericAmountAccuracy;
   }

   public void setNumericAmountAccuracy(int numericAmountAccuracy)
   {
      this.numericAmountAccuracy = numericAmountAccuracy;
   }

   public int getSex()
   {
      return sex;
   }

   public void setSex(int sex)
   {
      this.sex = sex;
   }

   public int getLifeStage()
   {
      return lifeStage;
   }

   public void setLifeStage(int lifeStage)
   {
      this.lifeStage = lifeStage;
   }

   public int getMakropter()
   {
      return makropter;
   }

   public void setMakropter(int makropter)
   {
      this.makropter = makropter;
   }

   public int getReproduction()
   {
      return reproduction;
   }

   public void setReproduction(int reproduction)
   {
      this.reproduction = reproduction;
   }

   public int getNumericAmount()
   {
      return numericAmount;
   }

   public void setNumericAmount(int numericAmount)
   {
      this.numericAmount = numericAmount;
   }

   public int getDeterminationComment()
   {
      return determinationComment;
   }

   public void setDeterminationComment(int determinationComment)
   {
      this.determinationComment = determinationComment;
   }

   public int getCoveredArea()
   {
      return coveredArea;
   }

   public void setCoveredArea(int coveredArea)
   {
      this.coveredArea = coveredArea;
   }

   public int getCartNumber()
   {
      return cartNumber;
   }

   public void setCartNumber(int cartNumber)
   {
      this.cartNumber = cartNumber;
   }

   public int getLitCode()
   {
      return litCode;
   }

   public void setLitCode(int litCode)
   {
      this.litCode = litCode;
   }

   public int getExtinctionDate()
   {
      return extinctionDate;
   }

   public void setExtinctionDate(int extinctionDate)
   {
      this.extinctionDate = extinctionDate;
   }

   public int getLitRemark()
   {
      return litRemark;
   }

   public void setLitRemark(int litRemark)
   {
      this.litRemark = litRemark;
   }

   public int getAmount()
   {
      return amount;
   }

   public void setAmount(int amount)
   {
      this.amount = amount;
   }

   public int getStatus()
   {
      return status;
   }

   public void setStatus(int status)
   {
      this.status = status;
   }

   public int getLegName()
   {
      return legName;
   }

   public void setLegName(int legName)
   {
      this.legName = legName;
   }

   public int getUuid()
   {
      return uuid;
   }

   public void setUuid(int uuid)
   {
      this.uuid = uuid;
   }

   public int getFkDeck()
   {
      return fkDeck;
   }

   public void setFkDeck(int fkDeck)
   {
      this.fkDeck = fkDeck;
   }

   public int getCoverageMin()
   {
      return coverageMin;
   }

   public void setCoverageMin(int coverageMin)
   {
      this.coverageMin = coverageMin;
   }

   public int getCoverageMax()
   {
      return coverageMax;
   }

   public void setCoverageMax(int coverageMax)
   {
      this.coverageMax = coverageMax;
   }

   public int getCoverageMean()
   {
      return coverageMean;
   }

   public void setCoverageMean(int coverageMean)
   {
      this.coverageMean = coverageMean;
   }

   public int getLayer()
   {
      return layer;
   }

   public void setLayer(int layer)
   {
      this.layer = layer;
   }

   public int getCoverageCode()
   {
      return coverageCode;
   }

   public void setCoverageCode(int coverageCode)
   {
      this.coverageCode = coverageCode;
   }

   public int getValidationStatus()
   {
      return validationStatus;
   }

   public void setValidationStatus(int validationStatus)
   {
      this.validationStatus = validationStatus;
   }

   @Override
   public String toString()
   {
      return AdditionalOccurrenceAttributeConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return AdditionalOccurrenceAttributeConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return AdditionalOccurrenceAttributeConfigBeanUtil.doEquals(this, obj);
   }

   public int getVitality()
   {
      return vitality;
   }

   public void setVitality(int vitality)
   {
      this.vitality = vitality;
   }

   public int getOccurrenceStatus()
   {
      return occurrenceStatus;
   }

   public void setOccurrenceStatus(int occurrenceStatus)
   {
      this.occurrenceStatus = occurrenceStatus;
   }

   public int getBloomingSprouts()
   {
      return bloomingSprouts;
   }

   public void setBloomingSprouts(int bloomingSprouts)
   {
      this.bloomingSprouts = bloomingSprouts;
   }

   public int getQuantity()
   {
      return quantity;
   }

   public void setQuantity(int quantity)
   {
      this.quantity = quantity;
   }

   public int getObservers()
   {
      return observers;
   }

   public void setObservers(int observers)
   {
      this.observers = observers;
   }

   public int getHerbariumCode()
   {
      return herbariumCode;
   }

   public void setHerbariumCode(int herbariumCode)
   {
      this.herbariumCode = herbariumCode;
   }

   public int getHerbary()
   {
      return herbary;
   }

   public void setHerbary(int herbary)
   {
      this.herbary = herbary;
   }

   public int getHabitat()
   {
      return habitat;
   }

   public void setHabitat(int habitat)
   {
      this.habitat = habitat;
   }

   public int getTimeOfDay()
   {
      return timeOfDay;
   }

   public void setTimeOfDay(int timeOfDay)
   {
      this.timeOfDay = timeOfDay;
   }

   public void setValidationComment(int validationComment)
   {
      this.validationComment = validationComment;
   }

   public int getValidationComment()
   {
      return validationComment;
   }

   public int getRemark()
   {
      return remark;
   }

   public void setRemark(int remark)
   {
      this.remark = remark;
   }

   public int getCiteId()
   {
      return citeId;
   }

   public void setCiteId(int citeId)
   {
      this.citeId = citeId;
   }

   public int getCiteComment()
   {
      return citeComment;
   }

   public void setCiteComment(int citeComment)
   {
      this.citeComment = citeComment;
   }
}
