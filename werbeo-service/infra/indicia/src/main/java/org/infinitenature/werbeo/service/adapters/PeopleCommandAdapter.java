package org.infinitenature.werbeo.service.adapters;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.infra.commands.PeopleCommands;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.IndiciaApi;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBPersonAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBPersonAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.PersonAttributeValuesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service
public class PeopleCommandAdapter implements PeopleCommands
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PeopleCommandAdapter.class);
   @Autowired
   private IndiciaApi indiciaApi;
   @Autowired
   private PersonAttributeValuesRepository personAttributeValuesRepository;
   @Autowired
   private AdditionalAttributeConfigFactory configFactory;
   @Autowired
   private IndiciaUserQueries indiciaUserQueries;

   @Override
   @Transactional
//   @CacheEvict(cacheNames = "people", allEntries = true)
   public int saveOrUpdate(Person person, Context context)
   {
      Map<String, String> existingAttributes = person.getId() != null
            ? loadAdditionalAttributeValues(person.getId())
            : new HashMap<>();
      Map<String, String> newAttributes = createAttributes(person);

      return indiciaApi.savePerson(PersonMapper.INSTANCE.map(person), CustomAttributeMerger.merge(newAttributes, existingAttributes), context.getPortal().getId(),
            indiciaUserQueries.getUserId(context));
   }



   private Map<String, String> createAttributes(Person person)
   {
      Map<String, String> attributes = new HashMap<>();
      AdditionalPersonAttributeConfig config = configFactory.getPersonConfig();
//      attributes.put(String.valueOf(config.getJobId()),
//            person..getAvailability().toString());
//      attributes.put(String.valueOf(config.getContainerId()),
      return attributes;
   }

   private Map<String, String> loadAdditionalAttributeValues(Integer personId)
   {
      QDBPersonAttributeValue qPersonAttributeValues = QDBPersonAttributeValue.dBPersonAttributeValue;

      Iterable<DBPersonAttributeValue> jpaValues = personAttributeValuesRepository
            .findAll(qPersonAttributeValues.person.id.eq(personId)
                  .and(qPersonAttributeValues.deleted.isFalse()));
      Map<String, String> additionalValues = new HashMap<>();
      jpaValues.forEach(jpaValue ->
      {
         String key = jpaValue.getPersonAttribute().getId() + ":"
               + jpaValue.getId();
         String value = AttributeUtils.getValue(jpaValue);
         // TODO remove null check after implementing all types of values
         if (value != null)
         {
            additionalValues.put(key, value);
         }
      });

      return additionalValues;
   }
}
