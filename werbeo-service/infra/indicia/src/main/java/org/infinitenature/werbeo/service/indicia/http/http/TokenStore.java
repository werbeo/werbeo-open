package org.infinitenature.werbeo.service.indicia.http.http;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TokenStore<S extends AuthToken>
{
   private Map<Integer, Queue<S>> queues = new HashMap<>();

   public S get(int websiteId)
   {
      return saveGet(websiteId).peek();
   }

   private Queue<S> saveGet(int websiteId)
   {
      Queue<S> queue = queues.get(websiteId);
      if (queue == null)
      {
         queue = new ConcurrentLinkedQueue<>();
         queues.put(websiteId, queue);
      }
      return queue;
   }

   public void put(int websiteId, S authToken)
   {
      saveGet(websiteId).add(authToken);
   }

   public void remove(int websiteId, S authToken)
   {
      saveGet(websiteId).remove(authToken);
   }
}