package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "occurrence_attributes")
public class DBOccurrenceAttribute extends DBAbstractAttribute
{
   private static final long serialVersionUID = 1L;
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "occurrenceAttribute")
   private Collection<DBOccurrenceAttributesWebsites> occurrenceAttributesWebsitesCollection;
   @OneToMany(mappedBy = "occurrenceAttribute")
   private Collection<DBOccurrenceAttributeValue> occurrenceAttributeValuesCollection;

   public Collection<DBOccurrenceAttributesWebsites> getOccurrenceAttributesWebsitesCollection()
   {
      return occurrenceAttributesWebsitesCollection;
   }

   public void setOccurrenceAttributesWebsitesCollection(
         Collection<DBOccurrenceAttributesWebsites> occurrenceAttributesWebsitesCollection)
   {
      this.occurrenceAttributesWebsitesCollection = occurrenceAttributesWebsitesCollection;
   }

   public Collection<DBOccurrenceAttributeValue> getOccurrenceAttributeValuesCollection()
   {
      return occurrenceAttributeValuesCollection;
   }

   public void setOccurrenceAttributeValuesCollection(
         Collection<DBOccurrenceAttributeValue> occurrenceAttributeValuesCollection)
   {
      this.occurrenceAttributeValuesCollection = occurrenceAttributeValuesCollection;
   }

}
