package org.infinitenature.werbeo.service.adapters;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class AdditionalSurveyAttributeConfig
{
   private int availabitlityId;
   private int containerId;
   private int obfuscationPoliciesId;
   private int werbeoOriginalId;
   private int allowDataEntryId;
   private int tagId;

   public int getAvailabitlityId()
   {
      return availabitlityId;
   }

   public void setAvailabitlityId(int availabitlityId)
   {
      this.availabitlityId = availabitlityId;
   }

   public int getContainerId()
   {
      return containerId;
   }

   public void setContainerId(int containerId)
   {
      this.containerId = containerId;
   }

   public int getWerbeoOriginalId()
   {
      return werbeoOriginalId;
   }

   public void setWerbeoOriginalId(int werbeoOriginalId)
   {
      this.werbeoOriginalId = werbeoOriginalId;
   }

   public int getAllowDataEntryId()
   {
      return allowDataEntryId;
   }

   public void setAllowDataEntryId(int allowDataEntryId)
   {
      this.allowDataEntryId = allowDataEntryId;
   }

   public int getTagId()
   {
      return tagId;
   }

   public void setTagId(int tagId)
   {
      this.tagId = tagId;
   }

   @Override
   public String toString()
   {
      return AdditionalSurveyAttributeConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return AdditionalSurveyAttributeConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return AdditionalSurveyAttributeConfigBeanUtil.doEquals(this, obj);
   }

   public int getObfuscationPoliciesId()
   {
      return obfuscationPoliciesId;
   }

   public void setObfuscationPoliciesId(int obfuscationPoliciesId)
   {
      this.obfuscationPoliciesId = obfuscationPoliciesId;
   }

}
