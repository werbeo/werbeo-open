package org.infinitenature.werbeo.service.indicia.http;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 * Enumeration for the indicia data service api {@code sharing} parameter.
 * 
 * @author dve
 *
 */
public enum SharingOption
{
   REPORTING("reporting"), PEER_REVIEW("peer_review"), VERIFICATION(
         "verification"), DATA_FLOW("data_flow"), MODERATION("moderation");

   private final String indiciaString;
   private final static String indiciaParameterName = "sharing";

   private SharingOption(String indiciaString)
   {
      this.indiciaString = indiciaString;
   }

   @Override
   public String toString()
   {
      return indiciaString;
   }

   public NameValuePair asRequestParameter()
   {
      return new BasicNameValuePair(indiciaParameterName, indiciaString);
   }
}
