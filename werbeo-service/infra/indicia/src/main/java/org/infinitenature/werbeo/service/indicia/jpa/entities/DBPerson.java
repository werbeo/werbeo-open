/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author dve
 */
@Entity
@Table(name = "people")
public class DBPerson extends Base
{
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 50)
   @Column(name = "first_name")
   private String firstName;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 50)
   @Column(name = "surname")
   private String lastName;
   @Size(max = 6)
   private String initials;
   @Size(max = 100)
   @Column(name = "email_address")
   private String emailAddress;
   @Size(max = 1000)
   @Column(name = "website_url")
   private String websiteUrl;
   @Size(max = 200)
   private String address;
   @Size(max = 50)
   @Column(name = "external_key")
   private String externalKey;

   public DBPerson()
   {
   }

   public DBPerson(Integer id)
   {
      this.id = id;
   }

   public DBPerson(Integer id, String firstName, String surname, Date createdOn,
         Date updatedOn, boolean deleted)
   {
      this.id = id;
      this.firstName = firstName;
      this.lastName = surname;
      this.createdOn = createdOn;
      this.updatedOn = updatedOn;
      this.deleted = deleted;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName(String surname)
   {
      this.lastName = surname;
   }

   public String getInitials()
   {
      return initials;
   }

   public void setInitials(String initials)
   {
      this.initials = initials;
   }

   public String getEmailAddress()
   {
      return emailAddress;
   }

   public void setEmailAddress(String emailAddress)
   {
      this.emailAddress = emailAddress;
   }

   public String getWebsiteUrl()
   {
      return websiteUrl;
   }

   public void setWebsiteUrl(String websiteUrl)
   {
      this.websiteUrl = websiteUrl;
   }

   public String getAddress()
   {
      return address;
   }

   public void setAddress(String address)
   {
      this.address = address;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }



   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBPerson))
      {
         return false;
      }
      DBPerson other = (DBPerson) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.People[ id=" + id + " ]";
   }

}
