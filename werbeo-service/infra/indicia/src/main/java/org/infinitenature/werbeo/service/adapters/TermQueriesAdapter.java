package org.infinitenature.werbeo.service.adapters;

import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.infra.query.TermQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TermQueriesAdapter implements TermQueries
{
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;
}
