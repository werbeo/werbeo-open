package org.infinitenature.werbeo.service.adapters;

import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SurveyMapper
{
   public static final SurveyMapper INSTANCE = Mappers
         .getMapper(SurveyMapper.class);

   @Mapping(target = "createdOn", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "updatedBy", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "createdBy", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "updatedOn", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "title", source = "name")
   @Mapping(target = "website", ignore = true)
   IndiciaSurvey map(Survey survey);

}
