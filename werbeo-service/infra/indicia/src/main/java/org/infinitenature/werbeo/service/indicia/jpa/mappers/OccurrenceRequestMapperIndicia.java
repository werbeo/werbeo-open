package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.mapstruct.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Mapper(componentModel = "spring")
public abstract class OccurrenceRequestMapperIndicia
      implements RequestMapperIndicia<OccurrenceSortField>
{

   static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceRequestMapperIndicia.class);

   @Override
   public Function<OccurrenceSortField, String[]> getSortFieldMapper()
   {
      return sortField ->
      {
         switch (sortField)
         {
         case MOD_DATE:
            return new String[] { "updatedOn" };
         case TAXON:
            return new String[] { "taxaTaxonList.taxon.taxon" };
         case DETERMINER:
            return new String[] { "determiner.lastName",
                  "determiner.firstName" };
         case DATE:
            return new String[] { "sample.startDate", "sample.endDate" };
         case MEDIA:
            return new String[] { "occurrenceMedia.id" };
         case VALIDATION_STATUS:
            LOGGER.warn("No sort for VALIDATION_STATUS defined");
            return new String[] { "id" };
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
