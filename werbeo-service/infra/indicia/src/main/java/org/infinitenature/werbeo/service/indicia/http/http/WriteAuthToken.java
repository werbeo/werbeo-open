package org.infinitenature.werbeo.service.indicia.http.http;

public class WriteAuthToken extends AuthToken
{
   private ReadWriteNonce readWriteNonce;

   public WriteAuthToken(ReadWriteNonce readWriteNonce, String password)
   {
      super(password);
      this.readWriteNonce = readWriteNonce;
   }

   @Override
   public String getNonce()
   {
      return readWriteNonce.getWriteNonce();
   }

   @Override
   public String getToken()
   {
      return createAuthToken(getNonce());
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result
            + ((readWriteNonce == null) ? 0 : readWriteNonce.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
         return true;
      if (!super.equals(obj))
         return false;
      if (getClass() != obj.getClass())
         return false;
      WriteAuthToken other = (WriteAuthToken) obj;
      if (readWriteNonce == null)
      {
         if (other.readWriteNonce != null)
            return false;
      } else if (!readWriteNonce.equals(other.readWriteNonce))
         return false;
      return true;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("WriteAuthToken@");
      builder.append(System.identityHashCode(this));
      builder.append(" [readWriteNonce=");
      builder.append(readWriteNonce);
      builder.append("]");
      return builder.toString();
   }
}