package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "termlists_terms")
public class DBTermlistTerm extends Base
{
   @JoinColumn(name = "term_id", referencedColumnName = "id")
   @OneToOne
   private DBTerm term;
   @JoinColumn(name = "termlist_id", referencedColumnName = "id")
   @OneToOne
   private DBTermlist termlist;
   @Column(name = "meaning_id")
   private int meaningId;
   @Column(name = "preferred")
   private boolean preferred;

   public DBTermlistTerm()
   {
      super();
   }

   public DBTermlistTerm(int id, DBTerm term)
   {
      super();
      this.id = id;
      this.term = term;
   }

   public DBTerm getTerm()
   {
      return term;
   }

   public void setTerm(DBTerm term)
   {
      this.term = term;
   }

   public DBTermlist getTermlist()
   {
      return termlist;
   }

   public void setTermlist(DBTermlist termlist)
   {
      this.termlist = termlist;
   }

   public int getMeaningId()
   {
      return meaningId;
   }

   public void setMeaningId(int meaningId)
   {
      this.meaningId = meaningId;
   }

   public boolean isPreferred()
   {
      return preferred;
   }

   public void setPreferred(boolean preferred)
   {
      this.preferred = preferred;
   }
}
