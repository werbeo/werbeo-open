package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;
import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.AbstractComment;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceComment;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleComment;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(config = CentralConfigIndiciaJPA.class, uses = {
      PersonMapperIndicia.class })
public interface CommentMapperIndicia
{

   List<SampleComment> mapSampleComments(List<DBSampleComment> content,
         @Context UUID sampleId);

   @Mapping(target = "targetId", ignore = true)

   SampleComment map(DBSampleComment comment, @Context UUID sampleId);

   List<OccurrenceComment> mapOccurrenceComments(
         List<DBOccurrenceComment> content, @Context UUID occurrenceId);

   @Mapping(target = "targetId", ignore = true)
   @Mapping(target = "allowedOperations", ignore = true) // set in core-impl
   @Mapping(target = "obfuscated", ignore = true) // set in core-impl
   OccurrenceComment map(DBOccurrenceComment comment,
         @Context UUID occurrenceId);

   @AfterMapping
   default void addTargetId(@MappingTarget AbstractComment comment,
         @Context UUID targetId)
   {
      comment.setTargetId(targetId);
   }

}
