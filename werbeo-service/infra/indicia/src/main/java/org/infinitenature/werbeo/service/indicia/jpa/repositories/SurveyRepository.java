package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurvey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SurveyRepository extends JpaRepository<DBSurvey, Integer>,
      QuerydslPredicateExecutor<DBSurvey>
{

}
