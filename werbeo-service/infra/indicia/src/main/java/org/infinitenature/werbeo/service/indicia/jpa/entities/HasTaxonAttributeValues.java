package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Set;

public interface HasTaxonAttributeValues
{
   Set<DBTaxonAttributeValue> getTaxonAttributeValues();
}
