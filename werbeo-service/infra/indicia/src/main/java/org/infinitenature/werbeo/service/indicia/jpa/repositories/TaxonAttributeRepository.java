package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxonAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface TaxonAttributeRepository
      extends JpaRepository<DBTaxonAttribute, Integer>,
      QuerydslPredicateExecutor<DBTaxonAttribute>
{

}
