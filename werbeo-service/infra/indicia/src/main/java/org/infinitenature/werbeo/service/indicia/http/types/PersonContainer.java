package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "people")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonContainer
{
   @XmlElement(name = "person")
   private List<Person> persons = new ArrayList<>();

   public List<Person> getPersons()
   {
      return persons;
   }

   public void setPersons(List<Person> persons)
   {
      this.persons = persons;
   }

}
