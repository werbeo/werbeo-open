package org.infinitenature.werbeo.service.indicia.http.types;

public interface MediaType
{
   public int getId();

   public String getTypeDescription();
}
