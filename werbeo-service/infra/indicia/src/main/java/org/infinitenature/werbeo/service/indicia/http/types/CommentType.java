package org.infinitenature.werbeo.service.indicia.http.types;

public interface CommentType extends IndiciaType
{
   public int getTargetId();

   public void setTargetId(int targetId);

   public String getComment();

   public void setComment(String comment);
}