package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.mapstruct.Mapper;

import java.util.function.Function;

@Mapper(componentModel = "spring")
public interface OccurrenceCentroidRequestMapperIndicia extends RequestMapperIndicia<OccurrenceCentroidSortField>
{
   @Override
   default Function<OccurrenceCentroidSortField, String[]> getSortFieldMapper()
   {
      return sortField ->
      {
         switch (sortField)
         {
         case DATE:
         default:
            return new String[] { "sample.startDate", "sample.endDate" };
         }
      };
   }
}
