package org.infinitenature.werbeo.service.indicia.http.types;

public interface User
{
   public abstract int getId();

   public abstract String getName();

   public int getPersonId();
}