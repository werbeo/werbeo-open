package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "occurrence_attributes_websites")
public class DBOccurrenceAttributesWebsites extends DBAbstractAttributesWebsites
{
   private static final long serialVersionUID = 1L;
   @JoinColumn(name = "occurrence_attribute_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private DBOccurrenceAttribute occurrenceAttribute;

   public DBOccurrenceAttribute getOccurrenceAttribute()
   {
      return occurrenceAttribute;
   }

   public void setOccurrenceAttribute(
         DBOccurrenceAttribute occurrenceAttribute)
   {
      this.occurrenceAttribute = occurrenceAttribute;
   }

}