package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxaList;

public interface TaxaListRepository
      extends QueryDslPredicateAndProjectionExecutor<DBTaxaList, Integer>
{

}
