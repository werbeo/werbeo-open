package org.infinitenature.werbeo.service.indicia.http;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.message.BasicNameValuePair;
import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaException;
import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaHttpException;
import org.infinitenature.werbeo.service.indicia.http.http.IndiciaHttpConnector;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaType;
import org.infinitenature.werbeo.service.indicia.http.types.Location;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceComment;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceMedium;
import org.infinitenature.werbeo.service.indicia.http.types.Person;
import org.infinitenature.werbeo.service.indicia.http.types.SampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.http.types.SampleComment;
import org.infinitenature.werbeo.service.indicia.http.types.SampleSubmissionContainer;
import org.infinitenature.werbeo.service.indicia.http.types.SurveySubmissionContrainer;
import org.infinitenature.werbeo.service.indicia.http.types.UserIdentifier;
import org.infinitenature.werbeo.service.indicia.http.types.submission.Field;
import org.infinitenature.werbeo.service.indicia.http.types.submission.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import external.org.json.JSONArray;
import external.org.json.JSONException;
import external.org.json.JSONObject;

@SuppressWarnings("serial")
@Component
public class IndiciaApiImpl implements IndiciaApi
{

   private static final String USER_ID_KEY = "user_id";
   private static final String VALUE_DELETED_TRUE = "t";
   private static final String KEY_DELETED = "deleted";
   private ThreadLocal<Model> lastRequest = new ThreadLocal<>();

   private static final Logger LOGGER = LoggerFactory
         .getLogger(IndiciaApiImpl.class);
   @Autowired
   private IndiciaHttpConnector httpConnector;

   @Override
   public String getConnectionURL()
   {
      return httpConnector.toString();
   }

   private String submit(Model model, int webSiteId, int indiciaUserId)
   {
      LOGGER.debug("Submitting model: {} ", model);
      try
      {
         List<NameValuePair> params = new ArrayList<>();
         params.add(new BasicNameValuePair("submission",
               model.getJsonObject().toString()));
         params.add(new BasicNameValuePair(USER_ID_KEY,
               String.valueOf(indiciaUserId)));
         if (LOGGER.isDebugEnabled())
         {
            LOGGER.debug("submission: {} ", model.getJsonObject().toString(3));
         }
         lastRequest.set(model);
         try
         {
            String response = httpConnector.doHttpPostRequestRW(
                  "/index.php/services/data/save", params, webSiteId);
            LOGGER.debug("response: {} ", response);
            return response;
         } catch (IndiciaHttpException e)
         {
            LOGGER.error(
                  "Request: " + lastRequest.get().getJsonObject().toString(3));
            LOGGER.error("Response: {}", e.getResponse(), e);
            throw e;
         } catch (IndiciaException e)
         {
            LOGGER.error(
                  "Request: " + lastRequest.get().getJsonObject().toString(3));
            LOGGER.error("Response: ", e);
            throw e;
         }
      } catch (JSONException e)
      {
         LOGGER.error("Error parsing json data", e);
         throw new IndiciaException("Error parsing json data", e);
      }

   }

   @Override
   public int saveLocation(Location location, int webSiteId, int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(location), webSiteId,
            indiciaUserId);
   }

   private int save(Model model, int webSiteId, int indiciaUserId)
   {
      try
      {
         String response = submit(model, webSiteId, indiciaUserId);
         return handleResponse(response);
      } catch (Exception e)
      {
         String message = "Error saving " + model.getId();
         LOGGER.error(message, e);
         throw new IndiciaException(message, e);
      }
   }

   @Override
   public int saveOccurence(IndiciaOccurrence occurrence,
         Map<String, String> additionAtributes, int webSiteId,
         int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(occurrence, additionAtributes),
            webSiteId, indiciaUserId);
   }

   @Override
   public int saveOccurrenceComment(OccurrenceComment occurrenceComment,
         int webSiteId, int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(occurrenceComment), webSiteId,
            indiciaUserId);
   }

   @Override
   public int save(OccurrenceMedium occurrenceMedium, int webSiteId, int userId)
   {
      return save(SubmissionMapper.createModel(occurrenceMedium), webSiteId,
            userId);
   }

   @Override
   public int saveSampleComment(SampleComment sampleComment, int webSiteId,
         int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(sampleComment), webSiteId,
            indiciaUserId);
   }

   @Override
   public int savePerson(Person person,
         Map<String, String> additionalAttributes, int webSiteId,
         int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(person, additionalAttributes),
            webSiteId, indiciaUserId);
   }

   @Override
   public int saveSample(IndiciaSample sample,
         Map<String, String> additionAtributes, int webSiteId,
         int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(sample, additionAtributes),
            webSiteId, indiciaUserId);
   }

   @Override
   public int save(IndiciaSurvey survey, int webSiteId, int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(survey, webSiteId), webSiteId,
            indiciaUserId);
   }

   @Override
   public int saveSampleAttributeValue(
         SampleAttributeValue sampleAttributeValue, int webSiteId,
         int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(sampleAttributeValue), webSiteId,
            indiciaUserId);
   }

   @Override
   public int save(SampleSubmissionContainer sampleSubmissionContainer,
         int webSiteId, int indiciaUserId)
   {
      return save(SubmissionMapper.createModel(sampleSubmissionContainer),
            webSiteId, indiciaUserId);
   }

   @Override
   public int save(SurveySubmissionContrainer surveySubmissionContrainer,
         int webSiteId, int indiciaUserId)
   {
      return save(
            SubmissionMapper.createModel(surveySubmissionContrainer, webSiteId),
            webSiteId, indiciaUserId);
   }

   @Override
   public int save(OccurrenceAttributeValue occurrenceAttributeValue,
         int webSiteId, int indiciaUserId)
   {
      return save(SubmissionMapper.createModelInt(occurrenceAttributeValue),
            webSiteId, indiciaUserId);
   }

   @Override
   public void delete(Person person, int webSiteId, int indiciaUserId)
   {
      try
      {
         Set<Field> fields = new HashSet<>();
         SubmissionMapper.addBaseFields(person, fields);
         fields.add(new Field(KEY_DELETED, VALUE_DELETED_TRUE));
         Model model = new Model("person", fields);
         String response = submit(model, webSiteId, indiciaUserId);
         handleResponse(response);
         return;
      } catch (Exception e)
      {
         throw new IndiciaException("Error deleteding person", e);
      }
   }

   @Override
   public void delete(IndiciaType type, int webSiteId, int indiciaUserId)
   {
      try
      {
         Set<Field> fields = new HashSet<>();
         SubmissionMapper.addBaseFields(type, fields);
         fields.add(new Field(IndiciaApi.KEY_WEBSITE_ID, webSiteId));
         fields.add(new Field(KEY_DELETED, VALUE_DELETED_TRUE));
         Model model = new Model(type.getModelName(), fields);
         String response = submit(model, webSiteId, indiciaUserId);
         handleResponse(response);
      } catch (Exception e)
      {
         LOGGER.error("Error deleteding entity", e);
         throw new IndiciaException("Error deleteding entity", e);
      }
   }

   @Override
   public void delete(IndiciaSurvey survey, int webSiteId, int indiciaUserId)
   {
      try
      {
         Set<Field> fields = new HashSet<>();
         SubmissionMapper.addBaseFields(survey, fields);
         fields.add(new Field(IndiciaApi.KEY_WEBSITE_ID, webSiteId));
         fields.add(new Field(KEY_DELETED, VALUE_DELETED_TRUE));

         Model model = new Model("survey", fields);
         String response = submit(model, webSiteId, indiciaUserId);
         handleResponse(response);
         return;
      } catch (Exception e)
      {
         LOGGER.error("Error deleteding survey", e);
         throw new IndiciaException("Error deleteding survey", e);
      }
   }

   private int handleResponse(String response)
   {
      try
      {
         JSONObject responseJSON = new JSONObject(response);
         if (LOGGER.isDebugEnabled())
         {
            LOGGER.debug("Response: {}", responseJSON.toString(2));
         }
         return Integer.parseInt(responseJSON.get("outer_id").toString());
      } catch (JSONException e)
      {
         LOGGER.error("Error submitting data", e);
         try
         {
            LOGGER.error(
                  "Request: " + lastRequest.get().getJsonObject().toString(3));
         } catch (JSONException e1)
         {
            LOGGER.error("Request was no vaild json", e1);
         }
         LOGGER.error("Response: " + response);
         throw new IndiciaException(
               "Error submitting data: " + createErrorMessage(response), e);
      }
   }

   private String createErrorMessage(String response)
   {
      StringBuilder sb = new StringBuilder();
      try
      {
         JSONObject responseJSON = new JSONObject(response);
         sb.append("Error: ");
         sb.append(responseJSON.get("error").toString());
         JSONObject errors = responseJSON.getJSONObject("errors");
         for (String name : JSONObject.getNames(errors))
         {
            sb.append(", ");
            sb.append(name);
            sb.append(": ");
            sb.append(errors.get(name).toString());
         }
      } catch (Exception e)
      {
         LOGGER.error("Error creating error message", e);
         sb.append(response);
      }
      return sb.toString();
   }

   @Override
   public UserIdentifier identifyUser(Map<String, String> identifiers,
         String lastName, String firstName, Integer cmsUserId, int webSiteId)
   {
      JSONArray jsonIdententifiers = new JSONArray();
      try
      {

         for (String identifierType : identifiers.keySet())
         {
            JSONObject jsonIdentifier = new JSONObject();
            jsonIdentifier.put("type", identifierType);
            jsonIdentifier.put("identifier", identifiers.get(identifierType));
            jsonIdententifiers.put(jsonIdentifier);
         }
      } catch (JSONException e)
      {
         // should never happen
         LOGGER.error("Failure creating json object", e);
         throw new IndiciaException("Failure creating json object", e);
      }
      JSONObject jsonObject = null;
      String response = null;
      try
      {
         List<NameValuePair> params = new ArrayList<>();

         params.add(new BasicNameValuePair("identifiers",
               jsonIdententifiers.toString()));
         params.add(new BasicNameValuePair("surname", lastName));
         params.add(new BasicNameValuePair("first_name", firstName));
         if (cmsUserId != null)
         {
            params.add(new BasicNameValuePair("cms_user_id",
                  String.valueOf(cmsUserId)));
         }
         response = httpConnector.doHttpPostRequestRW(
               "/index.php/services/user_identifier/get_user_id", params,
               webSiteId);
         jsonObject = new JSONObject(response);


         return determineUserId(webSiteId, jsonObject);
      } catch (JSONException e)
      {
         LOGGER.error("No userId in response", e);
         try
         {
            if (jsonObject != null)
            {
               String error = jsonObject.getString("error");
               LOGGER.error("Failure identifing user: {}", error);
               throw new IndiciaException("Failure identifing user" + error);
            } else
            {
               LOGGER.error("Failure identifing user ");
               throw new IndiciaException("Failure identifing user");
            }
         } catch (JSONException e1)
         {
            LOGGER.error("Failure in response: " + response);
            throw new IndiciaException("Failure in response from webservice");
         }
      }
   }

   private UserIdentifier determineUserId(int webSiteId, JSONObject jsonObject)
         throws JSONException
   {
      try
      {
         int userId = jsonObject.getInt("userId");
         return new UserIdentifier(userId);
      }
      catch(JSONException ee)
      {
         LOGGER.error("Multiple users found");
         // if multiple possible mathces, choose the first user with right portalId/websiteId
         JSONArray userArray = jsonObject.getJSONArray("possibleMatches");
         for(int i = 0; i < userArray.length(); i++)
         {
            JSONObject user = (JSONObject)userArray.get(i);
            if(user.getInt("website_id") == webSiteId)
            {
               LOGGER.error("Choose user:");
               LOGGER.error(String.valueOf(user.getInt(USER_ID_KEY)));
               return new UserIdentifier(user.getInt(USER_ID_KEY));
            }
         }
         // not found via websiteId? return first user
         int userId = ((JSONObject)jsonObject.getJSONArray("possibleMatches").get(0)).getInt(USER_ID_KEY);

         return new UserIdentifier(userId);
      }
   }

   @Override
   public String uploadFile(InputStream inputStream, String fileName,
         int webSiteId)
   {
      Validate.notNull(inputStream,
            "An InputStream with image data is needed.");
      Validate.notEmpty(fileName, "A file name is needed.");

      InputStreamBody isb = new InputStreamBody(inputStream, fileName);
      Map<String, ContentBody> parts = new HashMap<>();
      parts.put("media_upload", isb);
      return httpConnector.doHttpMulitPartPost(
            "/index.php/services/data/handle_media", parts, webSiteId);
   }

   public void setHttpConnector(IndiciaHttpConnector httpConnector)
   {
      this.httpConnector = httpConnector;
   }

}