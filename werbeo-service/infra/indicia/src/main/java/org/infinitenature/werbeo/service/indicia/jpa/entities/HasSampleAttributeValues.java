package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Collection;

public interface HasSampleAttributeValues
{
   public Collection<DBSampleAttributeValue> getSampleAttributeValues();
}
