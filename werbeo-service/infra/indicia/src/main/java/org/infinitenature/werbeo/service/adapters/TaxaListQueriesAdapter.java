package org.infinitenature.werbeo.service.adapters;

import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxaListQueries;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxaList;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBTaxaList;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBTaxon;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.IdSortRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.TaxaListMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.TaxaListRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.TaxonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class TaxaListQueriesAdapter implements TaxaListQueries
{
   @Autowired
   private TaxaListRepository repo;
   @Autowired
   private TaxonRepository taxonRepo;
   @Autowired
   private TaxaListMapperIndicia mapper;
   @Autowired
   private IdSortRequestMapperIndicia requestMapper;
   @Autowired
   private InstanceConfig instanceConfig;

   @Override
   @Transactional(readOnly = true)
   public Slice<TaxaList, TaxaListSortField> find(TaxaListFilter filter,
         Context context, OffsetRequest<TaxaListSortField> offsetRequest)
   {
      BooleanExpression predicate = prepareQuery(filter, context);
      return new SliceImpl<>(mapper.map(repo
            .findAll(predicate, requestMapper.map(offsetRequest)).getContent()),
            offsetRequest);
   }

   private BooleanExpression prepareQuery(TaxaListFilter filter,
         Context context)
   {
      QDBTaxaList dbtaxalist = QDBTaxaList.dBTaxaList;
      BooleanExpression expression = dbtaxalist.deleted.isFalse();

      Set<Integer> configuredTaxonLists = instanceConfig
            .getTaxonListsPerPortal()
            .get(context.getPortal().getId());
      if (configuredTaxonLists != null)
      {
         expression = expression.and(dbtaxalist.id.in(configuredTaxonLists));
      }
      if (StringUtils.isNotBlank(filter.getName()))
      {
         expression = expression.and(dbtaxalist.name.eq(filter.getName()));
      }
      return expression;
   }

   private List<String> getAvailableGroups(DBTaxaList taxaList,
         Context context)
   {
      QDBTaxon qdbTaxon = QDBTaxon.dBTaxon;
      BooleanExpression predicate = qdbTaxon.deleted.isFalse()
            .and(qdbTaxon.taxaTaxonList.taxaList.id.eq(taxaList.getId()));
      return taxonRepo.findDistinctWithProjection(
            Projections.constructor(String.class, qdbTaxon.taxonGroup.title),
            predicate);
   }

   @Override
   public long count(TaxaListFilter filter, Context context)
   {
      return repo.count(prepareQuery(filter, context));
   }

   @Override
   @Transactional(readOnly = true)
   public TaxaList get(Integer id, Context context)
   {
      QDBTaxaList qTaxaList = QDBTaxaList.dBTaxaList;
      BooleanExpression expression = qTaxaList.id.eq(id)
            .and(qTaxaList.deleted.isFalse());
      DBTaxaList dbTaxaList = repo.findOne(expression).orElse(null);
      TaxaList map = mapper.map(dbTaxaList);
      if (map != null)
      {
         map.setTaxaGroups(getAvailableGroups(dbTaxaList, context));
      }
      return map;

   }

}
