package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrence_media")
@XmlAccessorType(XmlAccessType.FIELD)
public class OccurrenceMediaContainer
{
   @XmlElement(name = "occurrence_medium")
   private List<OccurrenceMedium> occurrenceMedia = new ArrayList<>();

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("OccurrenceMediaContainer@");
      builder.append(System.identityHashCode(this));
      builder.append(" [");
      if (occurrenceMedia != null)
      {
         builder.append("occurrenceMedia=");
         builder.append(occurrenceMedia);
      }
      builder.append("]");
      return builder.toString();
   }

   public List<OccurrenceMedium> getOccurrenceMedia()
   {
      return occurrenceMedia;
   }

   public void setOccurrenceMedia(List<OccurrenceMedium> occurrenceMedia)
   {
      this.occurrenceMedia = occurrenceMedia;
   }

}
