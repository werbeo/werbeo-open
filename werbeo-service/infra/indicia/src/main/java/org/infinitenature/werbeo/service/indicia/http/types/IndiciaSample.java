package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement(name = "sample")
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class IndiciaSample extends IndiciaTypeImpl
{
   public IndiciaSample()
   {

   }

   public IndiciaSample(int id)
   {
      this.id = id;
   }

   @Override
   public String getModelName()
   {
      return "sample";
   }

   @XmlElement(name = "date_type")
   private String dateType;

   @XmlElement(name = "date_start")
   private Date startDate;
   @XmlElement(name = "date_end")
   private Date endDate;
   // @NotNull
   /**
    * latitude, longitude
    */
   @XmlElement(name = "entered_sref")
   private String enteredSref;
   // @NotNull
   @XmlElement(name = "entered_sref_system")
   private String enteredSrefSystem;
   @XmlElement(name = "geom")
   private String geom;
   @XmlElement(name = "wkt")
   private String wkt;

   @XmlElement(name = "website_id")
   private int websiteId;
   @XmlElement(name = "recorder_names")
   private String recorderNames;
   @NotNull
   @XmlElement(name = "survey_id")
   private int surveyId;
   @XmlElement(name = "comment")
   private String comment;
   @XmlElement(name = "parent_id")
   private int parentId;

   private int locationId;
   private String year;
   private String month;
   private String date;
   private int sampleMethodId;

   public int getSampleMethodId()
   {
      return sampleMethodId;
   }

   public void setSampleMethodId(int sampleMethodId)
   {
      this.sampleMethodId = sampleMethodId;
   }

   public String getYear()
   {
      return year;
   }

   public void setYear(String year)
   {
      this.year = year;
   }

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public int getSurveyId()
   {
      return surveyId;
   }

   public void setSurveyId(int surveyId)
   {
      this.surveyId = surveyId;
   }

   public String getRecorderNames()
   {
      return recorderNames;
   }

   public void setRecorderNames(String recorderNames)
   {
      this.recorderNames = recorderNames;
   }

   public String getDateType()
   {
      return dateType;
   }

   public Date getEndDate()
   {
      return endDate;
   }

   public String getEnteredSref()
   {
      return enteredSref;
   }

   public String getEnteredSrefSystem()
   {
      return enteredSrefSystem;
   }

   public String getGeom()
   {
      return geom;
   }

   public Date getStartDate()
   {
      return startDate;
   }

   public int getWebsiteId()
   {
      return websiteId;
   }

   public void setDateType(String dateType)
   {
      this.dateType = dateType;
   }

   public void setEndDate(Date endDate)
   {
      this.endDate = endDate;
   }

   public void setEnteredSref(String enterdSref)
   {
      this.enteredSref = enterdSref;
   }

   public void setEnteredSrefSystem(String enterdSrefSystem)
   {
      this.enteredSrefSystem = enterdSrefSystem;
   }

   public void setGeom(String geom)
   {
      this.geom = geom;
   }

   public void setStartDate(Date startDate)
   {
      this.startDate = startDate;
   }

   public void setWebsiteId(int websiteId)
   {
      this.websiteId = websiteId;
   }

   public String getWkt()
   {
      return wkt;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

   public int getParentId()
   {
      return parentId;
   }

   public void setParentId(int parentId)
   {
      this.parentId = parentId;
   }

   @Override
   public String toString()
   {
      return IndiciaSampleBeanUtil.doToString(this);
   }

   public void setLocationId(int locationId)
   {
      this.locationId = locationId;
   }

   public int getLocationId()
   {
      return locationId;
   }

   public String getMonth()
   {
      return month;
   }

   public void setMonth(String month)
   {
      this.month = month;
   }

   public void setDate(String date)
   {
      this.date = date;
   }

   public String getDate()
   {
      return date;
   }
}
