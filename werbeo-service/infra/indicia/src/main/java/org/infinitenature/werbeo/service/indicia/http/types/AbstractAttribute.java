package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractAttribute
{
   @XmlElement(name = "id")
   private int id;
   @XmlElement(name = "caption")
   private String caption;

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Attribute [id=");
      builder.append(id);
      builder.append(", ");
      if (caption != null)
      {
         builder.append("caption=");
         builder.append(caption);
      }
      builder.append("]");
      return builder.toString();
   }

}