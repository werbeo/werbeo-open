package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "survey")
@XmlAccessorType(XmlAccessType.FIELD)
public class IndiciaSurvey extends IndiciaTypeImpl
{
   public IndiciaSurvey()
   {

   }

   public IndiciaSurvey(int id)
   {
      this.id = id;
   }

   public IndiciaSurvey(String title)
   {
      this.title = title;
   }

   public IndiciaSurvey(String title, int parentId)
   {
      super();
      this.title = title;
      this.parentId = parentId;
   }

   @Override
   public String getModelName()
   {
      return "survey";
   }

   @XmlElement(name = "title")
   private String title;
   @XmlElement(name = "description")
   private String description;
   @XmlElement(name = "website")
   private WebsiteAsSubelement websiteAsSubelement = new WebsiteAsSubelement();
   @XmlElement(name = "parent_id")
   private int parentId;
   @XmlElement(name = "owner")
   private PersonAsSubelement owner;

   public WebsiteAsSubelement getWebsite()
   {
      return websiteAsSubelement;
   }

   public void setWebsite(WebsiteAsSubelement websiteAsSubelement)
   {
      this.websiteAsSubelement = websiteAsSubelement;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public int getParentId()
   {
      return parentId;
   }

   public void setParentId(int parentId)
   {
      this.parentId = parentId;
   }

   public PersonAsSubelement getOwner()
   {
      return owner;
   }

   public void setOwner(PersonAsSubelement owner)
   {
      this.owner = owner;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Survey@");
      builder.append(System.identityHashCode(this));
      builder.append(" [id=");
      builder.append(id);
      builder.append(", ");
      if (createdOn != null)
      {
         builder.append("createdOn=");
         builder.append(createdOn);
         builder.append(", ");
      }
      if (updatedOn != null)
      {
         builder.append("updatedOn=");
         builder.append(updatedOn);
         builder.append(", ");
      }
      if (createdBy != null)
      {
         builder.append("createdBy=");
         builder.append(createdBy);
         builder.append(", ");
      }
      if (updatedBy != null)
      {
         builder.append("updatedBy=");
         builder.append(updatedBy);
         builder.append(", ");
      }
      if (title != null)
      {
         builder.append("title=");
         builder.append(title);
         builder.append(", ");
      }
      if (description != null)
      {
         builder.append("description=");
         builder.append(description);
         builder.append(", ");
      }
      if (websiteAsSubelement != null)
      {
         builder.append("websiteAsSubelement=");
         builder.append(websiteAsSubelement);
         builder.append(", ");
      }
      builder.append("parentId=");
      builder.append(parentId);
      builder.append(", ");
      if (owner != null)
      {
         builder.append("owner=");
         builder.append(owner);
      }
      builder.append("]");
      return builder.toString();
   }
}