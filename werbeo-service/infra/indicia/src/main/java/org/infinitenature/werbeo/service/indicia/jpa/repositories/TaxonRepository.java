package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.util.Set;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TaxonRepository extends JpaRepository<DBTaxon, Integer>,
      QueryDslPredicateAndProjectionExecutor<DBTaxon, Integer>
{

   @Query(value = "WITH RECURSIVE search_taxa(top_id, id, low_meaning_id, high_meaning_id, distance) AS (\n"
         + "         SELECT t.id,\n" //
         + "            t.id,\n" //
         + "            t.taxon_meaning_id,\n"//
         + "            t.taxon_meaning_id,\n"//
         + "            0\n"//
         + "           FROM taxa_taxon_lists t\n"//
         + "        UNION ALL\n"//
         + "         SELECT tt.id,\n"//
         + "            t.id,\n" //
         + "            t.taxon_meaning_id,\n"//
         + "            tt.high_meaning_id,\n" //
         + "            tt.distance + 1\n"
         + "           FROM taxa_taxon_lists t,\n"//
         + "            search_taxa tt\n"//
         + "          WHERE t.parent_id = tt.id\n" //
         + "        )\n"//
         + " SELECT DISTINCT search_taxa.low_meaning_id\n"//
         + "   FROM search_taxa\n"//
         + "   WHERE search_taxa.high_meaning_id = (SELECT DISTINCT taxon_meaning_id FROM taxa_taxon_lists WHERE id = ?1)"//
         , nativeQuery = true)
   public Set<Integer> findChildTaxonMeaningIds(int taxonId);

   @Query(value = "WITH RECURSIVE search_taxa(top_id, id, low_meaning_id, high_meaning_id, distance) AS (\n"
         + "         SELECT t.id,\n" //
         + "            t.id,\n" //
         + "            t.taxon_meaning_id,\n"//
         + "            t.taxon_meaning_id,\n"//
         + "            0\n"//
         + "           FROM taxa_taxon_lists t\n"//
         + "        UNION ALL\n"//
         + "         SELECT tt.id,\n"//
         + "            t.id,\n" //
         + "            t.taxon_meaning_id,\n"//
         + "            tt.high_meaning_id,\n" //
         + "            tt.distance + 1\n"
         + "           FROM taxa_taxon_lists t,\n"//
         + "            search_taxa tt\n"//
         + "          WHERE t.parent_id = tt.id\n" //
         + "        )\n"//
         + " SELECT DISTINCT search_taxa.high_meaning_id\n"//
         + "   FROM search_taxa\n"//
         + "   WHERE search_taxa.low_meaning_id = (SELECT DISTINCT taxon_meaning_id FROM taxa_taxon_lists WHERE id = ?1)"//
         , nativeQuery = true)
   public Set<Integer> findParentTaxonMeaningIds(int taxonId);

   @Query(value= "SELECT DISTINCT * FROM indicia.taxa t JOIN indicia.taxa_taxon_lists ttl on t.id = ttl.taxon_id "
         + "where ttl.taxon_meaning_id = (SELECT DISTINCT taxon_meaning_id FROM taxa_taxon_lists WHERE id = ?1)",  nativeQuery = true)
   public Set<DBTaxon> findSynonymsById(int taxonId);

   public Set<DBTaxon> findByTaxaTaxonListTaxonMeaningIdIn(
         Set<Integer> parentTaxonMeaningIds);

   @Query(value =  "SELECT DISTINCT * "
         + "FROM taxa t JOIN taxa_taxon_lists ttl ON t.id = ttl.taxon_id "
         + "WHERE ttl.id = ?1", nativeQuery = true)
   public DBTaxon getOneByTaxaTaxonListId(int id);


}
