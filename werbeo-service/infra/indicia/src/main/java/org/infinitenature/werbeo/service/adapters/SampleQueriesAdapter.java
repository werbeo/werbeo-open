package org.infinitenature.werbeo.service.adapters;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.infra.query.SampleQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSample;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.SampleMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SampleQueriesAdapter implements SampleQueries
{

   @Autowired
   private SampleRepository sampleRepository;
   @Autowired
   private SampleMapperIndicia sampleMapper;
   @Autowired
   private IdQueries idQueries;

   @Transactional(readOnly = true)
   @Override
   public Optional<Sample> get(UUID uuid, Context context)
   {
      int sampleId = idQueries.getIndiciaSampleId(uuid);
      if (sampleId == 0)
      {
         return Optional.empty();
      }
      try
      {
         DBSample dbSample = sampleRepository.getOne(sampleId);
         dbSample.setOccurrences(dbSample.getOccurrences().stream()
               .filter(occ -> !occ.isDeleted()).collect(Collectors.toList()));
         return Optional.of(sampleMapper.mapFull(dbSample, context));
      } catch (EntityNotFoundException e)
      {
         return Optional.empty();
      }
   }

}
