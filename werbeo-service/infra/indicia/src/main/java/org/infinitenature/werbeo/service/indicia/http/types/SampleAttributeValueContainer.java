package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_attribute_values")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleAttributeValueContainer
{
   @XmlElement(name = "sample_attribute_value")
   private List<SampleAttributeValue> sampleAttributeValues = new ArrayList<>();

   public List<SampleAttributeValue> getSampleAttributeValues()
   {
      return sampleAttributeValues;
   }

   public void setSampleAttributeValues(
         List<SampleAttributeValue> sampleAttributeValues)
   {
      this.sampleAttributeValues = sampleAttributeValues;
   }
}
