package org.infinitenature.werbeo.service.indicia.http.types;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "location")
@XmlAccessorType(XmlAccessType.FIELD)
public class Location extends IndiciaTypeImpl
{
   @Override
   public String getModelName()
   {
      return "location";
   }

   @NotNull
   @XmlElement(name = "name")
   private String name;
   @XmlElement(name = "code")
   private String code;
   @XmlElement(name = "centroid_sref")
   private String centroidSref;
   @XmlElement(name = "location_type_id")
   private int locationTypeId;
   @XmlElement(name = "centroid_sref_system")
   private String centroidSrefSystem;
   @XmlElement(name = "centroid_geom")
   private String centroidGeom;
   @NotNull
   @XmlElement(name = "boundary_geom")
   private String boundaryGeom;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getCode()
   {
      return code;
   }

   public void setCode(String code)
   {
      this.code = code;
   }

   public String getCentroidSref()
   {
      return centroidSref;
   }

   public void setCentroidSref(String centroidSref)
   {
      this.centroidSref = centroidSref;
   }

   public int getLocationTypeId()
   {
      return locationTypeId;
   }

   public void setLocationTypeId(int locationTypeId)
   {
      this.locationTypeId = locationTypeId;
   }

   public String getCentroidSrefSystem()
   {
      return centroidSrefSystem;
   }

   public void setCentroidSrefSystem(String centroidSrefSystem)
   {
      this.centroidSrefSystem = centroidSrefSystem;
   }

   public String getCentroidGeom()
   {
      return centroidGeom;
   }

   public void setCentroidGeom(String centroidGeom)
   {
      this.centroidGeom = centroidGeom;
   }

   public String getBoundaryGeom()
   {
      return boundaryGeom;
   }

   public void setBoundaryGeom(String boundaryGeom)
   {
      this.boundaryGeom = boundaryGeom;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Locality [name=");
      builder.append(name);
      builder.append(", code=");
      builder.append(code);
      builder.append(", centroidSref=");
      builder.append(centroidSref);
      builder.append(", locationTypeId=");
      builder.append(locationTypeId);
      builder.append(", centroidSrefSystem=");
      builder.append(centroidSrefSystem);
      builder.append(", centroidGeom=");
      builder.append(centroidGeom);
      builder.append(", boundaryGeom=");
      builder.append(boundaryGeom);
      builder.append(", id=");
      builder.append(id);
      builder.append(", createdOn=");
      builder.append(createdOn);
      builder.append(", updatedOn=");
      builder.append(updatedOn);
      builder.append(", createdBy=");
      builder.append(createdBy);
      builder.append(", updatedBy=");
      builder.append(updatedBy);
      builder.append("]");
      return builder.toString();
   }

}
