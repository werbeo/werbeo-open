package org.infinitenature.werbeo.service.indicia.http.http;

import org.apache.commons.codec.digest.DigestUtils;

public abstract class AuthToken
{
   private final String password;

   public AuthToken(String password)
   {
      this.password = password;
   }

   protected String getPassword()
   {
      return password;
   }

   /**
    * Computes an auth token for the indicia webservices
    * <p>
    * The auth token is a hexstring of the sha1 hash of "nonce":"password"
    * 
    * @param nonce
    * @return
    */
   protected String createAuthToken(String nonce)
   {
      return DigestUtils.sha1Hex(nonce + ":" + getPassword());
   }

   public abstract String getToken();

   public abstract String getNonce();

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((password == null) ? 0 : password.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
      {
         return true;
      }
      if (obj == null)
      {
         return false;
      }
      if (getClass() != obj.getClass())
      {
         return false;
      }
      AuthToken other = (AuthToken) obj;
      if (password == null)
      {
         if (other.password != null)
         {
            return false;
         }
      } else if (!password.equals(other.password))
      {
         return false;
      }
      return true;
   }

}
