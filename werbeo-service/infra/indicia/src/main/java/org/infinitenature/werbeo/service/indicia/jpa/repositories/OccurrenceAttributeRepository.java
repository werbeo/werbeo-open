package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface OccurrenceAttributeRepository
      extends JpaRepository<DBOccurrenceAttribute, Integer>,
      QuerydslPredicateExecutor<DBOccurrenceAttribute>
{

}
