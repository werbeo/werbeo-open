package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "person_attributes")
public class DBPersonAttribute extends DBAbstractAttribute implements Serializable
{

   private static final long serialVersionUID = 1L;
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "personAttribute")
   private Collection<DBPersonAttributesWebsites> personAttributesWebsitesCollection;
   @OneToMany(mappedBy = "personAttribute")
   private Collection<DBPersonAttributeValue> personAttributeValuesCollection;

   public DBPersonAttribute()
   {
   }

   public DBPersonAttribute(Integer id)
   {
      this.id = id;
   }

   public DBPersonAttribute(Integer id, Date createdOn, Date updatedOn,
         boolean public1, boolean deleted)
   {
      this.id = id;
      this.createdOn = createdOn;
      this.updatedOn = updatedOn;
      this.public1 = public1;
      this.deleted = deleted;
   }

   @XmlTransient
   public Collection<DBPersonAttributesWebsites> getPersonAttributesWebsitesCollection()
   {
      return personAttributesWebsitesCollection;
   }

   public void setPersonAttributesWebsitesCollection(
         Collection<DBPersonAttributesWebsites> personAttributesWebsitesCollection)
   {
      this.personAttributesWebsitesCollection = personAttributesWebsitesCollection;
   }

   @XmlTransient
   public Collection<DBPersonAttributeValue> getPersonAttributeValuesCollection()
   {
      return personAttributeValuesCollection;
   }

   public void setPersonAttributeValuesCollection(
         Collection<DBPersonAttributeValue> personAttributeValuesCollection)
   {
      this.personAttributeValuesCollection = personAttributeValuesCollection;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBPersonAttribute))
      {
         return false;
      }
      DBPersonAttribute other = (DBPersonAttribute) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.PersonAttributes[ id=" + id + " ]";
   }

}
