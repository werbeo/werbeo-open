package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "taxa_taxon_list_attributes")
public class DBTaxonAttribute extends DBAbstractAttribute
{
   private static final long serialVersionUID = 1L;
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "taxonAttribute")
   private Collection<DBTaxonAttributesWebsites> taxonAttributesWebsitesCollection;
   @OneToMany(mappedBy = "taxonAttribute")
   private Collection<DBTaxonAttributeValue> taxonAttributeValuesCollection;
   public Collection<DBTaxonAttributesWebsites> getTaxonAttributesWebsitesCollection()
   {
      return taxonAttributesWebsitesCollection;
   }
   public void setTaxonAttributesWebsitesCollection(
         Collection<DBTaxonAttributesWebsites> taxonAttributesWebsitesCollection)
   {
      this.taxonAttributesWebsitesCollection = taxonAttributesWebsitesCollection;
   }
   public Collection<DBTaxonAttributeValue> getTaxonAttributeValuesCollection()
   {
      return taxonAttributeValuesCollection;
   }
   public void setTaxonAttributeValuesCollection(
         Collection<DBTaxonAttributeValue> taxonAttributeValuesCollection)
   {
      this.taxonAttributeValuesCollection = taxonAttributeValuesCollection;
   }

  

}
