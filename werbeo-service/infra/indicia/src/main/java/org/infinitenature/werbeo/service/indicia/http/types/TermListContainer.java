package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "termlists")
public class TermListContainer
{

   private List<TermList> termLists = new ArrayList<>();

   @XmlElement(name = "termlist")
   public List<TermList> getTermLists()
   {
      return termLists;
   }

   public void setTermLists(List<TermList> termLists)
   {
      this.termLists = termLists;
   }
}
