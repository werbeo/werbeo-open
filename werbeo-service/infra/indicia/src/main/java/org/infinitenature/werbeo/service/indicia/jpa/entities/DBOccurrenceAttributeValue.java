package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryInit;

@Entity
@Table(name = "occurrence_attribute_values")
public class DBOccurrenceAttributeValue extends DBAbstractAttributeValue
      implements Serializable
{
   private static final long serialVersionUID = 1L;

   @JoinColumn(name = "occurrence_attribute_id", referencedColumnName = "id")
   @ManyToOne
   private DBOccurrenceAttribute occurrenceAttribute;
   @JoinColumn(name = "occurrence_id", referencedColumnName = "id")
   @ManyToOne
   @QueryInit("sample.survey.website")
   private DBOccurrence occurrence;

   public DBOccurrenceAttribute getOccurrenceAttribute()
   {
      return occurrenceAttribute;
   }

   public void setOccurrenceAttribute(
         DBOccurrenceAttribute occurrenceAttribute)
   {
      this.occurrenceAttribute = occurrenceAttribute;
   }

   public DBOccurrence getOccurrence()
   {
      return occurrence;
   }

   public void setOccurrence(DBOccurrence occurrence)
   {
      this.occurrence = occurrence;
   }

   @Override
   public Base getEntity()
   {
      return getOccurrence();
   }

   @Override
   public DBAbstractAttribute getAttribute()
   {
      return getOccurrenceAttribute();
   }
}
