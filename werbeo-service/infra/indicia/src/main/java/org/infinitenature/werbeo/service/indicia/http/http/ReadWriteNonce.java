package org.infinitenature.werbeo.service.indicia.http.http;

public class ReadWriteNonce extends ReadNonce
{
   private String writeNonce;

   public ReadWriteNonce(String readNonce, String writeNonce)
   {
      super(readNonce);
      this.writeNonce = writeNonce;
   }

   public String getWriteNonce()
   {
      return writeNonce;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("ReadWriteNonce [writeNonce=");
      builder.append(writeNonce);
      builder.append(", getReadNonce()=");
      builder.append(getReadNonce());
      builder.append("]");
      return builder.toString();
   }

}
