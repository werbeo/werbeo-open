package org.infinitenature.werbeo.service.adapters;

import static org.infinitenature.werbeo.service.adapters.QueriesUtil.createWebsiteAgreementQuery;

import java.time.ZoneId;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.core.api.enity.Language;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.infinitenature.werbeo.service.core.api.infra.query.TaxonQueries;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxon;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBTaxon;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.TaxonBaseMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.TaxonMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.TaxonRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.TaxonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.togglz.core.manager.FeatureManager;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;

@Service
public class TaxonQueriesAdapter implements TaxonQueries
{
   @Autowired
   private TaxonRepository taxonRepository;
   @Autowired
   private TaxonMapperIndicia taxonMapper;
   @Autowired
   private TaxonBaseMapperIndicia taxonBaseMapper;
   @Autowired
   private TaxonRequestMapperIndicia requestMapper;
   @Autowired
   private InstanceConfig instanceConfig;
   @Autowired
   private FeatureManager featureManager;

   @Cacheable(cacheNames = "get-taxon")
   @Transactional(readOnly = true)
   @Override
   public Taxon get(Integer id, Context context)
   {
      DBTaxon oneByTaxaTaxonListId = taxonRepository.getOneByTaxaTaxonListId(id);

      if (oneByTaxaTaxonListId == null)
      {
         throw new EntityNotFoundException("No taxon with ID " + id + "found.");
      }
      else if (!featureManager.isActive(WerBeoFeatures.ALLOW_GET_ALL_TAXA)
            && isTaxonListsConfigured(context))
      {
         Integer taxaListId = oneByTaxaTaxonListId.getTaxaTaxonList()
               .getTaxaList().getId();
         if (!instanceConfig.getTaxonListsPerPortal()
               .get(context.getPortal().getId()).contains(taxaListId))
         {
            throw new EntityNotFoundException(
                  "No taxon with ID " + id + "found for portal "
                        + context.getPortal().getId());
         }
      }
      return taxonMapper.map(oneByTaxaTaxonListId);
   }

   @Override
   @Transactional(readOnly = true)
   public Slice<Taxon, TaxonSortField> find(TaxonFilter filter, Context context,
         OffsetRequest<TaxonSortField> offsetRequest)
   {
      BooleanExpression predicate = prepareQuery(filter, context);

      return new SliceImpl<>(taxonMapper.map(taxonRepository
            .findAll(predicate, requestMapper.map(offsetRequest)).getContent()),
            offsetRequest);
   }

   @Transactional(readOnly = true)
   @Override
   public Slice<TaxonBase, TaxonSortField> findBase(TaxonFilter filter,
         Context context, OffsetRequest<TaxonSortField> offsetRequest)
   {
      BooleanExpression predicate = prepareQuery(filter, context);

      return new SliceImpl<>(taxonBaseMapper.map(taxonRepository
            .findAll(predicate, requestMapper.map(offsetRequest)).getContent()),
            offsetRequest);
   }

   @Transactional(readOnly = true)
   @Override
   public List<Taxon> findAllSynonyms(Integer taxonId, Context context)
   {
      return taxonMapper.map(taxonRepository.findSynonymsById(taxonId));
   }

   protected BooleanExpression prepareQuery(TaxonFilter filter, Context context)
   {
      QDBTaxon qTaxon = QDBTaxon.dBTaxon;
      BooleanExpression predicate = qTaxon.deleted.isFalse();

      BooleanExpression website = qTaxon.taxaTaxonList.taxaList.website.isNull().or(qTaxon.taxaTaxonList.taxaList.website.id
            .eq(context.getPortal().getId()));

      predicate = predicate.and(website);

      if (isTaxonListsConfigured(context))
      {
         predicate = predicate.and(qTaxon.taxaTaxonList.taxaList.id
               .in(instanceConfig.getTaxonListsPerPortal()
                     .get(context.getPortal().getId())));
      }
      if (StringUtils.isNotBlank(filter.getNameContains()))
      {
         String searchString = StringUtils
               .join(filter.getNameContains().split(" ", -1), "% ") + "%";
         predicate = predicate.and(qTaxon.taxon.likeIgnoreCase(searchString));
      }
      if (!filter.getLanguages().isEmpty())
      {
         predicate = predicate
               .and(qTaxon.language.iso.in(toString(filter.getLanguages())));
      }
      if (!filter.getExternalKeys().isEmpty())
      {
         predicate = predicate
               .and(qTaxon.externalKey.in(filter.getExternalKeys()));
      }
      if (!filter.isIncludeSynonyms())
      {
         predicate = predicate.and(qTaxon.taxaTaxonList.preferred.isTrue());
      }
      if (filter.isOnlyUsedTaxa())
      {
         QDBOccurrence qOccurrence = QDBOccurrence.dBOccurrence;
         JPQLQuery<Integer> queryMeaningIds = JPAExpressions
               .selectDistinct(qOccurrence.taxaTaxonList.taxonMeaningId)
               .from(qOccurrence)
               .where(qOccurrence.deleted.isFalse()
                     .and(qOccurrence.sample.survey.website
                           .in(createWebsiteAgreementQuery(context))));

         predicate = predicate
               .and(qTaxon.taxaTaxonList.taxonMeaningId.in(queryMeaningIds));
      }
      if (filter.getOccurrencesChangedSince() != null)
      {
         QDBOccurrence qOccurrence = QDBOccurrence.dBOccurrence;

         Date date = Date.from(filter.getOccurrencesChangedSince()
               .atStartOfDay(ZoneId.systemDefault()).toInstant());

         JPQLQuery<Integer> queryMeaningIds = JPAExpressions
               .selectDistinct(qOccurrence.taxaTaxonList.taxonMeaningId)
               .from(qOccurrence)
               .where(qOccurrence.updatedOn.after(date)
                     .or(qOccurrence.sample.updatedOn.after(date))
                     .and(qOccurrence.sample.survey.website
                           .in(createWebsiteAgreementQuery(context))));

         predicate = predicate
               .and(qTaxon.taxaTaxonList.taxonMeaningId.in(queryMeaningIds));
      }
      if (StringUtils.isNotBlank(filter.getTaxaGroup()))
      {
         predicate = predicate
               .and(qTaxon.taxonGroup.title.eq(filter.getTaxaGroup()));
      } else if (!instanceConfig.getTaxonGroupsPerPortal()
            .getOrDefault(context.getPortal().getId(), Collections.emptySet())
            .isEmpty())
      {
         predicate = predicate.and(qTaxon.taxonGroup.title.in(instanceConfig
               .getTaxonGroupsPerPortal().get(context.getPortal().getId())));
      }
      if (filter.isOnlyTaxaAvailableForInput())
      {
         predicate = predicate
               .and(qTaxon.taxaTaxonList.allowDataEntry.isTrue());
      }
      return predicate;
   }

   private boolean isTaxonListsConfigured(Context context)
   {
      return instanceConfig.getTaxonListsPerPortal()
            .get(context.getPortal().getId()) != null;
   }

   private Collection<? extends String> toString(Set<Language> languages)
   {
      return languages.stream().map(language -> language.name().toLowerCase())
            .collect(Collectors.toSet());
   }

   @Override
   public long count(TaxonFilter filter, Context context)
   {
      return (int) taxonRepository.count(prepareQuery(filter, context));
   }

   @Transactional(readOnly = true)
   @Override
   @Cacheable(cacheNames = "taxa-parrent-swarm")
   public Set<TaxonBase> findParrentSwarm(TaxonBase taxon)
   {
      Set<Integer> parentTaxonMeaningIds = taxonRepository
            .findParentTaxonMeaningIds(taxon.getId());
      return taxonBaseMapper.map(taxonRepository
            .findByTaxaTaxonListTaxonMeaningIdIn(parentTaxonMeaningIds));
   }

   @Override
   public Set<TaxonBase> loadPrefferedChildren(TaxonBase taxon)
   {
      // TODO implement
      return null;
   }

   @Transactional(readOnly = true)
   @Override
   @Cacheable(cacheNames = "taxa-by-external-id")
   public TaxonBase getByExternalId(int taxaListId, String externalId)
   {
      QDBTaxon qTaxon = QDBTaxon.dBTaxon;
      BooleanExpression predicate = qTaxon.deleted.isFalse();
      predicate = predicate.and(qTaxon.externalKey.eq(externalId));
      predicate = predicate.and(qTaxon.taxaTaxonList.taxaList.id.eq(taxaListId))
            .and(qTaxon.taxaTaxonList.taxaList.deleted.isFalse());

      long count = taxonRepository.count(predicate);
      if (count == 1)
      {
         Optional<DBTaxon> one = taxonRepository.findOne(predicate);
         return taxonMapper.map(one.get());
      } else if (count == 0)
      {
         throw new EntityNotFoundException("No taxon with externalId "
               + externalId + " in list with id " + taxaListId + " found.");
      } else
      {
         try
         {
            predicate = qTaxon.deleted.isFalse();
            predicate = predicate.and(qTaxon.externalKey.eq(externalId))
                  .and(qTaxon.taxaTaxonList.preferred.isTrue());
            predicate = predicate
                  .and(qTaxon.taxaTaxonList.taxaList.id.eq(taxaListId))
                  .and(qTaxon.taxaTaxonList.taxaList.deleted.isFalse());
            Optional<DBTaxon> one = taxonRepository.findOne(predicate);
            if (one.isPresent())
            {
               return taxonMapper.map(one.get());
            } else
            {
               throw new EntityNotFoundException(
                     "No taxon with externalId " + externalId
                           + " in list with id " + taxaListId + " found.");
            }
         } catch (IncorrectResultSizeDataAccessException e)
         {
            throw new WerbeoException("More than one taxon with externalId "
                  + externalId + " in list with id " + taxaListId + " found.");
         }
      }
   }

   public void setInstanceConfig(InstanceConfig instanceConfig)
   {
      this.instanceConfig = instanceConfig;
   }
}