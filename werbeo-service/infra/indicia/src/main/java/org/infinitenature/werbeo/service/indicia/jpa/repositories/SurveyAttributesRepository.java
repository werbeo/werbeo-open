package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurveyAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SurveyAttributesRepository
      extends JpaRepository<DBSurveyAttribute, Integer>,
      QuerydslPredicateExecutor<DBSurveyAttribute>
{

}
