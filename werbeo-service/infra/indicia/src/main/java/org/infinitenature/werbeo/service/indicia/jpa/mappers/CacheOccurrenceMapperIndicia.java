package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasOccurrenceAttributeValues;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasPosition;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasSampleAttributeValues;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasVagueDate;
import org.infinitenature.werbeo.service.indicia.jpa.entities.cache.CacheOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigIndiciaJPA.class, uses = {})
public abstract class CacheOccurrenceMapperIndicia
{
   @Autowired
   private OccurrenceMappingHelper occurrenceMappingHelper;

   @Autowired
   private SampleMappingHelper sampleMappingHelper;

   @Mapping(target = "creationDate", source = "createdOn")
   @Mapping(target = "modificationDate", source = "updatedOn")
   @Mapping(target = "modifiedBy", ignore = true)
   @Mapping(target = "amount", ignore = true) // handled by additional attribute
   @Mapping(target = "determiner", ignore = true)
   @Mapping(target = "settlementStatus", ignore = true) // handled by additional
                                                        // attribute
   @Mapping(target = "id", ignore = true) // handled by additional attribute
   @Mapping(target = "createdBy", ignore = true)
   @Mapping(target = "taxon.name", source = "taxon")
   @Mapping(target = "taxon.id", source = "taxaTaxonListId")
   @Mapping(target = "sample", expression = "java(mapSamplePart(cacheOccurrence, context))")

   public abstract Occurrence map(CacheOccurrence cacheOccurrence,
         @org.mapstruct.Context Context context);

   @Mapping(target = "id", ignore = true) // handed by additional
   // attribute
   @Mapping(target = "survey.id", source = "surveyId")
   @Mapping(target = "survey.name", source = "surveyTitle")
   public abstract SampleBase mapSamplePart(
         CacheOccurrence cacheOccurrence,
         @org.mapstruct.Context Context context);

   public abstract List<Occurrence> map(List<CacheOccurrence> content,
         @org.mapstruct.Context Context context);

   protected RecordStatus map(Character statusCode)
   {
      if (statusCode == null)
      {
         return null;
      } else if (IndiciaOccurrence.COMPLETED.equals(statusCode.toString()))
      {
         return RecordStatus.COMPLETE;
      } else if (IndiciaOccurrence.IN_PROGRESS.equals(statusCode.toString()))
      {
         return RecordStatus.IN_PROGRESS;
      } else
      {
         throw new IllegalArgumentException(
               "Record status code " + statusCode + " not supported");
      }
   }

   @AfterMapping
   public void setSampleAttributes(@MappingTarget SampleBase sample,
         HasSampleAttributeValues jpaSample)
   {
      sampleMappingHelper.mapAdditionalAttributes(sample, jpaSample);
   }

   @AfterMapping
   public void setDate(@MappingTarget SampleBase sample, HasVagueDate jpaSample)
   {
      sampleMappingHelper.mapDate(sample, jpaSample);
   }

   @AfterMapping
   public void setPosition(@MappingTarget SampleBase sample,
         HasPosition jpaSample)
   {
      sampleMappingHelper.setPosition(sample, jpaSample);
   }

   @AfterMapping
   public void setAttributes(@MappingTarget Occurrence occurrence,
         HasOccurrenceAttributeValues jpaOccurrence)
   {
      occurrenceMappingHelper.mapAdditionalAttributes(occurrence,
            jpaOccurrence);
   }

}
