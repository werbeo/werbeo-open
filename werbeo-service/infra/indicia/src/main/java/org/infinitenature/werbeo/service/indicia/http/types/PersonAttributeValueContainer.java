package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "person_attribute_values")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonAttributeValueContainer
{
   @XmlElement(name = "person_attribute_value")
   private List<PersonAttributeValue> personAttributeValues = new ArrayList<>();

   public List<PersonAttributeValue> getPersonAttributeValues()
   {
      return personAttributeValues;
   }

   public void setPersonAttributeValues(
         List<PersonAttributeValue> personAttributeValues)
   {
      this.personAttributeValues = personAttributeValues;
   }
}