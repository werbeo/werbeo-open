package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Map;
import java.util.Set;

public class OccurrenceSubmissionContainer
{
   private IndiciaOccurrence occurrence;
   private Map<String, String> additionAtributes;
   private Set<OccurrenceComment> comments;
   private Set<OccurrenceMedium> media;

   public OccurrenceSubmissionContainer(IndiciaOccurrence occurrence,
         Map<String, String> additionAtributes, Set<OccurrenceComment> comments,
         Set<OccurrenceMedium> occurrenceMedia)
   {
      super();
      this.occurrence = occurrence;
      this.additionAtributes = additionAtributes;
      this.comments = comments;
      this.media = occurrenceMedia;
   }

   public IndiciaOccurrence getOccurrence()
   {
      return occurrence;
   }

   public void setOccurrence(IndiciaOccurrence occurrence)
   {
      this.occurrence = occurrence;
   }

   public Map<String, String> getAdditionAtributes()
   {
      return additionAtributes;
   }

   public void setAdditionAtributes(Map<String, String> additionAtributes)
   {
      this.additionAtributes = additionAtributes;
   }

   public Set<OccurrenceComment> getComments()
   {
      return comments;
   }

   public void setComments(Set<OccurrenceComment> comments)
   {
      this.comments = comments;
   }

   public Set<OccurrenceMedium> getMedia()
   {
      return media;
   }

   public void setMedia(Set<OccurrenceMedium> media)
   {
      this.media = media;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("OccurrenceSubmissionContainer@");
      builder.append(System.identityHashCode(this));
      builder.append(" [");
      if (occurrence != null)
      {
         builder.append("occurrence=");
         builder.append(occurrence);
         builder.append(", ");
      }
      if (additionAtributes != null)
      {
         builder.append("additionAtributes=");
         builder.append(additionAtributes);
         builder.append(", ");
      }
      if (comments != null)
      {
         builder.append("comments=");
         builder.append(comments);
         builder.append(", ");
      }
      if (media != null)
      {
         builder.append("media=");
         builder.append(media);
      }
      builder.append("]");
      return builder.toString();
   }

}