package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.adapters.AdditionalSampleAttributeConfig;
import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.adapters.config.TermConfig;
import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSample;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasPosition;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasSampleAttributeValues;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasVagueDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SampleMappingHelper
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SampleMappingHelper.class);

   @Autowired
   private AdditionalAttributeConfigFactory attributeConfigFactory;
   @Autowired
   private PosionMapperIndiciaImpl positionMapper;
   @Autowired
   protected PersonQueries personQueries;
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;

   public void setPosition(SampleBase sample, HasPosition jpaSample)
   {
      sample.getLocality().setPosition(
            positionMapper.map2Position(jpaSample.getEnteredSrefSystem(),
                  jpaSample.getEnteredSref(), jpaSample.getGeometry(), null));
   }

   public void mapSampleMethod(SampleBase sample, DBSample jpaSample)
   {
      Integer sampleMethodId = jpaSample.getSampleMethodId();
      TermConfig termConfig = termlistConfigFactory.getTermConfig();
      if (sampleMethodId != null)
      {
         sample.setSampleMethod(termConfig.getSampleMethod(sampleMethodId));
      }
   }
   public void mapAdditionalAttributes(SampleBase sample,
         HasSampleAttributeValues jpaSample)
   {
      AdditionalSampleAttributeConfig sampleConfig = attributeConfigFactory
            .getSampleConfig();

      List<DBSampleAttributeValue> attributeValues = jpaSample
            .getSampleAttributeValues().stream()
            .filter(value -> !value.isDeleted()
                  && !value.getAttribute().isDeleted())
            .collect(Collectors.toList());

      attributeValues.forEach(attributeValue ->
      {
         Integer attributeId = attributeValue.getAttribute().getId();
         if (sampleConfig.getUuid() == attributeId)
         {
            sample.setId(UUID.fromString(attributeValue.getTextValue()));
         } else if (sampleConfig.getPrecision() == attributeId)
         {
            sample.getLocality().setPrecision(attributeValue.getIntValue());
         } else if (sampleConfig.getLocality() == attributeId)
         {
            sample.getLocality().setLocality(attributeValue.getTextValue());
         } else if (sampleConfig.getLocationDescription() == attributeId)
         {
            sample.getLocality().setLocationComment(attributeValue.getTextValue());
         }
      });
   }

   public Person toPerson(String recorderNames, Context context)
   {
      try
      {
         if (recorderNames != null && recorderNames.startsWith("#id-")
               && recorderNames.endsWith("#") && recorderNames.length() > 5)
         {
            int recorderId = Integer.parseInt(
                  recorderNames.substring(4, recorderNames.length() - 1));
            return personQueries.get(recorderId, context);
         }

      } catch (Exception e)
      {
         LOGGER.error("Error loading recorder for recorderNames: {}",
               recorderNames, e);
      }

      return null;
   }

   public void mapDate(SampleBase sample, HasVagueDate jpaSample)
   {
      VagueDate vagueDate = VagueDateFactory.create(jpaSample.getStartDate(),
            jpaSample.getEndDate(), jpaSample.getDateType());
      sample.setDate(vagueDate);
   }

}
