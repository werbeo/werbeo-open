package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSample;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(config = CentralConfigIndiciaJPA.class, uses = {
      SurveyMapperIndicia.class })

public abstract class SampleBaseMapperIndicia
{

   @Autowired
   private SampleMappingHelper mappingHelper;

   @Mapping(target = "locality.precision", ignore = true) // handled by additional
                                                 // attributes
   @Mapping(target = "locality.position", ignore = true)
   @Mapping(target = "date", ignore = true)
   @Mapping(target = "recorder", source = "recorderNames")
   @Mapping(target = "id", ignore = true) // handled by additional attributes
   public abstract SampleBase map(DBSample jpaSample,
         @org.mapstruct.Context Context context);

   public abstract List<SampleBase> mapAll(List<DBSample> content,
         @org.mapstruct.Context Context context);

   @AfterMapping
   public void setPosion(@MappingTarget SampleBase sample, DBSample jpaSample)
   {
      mappingHelper.setPosition(sample, jpaSample);
      mappingHelper.mapDate(sample, jpaSample);
      mappingHelper.mapSampleMethod(sample, jpaSample);
      mappingHelper.mapAdditionalAttributes(sample, jpaSample);
   }

   public Person toPerson(String recorderNames,
         @org.mapstruct.Context Context context)
   {
      return mappingHelper.toPerson(recorderNames, context);
   }
}
