package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "person_attributes_websites")
public class DBPersonAttributesWebsites extends DBAbstractAttributesWebsites
      implements Serializable
{

   private static final long serialVersionUID = 1L;
   @JoinColumn(name = "person_attribute_id", referencedColumnName = "id")
   @ManyToOne(optional = false)
   private DBPersonAttribute personAttribute;

   public DBPersonAttributesWebsites()
   {
   }

   public DBPersonAttributesWebsites(Integer id)
   {
      this.id = id;
   }

   public DBPersonAttributesWebsites(Integer id, Date createdOn,
         boolean deleted, int weight)
   {
      this.id = id;
      this.createdOn = createdOn;
      this.deleted = deleted;
      setWeight(weight);
   }

   public DBPersonAttribute getPersonAttribute()
   {
      return personAttribute;
   }

   public void setPersonAttribute(DBPersonAttribute personAttribute)
   {
      this.personAttribute = personAttribute;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBPersonAttributesWebsites))
      {
         return false;
      }
      DBPersonAttributesWebsites other = (DBPersonAttributesWebsites) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.PersonAttributesWebsites[ id=" + id
            + " ]";
   }

}
