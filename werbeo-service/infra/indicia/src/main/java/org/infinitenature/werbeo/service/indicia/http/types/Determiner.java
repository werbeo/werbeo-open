package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
public class Determiner
{
   int id;
   String name;

   @XmlAttribute
   public int getId()
   {
      return id;
   }

   @XmlValue
   public String getName()
   {
      return name;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Determiner [id=");
      builder.append(id);
      builder.append(", name=");
      builder.append(name);
      builder.append("]");
      return builder.toString();
   }

}
