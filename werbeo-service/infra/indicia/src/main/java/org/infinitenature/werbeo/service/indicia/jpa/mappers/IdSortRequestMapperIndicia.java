package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IdSortRequestMapperIndicia extends RequestMapperIndicia
{
   @Override
   default Function<Object, String[]> getSortFieldMapper()
   {
      return sortField -> new String[] { "id" };
   }
}
