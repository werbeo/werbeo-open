package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_attributes")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleAttributeContainer
{
   @XmlElement(name = "sample_attribute")
   private List<SampleAttribute> sampleAttributes = new ArrayList<>();

   public List<SampleAttribute> getSampleAttributes()
   {
      return sampleAttributes;
   }

   public void setSampleAttributes(List<SampleAttribute> sampleAttributes)
   {
      this.sampleAttributes = sampleAttributes;
   }

}
