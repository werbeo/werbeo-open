package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.util.Optional;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface OccurrenceAttributeValueRepository
      extends JpaRepository<DBOccurrenceAttributeValue, Integer>,
      QuerydslPredicateExecutor<DBOccurrenceAttributeValue>
{
   Optional<DBOccurrenceAttributeValue> findOneByOccurrenceIdAndOccurrenceAttributeIdAndDeletedIsFalse(
         int occurrenceId, int occurrenceAttributeId);
}
