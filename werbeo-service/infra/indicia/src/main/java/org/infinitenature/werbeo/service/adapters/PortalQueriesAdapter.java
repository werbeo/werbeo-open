package org.infinitenature.werbeo.service.adapters;

import java.util.Set;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.PortalQueries;
import org.infinitenature.werbeo.service.core.api.ports.PortalFilter;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBWebsite;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.PortalMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.PortalRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.PortalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class PortalQueriesAdapter implements PortalQueries
{
   @Autowired
   private PortalRepository repo;
   @Autowired
   private PortalMapperIndicia mapper;
   @Autowired
   private PortalRequestMapperIndicia requestMapper;

   @Override
   @Transactional(readOnly = true)
   public Portal load(int id)
   {
      QDBWebsite qWebsites = QDBWebsite.dBWebsite;
      BooleanExpression expression = qWebsites.id.eq(id)
            .and(qWebsites.deleted.isFalse());

      return mapper.map(repo.findOne(expression).orElseThrow(
            () -> new org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException(
                  "No portal with id " + id + " found.")));
   }

   @Override
   @Transactional(readOnly = true)
   public Set<Portal> findAll()
   {
      return mapper.map(repo.findAll(QDBWebsite.dBWebsite.deleted.isFalse()));
   }

   @Override
   @Transactional(readOnly = true)
   public Slice<Portal, PortalSortField> findAssociated(int id)
   {
      return new SliceImpl<>(
            mapper.mapList(repo.findAll(
                  QDBWebsite.dBWebsite
                        .in(QueriesUtil.createWebsiteAgreementQuery(id)),
                  Sort.by(Direction.ASC, "id"))),
            new OffsetRequestImpl<>(0, Integer.MAX_VALUE,
                  SortOrder.ASC, PortalSortField.ID));
   }

   @Override
   @Transactional(readOnly = true)
   public Slice<Portal, PortalSortField> find(PortalFilter filter,
         OffsetRequest<PortalSortField> offsetRequest)
   {
      BooleanExpression predicate = prepareQuery(filter);

      return new SliceImpl<>(mapper.mapList(repo
            .findAll(predicate, requestMapper.map(offsetRequest)).getContent()),
            offsetRequest);

   }

   @Override
   @Transactional(readOnly = true)
   public long count(PortalFilter filter)
   {
      return repo.count(prepareQuery(filter));
   }

   private BooleanExpression prepareQuery(PortalFilter filter)
   {
      QDBWebsite qWebsites = QDBWebsite.dBWebsite;
      BooleanExpression expression = qWebsites.deleted.isFalse();
      return expression;
   }
}
