package org.infinitenature.werbeo.service.adapters;

import java.util.Map.Entry;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
class Attribute
{
   private final String attributeId;
   private final String index;
   private final String valueId;
   private final String value;
   private final String key;

   public Attribute(Entry<String, String> mapEntry)
   {
      this.key = mapEntry.getKey();
      this.attributeId = key.split(":")[0];
      this.value = mapEntry.getValue();
      if (key.contains("::"))
      {
         valueId = "";
         index = key.substring(key.lastIndexOf(':') + 1);
      } else if (key.contains(":"))
      {
         valueId = key.substring(key.lastIndexOf(':') + 1);
         index = "";
      } else
      {
         valueId = "";
         index = "";
      }
   }

   public String getAttributeId()
   {
      return attributeId;
   }

   public String getIndex()
   {
      return index;
   }

   public String getValueId()
   {
      return valueId;
   }

   public String getValue()
   {
      return value;
   }

   public String getKey()
   {
      return key;
   }

   @Override
   public String toString()
   {
      return AttributeBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return AttributeBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return AttributeBeanUtil.doEquals(this, obj);
   }
}