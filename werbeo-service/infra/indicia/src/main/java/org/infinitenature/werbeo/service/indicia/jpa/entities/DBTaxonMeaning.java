package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import net.vergien.beanautoutils.annotation.Bean;

@Entity
@Table(name = "taxon_meanings")
@Bean
public class DBTaxonMeaning
{
   @Id
   @GenericGenerator(name = "indicia-sequence", strategy = "org.infinitenature.werbeo.service.indicia.jpa.IdGenerator")
   @GeneratedValue(generator = "indicia-sequence", strategy = GenerationType.SEQUENCE)
   @Basic(optional = false)
   private Integer id;

   public Integer getId()
   {
      return id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   @Override
   public String toString()
   {
      return DBTaxonMeaningBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return DBTaxonMeaningBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return DBTaxonMeaningBeanUtil.doEquals(this, obj);
   }
}
