package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "website")
public class Website extends IndiciaTypeImpl
{
   @XmlElement(name = "title")
   private String title;
   @XmlElement(name = "url")
   private String url;

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   @Override
   public String getModelName()
   {
      return "website";
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Website [");
      if (title != null)
      {
         builder.append("title=");
         builder.append(title);
         builder.append(", ");
      }
      if (url != null)
      {
         builder.append("url=");
         builder.append(url);
         builder.append(", ");
      }
      builder.append("id=");
      builder.append(id);
      builder.append(", ");
      if (createdOn != null)
      {
         builder.append("createdOn=");
         builder.append(createdOn);
         builder.append(", ");
      }
      if (updatedOn != null)
      {
         builder.append("updatedOn=");
         builder.append(updatedOn);
         builder.append(", ");
      }
      if (createdBy != null)
      {
         builder.append("createdBy=");
         builder.append(createdBy);
         builder.append(", ");
      }
      if (updatedBy != null)
      {
         builder.append("updatedBy=");
         builder.append(updatedBy);
      }
      builder.append("]");
      return builder.toString();
   }

}
