package org.infinitenature.werbeo.service.adapters.config;

import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.AMOUNT_A_LOT_TITLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.AMOUNT_FEW_TITLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.AMOUNT_ONE_TITLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.AMOUNT_SOME_TITLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.AMOUNT_SCATTERED;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_FIFTY;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_HUNDRED;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_LESS_ONE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_ONE_TO_FIVE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_SIX_TO_TWENTYFIVE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_TENTHOUSAND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_THOUSAND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.BLOOMING_SPROUTS_TWENTYSIX;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_FIFTY;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_HUNDRED;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_LESS_ONE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_ONE_TO_FIVE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_SIX_TO_TWENTYFIVE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_TENTHOUSAND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_THOUSAND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.COVERED_AREA_TWENTYSIX;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.OCCURRENCE_STATUS_ANGESALBT_KULTIVIERT;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.OCCURRENCE_STATUS_ANGESIEDELT;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.OCCURRENCE_STATUS_EINGEBUERGERT;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.OCCURRENCE_STATUS_INDIGEN;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.OCCURRENCE_STATUS_SYNANTHROP;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.OCCURRENCE_STATUS_UNBEKANNT;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.OCCURRENCE_STATUS_UNBESTAENDIG;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.QUANTITY_FIFTY;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.QUANTITY_HUNDRED;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.QUANTITY_ONE_TO_FIVE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.QUANTITY_SIX_TO_TWENTYFIVE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.QUANTITY_TENTHOUSAND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.QUANTITY_THOUSAND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.QUANTITY_TWENTYSIX;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.STATUS_PLANTED_TITLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.STATUS_RESETTELD_TITLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.STATUS_WILD_TITLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VALIDATION_IMPROBALE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VALIDATION_INVALID;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VALIDATION_PROBABLE;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VALIDATION_UNKNOWN;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VALIDATION_VALID;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VITALITY_ABGESTORBEN;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VITALITY_ABSTERBEND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VITALITY_KUEMMERND;
import static org.infinitenature.werbeo.service.adapters.config.IndiciaConstans.VITALITY_VITAL;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Area;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.BloomingSprouts;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.LifeStage;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Makropter;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.NumericAmountAccuracy;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Quantity;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Reproduction;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatusFukarek;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Sex;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Vitality;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.enity.Taxon.RedListStatus;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTermlist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TermConfig
{
   private static final String UNKNOWN = "unknown";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TermConfig.class);
   private final Map<Amount, Integer> amountMap = new EnumMap<>(Amount.class);
   private final Map<Integer, Amount> amountMapInvers = new HashMap<>();
   private final Map<SettlementStatus, Integer> statusMap = new EnumMap<>(
         SettlementStatus.class);
   private final Map<Integer, SettlementStatus> statusMapInvers = new HashMap<>();
   private final Map<Area, Integer> areaMap = new EnumMap<>(Area.class);
   private final Map<Integer, Area> areaMapInvers = new HashMap<>();

   private final Map<Vitality, Integer> vitalityMap = new EnumMap<>(
         Vitality.class);
   private final Map<Integer, Vitality> vitalityMapInvers = new HashMap<>();

   private final Map<SettlementStatusFukarek, Integer> occurrenceStatusFukarekMap = new EnumMap<>(
         SettlementStatusFukarek.class);
   private final Map<Integer, SettlementStatusFukarek> occurrenceStatusMapFukarekInvers = new HashMap<>();

   private final Map<BloomingSprouts, Integer> bloomingSproutsMap = new EnumMap<>(
         BloomingSprouts.class);
   private final Map<Integer, BloomingSprouts> bloomingSproutsMapInvers = new HashMap<>();

   private final Map<Quantity, Integer> quantityMap = new EnumMap<>(
         Quantity.class);
   private final Map<Integer, Quantity> quantityMapInvers = new HashMap<>();

   private final Map<Sex, Integer> sexMap = new EnumMap<>(Sex.class);
   private final Map<Integer, Sex> sexMapInvers = new HashMap<>();

   private final Map<Reproduction, Integer> reproductionMap = new EnumMap<>(
         Reproduction.class);
   private final Map<Integer, Reproduction> reproductionMapInvers = new HashMap<>();

   private final Map<SampleMethod, Integer> sampleMethodMap = new EnumMap<>(
         SampleMethod.class);
   private final Map<Integer, SampleMethod> sampleMethodMapInvers = new HashMap<>();

   private final Map<MediaType, Integer> mediaTypes = new EnumMap<>(
         MediaType.class);
   private final Map<Integer, MediaType> mediaTypesInvers = new HashMap<>();

   private final Map<RedListStatus, Integer> redListStatusMap = new EnumMap<>(
         RedListStatus.class);
   private final Map<Integer, RedListStatus> redListStatusMapInvers = new HashMap<>();

   private final Map<ValidationStatus, Integer> validationStatusMap = new EnumMap<>(
         ValidationStatus.class);
   private final Map<Integer, ValidationStatus> validationStatusMapInvers = new HashMap<>();

   private final Map<LifeStage, Integer> lifeStageMap = new EnumMap<>(LifeStage.class);
   private final Map<Integer, LifeStage> lifeStageMapInvers = new HashMap<>();
   private final Map<Makropter, Integer> makropterMap = new EnumMap<>(Makropter.class);
   private final Map<Integer, Makropter> makropterMapInvers = new HashMap<>();
   private final Map<NumericAmountAccuracy, Integer> numericAmountAccuracyMap = new EnumMap<>(
         NumericAmountAccuracy.class);
   private final Map<Integer, NumericAmountAccuracy> numericAmountAccuracyMapInvers = new HashMap<>();

   public TermConfig(DBTermlist amountList, DBTermlist statusList,
         DBTermlist areaList, DBTermlist originList, DBTermlist vitalityList,
         DBTermlist occurrenceStatusList, DBTermlist bloomingSproutsList,
         DBTermlist quantityList, DBTermlist sexList,
         DBTermlist reproducitionList, DBTermlist sampleMethodList,
         DBTermlist mediaTypesList, DBTermlist redListStatusList,
         DBTermlist validationStatusList, DBTermlist lifeStageList,
         DBTermlist makropterList, DBTermlist numericAmountAccuracyList)
   {
      fillAmountMaps(amountList);
      fillStatusMaps(statusList);
      fillAreaMaps(areaList);
      fillVitalityMaps(vitalityList);
      fillOccurrenceStatusMaps(occurrenceStatusList);
      fillBloomingSproutsMaps(bloomingSproutsList);
      fillQuantityMaps(quantityList);
      fillReproductionMaps(reproducitionList);
      fillSexMaps(sexList);
      fillSampleMethodMaps(sampleMethodList);
      fillMediaTypesMaps(mediaTypesList);
      fillRedListStatusMaps(redListStatusList);
      fillValidationStatusMaps(validationStatusList);
      fillLifeStageMaps(lifeStageList);
      fillMakropterMaps(makropterList);
      fillNumericAmountAccuracyMaps(numericAmountAccuracyList);
   }

   private void fillSampleMethodMaps(DBTermlist sampleMethodList)
   {
      sampleMethodList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();

               switch (term.getTerm().getTerm())

               {
               case "Unknown":
                  addSampleMethod(termId, SampleMethod.UNKNOWN);
                  break;
               case "Field Observation":
                  addSampleMethod(termId, SampleMethod.FIELD_OBSERVATION);
                  break;
               case "Estiminiation":
                  addSampleMethod(termId, SampleMethod.ESTIMINATION);
                  break;
               case "Transect count":
                  addSampleMethod(termId, SampleMethod.TRANSECT_COUNT);
                  break;
               case "Quadrat":
                  addSampleMethod(termId, SampleMethod.QUADRAT);
                  break;
               case "Transect":
                  addSampleMethod(termId, SampleMethod.TRANSECT);
                  break;
               case "Net":
                  addSampleMethod(termId, SampleMethod.NET);
                  break;
               case "Pitfall Trap":
                  addSampleMethod(termId, SampleMethod.PITFALL_TRAP);
                  break;
               case "Light Trap":
                  addSampleMethod(termId, SampleMethod.LIGHT_TRAP);
                  break;
               case "Transect Section":
                  addSampleMethod(termId, SampleMethod.TRANSECT_SECTION);
                  break;
               case "Timed Count":
                  addSampleMethod(termId, SampleMethod.TIMED_COUNT);
                  break;
               case "Sweep netting":
                  addSampleMethod(termId, SampleMethod.SWEEP_NETTING);
                  break;
               case "Box quadrat":
                  addSampleMethod(termId, SampleMethod.BOX_QUADRAT);
                  break;
               case "Suction sampler":
                  addSampleMethod(termId, SampleMethod.SUCTION_SAMPLER);
                  break;
               case "Other methods":
                  addSampleMethod(termId, SampleMethod.OTHER_METHOD);
                  break;
               default:
                  LOGGER.warn("Unknown sample method term \"{}\" with id {}",
                        term.getTerm().getTerm(), termId);
                  break;
               }
            });
   }

   private void addSampleMethod(Integer termId, SampleMethod sampleMethod)
   {
      sampleMethodMap.put(sampleMethod, termId);
      sampleMethodMapInvers.put(termId, sampleMethod);
   }
   private void fillSexMaps(DBTermlist sexList)
   {
      sexList.getTermListTerms().stream().filter(term -> !term.isDeleted())
            .forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case UNKNOWN:
                  addSex(termId, Sex.UNKNOWN);
                  break;
               case "male":
                  addSex(termId, Sex.MALE);
                  break;
               case "both":
                  addSex(termId, Sex.BOTH);
                  break;
               case "female":
                  addSex(termId, Sex.FEMALE);
                  break;
               default:
                  LOGGER.warn("Unknown sex term \"{}\" with id {}",
                        term.getTerm().getTerm(), termId);
                  break;
               }
            });
   }

   private void fillMakropterMaps(DBTermlist makropterList)
   {
      makropterList.getTermListTerms().stream().filter(term -> !term.isDeleted())
      .forEach(term ->
      {
         Integer termId = term.getId();
         switch (term.getTerm().getTerm())
         {
         case "UNKNOWN":
            addMakropter(termId, Makropter.UNKNOWN);
            break;
         case "YES":
            addMakropter(termId, Makropter.YES);
            break;
         case "NO":
            addMakropter(termId, Makropter.NO);
            break;
         default:
            LOGGER.warn("Unknown makropter term \"{}\" with id {}",
                  term.getTerm().getTerm(), termId);
            break;
         }
      });

   }

   private void fillNumericAmountAccuracyMaps(
         DBTermlist numericAmountAccuracyList)
   {
      numericAmountAccuracyList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case "MORETHAN":
                  addNumericAmountAccuracy(termId,
                        NumericAmountAccuracy.MORETHAN);
                  break;
               case "COUNTED":
                  addNumericAmountAccuracy(termId,
                        NumericAmountAccuracy.COUNTED);
                  break;
               case "ABOUT":
                  addNumericAmountAccuracy(termId, NumericAmountAccuracy.APPROXIMATE);
                  break;
               default:
                  LOGGER.warn(
                        "Unknown numerica amount accuracy term \"{}\" with id {}",
                        term.getTerm().getTerm(), termId);
                  break;
               }
            });
   }

   private void fillLifeStageMaps(DBTermlist lifeStageList)
   {
      lifeStageList.getTermListTerms().stream().filter(term -> !term.isDeleted())
      .forEach(term ->
      {
         Integer termId = term.getId();
         switch (term.getTerm().getTerm())
         {
         case "UNKNOWN":
            addLifeStage(termId, LifeStage.UNKNOWN);
            break;
         case "LARVE":
            addLifeStage(termId, LifeStage.LARVE);
            break;
         case "IMAGO":
            addLifeStage(termId, LifeStage.IMAGO);
            break;
         case "LARVE_IMAGO":
            addLifeStage(termId, LifeStage.LARVE_IMAGO);
            break;
         default:
            LOGGER.warn("Unknown lifeStage term \"{}\" with id {}",
                  term.getTerm().getTerm(), termId);
            break;
         }
      });

   }

   private void fillMediaTypesMaps(DBTermlist mediaTypesList)
   {
      mediaTypesList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case "Audio":
                  addMediaType(termId, MediaType.AUDIO_LINK);
                  break;
               case "Audio:Local":
                  addMediaType(termId, MediaType.AUDIO_LOCAL);
                  break;
               case "Image":
                  addMediaType(termId, MediaType.IMAGE_LINK);
                  break;
               case "Image:Local":
                  addMediaType(termId, MediaType.IMAGE_LOCAL);
                  break;
               default:
                  break;
               }

            });
   }


   private void fillRedListStatusMaps(DBTermlist redList)
   {
      redList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case "*":
                  addRedListStatus(termId, RedListStatus.SAFE);
                  break;
               case "nb":
                  addRedListStatus(termId, RedListStatus.NB);
                  break;
               case "0":
                  addRedListStatus(termId, RedListStatus.ZERO);
                  break;
               case "1":
                  addRedListStatus(termId, RedListStatus.ONE);
                  break;
               case "2":
                  addRedListStatus(termId, RedListStatus.TWO);
                  break;
               case "3":
                  addRedListStatus(termId, RedListStatus.THREE);
                  break;
               case "D":
                  addRedListStatus(termId, RedListStatus.D);
                  break;
               case "G":
                  addRedListStatus(termId, RedListStatus.G);
                  break;
               case "R":
                  addRedListStatus(termId, RedListStatus.R);
                  break;
               case "V":
                  addRedListStatus(termId, RedListStatus.V);
                  break;
               default:
                  break;
               }

            });
   }


   private void fillReproductionMaps(DBTermlist reproducitionList)
   {
      reproducitionList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case UNKNOWN:
                  addReproduction(termId, Reproduction.UNKNOWN);
                  break;
               case "sure":
                  addReproduction(termId, Reproduction.SURE);
                  break;
               case "plausible":
                  addReproduction(termId, Reproduction.PLAUSIBLE);
                  break;
               case "unlikely":
                  addReproduction(termId, Reproduction.UNLIKELY);
                  break;
               default:
                  LOGGER.warn("Unknown reproduction term \"{}\" with id {}",
                        term.getTerm().getTerm(), termId);
                  break;
               }
            });
   }

   private void fillBloomingSproutsMaps(DBTermlist bloomingSproutsList)
   {
      bloomingSproutsList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case BLOOMING_SPROUTS_LESS_ONE:
                  addBlooming(termId, BloomingSprouts.ONE_TO_FIVE);
                  break;
               case BLOOMING_SPROUTS_ONE_TO_FIVE:
                  addBlooming(termId, BloomingSprouts.ONE_TO_FIVE);
                  break;
               case BLOOMING_SPROUTS_SIX_TO_TWENTYFIVE:
                  addBlooming(termId, BloomingSprouts.SIX_TO_TWENTYFIVE);
                  break;
               case BLOOMING_SPROUTS_TWENTYSIX:
                  addBlooming(termId, BloomingSprouts.TWENTYSIX);
                  break;
               case BLOOMING_SPROUTS_FIFTY:
                  addBlooming(termId, BloomingSprouts.FIFTY);
                  break;
               case BLOOMING_SPROUTS_HUNDRED:
                  addBlooming(termId, BloomingSprouts.HUNDRED);
                  break;
               case BLOOMING_SPROUTS_THOUSAND:
                  addBlooming(termId, BloomingSprouts.THOUSAND);
                  break;
               case BLOOMING_SPROUTS_TENTHOUSAND:
                  addBlooming(termId, BloomingSprouts.TENTHOUSAND);
                  break;
               default:
                  LOGGER.warn("Unknown BloomingSprout term \"{}\" with id {}",
                        term.getTerm().getTerm(), termId);
                  break;
               }
            });
   }

   private void fillQuantityMaps(DBTermlist quantitiyList)
   {
      quantitiyList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case QUANTITY_ONE_TO_FIVE:
                  addQuantity(termId, Quantity.ONE_TO_FIVE);
                  break;
               case QUANTITY_SIX_TO_TWENTYFIVE:
                  addQuantity(termId, Quantity.SIX_TO_TWENTYFIVE);
                  break;
               case QUANTITY_TWENTYSIX:
                  addQuantity(termId, Quantity.TWENTYSIX);
                  break;
               case QUANTITY_FIFTY:
                  addQuantity(termId, Quantity.FIFTY);
                  break;
               case QUANTITY_HUNDRED:
                  addQuantity(termId, Quantity.HUNDRED);
                  break;
               case QUANTITY_THOUSAND:
                  addQuantity(termId, Quantity.THOUSAND);
                  break;
               case QUANTITY_TENTHOUSAND:
                  addQuantity(termId, Quantity.TENTHOUSAND);
                  break;
               default:
                  LOGGER.warn("Unknown QUANTITY term \"{}\" with id {}",
                        term.getTerm(), termId);
                  break;
               }
            });
   }

   private void fillOccurrenceStatusMaps(DBTermlist occurrenceStatusList)
   {
      occurrenceStatusList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case OCCURRENCE_STATUS_ANGESALBT_KULTIVIERT:
                  addOccurrenceStatus(termId,
                        SettlementStatusFukarek.ANGESALBT_KULTIVIERT);
                  break;
               case OCCURRENCE_STATUS_ANGESIEDELT:
                  addOccurrenceStatus(termId,
                        SettlementStatusFukarek.ANGESIEDELT);
                  break;
               case OCCURRENCE_STATUS_EINGEBUERGERT:
                  addOccurrenceStatus(termId,
                        SettlementStatusFukarek.EINGEBUERGERT);
                  break;
               case OCCURRENCE_STATUS_INDIGEN:
                  addOccurrenceStatus(termId, SettlementStatusFukarek.INDIGEN);
                  break;
               case OCCURRENCE_STATUS_SYNANTHROP:
                  addOccurrenceStatus(termId,
                        SettlementStatusFukarek.SYNANTHROP);
                  break;
               case OCCURRENCE_STATUS_UNBEKANNT:
                  addOccurrenceStatus(termId,
                        SettlementStatusFukarek.UNBEKANNT);
                  break;
               case OCCURRENCE_STATUS_UNBESTAENDIG:
                  addOccurrenceStatus(termId,
                        SettlementStatusFukarek.UNBESTAENDIG);
                  break;
               default:
                  LOGGER.warn("Unknown OccurrenceStatus term \"{}\" with id {}",
                        term.getTerm(), termId);
                  break;
               }
            });
   }

   private void fillVitalityMaps(DBTermlist vitalityList)
   {
      vitalityList.getTermListTerms().stream().filter(term -> !term.isDeleted())
            .forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case VITALITY_ABGESTORBEN:
                  addVitality(termId, Vitality.ABGESTORBEN);
                  break;
               case VITALITY_ABSTERBEND:
                  addVitality(termId, Vitality.ABSTERBEND);
                  break;
               case VITALITY_KUEMMERND:
                  addVitality(termId, Vitality.KUEMMERND);
                  break;
               case VITALITY_VITAL:
                  addVitality(termId, Vitality.VITAL);
                  break;
               default:
                  LOGGER.warn("Unknown Vitality term \"{}\" with id {}",
                        term.getTerm(), termId);
                  break;
               }
            });
   }

   private void fillValidationStatusMaps(DBTermlist validationStatusList)
   {
      validationStatusList.getTermListTerms().stream()
            .filter(term -> !term.isDeleted()).forEach(term ->
            {
                     Integer termId = term.getId();
                     switch (term.getTerm().getTerm())
                     {
                     case VALIDATION_VALID:
                        addValidationStatus(termId, ValidationStatus.VALID);
                        break;
                     case VALIDATION_PROBABLE:
                        addValidationStatus(termId, ValidationStatus.PROBABLE);
                        break;
                     case VALIDATION_UNKNOWN:
                        addValidationStatus(termId, ValidationStatus.UNKNOWN);
                        break;
                     case VALIDATION_IMPROBALE:
                        addValidationStatus(termId, ValidationStatus.IMPROBALE);
                        break;
                     case VALIDATION_INVALID:
                        addValidationStatus(termId, ValidationStatus.INVALID);
                        break;
                     default:
                        LOGGER.warn(
                              "Unknown ValidationStatus term \"{}\" with id {}",
                              term.getTerm(), termId);
                        break;
                     }
            });
   }

   private void fillAmountMaps(DBTermlist amountList)
   {
      amountList.getTermListTerms().stream().filter(term -> !term.isDeleted())
            .forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case AMOUNT_ONE_TITLE:
                  addAmount(termId, Amount.ONE);
                  break;
               case AMOUNT_FEW_TITLE:
                  addAmount(termId, Amount.FEW);
                  break;
               case AMOUNT_SOME_TITLE:
                  addAmount(termId, Amount.SOME);
                  break;
               case AMOUNT_A_LOT_TITLE:
                  addAmount(termId, Amount.A_LOT);
                  break;
               case AMOUNT_SCATTERED:
                  addAmount(termId, Amount.SCATTERED);
                  break;
               default:
                  LOGGER.warn("Unknown amount term \"{}\" with id {}",
                        term.getTerm(), termId);
                  break;
               }
            });
   }

   private void fillAreaMaps(DBTermlist areaList)
   {
      areaList.getTermListTerms().stream().filter(term -> !term.isDeleted())
            .forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case COVERED_AREA_LESS_ONE:
                  addArea(termId, Area.LESS_ONE);
                  break;
               case COVERED_AREA_ONE_TO_FIVE:
                  addArea(termId, Area.ONE_TO_FIVE);
                  break;
               case COVERED_AREA_SIX_TO_TWENTYFIVE:
                  addArea(termId, Area.SIX_TO_TWENTYFIVE);
                  break;
               case COVERED_AREA_TWENTYSIX:
                  addArea(termId, Area.TWENTYSIX);
                  break;
               case COVERED_AREA_FIFTY:
                  addArea(termId, Area.FIFTY);
                  break;
               case COVERED_AREA_HUNDRED:
                  addArea(termId, Area.HUNDRED);
                  break;
               case COVERED_AREA_THOUSAND:
                  addArea(termId, Area.THOUSAND);
                  break;
               case COVERED_AREA_TENTHOUSAND:
                  addArea(termId, Area.TENTHOUSAND);
                  break;
               default:
                  LOGGER.warn("Unknown area term \"{}\" with id {}",
                        term.getTerm(), termId);
                  break;
               }
            });
   }

   private void fillStatusMaps(DBTermlist statusList)
   {
      statusList.getTermListTerms().stream().filter(term -> !term.isDeleted())
            .forEach(term ->
            {
               Integer termId = term.getId();
               switch (term.getTerm().getTerm())
               {
               case STATUS_WILD_TITLE:
                  addStatus(termId, SettlementStatus.WILD);
                  break;
               case STATUS_PLANTED_TITLE:
                  addStatus(termId, SettlementStatus.PLANTED);
                  break;
               case STATUS_RESETTELD_TITLE:
                  addStatus(termId, SettlementStatus.RESETTELD);
                  break;
               default:
                  LOGGER.warn("Unknown status term \"{}\" with id {}",
                        term.getTerm(), termId);
                  break;
               }
            });
   }

   private void addMediaType(Integer termId, MediaType mediaType)
   {
      mediaTypes.put(mediaType, termId);
      mediaTypesInvers.put(termId, mediaType);
   }

   private void addRedListStatus(Integer termId, RedListStatus redListStatus)
   {
      redListStatusMap.put(redListStatus, termId);
      redListStatusMapInvers.put(termId, redListStatus);
   }

   private void addAmount(Integer termId, Amount amount)
   {
      amountMap.put(amount, termId);
      amountMapInvers.put(termId, amount);
   }

   private void addBlooming(Integer termId, BloomingSprouts sprout)
   {
      bloomingSproutsMap.put(sprout, termId);
      bloomingSproutsMapInvers.put(termId, sprout);
   }

   private void addQuantity(Integer termId, Quantity quantity)
   {
      quantityMap.put(quantity, termId);
      quantityMapInvers.put(termId, quantity);
   }

   private void addVitality(Integer termId, Vitality vitality)
   {
      vitalityMap.put(vitality, termId);
      vitalityMapInvers.put(termId, vitality);
   }

   private void addValidationStatus(Integer termId,
         ValidationStatus validationStatus)
   {
      validationStatusMap.put(validationStatus, termId);
      validationStatusMapInvers.put(termId, validationStatus);
   }

   private void addReproduction(Integer termId, Reproduction reproduction)
   {
      reproductionMap.put(reproduction, termId);
      reproductionMapInvers.put(termId, reproduction);
   }

   private void addSex(Integer termId, Sex sex)
   {
      sexMap.put(sex, termId);
      sexMapInvers.put(termId, sex);
   }

   private void addMakropter(Integer termId, Makropter makropter)
   {
      makropterMap.put(makropter, termId);
      makropterMapInvers.put(termId, makropter);
   }

   private void addLifeStage(Integer termId, LifeStage lifeStage)
   {
      lifeStageMap.put(lifeStage, termId);
      lifeStageMapInvers.put(termId, lifeStage);
   }

   private void addOccurrenceStatus(Integer termId,
         SettlementStatusFukarek occurrenceStatus)
   {
      occurrenceStatusFukarekMap.put(occurrenceStatus, termId);
      occurrenceStatusMapFukarekInvers.put(termId, occurrenceStatus);
   }

   private void addArea(Integer termId, Area area)
   {
      areaMap.put(area, termId);
      areaMapInvers.put(termId, area);
   }

   private void addStatus(Integer termId, SettlementStatus status)
   {
      statusMap.put(status, termId);
      statusMapInvers.put(termId, status);
   }

   private void addNumericAmountAccuracy(Integer termId,
         NumericAmountAccuracy numericAmountAccuracy)
   {
      numericAmountAccuracyMap.put(numericAmountAccuracy, termId);
      numericAmountAccuracyMapInvers.put(termId, numericAmountAccuracy);
   }

   public int getId(NumericAmountAccuracy numericAmountAccuracy)
   {
      return numericAmountAccuracyMap.get(numericAmountAccuracy);
   }

   public int getId(MediaType mediaType)
   {
      return mediaTypes.get(mediaType);
   }

   public int getId(Sex sex)
   {
      return sexMap.get(sex);
   }

   public int getId(Reproduction reproduction)
   {
      return reproductionMap.get(reproduction);
   }

   public Sex getSex(int id)
   {
      return sexMapInvers.get(id);
   }

   public int getId(LifeStage lifeStage)
   {
      return lifeStageMap.get(lifeStage);
   }

   public int getId(Makropter makropter)
   {
      return makropterMap.get(makropter);
   }

   public LifeStage getLifeStage(int id)
   {
      return lifeStageMapInvers.get(id);
   }

   public Makropter getMakropter(int id)
   {
      return makropterMapInvers.get(id);
   }

   public Reproduction getReproduction(int id)
   {
      return reproductionMapInvers.get(id);
   }

   public int getId(Amount amount)
   {
      return amountMap.get(amount);
   }

   public int getId(SettlementStatus status)
   {
      return statusMap.get(status);
   }

   public Amount getAmount(int id)
   {
      return amountMapInvers.get(id);
   }

   public Vitality getVitality(int id)
   {
      return vitalityMapInvers.get(id);
   }

   public int getId(Vitality vitality)
   {
      return vitalityMap.get(vitality);
   }

   public int getId(ValidationStatus validationStatus)
   {
      return validationStatusMap.get(validationStatus);
   }

   public SettlementStatusFukarek getOccurrenceStatusFukarek(int id)
   {
      return occurrenceStatusMapFukarekInvers.get(id);
   }

   public int getId(SettlementStatusFukarek occurrenceStatus)
   {
      return occurrenceStatusFukarekMap.get(occurrenceStatus);
   }

   public BloomingSprouts getBloomingSprouts(int id)
   {
      return bloomingSproutsMapInvers.get(id);
   }

   public int getId(BloomingSprouts bloomingSprouts)
   {
      return bloomingSproutsMap.get(bloomingSprouts);
   }

   public Area getArea(int id)
   {
      return areaMapInvers.get(id);
   }

   public NumericAmountAccuracy getNumericAmountAccuracy(int id)
   {
      return numericAmountAccuracyMapInvers.get(id);
   }

   public Quantity getQuantity(int id)
   {
      return quantityMapInvers.get(id);
   }

   public int getId(Quantity quantity)
   {
      return quantityMap.get(quantity);
   }

   public RedListStatus getRedListStatus(int id)
   {
      return redListStatusMapInvers.get(id);
   }

   public int getId(RedListStatus redListStatus)
   {
      return redListStatusMap.get(redListStatus);
   }


   public int getId(Area area)
   {
      return areaMap.get(area);
   }

   public SettlementStatus getStatus(int id)
   {
      return statusMapInvers.get(id);
   }

   public MediaType getMediaType(int id)
   {
      return mediaTypesInvers.get(id);
   }

   public Map<MediaType, Integer> getMediaTypes()
   {
      return mediaTypes;
   }

   public Map<Integer, MediaType> getMediaTypesInvers()
   {
      return mediaTypesInvers;
   }

   Map<Amount, Integer> getAmountMap()
   {
      return amountMap;
   }

   Map<Integer, Amount> getAmountMapInvers()
   {
      return amountMapInvers;
   }

   Map<SettlementStatus, Integer> getStatusMap()
   {
      return statusMap;
   }

   Map<Integer, SettlementStatus> getStatusMapInvers()
   {
      return statusMapInvers;
   }

   @Override
   public int hashCode()
   {
      return TermConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TermConfigBeanUtil.doEquals(this, obj);
   }

   @Override
   public String toString()
   {
      return TermConfigBeanUtil.doToString(this);
   }

   public Map<Vitality, Integer> getVitalityMap()
   {
      return vitalityMap;
   }

   public Map<Integer, Vitality> getVitalityMapInvers()
   {
      return vitalityMapInvers;
   }

   public Map<SettlementStatusFukarek, Integer> getOccurrenceStatusFukarekMap()
   {
      return occurrenceStatusFukarekMap;
   }

   public Map<Integer, SettlementStatusFukarek> getOccurrenceStatusMapFukarekInvers()
   {
      return occurrenceStatusMapFukarekInvers;
   }

   public Map<BloomingSprouts, Integer> getBloomingSproutsMap()
   {
      return bloomingSproutsMap;
   }

   public Map<Integer, BloomingSprouts> getBloomingSproutsMapInvers()
   {
      return bloomingSproutsMapInvers;
   }

   public Map<Quantity, Integer> getQuantityMap()
   {
      return quantityMap;
   }

   public Map<Integer, Quantity> getQuantityMapInvers()
   {
      return quantityMapInvers;
   }

   public Map<Sex, Integer> getSexMap()
   {
      return sexMap;
   }

   public Map<Integer, Sex> getSexMapInvers()
   {
      return sexMapInvers;
   }

   public Map<Reproduction, Integer> getReproductionMap()
   {
      return reproductionMap;
   }

   public Map<Integer, Reproduction> getReproductionMapInvers()
   {
      return reproductionMapInvers;
   }

   public Map<Area, Integer> getAreaMap()
   {
      return areaMap;
   }

   public Map<Integer, Area> getAreaMapInvers()
   {
      return areaMapInvers;
   }

   public Map<SampleMethod, Integer> getSampleMethodMap()
   {
      return sampleMethodMap;
   }

   public SampleMethod getSampleMethod(int sampleMethodId)
   {
      return sampleMethodMapInvers.get(sampleMethodId);
   }

   public Map<Integer, SampleMethod> getSampleMethodMapInvers()
   {
      return sampleMethodMapInvers;
   }

   public Map<RedListStatus, Integer> getRedListStatusMap()
   {
      return redListStatusMap;
   }

   public Map<Integer, RedListStatus> getRedListStatusMapInvers()
   {
      return redListStatusMapInvers;
   }

   public ValidationStatus getValidationStatus(int id)
   {
      return validationStatusMapInvers.get(id);
   }

   public Map<ValidationStatus, Integer> getValidationStatusMap()
   {
      return validationStatusMap;
   }

   public Map<Integer, ValidationStatus> getValidationStatusMapInvers()
   {
      return validationStatusMapInvers;
   }

   public Map<LifeStage, Integer> getLifeStageMap()
   {
      return lifeStageMap;
   }

   public Map<Integer, LifeStage> getLifeStageMapInvers()
   {
      return lifeStageMapInvers;
   }

   public Map<Makropter, Integer> getMakropterMap()
   {
      return makropterMap;
   }

   public Map<Integer, Makropter> getMakropterMapInvers()
   {
      return makropterMapInvers;
   }

   public Map<NumericAmountAccuracy, Integer> getNumericAmountAccuracyMap()
   {
      return numericAmountAccuracyMap;
   }

   public Map<Integer, NumericAmountAccuracy> getNumericAmountAccuracyMapInvers()
   {
      return numericAmountAccuracyMapInvers;
   }
}
