package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrence_attribute")
public class OccurrenceAttribute extends AbstractAttribute implements Attribute
{
}