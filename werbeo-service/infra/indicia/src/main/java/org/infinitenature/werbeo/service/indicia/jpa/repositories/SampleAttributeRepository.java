package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface SampleAttributeRepository
      extends JpaRepository<DBSampleAttribute, Integer>,
      QuerydslPredicateExecutor<DBSampleAttribute>
{

}
