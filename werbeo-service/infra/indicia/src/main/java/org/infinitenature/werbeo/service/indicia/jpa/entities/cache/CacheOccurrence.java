package org.infinitenature.werbeo.service.indicia.jpa.entities.cache;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBWebsite;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasOccurrenceAttributeValues;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasPosition;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasSampleAttributeValues;
import org.infinitenature.werbeo.service.indicia.jpa.entities.HasVagueDate;

import org.locationtech.jts.geom.Geometry;

@Entity
@Table(name = "cache_occurrences")
public class CacheOccurrence implements HasOccurrenceAttributeValues,
      HasVagueDate, HasSampleAttributeValues, HasPosition
{
   @Id
   protected Integer id;
   @Column(name = "attr_det_first_name")
   private String determinerFirstName;
   @Column(name = "attr_det_last_name")
   private String determinerLastName;
   @Column(name = "survey_title")
   private String surveyTitle;
   @Column(name = "taxon")
   private String taxon;
   @Column(name = "public_geom")
   private Geometry geometry;
   @Column(name = "public_entered_sref")
   private String enteredSref;
   @Column(name = "entered_sref_system")
   private String enteredSrefSystem;
   @Column(name = "date_start")
   private LocalDate startDate;
   @Column(name = "date_end")
   private LocalDate endDate;
   @Column(name = "date_type")
   private String dateType;
   @OneToMany(mappedBy = "occurrence", fetch = FetchType.EAGER)
   private Set<DBOccurrenceAttributeValue> occurrenceAttributeValues;
   @OneToMany
   @JoinColumn(name = "sample_id", referencedColumnName = "id")
   private Collection<DBSampleAttributeValue> sampleAttributeValues;
   @Column(name = "survey_id", insertable = false, updatable = false)
   private int surveyId;
   @Column(name = "sample_id")
   private int sampleId;

   @JoinColumn(name = "website_id", referencedColumnName = "id")
   @ManyToOne
   private DBWebsite website;
   private Character recordStatus;

   @Column(name = "taxa_taxon_list_id")
   private int taxaTaxonListId;

   @Column(name = "preferred_taxa_taxon_list_id")
   private int preferredTaxaTaxonListId;

   @Column(name = "cache_created_on")
   @Temporal(TemporalType.TIMESTAMP)
   protected Date createdOn;

   @Column(name = "cache_updated_on")
   @Temporal(TemporalType.TIMESTAMP)
   protected Date updatedOn;

   @Override
   public Set<DBOccurrenceAttributeValue> getOccurrenceAttributeValues()
   {
      return occurrenceAttributeValues;
   }

   public int getSampleId()
   {
      return sampleId;
   }

   public void setSampleId(int sampleId)
   {
      this.sampleId = sampleId;
   }

   @Override
   public Collection<DBSampleAttributeValue> getSampleAttributeValues()
   {
      return sampleAttributeValues;
   }

   public int getSurveyId()
   {
      return surveyId;
   }

   public void setSurveyId(int surveyId)
   {
      this.surveyId = surveyId;
   }

   public void setSampleAttributeValues(
         Collection<DBSampleAttributeValue> sampleAttributeValues)
   {
      this.sampleAttributeValues = sampleAttributeValues;
   }

   public Integer getId()
   {
      return id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   public void setOccurrenceAttributeValues(
         Set<DBOccurrenceAttributeValue> occurrenceAttributeValues)
   {
      this.occurrenceAttributeValues = occurrenceAttributeValues;
   }

   public Character getRecordStatus()
   {
      return recordStatus;
   }

   public void setRecordStatus(Character recordStatus)
   {
      this.recordStatus = recordStatus;
   }


   public int getTaxaTaxonListId()
   {
      return taxaTaxonListId;
   }

   public void setTaxaTaxonListId(int taxaTaxonListId)
   {
      this.taxaTaxonListId = taxaTaxonListId;
   }

   public int getPreferredTaxaTaxonListId()
   {
      return preferredTaxaTaxonListId;
   }

   public void setPreferredTaxaTaxonListId(int preferredTaxaTaxonListId)
   {
      this.preferredTaxaTaxonListId = preferredTaxaTaxonListId;
   }

   public String getSurveyTitle()
   {
      return surveyTitle;
   }

   public void setSurveyTitle(String surveyTitle)
   {
      this.surveyTitle = surveyTitle;
   }

   public String getTaxon()
   {
      return taxon;
   }

   public void setTaxon(String taxon)
   {
      this.taxon = taxon;
   }

   @Override
   public Geometry getGeometry()
   {
      return geometry;
   }

   public void setGeometry(Geometry geometry)
   {
      this.geometry = geometry;
   }

   @Override
   public LocalDate getStartDate()
   {
      return startDate;
   }

   public void setStartDate(LocalDate startDate)
   {
      this.startDate = startDate;
   }

   @Override
   public LocalDate getEndDate()
   {
      return endDate;
   }

   public void setEndDate(LocalDate endDate)
   {
      this.endDate = endDate;
   }

   @Override
   public String getDateType()
   {
      return dateType;
   }

   public void setDateType(String dateType)
   {
      this.dateType = dateType;
   }



   public DBWebsite getWebsite()
   {
      return website;
   }

   public void setWebsite(DBWebsite website)
   {
      this.website = website;
   }

   public String getDeterminerFirstName()
   {
      return determinerFirstName;
   }

   public void setDeterminerFirstName(String determinerFirstName)
   {
      this.determinerFirstName = determinerFirstName;
   }

   public String getDeterminerLastName()
   {
      return determinerLastName;
   }

   public void setDeterminerLastName(String determinerLastName)
   {
      this.determinerLastName = determinerLastName;
   }

   @Override
   public String getEnteredSref()
   {
      return enteredSref;
   }

   public void setEnteredSref(String enteredSref)
   {
      this.enteredSref = enteredSref;
   }

   @Override
   public String getEnteredSrefSystem()
   {
      return enteredSrefSystem;
   }

   public void setEnteredSrefSystem(String etneredSrefSystem)
   {
      this.enteredSrefSystem = etneredSrefSystem;
   }

   public Date getCreatedOn()
   {
      return createdOn;
   }

   public void setCreatedOn(Date createdOn)
   {
      this.createdOn = createdOn;
   }

   public Date getUpdatedOn()
   {
      return updatedOn;
   }

   public void setUpdatedOn(Date updatedOn)
   {
      this.updatedOn = updatedOn;
   }

}
