package org.infinitenature.werbeo.service.adapters;

import java.util.UUID;

import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceCommentsCommands;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.IndiciaApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceCommentsCommansAdapter
      implements OccurrenceCommentsCommands
{
   @Autowired
   private IndiciaApi indiciaApi;
   @Autowired
   private IdQueries idQueries;
   @Autowired
   private IndiciaUserQueries indiciaUserQueries;

   @Override
   public int addComment(UUID occurrenceID, OccurrenceComment comment,
         Context context)
   {
      int occurrenceId = idQueries.getIndiciaOccurrenceId(occurrenceID);
      org.infinitenature.werbeo.service.indicia.http.types.OccurrenceComment indiciaOccurrenceComment = new org.infinitenature.werbeo.service.indicia.http.types.OccurrenceComment(
            comment.getComment(), occurrenceId);
      return indiciaApi.saveOccurrenceComment(indiciaOccurrenceComment,
            context.getPortal().getId(), indiciaUserQueries.getUserId(context));
   }
}
