package org.infinitenature.werbeo.service.indicia.jpa.entities;

import org.locationtech.jts.geom.Geometry;

public interface HasPosition
{
   public String getEnteredSref();

   public String getEnteredSrefSystem();

   public Geometry getGeometry();
}
