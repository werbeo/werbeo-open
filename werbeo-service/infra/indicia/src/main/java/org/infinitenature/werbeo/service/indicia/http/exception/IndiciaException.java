package org.infinitenature.werbeo.service.indicia.http.exception;

public class IndiciaException extends RuntimeException
{
   public IndiciaException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public IndiciaException(String message)
   {
      super(message);
   }

   @Deprecated
   public IndiciaException(Throwable cause)
   {
      super(cause);
   }
}