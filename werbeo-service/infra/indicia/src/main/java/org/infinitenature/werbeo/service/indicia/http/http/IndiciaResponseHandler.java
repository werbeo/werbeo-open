package org.infinitenature.werbeo.service.indicia.http.http;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.infinitenature.werbeo.service.indicia.http.exception.EntityNotFoundException;
import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaAuthenticationException;
import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaHttpException;

/**
 * Response handler for the communication with indicia. Extracts the body of the
 * response to a String or wraps the error code to an exception.
 * 
 * @author dve
 * 
 */
public class IndiciaResponseHandler implements ResponseHandler<String>
{
   @Override
   public String handleResponse(HttpResponse response)
         throws ClientProtocolException, IOException
   {

      HttpEntity entity = response.getEntity();
      String responseString = entity == null ? null
            : EntityUtils.toString(entity, "utf-8");

      StatusLine statusLine = response.getStatusLine();
      if (statusLine.getStatusCode() >= 300)
      {
         switch (statusLine.getStatusCode())
         {
         case 403:
            throw new IndiciaAuthenticationException(
                  statusLine.getReasonPhrase(), responseString);
         case 404:
            throw new EntityNotFoundException(statusLine.getReasonPhrase(),
                  responseString);
         default:
            throw new IndiciaHttpException(statusLine.getReasonPhrase(),
                  statusLine.getStatusCode(), responseString);
         }
      }
      return responseString;
   }
}