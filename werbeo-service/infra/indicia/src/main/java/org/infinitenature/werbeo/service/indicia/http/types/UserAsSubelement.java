package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
public class UserAsSubelement implements User
{

   private String name;

   private int id;

   @Override
   @XmlAttribute
   public int getId()
   {
      return id;
   }

   @Override
   @XmlValue
   public String getName()
   {
      return name;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   @Override
   public int getPersonId()
   {
      return 0;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("UserAsSubelement [name=");
      builder.append(name);
      builder.append(", id=");
      builder.append(id);
      builder.append("]");
      return builder.toString();
   }
}