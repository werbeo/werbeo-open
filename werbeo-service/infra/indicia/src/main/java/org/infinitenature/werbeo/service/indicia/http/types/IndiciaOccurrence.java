package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Date;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement(name = "occurrence")
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class IndiciaOccurrence extends IndiciaTypeImpl
{
   public IndiciaOccurrence()
   {
      super();
   }

   public IndiciaOccurrence(int id)
   {
      super();
      this.id = id;
   }

   @Override
   public String getModelName()
   {
      return "occurrence";
   }

   public static final String COMPLETED = "C";
   public static final String DATE_TYPE_DAY = "D";
   public static final String DATE_TYPE_YEAR = "Y";
   public static final String IN_PROGRESS = "I";
   public static final String REJECTED = "R";
   public static final String SEND_FOR_VERIFICATION = "V";

   public static final String TEST = "T";
   public static final String VERIFIED = "V";

   @XmlElement(name = "comment")
   private String comment;
   @XmlElement(name = "date_type")
   private String dateType;
   @XmlElement(name = "determiner")
   private Determiner determiner = new Determiner();
   @XmlElement(name = "date_end")
   private Date endDate;
   @XmlElement(name = "entered_sref")
   private String enteredSref;
   @XmlElement(name = "entered_sref_system")
   private String enteredSrefSystem;
   @XmlElement(name = "external_key")
   private String externalKey;
   @XmlElement(name = "geom")
   private String geom;
   @XmlElement(name = "record_status")
   private String recordStatus;
   @XmlElement(name = "sample_id")
   private int sampleId;
   @XmlElement(name = "date_start")
   private Date startDate;
   @XmlElement(name = "survey")
   private String survey;
   @XmlElement(name = "survey_id")
   private int surveyId;
   @XmlElement(name = "taxa_taxon_list_id")
   private int taxaTaxonListId;
   @XmlElement(name = "taxon")
   private String taxon;
   @XmlElement(name = "website_id")
   private int websiteId;
   @XmlElement(name = "wkt")
   private String wkt;
   @XmlElement(name = "taxon_meaning_id")
   private int taxonMeaningId;

   private Set<AdditionalAttribute> sampleAttributes;
   private Set<AdditionalAttribute> occurrenceAttributes;

   public String getComment()
   {
      return comment;
   }

   public String getDateType()
   {
      return dateType;
   }

   public Determiner getDeterminer()
   {
      return determiner;
   }

   public int getDeterminerId()
   {
      return determiner.getId();
   }

   public String getDeterminerName()
   {
      return determiner.getName();
   }

   public Date getEndDate()
   {
      return endDate;
   }

   public String getEnteredSref()
   {
      return enteredSref;
   }

   public String getEnteredSrefSystem()
   {
      return enteredSrefSystem;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public String getGeom()
   {
      return geom;
   }

   public String getRecordStatus()
   {
      return recordStatus;
   }

   public int getSampleId()
   {
      return sampleId;
   }

   public Date getStartDate()
   {
      return startDate;
   }

   public String getSurvey()
   {
      return survey;
   }

   public int getSurveyId()
   {
      return surveyId;
   }

   public int getTaxaTaxonListId()
   {
      return taxaTaxonListId;
   }

   public String getTaxon()
   {
      return taxon;
   }

   public int getWebsiteId()
   {
      return websiteId;
   }

   public String getWkt()
   {
      return wkt;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public void setDateType(String dateType)
   {
      this.dateType = dateType;
   }

   public void setDeterminerId(int determinerId)
   {
      this.determiner.setId(determinerId);
   }

   public void setDeterminerName(String determinerName)
   {
      this.determiner.name = determinerName;
   }

   public void setEndDate(Date endDate)
   {
      this.endDate = endDate;
   }

   public void setEnteredSref(String enterdSref)
   {
      this.enteredSref = enterdSref;
   }

   public void setEnteredSrefSystem(String enterdSrefSystem)
   {
      this.enteredSrefSystem = enterdSrefSystem;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public void setGeom(String geom)
   {
      this.geom = geom;
   }

   public void setRecordStatus(String recordStatus)
   {
      this.recordStatus = recordStatus;
   }

   public void setSampleId(int sampleId)
   {
      this.sampleId = sampleId;
   }

   public void setStartDate(Date startDate)
   {
      this.startDate = startDate;
   }

   public void setSurvey(String survey)
   {
      this.survey = survey;
   }

   public void setSurveyId(int surveyId)
   {
      this.surveyId = surveyId;
   }

   public void setTaxaTaxonListId(int taxaTaxonListId)
   {
      this.taxaTaxonListId = taxaTaxonListId;
   }

   public void setTaxon(String taxon)
   {
      this.taxon = taxon;
   }

   public void setWebsiteId(int websiteId)
   {
      this.websiteId = websiteId;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

   public Set<AdditionalAttribute> getSampleAttributes()
   {
      return sampleAttributes;
   }

   public void setSampleAttributes(Set<AdditionalAttribute> sampleAttributes)
   {
      this.sampleAttributes = sampleAttributes;
   }

   public Set<AdditionalAttribute> getOccurrenceAttributes()
   {
      return occurrenceAttributes;
   }

   public void setOccurrenceAttributes(
         Set<AdditionalAttribute> occurrenceAttributes)
   {
      this.occurrenceAttributes = occurrenceAttributes;
   }

   public int getTaxonMeaningId()
   {
      return taxonMeaningId;
   }

   public void setTaxonMeaningId(int taxonMeaningId)
   {
      this.taxonMeaningId = taxonMeaningId;
   }

   public void setDeterminer(Determiner determiner)
   {
      this.determiner = determiner;
   }

   @Override
   public String toString()
   {
      return IndiciaOccurrenceBeanUtil.doToString(this);
   }
}