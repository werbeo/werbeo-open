package org.infinitenature.werbeo.service.indicia.http.types;

public enum ReportSource
{
   local, remote, provided
}
