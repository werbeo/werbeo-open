package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "termlists_terms")
public class TermListTermContainer
{
   private List<TermListTerm> termListTerms = new ArrayList<>();

   @XmlElement(name = "termlists_term")
   public List<TermListTerm> getTermListTerms()
   {
      return termListTerms;
   }

   public void setTermListTerms(List<TermListTerm> termListTerms)
   {
      this.termListTerms = termListTerms;
   }

}
