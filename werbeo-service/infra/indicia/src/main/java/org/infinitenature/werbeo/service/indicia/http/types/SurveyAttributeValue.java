package org.infinitenature.werbeo.service.indicia.http.types;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "survey_attribute_value")
@XmlAccessorType(XmlAccessType.FIELD)
public class SurveyAttributeValue extends IndiciaTypeImpl
      implements IndiciaAdditionalValue
{
   @Override
   public String getModelName()
   {
      return "survey_attribute_value";
   }

   @NotNull
   @XmlElement(name = "survey_id")
   private int surveyId;
   @NotNull
   @XmlElement(name = "survey_attribute_id")
   private int surveyAttributeId;
   @XmlElement(name = "data_type")
   private String dataType;
   @XmlElement(name = "caption")
   private String caption;
   @NotNull
   @XmlElement(name = "value")
   private String value;
   @XmlElement(name = "website_id")
   private int websiteId;

   public SurveyAttributeValue()
   {
      super();
   }

   public SurveyAttributeValue(int surveyId, int surveyAttributeId,
         String value)
   {
      super();
      this.surveyId = surveyId;
      this.surveyAttributeId = surveyAttributeId;
      this.value = value;
   }

   public SurveyAttributeValue(int id, int surveyId, int surveyAttributeId,
         String value)
   {
      super();
      this.id = id;
      this.surveyId = surveyId;
      this.surveyAttributeId = surveyAttributeId;
      this.value = value;
   }

   public String getDataType()
   {
      return dataType;
   }

   public void setDataType(String dataType)
   {
      this.dataType = dataType;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   @Override
   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   public int getWebsiteId()
   {
      return websiteId;
   }

   public void setWebsiteId(int websiteId)
   {
      this.websiteId = websiteId;
   }

   @Override
   public int getValueId()
   {
      return getId();
   }

   public int getSurveyId()
   {
      return surveyId;
   }

   public void setSurveyId(int surveyId)
   {
      this.surveyId = surveyId;
   }

   public int getSurveyAttributeId()
   {
      return surveyAttributeId;
   }

   public void setSurveyAttributeId(int surveyAttributeId)
   {
      this.surveyAttributeId = surveyAttributeId;
   }

   @Override
   public String toString()
   {
      return "SurveyAttributeValue [surveyId=" + surveyId
            + ", surveyAttributeId=" + surveyAttributeId + ", dataType="
            + dataType + ", caption=" + caption + ", value=" + value
            + ", websiteId=" + websiteId + "]";
   }

   @Override
   public int getAttributeId()
   {
      return surveyAttributeId;
   }

}
