package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CacheOccurrenceRequestMapperIndicia
      extends RequestMapperIndicia<OccurrenceSortField>
{

   @Override
   default Function<OccurrenceSortField, String[]> getSortFieldMapper()
   {
      return sortField ->
      {
         switch (sortField)
         {
         case TAXON:
            return new String[] { "taxon" };
         case DETERMINER:
            return new String[] { "determinerLastName", "determinerFirstName" };
         case DATE:
            return new String[] { "startDate", "endDate" };
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
