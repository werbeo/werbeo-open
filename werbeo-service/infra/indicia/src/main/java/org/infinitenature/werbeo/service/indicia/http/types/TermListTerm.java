package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "termlists_term")
@XmlAccessorType(XmlAccessType.FIELD)
public class TermListTerm extends IndiciaTypeImpl
{
   @Override
   public String getModelName()
   {
      return "termlist_term";
   }

   @XmlElement(name = "term")
   private String term;
   @XmlElement(name = "termlist")
   private String termlist;
   @XmlElement(name = "meaning_id")
   private String meaningId;
   @XmlElement(name = "preferred")
   private boolean preferred;
   @XmlElement(name = "iso")
   private String language;

   public String getTerm()
   {
      return term;
   }

   public void setTerm(String term)
   {
      this.term = term;
   }

   public String getTermlist()
   {
      return termlist;
   }

   public void setTermlist(String termlist)
   {
      this.termlist = termlist;
   }

   public String getMeaningId()
   {
      return meaningId;
   }

   public void setMeaningId(String meaningId)
   {
      this.meaningId = meaningId;
   }

   public boolean isPreferred()
   {
      return preferred;
   }

   public void setPreferred(boolean preferred)
   {
      this.preferred = preferred;
   }

   public String getLanguage()
   {
      return language;
   }

   public void setLanguage(String language)
   {
      this.language = language;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("TermListTerm [term=");
      builder.append(term);
      builder.append(", termlist=");
      builder.append(termlist);
      builder.append(", meaningId=");
      builder.append(meaningId);
      builder.append(", preferred=");
      builder.append(preferred);
      builder.append(", language=");
      builder.append(language);
      builder.append(", id=");
      builder.append(id);
      builder.append(", createdOn=");
      builder.append(createdOn);
      builder.append(", updatedOn=");
      builder.append(updatedOn);
      builder.append(", createdBy=");
      builder.append(createdBy);
      builder.append(", updatedBy=");
      builder.append(updatedBy);
      builder.append("]");
      return builder.toString();
   }

}
