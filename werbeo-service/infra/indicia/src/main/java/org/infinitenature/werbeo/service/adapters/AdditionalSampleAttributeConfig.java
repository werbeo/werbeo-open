package org.infinitenature.werbeo.service.adapters;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class AdditionalSampleAttributeConfig
{
   private int precision;
   private int status;
   private int legnam;
   private int letterDate;
   private int locationDescription;
   private int mtb;
   private int uuid;
   private int availability;
   private int turboeVegAttribute;
   private int area;
   private int areaMin;
   private int areaMax;
   private int coverScaleCode;
   private int coverScaleName;
   private int locality;
   

   public int getPrecision()
   {
      return precision;
   }

   public void setPrecision(int precision)
   {
      this.precision = precision;
   }

   public int getStatus()
   {
      return status;
   }

   public void setStatus(int status)
   {
      this.status = status;
   }

   public int getLegnam()
   {
      return legnam;
   }

   public void setLegnam(int legnam)
   {
      this.legnam = legnam;
   }

   public int getLetterDate()
   {
      return letterDate;
   }

   public void setLetterDate(int letterDate)
   {
      this.letterDate = letterDate;
   }

   public int getLocationDescription()
   {
      return locationDescription;
   }

   public void setLocationDescription(int locationDescription)
   {
      this.locationDescription = locationDescription;
   }

   public int getMtb()
   {
      return mtb;
   }

   public void setMtb(int mtb)
   {
      this.mtb = mtb;
   }

   public int getUuid()
   {
      return uuid;
   }

   public void setUuid(int uuid)
   {
      this.uuid = uuid;
   }

   public int getAvailability()
   {
      return availability;
   }

   public void setAvailability(int availability)
   {
      this.availability = availability;
   }

   public int getTurboeVegAttribute()
   {
      return turboeVegAttribute;
   }

   public void setTurboeVegAttribute(int turboeVegAttribute)
   {
      this.turboeVegAttribute = turboeVegAttribute;
   }

   public int getArea()
   {
      return area;
   }

   public void setArea(int area)
   {
      this.area = area;
   }

   public int getAreaMin()
   {
      return areaMin;
   }

   public void setAreaMin(int areaMin)
   {
      this.areaMin = areaMin;
   }

   public int getAreaMax()
   {
      return areaMax;
   }

   public void setAreaMax(int areaMax)
   {
      this.areaMax = areaMax;
   }

   public int getCoverScaleCode()
   {
      return coverScaleCode;
   }

   public void setCoverScaleCode(int coverScaleCode)
   {
      this.coverScaleCode = coverScaleCode;
   }

   public int getCoverScaleName()
   {
      return coverScaleName;
   }

   public void setCoverScaleName(int coverScaleName)
   {
      this.coverScaleName = coverScaleName;
   }

   public int getLocality()
   {
      return locality;
   }

   public void setLocality(int locality)
   {
      this.locality = locality;
   }

   @Override
   public String toString()
   {
      return AdditionalSampleAttributeConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return AdditionalSampleAttributeConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return AdditionalSampleAttributeConfigBeanUtil.doEquals(this, obj);
   }
}
