package org.infinitenature.werbeo.service.adapters.config;

public interface IndiciaConstans
{
   public static final String STATUS_PLANTED_TITLE = "Pflanzung";
   public static final String STATUS_RESETTELD_TITLE = "Ansalbung";
   public static final String STATUS_WILD_TITLE = "Wildvorkommen";

   public static final String AMOUNT_A_LOT_TITLE = "100+ (sehr Viele)"; // AMOUNT = Menge
   public static final String AMOUNT_SOME_TITLE = "11-100 (viele)";
   public static final String AMOUNT_FEW_TITLE = "2-10 (wenige)";
   public static final String AMOUNT_ONE_TITLE = "1 (Einzelfund)";
   public static final String AMOUNT_SCATTERED = "zerstreut";

   public static final String COVERED_AREA_LESS_ONE = "<1";
   public static final String COVERED_AREA_ONE_TO_FIVE = "1-5";
   public static final String COVERED_AREA_SIX_TO_TWENTYFIVE = "6-25";
   public static final String COVERED_AREA_TWENTYSIX = "26-50";
   public static final String COVERED_AREA_FIFTY = ">50";
   public static final String COVERED_AREA_HUNDRED = ">100";
   public static final String COVERED_AREA_THOUSAND = ">1000";
   public static final String COVERED_AREA_TENTHOUSAND = ">10000";

   public static final String ORIGIN_STATUS_INDIGENOUS = "indigen";
   public static final String ORIGIN_STATUS_CULTIVATED = "kultiviert";
   public static final String ORIGIN_STATUS_IMPORTED = "eingeführt";
   public static final String ORIGIN_STATUS_ANSALBUNG = "angesalbt";
   public static final String ORIGIN_STATUS_FERAL = "verwildert";


   public static final String VITALITY_VITAL = "vital";
   public static final String VITALITY_KUEMMERND = "kuemmernd";
   public static final String VITALITY_ABSTERBEND = "absterbend";
   public static final String VITALITY_ABGESTORBEN = "abgestorben";


   public static final String OCCURRENCE_STATUS_UNBEKANNT = "unbekannt";
   public static final String OCCURRENCE_STATUS_INDIGEN = "indigen_n1";
   public static final String OCCURRENCE_STATUS_EINGEBUERGERT = "eingebuergert";
   public static final String OCCURRENCE_STATUS_UNBESTAENDIG = "unbestaendig";
   public static final String OCCURRENCE_STATUS_SYNANTHROP = "synanthrop";
   public static final String OCCURRENCE_STATUS_ANGESIEDELT = "angesiedelt";
   public static final String OCCURRENCE_STATUS_ANGESALBT_KULTIVIERT = "angesalbt";

   public static final String BLOOMING_SPROUTS_LESS_ONE = "1";
   public static final String BLOOMING_SPROUTS_ONE_TO_FIVE = "1-5";
   public static final String BLOOMING_SPROUTS_SIX_TO_TWENTYFIVE = "6-25";
   public static final String BLOOMING_SPROUTS_TWENTYSIX = "26-50";
   public static final String BLOOMING_SPROUTS_FIFTY = ">50";
   public static final String BLOOMING_SPROUTS_HUNDRED = ">100";
   public static final String BLOOMING_SPROUTS_THOUSAND = ">1000";
   public static final String BLOOMING_SPROUTS_TENTHOUSAND = ">10000";

   public static final String QUANTITY_ONE_TO_FIVE = "1-5"; // QUANTITY = Anzahl
   public static final String QUANTITY_SIX_TO_TWENTYFIVE = "6-25";
   public static final String QUANTITY_TWENTYSIX = "26-50";
   public static final String QUANTITY_FIFTY = ">50";
   public static final String QUANTITY_HUNDRED = ">100";
   public static final String QUANTITY_THOUSAND = ">1000";
   public static final String QUANTITY_TENTHOUSAND = ">10000";

   public static final String VALIDATION_VALID = "valid";
   public static final String VALIDATION_PROBABLE = "probable";
   public static final String VALIDATION_UNKNOWN = "unknown";
   public static final String VALIDATION_IMPROBALE = "improbale";
   public static final String VALIDATION_INVALID = "invalid";
}
