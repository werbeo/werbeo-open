package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class IndiciaTypeImpl implements IndiciaType
{
   @NotNull
   @XmlElement(name = "id")
   protected int id;
   @XmlElement(name = "created_on")
   protected Date createdOn;
   @XmlElement(name = "updated_on")
   protected Date updatedOn;
   @XmlElement(name = "created_by")
   protected UserAsSubelement createdBy = new UserAsSubelement();
   @XmlElement(name = "updated_by")
   protected UserAsSubelement updatedBy = new UserAsSubelement();

   @XmlElement(name = "deleted")
   protected boolean deleted;
   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getId()
    */
   @Override
   public int getId()
   {
      return id;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#setId(int)
    */
   @Override
   public void setId(int id)
   {
      this.id = id;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getCreatedOn()
    */
   @Override
   public Date getCreatedOn()
   {
      return createdOn;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#setCreatedOn(java.util.Date)
    */
   @Override
   public void setCreatedOn(Date createdOn)
   {
      this.createdOn = createdOn;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getUpdatedOn()
    */
   @Override
   public Date getUpdatedOn()
   {
      return updatedOn;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#setUpdatedOn(java.util.Date)
    */
   @Override
   public void setUpdatedOn(Date updatedOn)
   {
      this.updatedOn = updatedOn;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getUpdatedById()
    */
   @Override
   public long getUpdatedById()
   {
      return updatedBy.getId();
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getUpdatedByName()
    */
   @Override
   public String getUpdatedByName()
   {
      return updatedBy.getName();
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getCreatedById()
    */
   @Override
   public long getCreatedById()
   {
      return createdBy.getId();
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getCreatedByName()
    */
   @Override
   public String getCreatedByName()
   {
      return createdBy.getName();
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#setCreatedBy(int)
    */
   @Override
   public void setCreatedBy(int userId)
   {
      createdBy.setId(userId);
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#setUpdatedBy(int)
    */
   @Override
   public void setUpdatedBy(int userId)
   {
      updatedBy.setId(userId);
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getCreatedBy()
    */
   @Override
   public User getCreatedBy()
   {
      return createdBy;
   }

   /*
    * (non-Javadoc)
    *
    * @see
    * org.indiciaConnector.types.IndiciaType#setCreatedBy(org.indiciaConnector
    * .types.UserAsSubelement)
    */
   @Override
   public void setCreatedBy(UserAsSubelement createdBy)
   {
      this.createdBy = createdBy;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaType#getUpdatedBy()
    */
   @Override
   public User getUpdatedBy()
   {
      return updatedBy;
   }

   /*
    * (non-Javadoc)
    *
    * @see
    * org.indiciaConnector.types.IndiciaType#setUpdatedBy(org.indiciaConnector
    * .types.UserAsSubelement)
    */
   @Override
   public void setUpdatedBy(UserAsSubelement updatedBy)
   {
      this.updatedBy = updatedBy;
   }

   @Override
   public boolean isDeleted()
   {
      return deleted;
   }

   @Override
   public void setDeleted(boolean deleted)
   {
      this.deleted = deleted;
   }
}