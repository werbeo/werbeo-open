/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class DBUser extends Base implements Serializable
{

   private static final long serialVersionUID = 1L;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 30)
   private String username;
   @JoinColumn(name = "person_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBPerson person;
   public String getUsername()
   {
      return username;
   }

   public void setUsername(String username)
   {
      this.username = username;
   }

   public DBPerson getPerson()
   {
      return person;
   }

   public void setPerson(DBPerson personId)
   {
      this.person = personId;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBUser))
      {
         return false;
      }
      DBUser other = (DBUser) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.Users[ id=" + id + " ]";
   }

}
