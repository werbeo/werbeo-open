package org.infinitenature.werbeo.service.adapters.config;

import javax.transaction.Transactional;

import org.infinitenature.werbeo.service.indicia.jpa.repositories.TermlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class TermlistConfigFactory
{
   @Autowired
   private TermlistRepository termlistRepository;

   @Transactional
   @Cacheable("indicia-term-config")
   public TermConfig getTermConfig()
   {
      return new TermConfig(
            termlistRepository.findOneByTitleAndDeletedIsFalse("Menge"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("Status"),
            termlistRepository
                  .findOneByTitleAndDeletedIsFalse("BedeckteFlaeche"),
            termlistRepository
                  .findOneByTitleAndDeletedIsFalse("EinbuergStatus"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("Vitalitaet"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("VorkStatus"),
            termlistRepository
                  .findOneByTitleAndDeletedIsFalse("BluehendeSprosse"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("Anzahl"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("sex"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("reproduction"),
            termlistRepository
                  .findOneByTitleAndDeletedIsFalse("Sample methods"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("Media types"),
            termlistRepository
                  .findOneByTitleAndDeletedIsFalse("red-list-status"),
            termlistRepository
                  .findOneByTitleAndDeletedIsFalse("validation-status"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("LifeStage"),
            termlistRepository.findOneByTitleAndDeletedIsFalse("Makropter"),
            termlistRepository
                  .findOneByTitleAndDeletedIsFalse("numeric_amount_accuracy"));
   }
}
