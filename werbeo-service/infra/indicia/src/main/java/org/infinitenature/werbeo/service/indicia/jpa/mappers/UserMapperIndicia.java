package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapperIndicia
{
   @Mapping(target = "creationDate", source = "createdOn")
   @Mapping(target = "modificationDate", source = "updatedOn")
   @Mapping(target = "login", source = "person.emailAddress")
   User map(DBUser jpaUser);
}
