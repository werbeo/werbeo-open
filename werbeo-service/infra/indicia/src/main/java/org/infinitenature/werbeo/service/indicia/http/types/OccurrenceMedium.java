package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrence_medium")
@XmlAccessorType(XmlAccessType.FIELD)
public class OccurrenceMedium extends IndiciaTypeImpl implements MediumType
{
   @XmlElement(name = "occurrence_id")
   private int occurrenceId;
   @XmlElement
   private String path;
   @XmlElement
   private String caption;
   @XmlElement(name = "media_type")
   private MediaTypeAsSubelement mediaType;

   public OccurrenceMedium()
   {
      super();
   }

   public OccurrenceMedium(int id)
   {
      super();
      this.id = id;
   }

   public OccurrenceMedium(int occurrenceId, String path, String caption,
         int mediaTypeId)
   {
      super();
      this.occurrenceId = occurrenceId;
      this.path = path;
      this.caption = caption;
      this.mediaType = new MediaTypeAsSubelement(mediaTypeId);
   }


   public int getOccurrenceId()
   {
      return occurrenceId;
   }

   public void setOccurrenceId(int occurrenceId)
   {
      this.occurrenceId = occurrenceId;
   }

   @Override
   public String getPath()
   {
      return path;
   }

   public void setPath(String path)
   {
      this.path = path;
   }

   @Override
   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   @Override
   public MediaType getMediaType()
   {
      return mediaType;
   }

   public void setMediaType(MediaTypeAsSubelement mediaType)
   {
      this.mediaType = mediaType;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("OccurrenceMedium@");
      builder.append(System.identityHashCode(this));
      builder.append(" [occurrenceId=");
      builder.append(occurrenceId);
      builder.append(", ");
      if (path != null)
      {
         builder.append("path=");
         builder.append(path);
         builder.append(", ");
      }
      if (caption != null)
      {
         builder.append("caption=");
         builder.append(caption);
         builder.append(", ");
      }
      if (mediaType != null)
      {
         builder.append("mediaType=");
         builder.append(mediaType);
      }
      builder.append("]");
      return builder.toString();
   }

   @Override
   public String getModelName()
   {
      return "occurrence_medium";
   }

   @Override
   public int getTargetId()
   {
      return getOccurrenceId();
   }

}
