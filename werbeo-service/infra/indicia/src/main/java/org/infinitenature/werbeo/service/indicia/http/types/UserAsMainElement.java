package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserAsMainElement implements User
{
   @XmlElement(name = "id")
   private int id;
   @XmlElement(name = "username")
   private String username;
   @XmlElement(name = "person_id")
   private int personId;

   @Override
   public int getId()
   {
      return id;
   }

   @Override
   public String getName()
   {
      return username;
   }

   @Override
   public int getPersonId()
   {
      return personId;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("UserAsMainElement [id=");
      builder.append(id);
      builder.append(", username=");
      builder.append(username);
      builder.append(", personId=");
      builder.append(personId);
      builder.append("]");
      return builder.toString();
   }
}