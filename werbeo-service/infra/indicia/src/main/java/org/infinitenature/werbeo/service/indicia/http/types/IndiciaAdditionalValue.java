package org.infinitenature.werbeo.service.indicia.http.types;

public interface IndiciaAdditionalValue
{

   public abstract int getAttributeId();

   public abstract String getValue();

   public abstract int getValueId();

}