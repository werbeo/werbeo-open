package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MediaTypeAsSubelement implements MediaType
{
   @XmlAttribute
   private int id;
   @XmlValue
   private String typeDescription;

   public MediaTypeAsSubelement()
   {
      super();
   };

   public MediaTypeAsSubelement(int id)
   {
      super();
      this.id = id;
   }

   @Override
   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   @Override
   public String getTypeDescription()
   {
      return typeDescription;
   }

   public void setTypeDescription(String title)
   {
      this.typeDescription = title;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("MediaTypeAsSubelement@");
      builder.append(System.identityHashCode(this));
      builder.append(" [id=");
      builder.append(id);
      builder.append(", ");
      if (typeDescription != null)
      {
         builder.append("typeDescription=");
         builder.append(typeDescription);
      }
      builder.append("]");
      return builder.toString();
   }

}
