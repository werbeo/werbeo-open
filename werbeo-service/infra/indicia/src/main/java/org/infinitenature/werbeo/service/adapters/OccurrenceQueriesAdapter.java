package org.infinitenature.werbeo.service.adapters;

import static org.infinitenature.werbeo.service.adapters.QueriesUtil.createWebsiteAgreementQuery;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.StopWatch;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.adapters.entities.OccurrenceGeometry;
import org.infinitenature.werbeo.service.adapters.mapper.OccurrenceCentroidMapper;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.StreamSlice;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.IdOnly;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.OccurrenceCentroidRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.OccurrenceMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.OccurrenceRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.togglz.core.manager.FeatureManager;

import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;

@Component
public class OccurrenceQueriesAdapter implements OccurrenceQueries
{
   private static final int BATCH_SIZE = 150;
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceQueriesAdapter.class);
   @Autowired
   private OccurrenceRepository repository;
   @Autowired
   private OccurrenceRequestMapperIndicia requestMapper;
   @Autowired
   private OccurrenceMapperIndicia occurrenceMapper;
   @Autowired
   private IdQueries idQueries;
   @Autowired
   private FeatureManager featureManager;
   @Autowired
   private AdditionalAttributeConfigFactory configFactory;
   @Autowired
   private OccurrenceCentroidRequestMapperIndicia occurrenceCentroidRequestMapperIndicia;
   @Autowired
   private OccurrenceCentroidMapper occurrenceCentroidMapper;
   @Autowired
   private IndiciaOccurrenceQueryBuilder queryBuilder;

   @Override
   @Transactional(readOnly = true)
   public Slice<Occurrence, OccurrenceSortField> findOccurrences(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceSortField> offsetRequest)
   {
      BooleanExpression predicate = prepareQuery(filter, context);
      List<DBOccurrence> occurrences = findByIdSelect(offsetRequest, predicate);

      return new SliceImpl<>(occurrenceMapper.map(occurrences, context),
            offsetRequest);
   }

   @Override
   @Transactional(readOnly = true)
   public StreamSlice<Occurrence, UUID> streamOccurrencesForSinglePortal(
         Context context, ContinuationToken continuationToken)
   {
      BooleanExpression predicate = prepareStreamingQuery(context,
            continuationToken, false);
      return streamOccurrences(context, continuationToken, predicate);
   }

   @Override
   @Transactional(readOnly = true)
   public StreamSlice<Occurrence, UUID> streamOccurrences(Context context,
         ContinuationToken continuationToken)
   {
      BooleanExpression predicate = prepareStreamingQuery(context,
            continuationToken, true);
      return streamOccurrences(context, continuationToken, predicate);
   }

   private StreamSlice<Occurrence, UUID> streamOccurrences(Context context,
         ContinuationToken continuationToken, BooleanExpression predicate)
   {
      List<DBOccurrence> occurrences = repository.findWithoutCount(predicate,
            PageRequest.of(0, BATCH_SIZE, Direction.ASC, "updatedOn", "id"));
      StreamSlice<Occurrence, UUID> streamSlice = new StreamSlice<>();
      for (DBOccurrence occurrence : occurrences)
      {
         if (occurrence.isDeleted())
         {
            AdditionalOccurrenceAttributeConfig occurrenceConfig = configFactory
                  .getOccurrenceConfig();

            List<DBOccurrenceAttributeValue> uuidAttributeValues = occurrence
                  .getOccurrenceAttributeValues().stream()
                  .filter(value -> value.getAttribute()
                        .getId() == occurrenceConfig.getUuid())
                  .filter(value -> !value.getAttribute().isDeleted())
                  .collect(Collectors.toList());
            if (!uuidAttributeValues.isEmpty()) // there might be occurrences
                                                // without uuid in the database
                                                // from testing
            {
               streamSlice.addDeleted(UUID
                     .fromString(uuidAttributeValues.get(0).getTextValue()));
            } else
            {
               LOGGER.info(
                     "Occurrence without uuid, ignore it in the deleted list");
            }
         } else
         {
            try
            {
               streamSlice.addEntity(occurrenceMapper.map(occurrence, context));
            } catch (Exception e)
            {
               LOGGER.error(
                     "Failure mapping occurrence from indicia with indicia id {}",
                     occurrence.getId(), e);
            }
         }
      }
      ContinuationToken nextToken;
      if (occurrences.isEmpty())
      {
         nextToken = continuationToken;
      } else
      {
         int occurrencesSize = occurrences.size();
         DBOccurrence lastOccurrence = occurrences.get(occurrencesSize - 1);
         LOGGER.info("Found {} occurrences for token {}", occurrencesSize,
               continuationToken);
         nextToken = new ContinuationToken(lastOccurrence.getId(),
               lastOccurrence.getUpdatedOn().getTime());
      }
      streamSlice.setContinuationToken(nextToken);
      return streamSlice;
   }

   @Override
   public Slice<OccurrenceCentroid, OccurrenceCentroidSortField> findOccurrenceCentroids(
         OccurrenceFilter filter, Context context,
         OffsetRequest<OccurrenceCentroidSortField> offsetRequest)
   {
      FactoryExpression<OccurrenceGeometry> factoryExpression = Projections
            .bean(OccurrenceGeometry.class,
                  QDBOccurrence.dBOccurrence.sample.geometry.transform(31468)
                        .as("geometry"),
                  QDBOccurrence.dBOccurrence.sample.startDate.as("date"),
                  QDBOccurrence.dBOccurrence.sample.survey.id.as("surveyId"));

      filter.setIncludeChildTaxa(true);
      Page<OccurrenceGeometry> occurrenceGeometrys = repository
            .findWithProjection(factoryExpression,
                  prepareQuery(filter, context),
                  occurrenceCentroidRequestMapperIndicia.map(offsetRequest));

      return new SliceImpl<>(occurrenceCentroidMapper.mapGeometryToCentroidList(
            occurrenceGeometrys.getContent()), offsetRequest);
   }

   @Override
   public long countOcurrenceCentroids(OccurrenceFilter filter, Context context)
   {
      return repository.count(prepareQuery(filter, context));
   }

   private BooleanExpression prepareStreamingQuery(Context context,
         ContinuationToken continuationToken, boolean includePartnerPortals)
   {
      QDBOccurrence dboccurrence = QDBOccurrence.dBOccurrence;
      BooleanExpression query;
      if (includePartnerPortals)
      {
         query = dboccurrence.sample.survey.website
               .in(createWebsiteAgreementQuery(context));
      } else
      {
         query = dboccurrence.sample.survey.website.id
               .eq(context.getPortal().getId());
      }
      Date timestamp = new Date(continuationToken.getTimestamp());
      return query.and(dboccurrence.updatedOn
            .after(timestamp)
            // simplify things, until a better solution is found
//            .or(dboccurrence.sample.updatedOn.after(timestamp))
//            .or(dboccurrence.occurrenceAttributeValues.any().updatedOn
//                  .after(timestamp))
//            .or(dboccurrence.sample.sampleAttributeValues.any().updatedOn
//                  .after(timestamp))
            .or(dboccurrence.updatedOn
                  .eq(timestamp)
                  .and(dboccurrence.id.gt(continuationToken.getId()))));
   }

   private List<DBOccurrence> findByIdSelect(
         OffsetRequest<OccurrenceSortField> offsetRequest,
         BooleanExpression predicate)
   {
      LOGGER.info("Load occurrences by first selecting ids");
      StopWatch stopWatch = StopWatch.createStarted();
      Page<IdOnly> ids = repository.findWithProjection(
            Projections.bean(IdOnly.class, QDBOccurrence.dBOccurrence.id),
            predicate, requestMapper.map(offsetRequest));
      List<Integer> id = ids.getContent().stream().map(IdOnly::getId)
            .collect(Collectors.toList());
      stopWatch.stop();
      LOGGER.info("Getting occurrence ids took {}ms",
            stopWatch.getTime(TimeUnit.MILLISECONDS));
      stopWatch.reset();
      stopWatch.start();
      // add Alibi id, List should not be empty
      id.add(-1000);
      List<DBOccurrence> occurrences = repository.findDistinctByIdIn(id);
      sortById(occurrences, id);
      stopWatch.stop();
      LOGGER.info("Getting occurences by id took {}ms",
            stopWatch.getTime(TimeUnit.MILLISECONDS));
      return occurrences;
   }

   private void sortById(List<DBOccurrence> occurrences, List<Integer> ids)
   {
      Collections.sort(occurrences, new Comparator<DBOccurrence>()
      {
         @Override
         public int compare(DBOccurrence i1, DBOccurrence i2)
         {
            int index1 = ids.indexOf(i1.getId());
            int index2 = ids.indexOf(i2.getId());
            return Integer.valueOf(index1).compareTo(Integer.valueOf(index2));
         }
      });
   }

   @Override
   public long countOccurrences(OccurrenceFilter filter, Context context)
   {
      return repository.count(prepareQuery(filter, context));
   }

   protected BooleanExpression prepareQuery(OccurrenceFilter filter,
         Context context)
   {
      return queryBuilder.buildQuery(filter, context);
   }

   @Override
   @Transactional(readOnly = true)
   public Occurrence get(UUID uuid, Context context)
   {
      int id = idQueries.getIndiciaOccurrenceId(uuid);
      if (id == 0)
      {
         throw new EntityNotFoundException(
               "Occurrence not found with UUID = " + uuid);
      }
      return occurrenceMapper.map(repository.getOne(id), context);
   }

   @Override
   @Transactional(readOnly = true)
   public boolean notExists(UUID occurrenceUUID)
   {
      return idQueries.getIndiciaOccurrenceId(occurrenceUUID) == 0;
   }
}
