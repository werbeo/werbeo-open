package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.util.Optional;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxonGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaxonGroupRepository
      extends JpaRepository<DBTaxonGroup, Integer>,
      QueryDslPredicateAndProjectionExecutor<DBTaxonGroup, Integer>
{

   Optional<DBTaxonGroup> findOneByTitle(String title);

}
