package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_comments")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleCommentContainer
{
   @XmlElement(name = "sample_comment")
   private List<SampleComment> sampleComments = new ArrayList<>();

   public List<SampleComment> getSampleComments()
   {
      return sampleComments;
   }

   public void setSampleComments(List<SampleComment> sampleComments)
   {
      this.sampleComments = sampleComments;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SampleCommentContainer@");
      builder.append(System.identityHashCode(this));
      builder.append(" [sampleComments=");
      builder.append(sampleComments);
      builder.append("]");
      return builder.toString();
   }
}