package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sample_attributes")
public class DBSampleAttribute extends DBAbstractAttribute
{
   private static final long serialVersionUID = 1L;
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "sampleAttribute")
   private Collection<DBSampleAttributesWebsites> sampleAttributesWebsitesCollection;

   public Collection<DBSampleAttributesWebsites> getSampleAttributesWebsitesCollection()
   {
      return sampleAttributesWebsitesCollection;
   }

   public void setSampleAttributesWebsitesCollection(
         Collection<DBSampleAttributesWebsites> sampleAttributesWebsitesCollection)
   {
      this.sampleAttributesWebsitesCollection = sampleAttributesWebsitesCollection;
   }
}
