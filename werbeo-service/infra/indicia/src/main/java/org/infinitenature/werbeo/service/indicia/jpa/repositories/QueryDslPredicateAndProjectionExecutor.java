package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.querydsl.core.types.FactoryExpression;
import com.querydsl.core.types.Predicate;

@NoRepositoryBean
public interface QueryDslPredicateAndProjectionExecutor<T, ID extends Serializable>
      extends JpaRepository<T, ID>, QuerydslPredicateExecutor<T>
{

   <PROJ> Page<PROJ> findWithProjection(
         FactoryExpression<PROJ> factoryExpression, Predicate predicate,
         Pageable pageable);

   List<T> findWithoutCount(Predicate predicate, Pageable pageable);

   <PROJ> List<PROJ> findWithProjection(
         FactoryExpression<PROJ> factoryExpression, Predicate predicate);

   <PROJ> List<PROJ> findDistinctWithProjection(
         FactoryExpression<PROJ> factoryExpression, Predicate predicate);
}