package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryInit;

@Entity
@Table(name = "taxa")
public class DBTaxon extends Base implements HasTaxonAttributeValues
{
   private String taxon;

   private String externalKey;

   private String authority;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "taxon_group_id")
   private DBTaxonGroup taxonGroup;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "language_id")
   private DBLanguage language;

   @OneToOne(mappedBy = "taxon", fetch = FetchType.LAZY)
   @QueryInit("taxaList.website")
   private DBTaxaTaxonList taxaTaxonList;



   public String getTaxon()
   {
      return taxon;
   }

   public DBTaxaTaxonList getTaxaTaxonList()
   {
      return taxaTaxonList;
   }

   public void setTaxaTaxonList(DBTaxaTaxonList taxaTaxonList)
   {
      this.taxaTaxonList = taxaTaxonList;
   }

   public void setTaxon(String taxon)
   {
      this.taxon = taxon;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public DBTaxonGroup getTaxonGroup()
   {
      return taxonGroup;
   }

   public void setTaxonGroup(DBTaxonGroup taxonGroup)
   {
      this.taxonGroup = taxonGroup;
   }

   public DBLanguage getLanguage()
   {
      return language;
   }

   public void setLanguage(DBLanguage language)
   {
      this.language = language;
   }

   public String getAuthority()
   {
      return authority;
   }

   public void setAuthority(String authority)
   {
      this.authority = authority;
   }

   @Override
   public Set<DBTaxonAttributeValue> getTaxonAttributeValues()
   {
      return taxaTaxonList.getTaxonAttributeValues();
   }
}
