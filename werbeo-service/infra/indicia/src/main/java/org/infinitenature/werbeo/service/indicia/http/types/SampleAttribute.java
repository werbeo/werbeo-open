package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_attribute")
public class SampleAttribute extends AbstractAttribute implements Attribute
{
}