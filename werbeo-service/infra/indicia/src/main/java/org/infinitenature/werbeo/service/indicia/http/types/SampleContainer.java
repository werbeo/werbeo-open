package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "samples")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleContainer
{
   @XmlElement(name = "sample")
   private List<IndiciaSample> samples = new ArrayList<>();

   public List<IndiciaSample> getSamples()
   {
      return samples;
   }

   public void setSamples(List<IndiciaSample> samples)
   {
      this.samples = samples;
   }

}
