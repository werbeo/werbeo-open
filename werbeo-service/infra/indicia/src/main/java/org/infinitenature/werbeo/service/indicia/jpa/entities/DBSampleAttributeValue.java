package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "sample_attribute_values")
public class DBSampleAttributeValue extends DBAbstractAttributeValue
  implements Serializable
{
   private static final long serialVersionUID = 1L;

   @JoinColumn(name = "sample_attribute_id", referencedColumnName = "id")
   @ManyToOne
   private DBSampleAttribute sampleAttribute;
   @JoinColumn(name = "sample_id", referencedColumnName = "id")
   @ManyToOne
   private DBSample sample;

   public DBSampleAttribute getSampleAttribute()
   {
      return sampleAttribute;
   }

   public void setSampleAttribute(DBSampleAttribute sampleAttributeId)
   {
      this.sampleAttribute = sampleAttributeId;
   }

   public DBSample getSample()
   {
      return sample;
   }

   public void setSample(DBSample sample)
   {
      this.sample = sample;
   }

   @Override
   public Base getEntity()
   {
      return getSample();
   }

   @Override
   public DBAbstractAttribute getAttribute()
   {
      return getSampleAttribute();
   }
}
