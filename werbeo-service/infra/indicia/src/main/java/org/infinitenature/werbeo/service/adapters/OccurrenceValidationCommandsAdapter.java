package org.infinitenature.werbeo.service.adapters;

import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceValidationCommands;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.IndiciaApi;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceAttributeValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OccurrenceValidationCommandsAdapter
      implements OccurrenceValidationCommands
{

   @Autowired
   private IdQueries idQueries;
   @Autowired
   private IndiciaApi indiciaApi;
   @Autowired
   private IndiciaUserQueries userQueries;
   @Autowired
   private OccurrenceAttributeValueRepository occurrenceAttributeValueRepository;
   @Autowired
   private AdditionalAttributeConfigFactory additionalAttributeConfigFacotry;
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;

   @Override
   public void setValidationStatus(UUID occurrenceUUID, Validation validation,
         Context context)
   {
      int indiciaOccurrenceId = idQueries
            .getIndiciaOccurrenceId(occurrenceUUID);
      ValidationStatus validationStatus = validation.getStatus();
      String validationComment = validation.getComment();
      setValidationStatus(context, indiciaOccurrenceId, validationStatus);
      setValidationComment(context, indiciaOccurrenceId, validationComment);
   }

   private void setValidationComment(Context context, int indiciaOccurrenceId,
         String validationComment)
   {
      int validationCommentAttributeId = additionalAttributeConfigFacotry
            .getOccurrenceConfig().getValidationComment();
      Optional<DBOccurrenceAttributeValue> validationCommentAttributeValue = occurrenceAttributeValueRepository
            .findOneByOccurrenceIdAndOccurrenceAttributeIdAndDeletedIsFalse(
                  indiciaOccurrenceId, validationCommentAttributeId);

      if (StringUtils.isBlank(validationComment)
            && validationCommentAttributeValue.isPresent())
      {
         OccurrenceAttributeValue type = new OccurrenceAttributeValue();
         type.setId(validationCommentAttributeValue.get().getId());
         indiciaApi.delete(type, context.getPortal().getId(),
               userQueries.getUserId(context));
      } else if (StringUtils.isNotBlank(validationComment))
      {
         OccurrenceAttributeValue submissionValue = new OccurrenceAttributeValue();
         submissionValue.setIntValue(false);
         submissionValue.setOccurrenceId(indiciaOccurrenceId);

         submissionValue.setValue(validationComment);
         submissionValue.setOccurrenceAttributeId(validationCommentAttributeId);
         if (validationCommentAttributeValue.isPresent())
         {
            submissionValue
                  .setId(validationCommentAttributeValue.get().getId());
         }
         indiciaApi.save(submissionValue, context.getPortal().getId(),
               userQueries.getUserId(context));
      }
   }

   void setValidationStatus(Context context, int indiciaOccurrenceId,
         ValidationStatus validationStatus)
   {
      int validationStatusAttributeId = additionalAttributeConfigFacotry
            .getOccurrenceConfig().getValidationStatus();
      Optional<DBOccurrenceAttributeValue> validationStatusAttributeValue = occurrenceAttributeValueRepository
            .findOneByOccurrenceIdAndOccurrenceAttributeIdAndDeletedIsFalse(
                  indiciaOccurrenceId, validationStatusAttributeId);

      OccurrenceAttributeValue submissionValue = new OccurrenceAttributeValue();
      submissionValue.setOccurrenceId(indiciaOccurrenceId);

      submissionValue.setValue(String.valueOf(termlistConfigFactory
            .getTermConfig().getValidationStatusMap().get(validationStatus)));
      submissionValue.setOccurrenceAttributeId(validationStatusAttributeId);
      if (validationStatusAttributeValue.isPresent())
      {
         submissionValue.setId(validationStatusAttributeValue.get().getId());
      }
      indiciaApi.save(submissionValue, context.getPortal().getId(),
            userQueries.getUserId(context));
   }
}
