package org.infinitenature.werbeo.service.adapters;

import java.util.HashMap;
import java.util.Map;

import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.IndiciaApi;
import org.infinitenature.werbeo.service.indicia.http.types.UserIdentifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IndiciaUserQueries
{
   @Autowired
   private IndiciaApi indiciaApi;

   public int getUserId(Context context)
   {
      String email = context.getUser().getLogin();
      String lastName = context.getUser().getPerson().getLastName();
      String firstName = context.getUser().getPerson().getFirstName();

      return getUserId(email, lastName, firstName, context);
   }

   public int getUserId(String email, String lastName, String firstName,
         Context context)
   {
      Map<String, String> stringIdentifiers = new HashMap<>();
      stringIdentifiers.put("email", email);
      UserIdentifier userIdentifier = indiciaApi.identifyUser(stringIdentifiers,
            lastName,
            firstName, null,
            context.getPortal().getId());
      return userIdentifier.getUserId();
   }
}
