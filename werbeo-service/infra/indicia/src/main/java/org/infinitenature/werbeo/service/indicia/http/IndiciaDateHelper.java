package org.infinitenature.werbeo.service.indicia.http;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndiciaDateHelper
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(IndiciaDateHelper.class);

   public static Date parse(String value)
   {
      SimpleDateFormat sdf = new SimpleDateFormat(
            IndiciaApi.INDICIA_DATE_FORMAT);
      try
      {
         return sdf.parse(value);
      } catch (ParseException e)
      {
         LOGGER.error("error parsing date ", e);
         return null;
      }
   }

}
