package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "occurrence_media")
public class DBOccurrenceMedia extends Base
{
   @ManyToOne
   @JoinColumn(name = "occurrence_id", referencedColumnName = "id")
   private DBOccurrence occurrence;
   @ManyToOne
   @JoinColumn(name = "media_type_id", referencedColumnName = "id")
   private DBTermlistTerm mediaType;
   @Column(name = "path")
   private String path;
   @Column(name = "caption")
   private String caption;
   @Column(name = "external_details")
   private String externalDetails;
   @Column(name = "exif")
   private String exif;

   public DBOccurrence getOccurrence()
   {
      return occurrence;
   }

   public void setOccurrence(DBOccurrence dbOccurrence)
   {
      this.occurrence = dbOccurrence;
   }

   public DBTermlistTerm getMediaType()
   {
      return mediaType;
   }

   public void setMediaType(DBTermlistTerm mediaType)
   {
      this.mediaType = mediaType;
   }

   public String getPath()
   {
      return path;
   }

   public void setPath(String path)
   {
      this.path = path;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   public String getExternalDetails()
   {
      return externalDetails;
   }

   public void setExternalDetails(String externalDetails)
   {
      this.externalDetails = externalDetails;
   }

   public String getExif()
   {
      return exif;
   }

   public void setExif(String exif)
   {
      this.exif = exif;
   }
}
