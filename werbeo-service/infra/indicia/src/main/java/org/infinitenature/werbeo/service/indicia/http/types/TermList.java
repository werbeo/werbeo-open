package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "termlist")
@XmlAccessorType(XmlAccessType.FIELD)
public class TermList
{
   @XmlElement(name = "id")
   long id;
   @XmlElement(name = "title")
   String title;
   @XmlElement(name = "website_id")
   Long website_id;

   public long getId()
   {
      return id;
   }

   public void setId(long id)
   {
      this.id = id;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public Long getWebsite_id()
   {
      return website_id;
   }

   public void setWebsite_id(Long websiteId)
   {
      website_id = websiteId;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Termlist [id=");
      builder.append(id);
      builder.append(", title=");
      builder.append(title);
      builder.append(", website_id=");
      builder.append(website_id);
      builder.append("]");
      return builder.toString();
   }

}
