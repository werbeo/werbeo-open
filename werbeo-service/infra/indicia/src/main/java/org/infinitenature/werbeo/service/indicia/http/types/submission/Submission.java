package org.infinitenature.werbeo.service.indicia.http.types.submission;

import java.util.ArrayList;
import java.util.List;

import external.org.json.JSONException;
import external.org.json.JSONObject;

/**
 * Container for submissions to indicia
 * 
 * @author dve
 * 
 */
public class Submission
{

   private List<Model> models = new ArrayList<>();
   private List<SubModel> subModels = new ArrayList<>();

   public List<SubModel> getSubModels()
   {
      return subModels;
   }

   public void setSubModels(List<SubModel> subModels)
   {
      this.subModels = subModels;
   }

   public Submission(List<Model> models)
   {
      this.models = models;
   }

   public List<Model> getModels()
   {
      return models;
   }

   public void setModels(List<Model> models)
   {
      this.models = models;
   }

   public JSONObject getJsonObject() throws JSONException
   {
      JSONObject submission = new JSONObject();
      List<JSONObject> jsonModels = new ArrayList<>();
      for (Model model : models)
      {
         jsonModels.add(model.getJsonObject());
      }
      JSONObject entry = new JSONObject();
      entry.put("entries", jsonModels);
      submission.put("submission", entry);
      return submission;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Submission [models=");
      builder.append(models);
      builder.append("]");
      return builder.toString();
   }

}
