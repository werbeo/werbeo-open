package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrences")
public class OccurrenceContainer
{

   private List<IndiciaOccurrence> occurrenceList = new ArrayList<>();

   @XmlElement(name = "occurrence")
   public List<IndiciaOccurrence> getOccurenceList()
   {
      return occurrenceList;
   }

   public void setTaxaTaxonLists(List<IndiciaOccurrence> occurrenceList)
   {
      this.occurrenceList = occurrenceList;
   }

}
