package org.infinitenature.werbeo.service.indicia.http.types;

public interface MediumType extends IndiciaType

{
   public int getTargetId();

   public String getPath();

   public String getCaption();

   public MediaType getMediaType();
}
