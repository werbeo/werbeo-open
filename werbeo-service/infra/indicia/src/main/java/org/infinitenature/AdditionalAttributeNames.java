package org.infinitenature;

public class AdditionalAttributeNames
{
   public static final String AVAILABILITY = "availability";
   public static final String CONTAINER = "container";
   public static final String OBFUSCATION_POLICIES = "obfuscationPolicies";
   public static final String WERBEO_ORIGINAL = "werbeoOriginal";
   public static final String ALLOW_DATA_ENTRY = "allowDataEntry";
   public static final String TAG = "tag";
}
