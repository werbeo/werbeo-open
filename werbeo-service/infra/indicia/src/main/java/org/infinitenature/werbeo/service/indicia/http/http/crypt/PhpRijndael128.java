package org.infinitenature.werbeo.service.indicia.http.http.crypt;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PhpRijndael128
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PhpRijndael128.class);
   private static final String SPEC = "AES/ECB/NoPadding";
   private Cipher cipher;
   private Key key;

   public PhpRijndael128(byte[] keyBytesArg)

   {
      try
      {
         key = new SecretKeySpec(padKey(keyBytesArg), "AES");
         cipher = Cipher.getInstance(SPEC);
      } catch (NoSuchAlgorithmException | NoSuchPaddingException e)
      {
         LOGGER.error("Failure initinalizing encryption", e);
         throw new IndiciaException("Failure initializing encryption", e);
      }
   }

   /**
    * Encrypts data.
    */
   public byte[] encrypt(byte[] data)
   {
      try
      {
         cipher.init(Cipher.ENCRYPT_MODE, key);
         return cipher.doFinal(pad(data));
      } catch (InvalidKeyException | IllegalBlockSizeException
            | BadPaddingException e)
      {
         LOGGER.error("Failure encrypting", e);
         throw new IndiciaException("Failure encrypting", e);
      }

   }

   public byte[] decrypt(byte[] data)
   {
      try
      {
         cipher.init(Cipher.DECRYPT_MODE, key);
         return cipher.doFinal(data);
      } catch (InvalidKeyException | IllegalBlockSizeException
            | BadPaddingException e)
      {
         LOGGER.error("Failure decrypting", e);
         throw new IndiciaException("Failure decrypting", e);
      }

   }

   private byte[] pad(byte[] data)
   {
      int blockSize = cipher.getBlockSize();

      int len = data.length;
      int offset = len % blockSize;

      if (offset == 0)
      {
         return data;
      } else
      {
         byte[] pad = new byte[len + blockSize - offset];
         System.arraycopy(data, 0, pad, 0, data.length);
         return pad;
      }
   }

   private byte[] padKey(byte[] keyData)
   {
      int keySize = 16;
      int len = keyData.length;

      if (len == keySize)
      {
         return keyData;
      } else
      {
         byte[] paddedKey = new byte[keySize];
         System.arraycopy(keyData, 0, paddedKey, 0, len);
         return paddedKey;
      }
   }
}
