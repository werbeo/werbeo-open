package org.infinitenature.werbeo.service.adapters;

import org.infinitenature.werbeo.service.indicia.http.types.Person;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PersonMapper
{
   public static final PersonMapper INSTANCE = Mappers
         .getMapper(PersonMapper.class);

   @Mapping(target = "createdOn", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "updatedBy", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "createdBy", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "updatedOn", ignore = true) // The indicia system takes
                                                 // care of
   @Mapping(target = "websiteId", ignore = true)
   @Mapping(target = "emailAddress", source = "email")
   Person map(org.infinitenature.werbeo.service.core.api.enity.Person person);

}
