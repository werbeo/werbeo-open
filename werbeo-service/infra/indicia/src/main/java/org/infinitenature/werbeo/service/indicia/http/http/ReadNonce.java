package org.infinitenature.werbeo.service.indicia.http.http;

public class ReadNonce
{
   private String readNonce;

   public ReadNonce(String readNonce)
   {
      super();
      this.readNonce = readNonce;
   }

   public String getReadNonce()
   {
      return readNonce;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("ReadNonce [readNonce=");
      builder.append(readNonce);
      builder.append("]");
      return builder.toString();
   }

   @Override
   public int hashCode()
   {
      final int prime = 31;
      int result = 1;
      result = prime * result
            + ((readNonce == null) ? 0 : readNonce.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj)
   {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      ReadNonce other = (ReadNonce) obj;
      if (readNonce == null)
      {
         if (other.readNonce != null)
            return false;
      } else if (!readNonce.equals(other.readNonce))
         return false;
      return true;
   }
}