package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement(name = "taxa_taxon_list")
@XmlAccessorType(XmlAccessType.PROPERTY)
@Bean
public class TaxaTaxonList extends IndiciaTypeImpl
{
   @Override
   public String getModelName()
   {
      return "taxa_taxon_list";
   }

   private boolean allowDataEntry;
   private String authority;
   private String commonName;
   private String language;
   private boolean preferred;
   private String preferredName;
   private String taxonGroup;
   private int websiteId;
   private Taxon taxon;
   private TaxonList taxonList;
   private int taxonMeaningId;
   private String externalKey;

   public Taxon getTaxon()
   {
      return taxon;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   @XmlElement(name = "external_key")
   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   @XmlElement(name = "taxon_meaning_id")
   public void setTaxonMeaningId(int taxonMeaningId)
   {
      this.taxonMeaningId = taxonMeaningId;
   }

   public int getTaxonMeaningId()
   {
      return taxonMeaningId;
   }

   @XmlElement(name = "taxon")
   public void setTaxon(Taxon tax)
   {
      this.taxon = tax;
   }

   public String getAllowDataEntryString()
   {
      if (isAllowDataEntry())
      {
         return "t";
      } else
      {
         return "f";
      }
   }

   public String getAuthority()
   {
      return authority;
   }

   public String getCommonName()
   {
      return commonName;
   }

   public String getLanguage()
   {
      return language;
   }

   public String getPreferredName()
   {
      return preferredName;
   }

   public String getPreferredString()
   {
      if (isPreferred())
      {
         return "t";
      } else
      {
         return "f";
      }
   }

   public String getTaxonGroup()
   {
      return taxonGroup;
   }

   public int getTaxonId()
   {
      return getTaxon().getId();
   }

   public int getTaxonListId()
   {
      return taxonList.getId();
   }

   public String getTaxonListName()
   {
      return taxonList.getName();
   }

   public String getTaxonName()
   {
      return getTaxon().getTaxonName();
   }

   public int getWebsiteId()
   {
      return websiteId;
   }

   public boolean isAllowDataEntry()
   {
      return allowDataEntry;
   }

   public boolean isPreferred()
   {
      return preferred;
   }

   @XmlTransient
   public void setAllowDataEntry(boolean allowDataEntry)
   {
      this.allowDataEntry = allowDataEntry;
   }

   @XmlElement(name = "allow_data_entry")
   public void setAllowDataEntryString(String allowDataEntryString)
   {
      if (allowDataEntryString.equalsIgnoreCase("T"))
      {
         setAllowDataEntry(true);
      } else
      {
         setAllowDataEntry(false);
      }
   }

   public void setAuthority(String authority)
   {
      this.authority = authority;
   }

   @XmlElement(name = "common")
   public void setCommonName(String commonName)
   {
      this.commonName = commonName;
   }

   public void setLanguage(String language)
   {
      this.language = language;
   }

   @XmlTransient
   public void setPreferred(boolean preferred)
   {
      this.preferred = preferred;
   }

   @XmlElement(name = "preferred_name")
   public void setPreferredName(String preferredName)
   {
      this.preferredName = preferredName;
   }

   @XmlElement(name = "preferred")
   public void setPreferredString(String preferred)
   {
      if (preferred.equalsIgnoreCase("T"))
      {
         setPreferred(true);
      } else
      {
         setPreferred(false);
      }
   }

   @XmlElement(name = "taxon_group")
   public void setTaxonGroup(String taxonGroup)
   {
      this.taxonGroup = taxonGroup;
   }

   @XmlElement(name = "website_id")
   public void setWebsiteId(int websiteId)
   {
      this.websiteId = websiteId;
   }

   @Override
   public String toString()
   {
      return TaxaTaxonListBeanUtil.doToString(this);
   }

   @XmlElement(name = "taxon_list")
   public TaxonList getTaxonList()
   {
      return taxonList;
   }

   public void setTaxonList(TaxonList taxonList)
   {
      this.taxonList = taxonList;
   }

   @Override
   public int hashCode()
   {
      return TaxaTaxonListBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxaTaxonListBeanUtil.doEquals(this, obj);
   }

}
