package org.infinitenature.werbeo.service.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.infra.commands.SurveyCommands;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.IndiciaApi;
import org.infinitenature.werbeo.service.indicia.http.types.SurveySubmissionContrainer;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurveyAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSurveyAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SurveyAttributeValuesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service
public class SurveyCommandAdapter implements SurveyCommands
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyCommandAdapter.class);
   @Autowired
   private IndiciaApi indiciaApi;
   @Autowired
   private SurveyAttributeValuesRepository surveyAttributeValuesRepository;
   @Autowired
   private AdditionalAttributeConfigFactory configFactory;
   @Autowired
   private IndiciaUserQueries indiciaUserQueries;

   @Override
   @Transactional
   @CacheEvict(cacheNames = "surveys", allEntries = true)
   public int saveOrUpdate(Survey survey, Context context)
   {
      Map<String, String> existingAttributes = survey.getId() != null
            ? loadAdditionalAttributeValues(survey.getId())
            : new HashMap<>();
      Map<String, String> newAttributes = createAttributes(survey);
      SurveySubmissionContrainer ssc = new SurveySubmissionContrainer(
            SurveyMapper.INSTANCE.map(survey), Collections.emptySet(),
            CustomAttributeMerger.merge(newAttributes, existingAttributes));
      return indiciaApi.save(ssc, context.getPortal().getId(),
            indiciaUserQueries.getUserId(context));
   }

   private Map<String, String> createAttributes(Survey survey)
   {
      Map<String, String> attributes = new HashMap<>();
      AdditionalSurveyAttributeConfig config = configFactory.getSurveyConfig();
      attributes.put(String.valueOf(config.getAvailabitlityId()),
            survey.getAvailability().toString());
      attributes.put(String.valueOf(config.getContainerId()),
            String.valueOf(survey.isContainer()));
      attributes.put(String.valueOf(config.getObfuscationPoliciesId()),
            String.valueOf(merge(survey.getObfuscationPolicies())));
      attributes.put(String.valueOf(config.getWerbeoOriginalId()),
            String.valueOf(survey.isWerbeoOriginal()));
      attributes.put(String.valueOf(config.getAllowDataEntryId()),
            String.valueOf(survey.isAllowDataEntry()));
      attributes.putAll(createTags(config.getTagId(), survey.getTags()));
      return attributes;
   }

   private Map<String, String> createTags(int tagId, Set<String> tags)
   {
      Map<String, String> tagAttributes = new HashMap<>();
      for (String tag : tags)
      {
         tagAttributes.put(tagId + "::" + tagAttributes.size(), tag);
      }
      return tagAttributes;
   }

   protected String merge(List<ObfuscationPolicy> obfuscationPolicies)
   {
      List<String> policyStrings = new ArrayList<>();
      obfuscationPolicies.forEach(policy -> policyStrings
            .add(policy.getRole() + "_" + policy.getPermission()));
      return StringUtils.join(policyStrings, ",");
   }

   private Map<String, String> loadAdditionalAttributeValues(Integer surveyId)
   {
      QDBSurveyAttributeValue qSurveyAttributeValues = QDBSurveyAttributeValue.dBSurveyAttributeValue;

      Iterable<DBSurveyAttributeValue> jpaValues = surveyAttributeValuesRepository
            .findAll(qSurveyAttributeValues.survey.id.eq(surveyId)
                  .and(qSurveyAttributeValues.deleted.isFalse()));
      Map<String, String> additionalValues = new HashMap<>();
      jpaValues.forEach(jpaValue ->
      {
         String key = jpaValue.getSurveyAttribute().getId() + ":"
               + jpaValue.getId();
         String value = AttributeUtils.getValue(jpaValue);
         // TODO remove null check after implementing all types of values
         if (value != null)
         {
            additionalValues.put(key, value);
         }
      });

      return additionalValues;
   }

   @Override
   @CacheEvict(cacheNames = "surveys", allEntries = true)
   public void delete(Survey survey, Context context)
   {
      org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey indiciaSurvey = new org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey(
            survey.getId());
      indiciaApi.delete(indiciaSurvey, context.getPortal().getId(),
            indiciaUserQueries.getUserId(context));
   }

}
