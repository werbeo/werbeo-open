package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "survey_attributes")
public class DBSurveyAttribute extends DBAbstractAttribute implements Serializable
{

   private static final long serialVersionUID = 1L;
   @OneToMany(cascade = CascadeType.ALL, mappedBy = "surveyAttribute")
   private Collection<DBSurveyAttributesWebsites> surveyAttributesWebsitesCollection;
   @OneToMany(mappedBy = "surveyAttribute")
   private Collection<DBSurveyAttributeValue> surveyAttributeValuesCollection;

   public DBSurveyAttribute()
   {
   }

   public DBSurveyAttribute(Integer id)
   {
      this.id = id;
   }

   public DBSurveyAttribute(Integer id, Date createdOn, Date updatedOn,
         boolean public1, boolean deleted)
   {
      this.id = id;
      this.createdOn = createdOn;
      this.updatedOn = updatedOn;
      this.public1 = public1;
      this.deleted = deleted;
   }

   @XmlTransient
   public Collection<DBSurveyAttributesWebsites> getSurveyAttributesWebsitesCollection()
   {
      return surveyAttributesWebsitesCollection;
   }

   public void setSurveyAttributesWebsitesCollection(
         Collection<DBSurveyAttributesWebsites> surveyAttributesWebsitesCollection)
   {
      this.surveyAttributesWebsitesCollection = surveyAttributesWebsitesCollection;
   }

   @XmlTransient
   public Collection<DBSurveyAttributeValue> getSurveyAttributeValuesCollection()
   {
      return surveyAttributeValuesCollection;
   }

   public void setSurveyAttributeValuesCollection(
         Collection<DBSurveyAttributeValue> surveyAttributeValuesCollection)
   {
      this.surveyAttributeValuesCollection = surveyAttributeValuesCollection;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBSurveyAttribute))
      {
         return false;
      }
      DBSurveyAttribute other = (DBSurveyAttribute) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.SurveyAttributes[ id=" + id + " ]";
   }

}
