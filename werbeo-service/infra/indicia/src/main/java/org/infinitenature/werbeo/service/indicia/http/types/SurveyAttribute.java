package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "survey_attribute")
public class SurveyAttribute extends AbstractAttribute implements Attribute
{
}