/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author dve
 */
@Entity
@Table(name = "survey_attribute_values")
public class DBSurveyAttributeValue extends DBAbstractAttributeValue
      implements Serializable
{

   private static final long serialVersionUID = 1L;

   @JoinColumn(name = "survey_attribute_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBSurveyAttribute surveyAttribute;
   @JoinColumn(name = "survey_id", referencedColumnName = "id")
   @ManyToOne(fetch = FetchType.LAZY)
   private DBSurvey survey;

   public DBSurveyAttributeValue()
   {
   }

   @Override
   public Base getEntity()
   {
      return getSurvey();
   }

   @Override
   public DBAbstractAttribute getAttribute()
   {
      return getSurveyAttribute();
   }

   public DBSurveyAttributeValue(Integer id)
   {
      this.id = id;
   }

   public DBSurveyAttributeValue(Integer id, Date createdOn, Date updatedOn,
         boolean deleted)
   {
      this.id = id;
      this.createdOn = createdOn;
      this.updatedOn = updatedOn;
      this.deleted = deleted;
   }

   public DBSurveyAttribute getSurveyAttribute()
   {
      return surveyAttribute;
   }

   public void setSurveyAttribute(DBSurveyAttribute surveyAttribute)
   {
      this.surveyAttribute = surveyAttribute;
   }

   public DBSurvey getSurvey()
   {
      return survey;
   }

   public void setSurvey(DBSurvey survey)
   {
      this.survey = survey;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (id != null ? id.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are
      // not set
      if (!(object instanceof DBSurveyAttributeValue))
      {
         return false;
      }
      DBSurveyAttributeValue other = (DBSurveyAttributeValue) object;
      if ((this.id == null && other.id != null)
            || (this.id != null && !this.id.equals(other.id)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      return "org.infinitenature.indicia.jpa.SurveyAttributeValues[ id=" + id
            + " ]";
   }

}
