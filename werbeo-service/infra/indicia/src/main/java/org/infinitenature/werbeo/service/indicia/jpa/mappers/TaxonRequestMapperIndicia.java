package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TaxonRequestMapperIndicia
      extends RequestMapperIndicia<TaxonSortField>
{
   @Override
   default Function<TaxonSortField, String[]> getSortFieldMapper()
   {
      return taxonSortField ->
      {
         switch (taxonSortField)
         {
         case TAXON:
            return new String[] { "taxon" };
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
