package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CommentRequestMapperIndicia
      extends RequestMapperIndicia<CommentSortField>
{
   @Override
   default Function<CommentSortField, String[]> getSortFieldMapper()
   {
      return commentSortField ->
      {
         switch (commentSortField)
         {
         case MOD_DATE:
            return new String[] { "updatedOn" };
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
