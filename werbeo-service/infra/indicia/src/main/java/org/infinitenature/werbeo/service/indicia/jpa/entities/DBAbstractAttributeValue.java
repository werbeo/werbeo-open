package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class DBAbstractAttributeValue extends Base
{
   private static final long serialVersionUID = 1L;
   @Column(name = "date_end_value")
   @Temporal(TemporalType.DATE)
   private Date dateEndValue;
   @Column(name = "date_start_value")
   @Temporal(TemporalType.DATE)
   private Date dateStartValue;
   @Size(max = 2)
   @Column(name = "date_type_value")
   private String dateTypeValue;
   @Column(name = "float_value")
   private Double floatValue;
   @Column(name = "int_value")
   private Integer intValue;
   @Size(max = 2147483647)
   @Column(name = "text_value")
   private String textValue;

   public DBAbstractAttributeValue()
   {
      super();
   }

   public abstract Base getEntity();

   public abstract DBAbstractAttribute getAttribute();

   public Integer getEntityId()
   {
      Base entity = getEntity();
      return (entity != null) ? entity.getId() : null;
   }

   public Date getDateEndValue()
   {
      return dateEndValue;
   }

   public Date getDateStartValue()
   {
      return dateStartValue;
   }

   public String getDateTypeValue()
   {
      return dateTypeValue;
   }

   public Double getFloatValue()
   {
      return floatValue;
   }

   public Integer getIntValue()
   {
      return intValue;
   }

   public String getTextValue()
   {
      return textValue;
   }

   public void setDateEndValue(Date dateEndValue)
   {
      this.dateEndValue = dateEndValue;
   }

   public void setDateStartValue(Date dateStartValue)
   {
      this.dateStartValue = dateStartValue;
   }

   public void setDateTypeValue(String dateTypeValue)
   {
      this.dateTypeValue = dateTypeValue;
   }

   public void setFloatValue(Double floatValue)
   {
      this.floatValue = floatValue;
   }

   public void setIntValue(Integer intValue)
   {
      this.intValue = intValue;
   }

   public void setTextValue(String textValue)
   {
      this.textValue = textValue;
   }

}