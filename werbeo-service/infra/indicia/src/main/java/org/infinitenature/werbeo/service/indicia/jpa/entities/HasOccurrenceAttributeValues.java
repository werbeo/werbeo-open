package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Set;

public interface HasOccurrenceAttributeValues
{
   Set<DBOccurrenceAttributeValue> getOccurrenceAttributeValues();
}
