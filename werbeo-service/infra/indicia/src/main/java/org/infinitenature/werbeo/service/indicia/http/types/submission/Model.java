package org.infinitenature.werbeo.service.indicia.http.types.submission;

import java.util.List;
import java.util.Set;

import external.org.json.JSONArray;
import external.org.json.JSONException;
import external.org.json.JSONObject;

public class Model
{
   private String id;
   private Set<Field> fields;
   private Set<Field> fkFields;
   private List<SubModel> subModels;
   private Set<Field> metaFields;

   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public Set<Field> getFields()
   {
      return fields;
   }

   public void setFields(Set<Field> fields)
   {
      this.fields = fields;
   }

   public Set<Field> getFkFields()
   {
      return fkFields;
   }

   public void setFkFields(Set<Field> fkFields)
   {
      this.fkFields = fkFields;
   }

   public List<SubModel> getSubModels()
   {
      return subModels;
   }

   public void setSubModels(List<SubModel> subModels)
   {
      this.subModels = subModels;
   }

   public Set<Field> getMetaFields()
   {
      return metaFields;
   }

   public void setMetaFields(Set<Field> meataFields)
   {
      this.metaFields = meataFields;
   }

   public Model(String id, Set<Field> fields)
   {
      this.id = id;
      this.fields = fields;
   }

   public JSONObject getJsonObject() throws JSONException
   {
      JSONObject jsonFields = new JSONObject();
      for (Field field : fields)
      {
         jsonFields.put(field.getId(), field.getJSONObject());
      }
      JSONObject modelValue = new JSONObject();
      modelValue.put("id", id);
      modelValue.put("fields", jsonFields);
      if (subModels != null && !subModels.isEmpty())
      {
         JSONArray jsonSubModels = new JSONArray();
         for (SubModel subModel : subModels)
         {
            jsonSubModels.put(subModel.getJsonObject());
         }
         modelValue.put("subModels", jsonSubModels);
      }
      if (metaFields != null && !metaFields.isEmpty())
      {
         JSONObject jsonMetaFields = new JSONObject();
         for (Field metaField : metaFields)
         {
            jsonMetaFields.put(metaField.getId(), metaField.getJSONObject());
         }
         modelValue.put("metaFields", jsonMetaFields);
      }
      JSONObject model = new JSONObject();
      model.put("model", modelValue);
      return modelValue;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Model [id=");
      builder.append(id);
      builder.append(", fields=");
      builder.append(fields);
      builder.append(", fkFields=");
      builder.append(fkFields);
      builder.append(", subModels=");
      builder.append(subModels);
      builder.append(", meataFields=");
      builder.append(metaFields);
      builder.append("]");
      return builder.toString();
   }
}
