package org.infinitenature.werbeo.service.adapters.entities;

import java.time.LocalDate;

import org.locationtech.jts.geom.Geometry;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceGeometry
{
   private LocalDate date;
   private Geometry geometry;
   private int surveyId;

   public int getSurveyId()
   {
      return surveyId;
   }

   public void setSurveyId(int surveyId)
   {
      this.surveyId = surveyId;
   }

   public LocalDate getDate()
   {
      return date;
   }

   public void setDate(LocalDate date)
   {
      this.date = date;
   }

   public Geometry getGeometry()
   {
      return geometry;
   }

   public void setGeometry(Geometry geometry)
   {
      this.geometry = geometry;
   }

   @Override
   public String toString()
   {
      return OccurrenceGeometryBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceGeometryBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceGeometryBeanUtil.doEquals(this, obj);
   }
}
