package org.infinitenature.werbeo.service.adapters;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.impl.SliceImpl;
import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBPerson;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.PersonMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.PersonRequestMapperIndicia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;

@Component
public class PersonQueriesAdapter implements PersonQueries
{
   @Autowired
   private PersonRepository repo;
   @Autowired
   private PersonMapperIndicia mapper;
   @Autowired
   private PersonRequestMapperIndicia requestMapper;
   @Autowired
   private AdditionalAttributeConfigFactory additionalAttributeConfigFactory;

   @Override
   @Transactional(readOnly = true)
   public Person get(Integer id, Context context)
   {
      QDBPerson qPeople = QDBPerson.dBPerson;
      BooleanExpression expression = qPeople.id.eq(id)
            .and(qPeople.deleted.isFalse());
      return mapper.map(repo.findOne(expression).orElse(null));
   }

   @Override
   @Transactional(readOnly = true)
//   @Cacheable(cacheNames = "people")
   public Slice<Person, PersonSortField> find(PersonFilter filter,
         Context context, OffsetRequest<PersonSortField> offsetRequest)
   {
      BooleanExpression predicate = prepareQuery(filter, context);

      return new SliceImpl<>(mapper.map(repo
            .findAll(predicate, requestMapper.map(offsetRequest)).getContent()),
            offsetRequest);

   }

   private BooleanExpression prepareQuery(PersonFilter filter, Context context)
   {
      QDBPerson qPeople = QDBPerson.dBPerson;
      BooleanExpression expression = qPeople.deleted.isFalse();
      if (StringUtils.isNotBlank(filter.getNameContains()))
      {
         expression = expression
               .and(qPeople.firstName
                     .containsIgnoreCase(filter.getNameContains())
                     .or(qPeople.lastName
                           .containsIgnoreCase(filter.getNameContains())));
      }
      if (StringUtils.isNotBlank(filter.getFirstName()))
      {
         expression = expression
               .and(qPeople.firstName.equalsIgnoreCase(filter.getFirstName()));
      }
      if (StringUtils.isNotBlank(filter.getLastName()))
      {
         expression = expression.and(qPeople.lastName.equalsIgnoreCase(filter.getLastName()));
      }
      if (StringUtils.isNotBlank(filter.getEmail()))
      {
         expression = expression
               .and(qPeople.emailAddress.equalsIgnoreCase(filter.getEmail()));
      }
      if (filter.getValidator() != null && filter.getValidator() == true)
      {
         QDBOccurrenceAttributeValue oav = QDBOccurrenceAttributeValue.dBOccurrenceAttributeValue;
         BooleanExpression isValidator = JPAExpressions.selectFrom(oav)
               .where(oav.deleted.isFalse().and(oav.occurrenceAttribute.id
                     .eq(additionalAttributeConfigFactory.getOccurrenceConfig()
                           .getValidationStatus())
                     .and(oav.occurrence.sample.survey.website.id
                           .eq(context.getPortal().getId()))
                     .and(oav.createdById.person.emailAddress
                           .eq(qPeople.emailAddress))))
               .exists();
         expression = expression.and(isValidator);
      }
      if (StringUtils.isNotBlank(filter.getExternalKey()))
      {
         expression = expression
               .and(qPeople.externalKey.eq(filter.getExternalKey()));
      }
      return expression;
   }

   @Override
   // @Cacheable(cacheNames = "people")
   public long count(PersonFilter filter, Context context)
   {
      return repo.count(prepareQuery(filter, context));
   }

}
