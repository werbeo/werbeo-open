package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBLanguage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<DBLanguage, Integer>
{

   DBLanguage getOneByIso(String iso);
}
