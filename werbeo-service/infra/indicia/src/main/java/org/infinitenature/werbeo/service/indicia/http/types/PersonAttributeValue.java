package org.infinitenature.werbeo.service.indicia.http.types;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "person_attribute_value")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonAttributeValue implements IndiciaAdditionalValue
{
   @XmlElement(name = "id")
   private int id;
   @NotNull
   @XmlElement(name = "person_id")
   private int personId;
   @NotNull
   @XmlElement(name = "person_attribute_id")
   private int personAttributeId;
   @XmlElement(name = "data_type")
   private String dataType;
   @XmlElement(name = "caption")
   private String caption;
   @NotNull
   @XmlElement(name = "value")
   private String value;
   @XmlElement(name = "raw_value")
   private String rawValue;
   @XmlElement(name = "website_id")
   private int websiteId;

   public int getId()
   {
      return id;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public int getPersonId()
   {
      return personId;
   }

   public void setPersonId(int personId)
   {
      this.personId = personId;
   }

   public int getPersonAttributeId()
   {
      return personAttributeId;
   }

   public void setPersonAttributeId(int personAttributeId)
   {
      this.personAttributeId = personAttributeId;
   }

   public String getDataType()
   {
      return dataType;
   }

   public void setDataType(String dataType)
   {
      this.dataType = dataType;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   @Override
   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   public int getWebsiteId()
   {
      return websiteId;
   }

   public void setWebsiteId(int websiteId)
   {
      this.websiteId = websiteId;
   }

   public String getRawValue()
   {
      return rawValue;
   }

   public void setRawValue(String rawValue)
   {
      this.rawValue = rawValue;
   }

   @Override
   public int getAttributeId()
   {
      return getPersonAttributeId();
   }

   @Override
   public int getValueId()
   {
      return getId();
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("PersonAttributeValue@");
      builder.append(System.identityHashCode(this));
      builder.append(" [id=");
      builder.append(id);
      builder.append(", personId=");
      builder.append(personId);
      builder.append(", personAttributeId=");
      builder.append(personAttributeId);
      builder.append(", dataType=");
      builder.append(dataType);
      builder.append(", caption=");
      builder.append(caption);
      builder.append(", value=");
      builder.append(value);
      builder.append(", rawValue=");
      builder.append(rawValue);
      builder.append(", websiteId=");
      builder.append(websiteId);
      builder.append("]");
      return builder.toString();
   }

}