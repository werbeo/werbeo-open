package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_comment")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleComment extends IndiciaTypeImpl implements CommentType
{
   @XmlElement
   private String comment;
   @XmlElement(name = "sample_id")
   private int sampleId;

   public SampleComment()
   {
      super();
   }

   public SampleComment(String comment)
   {
      super();
      this.comment = comment;
   }

   public SampleComment(String comment, int sampleId)
   {
      super();
      this.comment = comment;
      this.sampleId = sampleId;
   }

   @Override
   public String getModelName()
   {
      return "sample_comment";
   }

   @Override
   public int getTargetId()
   {
      return getSampleId();
   }

   @Override
   public void setTargetId(int targetId)
   {
      setSampleId(targetId);
   }

   @Override
   public String getComment()
   {
      return comment;
   }

   @Override
   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public int getSampleId()
   {
      return sampleId;
   }

   public void setSampleId(int occurrenceId)
   {
      this.sampleId = occurrenceId;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SampleComment@");
      builder.append(System.identityHashCode(this));
      builder.append(" [comment=");
      builder.append(comment);
      builder.append(", sampleId=");
      builder.append(sampleId);
      builder.append(", id=");
      builder.append(id);
      builder.append(", createdOn=");
      builder.append(createdOn);
      builder.append(", updatedOn=");
      builder.append(updatedOn);
      builder.append(", createdBy=");
      builder.append(createdBy);
      builder.append(", updatedBy=");
      builder.append(updatedBy);
      builder.append("]");
      return builder.toString();
   }
}