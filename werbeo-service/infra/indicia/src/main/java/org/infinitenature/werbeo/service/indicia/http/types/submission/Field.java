package org.infinitenature.werbeo.service.indicia.http.types.submission;

import external.org.json.JSONException;
import external.org.json.JSONObject;

public class Field implements Comparable<Field>
{
   private String id;
   private String value;

   public Field(String id, String value)
   {
      this.id = id;
      this.value = value;
   }

   public Field(String id, long value)
   {
      this.id = id;
      this.value = Long.toString(value);
   }

   public JSONObject getJSONObject() throws JSONException
   {
      JSONObject valuex = new JSONObject();
      valuex.put("value", value);
      return valuex;
   }

   public String getId()
   {
      return id;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Field [id=");
      builder.append(id);
      builder.append(", value=");
      builder.append(value);
      builder.append("]");
      return builder.toString();
   }

   @Override
   public int compareTo(Field o)
   {
      return this.id.compareTo(o.id);
   }
}
