package org.infinitenature.werbeo.service.adapters;

import java.io.ByteArrayInputStream;
import java.util.UUID;

import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.infra.commands.MediaCommands;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.IndiciaApi;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceMedium;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceMedia;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MediaCommandsAdapter implements MediaCommands
{
   @Autowired
   private IndiciaApi indiciaApi;
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;
   @Autowired
   private IdQueries idQueries;
   @Autowired
   private IndiciaUserQueries indiciaUserQueries;
   @Autowired
   private OccurrenceRepository occurrenceRepository;
   @Override
   public String uploadMedia(byte[] imageData, String fileName, Context context)
   {
      return indiciaApi.uploadFile(new ByteArrayInputStream(imageData),
            fileName, context.getPortal().getId());
   }

   @Override
   public int addOccurrenceMedia(UUID occurrenceID, String path,
         String description, MediaType mediaType, Context context)
   {
      int mediaTypeId = termlistConfigFactory.getTermConfig().getId(mediaType);
      int occurrenceId = idQueries.getIndiciaOccurrenceId(occurrenceID);
      OccurrenceMedium occurrenceMedium = new OccurrenceMedium(occurrenceId,
            path, description, mediaTypeId);
      return indiciaApi.save(occurrenceMedium, context.getPortal().getId(),
            indiciaUserQueries.getUserId(context));
   }

   @Override
   @Transactional(readOnly = true) // we don't write through jpa
   public void deleteOccurrenceMedium(UUID occurrenceId, Medium mediumToDelete,
         Context context)
   {
      int indiciaOccurrenceId = idQueries.getIndiciaOccurrenceId(occurrenceId);
      DBOccurrence occurrence = occurrenceRepository
            .getOne(indiciaOccurrenceId);

      int occurrenceMediaId = occurrence.getOccurrenceMedia().stream()
            .filter(
            occMedium -> occMedium.getPath().equals(mediumToDelete.getPath()))
            .mapToInt(DBOccurrenceMedia::getId).findAny().getAsInt();

      indiciaApi.delete(new OccurrenceMedium(occurrenceMediaId),
            context.getPortal().getId(), indiciaUserQueries.getUserId(context));
   }
}
