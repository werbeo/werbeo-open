package org.infinitenature.werbeo.service.indicia.http.types;

public class UserIdentifier
{
   private int userId;

   public UserIdentifier(int userId)
   {
      this.userId = userId;
   }

   public int getUserId()
   {
      return userId;
   }

   public void setUserId(int userId)
   {
      this.userId = userId;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("UserIdentifier [userId=");
      builder.append(userId);
      builder.append("]");
      return builder.toString();
   }
}