package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.querydsl.core.annotations.QueryInit;

@MappedSuperclass
public abstract class DBAbstractAttributesWebsites
{
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   @Basic(optional = false)
   protected Integer id;
   @Basic(optional = false)
   @NotNull
   @Column(name = "created_on")
   @Temporal(TemporalType.TIMESTAMP)
   protected Date createdOn;

   @Basic(optional = false)
   @NotNull
   protected boolean deleted;
   @JoinColumn(name = "created_by_id", referencedColumnName = "id")
   @ManyToOne(optional = false, fetch = FetchType.LAZY)
   @QueryInit("person.emailAddress")
   protected DBUser createdById;

   @Size(max = 500)
   @Column(name = "validation_rules")
   private String validationRules;
   @Basic(optional = false)
   @NotNull
   private int weight;
   @Size(max = 2147483647)
   @Column(name = "default_text_value")
   private String defaultTextValue;
   @Column(name = "default_float_value")
   private Double defaultFloatValue;
   @Column(name = "default_int_value")
   private Integer defaultIntValue;
   @Column(name = "default_date_start_value")
   @Temporal(TemporalType.DATE)
   private Date defaultDateStartValue;
   @Column(name = "default_date_end_value")
   @Temporal(TemporalType.DATE)
   private Date defaultDateEndValue;
   @Size(max = 2)
   @Column(name = "default_date_type_value")
   private String defaultDateTypeValue;

   public DBAbstractAttributesWebsites()
   {
      super();
   }

   public String getValidationRules()
   {
      return validationRules;
   }

   public void setValidationRules(String validationRules)
   {
      this.validationRules = validationRules;
   }

   public int getWeight()
   {
      return weight;
   }

   public void setWeight(int weight)
   {
      this.weight = weight;
   }

   public String getDefaultTextValue()
   {
      return defaultTextValue;
   }

   public void setDefaultTextValue(String defaultTextValue)
   {
      this.defaultTextValue = defaultTextValue;
   }

   public Double getDefaultFloatValue()
   {
      return defaultFloatValue;
   }

   public void setDefaultFloatValue(Double defaultFloatValue)
   {
      this.defaultFloatValue = defaultFloatValue;
   }

   public Integer getDefaultIntValue()
   {
      return defaultIntValue;
   }

   public void setDefaultIntValue(Integer defaultIntValue)
   {
      this.defaultIntValue = defaultIntValue;
   }

   public Date getDefaultDateStartValue()
   {
      return defaultDateStartValue;
   }

   public void setDefaultDateStartValue(Date defaultDateStartValue)
   {
      this.defaultDateStartValue = defaultDateStartValue;
   }

   public Date getDefaultDateEndValue()
   {
      return defaultDateEndValue;
   }

   public void setDefaultDateEndValue(Date defaultDateEndValue)
   {
      this.defaultDateEndValue = defaultDateEndValue;
   }

   public String getDefaultDateTypeValue()
   {
      return defaultDateTypeValue;
   }

   public void setDefaultDateTypeValue(String defaultDateTypeValue)
   {
      this.defaultDateTypeValue = defaultDateTypeValue;
   }

   public Integer getId()
   {
      return id;
   }

   public void setId(Integer id)
   {
      this.id = id;
   }

   public Date getCreatedOn()
   {
      return createdOn;
   }

   public void setCreatedOn(Date createdOn)
   {
      this.createdOn = createdOn;
   }

   public boolean isDeleted()
   {
      return deleted;
   }

   public void setDeleted(boolean deleted)
   {
      this.deleted = deleted;
   }

   public DBUser getCreatedById()
   {
      return createdById;
   }

   public void setCreatedById(DBUser createdById)
   {
      this.createdById = createdById;
   }

}