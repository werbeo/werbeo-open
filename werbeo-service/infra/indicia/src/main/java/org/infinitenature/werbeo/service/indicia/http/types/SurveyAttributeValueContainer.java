package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "survey_attribute_values")
@XmlAccessorType(XmlAccessType.FIELD)
public class SurveyAttributeValueContainer
{
   @XmlElement(name = "survey_attribute_value")
   private List<SurveyAttributeValue> surveyAttributeValues = new ArrayList<>();

   public List<SurveyAttributeValue> getSurveyAttributeValues()
   {
      return surveyAttributeValues;
   }

   public void setSurveyAttributeValues(
         List<SurveyAttributeValue> surveyAttributeValues)
   {
      this.surveyAttributeValues = surveyAttributeValues;
   }
}
