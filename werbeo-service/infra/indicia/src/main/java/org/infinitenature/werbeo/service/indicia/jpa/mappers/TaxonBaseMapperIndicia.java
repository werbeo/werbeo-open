package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;
import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxon;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigIndiciaJPA.class, uses = TaxaListMapperIndicia.class)
public interface TaxonBaseMapperIndicia
{
   @Mapping(target = "documentsAvailable", ignore = true)
   @Mapping(target = "name", source = "taxon")
   @Mapping(target = "group", source = "taxonGroup.title")
   @Mapping(target = "id", source = "taxaTaxonList.id")
   @Mapping(target = "authority", source = "authority")
   @Mapping(target = "allowDataEntry", source = "taxaTaxonList.allowDataEntry")
   @Mapping(target = "taxaList", source = "taxaTaxonList.taxaList")
   @Mapping(target = "preferred", source = "taxaTaxonList.preferred")
   public TaxonBase map(DBTaxon taxon);

   public List<TaxonBase> map(List<DBTaxon> taxa);

   public Set<TaxonBase> map(Set<DBTaxon> taxa);
}
