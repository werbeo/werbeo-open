package org.infinitenature.werbeo.service.indicia.http;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;

import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaType;
import org.infinitenature.werbeo.service.indicia.http.types.Location;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceComment;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceMedium;
import org.infinitenature.werbeo.service.indicia.http.types.Person;
import org.infinitenature.werbeo.service.indicia.http.types.SampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.http.types.SampleComment;
import org.infinitenature.werbeo.service.indicia.http.types.SampleSubmissionContainer;
import org.infinitenature.werbeo.service.indicia.http.types.SurveySubmissionContrainer;
import org.infinitenature.werbeo.service.indicia.http.types.UserIdentifier;

/**
 * Lowlevel-API to the indicia webservice
 *
 * @author dve
 *
 */
public interface IndiciaApi extends Serializable
{
   public static final String INDICIA_DATE_FORMAT = "yyyy-MM-dd";
   String KEY_WEBSITE_ID = "website_id";
   public static final String VAGUE_DATE_DAY = "D";
   public static final String VAGUE_DATE_DAYS = "DD";
   public static final String VAGUE_DATE_MONTH_IN_YEAR = "O";
   public static final String VAGUE_DATE_TO_YEAR = "-Y";

   public static final String VAGUE_DATE_YEAR = "Y";

   public void delete(IndiciaType type, int webSiteId, int indiciaUserId);

   public void delete(Person person, int webSiteId, int indiciaUserId);

   public void delete(IndiciaSurvey survey, int webSiteId, int indiciaUserId);

   public String getConnectionURL();

   public UserIdentifier identifyUser(Map<String, String> identifiers,
         String lastName, String firstName, Integer cmsUserId, int webSiteId);

   public int save(org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey survey,
         int webSiteId, int indiciaUserId);

   public int save(SampleSubmissionContainer sample, int webSiteId, int indiciaUserId);

   public int save(SurveySubmissionContrainer surveySubmissionContrainer,
         int webSiteId, int indiciaUserId);

   public int saveLocation(Location location, int webSiteId, int indiciaUserId);

   public int saveOccurence(IndiciaOccurrence occurence,
         Map<String, String> additionAtributes, int webSiteId, int indiciaUserId);

   int saveOccurrenceComment(OccurrenceComment occurrenceComment,
         int webSiteId, int indiciaUserId);

   public int savePerson(Person person,
         Map<String, String> additionalAttributes, int webSiteId, int indiciaUserId);

   public int saveSample(IndiciaSample sample,
         Map<String, String> additionAtributes,
         int webSiteId, int indiciaUserId);

   public int saveSampleAttributeValue(
         SampleAttributeValue sampleAttributeValue, int webSiteId, int indiciaUserId);

   int saveSampleComment(SampleComment sampleComment, int webSiteId, int indiciaUserId);

   public String uploadFile(InputStream inputStream, String fileName,
         int webSiteId);

   public int save(OccurrenceMedium occurrenceMedium, int webSiteId,
         int userId);

   public int save(OccurrenceAttributeValue submissionValue,
         int webSiteId,
         int userId);
}