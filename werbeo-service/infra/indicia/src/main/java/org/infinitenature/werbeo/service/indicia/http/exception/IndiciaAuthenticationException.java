package org.infinitenature.werbeo.service.indicia.http.exception;

public class IndiciaAuthenticationException extends IndiciaHttpException
{
   public IndiciaAuthenticationException(String message, String response)
   {
      super(message, 403, response);
   }
}
