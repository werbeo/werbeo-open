package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrence_comment")
@XmlAccessorType(XmlAccessType.FIELD)
public class OccurrenceComment extends IndiciaTypeImpl implements CommentType
{

   @XmlElement
   private String comment;
   @XmlElement(name = "occurrence_id")
   private int occurrenceId;

   public OccurrenceComment()
   {
      super();
   }

   public OccurrenceComment(String comment)
   {
      super();
      this.comment = comment;
   }

   public OccurrenceComment(String comment, int occurrenceId)
   {
      super();
      this.comment = comment;
      this.occurrenceId = occurrenceId;
   }

   @Override
   public String getModelName()
   {
      return "occurrence_comment";
   }

   @Override
   public int getTargetId()
   {
      return getOccurrenceId();
   }

   @Override
   public void setTargetId(int targetId)
   {
      setOccurrenceId(targetId);
   }

   @Override
   public String getComment()
   {
      return comment;
   }

   @Override
   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public int getOccurrenceId()
   {
      return occurrenceId;
   }

   public void setOccurrenceId(int occurrenceId)
   {
      this.occurrenceId = occurrenceId;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("OccurrenceComment@");
      builder.append(System.identityHashCode(this));
      builder.append(" [comment=");
      builder.append(comment);
      builder.append(", occurrenceId=");
      builder.append(occurrenceId);
      builder.append(", id=");
      builder.append(id);
      builder.append(", createdOn=");
      builder.append(createdOn);
      builder.append(", updatedOn=");
      builder.append(updatedOn);
      builder.append(", createdBy=");
      builder.append(createdBy);
      builder.append(", updatedBy=");
      builder.append(updatedBy);
      builder.append("]");
      return builder.toString();
   }
}