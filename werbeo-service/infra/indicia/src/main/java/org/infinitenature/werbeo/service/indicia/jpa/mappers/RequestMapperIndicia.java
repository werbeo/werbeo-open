package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.spring.data.domain.OffsetPageable;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public interface RequestMapperIndicia<S>
{
   @Mapping(source = "count", target = "limit")
   @Mapping(target = "sort", expression = "java(mapSort(offsetRequest))")
   OffsetPageable map(OffsetRequest<S> offsetRequest);

   default Sort mapSort(OffsetRequest<S> offsetRequest)
   {
      return Sort.by(map(offsetRequest.getSortOrder()),
            getSortFieldMapper().apply(offsetRequest.getSortField()));
   }

   default Direction map(SortOrder sortOrder)
   {
      if (sortOrder == SortOrder.ASC)
      {
         return Direction.ASC;
      } else
      {
         return Direction.DESC;
      }
   }

   Function<S, String[]> getSortFieldMapper();
}
