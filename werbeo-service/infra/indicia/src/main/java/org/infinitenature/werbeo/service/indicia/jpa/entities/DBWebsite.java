package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "websites")
public class DBWebsite extends Base
{
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 100)
   private String title;
   @Size(max = 2147483647)
   private String description;

   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 500)
   private String url;

   @JoinColumn(name = "default_survey_id", referencedColumnName = "id")
   @ManyToOne
   private DBSurvey defaultSurvey;
   
   @OneToMany(mappedBy = "website", fetch = FetchType.LAZY)
   private Set<DBTaxaList> taxonLists = new HashSet();

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   public DBSurvey getDefaultSurvey()
   {
      return defaultSurvey;
   }

   public void setDefaultSurvey(DBSurvey defaultSurvey)
   {
      this.defaultSurvey = defaultSurvey;
   }
}
