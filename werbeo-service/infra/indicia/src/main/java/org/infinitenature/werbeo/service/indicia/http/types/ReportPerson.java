package org.infinitenature.werbeo.service.indicia.http.types;

import org.infinitenature.vaguedate.VagueDate;

public class ReportPerson extends Person
{
   private String job;
   private VagueDate workingPeriod;

   public ReportPerson()
   {
      super();
   }

   public ReportPerson(int id, String firstName, String lastName,
         String initials, String emailAddress, String externalKey,
         long websiteId)
   {
      super(id, firstName, lastName, initials, emailAddress, externalKey,
            websiteId);
   }

   public ReportPerson(String firstName, String lastName, String externalKey)
   {
      super(firstName, lastName, externalKey);
   }

   public String getJob()
   {
      return job;
   }

   public void setJob(String job)
   {
      this.job = job;
   }

   public VagueDate getWorkingPeriod()
   {
      return workingPeriod;
   }

   public void setWorkingPeriod(VagueDate workingPeriod)
   {
      this.workingPeriod = workingPeriod;
   }
}