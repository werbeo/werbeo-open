package org.infinitenature.werbeo.service.indicia.http.types;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class AdditionalAttribute implements IndiciaAdditionalValue
{
   public enum Target
   {
      OCCURRENCE, SAMPLE, SURVEY
   }

   private int id;
   private String term;
   private String value;
   private int valueId;

   public AdditionalAttribute()
   {
      super();
   }

   public AdditionalAttribute(int id, String term, String value, int valueId)
   {
      super();
      this.id = id;
      this.term = term;
      this.value = value;
      this.valueId = valueId;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaAdditionalValue#getId()
    */
   @Override
   public int getAttributeId()
   {
      return id;
   }

   protected int getId()
   {
      return id;
   }
   public String getTerm()
   {
      return term;
   }

   /*
    * (non-Javadoc)
    *
    * @see org.indiciaConnector.types.IndiciaAdditionalValue#getValue()
    */
   @Override
   public String getValue()
   {
      return value;
   }

   public void setId(int id)
   {
      this.id = id;
   }

   public void setTerm(String term)
   {
      this.term = term;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   @Override
   public int getValueId()
   {
      return valueId;
   }

   public void setValueId(int valueId)
   {
      this.valueId = valueId;
   }

   @Override
   public String toString()
   {
      return AdditionalAttributeBeanUtil.doToString(this);
   }
}
