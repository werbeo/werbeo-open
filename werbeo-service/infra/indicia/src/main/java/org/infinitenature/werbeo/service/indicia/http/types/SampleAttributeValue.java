package org.infinitenature.werbeo.service.indicia.http.types;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "sample_attribute_value")
@XmlAccessorType(XmlAccessType.FIELD)
public class SampleAttributeValue extends IndiciaTypeImpl
      implements IndiciaAdditionalValue
{
   @Override
   public String getModelName()
   {
      return "sample_attribute_value";
   }

   @NotNull
   @XmlElement(name = "sample_id")
   private int sampleId;
   @NotNull
   @XmlElement(name = "sample_attribute_id")
   private int sampleAttributeId;
   @XmlElement(name = "data_type")
   private String dataType;
   @XmlElement(name = "caption")
   private String caption;
   @NotNull
   @XmlElement(name = "value")
   private String value;
   @XmlElement(name = "website_id")
   private int websiteId;

   public SampleAttributeValue(int sampleId, int sampleAttributeId,
         String value)
   {
      super();
      this.sampleId = sampleId;
      this.sampleAttributeId = sampleAttributeId;
      this.value = value;
   }

   public SampleAttributeValue()
   {
      super();
   }

   public int getSampleId()
   {
      return sampleId;
   }

   public void setSampleId(int sampleId)
   {
      this.sampleId = sampleId;
   }

   public int getSampleAttributeId()
   {
      return sampleAttributeId;
   }

   public void setSampleAttributeId(int sampleAttributeId)
   {
      this.sampleAttributeId = sampleAttributeId;
   }

   public String getDataType()
   {
      return dataType;
   }

   public void setDataType(String dataType)
   {
      this.dataType = dataType;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   @Override
   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   public int getWebsiteId()
   {
      return websiteId;
   }

   public void setWebsiteId(int websiteId)
   {
      this.websiteId = websiteId;
   }

   @Override
   public int getAttributeId()
   {
      return getSampleAttributeId();
   }

   @Override
   public int getValueId()
   {
      return getId();
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SampleAttributeValue [id=");
      builder.append(id);
      builder.append(", sampleId=");
      builder.append(sampleId);
      builder.append(", sampleAttributeId=");
      builder.append(sampleAttributeId);
      builder.append(", dataType=");
      builder.append(dataType);
      builder.append(", caption=");
      builder.append(caption);
      builder.append(", value=");
      builder.append(value);
      builder.append(", websiteId=");
      builder.append(websiteId);
      builder.append("]");
      return builder.toString();
   }
}
