package org.infinitenature.werbeo.service.indicia.http.exception;

public class EntityNotFoundException extends IndiciaHttpException
{
   public EntityNotFoundException(String message, String response)
   {
      super(message, 404, response);
   }
}