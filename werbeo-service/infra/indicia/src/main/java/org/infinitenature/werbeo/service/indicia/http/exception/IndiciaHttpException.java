package org.infinitenature.werbeo.service.indicia.http.exception;

public class IndiciaHttpException extends IndiciaException
{
   private final int httpStatusCode;
   private final String response;

   public IndiciaHttpException(String message, int httpStatusCode,
         String response)
   {
      super(message);
      this.httpStatusCode = httpStatusCode;
      this.response = response;
   }

   public int getHttpStatusCode()
   {
      return httpStatusCode;
   }

   public String getResponse()
   {
      return response;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("IndiciaHttpException@");
      builder.append(System.identityHashCode(this));
      builder.append(" [httpStatusCode=");
      builder.append(httpStatusCode);
      builder.append(", response=");
      builder.append(response);
      builder.append("]");
      return builder.toString();
   }
}