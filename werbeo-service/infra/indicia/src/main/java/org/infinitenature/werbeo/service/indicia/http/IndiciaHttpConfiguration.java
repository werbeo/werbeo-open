package org.infinitenature.werbeo.service.indicia.http;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "indicia")
public class IndiciaHttpConfiguration
{
   private Map<Integer, String> websitePasswords;

   public Map<Integer, String> getWebsitePasswords()
   {
      return websitePasswords;
   }

   public void setWebsitePasswords(Map<Integer, String> websitePasswords)
   {
      this.websitePasswords = websitePasswords;
   }

   public String getHost()
   {
      return host;
   }

   public void setHost(String host)
   {
      this.host = host;
   }

   public String getPath()
   {
      return path;
   }

   public void setPath(String path)
   {
      this.path = path;
   }

   private String host;
   private String path;
}
