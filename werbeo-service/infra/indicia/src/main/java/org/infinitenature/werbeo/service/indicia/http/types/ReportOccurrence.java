package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.Date;

public class ReportOccurrence extends IndiciaOccurrence
{
   private String recorderNames;
   private String determinerFirstName;
   private String determinerLastName;
   private Date sampleUpdatedOn;
   private User sampleUpdatedBy;
   private String sampleUpdatedByFirstName;
   private String sampleUpdatedByLastName;
   private int sampleUpdatedByPersonId;
   private String updatedByFirstName;
   private String updatedByLastName;
   private int updatedByPersonId;
   private int sampleParentSampleId;
   private int geoPrecision;
   private int precison;

   public String getSampleUpdatedByFirstName()
   {
      return sampleUpdatedByFirstName;
   }

   public void setSampleUpdatedByFirstName(String sampleUpdatedByFirstName)
   {
      this.sampleUpdatedByFirstName = sampleUpdatedByFirstName;
   }

   public String getSampleUpdatedByLastName()
   {
      return sampleUpdatedByLastName;
   }

   public void setSampleUpdatedByLastName(String sampleUpdatedByLastName)
   {
      this.sampleUpdatedByLastName = sampleUpdatedByLastName;
   }

   public int getSampleUpdatedByPersonId()
   {
      return sampleUpdatedByPersonId;
   }

   public void setSampleUpdatedByPersonId(int sampleUpdatedByPersionId)
   {
      this.sampleUpdatedByPersonId = sampleUpdatedByPersionId;
   }

   public String getUpdatedByFirstName()
   {
      return updatedByFirstName;
   }

   public void setUpdatedByFirstName(String updatedByFirstName)
   {
      this.updatedByFirstName = updatedByFirstName;
   }

   public String getUpdatedByLastName()
   {
      return updatedByLastName;
   }

   public void setUpdatedByLastName(String updatedByLastName)
   {
      this.updatedByLastName = updatedByLastName;
   }

   public int getUpdatedByPersonId()
   {
      return updatedByPersonId;
   }

   public void setUpdatedByPersonId(int updatedByPersionId)
   {
      this.updatedByPersonId = updatedByPersionId;
   }

   public String getDeterminerFirstName()
   {
      return determinerFirstName;
   }

   public void setDeterminerFirstName(String determinerFirstName)
   {
      this.determinerFirstName = determinerFirstName;
   }

   public String getDeterminerLastName()
   {
      return determinerLastName;
   }

   public void setDeterminerLastName(String determinerLastName)
   {
      this.determinerLastName = determinerLastName;
   }

   public String getRecorderNames()
   {
      return recorderNames;
   }

   public void setRecorderNames(String recorderNames)
   {
      this.recorderNames = recorderNames;
   }

   public Date getSampleUpdatedOn()
   {
      return sampleUpdatedOn;
   }

   public void setSampleUpdatedOn(Date sampleUpdatedOn)
   {
      this.sampleUpdatedOn = sampleUpdatedOn;
   }

   public User getSampleUpdatedBy()
   {
      return sampleUpdatedBy;
   }

   public void setSampleUpdatedBy(User sampleUpdatedBy)
   {
      this.sampleUpdatedBy = sampleUpdatedBy;
   }

   public int getSampleParentSampleId()
   {
      return sampleParentSampleId;
   }

   public void setSampleParentSampleId(int sampleParentSampleId)
   {
      this.sampleParentSampleId = sampleParentSampleId;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("ReportOccurrence [recorderNames=");
      builder.append(recorderNames);
      builder.append(", determinerFirstName=");
      builder.append(determinerFirstName);
      builder.append(", determinerLastName=");
      builder.append(determinerLastName);
      builder.append(", sampleUpdatedOn=");
      builder.append(sampleUpdatedOn);
      builder.append(", sampleUpdatedBy=");
      builder.append(sampleUpdatedBy);
      builder.append(", sampleUpdatedByFirstName=");
      builder.append(sampleUpdatedByFirstName);
      builder.append(", sampleUpdatedByLastName=");
      builder.append(sampleUpdatedByLastName);
      builder.append(", sampleUpdatedByPersonId=");
      builder.append(sampleUpdatedByPersonId);
      builder.append(", updatedByFirstName=");
      builder.append(updatedByFirstName);
      builder.append(", updatedByLastName=");
      builder.append(updatedByLastName);
      builder.append(", updatedByPersonId=");
      builder.append(updatedByPersonId);
      builder.append(", toString()=");
      builder.append(super.toString());
      builder.append("]");
      return builder.toString();
   }

   public int getGeoPrecision()
   {
      return geoPrecision;
   }

   public void setGeoPrecision(int geoPrecision)
   {
      this.geoPrecision = geoPrecision;
   }

   public int getPrecison()
   {
      return precison;
   }

   public void setPrecison(int precison)
   {
      this.precison = precison;
   }
}