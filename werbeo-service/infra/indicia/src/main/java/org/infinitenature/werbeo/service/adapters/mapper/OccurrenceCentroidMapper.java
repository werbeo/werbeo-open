package org.infinitenature.werbeo.service.adapters.mapper;

import org.infinitenature.werbeo.service.adapters.entities.OccurrenceGeometry;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class OccurrenceCentroidMapper
{
   public List<OccurrenceCentroid> mapGeometryToCentroidList(List<OccurrenceGeometry> content)
   {
      List<OccurrenceCentroid> occurrenceCentroidList = new ArrayList<>();
      for(OccurrenceGeometry occurrenceGeometry : content)
      {
         occurrenceCentroidList.add(mapGeometryToCentroid(occurrenceGeometry));
      }
      return occurrenceCentroidList;
   }


   @Mapping(target = "point", expression = "java(occurrenceGeometry.getGeometry().getCentroid())")
   public abstract OccurrenceCentroid mapGeometryToCentroid (OccurrenceGeometry occurrenceGeometry);
}
