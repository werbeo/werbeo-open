package org.infinitenature.werbeo.service.adapters;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.adapters.config.AdditionalAttributeConfigFactory;
import org.infinitenature.werbeo.service.adapters.config.TermConfig;
import org.infinitenature.werbeo.service.adapters.config.TermlistConfigFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.infra.commands.SampleCommands;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.http.IndiciaApi;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceSubmissionContainer;
import org.infinitenature.werbeo.service.indicia.http.types.SampleSubmissionContainer;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBOccurrenceAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBSampleAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceAttributeValueRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.SampleAttributeValueRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampleCommandsAdapter implements SampleCommands
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SampleCommandsAdapter.class);

   @Autowired
   private IndiciaApi indiciaApi;
   @Autowired
   private SampleAttributeValueRepository sampleAttributeValuesRepository;
   @Autowired
   private OccurrenceAttributeValueRepository occurrenceAttributeValueRepository;
   @Autowired
   private AdditionalAttributeConfigFactory configFactory;
   @Autowired
   private SampleSubmissionMapper sampleSubmissionMapper;
   @Autowired
   private IdQueries idQueries;
   @Autowired
   private IndiciaUserQueries indiciaUserQueries;
   @Autowired
   private TermlistConfigFactory termlistConfigFactory;

   @Override
   public UUID saveOrUpdate(Sample sample, Context context)
   {
      UUID sampleUUID = sample.getId();
      LOGGER.info("Going to save sample in indicia: {}", sampleUUID);

      int sampleId = idQueries.getIndiciaSampleId(sampleUUID);

      Map<String, String> existingSampleAttributes = sampleId != 0
            ? loadAdditionalSampleAttributeValues(sampleId)
            : new HashMap<>();
      Map<String, String> newSampleAttributes = createAttributes(sample);

      IndiciaSample indiciaSample = sampleSubmissionMapper.map(sample, context);
      indiciaSample.setId(sampleId);

      Set<Integer> existingOcurrenceIds = idQueries
            .getIndiciaOccurrenceIds(sampleId);
      Set<OccurrenceSubmissionContainer> osc = createOccurrenceSubmissionContainers(
            sample.getOccurrences(), context, existingOcurrenceIds);

      SampleSubmissionContainer ssc = new SampleSubmissionContainer(
            indiciaSample,
            CustomAttributeMerger.merge(newSampleAttributes,
                  existingSampleAttributes),
            new HashSet<>(), osc, new HashSet<>());

      int userId;
      if (sample.getCreatedBy() != null
            && sample.getCreatedBy().getId() == null)
      {
         userId = indiciaUserQueries.getUserId(sample.getCreatedBy().getLogin(),
               sample.getCreatedBy().getPerson().getLastName(),
               sample.getCreatedBy().getPerson().getFirstName(), context);

      } else
      {
         userId = indiciaUserQueries.getUserId(context);
      }
      int indiciaSampleId = indiciaApi.save(ssc, context.getPortal().getId(),
            userId);
      LOGGER.info("Create sample with indicia id {}", indiciaSampleId);
      return sampleUUID;

   }

   @Override
   public int addComment(UUID sampleUUID, SampleComment comment,
         Context context)
   {
      int sampleId = idQueries.getIndiciaSampleId(sampleUUID);
      org.infinitenature.werbeo.service.indicia.http.types.SampleComment indiciaSamplecomment = new org.infinitenature.werbeo.service.indicia.http.types.SampleComment(
            comment.getComment(), sampleId);
      return indiciaApi.saveSampleComment(indiciaSamplecomment,
            context.getPortal().getId(), indiciaUserQueries.getUserId(context));
   }



   private Set<OccurrenceSubmissionContainer> createOccurrenceSubmissionContainers(
         List<Occurrence> occurrences, Context context,
         Set<Integer> existingOccurrenceIds)
   {
      Map<UUID, Integer> occurrenceIds = getIndiciaOccurrenceIds(occurrences
            .stream().map(Occurrence::getId).collect(Collectors.toSet()));
      Set<OccurrenceSubmissionContainer> ocs = new HashSet<>();

      for (Occurrence occurrence : occurrences)
      {
         IndiciaOccurrence indiciaOccurrence = sampleSubmissionMapper
               .map(occurrence, context);
         Integer indiciaId = occurrenceIds.getOrDefault(occurrence.getId(), 0);
         existingOccurrenceIds.remove(indiciaId);
         Map<String, String> existingOccurrenceAttributes = indiciaId != 0
               ? loadAdditionalOccurrenceAttributeValues(indiciaId)
               : new HashMap<>();
         Map<String, String> newOccurrenceAttributes = createAttributes(
               occurrence);

         indiciaOccurrence.setId(indiciaId);
         indiciaOccurrence.setUpdatedOn(new Date());
         ocs.add(new OccurrenceSubmissionContainer(indiciaOccurrence,
               CustomAttributeMerger.merge(newOccurrenceAttributes,
                     existingOccurrenceAttributes),
               new HashSet<>(), Collections.emptySet()));
      }

      for (int occurrenceIdToDelete : existingOccurrenceIds)
      {
         IndiciaOccurrence indiciaOccurrence = new IndiciaOccurrence(
               occurrenceIdToDelete);
         indiciaOccurrence.setDeleted(true);
         ocs.add(new OccurrenceSubmissionContainer(indiciaOccurrence,
               new HashMap<>(), new HashSet<>(), new HashSet<>()));
      }
      return ocs;
   }

   private Map<String, String> createAttributes(Sample sample)
   {
      Map<String, String> attributes = new HashMap<>();
      AdditionalSampleAttributeConfig config = configFactory.getSampleConfig();
      attributes.put(String.valueOf(config.getUuid()),
            sample.getId().toString());
      attributes.put(String.valueOf(config.getPrecision()),
            String.valueOf(sample.getLocality().getPrecision()));
      attributes.put(String.valueOf(config.getMtb()),
            sample.getLocality().getPosition().getMtb().getMtb());

      if(StringUtils.isNotBlank(sample.getLocality().getLocality()))
      {
         attributes.put(String.valueOf(config.getLocality()),
            sample.getLocality().getLocality());
      }
      if(StringUtils.isNotBlank(sample.getLocality().getLocationComment()))
      {
         attributes.put(String.valueOf(config.getLocationDescription()),
               sample.getLocality().getLocationComment());
      }
      // TODO add other attributes
      return attributes;
   }

   private Map<String, String> createAttributes(Occurrence occurrence)
   {
      TermConfig termConfig = termlistConfigFactory.getTermConfig();
      Map<String, String> attributes = new HashMap<>();
      AdditionalOccurrenceAttributeConfig config = configFactory
            .getOccurrenceConfig();
      attributes.put(String.valueOf(config.getUuid()),
            occurrence.getId().toString());

      createFloristicAttributes(occurrence, termConfig, attributes, config);

      createFaunisticAttributes(occurrence, termConfig, attributes, config);

      if(StringUtils.isNotBlank(occurrence.getObservers()))
      {
         attributes.put(String.valueOf(config.getObservers()),
               occurrence.getObservers());
      }
      if (StringUtils.isNotBlank(occurrence.getDeterminationComment()))
      {
         attributes.put(String.valueOf(config.getDeterminationComment()),
               occurrence.getDeterminationComment());
      }
      if (occurrence.getSex() != null)
      {
         attributes.put(String.valueOf(config.getSex()),
               String.valueOf(termConfig.getId(occurrence.getSex())));
      }
      if(StringUtils.isNotBlank(occurrence.getHabitat()))
      {
         attributes.put(String.valueOf(config.getHabitat()),
                occurrence.getHabitat());
      }
      if(StringUtils.isNotBlank(occurrence.getCiteId()))
      {
         attributes.put(String.valueOf(config.getCiteId()),
                occurrence.getCiteId());
      }
      if(StringUtils.isNotBlank(occurrence.getCiteComment()))
      {
         attributes.put(String.valueOf(config.getCiteComment()),
                occurrence.getCiteComment());
      }
      if (occurrence.getNumericAmountAccuracy() != null)
      {
         attributes.put(String.valueOf(config.getNumericAmountAccuracy()),
               String.valueOf(
                     termConfig.getId(occurrence.getNumericAmountAccuracy())));
      }
      if (StringUtils.isNotBlank(occurrence.getRemark()))
      {
         attributes.put(String.valueOf(config.getRemark()),
               occurrence.getRemark());
      }

      return attributes;
   }

   private void createFaunisticAttributes(Occurrence occurrence,
         TermConfig termConfig, Map<String, String> attributes,
         AdditionalOccurrenceAttributeConfig config)
   {
      if (occurrence.getLifeStage() != null)
      {
         attributes.put(String.valueOf(config.getLifeStage()),
               String.valueOf(termConfig.getId(occurrence.getLifeStage())));
      }
      if (occurrence.getMakropter() != null)
      {
         attributes.put(String.valueOf(config.getMakropter()),
               String.valueOf(termConfig.getId(occurrence.getMakropter())));
      }
      if (occurrence.getReproduction() != null)
      {
         attributes.put(String.valueOf(config.getReproduction()),
               String.valueOf(termConfig.getId(occurrence.getReproduction())));
      }
      if (occurrence.getTimeOfDay() != null)
      {
         attributes.put(String.valueOf(config.getTimeOfDay()),
               occurrence.getTimeOfDay());
      }
      if (occurrence.getNumericAmount() != null)
      {
         attributes.put(String.valueOf(config.getNumericAmount()),
               occurrence.getNumericAmount().toString());
      }
   }

   private void createFloristicAttributes(Occurrence occurrence,
         TermConfig termConfig, Map<String, String> attributes,
         AdditionalOccurrenceAttributeConfig config)
   {
      if(occurrence.getSettlementStatus() != null)
      {
         attributes.put(String.valueOf(config.getStatus()),
               String.valueOf(termConfig.getId(occurrence.getSettlementStatus())));
      }
      if (occurrence.getCoveredArea() != null)
      {
         attributes.put(String.valueOf(config.getCoveredArea()),
               String.valueOf(termConfig.getId(occurrence.getCoveredArea())));
      }
      if(occurrence.getVitality() != null)
      {
         attributes.put(String.valueOf(config.getVitality()),
               String.valueOf(termConfig.getId(occurrence.getVitality())));
      }
      if (occurrence.getSettlementStatusFukarek() != null)
      {
         attributes.put(String.valueOf(config.getOccurrenceStatus()),
               String.valueOf(termConfig
                     .getId(occurrence.getSettlementStatusFukarek())));
      }
      if(occurrence.getBloomingSprouts() != null)
      {
         attributes.put(String.valueOf(config.getBloomingSprouts()),
               String.valueOf(termConfig.getId(occurrence.getBloomingSprouts())));
      }
      if(occurrence.getHerbarium() != null)
      {
         attributes.put(String.valueOf(config.getHerbariumCode()), occurrence.getHerbarium().getCode());
         attributes.put(String.valueOf(config.getHerbary()), occurrence.getHerbarium().getHerbary());
      }
      if (occurrence.getAmount() != null)
      {
         attributes.put(String.valueOf(config.getAmount()),
               String.valueOf(termConfig.getId(occurrence.getAmount())));
      }
      if(occurrence.getQuantity() != null)
      {
         attributes.put(String.valueOf(config.getQuantity()),
               String.valueOf(termConfig.getId(occurrence.getQuantity())));
      }
   }

   private Map<String, String> loadAdditionalSampleAttributeValues(int sampleId)
   {
      QDBSampleAttributeValue qdbSampleAttributeValue = QDBSampleAttributeValue.dBSampleAttributeValue;
      Iterable<DBSampleAttributeValue> jpaValues = sampleAttributeValuesRepository
            .findAll(qdbSampleAttributeValue.sample.id.eq(sampleId)
                  .and(qdbSampleAttributeValue.deleted.isFalse()));

      Map<String, String> additionalValues = new HashMap<>();
      jpaValues.forEach(jpaValue ->
      {
         String key = jpaValue.getSampleAttribute().getId() + ":"
               + jpaValue.getId();
         String value = AttributeUtils.getValue(jpaValue);
         // TODO remove null check after implementing all types of values
         if (value != null)
         {
            additionalValues.put(key, value);
         }
      });
      return additionalValues;
   }

   private Map<String, String> loadAdditionalOccurrenceAttributeValues(
         int occurrenceId)
   {
      QDBOccurrenceAttributeValue qdbOccurrenceAttributeValue = QDBOccurrenceAttributeValue.dBOccurrenceAttributeValue;
      Iterable<DBOccurrenceAttributeValue> jpaValues = occurrenceAttributeValueRepository
            .findAll(qdbOccurrenceAttributeValue.occurrence.id.eq(occurrenceId)
                  .and(qdbOccurrenceAttributeValue.deleted.isFalse()));

      Map<String, String> additionalValues = new HashMap<>();
      jpaValues.forEach(jpaValue ->
      {
         String key = jpaValue.getOccurrenceAttribute().getId() + ":"
               + jpaValue.getId();
         String value = AttributeUtils.getValue(jpaValue);
         // TODO remove null check after implementing all types of values
         if (value != null)
         {
            additionalValues.put(key, value);
         }
      });
      return additionalValues;
   }

   private Map<UUID, Integer> getIndiciaOccurrenceIds(Set<UUID> occurrenceUUIDs)
   {
      Map<UUID, Integer> occurrenceIds = new HashMap<>();
      for (UUID occurrenceUUID : occurrenceUUIDs)
      {
         occurrenceIds.put(occurrenceUUID,
               idQueries.getIndiciaOccurrenceId(occurrenceUUID));
      }
      return occurrenceIds;
   }

   @Override
   public void delete(Sample sample, Context context)
   {
      int sampleId = idQueries.getIndiciaSampleId(sample.getId());
      org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample indiciaSample = new org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample(
            sampleId);
      indiciaApi.delete(indiciaSample, context.getPortal().getId(),
            indiciaUserQueries.getUserId(context));
   }

}
