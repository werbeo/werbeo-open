package org.infinitenature.werbeo.service.indicia.http.http.crypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaException;

public class PhpWrapper
{
   static final String HEXES = "0123456789abcdef";
   static MessageDigest sha1;

   private static String getHex(byte[] raw)
   {
      if (raw == null)
      {
         return null;
      }
      final StringBuilder hex = new StringBuilder(2 * raw.length);
      for (final byte b : raw)
      {
         hex.append(HEXES.charAt((b & 0xF0) >> 4))
               .append(HEXES.charAt((b & 0x0F)));
      }
      return hex.toString();
   }

   public static String sha1(String s)
   {
      if (sha1 == null)
      {
         try
         {
            sha1 = MessageDigest.getInstance("SHA1");
         } catch (NoSuchAlgorithmException e)
         {
            throw new IndiciaException(e);
         }
      }
      return getHex(sha1.digest(s.getBytes()));
   }

}
