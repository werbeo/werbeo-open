package org.infinitenature.werbeo.service.indicia.http.types;

public enum Mode
{
   xml, json, cvs
}
