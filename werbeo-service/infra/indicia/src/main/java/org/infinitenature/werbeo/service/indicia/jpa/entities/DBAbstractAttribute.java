package org.infinitenature.werbeo.service.indicia.jpa.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class DBAbstractAttribute extends Base
{

   @Size(max = 50)
   private String caption;
   @Column(name = "data_type")
   private Character dataType;
   @Size(max = 2147483647)
   @Column(name = "validation_rules")
   private String validationRules;
   @Column(name = "multi_value")
   private Boolean multiValue;
   @Basic(optional = false)
   @NotNull
   @Column(name = "public")
   protected boolean public1;
   @Size(max = 30)
   @Column(name = "system_function")
   private String systemFunction;
   @Size(max = 2147483647)
   private String description;

   public DBAbstractAttribute()
   {
      super();
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   public Character getDataType()
   {
      return dataType;
   }

   public void setDataType(Character dataType)
   {
      this.dataType = dataType;
   }

   public String getValidationRules()
   {
      return validationRules;
   }

   public void setValidationRules(String validationRules)
   {
      this.validationRules = validationRules;
   }

   public Boolean getMultiValue()
   {
      return multiValue;
   }

   public void setMultiValue(Boolean multiValue)
   {
      this.multiValue = multiValue;
   }

   public boolean getPublic1()
   {
      return public1;
   }

   public void setPublic1(boolean public1)
   {
      this.public1 = public1;
   }

   public String getSystemFunction()
   {
      return systemFunction;
   }

   public void setSystemFunction(String systemFunction)
   {
      this.systemFunction = systemFunction;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

}