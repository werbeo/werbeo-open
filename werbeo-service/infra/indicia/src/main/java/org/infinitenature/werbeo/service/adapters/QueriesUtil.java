package org.infinitenature.werbeo.service.adapters;

import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBWebsite;
import org.infinitenature.werbeo.service.indicia.jpa.entities.index.QWebsiteAgreementsIndex;

import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;

public class QueriesUtil
{
   public static JPQLQuery<DBWebsite> createWebsiteAgreementQuery(
         Context context)
   {
      Integer portalId = context.getPortal().getId();

      return createWebsiteAgreementQuery(portalId);
   }

   public static JPQLQuery<DBWebsite> createWebsiteAgreementQuery(
         Integer portalId)
   {
      QWebsiteAgreementsIndex websiteAgreementsIndex = QWebsiteAgreementsIndex.websiteAgreementsIndex;

      return JPAExpressions.select(websiteAgreementsIndex.fromWebsite)
            .from(websiteAgreementsIndex)
            .where(websiteAgreementsIndex.toWebsite.id.eq(portalId)
                  .and(websiteAgreementsIndex.provideForDataFlow.isTrue()));
   }
}
