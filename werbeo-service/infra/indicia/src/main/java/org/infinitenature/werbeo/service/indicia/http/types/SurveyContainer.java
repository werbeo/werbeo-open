package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "surveys")
@XmlAccessorType(XmlAccessType.FIELD)
public class SurveyContainer
{
   @XmlElement(name = "survey")
   private List<IndiciaSurvey> surveys = new ArrayList<>();

   public List<IndiciaSurvey> getSurveys()
   {
      return surveys;
   }

   public void setSurveys(List<IndiciaSurvey> samples)
   {
      this.surveys = samples;
   }

}