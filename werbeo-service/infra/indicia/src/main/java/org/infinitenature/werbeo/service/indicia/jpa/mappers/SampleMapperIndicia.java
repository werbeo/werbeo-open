package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSample;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigIndiciaJPA.class, uses = {
      SurveyMapperIndicia.class, OccurrenceMapperIndicia.class,
      PersonMapperIndicia.class })
public abstract class SampleMapperIndicia
{
   @Autowired
   private SampleMappingHelper mappingHelper;

   @Mapping(target = "allowedOperations", ignore = true) // filled later
   @Mapping(target = "sampleMethod", ignore = true)
   @Mapping(target = "locality.precision", ignore = true) // handled by
                                                          // additional
                                                          // attributes
   @Mapping(target = "locality.position", ignore = true)
   @Mapping(target = "date", ignore = true)
   @Mapping(target = "recorder", source = "recorderNames")
   @Mapping(target = "id", ignore = true) // handled by additional attributes
   public abstract Sample mapFull(DBSample jpaSample,
         @org.mapstruct.Context Context context);

   public abstract List<Sample> mapFull(List<DBSample> content,
         @org.mapstruct.Context Context context);


   @AfterMapping
   public void setPosion(@MappingTarget Sample sample, DBSample jpaSample)
   {
      mappingHelper.setPosition(sample, jpaSample);
      mappingHelper.mapDate(sample, jpaSample);
      mappingHelper.mapAdditionalAttributes(sample, jpaSample);
      mappingHelper.mapSampleMethod(sample, jpaSample);
   }

   public Person toPerson(String recorderNames,
         @org.mapstruct.Context Context context)
   {
      return mappingHelper.toPerson(recorderNames, context);
   }
}
