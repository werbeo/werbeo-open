package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SurveyRequestMapperIndicia
      extends RequestMapperIndicia<SurveySortField>
{
   @Override
   default Function<SurveySortField, String[]> getSortFieldMapper()
   {
      return surveySortField ->
      {
         switch (surveySortField)
         {
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
