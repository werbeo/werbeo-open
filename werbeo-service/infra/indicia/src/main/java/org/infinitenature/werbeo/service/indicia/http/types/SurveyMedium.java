package org.infinitenature.werbeo.service.indicia.http.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "survey_medium")
@XmlAccessorType(XmlAccessType.FIELD)
public class SurveyMedium extends IndiciaTypeImpl implements MediumType
{
   @XmlElement(name = "survey_id")
   private int surveyId;
   @XmlElement
   private String path;
   @XmlElement
   private String caption;
   @XmlElement(name = "media_type")
   private MediaTypeAsSubelement mediaType;

   public SurveyMedium()
   {
      super();
   }

   public SurveyMedium(String path, String caption)
   {
      super();
      this.path = path;
      this.caption = caption;
   }

   public int getSurveyId()
   {
      return surveyId;
   }

   public void setSurveyId(int sampleId)
   {
      this.surveyId = sampleId;
   }

   @Override
   public String getPath()
   {
      return path;
   }

   public void setPath(String path)
   {
      this.path = path;
   }

   @Override
   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   @Override
   public MediaTypeAsSubelement getMediaType()
   {
      return mediaType;
   }

   public void setMediaType(MediaTypeAsSubelement mediaType)
   {
      this.mediaType = mediaType;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SurveyMedium@");
      builder.append(System.identityHashCode(this));
      builder.append(" [surveyId=");
      builder.append(surveyId);
      builder.append(", ");
      if (path != null)
      {
         builder.append("path=");
         builder.append(path);
         builder.append(", ");
      }
      if (caption != null)
      {
         builder.append("caption=");
         builder.append(caption);
         builder.append(", ");
      }
      if (mediaType != null)
      {
         builder.append("mediaType=");
         builder.append(mediaType);
      }
      builder.append("]");
      return builder.toString();
   }

   @Override
   public String getModelName()
   {
      return "survey_medium";
   }

   @Override
   public int getTargetId()
   {
      return getSurveyId();
   }

}