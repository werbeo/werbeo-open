package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "survey_media")
@XmlAccessorType(XmlAccessType.FIELD)
public class SurveyMediaContainer
{
   @XmlElement(name = "survey_medium")
   private List<SurveyMedium> surveyMedia = new ArrayList<>();

   public List<SurveyMedium> getSurveyMedia()
   {
      return surveyMedia;
   }

   public void setSurveyMedia(List<SurveyMedium> surveyMedia)
   {
      this.surveyMedia = surveyMedia;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("SurveyMediaContainer@");
      builder.append(System.identityHashCode(this));
      builder.append(" [");
      if (surveyMedia != null)
      {
         builder.append("surveyMedia=");
         builder.append(surveyMedia);
      }
      builder.append("]");
      return builder.toString();
   }
}