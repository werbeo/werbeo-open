package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBPersonAttributeValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PersonAttributeValuesRepository
      extends JpaRepository<DBPersonAttributeValue, Integer>,
      QuerydslPredicateExecutor<DBPersonAttributeValue>
{

}
