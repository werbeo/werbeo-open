package org.infinitenature.werbeo.service.indicia.http.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "occurrence_attributes")
@XmlAccessorType(XmlAccessType.FIELD)
public class OccurrenceAttributeContainer
{
   @XmlElement(name = "occurrence_attribute")
   private List<OccurrenceAttribute> occurrenceAttributes = new ArrayList<>();

   public List<OccurrenceAttribute> getOccurrenceAttributes()
   {
      return occurrenceAttributes;
   }

   public void setOccurrenceAttributes(
         List<OccurrenceAttribute> occurrenceAttributes)
   {
      this.occurrenceAttributes = occurrenceAttributes;
   }
}