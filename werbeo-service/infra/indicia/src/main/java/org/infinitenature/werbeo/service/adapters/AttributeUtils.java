package org.infinitenature.werbeo.service.adapters;

import org.infinitenature.werbeo.service.indicia.jpa.entities.DBAbstractAttributeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AttributeUtils
{
   private static final Logger LOGGER = LoggerFactory
     .getLogger(AttributeUtils.class);

   private AttributeUtils()
   {
      throw new IllegalAccessError("Utility class");
   }

   public static String getValue(DBAbstractAttributeValue jpaValue)
   {
      Character type = jpaValue.getAttribute().getDataType();

      switch (type)
      {
      case 'T':
         return jpaValue.getTextValue();
      case 'I':
         return String.valueOf(jpaValue.getIntValue());
      case 'F':
         return String.valueOf(jpaValue.getFloatValue());
      case 'B':
         return jpaValue.getIntValue() == 1 ?
           String.valueOf(true) :
           String.valueOf(false);
      case 'D':
         // TODO implement
         LOGGER.error("Date attribute not implemented yet, return null");
         return null;
      case 'V':
         // TODO implement
         LOGGER.error("VagueDate attribute not implemented yet, return null");
         return null;
      case 'L':
         return String.valueOf(jpaValue.getIntValue());
      default:
         throw new IllegalArgumentException("Don't know datatype " + type);
      }
   }
}
