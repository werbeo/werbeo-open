package org.infinitenature.werbeo.service.indicia.http.http;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.infinitenature.werbeo.service.indicia.http.IndiciaHttpConfiguration;
import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaAuthenticationException;
import org.infinitenature.werbeo.service.indicia.http.exception.IndiciaException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import external.org.json.JSONObject;

/**
 * HTTP-Handling for communication with indicia. This implementation needs
 * proper http status codes to be returned from indicia.
 *
 * @author dve
 *
 */
@SuppressWarnings("serial")
@Component
public class IndiciaHttpConnector implements Serializable
{
   private static final String PARAM_NAME_NONCE = "nonce";

   private static final String PARAM_NAME_AUTH_TOKEN = "auth_token";

   private static final String FAILURE_DURING_REQUEST = "Failure during request";

   private static final String FAILURE_CREATING_URI = "Failure creating URI";

   private static final org.slf4j.Logger LOGGER = LoggerFactory
         .getLogger(IndiciaHttpConnector.class);

   private final String indiciaPath;
   private final String host;
   private final HttpClient httpClient;
   private final Map<Integer, String> websitePasswords;

   private final TokenStore<ReadAuthToken> readAuthTokens = new TokenStore<>();

   public IndiciaHttpConnector(
         @Autowired IndiciaHttpConfiguration indiciaHttpConfiguration)
   {
      this.host = indiciaHttpConfiguration.getHost();
      this.indiciaPath = indiciaHttpConfiguration.getPath();
      this.websitePasswords = indiciaHttpConfiguration.getWebsitePasswords();

      SchemeRegistry schemeRegistry = new SchemeRegistry();
      schemeRegistry.register(
            new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
      ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(
            schemeRegistry);
      cm.setMaxTotal(80);
      this.httpClient = new DefaultHttpClient(cm);

      // LOGGER.info("Created: " + this.toString());
   }

   /**
    * Performs a GET request to a given service. If the request fails because of
    * an authentication failure a second try is performed with a new
    * authentication token.
    *
    * @param service
    * @param params
    * @return
    */
   public String doHttpGetRequest(String service, List<NameValuePair> params,
         int websiteId)
   {
      ReadAuthToken readAuthToken = getReadAuthToken(websiteId, true);
      try
      {
         List<NameValuePair> myParams = addToken(params, readAuthToken);
         return innerDoHttpGetRequest(service, myParams);
      } catch (IndiciaAuthenticationException e)
      {
         LOGGER.debug("Failure in request, doing it a second time.", e);
         readAuthTokens.remove(websiteId, readAuthToken);
         readAuthToken = getReadAuthToken(websiteId, false);
         List<NameValuePair> myParams = addToken(params, readAuthToken);
         try
         {
            return innerDoHttpGetRequest(service, myParams);
         } catch (IndiciaAuthenticationException e2)
         {
            throw new IndiciaException("Authentication failed twice", e2);
         }
      }
   }

   private List<NameValuePair> addToken(List<NameValuePair> params,
         ReadAuthToken readAuthToken)
   {
      List<NameValuePair> myParams = new ArrayList<>(params);
      myParams.add(
            new BasicNameValuePair(PARAM_NAME_NONCE, readAuthToken.getNonce()));
      myParams.add(new BasicNameValuePair(PARAM_NAME_AUTH_TOKEN,
            readAuthToken.getToken()));
      return myParams;
   }

   private String innerDoHttpGetRequest(String service,
         List<NameValuePair> myParams)
   {
      try
      {
         URI uri;
         uri = URIUtils.createURI("http", host, -1, indiciaPath + service,
               URLEncodedUtils.format(myParams, "UTF-8"), null);
         LOGGER.debug(uri.toString());
         HttpUriRequest httpget = new HttpGet(uri);
         return performHTTPRequest(httpget);
      }

      catch (URISyntaxException | ClientProtocolException e)
      {
         throw new IndiciaException(FAILURE_CREATING_URI, e);
      } catch (IOException e)
      {
         throw new IndiciaException(FAILURE_DURING_REQUEST, e);
      }
   }

   private String performHTTPRequest(HttpUriRequest httpUriRequest)
         throws IOException
   {
      return httpClient.execute(httpUriRequest, new IndiciaResponseHandler());
   }

   protected String doHttpPostRequest(String service,
         List<NameValuePair> params)
   {
      try
      {
         UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(params,
               "UTF-8");
         URI uri = URIUtils.createURI("http", host, -1, indiciaPath + service,
               null, null);
         HttpPost httpPost = new HttpPost(uri);
         httpPost.setEntity(formEntity);
         return performHTTPRequest(httpPost);
      } catch (URISyntaxException e)
      {
         throw new IndiciaException(FAILURE_CREATING_URI, e);
      } catch (IOException e)
      {
         throw new IndiciaException(FAILURE_DURING_REQUEST, e);
      }
   }

   protected ReadAuthToken getReadAuthToken(int websiteId, boolean fromCache)
   {
      ReadAuthToken readAuthToken = null;
      if (fromCache)
      {
         readAuthToken = readAuthTokens.get(websiteId);
      }
      if (readAuthToken == null)
      {
         readAuthToken = new ReadAuthToken(getReadNonce(websiteId),
               websitePasswords.get(websiteId));
         readAuthTokens.put(websiteId, readAuthToken);
         LOGGER.info("Requesting ReadNonce in thread:"
               + java.lang.Thread.currentThread().getName());
      }
      return readAuthToken;
   }

   protected ReadNonce getReadNonce(int websiteId)
   {
      try
      {
         List<NameValuePair> params = new ArrayList<>();
         params.add(
               new BasicNameValuePair("website_id", String.valueOf(websiteId)));
         String result = doHttpPostRequest(
               "/index.php/services/security/get_read_nonce", params);
         return new ReadNonce(result);
      } catch (Exception e)
      {
         LOGGER.error("Error getting readNonce", e);
         throw new IndiciaException("Error getting readNonce", e);
      }
   }

   protected ReadWriteNonce getReadWriteNonce(int websiteId)
   {
      try
      {
         List<NameValuePair> params = new ArrayList<>();
         params.add(
               new BasicNameValuePair("website_id", String.valueOf(websiteId)));
         String s = doHttpPostRequest(
               "/index.php/services/security/get_read_write_nonces", params);

         JSONObject jsonRespnse = new JSONObject(s);
         return new ReadWriteNonce(jsonRespnse.getString("read"),
               jsonRespnse.getString("write"));
      } catch (Exception e)
      {
         LOGGER.error("Error getting readWriteNonce", e);
         throw new IndiciaException("Error getting readWriteNonce", e);
      }
   }

   protected AuthToken getWriteAuthToken(int websiteId)
   {
      return new WriteAuthToken(getReadWriteNonce(websiteId),
            websitePasswords.get(websiteId));
   }

   /**
    * Performs a read-only POST request. If the request fails because of an
    * authentication failure a second try is done with a new authentication
    * token.
    *
    * @param service
    * @param params
    * @return
    */
   public String doHttpPostRequestRO(String service, List<NameValuePair> params,
         int websiteId)
   {
      ReadAuthToken readAuthToken = getReadAuthToken(websiteId, true);
      try
      {
         List<NameValuePair> myParams = addToken(params, readAuthToken);
         return doHttpPostRequest(service, myParams);
      } catch (IndiciaAuthenticationException e)
      {
         LOGGER.debug("Failure in request, doing it a second time.", e);
         readAuthTokens.remove(websiteId, readAuthToken);
         readAuthToken = getReadAuthToken(websiteId, false);
         List<NameValuePair> myParams = addToken(params, readAuthToken);
         return doHttpPostRequest(service, myParams);
      }
   }

   public String doHttpPostRequestRW(String service, List<NameValuePair> params,
         int websiteId)
   {
      AuthToken token = getWriteAuthToken(websiteId);
      params.add(
            new BasicNameValuePair(PARAM_NAME_AUTH_TOKEN, token.getToken()));
      params.add(new BasicNameValuePair(PARAM_NAME_NONCE, token.getNonce()));
      return doHttpPostRequest(service, params);
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("IndiciaHttpConnector@");
      builder.append(System.identityHashCode(this));
      builder.append(" [indiciaURL=");
      builder.append(indiciaPath);
      builder.append(", websiteIds=");
      builder.append(websitePasswords.keySet());
      builder.append(", host=");
      builder.append(host);
      builder.append("]");
      return builder.toString();
   }

   public String doHttpMulitPartPost(String service,
         Map<String, ContentBody> parts, int websiteId)
   {
      try
      {
         AuthToken token = getWriteAuthToken(websiteId);
         MultipartEntityBuilder builder = MultipartEntityBuilder.create();

         builder = builder.addPart(PARAM_NAME_NONCE,
               new StringBody(token.getNonce(), ContentType.TEXT_PLAIN));
         builder = builder.addPart(PARAM_NAME_AUTH_TOKEN,
               new StringBody(token.getToken(), ContentType.TEXT_PLAIN));

         for (Entry<String, ContentBody> entry : parts.entrySet())
         {
            ContentBody contentBody = entry.getValue();
            String name = entry.getKey();
            builder.addPart(name, contentBody);
         }

         URI uri = URIUtils.createURI("http", host, -1, indiciaPath + service,
               null, null);
         HttpPost httpPost = new HttpPost(uri);
         httpPost.setEntity(builder.build());
         return performHTTPRequest(httpPost);
      } catch (URISyntaxException e)
      {
         throw new IndiciaException(FAILURE_CREATING_URI, e);
      } catch (IOException e)
      {
         throw new IndiciaException(FAILURE_DURING_REQUEST, e);
      }

   }
}
