package org.infinitenature.werbeo.service.indicia.http.types;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "person")
@XmlAccessorType(XmlAccessType.FIELD)
public class Person extends IndiciaTypeImpl
{
   @Override
   public String getModelName()
   {
      return "person";
   }

   @XmlElement(name = "first_name")
   @NotNull
   @Size(max = 30)
   private String firstName;
   @XmlElement(name = "surname")
   @NotNull
   @Size(max = 30)
   private String lastName;
   @XmlElement(name = "initials")
   @Size(max = 6)
   private String initials;
   @XmlElement(name = "email_address")
   private String emailAddress;
   @XmlElement(name = "external_key")
   private String externalKey;

   private String address;

   @XmlElement(name = "website_id")
   private long websiteId;

   public Person()
   {
      super();
   }

   public Person(String firstName, String lastName, String externalKey)
   {
      this.firstName = firstName;
      this.lastName = lastName;
      this.externalKey = externalKey;
   }

   public Person(int id, String firstName, String lastName, String initials,
         String emailAddress, String externalKey, long websiteId)
   {
      super();
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.initials = initials;
      this.emailAddress = emailAddress;
      this.externalKey = externalKey;
      this.websiteId = websiteId;

   }

   public String getAddress()
   {
      return address;
   }

   public String getEmailAddress()
   {
      return emailAddress;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public String getIdentifier()
   {
      StringBuilder sb = new StringBuilder();
      if (this.getFirstName() != null)
      {
         sb.append(this.getFirstName());
         sb.append(' ');
      }
      sb.append(this.getLastName());
      sb.append(", ");
      sb.append(this.getInitials());
      return sb.toString();
   }

   public String getInitials()
   {
      return initials;
   }

   public String getLastName()
   {
      return lastName;
   }

   public long getWebsiteId()
   {
      return websiteId;
   }

   public void setAddress(String address)
   {
      this.address = address;
   }

   public void setEmailAddress(String emailAddress)
   {
      this.emailAddress = emailAddress;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public void setInitials(String initials)
   {
      this.initials = initials;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   public void setWebsiteId(long websiteId)
   {
      this.websiteId = websiteId;
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Person [id=");
      builder.append(id);
      builder.append(", createdOn=");
      builder.append(createdOn);
      builder.append(", updatedOn=");
      builder.append(updatedOn);
      builder.append(", createdBy=");
      builder.append(createdBy);
      builder.append(", updatedBy=");
      builder.append(updatedBy);
      builder.append(", firstName=");
      builder.append(firstName);
      builder.append(", lastName=");
      builder.append(lastName);
      builder.append(", initials=");
      builder.append(initials);
      builder.append(", emailAddress=");
      builder.append(emailAddress);
      builder.append(", externalKey=");
      builder.append(externalKey);
      builder.append(", address=");
      builder.append(address);
      builder.append(", websiteId=");
      builder.append(websiteId);
      builder.append("]");
      return builder.toString();
   }

}
