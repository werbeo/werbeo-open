package org.infinitenature.werbeo.service.indicia.jpa.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryInit;

@Entity
@Table(name = "taxa_taxon_lists")
public class DBTaxaTaxonList extends Base implements HasTaxonAttributeValues
{

   @JoinColumn(name = "taxon_id", referencedColumnName = "id")
   @OneToOne(fetch = FetchType.LAZY)
   @QueryInit("taxon")

   private DBTaxon taxon;

   @Column(name = "preferred")
   private boolean preferred;

   @Column(name = "taxon_meaning_id")
   private int taxonMeaningId;

   @Column(name = "allow_data_entry")
   private boolean allowDataEntry;

   @ManyToOne
   @JoinColumn(name = "taxon_list_id", referencedColumnName = "id")
   @QueryInit("website")
   private DBTaxaList taxaList;

   @OneToMany(mappedBy = "taxaTaxonList", fetch = FetchType.LAZY)
   private Set<DBTaxonAttributeValue> taxonAttributeValues;

   @ManyToOne
   @JoinColumn(name = "parent_id", referencedColumnName = "id")
   private DBTaxaTaxonList parent;

   @OneToMany(mappedBy = "parent")
   private Set<DBTaxaTaxonList> children;

   @Override
   public Set<DBTaxonAttributeValue> getTaxonAttributeValues()
   {
      return taxonAttributeValues;
   }

   public void setTaxonAttributeValues(
         Set<DBTaxonAttributeValue> taxonAttributeValues)
   {
      this.taxonAttributeValues = taxonAttributeValues;
   }


   public DBTaxon getTaxon()
   {
      return taxon;
   }

   public void setTaxon(DBTaxon taxon)
   {
      this.taxon = taxon;
   }

   public boolean isPreferred()
   {
      return preferred;
   }

   public void setPreferred(boolean preferred)
   {
      this.preferred = preferred;
   }

   public int getTaxonMeaningId()
   {
      return taxonMeaningId;
   }

   public void setTaxonMeaningId(int taxonMeaningId)
   {
      this.taxonMeaningId = taxonMeaningId;
   }

   public DBTaxaList getTaxaList()
   {
      return taxaList;
   }

   public void setTaxaList(DBTaxaList taxaList)
   {
      this.taxaList = taxaList;
   }

   public DBTaxaTaxonList getParent()
   {
      return parent;
   }

   public void setParent(DBTaxaTaxonList parent)
   {
      this.parent = parent;
   }

   public Set<DBTaxaTaxonList> getChildren()
   {
      return children;
   }

   public void setChildren(Set<DBTaxaTaxonList> children)
   {
      this.children = children;
   }

   public boolean isAllowDataEntry()
   {
      return allowDataEntry;
   }

   public void setAllowDataEntry(boolean allowDataEntry)
   {
      this.allowDataEntry = allowDataEntry;
   }

}
