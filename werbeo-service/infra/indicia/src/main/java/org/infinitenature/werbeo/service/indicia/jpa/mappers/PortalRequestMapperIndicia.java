package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.function.Function;

import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PortalRequestMapperIndicia
      extends RequestMapperIndicia<PortalSortField>
{
   @Override
   default Function<PortalSortField, String[]> getSortFieldMapper()
   {
      return portalSortField ->
      {
         switch (portalSortField)
         {
         case URL:
            return new String[] { "url" };
         case TITLE:
            return new String[] { "title" };
         case ID:
         default:
            return new String[] { "id" };
         }
      };
   }
}
