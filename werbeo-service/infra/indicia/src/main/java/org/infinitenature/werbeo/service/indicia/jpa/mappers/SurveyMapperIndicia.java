package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.AdditionalAttributeNames;
import org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurvey;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurveyAttributeValue;
import org.infinitenature.werbeo.service.indicia.jpa.mappers.config.CentralConfigIndiciaJPA;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigIndiciaJPA.class, uses = {
      PersonMapperIndicia.class })
public interface SurveyMapperIndicia
{
   @Mapping(target = "allowDataEntry", expression = "java( getAllowDataEntry(jpaSurvey))")
   @Mapping(target = "availability", expression = "java( getAvailability(jpaSurvey))")
   @Mapping(target = "werbeoOriginal", expression = "java( getWerbeoOriginal(jpaSurvey))")
   @Mapping(target = "container", expression = "java( getContainer(jpaSurvey))")
   @Mapping(target = "obfuscationPolicies", expression = "java( getObfuscationPolicies(jpaSurvey))")
   @Mapping(target = "tags", expression = "java( getTags(jpaSurvey))")
   @Mapping(target = "deputyCustodians", ignore = true)
   @Mapping(target = "portal", source = "website")
   @Mapping(target = "parentId", expression = "java( getParentId(jpaSurvey))")
   @Mapping(target = "name", source = "title")
   @Mapping(target = "owner", source = "ownerId")
   Survey map(DBSurvey jpaSurvey);

   default Integer getParentId(DBSurvey dBSurvey)
   {
      if (dBSurvey.getParentId() == null)
      {
         return null;
      } else
      {
         return dBSurvey.getParentId().getId();
      }
   }

   default Availability getAvailability(
         org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurvey jpaSurvey)
   {
      Optional<DBSurveyAttributeValue> value = filterAttribute(jpaSurvey,
            AdditionalAttributeNames.AVAILABILITY);

      if (value.isPresent())
      {
         return Availability.valueOf(value.get().getTextValue());
      }
      return null;
   }

   default boolean getContainer(
         org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurvey jpaSurvey)
   {
      Optional<DBSurveyAttributeValue> value = filterAttribute(jpaSurvey,
            AdditionalAttributeNames.CONTAINER);

      if (value.isPresent())
      {
         return value.get().getIntValue() == 1 ? true : false;
      }
      return false;
   }

   default Optional<DBSurveyAttributeValue> filterAttribute(
         org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurvey jpaSurvey,
         String attributeName)
   {
      return jpaSurvey.getSurveyAttributeValuesCollection().stream()
            .filter(surveyAttributeValues -> surveyAttributeValues
                  .getSurveyAttribute().getCaption().equals(attributeName))
            .findFirst();
   }

   default Set<DBSurveyAttributeValue> filterAttributes(
         org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurvey jpaSurvey,
         String attributeName)
   {
      return jpaSurvey.getSurveyAttributeValuesCollection().stream()
            .filter(surveyAttributeValue -> !surveyAttributeValue.isDeleted())
            .filter(surveyAttributeValues -> surveyAttributeValues
                  .getSurveyAttribute().getCaption().equals(attributeName))
            .collect(Collectors.toSet());
   }
   default boolean getWerbeoOriginal(DBSurvey jpaSurvey)
   {
      Optional<DBSurveyAttributeValue> value = filterAttribute(jpaSurvey,
            AdditionalAttributeNames.WERBEO_ORIGINAL);

      if (value.isPresent())
      {
         return value.get().getIntValue() == 1 ? true : false;
      }
      return true;
   }

   default boolean getAllowDataEntry(DBSurvey jpaSurvey)
   {
      Optional<DBSurveyAttributeValue> value = filterAttribute(jpaSurvey,
            AdditionalAttributeNames.ALLOW_DATA_ENTRY);

      if (value.isPresent() && value.get().getIntValue() != null)
      {
         return value.get().getIntValue() == 1 ? true : false;
      }
      return true;
   }

   default List<ObfuscationPolicy> getObfuscationPolicies(
         org.infinitenature.werbeo.service.indicia.jpa.entities.DBSurvey jpaSurvey)
   {
      Optional<DBSurveyAttributeValue> value = filterAttribute(jpaSurvey,
            AdditionalAttributeNames.OBFUSCATION_POLICIES);

      List<ObfuscationPolicy> policies = new ArrayList<>();
      if (value.isPresent()
            && StringUtils.isNotBlank(value.get().getTextValue()))
      {
         for (String policy : value.get().getTextValue().split(","))
         {
            String[] policySplit = policy.split("_");
            policies.add(new ObfuscationPolicy(policySplit[0],
                  policy.substring(policy.indexOf("_") + 1)));
         }
      }
      return policies;
   }

   default Set<String> getTags(DBSurvey jpaSurvey)
   {
      return filterAttributes(jpaSurvey, AdditionalAttributeNames.TAG).stream()
            .map(DBSurveyAttributeValue::getTextValue)
            .collect(Collectors.toSet());

   }

   List<Survey> map(List<DBSurvey> content);
}
