package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListFilter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestTaxaListQueries extends IntegrationTest
{

   @Autowired
   private TaxaListQueriesAdapter taxaListQueriesAdapterUT;

   @Test
   public void testCount_ByName()
   {
      assertThat(
            taxaListQueriesAdapterUT.count(new TaxaListFilter("German SL 1.2"),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A))),
            is(1l));
   }
   @Test
   public void testCount_ByNameNoMatch()
   {
      assertThat(
            taxaListQueriesAdapterUT.count(new TaxaListFilter("GermanSL1.2"),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A))),
            is(0l));
   }

   @Test
   public void testfind_ALL()
   {
      Slice<TaxaList, TaxaListSortField> slice = taxaListQueriesAdapterUT.find(
            TaxaListFilter.ALL,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)),
            new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                  TaxaListSortField.ID));

      assertThat(slice.getNumberOfElements(), is(4));
      assertThat(slice.getContent().get(0).getName(), is("German SL 1.2"));
      assertThat(slice.getContent().get(1).getName(), is("Heuschrecken 1.0"));
      assertThat(slice.getContent().get(2).getName(), is("Dermaptera"));
      assertThat(slice.getContent().get(3).getName(), is("GermanSL 1.5.1"));
   }

   @Test
   public void testCount_ALL()
   {
      assertThat(taxaListQueriesAdapterUT.count(TaxaListFilter.ALL,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A))),
            is(4l));
   }

   @Test
   public void testGet()
   {
      TaxaList taxaList = taxaListQueriesAdapterUT.get(1, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(taxaList.getName(), is("German SL 1.2"));
      assertThat(taxaList.getTaxaGroups(), hasSize(6));
      assertThat(taxaList.getTaxaGroups(), containsInAnyOrder("Algen",
            "Flechten", "Moose", "Pteridophyta", "Rest", "Spermatophyta"));
   }

}
