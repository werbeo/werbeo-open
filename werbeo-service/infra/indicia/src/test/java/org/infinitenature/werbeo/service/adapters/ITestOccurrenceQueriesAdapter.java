package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.LifeStage;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Makropter;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.StreamSlice;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter.WKTMode;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ITestOccurrenceQueriesAdapter extends IntegrationTest
{

   @Autowired
   private OccurrenceQueriesAdapter occurrenceQueriesAdapterUT;
   @Autowired
   private CoordinateTransformerFactory coordinateTransformerFactory;

   @Test
   void testCount_spatialFilterMunich()
   {
      String munich4326 = mtbToWKT4326("7835");

      User user = null;
      long result = occurrenceQueriesAdapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, null, null, munich4326, null,
                        false, null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(result, is(0l));
   }

   @Test
   void testFind_spatialFilter1943()
   {
      String mtb = "1943";
      String grimmen4326 = mtbToWKT4326(mtb);

      User user = null;
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, grimmen4326, null,
            false, null, null);
      List<Occurrence> occurrences = occurrenceQueriesAdapterUT.findOccurrences(
                  filter,
            createContext(PortalTestUtil.FLORA_MV_ID, user),
            new OffsetRequestImpl<>(0, 500, SortOrder.ASC,
                  OccurrenceSortField.ID))
            .getContent();
      assertThat(occurrences.size(), is(greaterThan(0)));
      for (Occurrence occurrence : occurrences)
      {
         assertThat(occurrence.getSample().getLocality().getPosition().getMtb()
               .getMtb(), startsWith(mtb));
      }
   }

   private String mtbToWKT4326(String mtbString)
   {
      String wgt1943 = MTBHelper.toMTB(mtbString).toWkt();

      CoordinateTransformer ct = coordinateTransformerFactory
            .getCoordinateTransformer(4745, 4326);

      String munich4326 = ct.convert(wgt1943);
      return munich4326;
   }

   @Test
   void testCount_spatialFilterGlewitz()
   {

      String glewitz4745 = MTBHelper.toMTB("1943/1").getCenterWkt();

      CoordinateTransformer ct = coordinateTransformerFactory
            .getCoordinateTransformer(4745, 4326);

      String glewitz4326 = ct.convert(glewitz4745);

      User user = null;
      long result = occurrenceQueriesAdapterUT
            .countOccurrences(
                  new OccurrenceFilter(null, null, null, glewitz4326, null,
                        false, null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user));

      assertThat(result, is(0l)); // POINTS are never "WITHIN"
   }

   @Test
   void testFind_RestService()
   {
      User user = null;
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.REST_SRV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.ID));

      assertThat(occurrences.getNumberOfElements(), is(10));

   }

   @Test
   void testFind_FloraMV()
   {
      User user = null;
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.ID));

      assertThat(occurrences.getNumberOfElements(), is(10));
   }

   @Test
   void testFind_FloraBB()
   {
      User user = null;
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_BB_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.ID));

      assertThat(occurrences.getNumberOfElements(), is(0));

   }

   @Test
   void testFind_filterByStartDate()
   {
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(LocalDate.of(2018, 3, 29), null, null,
                        null, null, false, null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.ID));

      assertThat(occurrences.getNumberOfElements(), is(5));
   }

   @Test
   void testFind_filterByStartDateAndTaxa()
   {
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(LocalDate.of(2018, 3, 29), null,
                        25244,
                        null, null, false, null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.ID));

      Occurrence occurrence = occurrences.getContent().get(0);

      assertAll(() -> assertThat(occurrences.getNumberOfElements(), is(
            1)),
            () -> assertThat(occurrence.getId(),
                  is(UUID.fromString("41a53785-2092-42a3-95d3-b4dea6f099bc"))),
            () -> assertThat(occurrence.getTaxon().getName(),
                  is("Stigmidium fuscatae")));
   }

   @Test
   void testFind_filterByStartDateAndTaxa_NotUsedTaxa()
   {
      // Rubus visurgianus
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(LocalDate.of(2018, 3, 29), null, 12704,
                        null, null, false, null,
                        null),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.ID));

      assertThat(occurrences.getNumberOfElements(), is(0));
   }

   @Test
   void testCount_maxBlur10()
   {
      long occurrences = occurrenceQueriesAdapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, null, 10, false, null, null),
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(occurrences, is(0l));
   }

   @Test
   void testCount_maxBlur200()
   {
      long occurrences = occurrenceQueriesAdapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, null, 200, false, null,
                  null),
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(occurrences, is(7426l));
   }

//   @Test
//   public void testCount_filterByEndDate()
//   {
//
//      long occurrences = occurrenceQueriesAdapterUT.countOccurrences(
//            new OccurrenceFilter(null, LocalDate.of(1997, 12, 31), null, null,
//                  null, false, null, null),
//            createContext(PortalTestUtil.FLORA_MV_ID,
//                  getUser(UserTestUtil.USER_LOGIN_A)));
//
//      assertThat(occurrences, is(84l));
//   }

   @Test
   void testFind_SortByDateASC()
   {
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 1, SortOrder.ASC,
                        OccurrenceSortField.DATE));

      assertThat(occurrences.getContent().get(0).getSample().getDate(),
            is(VagueDateFactory.create(1997)));
   }

   @Test
   void testFind_SortByDateDESC()
   {
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 1, SortOrder.DESC,
                        OccurrenceSortField.DATE));

      assertThat(occurrences.getContent().get(0).getSample().getDate(),
            is(VagueDateFactory.createMonthInYear(2018, 10)));
   }

   @Test
   void testFind_SortByDateDESC2()
   {
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 500, SortOrder.DESC,
                        OccurrenceSortField.DATE));

      int index = 0;
      int lastIndex = occurrences.getContent().size() - 1;
      for (Occurrence occurrence : occurrences.getContent())
      {
         if ((index + 1) <= lastIndex)
         {
            assertTrue(occurrences.getContent().get(index + 1).getSample()
                  .getDate().compareTo(occurrence.getSample().getDate()) <= 0);
            index++;
         }
      }
      assertThat(occurrences.getContent().get(0).getSample().getDate(),
            is(VagueDateFactory.createMonthInYear(2018, 10)));
   }

   @Test
   void testFind_SortByTimeASC()
   {
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.HEUSCHRECKEN_D_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.TIME_OF_DAY));

      assertAll(
            () -> assertNull(occurrences.getContent().get(2).getTimeOfDay()),
            () -> assertThat(
                  occurrences.getContent().get(3).getTaxon().getName(),
                  is("Bicolorana bicolor")),
            () -> assertThat(occurrences.getContent().get(3).getTimeOfDay(),
                  is("06:25")),
            () -> assertThat(
                  occurrences.getContent().get(4).getTaxon().getName(),
                  is("Oedaleus decorus")),
            () -> assertThat(occurrences.getContent().get(4).getTimeOfDay(),
                  is("16:11")));
   }

   @Test
   void testFind_SortByTimeDESC()
   {
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  new OccurrenceFilter(null, null, null, null, null, false,
                        null, null),
                  createContext(PortalTestUtil.HEUSCHRECKEN_D_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 10, SortOrder.DESC,
                        OccurrenceSortField.TIME_OF_DAY));
      assertAll(() -> assertThat(
            occurrences.getContent().get(0).getTaxon().getName(),
            is("Oedaleus decorus")),
            () -> assertThat(occurrences.getContent().get(0).getTimeOfDay(),
                  is("16:11")),
            () -> assertThat(
                  occurrences.getContent().get(1).getTaxon().getName(),
                  is("Bicolorana bicolor")),
            () -> assertThat(occurrences.getContent().get(1).getTimeOfDay(),
                  is("06:25")),
            () -> assertNull(occurrences.getContent().get(2).getTimeOfDay()));
   }

   @Test
   void testCount_filterByOwner()
   {
      long occurrences = occurrenceQueriesAdapterUT.countOccurrences(
            new OccurrenceFilter(null, null, null, null, null, false,
                  "hans@dampf.de", null),
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(occurrences, is(0l));
   }

   @Test
    void testCount_filterOnlyOwn_haveningData()
   {

      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, null);
      Map<Filter, Collection<Portal>> portalBasedFilters = new HashMap<>();
      portalBasedFilters.put(Filter.ONLY_OWN_DATA, new HashSet<>());
      portalBasedFilters.get(Filter.ONLY_OWN_DATA)
            .add(new Portal(PortalTestUtil.FLORA_MV_ID));
      filter.setPortalBasedFilters(portalBasedFilters);
      long occurrences = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(occurrences, is(7426L));
   }

   @Test
    void testCount_filterOnlyOwn_haveningNoData()
   {

      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, null);
      Map<Filter, Collection<Portal>> portalBasedFilters = new HashMap<>();
      portalBasedFilters.put(Filter.ONLY_OWN_DATA, new HashSet<>());
      portalBasedFilters.get(Filter.ONLY_OWN_DATA)
            .add(new Portal(PortalTestUtil.FLORA_MV_ID));
      filter.setPortalBasedFilters(portalBasedFilters);
      long occurrences = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.REST_SRV_ID,
                  getUser(UserTestUtil.USER_LOGIN_B)));

      assertThat(occurrences, is(38l));
   }

   @Test
    void testStream_first()
   {
      ContinuationToken continuationToken = ContinuationToken.FIRST;

      StreamSlice<Occurrence, UUID> response = occurrenceQueriesAdapterUT
            .streamOccurrences(createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)), continuationToken);

      assertThat(response.getEntities().size(), is(150));
      assertThat(response.getDeletedIds().size(), is(0));
      assertThat(response.getContinuationToken(),
            is(not(ContinuationToken.FIRST)));
   }

   @Test
    void testStream_highIDButUpdated()
   {
      ContinuationToken continuationToken = new ContinuationToken(10000, 100L);

      StreamSlice<Occurrence, UUID> response = occurrenceQueriesAdapterUT
            .streamOccurrences(createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)), continuationToken);

      assertThat(response.getEntities().size(), is(150));
      assertThat(response.getDeletedIds().size(), is(0));
      assertThat(response.getContinuationToken(),
            is(not(new ContinuationToken(10000, 100L))));
   }

   @Test
    void testStream_noChangedOnes()
   {

      ContinuationToken continuationToken = new ContinuationToken(7378,
            2523607814000L);

      StreamSlice<Occurrence, UUID> response = occurrenceQueriesAdapterUT
            .streamOccurrences(createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)), continuationToken);

      assertThat(response.getEntities().size(), is(0));
      assertThat(response.getDeletedIds().size(), is(0));
      assertThat(response.getContinuationToken(),
            is(new ContinuationToken(7378, 2523607814000L)));
   }

   @Test

   void testGetOccurrenceCentroids()
   {
      // 34427 => Betula pubescens
      Slice<OccurrenceCentroid, OccurrenceCentroidSortField> response = occurrenceQueriesAdapterUT
            .findOccurrenceCentroids(OccurrenceFilter.taxonInclChildren(
                  34427),
                  createContext(PortalTestUtil.FLORA_MV_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceCentroidSortField.DATE));

      assertAll(() -> assertThat(response.getNumberOfElements(), is(3)),
            () -> assertThat(response.getContent().get(0).getDate(),
                  is(LocalDate.of(1997, 1,
                        1))),
            () -> assertThat(response.getContent().get(0).getSurveyId(),
                  is(16)));
   }

   @Test
    void testCountOccurrenceCentroids()
   {
      // 34427 => Betula pubescens
      long response = occurrenceQueriesAdapterUT.countOcurrenceCentroids(
            OccurrenceFilter.taxonInclChildren(
                  34427),
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(response, is(3L));
   }

   @Test
   void testCountSurvey_notExsiting()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, 9999);
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   void testCountSurvey_existing()
   {
      OccurrenceFilter filter = new OccurrenceFilter(null, null, null, null,
            null, false, null, 16);
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(7388L));
   }

   @Test
   void testCountMTB_noData()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setMtb(MTBHelper.toMTB("2222"));
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   void testCountMTB()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setMtb(MTBHelper.toMTB("1643"));
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(804L));
   }

   @Test
   void testCountMTBQ()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setMtb(MTBHelper.toMTB("1643/2"));
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(124L));
   }

   @Test
   void testCountHerbary()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setHerbaryCode("GFW");
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(1L));
   }

   @Test
   void testCountValdiationStatus()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setValidationStatus(ValidationStatus.VALID);
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(1L));
   }

   @Test
   @DisplayName("Find by non existing validator")
   void testCountValdiator()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setValidator("abc");
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("Find by existing validator")
   void testCountValdiator2()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setValidator(UserTestUtil.USER_LOGIN_A);
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(2L));
   }

   @Test
   @DisplayName("check on Bicolorana bicolor: LifeStage, Makropter")
   void testCheck_BiocoloranaBicolor_LifeStage_Makropter()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setTaxonId(19113);
      Slice<Occurrence, OccurrenceSortField> occurrences = occurrenceQueriesAdapterUT
            .findOccurrences(
                  filter,
                  createContext(PortalTestUtil.HEUSCHRECKEN_D_ID,
                        getUser(UserTestUtil.USER_LOGIN_A)),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        OccurrenceSortField.MOD_DATE)); // sorting: default values

      assertThat(occurrences.getContent().size(), is(1));
      assertThat(occurrences.getContent().get(0).getTaxon().getName(),
            is("Bicolorana bicolor"));
      assertThat(occurrences.getContent().get(0).getLifeStage(), is(LifeStage.LARVE_IMAGO));
      assertThat(occurrences.getContent().get(0).getMakropter(), is(Makropter.YES));
   }

   @Test
   @DisplayName("filter by external key - match")
   void test041()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setOccExternalKey("Flora-MV-Global-ID: 1234");
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(1L));
   }

   @Test
   @DisplayName("Count by external key - case sesitive no match")
   void test042()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setOccExternalKey("GLOBAL");
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("Count by external key - case sesitive match")
   void test043()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setOccExternalKey("Global");
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(2L));
   }

   @Test
   @DisplayName("Count by modified since - no data because date in the future")
   void test044()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setModifiedSince(LocalDateTime.now().plus(1, ChronoUnit.DAYS));
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }

   @Test
   @DisplayName("Count by modified since - all data because date long in the past")
   void test045()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setModifiedSince(LocalDateTime.of(2000, 1, 1, 10, 0));
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(7426L));
   }

   @Test
   @DisplayName("Count by wkt intersect")
   void test046()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setWkt(GeomTestUtil.WKT_INERSECTS_1944_1_AND_2);
      filter.setWktMode(WKTMode.INTERSECTS);
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(287L));
   }

   @Test
   @DisplayName("Count by wkt within")
   void test047()
   {
      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setWkt(GeomTestUtil.WKT_INERSECTS_1944_1_AND_2);
      filter.setWktMode(WKTMode.WITHIN);
      long count = occurrenceQueriesAdapterUT.countOccurrences(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)));

      assertThat(count, is(0L));
   }
}