package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.TaxaList;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ITestTaxonQueriesAdapter extends IntegrationTest
{
   @Autowired
   private TaxonQueriesAdapter taxonQueriesAdapter;

   @Test
   void testFindAllSynomyms()
   {
      // Wald-Sauerklee und Oxalis acetosella
      List<Taxon> list = taxonQueriesAdapter.findAllSynonyms(11935,
            createContext(PortalTestUtil.FLORA_MV_ID, null));

      Set<String> set = new HashSet<>();

      for (Taxon taxon : list)
      {
         set.add(taxon.getName());
      }
      assertAll(() -> assertThat(list.size(), is(
            2)),
            () -> assertThat(set.contains("Wald-Sauerklee"), is(true)),
            () -> assertThat(set.contains("Oxalis acetosella"), is(true)));
   }

   @Test
   void testFindTaxaWithChangedOcurrencesOrSamples()
   {
      TaxonFilter taxonFilter = new TaxonFilter("", null, false,
            LocalDate.of(2020, 01, 01), false, new HashSet<String>(), null, false);
      Slice<TaxonBase, TaxonSortField> slice = taxonQueriesAdapter.findBase(
            taxonFilter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.USER_LOGIN_A)),
            new OffsetRequestImpl<>(0, 50, SortOrder.ASC,
                  TaxonSortField.TAXON));

      assertThat(slice.getNumberOfElements(), is(40));
   }

   @Test
   void testGetByExternalId()
   {
      TaxaList list = new TaxaList();
      list.setId(1);
      String externalId = "80211";
      TaxonBase taxon = taxonQueriesAdapter.getByExternalId(list.getId(),
            externalId);
      assertThat(taxon.getName(), is("Arnellia species"));
   }

   @Test
   void testGetByExternalId_hasCommonName()
   {
      TaxaList list = new TaxaList();
      list.setId(1);
      String externalId = "4711"; // "Ranunculus cassubicus" and
                                  // "Wenden-Goldhahnenfuß"
      TaxonBase taxon = taxonQueriesAdapter.getByExternalId(list.getId(),
            externalId);
      assertThat(taxon.getName(), is("Ranunculus cassubicus"));
   }

   @Test
   void testGetByExternalId_notFound()
   {
      String externalId = "80211AABC";
      assertThrows(EntityNotFoundException.class, () -> taxonQueriesAdapter
            .getByExternalId(1, externalId));
   }

   @Test
   void testGet()
   {
      Taxon taxon = taxonQueriesAdapter.get(85, createContext(
            PortalTestUtil.FLORA_MV_ID, User.INTERNAL_SUPER_USER));

      assertAll(() -> assertThat(taxon.getName(), is("Desmarestia aculeata")),
            () -> assertThat(taxon.getGroup(), is("Algen")),
            () -> assertThat(taxon.getId(), is(85)),
            () -> assertThat(taxon.getExternalKey(), is("91019")));
   }

   @Test
   void testGet_invalidId()
   {
      Context contextFloraMV = createContext(PortalTestUtil.FLORA_MV_ID,
            User.INTERNAL_SUPER_USER);

      assertThrows(EntityNotFoundException.class,
            () -> taxonQueriesAdapter.get(Integer.MAX_VALUE, contextFloraMV));
   }

   @Test
   void testGet_invalidIdForPortal()
   {
      Context contextHeuschrecken = createContext(
            PortalTestUtil.HEUSCHRECKEN_D_ID, User.INTERNAL_SUPER_USER);
      assertThrows(EntityNotFoundException.class,
            () -> taxonQueriesAdapter.get(85, contextHeuschrecken));
   }
}
