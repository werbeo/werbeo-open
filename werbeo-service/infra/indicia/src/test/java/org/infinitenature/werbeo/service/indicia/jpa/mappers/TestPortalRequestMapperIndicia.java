package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Iterator;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Description;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

class TestPortalRequestMapperIndicia
{

   private PortalRequestMapperIndicia mapperUT;

   @BeforeEach
   void setUp() throws Exception
   {
      mapperUT = new PortalRequestMapperIndiciaImpl();
   }

   @Test
   @Description("Sort by title ascending")
   void test001()
   {
      Sort sort = mapperUT.mapSort(new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
            PortalSortField.TITLE));

      Iterator<Order> orders = sort.iterator();
      Order order = orders.next();
      assertThat(order.getProperty(), is("title"));
      assertThat(order.getDirection(), is(Direction.ASC));

      assertThat(orders.hasNext(), is(false));
   }

   @Test
   @Description("Sort by id descending")
   void test002()
   {
      Sort sort = mapperUT.mapSort(
            new OffsetRequestImpl<>(0, 10, SortOrder.ASC, PortalSortField.ID));

      Iterator<Order> orders = sort.iterator();
      Order order = orders.next();
      assertThat(order.getProperty(), is("id"));
      assertThat(order.getDirection(), is(Direction.ASC));

      assertThat(orders.hasNext(), is(false));
   }

   @Test
   @Description("Sort by url ascending")
   void test003()
   {
      Sort sort = mapperUT.mapSort(
            new OffsetRequestImpl<>(0, 10, SortOrder.ASC, PortalSortField.URL));

      Iterator<Order> orders = sort.iterator();
      Order order = orders.next();
      assertThat(order.getProperty(), is("url"));
      assertThat(order.getDirection(), is(Direction.ASC));

      assertThat(orders.hasNext(), is(false));
   }

   @Test
   @Description("Sort by id descending")
   void test004()
   {
      Sort sort = mapperUT.mapSort(
            new OffsetRequestImpl<>(0, 10, SortOrder.DESC, PortalSortField.ID));

      Iterator<Order> orders = sort.iterator();
      Order order = orders.next();
      assertThat(order.getProperty(), is("id"));
      assertThat(order.getDirection(), is(Direction.DESC));

      assertThat(orders.hasNext(), is(false));
   }
}
