package org.infinitenature.werbeo.service.adapters;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy;
import org.junit.jupiter.api.Test;

public class TestSurveyCommandsAdapter
{

   @Test
   void test_merge()
   {
      SurveyCommandAdapter adapter = new SurveyCommandAdapter();
      
      
      ObfuscationPolicy pol1 = new ObfuscationPolicy("ACCEPTED", "PERSONS");
      ObfuscationPolicy pol2 = new ObfuscationPolicy("ADMIN", "LOCATION_POINT");
      List<ObfuscationPolicy> pols = new ArrayList<>();
      pols.add(pol1);
      pols.add(pol2);
      assertEquals("ACCEPTED_PERSONS,ADMIN_LOCATION_POINT", adapter.merge(pols));
      
   }

   
}
