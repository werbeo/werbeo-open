package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestSampleMapperHelper
{
   private SampleMappingHelper sampleMappingHelperUT;
   private PersonQueries personQueriesMock;
   private Context context;
   @BeforeEach
   void setUp()
   {
      personQueriesMock = mock(PersonQueries.class);
      when(personQueriesMock.get(eq(4), any(Context.class)))
            .thenReturn(new Person(4, "Hans", "Wurst"));
      sampleMappingHelperUT = new SampleMappingHelper();

      sampleMappingHelperUT.personQueries = personQueriesMock;

      context = mock(Context.class);
   }

   @Test
   @DisplayName("Correct person id")
   void test001()
   {
      Person person = sampleMappingHelperUT.toPerson("#id-4#", context);
      assertThat(person.getFirstName(), is("Hans"));
      assertThat(person.getLastName(), is("Wurst"));
      assertThat(person.getId(), is(4));

      verify(personQueriesMock, times(1)).get(eq(4), any(Context.class));
   }

   @Test
   @DisplayName("Bad person id")
   void test002()
   {
      Person person = sampleMappingHelperUT.toPerson("   #id-4#", context);
      assertThat(person, is(nullValue()));

      verify(personQueriesMock, never()).get(anyInt(), any(Context.class));
   }

   @Test
   @DisplayName("Not existing person Id")
   void test003()
   {
      Person person = sampleMappingHelperUT.toPerson("#id-6#", context);
      assertThat(person, is(nullValue()));

      verify(personQueriesMock, times(1)).get(eq(6), any(Context.class));
   }

}
