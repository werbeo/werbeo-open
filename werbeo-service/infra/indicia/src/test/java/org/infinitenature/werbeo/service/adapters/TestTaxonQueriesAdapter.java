package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Collections;
import java.util.HashSet;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Language;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.querydsl.core.types.dsl.BooleanExpression;

class TestTaxonQueriesAdapter
{
   private Context context;
   private TaxonQueriesAdapter taxonQueriesAdapterUT;

   @BeforeEach
   void setUp() throws Exception
   {
      this.taxonQueriesAdapterUT = new TaxonQueriesAdapter();
      this.taxonQueriesAdapterUT.setInstanceConfig(new InstanceConfig());
      Portal portal = new Portal();
      portal.setId(55);
      this.context = new Context(portal, new User(), Collections.emptySet(),
            "test");
   }

   @Test
   @DisplayName("Prepare find all query")
   void test001()
   {
      BooleanExpression findAllQuery = taxonQueriesAdapterUT
            .prepareQuery(new TaxonFilter(null, null, true, null, false, new HashSet<String>(), null, false),
                  context);

      assertThat(findAllQuery.toString(), is("dBTaxon.deleted = false && (dBTaxon.taxaTaxonList.taxaList.website is null || dBTaxon.taxaTaxonList.taxaList.website.id = 55)"));
   }

   @Test
   @DisplayName("Prepare find by name")
   void test002()
   {
      BooleanExpression findAllQuery = taxonQueriesAdapterUT
            .prepareQuery(new TaxonFilter("Abies", null, true, null, false, new HashSet<String>(), null, false),
                  context);

      assertThat(findAllQuery.toString(),
            is("dBTaxon.deleted = false && (dBTaxon.taxaTaxonList.taxaList.website is null || dBTaxon.taxaTaxonList.taxaList.website.id = 55) && lower(dBTaxon.taxon) like abies%"));
   }

   @Test
   @DisplayName("Prepare find by parts of name")
   void test003()
   {
      BooleanExpression findAllQuery = taxonQueriesAdapterUT
            .prepareQuery(
                  new TaxonFilter("AB AL", new HashSet<>(), true, null, false, new HashSet<String>(), null, false),
                  context);

      assertThat(findAllQuery.toString(),
            is("dBTaxon.deleted = false && (dBTaxon.taxaTaxonList.taxaList.website is null || dBTaxon.taxaTaxonList.taxaList.website.id = 55) && lower(dBTaxon.taxon) like ab% al%"));
   }

   @Test
   @DisplayName("Prepare find by languages")
   void test004()
   {
      HashSet<Language> languages = new HashSet<>();
      languages.add(Language.LAT);
      languages.add(Language.DEU);
      BooleanExpression findAllQuery = taxonQueriesAdapterUT
            .prepareQuery(new TaxonFilter(null, languages, true, null, false, new HashSet<String>(), null, false),
                  context);

      assertThat(findAllQuery.toString(), is(
            "dBTaxon.deleted = false && (dBTaxon.taxaTaxonList.taxaList.website is null || dBTaxon.taxaTaxonList.taxaList.website.id = 55) && dBTaxon.language.iso in [deu, lat]"));
   }
}
