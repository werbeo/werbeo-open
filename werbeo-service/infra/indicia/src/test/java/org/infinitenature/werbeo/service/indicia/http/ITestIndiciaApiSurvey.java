package org.infinitenature.werbeo.service.indicia.http;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSurvey;
import org.infinitenature.werbeo.service.indicia.http.types.PersonAsSubelement;
import org.infinitenature.werbeo.service.indicia.http.types.SurveySubmissionContrainer;
import org.junit.jupiter.api.Test;

class ITestIndiciaApiSurvey extends IntegrationTest
{
   private final String childSurveyNamep = "JUNIT-TESTS " + LocalDateTime.now()
         + UUID.randomUUID();

   private String parentSurveyName = "JUNIT-TESTS " + LocalDateTime.now()
         + UUID.randomUUID();

   @Test
   void test()
   {

      IndiciaSurvey parentSurvey = new IndiciaSurvey(parentSurveyName);
      parentSurvey.setOwner(new PersonAsSubelement(5));
      Map<String, String> additionalAttributes = new HashMap<>();
      additionalAttributes.put("1", "FREE");
      additionalAttributes.put("2", "true");
      int parentSurveyId = indiciaApi.save(new SurveySubmissionContrainer(
            parentSurvey, Collections.emptySet(), additionalAttributes), 1, 0);

      assertThat(parentSurveyId, is(not(0)));
      IndiciaSurvey childSurvey = new IndiciaSurvey(childSurveyNamep, parentSurveyId);

      int childSurveyId = indiciaApi.save(new SurveySubmissionContrainer(
            childSurvey, Collections.emptySet(), additionalAttributes), 1, 0);

      assertThat(childSurveyId, is(not(0)));

   }
}
