package org.infinitenature.werbeo.service.indicia.http.types;

import static org.junit.Assert.assertEquals;

import java.util.GregorianCalendar;

import org.infinitenature.werbeo.service.indicia.http.types.DateAdapter;
import org.junit.*;

public class TestDateAdapter
{
   private DateAdapter dateAdapter;

   @Before
   public void setUp() throws Exception
   {
      dateAdapter = new DateAdapter();
   }

   @Test
   public void testUnmarshall() throws Exception
   {
      GregorianCalendar calendar = new GregorianCalendar(2011,
	    GregorianCalendar.MARCH, 22, 12, 8, 7);
      assertEquals(calendar.getTime(),
	    dateAdapter.unmarshal("2011-03-22 12:08:07"));
   }
}
