package org.infinitenature.werbeo.service.indicia.http.http;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.message.BasicNameValuePair;
import org.infinitenature.werbeo.service.indicia.http.IntegrationTest;
import org.infinitenature.werbeo.service.indicia.http.SharingOption;
import org.infinitenature.werbeo.service.indicia.http.types.Mode;
import org.infinitenature.werbeo.service.indicia.http.types.ReportSource;
import org.infinitenature.werbeo.service.indicia.http.types.submission.Field;
import org.infinitenature.werbeo.service.indicia.http.types.submission.Model;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

class MTestIndiciaHttpConnector extends IntegrationTest
{
   private static final org.slf4j.Logger LOGGER = LoggerFactory
	 .getLogger(MTestIndiciaHttpConnector.class);
   @Autowired
   private IndiciaHttpConnector indiciaHttpConnector;

   @Test
   void testHttpGetRequest() throws IOException
   {

      List<NameValuePair> params = new ArrayList<>();
      // params.add(new BasicNameValuePair("authority", "dve*"));
      // params.add(new BasicNameValuePair("view", "detail"));
      params.add(new BasicNameValuePair("mode", "xml"));
      params.add(SharingOption.DATA_FLOW.asRequestParameter());
      // params.add(new BasicNameValuePair("person_id", "2262"));
      // params.add(new BasicNameValuePair("query",
      // "{\"where\":[{\"taxon\":\"Sonnenblume\"}]}"));
      // params.add(new BasicNameValuePair("caption", "UUID"));
      // params.add(new BasicNameValuePair("survey",
      // "DE-BE/BB-Kartei-Onlineeingabe"));
      String xml = indiciaHttpConnector
            .doHttpGetRequest("/index.php/services/data/website/", params,
                  WEBSITE_ID);
      System.out.println(xml);
      // FileUtils.writeStringToFile(new File("GermanSL.xml"), xml);
   }

   @Test
   void testUpdateMultibleAdditionAttributes() throws Exception
   {
      Set<Field> fields = new HashSet<>();
      fields.add(new Field("title", "JUNIT test survey 2"));
      fields.add(new Field("id", "4"));
      fields.add(new Field("description", "JUNIT test survey"));
      fields.add(new Field("website_id", "2"));
      fields.add(new Field("srvAttr:1", ""));
     // fields.add(new Field("srvAttr:1::3", "194"));
      //fields.add(new Field("srvAttr:1::45", "15"));


      Model model = new Model("survey", fields);

      AuthToken token = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);

      List<NameValuePair> params = new ArrayList<>();

      params.add(new BasicNameValuePair("auth_token", token.getToken()));
      params.add(new BasicNameValuePair("nonce", token.getNonce()));
      params.add(new BasicNameValuePair("submission",
	    model.getJsonObject().toString()));

      System.out.println(model.getJsonObject().toString());

      String response = indiciaHttpConnector
	    .doHttpPostRequest("/index.php/services/data/save", params);
      System.out.println(response);
   }

   @Test
   void testSaveLocationDirect() throws Exception
   {
      Set<Field> fields = new HashSet<>();
      fields.add(new Field("name", "Berlin 2"));
      fields.add(new Field("boundary_geom",
	    "POLYGON((1473345.80176891 6920895.95907107,1492607.93289393 6928539.66189854,1528074.71401315 6917532.72982703,1513704.55269807 6857911.84777286,1451026.18951317 6864332.55814787,1473345.80176891 6920895.95907107))"));
      Model model = new Model("location", fields);
      List<NameValuePair> params = new ArrayList<>();
      // params.add(new BasicNameValuePair("authority", "dve*"));
      AuthToken token = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);
      params.add(new BasicNameValuePair("auth_token", token.getToken()));
      params.add(new BasicNameValuePair("nonce", token.getNonce()));
      params.add(new BasicNameValuePair("submission",
	    model.getJsonObject().toString()));
      String response = indiciaHttpConnector
	    .doHttpPostRequest("/index.php/services/data/save", params);
      System.out.println(response);
   }

   @Test
   void testSavePersonDirect() throws Exception
   {
      Set<Field> fields = new HashSet<>();

      fields.add(new Field("first_name", "JUNIT"));
      fields.add(new Field("surname", "ManualTest"));
      fields.add(new Field("email_address", UUID.randomUUID() + "@test.xx"));
      fields.add(new Field("psnAttr:1", "TEST"));
      Model model = new Model("person", fields);
      System.out.println(model);
      System.out.println(model.getJsonObject());
      List<NameValuePair> params = new ArrayList<>();
      // params.add(new BasicNameValuePair("authority", "dve*"));
      AuthToken token = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);
      params.add(new BasicNameValuePair("auth_token", token.getToken()));
      params.add(new BasicNameValuePair("nonce", token.getNonce()));
      params.add(new BasicNameValuePair("submission",
	    model.getJsonObject().toString()));
      String response = indiciaHttpConnector
	    .doHttpPostRequest("/index.php/services/data/save", params);
      System.out.println(response);
   }

   @Test
   void testSaveUserDirect() throws Exception
   {
      Set<Field> fields = new HashSet<>();
      // fields.add(new Field("website_id", "2"));
      fields.add(new Field("username", "a.spannenberg"));
      fields.add(new Field("person_id", "314"));
      fields.add(new Field("core_role_id", "null"));
      // fields.add(new Field("id", ""));
      // fields.add(new Field("taxa_taxon_list_id:taxon", "\"Moos\" spec."));
      // fields.add(new Field("sample_id", "5471"));
      Model model = new Model("user", fields);
      System.out.println(model);
      System.out.println(model.getJsonObject());
      List<NameValuePair> params = new ArrayList<>();
      // params.add(new BasicNameValuePair("authority", "dve*"));
      AuthToken token = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);
      params.add(new BasicNameValuePair("auth_token", token.getToken()));
      params.add(new BasicNameValuePair("nonce", token.getNonce()));
      params.add(new BasicNameValuePair("submission",
	    model.getJsonObject().toString()));
      String response = indiciaHttpConnector
	    .doHttpPostRequest("/index.php/services/data/save", params);
      System.out.println(response);

   }

   @Test
   void testSaveOccurenceDirect() throws Exception
   {
      Set<Field> fields = new HashSet<>();
      fields.add(new Field("website_id", "2"));
      fields.add(new Field("record_status", "C"));
      fields.add(new Field("taxa_taxon_list_id", "24509"));
      // fields.add(new Field("taxa_taxon_list_id:taxon", "\"Moos\" spec."));
      fields.add(new Field("sample_id", "5471"));
      Model model = new Model("occurrence", fields);
      System.out.println(model);
      System.out.println(model.getJsonObject());
      List<NameValuePair> params = new ArrayList<>();
      // params.add(new BasicNameValuePair("authority", "dve*"));
      AuthToken token = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);
      params.add(new BasicNameValuePair("auth_token", token.getToken()));
      params.add(new BasicNameValuePair("nonce", token.getNonce()));
      params.add(new BasicNameValuePair("submission",
	    model.getJsonObject().toString()));
      String response = indiciaHttpConnector
	    .doHttpPostRequest("/index.php/services/data/save", params);
      System.out.println(response);

   }

   @Test
   void testSaveSampleDirect() throws Exception
   {
      Set<Field> fields = new HashSet<>();
      // fields.add(new Field("id", "406644"));
      fields.add(new Field("website_id", "1"));
      fields.add(new Field("record_status", "C"));
      fields.add(new Field("date", "2011-09"));
      fields.add(new Field("entered_sref", "9 9"));
      fields.add(new Field("geom", ""));
      fields.add(new Field("entered_sref_system", "4326"));
      fields.add(new Field("survey_id", "2"));
      fields.add(new Field("comment", "updated via java bla"));
      fields.add(new Field("smpAttr:23", "20"));
      fields.add(new Field("updated_by_id", "4"));
      fields.add(new Field("recorder_names", "Daniel\nFrank"));
      fields.add(new Field("sample_method_id", "21"));
      fields.add(new Field("external_key", "chara#4711"));

      // fields.add(new Field("loaction_name", "Ein ort in der Welt"));
      // fields.add(new Field("location_id", 373));
      Model model = new Model("sample", fields);

      System.out.println(model);
      System.out.println(model.getJsonObject());
      List<NameValuePair> params = new ArrayList<>();
      // params.add(new BasicNameValuePair("authority", "dve*"));
      AuthToken token = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);
      params.add(SharingOption.REPORTING.asRequestParameter());
      params.add(new BasicNameValuePair("auth_token", token.getToken()));
      params.add(new BasicNameValuePair("nonce", token.getNonce()));
      params.add(new BasicNameValuePair("submission",
	    model.getJsonObject().toString()));
      String response = indiciaHttpConnector
	    .doHttpPostRequest("/index.php/services/data/save", params);
      System.out.println(response);
   }

   @Test
   void testGetUserId() throws JSONException
   {
      List<NameValuePair> params = new ArrayList<>();
      JSONObject email = new JSONObject();
      email.put("type", "email");
      email.put("identifier", "xxxxxx@web.de");
      JSONObject ldap = new JSONObject();
      ldap.put("type", "ldap");
      ldap.put("identifier", "ou=xxx");
      JSONArray identifieres = new JSONArray();
      identifieres.put(email);
      identifieres.put(ldap);
      System.out.println(identifieres.toString(2));
      AuthToken token = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);
      params.add(new BasicNameValuePair("auth_token", token.getToken()));
      params.add(new BasicNameValuePair("nonce", token.getNonce()));
      params.add(
	    new BasicNameValuePair("identifiers", identifieres.toString()));
      params.add(new BasicNameValuePair("surname", "xxxxxxxxx"));
      params.add(new BasicNameValuePair("cms_user_id", "99"));
      String response = indiciaHttpConnector.doHttpPostRequest(
	    "/index.php/services/user_identifier/get_user_id", params);
      System.out.println(response);
   }

   @Test
   void testMultiPartPost()
   {
      String service = "services/data/handle_media";

      InputStream is = getClass().getResourceAsStream("/black.png");
      InputStreamBody isb = new InputStreamBody(is, "black.png");

      Map<String, ContentBody> parts = new HashMap<>();
      parts.put("media_upload", isb);

      String response = indiciaHttpConnector
            .doHttpMulitPartPost("/index.php/" + service, parts, WEBSITE_ID);

      LOGGER.info("Fiele uploaded. File name on server: " + response);
   }

   @Test
   void testGetReportCustomAttribute()
   {
      Map<String, Object> params = new HashMap<>();
      params.put("limit", 5);
      params.put("parent_id", 4275);
      params.put("survey_website_id", 3);
      params.put("title", "%%");
      params.put("srvattrs", "1,2");
      params.put("attr_survey_2", 1);

      List<NameValuePair> postParams = new ArrayList<>();
      postParams.add(new BasicNameValuePair("report", "/uni-greifswald/findSurveys.xml"));
      postParams.add(new BasicNameValuePair("reportSource",
	    ReportSource.local.toString()));
      JSONObject json = new JSONObject(params);
      LOGGER.debug(json.toString());
      postParams.add(new BasicNameValuePair("params", json.toString()));
      postParams.add(new BasicNameValuePair("mode", Mode.xml.toString()));
      String report = indiciaHttpConnector.doHttpPostRequestRO(
            "/index.php/services/report/requestReport", postParams, WEBSITE_ID);
      System.out.println(report);
   }
}
