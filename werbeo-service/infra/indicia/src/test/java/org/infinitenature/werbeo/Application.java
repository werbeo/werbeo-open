package org.infinitenature.werbeo;

import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.QueryDslJpaEnhancedRepositoryImpl;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackageClasses = { Application.class,
      Jsr310JpaConverters.class })
@EnableJpaRepositories(repositoryBaseClass = QueryDslJpaEnhancedRepositoryImpl.class, basePackageClasses = OccurrenceRepository.class)
public class Application
{

}
