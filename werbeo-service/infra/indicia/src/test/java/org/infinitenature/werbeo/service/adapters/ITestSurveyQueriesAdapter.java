package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestSurveyQueriesAdapter extends IntegrationTest
{

   @Autowired
   private SurveyQueriesAdapter surveyQueriesAdapter;

   @Test
   public void testLoadSurvey() {
      Survey survey = surveyQueriesAdapter.load(
            7, // online eingabe
            createContext(PortalTestUtil.FLORA_MV_ID, null));
      assertThat(survey.getName(), is("DE-MV-Onlineeingabe"));
   }

   @Test
   public void testLoadSurvey_wrongPortal()
   {
      Survey survey = surveyQueriesAdapter.load(
            65, // online eingabe RP otoptera
            createContext(PortalTestUtil.FLORA_MV_ID, null));
      assertThat(survey, is(nullValue()));
   }

   @Test
   public void testFindSurveys_RestService()
   {
      User user = null;
      Slice<Survey, SurveySortField> findSurveys = surveyQueriesAdapter
            .findSurveys("", null, null,
                  createContext(PortalTestUtil.REST_SRV_ID, user),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        SurveySortField.ID));
      assertThat(findSurveys.getNumberOfElements(), is(9));

   }

   @Test
   public void testFindSurveys_vegetweb()
   {
      Slice<Survey, SurveySortField> findSurveys = surveyQueriesAdapter
            .findSurveys("", null, null,
                  createContext(PortalTestUtil.VEGETWEB_ID, null),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        SurveySortField.ID));
      assertThat(findSurveys.getNumberOfElements(), is(4));

   }

   @Test
   public void testCountSurveys_restService()
   {
      assertThat(surveyQueriesAdapter.countSurveys(null, "", null,
            createContext(PortalTestUtil.REST_SRV_ID, null)), is(9l));
   }
   @Test
   public void testCountSurveys_byTitle()
   {
      assertThat(
            surveyQueriesAdapter.countSurveys(null, "MV-onlineEingabe", null,
            createContext(PortalTestUtil.REST_SRV_ID, null)), is(1l));
   }

   @Test
   public void testCountSurveys_noMatch()
   {
      assertThat(
            surveyQueriesAdapter.countSurveys(null, "ABCDEFGHIJKLMNOP", null,
            createContext(PortalTestUtil.REST_SRV_ID, null)), is(0l));
   }

   @Test
   public void testFindSurveys_noMatch()
   {
      Slice<Survey, SurveySortField> findSurveys = surveyQueriesAdapter
            .findSurveys(null, "ABCDEFGHIJKLMNOP", null,
                  createContext(PortalTestUtil.REST_SRV_ID, null),
                  new OffsetRequestImpl<>(0, 10, SortOrder.ASC,
                        SurveySortField.ID));
      assertThat(findSurveys.getNumberOfElements(), is(0));
   }
}
