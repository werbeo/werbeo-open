package org.infinitenature.werbeo.service.adapters;



import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.infinitenature.herbariorum.client.HerbariorumGermanClient;
import org.junit.jupiter.api.Test;

public class ITestIndexHerbariorum extends IntegrationTest
{
   @Test
   public void testIndexHerbariorumClient()
   {
      HerbariorumGermanClient client = new HerbariorumGermanClient();
      assertThat("Index Herbariorum Cache must not be empty", client.initCache(), is(true));
   }
}
