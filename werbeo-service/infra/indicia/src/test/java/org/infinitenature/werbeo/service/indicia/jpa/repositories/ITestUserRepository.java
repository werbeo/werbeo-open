package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Optional;

import org.infinitenature.werbeo.service.adapters.IntegrationTest;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBUser;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ITestUserRepository extends IntegrationTest
{
   @Autowired
   private UserRepository repoUT;

   @DisplayName("Found user by username")
   @Test
   void test001()
   {
      DBUser user = repoUT.findOneByUsername("admin").get();
      assertThat(user.getId(), is(1));
   }

   @DisplayName("Not found user by username")
   @Test
   void test002()
   {
      Optional<DBUser> user = repoUT.findOneByUsername("admin111");
      assertThat(user.isPresent(), is(false));
   }
}
