package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Position;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestSampleSubmissionMapper
{
   private SampleSubmissionMapper ssmUT;
   @BeforeEach
   void setUp() throws Exception
   {
      ssmUT = new SampleSubmissionMapperImpl();
   }

   @Test
   void test()
   {
      Sample sample = new Sample();
      Position position = new Position();
      position.setPosCenter(new double[] { 12.1429820, 54.0955440 });
      position.setEpsg(4326);
      sample.setLocality(new Locality());
      sample.getLocality().setPosition(position);

      assertThat(ssmUT.createEnteredSrefString(sample),
            is("54.095544, 12.142982"));
   }

}
