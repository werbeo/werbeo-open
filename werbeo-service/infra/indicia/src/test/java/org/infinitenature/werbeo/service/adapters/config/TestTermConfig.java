package org.infinitenature.werbeo.service.adapters.config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Area;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTerm;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTermlist;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTermlistTerm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestTermConfig
{
   private TermConfig termConfigUT;

   @BeforeEach
   void setUp() throws Exception
   {
      DBTermlist amountTermlist = new DBTermlist();
      amountTermlist.getTermListTerms()
            .add(new DBTermlistTerm(144, new DBTerm(141, "1 (Einzelfund)")));
      amountTermlist.getTermListTerms()
            .add(new DBTermlistTerm(145, new DBTerm(142, "2-10 (wenige)")));
      amountTermlist.getTermListTerms()
            .add(new DBTermlistTerm(146, new DBTerm(143, "11-100 (viele)")));
      amountTermlist.getTermListTerms()
            .add(new DBTermlistTerm(147, new DBTerm(144, "100+ (sehr Viele)")));

      DBTermlist statusTermlist = new DBTermlist();
      statusTermlist.getTermListTerms()
            .add(new DBTermlistTerm(141, new DBTerm(138, "Wildvorkommen")));
      statusTermlist.getTermListTerms()
            .add(new DBTermlistTerm(142, new DBTerm(139, "Ansalbung")));
      statusTermlist.getTermListTerms()
            .add(new DBTermlistTerm(143, new DBTerm(140, "Pflanzung")));

      DBTermlist areaTermlist = new DBTermlist();
      areaTermlist.getTermListTerms()
            .add(new DBTermlistTerm(148, new DBTerm(145, "<1")));
      areaTermlist.getTermListTerms()
            .add(new DBTermlistTerm(149, new DBTerm(146, "1-5")));
      areaTermlist.getTermListTerms()
            .add(new DBTermlistTerm(150, new DBTerm(147, "6-25")));

      DBTermlist originmTermlist = new DBTermlist();
      originmTermlist.getTermListTerms()
            .add(new DBTermlistTerm(151, new DBTerm(148, "indigen")));
      originmTermlist.getTermListTerms()
            .add(new DBTermlistTerm(152, new DBTerm(149, "kultiviert")));

      termConfigUT = new TermConfig(amountTermlist, statusTermlist,
            areaTermlist, originmTermlist, new DBTermlist(), new DBTermlist(),
            new DBTermlist(), new DBTermlist(), new DBTermlist(),
            new DBTermlist(), new DBTermlist(), new DBTermlist(),
            new DBTermlist(), new DBTermlist(), new DBTermlist(),
            new DBTermlist(), new DBTermlist());
   }

   @Test
   void testGetIdAmount()
   {
      assertThat(termConfigUT.getId(Amount.SOME), is(146));
      assertThat(termConfigUT.getId(Amount.A_LOT), is(147));
   }

   @Test
   void testGetIdStatus()
   {
      assertThat(termConfigUT.getId(SettlementStatus.WILD), is(141));
      assertThat(termConfigUT.getId(SettlementStatus.PLANTED), is(143));
   }

   @Test
   void testGetAmount()
   {
      assertThat(termConfigUT.getAmount(144), is(Amount.ONE));
      assertThat(termConfigUT.getAmount(145), is(Amount.FEW));
   }

   @Test
   void testGetStatus()
   {
      assertThat(termConfigUT.getStatus(142), is(SettlementStatus.RESETTELD));
      assertThat(termConfigUT.getStatus(143), is(SettlementStatus.PLANTED));
   }

   @Test
   void testGetArea()
   {
      assertThat(termConfigUT.getArea(150), is(Area.SIX_TO_TWENTYFIVE));
   }

   @Test
   void testGetIdArea()
   {
      assertThat(termConfigUT.getId(Area.ONE_TO_FIVE), is(149));
   }
}
