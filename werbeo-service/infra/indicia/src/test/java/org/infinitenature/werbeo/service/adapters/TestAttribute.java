package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestAttribute
{
   class Entry implements java.util.Map.Entry<String, String>
   {
      private final String key;
      private final String value;

      public Entry(String key, String value)
      {
         super();
         this.key = key;
         this.value = value;
      }

      @Override
      public String setValue(String value)
      {
         // NOOP
         return null;
      }

      @Override
      public String getKey()
      {
         return key;
      }

      @Override
      public String getValue()
      {
         return value;
      }

   }

   @DisplayName("New attribute")
   @Test
   void test_001()
   {
      Attribute attribute = new Attribute(new Entry("1", "A"));
      assertThat(attribute.getAttributeId(), is("1"));
      assertThat(attribute.getValue(), is("A"));
      assertThat(attribute.getIndex(), is(""));
      assertThat(attribute.getValueId(), is(""));
      assertThat(attribute.getKey(), is("1"));
   }

   @DisplayName("Existing attribute")
   @Test
   void test_002()
   {
      Attribute attribute = new Attribute(new Entry("1:1", "A"));
      assertThat(attribute.getAttributeId(), is("1"));
      assertThat(attribute.getValue(), is("A"));
      assertThat(attribute.getIndex(), is(""));
      assertThat(attribute.getValueId(), is("1"));
      assertThat(attribute.getKey(), is("1:1"));
   }

   @DisplayName("New mulitvalue attribute")
   @Test
   void test_003()
   {
      Attribute attribute = new Attribute(new Entry("1::1", "A"));
      assertThat(attribute.getAttributeId(), is("1"));
      assertThat(attribute.getValue(), is("A"));
      assertThat(attribute.getIndex(), is("1"));
      assertThat(attribute.getValueId(), is(""));
      assertThat(attribute.getKey(), is("1::1"));
   }

}
