package org.infinitenature.werbeo.service.indicia.jpa.mappers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Iterator;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

class TestOccurrenceRequestMapperIndicia
{

   private OccurrenceRequestMapperIndicia requestMapperUT;

   @BeforeEach
   void setUp() throws Exception
   {
      requestMapperUT = new OccurrenceRequestMapperIndiciaImpl();
   }

   @Test
   @DisplayName("Sort for determiner")
   void test001()
   {
      Sort sort = requestMapperUT.mapSort(new OffsetRequestImpl<>(0, 10,
            SortOrder.ASC, OccurrenceSortField.DETERMINER));

      Iterator<Order> orders = sort.iterator();
      Order order = orders.next();
      assertThat(order.getProperty(), is("determiner.lastName"));
      assertThat(order.getDirection(), is(Direction.ASC));

      order = orders.next();
      assertThat(order.getProperty(), is("determiner.firstName"));
      assertThat(order.getDirection(), is(Direction.ASC));
   }

   @Test
   @DisplayName("sort for date")
   void test002()
   {
      Sort sort = requestMapperUT.mapSort(new OffsetRequestImpl<>(0, 10,
            SortOrder.DESC, OccurrenceSortField.DATE));

      Iterator<Order> orders = sort.iterator();
      Order order = orders.next();
      assertThat(order.getProperty(), is("sample.startDate"));
      assertThat(order.getDirection(), is(Direction.DESC));

      order = orders.next();
      assertThat(order.getProperty(), is("sample.endDate"));
      assertThat(order.getDirection(), is(Direction.DESC));
   }

}
