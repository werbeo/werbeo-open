package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.UUID;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.infra.commands.MediaCommands;
import org.infinitenature.werbeo.service.core.api.infra.commands.SampleCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.OccurrenceQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.SampleQueries;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ITestMediaCommandsAdapter extends IntegrationTest
{
   @Autowired
   private SampleCommands sampleCommands;
   @Autowired
   private SampleQueries sampleQueries;
   @Autowired
   private MediaCommands mediaCommands;
   @Autowired
   private OccurrenceQueries occurrenceQueries;

   @BeforeEach
   void setUp()
   {
      context = createContext(PortalTestUtil.DEMO_ID,
            getUser(UserTestUtil.USER_LOGIN_A));
   }

   @Test
   @DisplayName("Test attach media")
   void test001()
   {
      Sample sample = createSample();

      UUID savedSampleUUID = sampleCommands.saveOrUpdate(sample, context);

      Sample savedSample = sampleQueries.get(savedSampleUUID, context).get();

      UUID occurrenceId = savedSample.getOccurrences().get(0).getId();

      mediaCommands.addOccurrenceMedia(occurrenceId, "/abc",
            "A description for a media file which does not exist",
            MediaType.IMAGE_LINK, context);

      Occurrence occurrenceWithMedia = occurrenceQueries.get(occurrenceId,
            context);

      assertThat(occurrenceWithMedia.getMedia(), hasSize(1));
      Medium medium = occurrenceWithMedia.getMedia().get(0);
      assertAll(
            () -> assertThat(medium.getDescription(),
                  is("A description for a media file which does not exist")),
            () -> assertThat(medium.getType(), is(MediaType.IMAGE_LINK)),
            () -> assertThat(medium.getPath(), is("/abc")));
   }

   @Test
   @DisplayName("Delete media attechment")
   void test002()
   {
      Sample sample = createSample();

      UUID savedSampleUUID = sampleCommands.saveOrUpdate(sample, context);

      Sample savedSample = sampleQueries.get(savedSampleUUID, context).get();

      UUID occurrenceId = savedSample.getOccurrences().get(0).getId();

      mediaCommands.addOccurrenceMedia(occurrenceId, "/abc",
            "A description for a media file which does not exist",
            MediaType.IMAGE_LINK, context);

      Occurrence occurrenceWithMedia = occurrenceQueries.get(occurrenceId,
            context);

      Medium mediumToDelete = occurrenceWithMedia.getMedia().get(0);

      mediaCommands.deleteOccurrenceMedium(occurrenceId, mediumToDelete,
            context);

      Occurrence occurrenceWithoutMedia = occurrenceQueries.get(occurrenceId,
            context);

      assertThat(occurrenceWithoutMedia.getMedia(), hasSize(0));
   }
}
