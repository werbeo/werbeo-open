package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestPersonQueriesAdapter extends IntegrationTest
{
   @Autowired
   private PersonQueriesAdapter personQueriesAdapter;

   @Test
   public void testLoad()
   {
      Person person = personQueriesAdapter.get(70, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.LOGIN_CTOMALAA)));
      assertThat(person.getFirstName(), is("Gertruda"));
      assertThat(person.getLastName(), is("Lintin"));
   }

   @Test
   public void testFindValidator()
   {
      PersonFilter personFilter = new PersonFilter().setValidator(true);
      Slice<Person, PersonSortField> validators = personQueriesAdapter.find(
            personFilter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.LOGIN_CTOMALAA)),
            new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                  PersonSortField.ID));

      assertAll(() -> assertThat(validators.getContent().size(), is(1)),
            () -> assertThat(validators.getContent().get(0).getEmail(),
                  is("ctomalaa@ucoz.com")));
   }

   @Test
   public void testFindValidator_wrongPortal()
   {
      PersonFilter personFilter = new PersonFilter().setValidator(true);
      Slice<Person, PersonSortField> validators = personQueriesAdapter.find(
            personFilter,
            createContext(PortalTestUtil.HEUSCHRECKEN_D_ID,
                  getUser(UserTestUtil.LOGIN_CTOMALAA)),
            new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                  PersonSortField.ID));

      assertThat(validators.getContent().size(), is(0));
   }

   @Test
   public void testFind()
   {
      PersonFilter filter = new PersonFilter("ke");
      Slice<Person, PersonSortField> people = personQueriesAdapter.find(filter,
            createContext(PortalTestUtil.FLORA_MV_ID,
                  getUser(UserTestUtil.LOGIN_CTOMALAA)),
            new OffsetRequestImpl<>(0, Integer.MAX_VALUE, SortOrder.ASC,
                  PersonSortField.ID));

      assertThat(people.getContent().size(), is(26));
   }

   @Test
   public void testCount_nameContains()
   {
      PersonFilter filter = new PersonFilter("ke");
      assertThat(personQueriesAdapter.count(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.LOGIN_CTOMALAA))),
            is(26l));
   }

   @Test
   public void testCount_firstName()
   {
      PersonFilter filter = new PersonFilter().setFirstName("Gunner");
      assertThat(personQueriesAdapter.count(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.LOGIN_CTOMALAA))),
            is(2l));
   }

   @Test
   public void testCount_fullName()
   {
      PersonFilter filter = new PersonFilter().setFirstName("Gunner")
            .setLastName("Shaw");
      assertThat(personQueriesAdapter.count(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.LOGIN_CTOMALAA))),
            is(1l));
   }

   @Test
   public void testCount_externalKey_match()
   {
      PersonFilter filter = new PersonFilter().setExternalKey("p1");
      assertThat(personQueriesAdapter.count(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.LOGIN_CTOMALAA))),
            is(5l));
   }

   @Test
   public void testCount_externalKey_caseMismatch()
   {
      PersonFilter filter = new PersonFilter().setExternalKey("P1");
      assertThat(personQueriesAdapter.count(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.LOGIN_CTOMALAA))),
            is(0l));
   }

   @Test
   public void testCount_externalKey_onlyPartialmach()
   {
      PersonFilter filter = new PersonFilter().setExternalKey("p");
      assertThat(personQueriesAdapter.count(filter, createContext(
            PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.LOGIN_CTOMALAA))),
            is(0l));
   }
}
