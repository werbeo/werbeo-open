package org.infinitenature.werbeo.service.indicia.http;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaOccurrence;
import org.infinitenature.werbeo.service.indicia.http.types.IndiciaSample;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceComment;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceMedium;
import org.infinitenature.werbeo.service.indicia.http.types.OccurrenceSubmissionContainer;
import org.infinitenature.werbeo.service.indicia.http.types.SampleComment;
import org.infinitenature.werbeo.service.indicia.http.types.SampleMedium;
import org.infinitenature.werbeo.service.indicia.http.types.SampleSubmissionContainer;

/**
 * Methods for creating objects for testing the indicia api.
 *
 * @author dve
 *
 */
public class TestHelper
{
   private final int websiteId;
   private final int surveyId;

   public TestHelper(int websiteId, int surveyId)
   {
      this.websiteId = websiteId;
      this.surveyId = surveyId;
   }

   public IndiciaSample createSample(VagueDate date)
   {
      IndiciaSample sample = new IndiciaSample();
      sample.setWebsiteId(websiteId);
      sample.setDate(date.toString());
      sample.setEnteredSref("1846/1");
      sample.setEnteredSrefSystem("mtbqqq");
      sample.setSurveyId(surveyId);

      sample.setComment("Commited via the indica java api");
      sample.setRecorderNames("Justus Jonas\nPeter Shaw\nBob Andrews");
      return sample;
   }

   public SampleSubmissionContainer createSampleSubmission(VagueDate date,
	 int... taxaTaxonListIds)
   {
      Set<SampleComment> comments = new HashSet<>();
      comments.add(new SampleComment("Created by a junit test."));

      Map<String, String> additionAtributes = new HashMap<>();
      additionAtributes.put("27", UUID.randomUUID().toString());

      Set<OccurrenceSubmissionContainer> occurrenceSubmissionContainers = new HashSet<>();
      for (int taxaTaxonListId : taxaTaxonListIds)
      {
	 occurrenceSubmissionContainers
	       .add(createOccurrenceSubmissionContainer(taxaTaxonListId));
      }
      SampleSubmissionContainer container = new SampleSubmissionContainer(
	    createSample(date), additionAtributes, comments,
	    occurrenceSubmissionContainers, new HashSet<SampleMedium>());
      return container;
   }

   public OccurrenceSubmissionContainer createOccurrenceSubmissionContainer(
	 int taxaTaxonListId)
   {
      IndiciaOccurrence occurrence = createOccurrence(taxaTaxonListId);
      Map<String, String> additionAtributes = new HashMap<>();
      additionAtributes.put("10", UUID.randomUUID().toString());

      Set<OccurrenceComment> comments = new HashSet<>();
      comments.add(new OccurrenceComment("Created by a junit test."));
      OccurrenceSubmissionContainer occurrenceSubmissionContainer = new OccurrenceSubmissionContainer(
	    occurrence, additionAtributes, comments, new HashSet<OccurrenceMedium>());
      return occurrenceSubmissionContainer;
   }

   private IndiciaOccurrence createOccurrence(int taxaTaxonListId)
   {
      IndiciaOccurrence occurrence = new IndiciaOccurrence();
      occurrence.setTaxaTaxonListId(taxaTaxonListId);
      occurrence.setWebsiteId(websiteId);
      occurrence.setRecordStatus(IndiciaOccurrence.COMPLETED);
      return occurrence;
   }

}
