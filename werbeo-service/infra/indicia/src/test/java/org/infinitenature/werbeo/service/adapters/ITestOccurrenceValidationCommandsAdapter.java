package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.UUID;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Validation;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceValidationCommands;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ITestOccurrenceValidationCommandsAdapter extends IntegrationTest
{
   @Autowired
   private OccurrenceValidationCommands occurrenceValidationCommands;
   @Autowired
   private SampleCommandsAdapter sampleCommandsAdapter;
   @Autowired
   private SampleQueriesAdapter sampleQueriesAdapter;

   @BeforeEach
   public void setUp()
   {
      context = createContext(PortalTestUtil.DEMO_ID,
            getUser(UserTestUtil.USER_LOGIN_A));
   }

   @Test
   @DisplayName("Set validation status")
   void test_001()
   {
      UUID uuid = sampleCommandsAdapter.saveOrUpdate(createSample(), context);
      Sample sample = sampleQueriesAdapter.get(uuid, context).get();

      Occurrence occurrence = sample.getOccurrences().get(0);
      UUID occId = occurrence.getId();

      assertThat(occurrence.getValidation(), is(nullValue()));

      occurrenceValidationCommands.setValidationStatus(occId,
            new Validation(ValidationStatus.VALID, "A comment"), context);

      Occurrence loaded = sampleQueriesAdapter.get(uuid, context).get()
            .getOccurrences().stream().filter(occ -> occ.getId().equals(occId))
            .findFirst().get();

      assertAll(
            () -> assertThat(loaded.getValidation().getStatus(),
                  is(ValidationStatus.VALID)),
            () -> assertThat(loaded.getValidation().getComment(),
                  is("A comment")),
            () -> assertThat(loaded.getValidation().getCreatedBy().getLogin(),
                  is(UserTestUtil.USER_LOGIN_A)));
   }

   @Test
   @DisplayName("Change Validation Status")
   void test_002()
   {

      UUID uuid = sampleCommandsAdapter.saveOrUpdate(createSample(), context);
      Sample sample = sampleQueriesAdapter.get(uuid, context).get();

      Occurrence occurrence = sample.getOccurrences().get(0);
      UUID occId = occurrence.getId();

      assertThat(occurrence.getValidation(), is(nullValue()));

      occurrenceValidationCommands.setValidationStatus(occId,
            new Validation(ValidationStatus.VALID), context);

      occurrenceValidationCommands.setValidationStatus(occId,
            new Validation(ValidationStatus.INVALID), createContext(
                  PortalTestUtil.DEMO_ID,
                  getUser(UserTestUtil.USER_LOGIN_B)));

      occurrence = sampleQueriesAdapter.get(uuid, context).get()
            .getOccurrences().stream().filter(occ -> occ.getId().equals(occId))
            .findFirst().get();

      assertThat(occurrence.getValidation().getStatus(),
            is(ValidationStatus.INVALID));
      assertThat(occurrence.getValidation().getCreatedBy().getLogin(),
            is(UserTestUtil.USER_LOGIN_A));
      assertThat(occurrence.getValidation().getModifiedBy().getLogin(),
            is(UserTestUtil.USER_LOGIN_B));
   }
}
