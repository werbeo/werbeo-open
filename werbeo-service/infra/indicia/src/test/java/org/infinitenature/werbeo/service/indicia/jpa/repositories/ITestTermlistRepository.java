package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import javax.transaction.Transactional;

import org.infinitenature.werbeo.service.adapters.IntegrationTest;
import org.infinitenature.werbeo.service.adapters.config.IndiciaConstans;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTermlist;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestTermlistRepository extends IntegrationTest
{
   @Autowired
   private TermlistRepository termlistRepositoryUT;

   @Test
   @Transactional
   public void test_findMenge()
   {
      DBTermlist termlist = termlistRepositoryUT
            .findOneByTitleAndDeletedIsFalse("Menge");

      assertThat(termlist.getTermListTerms().size(), is(4));

      assertThat(
            termlist.getTermListTerms().stream()
                  .filter(term -> term.getTerm().getTerm()
                        .equals(IndiciaConstans.AMOUNT_ONE_TITLE))
                  .count(),
            is(1L));
      assertThat(
            termlist.getTermListTerms().stream()
                  .filter(term -> term.getTerm().getTerm()
                        .equals(IndiciaConstans.AMOUNT_FEW_TITLE))
                  .count(),
            is(1L));
      assertThat(
            termlist.getTermListTerms().stream()
                  .filter(term -> term.getTerm().getTerm()
                        .equals(IndiciaConstans.AMOUNT_SOME_TITLE))
                  .count(),
            is(1L));
      assertThat(
            termlist.getTermListTerms().stream()
                  .filter(term -> term.getTerm().getTerm()
                        .equals(IndiciaConstans.AMOUNT_A_LOT_TITLE))
                  .count(),
            is(1L));
   }

   @Test
   @Transactional
   public void test_findStatus()
   {
      DBTermlist termlist = termlistRepositoryUT
            .findOneByTitleAndDeletedIsFalse("Status");

      assertThat(termlist.getTermListTerms().size(), is(3));

      assertThat(
            termlist.getTermListTerms().stream()
                  .filter(term -> term.getTerm().getTerm()
                        .equals(IndiciaConstans.STATUS_WILD_TITLE))
                  .count(),
            is(1L));
      assertThat(termlist.getTermListTerms().stream()
            .filter(term -> term.getTerm().getTerm()
                  .equals(IndiciaConstans.STATUS_RESETTELD_TITLE))
            .count(), is(1L));
      assertThat(
            termlist.getTermListTerms().stream()
                  .filter(term -> term.getTerm().getTerm()
                        .equals(IndiciaConstans.STATUS_PLANTED_TITLE))
                  .count(),
            is(1L));
   }
}
