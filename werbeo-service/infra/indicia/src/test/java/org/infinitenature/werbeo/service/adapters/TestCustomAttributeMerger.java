package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestCustomAttributeMerger
{

   @Test
   @DisplayName("Only new Attributes")
   void test_001()
   {
      Map<String, String> newAttributes = new HashMap<>();

      newAttributes.put("1", "A");
      newAttributes.put("2::1", "B");
      newAttributes.put("2::2", "C");

      Map<String, String> existingAttriubtes = Collections.emptyMap();

      Map<String, String> mergedAttributes = CustomAttributeMerger
            .merge(newAttributes, existingAttriubtes);

      assertThat(mergedAttributes.size(), is(3));
      assertThat(mergedAttributes.get("1"), is("A"));
      assertThat(mergedAttributes.get("2::1"), is("B"));
      assertThat(mergedAttributes.get("2::2"), is("C"));
   }

   @Test
   @DisplayName("Delete a value")
   void test_002()
   {
      Map<String, String> newAttributes = new HashMap<>();
      newAttributes.put("1", "A");
      newAttributes.put("2::1", "B");

      Map<String, String> existingAttriubtes = new HashMap<>();
      existingAttriubtes.put("1:44", "A");
      existingAttriubtes.put("2:45", "B");
      existingAttriubtes.put("2:46", "C");

      Map<String, String> mergedAttributes = CustomAttributeMerger
            .merge(newAttributes, existingAttriubtes);

      assertThat(mergedAttributes.size(), is(3));
      assertThat(mergedAttributes.get("1:44"), is("A"));
      assertThat(mergedAttributes.get("2:45"), is("B"));
      assertThat(mergedAttributes.get("2:46"), is(""));
   }

   @Test
   @DisplayName("Modify a value")
   void test_003()
   {
      Map<String, String> newAttributes = new HashMap<>();
      newAttributes.put("1", "A");
      newAttributes.put("2::1", "B");
      newAttributes.put("2::2", "D");

      Map<String, String> existingAttriubtes = new HashMap<>();
      existingAttriubtes.put("1:44", "A");
      existingAttriubtes.put("2:45", "B");
      existingAttriubtes.put("2:46", "C");

      Map<String, String> mergedAttributes = CustomAttributeMerger
            .merge(newAttributes, existingAttriubtes);

      assertThat(mergedAttributes.size(), is(3));
      assertThat(mergedAttributes.get("1:44"), is("A"));
      assertThat(mergedAttributes.get("2:45"), is("B"));
      assertThat(mergedAttributes.get("2:46"), is("D"));
   }
}
