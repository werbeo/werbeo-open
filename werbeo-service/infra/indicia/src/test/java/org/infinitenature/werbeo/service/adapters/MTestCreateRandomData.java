package org.infinitenature.werbeo.service.adapters;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class MTestCreateRandomData extends IntegrationTest
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(MTestCreateRandomData.class);
   @Autowired
   private SurveyCommandAdapter surveyCommands;
   @Autowired
   private RandomSampleGenerator randomSampleGenerator;
   @Autowired
   private SampleCommandsAdapter adapter;
   @Autowired
   private TaxonQueriesAdapter taxonQueries;

   @Test
   public void createRasterKarierung()
   {
      Context context = createContext(PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A));
      Survey survey = new Survey();
      survey.setAvailability(Availability.FREE);
      survey.setName("DE-MV-Freie Daten");

      int surveyId = surveyCommands.saveOrUpdate(survey, context);
      survey.setId(surveyId);
      List<Sample> samples = randomSampleGenerator.generateSamples(20,
            4,
            survey, createMTBQs("1642", "1643", "1644", "1943", "1944", "2043",
                  "2044", "2136", "2137"));

      for (Sample sample : samples)
      {
         try
         {
            adapter.saveOrUpdate(sample, context);
         } catch (Exception e)
         {
            LOGGER.error("Failure saving sample {}", sample, e);
         }
      }

   }


   @Test
   public void testGetTaxon() throws Exception
   {
      Context context = createContext(PortalTestUtil.FLORA_MV_ID, getUser(UserTestUtil.USER_LOGIN_A));
      Taxon taxon = taxonQueries.get(19104, context);
      System.out.println();
   }

   private String[] createMTBQs(String... mtbs)
   {
      Set<String> mtbqs = new HashSet<>();

      for (String mtb : mtbs)
      {
         mtbqs.add(mtb + "/1");
         mtbqs.add(mtb + "/2");
         mtbqs.add(mtb + "/3");
         mtbqs.add(mtb + "/4");
      }
      return mtbqs.toArray(new String[0]);
   }
}
