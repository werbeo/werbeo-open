package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

import java.util.Set;

import org.infinitenature.werbeo.service.adapters.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ITestTaxonRepository extends IntegrationTest
{
   @Autowired
   private TaxonRepository taxonRepositoryUT;

   @Test
   @Transactional
   public void testFindChildTaxa_AbiesSpec()
   {
      Set<Integer> abiesSpecChildMeaningIds = taxonRepositoryUT
            .findChildTaxonMeaningIds(5930);

      assertThat(abiesSpecChildMeaningIds.size(), is(5));
      assertThat(abiesSpecChildMeaningIds,
            is(containsInAnyOrder(5769, 5767, 5766, 5770, 5768)));
   }

   @Test
   @Transactional
   public void testFindChildTaxa_AbiesNordmanniana()
   {
      Set<Integer> abiesNordmannianaChildMeaningIds = taxonRepositoryUT
            .findChildTaxonMeaningIds(5936);

      assertThat(abiesNordmannianaChildMeaningIds.size(), is(1));
      assertThat(abiesNordmannianaChildMeaningIds,
            is(containsInAnyOrder(5770)));
   }

   @Test
   @Transactional
   public void testFindParentTaxa_GruenlichesEtwas()
   {
      Set<Integer> someThingGreenParentMeaningIds = taxonRepositoryUT
            .findParentTaxonMeaningIds(1);

      assertThat(someThingGreenParentMeaningIds.size(), is(1));
      assertThat(someThingGreenParentMeaningIds, is(containsInAnyOrder(1)));
   }

   @Test
   @Transactional
   public void testFindParentTaxonMeaningIds_Rhodophyta()
   {
      Set<Integer> rhodophytaParentMeaningIds = taxonRepositoryUT
            .findParentTaxonMeaningIds(227);
      assertThat(rhodophytaParentMeaningIds.size(), is(3));
      assertThat(rhodophytaParentMeaningIds, is(containsInAnyOrder(1, 2, 225)));
   }
}
