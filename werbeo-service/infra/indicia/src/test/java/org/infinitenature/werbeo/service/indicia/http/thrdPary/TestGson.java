package org.infinitenature.werbeo.service.indicia.http.thrdPary;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import external.org.json.JSONArray;
import external.org.json.JSONException;
import external.org.json.JSONObject;

public class TestGson
{

   @Before
   public void setUp() throws Exception
   {
   }

   @Test
   public void testFromJson() throws Exception
   {

      String json = "[{\"id\":\"3\",\"title\":\"Site Usages\",\"website_id\":null}]";
      JSONArray array = new JSONArray(json);
      for (int i = 0; i < array.length(); i++)
      {
	 JSONObject obj = array.getJSONObject(i);
	 System.out.println(obj.toString());
      }

      JSONObject userIdentifierResponse = new JSONObject(
	    "{\"userId\":5,\"attrs\":[]}");
      System.out.println(userIdentifierResponse.toString(2));
      assertEquals(5, userIdentifierResponse.get("userId"));
   }

   @Test
   public void TestQueryJson() throws Exception
   {
      String expected = "{\"where\":[{\"taxon\":\"Hypnum touretti\"}]}";
      JSONObject clause = new JSONObject().put("taxon", "Hypnum touretti");
      JSONArray array = new JSONArray();
      array.put(clause);
      JSONObject json = new JSONObject().put("where", array);

      assertEquals(expected, json.toString());
   }

   @Test
   public void TestUnmarshall() throws Exception
   {
      String response = "{\"success\":\"multiple records\",\"outer_table\":\"sample\",\"outer_id\":\"12960\",\"struct\":{\"model\":\"sample\",\"id\":\"12960\",\"children\":[{\"model\":\"sample_attribute_value\",\"id\":\"7474\"}]}}";
      JSONObject json = new JSONObject(response);
      assertEquals("multiple records", json.get("success"));
      assertEquals("12960", json.get("outer_id"));
      boolean exception = false;
      try
      {
	 json.get("error");
      }
      catch (JSONException e)
      {
	 assertEquals("JSONObject[\"error\"] not found.", e.getMessage());
	 exception = true;
      }
      assertTrue(exception);
   }
}
