package org.infinitenature.werbeo.service.adapters.config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.HashSet;
import java.util.Set;

import org.infinitenature.werbeo.service.adapters.AdditionalOccurrenceAttributeConfig;
import org.infinitenature.werbeo.service.adapters.AdditionalSampleAttributeConfig;
import org.infinitenature.werbeo.service.adapters.AdditionalSurveyAttributeConfig;
import org.infinitenature.werbeo.service.indicia.http.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Import(Application.class)
public class ITestAdditionalAttributeConfigFacotry
{
   @Autowired
   private AdditionalAttributeConfigFactory factory;

   @Test
   public void testGetOccurrenceConfig()
   {
      AdditionalOccurrenceAttributeConfig config = factory.getOccurrenceConfig();

      Set<Integer> ids = new HashSet<>();
      ids.add(config.getCartNumber());
      ids.add(config.getLitCode());
      ids.add(config.getExtinctionDate());
      ids.add(config.getAmount());
      ids.add(config.getLitRemark());
      ids.add(config.getStatus());
      ids.add(config.getLegName());
      ids.add(config.getUuid());
      ids.add(config.getFkDeck());
      ids.add(config.getCoverageMin());
      ids.add(config.getCoverageMax());
      ids.add(config.getCoverageMean());
      ids.add(config.getLayer());
      ids.add(config.getCoverageCode());

      assertThat(ids.size(), is(14));
      assertThat(ids.contains(0), is(false));
   }

   @Test
   public void testGetSampleConfg()
   {
      AdditionalSampleAttributeConfig config = factory.getSampleConfig();

      Set<Integer> ids = new HashSet<>();
      ids.add(config.getArea());
      ids.add(config.getAreaMax());
      ids.add(config.getAreaMin());
      ids.add(config.getAvailability());
      ids.add(config.getCoverScaleCode());
      ids.add(config.getCoverScaleName());
      ids.add(config.getLegnam());
      ids.add(config.getLetterDate());
      ids.add(config.getLocationDescription());
      ids.add(config.getMtb());
      ids.add(config.getPrecision());
      ids.add(config.getStatus());
      ids.add(config.getTurboeVegAttribute());

      assertThat(ids.size(), is(13));
      assertThat(ids.contains(0), is(false));
   }

   @Test
   public void testGetSurveyConfig()
   {
      AdditionalSurveyAttributeConfig config = factory.getSurveyConfig();

      Set<Integer> ids = new HashSet<>();
      ids.add(config.getAvailabitlityId());
      ids.add(config.getContainerId());
      ids.add(config.getObfuscationPoliciesId());
      ids.add(config.getWerbeoOriginalId());

      assertAll(() -> assertThat(ids.size(), is((4))),
            () -> assertThat(ids.contains(0), is(false)));
   }

}
