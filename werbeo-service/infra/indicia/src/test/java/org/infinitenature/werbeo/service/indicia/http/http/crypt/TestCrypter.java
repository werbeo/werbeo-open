package org.infinitenature.werbeo.service.indicia.http.http.crypt;

import static org.junit.Assert.assertEquals;

import org.infinitenature.werbeo.service.indicia.http.http.crypt.Crypter;
import org.junit.*;

public class TestCrypter
{

   private Crypter crypter;

   @Before
   public void setUp()
   {
      crypter = new Crypter();
   }

   @Test
   public void testRoundTrip()
   {
      String test = "Geheimer Bootschaft";
      String key = "top secret key";
      assertEquals(test,
	    crypter.decrypt(new String(crypter.encrypt(test, key)), key));
   }

   @Test
   public void testDecrypt()
   {
      String plain = "Wichtiger Text";
      String encrypted = "msjuy6/Yiu000uBwggWBIQ==";

      System.out.println(crypter.decrypt(encrypted, "geheim"));
      assertEquals(plain, crypter.decrypt(encrypted, "geheim"));

   }

   @Test
   public void testEncrypt()
   {
      String encrypted = crypter.encrypt("Wichtiger Text", "geheim");
      System.out.println(encrypted);
      assertEquals("msjuy6/Yiu000uBwggWBIQ==", encrypted);
   }

}
