package org.infinitenature.werbeo.service.adapters;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.RandomUtils;
import org.infinitenature.vaguedate.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RandomSampleGenerator
{
   private final static int minTaxId = 100;
   private final static int maxTaxId = 19088;
   private final static int minPersonId = 2;
   private final static int maxPersonId = 200;

   @Autowired
   private PositionFactory positionFactory;

   public List<Sample> generateSamples(int amountSamples, int maxOccPerSample,
         Survey survey, String... mtbs)
   {
      List<Sample> samples = new ArrayList<>(amountSamples);
      while (amountSamples > 0)
      {
         Sample sample = new Sample();
         sample.setId(UUID.randomUUID());
         sample.setSurvey(survey);
         sample.setDate(createRandomVaugeDate());
         sample.setLocality(new Locality());
         sample.getLocality().setPosition(positionFactory
               .createFromMTB(mtbs[RandomUtils.nextInt(0, mtbs.length)]));
         sample.setRecorder(new Person(
               RandomUtils.nextInt(minPersonId, maxPersonId), null, null));
         sample.getLocality().setPrecision(200);
         int amountOccurrences = RandomUtils.nextInt(1, maxOccPerSample);
         while (amountOccurrences > 0)

         {
            Occurrence occurrence = new Occurrence();
            occurrence.setId(UUID.randomUUID());
            occurrence.setTaxon(new Taxon());
            occurrence.getTaxon()
                  .setId(RandomUtils.nextInt(minTaxId, maxTaxId));
            occurrence.setRecordStatus(RecordStatus.COMPLETE);
            occurrence.setDeterminer(new Person(
                  RandomUtils.nextInt(minPersonId, maxPersonId), null, null));
            occurrence.setSettlementStatus(getRandomStatus());
            sample.getOccurrences().add(occurrence);
            amountOccurrences--;
         }
         samples.add(sample);
         amountSamples--;
      }
      return samples;
   }

   private SettlementStatus getRandomStatus()
   {
      switch (RandomUtils.nextInt(1, 5))
      {
      case 1:
         return SettlementStatus.PLANTED;
      case 2:
         return SettlementStatus.RESETTELD;
      case 3:
      case 4:
      case 5:
         return SettlementStatus.WILD;
      default:
         // should not happen
         return SettlementStatus.PLANTED;
      }
   }

   private VagueDate createRandomVaugeDate()
   {
      LocalDate localDate = LocalDate.now()
            .minusDays(RandomUtils.nextInt(0, 365 * 20));

      switch (RandomUtils.nextInt(1, 4))
      {
      case 1:
         return VagueDateFactory.create(localDate, VagueDate.Type.DAY);
      case 2:
         return VagueDateFactory.create(localDate.minusYears(1),
               VagueDate.Type.YEAR);
      case 3:
         return VagueDateFactory.create(localDate,
               VagueDate.Type.MONTH_IN_YEAR);
      case 4:
         return VagueDateFactory.create(
               localDate.minusDays(RandomUtils.nextInt(1, 7)), localDate,
               VagueDate.Type.DAYS.getStringRepresentation());
      default:
         throw new IllegalArgumentException();
      }
   }
}
