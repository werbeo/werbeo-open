package org.infinitenature.werbeo.service.indicia.http.http;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import org.infinitenature.werbeo.service.indicia.http.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestIndiciaHttpConnector extends IntegrationTest
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ITestIndiciaHttpConnector.class);
   @Autowired
   private IndiciaHttpConnector indiciaHttpConnector;

   @Test
   void testGetReadNonce()
   {
      ReadNonce nonce = indiciaHttpConnector.getReadNonce(WEBSITE_ID);
      ReadNonce nonce2 = indiciaHttpConnector.getReadNonce(WEBSITE_ID);
      LOGGER.debug("Nonce: {}", nonce);
      LOGGER.debug("Nonce2: {}", nonce);
      assertFalse("Tow nonces should not be equal.",
            nonce.getReadNonce().equals(nonce2.getReadNonce()));
      assertNotSame(nonce, nonce2);
   }

   @Test
   void testGetWriteAuthToken()
   {
      AuthToken authToken = indiciaHttpConnector.getWriteAuthToken(WEBSITE_ID);
      LOGGER.debug(authToken.getToken());
      assertNotNull(authToken.getToken());
   }

   @Test
   void testGetReadWriteNonce()
   {
      ReadWriteNonce nonce = indiciaHttpConnector.getReadWriteNonce(WEBSITE_ID);
      ReadWriteNonce nonce2 = indiciaHttpConnector
            .getReadWriteNonce(WEBSITE_ID);
      LOGGER.debug("Nonce: {}", nonce);
      LOGGER.debug("Nonce2: {}", nonce);
      assertFalse("Tow nonces should not be equal.",
            nonce.getReadNonce().equals(nonce2.getReadNonce()));
      assertFalse("Tow nonces should not be equal.",
            nonce.getWriteNonce().equals(nonce2.getWriteNonce()));
      assertNotSame(nonce, nonce2);
   }

   @Test
   void testGetReadAuthToken()
   {
      AuthToken authToken = indiciaHttpConnector.getReadAuthToken(WEBSITE_ID,
            true);
      LOGGER.debug(authToken.getToken());
      assertNotNull(authToken.getToken());
   }
}