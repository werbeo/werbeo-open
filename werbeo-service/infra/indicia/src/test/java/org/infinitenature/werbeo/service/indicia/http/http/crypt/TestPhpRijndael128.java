package org.infinitenature.werbeo.service.indicia.http.http.crypt;

import static org.junit.Assert.*;

import org.apache.commons.codec.binary.Base64;
import org.infinitenature.werbeo.service.indicia.http.http.crypt.PhpRijndael128;
import org.junit.Test;

public class TestPhpRijndael128
{
   @Test
   public void testMcrypt() throws Exception
   {
      PhpRijndael128 rijndael = new PhpRijndael128("geheim".getBytes());

      String plain = "Wichtiger Text";
      byte[] crypt = rijndael.encrypt(plain.getBytes());
      String crypt64 = Base64.encodeBase64String(crypt);
      System.out.println(crypt64);
      assertEquals("msjuy6/Yiu000uBwggWBIQ==", crypt64.trim());
      assertArrayEquals(crypt, Base64.decodeBase64(crypt64));

      assertEquals(plain,
	    new String(rijndael.decrypt(Base64.decodeBase64(crypt64))).trim());

      crypt = rijndael.encrypt(
	    "Wichtiger Text, dieser ist aber länger und mit Umlauten ÄÖÜ"
		  .getBytes("utf-8"));
      System.out.println(new String(Base64.encodeBase64(crypt)));
      assertEquals(
	    "cNM/dXGscjvLkJLdb/assi4iPTcw+DFEU+v63NWBD8hnV0hjE6ziayN7rpAKLCOlcQgt8m7Vln3EBJgESwJFzA==",
	    new String(Base64.encodeBase64(crypt)));
      System.out.println(new String(rijndael.decrypt(crypt)));
      assertEquals("Wichtiger Text",
	    new String(rijndael
		  .decrypt(Base64.decodeBase64("msjuy6/Yiu000uBwggWBIQ==")))
			.trim());
   }
}
