package org.infinitenature.werbeo.service.indicia.http;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
@Import(Application.class)
public abstract class IntegrationTest
{

   protected int WEBSITE_ID = 1;
   @Autowired
   public IndiciaApi indiciaApi;


   protected TestHelper testHelper;

   public IntegrationTest()
   {
      super();
   }

   @BeforeEach
   public void init()
   {
      testHelper = new TestHelper(WEBSITE_ID, 2);
   }


}