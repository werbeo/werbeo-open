package org.infinitenature.werbeo.service.adapters;

import java.util.Collections;
import java.util.UUID;

import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.core.api.enity.Locality;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.NumericAmountAccuracy;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.infra.query.PersonQueries;
import org.infinitenature.werbeo.service.core.api.infra.query.SurveyQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@Import(org.infinitenature.werbeo.Application.class)
public abstract class IntegrationTest
{
   @Autowired
   private PositionFactory positionFactory;
   @Autowired
   private SurveyQueries surveyQueries;
   @Autowired
   protected PersonQueries personQueries;

   protected Context context;

   protected User getRandomUser()
   {
      String uuid = "Daba" + UUID.randomUUID().toString().substring(0, 10); // sorts
                                                                            // somewhere
                                                                            // in
                                                           // the middle
      Person person = new Person(uuid, uuid);
      User user = new User();
      user.setLogin(uuid + "@" + uuid + ".com");
      user.setPerson(person);
      return user;
   }

   protected User getUser(String login)
   {
      switch (login)
      {
      case UserTestUtil.USER_LOGIN_A:
         Person person = new Person("Cthrine", "Tomala");
         User user = new User();
         user.setLogin(UserTestUtil.USER_LOGIN_A);
         user.setPerson(person);
         return user;
      case UserTestUtil.USER_LOGIN_B:
         Person personB = new Person("Sarge", "Ellins");
         User userB = new User();
         userB.setLogin(UserTestUtil.USER_LOGIN_B);
         userB.setPerson(personB);
         return userB;
      default:
         throw new IllegalArgumentException("Unknown user login " + login);
      }

   }

   @Autowired
   private PortalQueriesAdapter portalQueriesAdapter;

   protected Context createContext(int portalId, User user)
   {
      return new Context(portalQueriesAdapter.load(portalId), user,
            Collections.emptySet(), "test");
   }

   protected Sample createSample()
   {
      Survey survey = surveyQueries.load(1, context);

      Person recorder = personQueries.get(4, context);
      Sample sample = new Sample();
      sample.setSampleMethod(SampleMethod.FIELD_OBSERVATION);
      sample.setId(UUID.randomUUID());
      sample.setSurvey(survey);
      sample.setDate(VagueDateFactory.create(2000));
      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(positionFactory.createFromMTB("1943"));
      sample.setRecorder(recorder);
      sample.getLocality().setPrecision(200);

      int taxonId = 521;
      sample.getOccurrences().add(createOccurrence(taxonId));
      sample.getOccurrences().add(createOccurrence(400));
      return sample;
   }

   private Occurrence createOccurrence(int taxonId)
   {
      Occurrence occurrence = new Occurrence();
      occurrence.setId(UUID.randomUUID());
      occurrence.setTaxon(new Taxon());
      occurrence.getTaxon().setId(taxonId);
      occurrence.setRecordStatus(RecordStatus.IN_PROGRESS);
      occurrence.setAmount(Amount.A_LOT);
      occurrence.setSettlementStatus(SettlementStatus.RESETTELD);
      occurrence.setDeterminer(new Person(20, null, null));
      occurrence.setDeterminationComment("This is a determination comment");
      occurrence.setTimeOfDay("15:30");
      occurrence.setNumericAmountAccuracy(NumericAmountAccuracy.APPROXIMATE);
      return occurrence;
   }
}
