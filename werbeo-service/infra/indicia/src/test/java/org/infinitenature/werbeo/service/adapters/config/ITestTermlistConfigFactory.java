package org.infinitenature.werbeo.service.adapters.config;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.werbeo.service.adapters.IntegrationTest;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestTermlistConfigFactory extends IntegrationTest
{
   @Autowired
   private TermlistConfigFactory termlistConfigFactoryUT;

   @Test
   public void test_getId()
   {
      TermConfig termlistConfig = termlistConfigFactoryUT.getTermConfig();

      assertThat(termlistConfig.getId(Amount.ONE), is(144));
      assertThat(termlistConfig.getId(Amount.A_LOT), is(147));

      assertThat(termlistConfig.getId(SettlementStatus.WILD), is(141));
      assertThat(termlistConfig.getId(SettlementStatus.PLANTED), is(143));
   }

   @Test
   public void test_getAmount()
   {
      TermConfig termlistConfig = termlistConfigFactoryUT.getTermConfig();

      assertThat(termlistConfig.getAmount(144), is(Amount.ONE));
      assertThat(termlistConfig.getAmount(147), is(Amount.A_LOT));
   }

   @Test
   public void test_getStatus()
   {
      TermConfig termlistConfig = termlistConfigFactoryUT.getTermConfig();
      assertThat(termlistConfig.getStatus(141), is(SettlementStatus.WILD));
      assertThat(termlistConfig.getStatus(143), is(SettlementStatus.PLANTED));
   }
}
