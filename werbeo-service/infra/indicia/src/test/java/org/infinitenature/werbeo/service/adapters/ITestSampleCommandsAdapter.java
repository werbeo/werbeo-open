package org.infinitenature.werbeo.service.adapters;

import static org.exparity.hamcrest.date.LocalDateTimeMatchers.within;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import org.infinitenature.vaguedate.VagueDateFactory;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.NumericAmountAccuracy;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus;
import org.infinitenature.werbeo.service.core.api.enity.Position.PositionType;
import org.infinitenature.werbeo.service.core.api.enity.RecordStatus;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.infra.query.SampleQueries;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class ITestSampleCommandsAdapter extends IntegrationTest
{

   @Autowired
   private SampleCommandsAdapter adapter;

   @Autowired
   private SampleQueries sampleQueries;

   @BeforeEach
   public void setUp()
   {
      context = createContext(PortalTestUtil.DEMO_ID,
            getUser(UserTestUtil.USER_LOGIN_A));
   }

   @Test
   void test_changeOccurrenceValue()
   {
      UUID uuid = adapter.saveOrUpdate(createSample(), context);

      Sample loadedSample = sampleQueries.get(uuid, context).get();
      Occurrence occurrence521 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 521).findFirst().get();
      occurrence521.setDeterminationComment("");

      adapter.saveOrUpdate(loadedSample, context);

      loadedSample = sampleQueries.get(uuid, context).get();

      occurrence521 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 521).findFirst().get();

      assertThat(occurrence521.getDeterminationComment(), is(nullValue()));
      Occurrence occurrence400 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 400).findFirst().get();
      assertThat(occurrence400.getDeterminationComment(),
            is("This is a determination comment"));
   }

   @Test
   void test_saveOtherCreated()
   {
      Sample sample = createSample();
      User user = getRandomUser();
      sample.setCreatedBy(user);

      UUID uuid = adapter.saveOrUpdate(sample, context);

      Sample loadedSample = sampleQueries.get(uuid, context).get();
      assertThat(loadedSample.getId(), is(uuid));
      assertThat(loadedSample.getCreatedBy().getLogin(), is(user.getLogin()));
      assertThat(loadedSample.getModifiedBy().getLogin(), is(user.getLogin()));
   }

   @Test
   void test_edit()
   {
      UUID uuid = adapter.saveOrUpdate(createSample(), context);

      Sample loadedSample = sampleQueries.get(uuid, context).get();

      Occurrence occurrence521 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 521).findFirst().get();

      assertThat(occurrence521.getSettlementStatus(),
            is(SettlementStatus.RESETTELD));

      Occurrence occurrence400 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 400).findFirst().get();

      assertThat(occurrence400.getSettlementStatus(),
            is(SettlementStatus.RESETTELD));

      occurrence400.setSettlementStatus(null);
      occurrence521.setSettlementStatus(SettlementStatus.WILD);


      adapter.saveOrUpdate(loadedSample, context);

      loadedSample = sampleQueries.get(uuid, context).get();

      occurrence521 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 521).findFirst().get();

      assertThat(occurrence521.getSettlementStatus(),
            is(SettlementStatus.WILD));

      occurrence400 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 400).findFirst().get();

      assertThat(occurrence400.getSettlementStatus(), is(nullValue()));

   }

   @Test
   void test_save()
   {
      UUID uuid = adapter.saveOrUpdate(createSample(), context);

      LocalDateTime now = LocalDateTime.now(ZoneId.of("UTC"));

      Sample loadedSample = sampleQueries.get(uuid, context).get();
      // TODO check that lodaded sample is the same as saved one

      assertThat(loadedSample.getId(), is(uuid));
      assertThat(loadedSample.getOccurrences().size(), is(2));
      assertThat(loadedSample.getSurvey().getId(), is(1));
      assertThat(loadedSample.getRecorder().getId(), is(4));
      assertThat(loadedSample.getLocality().getPosition().getType(),
            is(PositionType.MTB));
      assertThat(loadedSample.getLocality().getPosition().getMtb().getMtb(),
            is("1943"));
      assertThat(loadedSample.getLocality().getPrecision(), is(200));
      assertThat(loadedSample.getDate(), is(VagueDateFactory.create(2000)));

      assertThat(loadedSample.getCreatedBy().getLogin(),
            is(UserTestUtil.USER_LOGIN_A));
      assertThat(loadedSample.getModifiedBy().getLogin(),
            is(UserTestUtil.USER_LOGIN_A));

      assertThat(loadedSample.getCreationDate(),
            is(within(1, ChronoUnit.MINUTES, now)));
      assertThat(loadedSample.getModificationDate(),
            is(within(1, ChronoUnit.MINUTES, now)));

      assertThat(loadedSample.getSampleMethod(),
            is(SampleMethod.FIELD_OBSERVATION));

      Occurrence occurrence521 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 521).findFirst().get();
      assertThat(occurrence521.getId(), is(not(nullValue())));
      assertThat(occurrence521.getTaxon().getId(), is(521));
      assertThat(occurrence521.getRecordStatus(), is(RecordStatus.IN_PROGRESS));
      assertThat(occurrence521.getAmount(), is(Amount.A_LOT));
      assertThat(occurrence521.getSettlementStatus(),
            is(SettlementStatus.RESETTELD));
      assertThat(occurrence521.getDeterminer().getId(), is(20));
      assertThat(occurrence521.getDeterminationComment(),
            is("This is a determination comment"));
      assertThat(occurrence521.getNumericAmountAccuracy(),
            is(NumericAmountAccuracy.APPROXIMATE));

      Occurrence occurrence400 = loadedSample.getOccurrences().stream()
            .filter(occ -> occ.getTaxon().getId() == 400).findFirst().get();
      assertThat(occurrence400.getId(), is(not(nullValue())));
      assertThat(occurrence400.getTaxon().getId(), is(400));

      assertThat(occurrence400.getId(), is(not(occurrence521.getId())));
   }

   @Test
   void test_saveSameEmailOtherName()
   {
      // ensure email is used in indica
      Sample createSample = createSample();
      User createdBy = new User();
      createdBy.setPerson(personQueries.get(4, context));
      createdBy.getPerson().setId(null);
      createdBy.setLogin(createdBy.getPerson().getEmail());
      createdBy.getPerson().setEmail(null);
      createSample.setCreatedBy(createdBy);
      adapter.saveOrUpdate(createSample, context);

   }

}
