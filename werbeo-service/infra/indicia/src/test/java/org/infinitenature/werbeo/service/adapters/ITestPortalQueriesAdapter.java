package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.ports.PortalFilter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestPortalQueriesAdapter extends IntegrationTest
{
   @Autowired
   private PortalQueriesAdapter portalQueriesAdapterUT;

   @Test
   public void testLoad_FloraMV()
   {
      Portal mv = portalQueriesAdapterUT.load(PortalTestUtil.FLORA_MV_ID);

      assertThat(mv.getTitle(), is("flora-mv"));
      assertThat(mv.getUrl(), is("http://test.flora-mv.de"));
   }

   @Test
   public void testLoadAssisiated_FloraMV()
   {
      List<Portal> portals = portalQueriesAdapterUT
            .findAssociated(PortalTestUtil.FLORA_MV_ID).getContent();

      assertThat(portals.size(), is(1));
   }

   @Test
   public void testLoadAssisiated_Demo()
   {
      List<Portal> portals = portalQueriesAdapterUT
            .findAssociated(PortalTestUtil.DEMO_ID).getContent();

      assertThat(portals.size(), is(1));
   }

   @Test
   public void testLoadAssisiated_REST()
   {
      List<Portal> portals = portalQueriesAdapterUT
            .findAssociated(PortalTestUtil.REST_SRV_ID).getContent();

      assertThat(portals.size(), is(4));

      Set<String> titles = portals.stream().map(Portal::getTitle)
            .collect(Collectors.toSet());
      assertThat(titles, containsInAnyOrder("flora-bb", "flora-mv", "vegetweb",
            "rest-service"));
   }

   @Test
   @DisplayName("Find, sorted by title")
   void test001()
   {
      Portal vegetweb = portalQueriesAdapterUT
            .find(new PortalFilter(), new OffsetRequestImpl<>(
                  0,
                  1, SortOrder.DESC, PortalSortField.TITLE))
            .getContent().get(0);

      assertThat(vegetweb.getTitle(), is("vegetweb"));
   }

}
