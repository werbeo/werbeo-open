package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Survey.Availability;
import org.infinitenature.werbeo.service.core.api.infra.query.SurveyQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestSurveyCommandsAdapter extends IntegrationTest
{
   @Autowired
   private SurveyCommandAdapter surveyCommandAdapter;

   @Autowired
   private SurveyQueries surveyQuery;

   @Test
   public void test_save()
   {
      Context context = createContext(PortalTestUtil.DEMO_ID, getUser(UserTestUtil.USER_LOGIN_A));
      Survey survey = new Survey();

      String name = "JUNIT-" + LocalDateTime.now() + "-" + UUID.randomUUID();
      survey.setName(name);
      survey.setPortal(context.getPortal());
      survey.setAvailability(Availability.FREE);
      ObfuscationPolicy pol1 = new ObfuscationPolicy("ACCEPTED", "PERSONS");
      ObfuscationPolicy pol2 = new ObfuscationPolicy("ADMIN", "LOCATION_POINT");
      survey.getObfuscationPolicies().add(pol1);
      survey.getObfuscationPolicies().add(pol2);
      survey.getTags().add("TAG_1");
      survey.getTags().add("TAG_2");
      int savedSurveyId = surveyCommandAdapter.saveOrUpdate(survey, context);

      Survey savedSurvey = surveyQuery.load(savedSurveyId, context);
      assertAll(() -> assertThat(savedSurvey.getId(), is(not(nullValue()))),
            () -> assertThat(savedSurvey.getAvailability(),
                  is(Availability.FREE)),
            () -> assertThat(savedSurvey.getName(), is(name)),
            () -> assertThat(savedSurvey.isContainer(), is(false)),
            () -> assertThat(savedSurvey.getPortal().getId(),
                  is(PortalTestUtil.DEMO_ID)),
            () -> assertThat(savedSurvey.getObfuscationPolicies().size(),
                  is(2)),
            () -> assertThat(savedSurvey.getTags(),
                  containsInAnyOrder("TAG_1", "TAG_2")));
   }

   @Test
   public void test_update() throws InterruptedException
   {
      Context creationContext = createContext(PortalTestUtil.DEMO_ID, getUser(UserTestUtil.USER_LOGIN_A));
      Context updateContext = createContext(PortalTestUtil.DEMO_ID, getUser(UserTestUtil.USER_LOGIN_B));
      Survey survey = new Survey();

      String name = "JUNIT-" + LocalDateTime.now() + "-" + UUID.randomUUID();
      survey.setName(name);
      survey.setPortal(creationContext.getPortal());
      survey.setAvailability(Availability.FREE);
      survey.getTags().add("TAG_1");
      survey.getTags().add("TAG_2");
      survey.getTags().add("TAG_3");

      int surveyId = surveyCommandAdapter.saveOrUpdate(survey, creationContext);

      Survey savedSurvey = surveyQuery.load(surveyId, creationContext);
      savedSurvey.setContainer(true);
      savedSurvey.setName(savedSurvey.getName() + "-CONTAINER");
      savedSurvey.getTags().remove("TAG_2");
      savedSurvey.getTags().remove("TAG_3");
      savedSurvey.getTags().add("TAG_4");
      TimeUnit.SECONDS.sleep(2); // Wait to seconds, because indicia saves the
                                 // modification date in seconds resolution
      int updatedSurveyId = surveyCommandAdapter.saveOrUpdate(savedSurvey,
            updateContext);

      Survey updatedSurvey = surveyQuery.load(updatedSurveyId, creationContext);

      assertAll(
            () -> assertThat(updatedSurvey.getId(), is(savedSurvey.getId())),
            () -> assertThat(updatedSurvey.getAvailability(),
                  is(Availability.FREE)),
            () -> assertThat(updatedSurvey.getName(), is(name + "-CONTAINER")),
            () -> assertThat(updatedSurvey.isContainer(), is(true)),
            () -> assertThat(updatedSurvey.getPortal().getId(),
                  is(PortalTestUtil.DEMO_ID)),
            () -> assertThat(updatedSurvey.getCreatedBy().getLogin(),
                  is(UserTestUtil.USER_LOGIN_A)),
            () -> assertThat(updatedSurvey.getModifiedBy().getLogin(),
                  is(UserTestUtil.USER_LOGIN_B)),
            () -> assertThat(updatedSurvey.getCreationDate()
                  .isBefore(updatedSurvey.getModificationDate()), is(true)),
            () -> assertThat(updatedSurvey.getTags(),
                  containsInAnyOrder("TAG_4", "TAG_1")));
   }
}
