package org.infinitenature.werbeo.service.indicia.http.types.submission;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

public class TestSubmission
{
   @Test
   public void testModel() throws Exception
   {
      Set<Field> fields = new TreeSet<>();
      fields.add(new Field("_field 1", "_value 1"));
      Model model = new Model("_Model Name", fields);
      System.out.println(model.getJsonObject());
   }

   @Test
   public void testTest() throws Exception
   {
      Set<Field> fields = new HashSet<>();
      fields.add(new Field("_field id", "_field value"));
      fields.add(new Field("_field id 2", "_field value 2"));
      Model model = new Model("_Model Name", fields);
      System.out.println(model.getJsonObject());

   }

   @Test
   public void testSubmission() throws Exception
   {
      Set<Field> fields = new HashSet<>();
      fields.add(new Field("_field id", "_field value"));
      fields.add(new Field("_field id 2", "_field value 2"));
      Model model = new Model("_Model Name", fields);
      List<Model> models = new ArrayList<>();
      models.add(model);
      Set<Field> subModelFields = new HashSet<>();
      subModelFields.add(new Field("field_name", "field_value"));
      Model subModelModel = new Model("subModel", subModelFields);
      SubModel subModel = new SubModel("subModel_id", subModelModel);
      List<SubModel> subModels = new ArrayList<>();
      subModels.add(subModel);
      model.setSubModels(subModels);
      Submission submission = new Submission(models);
      System.out.println(submission);
      System.out.println(submission.getJsonObject());
   }

   @Test
   public void testSubModel() throws Exception
   {
      Set<Field> subModelFields = new HashSet<>();
      subModelFields.add(new Field("field_name", "field_value"));
      Model subModelModel = new Model("subModel", subModelFields);
      SubModel subModel = new SubModel("subModel_id", subModelModel);
      System.out.println(subModel.getJsonObject());
   }
}