package org.infinitenature.werbeo.service.indicia.http.http;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.werbeo.service.indicia.http.http.AuthToken;
import org.infinitenature.werbeo.service.indicia.http.http.ReadAuthToken;
import org.junit.*;

public class TestAuthToken
{
   private AuthToken authTokenUnderTest;

   @Before
   public void setUp() throws Exception
   {
      authTokenUnderTest = new ReadAuthToken(null, "geheim");
   }

   @Test
   public void testCreateAuthToken()
   {
      assertThat(authTokenUnderTest.createAuthToken("nonce"), is("6444dee9ede30f474b33d4fb0905e9e64dab6df6"));
   }

}
