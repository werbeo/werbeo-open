package org.infinitenature.werbeo.service.indicia.http.thrdPary;

import static org.junit.Assert.assertEquals;

import org.infinitenature.werbeo.service.indicia.http.http.crypt.PhpWrapper;
import org.junit.Test;

public class TestPhpWrapper
{

   @Test
   public void TestSHA1()
   {
      assertEquals("d6e6b4a2a3e9c934c0fd830383a06c405737a558",
	    PhpWrapper.sha1("wichtig"));
   }
}
