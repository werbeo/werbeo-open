package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.infinitenature.werbeo.service.adapters.IntegrationTest;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBOccurrence;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBSample;
import org.infinitenature.werbeo.service.indicia.jpa.entities.QDBTaxon;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.querydsl.core.types.dsl.BooleanExpression;


public class MTestSampleRepository extends IntegrationTest
{
   @Autowired
   private SampleRepository sampleRepositroy;
   @Autowired
   private OccurrenceRepository occurrenceRepository;
   @Autowired
   private TaxonRepository taxonRepository;

   @Test
   @Transactional
   public void getOne()
   {
      DBSample sample = sampleRepositroy.getOne(8);

      sample.getOccurrences().forEach(occ -> System.out.println(occ.getId()));
      System.out.println(sample.getOccurrences().size());

   }

   @Test
   @Transactional
   public void testFindTaxa()
   {
      QDBTaxon qTaxon = QDBTaxon.dBTaxon;
      BooleanExpression predicate = qTaxon.deleted.isFalse();
      predicate = predicate.and(qTaxon.taxon.likeIgnoreCase("Bue% Pha%"));

      System.out.println(taxonRepository.findAll(predicate).iterator().next().getTaxon());
   }

   @Test
   @Transactional
   public void findOccBySampleId()
   {
      List<DBOccurrence> occurrences = occurrenceRepository.findBySampleId(8);
      System.out.println(occurrences.size());
   }
}
