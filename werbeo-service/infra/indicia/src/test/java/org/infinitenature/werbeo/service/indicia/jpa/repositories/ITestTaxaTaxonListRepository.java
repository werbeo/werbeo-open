package org.infinitenature.werbeo.service.indicia.jpa.repositories;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import org.infinitenature.werbeo.service.adapters.IntegrationTest;
import org.infinitenature.werbeo.service.indicia.jpa.entities.DBTaxaTaxonList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ITestTaxaTaxonListRepository extends IntegrationTest
{
   @Autowired
   private TaxaTaxonListRepository repoUT;

   @Test
   @DisplayName("Find maximum taxon_meaning_id")
   void test001()
   {
      assertThat(repoUT.getMaxTaxonMeaningId(), is(28260));
   }

   @Test
   @DisplayName("Check that parent is not populated on parent tax")
   @Transactional(readOnly = true)
   void test002()
   {
      DBTaxaTaxonList greenSomeThing = repoUT.getOne(1);

      assertThat(greenSomeThing.getTaxon().getTaxon(),
            is("\"Grünliches etwas\""));
      assertThat(greenSomeThing.getParent(), is(nullValue()));

      assertThat(greenSomeThing.getChildren(), hasSize(4));
   }

   @Test
   @DisplayName("Check that parent is not populated on tax")
   @Transactional(readOnly = true)
   void test003()
   {
      DBTaxaTaxonList greenSomeThing = repoUT.getOne(5509);

      assertThat(greenSomeThing.getTaxon().getTaxon(),
            is("\"Gefäßpflanze\" species"));
      assertThat(greenSomeThing.getParent().getId(), is(1));

      assertThat(greenSomeThing.getChildren(), hasSize(2));
   }
}
