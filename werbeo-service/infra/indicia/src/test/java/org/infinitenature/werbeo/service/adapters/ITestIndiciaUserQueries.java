package org.infinitenature.werbeo.service.adapters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ITestIndiciaUserQueries extends IntegrationTest
{
   @Autowired
   private IndiciaUserQueries userQueriesUT;

   @Test
   public void testGetUserId_newUser()
   {
      User user = new User();
      user.setLogin("newUser@mymail.de");
      user.setPerson(new Person("xxx", "xxx-xxx"));

      Context context = createContext(PortalTestUtil.DEMO_ID, user);

      int newUserId = userQueriesUT.getUserId(context);

      assertThat("New user id must not be null", newUserId != 0, is(true));

      assertThat("User id must not change for new request",
            userQueriesUT.getUserId(context), is(newUserId));

      assertThat("User id must not change for other portal",
            userQueriesUT.getUserId(createContext(PortalTestUtil.FLORA_BB_ID, user)),
            is(newUserId));
   }

}
