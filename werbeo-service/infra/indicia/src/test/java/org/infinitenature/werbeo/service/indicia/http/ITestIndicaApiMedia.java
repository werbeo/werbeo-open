package org.infinitenature.werbeo.service.indicia.http;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

class ITestIndicaApiMedia extends IntegrationTest
{
   @Autowired
   private IndiciaHttpConfiguration indiciaHttpConfiguration;
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ITestIndicaApiMedia.class);

   @Test
   void testUploadImagePNG()
   {
      InputStream picture = getClass().getResourceAsStream("/black.png");
      String pictureName = indiciaApi.uploadFile(picture, "TEST.png", 1);

      LOGGER.info("Uploaded image: " + pictureName);

      assertThat(pictureName, endsWith("test.png"));

      assertThat(exists(pictureName), is(true));
      assertThat(exists("med-" + pictureName), is(true));
      assertThat(exists("thumb-" + pictureName), is(true));
   }

   @Test
   void testUploadWAV()
   {
      InputStream wav = getClass()
            .getResourceAsStream(
                  "/456440__inspectorj__bird-whistling-robin-single-13.wav"
                        + "");
      String wavName = indiciaApi.uploadFile(wav,
            "456440__inspectorj__bird-whistling-robin-single-13.wav", 1);

      LOGGER.info("Uploaded mp3: " + wavName);

      assertThat(wavName,
            endsWith("456440__inspectorj__bird-whistling-robin-single-13.wav"));

      assertThat(exists(wavName), is(true));
   }

   boolean exists(String fileName)
   {
      try
      {
         URL url = new URL("http://" + indiciaHttpConfiguration.getHost() + "/"
               + indiciaHttpConfiguration.getPath() + "/upload/" + fileName);
         LOGGER.info("Checking existens of {}", url);
         HttpURLConnection.setFollowRedirects(false);
         // note : you may also need
         // HttpURLConnection.setInstanceFollowRedirects(false)
         HttpURLConnection con = (HttpURLConnection) url.openConnection();
         con.setRequestMethod("HEAD");
         return (con.getResponseCode() == HttpURLConnection.HTTP_OK);

      } catch (Exception e)
      {
         LOGGER.error("Failure cheking existens of {}", fileName);
         return false;
      }
   }


}
