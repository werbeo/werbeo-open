package org.infinitenature.werbeo.service.tools.ticket969;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.apache.commons.io.FileUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.PeopleFilter;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.service.v1.types.response.SampleSliceResponse;
import org.infinitenature.werbeo.client.Werbeo;
import org.keycloak.OAuthErrorException;
import org.keycloak.adapters.ServerRequest.HttpFailure;
import org.keycloak.adapters.installed.KeycloakInstalled;
import org.keycloak.common.VerificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdatePositionUserR
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(UpdatePositionUserR.class);

   public static void main(String[] args)
         throws IOException, VerificationException, OAuthErrorException,
         URISyntaxException, InterruptedException
   {
      List<String> lines = FileUtils.readLines(
            new File("../sql/correct_user_r_mtb/dataFromRnotMTB.csv"),
            StandardCharsets.UTF_8);

      lines.remove(0); // headers

      KeycloakInstalled keycloak = new KeycloakInstalled();

      keycloak.setLocale(Locale.ENGLISH);
      try
      {
         keycloak.loginDesktop();
      } catch (HttpFailure e)
      {
         LOGGER.error("Failure during login: {}", e.getError(), e);
         System.exit(1);
      }
      Werbeo werbeo = new Werbeo("https://api.test.infinitenature.org/api",
            new ClientRequestFilter()
            {
               public static final String AUTHORIZATION_HEADER = "Authorization";

               @Override
               public void filter(ClientRequestContext requestContext)
                     throws IOException
               {
                  String token = "";
                  try
                  {
                     token = keycloak.getTokenString(30, TimeUnit.SECONDS);
                  } catch (VerificationException | IOException e)
                  {
                     LOGGER.error("Failure getting token", e);
                     System.exit(1);
                  } catch (HttpFailure e)
                  {
                     LOGGER.error("Failure getting token {}", e.getError(), e);
                     System.exit(1);
                  }

                  requestContext.getHeaders().add(AUTHORIZATION_HEADER,
                        "Bearer " + token);
               }

            });
      final int portalId = 5;
      Person unkown = werbeo.people()
            .find(0, 50, PersonField.ID, SortOrder.ASC,
                  new PeopleFilter(portalId, "unbekannt", null, null))
            .getContent().stream()
            .filter(person -> "unbekannt@werbeo.de".equals(person.getEmail()))
            .findAny().get();
      lines.parallelStream().forEach(lineString ->
      {
         Line line = new Line(lineString);
         String occExternalKey = "Flora-MV-Global-ID: " + line.getGlobalID();

         OccurrenceFilter filter = new OccurrenceFilter()
               .setOccExternalKey(occExternalKey).setPortalId(portalId);
         SampleSliceResponse response = werbeo.samples().find(0, 10,
               OccurrenceField.MOD_DATE, SortOrder.ASC, filter, false);

         if (response.getContent().isEmpty())
         {
            LOGGER.warn("No occurrence found for extKey: {}", occExternalKey);
         } else
         {
            UUID sampleId = response.getContent().get(0).getEntityId();
            Sample sample = werbeo.samples().get(portalId, sampleId)
                  .getSample();
            if (sample.getOccurrences().get(0).getDeterminer() == null)
            {
               sample.getOccurrences().get(0).setDeterminer(unkown);
            }
            if (sample.getRecorder() == null)
            {
               sample.setRecorder(unkown);
            }
            Occurrence occurrence = sample.getOccurrences().get(0);
            UUID occId = occurrence.getEntityId();
            PositionType positionType = sample.getLocality().getPosition()
                  .getType();
            LOGGER.info(
                  "Occurrence found for extKey {} with UUID {} and positionType {}",
                  occExternalKey, occId, positionType);

            sample.getLocality().setBlur(line.getUnsch());

            Position position = new Position();
            position
                  .setWkt("POINT (" + line.getRw() + " " + line.getHw() + ")");
            position.setWktEpsg(31468);
            position.setType(PositionType.POINT);
            LOGGER.info("new position {}", position);
            sample.getLocality().setPosition(position);

            werbeo.samples().save(portalId, sample);
            werbeo.sampleComments().saveComment(5, sample.getEntityId(),
                  new SampleComment("Ort von MTB auf Punkt geändert."));
         }
      });
      keycloak.logout();
      System.exit(0);
   }
}
