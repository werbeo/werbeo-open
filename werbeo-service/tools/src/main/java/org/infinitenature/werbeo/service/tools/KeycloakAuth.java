package org.infinitenature.werbeo.service.tools;

import java.io.InputStream;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.keycloak.adapters.ServerRequest.HttpFailure;
import org.keycloak.adapters.installed.KeycloakInstalled;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeycloakAuth
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(KeycloakAuth.class);

   public static void main(String[] args) throws Exception
   {
      InputStream config = Thread.currentThread().getContextClassLoader()
            .getResourceAsStream("META-INF/keycloak-prod.json");
      KeycloakInstalled keycloak = new KeycloakInstalled(config);

      keycloak.setLocale(Locale.ENGLISH);
      try
      {
         keycloak.loginDesktop();
      } catch (HttpFailure e)
      {
         LOGGER.error("Failure during login: {}", e.getError(), e);
      }
      AccessToken token = keycloak.getToken();
      Executors.newSingleThreadExecutor().submit(() ->
      {

         System.out.println("Logged in...");
         System.out.println("Token: " + token.getSubject());
         System.out.println("Username: " + token.getPreferredUsername());
         try
         {
            System.out.println("AccessToken: " + keycloak.getTokenString());
         } catch (Exception ex)
         {
            ex.printStackTrace();
         }

         int timeoutSeconds = 20;
         System.out.printf("Logging out in...%d Seconds%n", timeoutSeconds);
         try
         {
            TimeUnit.SECONDS.sleep(timeoutSeconds);
         } catch (Exception e)
         {
            e.printStackTrace();
         }

         try
         {
            keycloak.logout();
         } catch (Exception e)
         {
            e.printStackTrace();
         }

         System.out.println("Exiting...");
         // System.exit(0);
      });
   }
}
