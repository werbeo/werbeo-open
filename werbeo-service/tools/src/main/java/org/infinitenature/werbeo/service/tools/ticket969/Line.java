package org.infinitenature.werbeo.service.tools.ticket969;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Line
{
   private final String globalID;
   private final String mtb;
   private final String qu;
   // RW,HW,UNSCH
   private final double rw;
   private final double hw;
   private final int unsch;
   public Line(String line)
   {
      String[] splittedLine = line.split(",");
      globalID = splittedLine[0];
      mtb = splittedLine[1];
      qu = splittedLine[2];
      rw = Double.valueOf(splittedLine[3]);
      hw = Double.valueOf(splittedLine[4]);
      unsch = Integer.valueOf(splittedLine[5]);
   }

   public String getGlobalID()
   {
      return globalID;
   }

   public String getMtb()
   {
      return mtb;
   }

   public String getQu()
   {
      return qu;
   }

   public double getRw()
   {
      return rw;
   }

   public double getHw()
   {
      return hw;
   }

   public int getUnsch()
   {
      return unsch;
   }

   @Override
   public String toString()
   {
      return LineBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return LineBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return LineBeanUtil.doEquals(this, obj);
   }
}
