package org.infinitenature.werbeo;

import org.infinitenature.service.support.xml.converter.LocalDateConverterProvider;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.resources.mock.MockData;
import org.infinitenature.service.v1.resources.mock.OccurrenceResourceMock;
import org.infinitenature.service.v1.resources.mock.PortalResourceMock;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

@EntityScan(basePackageClasses = { ServiceApp.class, })
@SpringBootApplication

public class ServiceApp
{
   public static void main(String[] args)
   {
      SpringApplication.run(ServiceApp.class, args);
   }

   @Bean
   public JaxbAnnotationModule jacksonJaxb()
   {
      return new JaxbAnnotationModule();
   }

   @Bean
   public MockData mockData()
   {
      return new MockData();
   }

   @Bean
   public OccurrenceResource occurrenceResource(MockData data)
   {
      return new OccurrenceResourceMock(data);
   }

   @Bean
   public PortalResource PortalResource(MockData data)
   {
      return new PortalResourceMock(data);
   }

   @Bean
   public LocalDateConverterProvider localDateConverterProvider()
   {
      return new LocalDateConverterProvider();
   }

}
