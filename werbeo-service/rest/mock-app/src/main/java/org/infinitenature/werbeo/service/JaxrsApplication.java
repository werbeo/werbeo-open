package org.infinitenature.werbeo.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/api")
public class JaxrsApplication extends Application
{

}
