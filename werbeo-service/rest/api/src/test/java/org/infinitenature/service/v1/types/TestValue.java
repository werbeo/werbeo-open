package org.infinitenature.service.v1.types;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.infinitenature.service.v1.types.Occurrence.Quantity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestValue
{
   @DisplayName("Sort order is correct")
   @Test
   void test_001()
   {
      SortedSet<Quantity> values = new TreeSet<>(Arrays
            .asList(Quantity.THOUSAND, Quantity.FIFTY, Quantity.ONE_TO_FIVE));

      assertThat(values, contains(Quantity.ONE_TO_FIVE, Quantity.FIFTY,
            Quantity.THOUSAND));
   }

}
