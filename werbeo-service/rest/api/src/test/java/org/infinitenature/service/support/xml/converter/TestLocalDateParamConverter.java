package org.infinitenature.service.support.xml.converter;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestLocalDateParamConverter
{
   private LocalDateParamConverter converterUT;

   @BeforeEach
   void setUp() throws Exception
   {
      converterUT = new LocalDateParamConverter();
   }

   @Test
   @DisplayName("Test read from string 1st of january 1500")
   void test_001()
   {
      assertThat(converterUT.fromString("1500-01-01"),
            is(LocalDate.of(1500, 1, 1)));
   }

   @Test
   @DisplayName("Test read from string 31st of january 1500")
   void test_002()
   {
      assertThat(converterUT.fromString("1500-01-31"),
            is(LocalDate.of(1500, 1, 31)));
   }
}
