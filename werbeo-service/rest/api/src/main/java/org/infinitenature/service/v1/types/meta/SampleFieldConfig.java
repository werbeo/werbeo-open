package org.infinitenature.service.v1.types.meta;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.types.Value;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SampleFieldConfig implements FieldConfig<SampleField>
{
   private SampleField field;
   private boolean mandantory;
   private String property;
   private String translationKey;
   private List<Value> values = new ArrayList<>();

   public SampleFieldConfig()
   {
      super();
   }

   public SampleFieldConfig(SampleField field, boolean mandantory)
   {
      super();
      this.field = field;
      this.mandantory = mandantory;
      this.property = field.getProperty();
      this.translationKey = field.getTranslationKey();

   }

   @Override
   public SampleField getField()
   {
      return field;
   }

   @Override
   public void setField(SampleField field)
   {
      this.field = field;
      if (field != null)
      {
         this.property = field.getProperty();
         this.translationKey = field.getTranslationKey();
      }
   }

   @Override
   public boolean isMandantory()
   {
      return mandantory;
   }

   @Override
   public void setMandantory(boolean mandantory)
   {
      this.mandantory = mandantory;
   }

   public String getProperty()
   {
      return property;
   }

   public void setProperty(String property)
   {
      this.property = property;
   }

   @Override
   public String getTranslationKey()
   {
      return translationKey;
   }

   public void setTranslationKey(String translationKey)
   {
      this.translationKey = translationKey;
   }

   @Override
   public String toString()
   {
      return SampleFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleFieldConfigBeanUtil.doEquals(this, obj);
   }

   @Override
   public List<Value> getValues()
   {
      return values;
   }

   public void setValues(List<Value> values)
   {
      this.values = values;
   }

}
