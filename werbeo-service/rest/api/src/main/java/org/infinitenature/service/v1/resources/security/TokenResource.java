package org.infinitenature.service.v1.resources.security;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.response.TokenResponse;
import org.infinitenature.service.v1.types.support.UserAuth;

/**
 * Handling of access tokens
 * 
 * @author dve
 *
 */
@Produces({ MediaType.APPLICATION_JSON })
@Path("/v1/security/token")
public interface TokenResource
{
   /**
    * Retrieving an access token
    * 
    * @param userAuth
    * @return
    */
   @Consumes({ MediaType.APPLICATION_JSON })
   @POST
   public TokenResponse getToken(@NotNull UserAuth userAuth);
}
