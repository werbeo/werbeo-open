package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class PeopleFilter
{
   /**
    * The portal.
    */
   @PathParam("portalId")
   @Min(1)
   private int portalId;
   @QueryParam("nameContains")
   private String nameContains;
   @QueryParam("email")
   private String email;
   /**
    * Case insensitive equals filter on the first name
    */
   @QueryParam("firstName")
   private String firstName;
   /**
    * Case insensitive equals filter on the last name
    */
   @QueryParam("lastName")
   private String lastName;
   /**
    * Filters by people who have validated at least on occurrence in the portal.
    */
   @QueryParam("validator")
   private Boolean validator;
   /**
    * Case sensitve filter on the externalKey property
    */
   @QueryParam("externalKey")
   private String externalKey;

   public PeopleFilter()
   {
      super();
   }

   public PeopleFilter(int portalId)
   {
      super();
      this.portalId = portalId;
   }

   public PeopleFilter(int portalId, Boolean validator)
   {
      super();
      this.portalId = portalId;
      this.validator = validator;
   }

   public PeopleFilter(int portalId, String nameContains, String email,
         Boolean validator)
   {
      this();
      this.portalId = portalId;
      this.nameContains = nameContains;
      this.email = email;
      this.validator = validator;
   }

   public int getPortalId()
   {
      return portalId;
   }

   public void setPortalId(int portalId)
   {
      this.portalId = portalId;
   }

   public String getNameContains()
   {
      return nameContains;
   }

   public void setNameContains(String nameContains)
   {
      this.nameContains = nameContains;
   }

   public String getEmail()
   {
      return email;
   }

   public PeopleFilter setEmail(String email)
   {
      this.email = email;
      return this;
   }

   public Boolean getValidator()
   {
      return validator;
   }

   public PeopleFilter setValidator(Boolean validator)
   {
      this.validator = validator;
      return this;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public PeopleFilter setFirstName(String firstName)
   {
      this.firstName = firstName;
      return this;
   }

   public String getLastName()
   {
      return lastName;
   }

   public PeopleFilter setLastName(String lastName)
   {
      this.lastName = lastName;
      return this;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public PeopleFilter setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
      return this;
   }

   @Override
   public String toString()
   {
      return PeopleFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PeopleFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PeopleFilterBeanUtil.doEquals(this, obj);
   }

}
