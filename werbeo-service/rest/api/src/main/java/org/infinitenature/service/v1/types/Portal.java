package org.infinitenature.service.v1.types;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Portal extends PortalBase
{
   /**
    * The configuration of this portal.
    */
   private PortalConfiguration config;

   public Portal()
   {
      
   }
   
   public Portal(Integer id)
   {
      setEntityId(id);
   }
   
   public PortalConfiguration getConfig()
   {
      return config;
   }

   public void setConfig(PortalConfiguration config)
   {
      this.config = config;
   }

   @Override
   public String toString()
   {
      return PortalBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PortalBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalBeanUtil.doEquals(this, obj);
   }
}
