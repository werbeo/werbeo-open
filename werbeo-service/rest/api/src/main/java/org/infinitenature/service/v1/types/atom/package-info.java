@XmlSchema(namespace = Link.ATOM_NAMESPACE, elementFormDefault = XmlNsForm.QUALIFIED, xmlns = {
      @XmlNs(prefix = "atom", namespaceURI = Link.ATOM_NAMESPACE) })

package org.infinitenature.service.v1.types.atom;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
