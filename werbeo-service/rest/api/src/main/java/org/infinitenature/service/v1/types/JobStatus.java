package org.infinitenature.service.v1.types;

public enum JobStatus
{
   // before
   CREATING,
   INITIALIZED,INITIALIZED_CSV,
   PREPARED,
   // while
   RUNNING,RUNNING_CSV,
   // after
   CANCELD, FINISHED, REMOVED, FAILURE
   // running a job
}
