package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.PortalBase;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class PortalSliceResponse extends SliceResponse<PortalBase>
{

   public PortalSliceResponse()
   {
      super();
   }

   public PortalSliceResponse(List<PortalBase> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "portals")
   @XmlElement(name = "portal")
   public List<PortalBase> getContent()
   {
      return content;
   }

   public void setContent(List<PortalBase> content)
   {
      this.content = content;
   }

   @Override
   public String toString()
   {
      return PortalSliceResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PortalSliceResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalSliceResponseBeanUtil.doEquals(this, obj);
   }
}
