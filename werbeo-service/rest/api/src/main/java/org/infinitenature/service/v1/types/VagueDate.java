package org.infinitenature.service.v1.types;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

import net.vergien.beanautoutils.annotation.Bean;

/**
 * Describes the date. Allows vague dates like in June 2017.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class VagueDate
{
   /**
    * The start date. Needed for all submissions, expect {@link VagueDateType}.TO_YEAR
    */
   @XmlAttribute
   private LocalDate from;

   /**
    * The end date. Needed for submissions of the {@link VagueDateType} DAYS, TO_YEAR and
    * YEARS
    */
   @XmlAttribute
   private LocalDate to;
   /**
    * The type of the date
    */
   @XmlAttribute
   private VagueDateType type;

   /**
    * The type of the date
    *
    */
   public enum VagueDateType
   {
      /**
       * A precise day
       */
      DAY,
      /**
       * An interval between tow days
       */
      DAYS,
      /**
       * A month in a year
       */
      MONTH_IN_YEAR,
      /** A year */
      YEAR,
      /** It's only known that the date is before a certain year */
      TO_YEAR,
      /**
       * Don't use, you always can use today as an end date
       */
      @Deprecated
      FROM_YEAR,
      /** An interval between tow years */
      YEARS;
   }

   public LocalDate getFrom()
   {
      return from;
   }

   public void setFrom(LocalDate from)
   {
      this.from = from;
   }

   public LocalDate getTo()
   {
      return to;
   }

   public void setTo(LocalDate to)
   {
      this.to = to;
   }

   public VagueDateType getType()
   {
      return type;
   }

   public void setType(VagueDateType type)
   {
      this.type = type;
   }

   @Override
   public String toString()
   {
      return VagueDateBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return VagueDateBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return VagueDateBeanUtil.doEquals(this, obj);
   }
   
   public static String format(VagueDate date, boolean withLabel)
   {
      // first simple approach!
      String caption = "";
      if(date != null)
      {
         String dateFormat = "dd.MM.yyyy";
         if(date.getType().equals(VagueDate.VagueDateType.YEARS))
         {
            dateFormat = "yyyy";
         }
         
         if(date.getFrom() != null)
         {
            caption+= new SimpleDateFormat(dateFormat).format(Date.from(date.getFrom().atStartOfDay(
                  ZoneId.systemDefault()).toInstant()));
         }
         if (date.getType().equals(VagueDate.VagueDateType.DAY))
         {
            // nothing to do
         } 
         else
         {
            caption += " - ";
         
            if (date.getTo() != null)
            {
               
               caption += new SimpleDateFormat(dateFormat)
                     .format(Date.from(date.getTo()
                           .atStartOfDay(ZoneId.systemDefault()).toInstant()));
            }
         }
      }
      if(StringUtils.isBlank(caption))
      {
         caption = withLabel ?"Datum der Aufnahme: ..." : "";
      }
      else
      {
         caption = withLabel? "Datum der Aufnahme: " + caption :"" + caption;
      }
      return  caption;
   }
}
