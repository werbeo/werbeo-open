package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.MTBResponse;
import org.infinitenature.service.v1.types.response.TaxonBaseSliceResponse;
import org.infinitenature.service.v1.types.response.TaxonFieldConfigResponse;
import org.infinitenature.service.v1.types.response.TaxonResponse;
import org.infinitenature.service.v1.types.response.TaxonSliceResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/taxa")
public interface TaxaResource
{
   @GET
   @Path("{id}")
   public TaxonResponse getTaxon(@PathParam("portalId") @Min(1) int portalId,
         @PathParam("id") int id);

   @GET
   public TaxonBaseSliceResponse find(
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @BeanParam TaxaFilter taxaFilter);

   @Path("count")
   @GET
   public CountResponse count(@BeanParam TaxaFilter taxaFilter);

   @GET
   @Path("{id}/synonyms")
   @Produces(MediaType.APPLICATION_JSON)
   public TaxonSliceResponse findAllSynonyms(
         @PathParam("portalId") @Min(1) int portalId, @PathParam("id") int id);

   @GET
   @Path("{id}/coveredMTB")
   public MTBResponse getCoveredMTB(@PathParam("portalId") @Min(1) int portalId,
         @PathParam("id") int id,
         @QueryParam("inclusiveChildTaxa") @DefaultValue("true") boolean inclusiveChildTaxa);

   @Path("meta")
   @GET
   /**
    * Returns the portal wide field configuration
    *
    * @param portalId
    * @return
    */
   public TaxonFieldConfigResponse getFieldConfig(
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Returns the survey specific field configuration
    *
    * @param portalId
    * @param surveyId
    * @return
    */
   @Path("meta/{surveyId}")
   @GET
   public TaxonFieldConfigResponse getFieldConfig(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("surveyId") @Min(1) int surveyId);

}
