package org.infinitenature.service.v1.types;

import javax.validation.constraints.NotNull;

import org.infinitenature.service.v1.types.meta.HasEnumTranslationKey;
import org.infinitenature.service.v1.types.support.BaseUUID;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SampleBase extends BaseUUID
{
   public enum SampleMethod implements HasEnumTranslationKey<SampleMethod>
   {
      FIELD_OBSERVATION, UNKNOWN,
      /**
       * Die Anzahl der Individuen pro Art wird für eine definierte Probefläche
       * (Plot) geschätzt.
       */
      ESTIMINATION,
      /**
       * Erfassung der Anzahl aller Individuen pro Art mittels Sicht und Verhör
       * entlang einer in seiner Länge und Breite definierten Strecke
       * (Transektes) (Ingrisch & Köhler 1998; Gardiner et al. 2009).
       */
      TRANSECT_COUNT,
      /**
       * MTB(Q) Rasterkartierung
       */
      QUADRAT,
      /**
       * line, with or without methodological description
       */
      TRANSECT, NET,
      /**
       * Zur Erfassung von Heuschreckengemeinschaften werden Bodenfallen nur
       * selten eingesetzt, da Krautschichtbewohner mit dieser Methode weniger
       * gut erfasst werden als geophile Arten (Ingrisch & Köhler 1998; Gardiner
       * et al. 2009). Der alleinige Einsatz von Bodenfallen ist daher nur in
       * sehr vegetationsarmen Habitaten sinnvoll (Schirmel et al. 2009).
       * Bodenfallen liefern hauptsächlich Informationen zur Aktivitätsdichte,
       * Aktivitätsdominanz und Jahresdynamik von Heuschrecken (Ingrisch &
       * Köhler 1998). Ein standardisiertes Verfahren gibt es bislang nicht. In
       * den vorliegenden Studien mit Heuschrecken wurden zwischen 2 und 24
       * Bodenfallen pro Probefläche eingesetzt und zum Teil sehr
       * unterschiedliche Fangflüssigkeiten verwendet (Ingrisch & Köhler 1998).
       */
      PITFALL_TRAP, LIGHT_TRAP, TRANSECT_SECTION, TIMED_COUNT,
      /**
       * Die Erfassung von Heuschrecken mittels Kescher gehört zu den
       * halbquantitativen Erfassungsmethoden, da zwar die Individuen pro
       * Kescherschlag (relative Abundanz) ermittelt werden können, nicht aber
       * die Individuen pro Quadratmeter (vgl. Gardiner et al. 2005). Eine
       * Standardisierung ergibt sich durch die Festlegung der Kescherschläge
       * pro Probefläche oder Transekt (Ingrisch & Köhler 1998). Kleinert (1992)
       * hat zum Beispiel die Heuschreckenfauna verschiedener Habitattypen
       * erfasst, indem sie innerhalb der Habitate einen einheitlich großen
       * Bereich (hier 200 m²) mit 100 Kescherschlägen beprobt hat.
       */
      SWEEP_NETTING,
      /**
       * Ein Isolationsquadrat (IQ) ist ein mit hellem Stoff bespanntes Gestell,
       * das oben und unten offen ist (Ingrisch & Köhler 1998; Gardiner et al.
       * 2009). Es wird gegen die Sonne (kein Schattenwurf!) auf die Vegetation
       * aufgesetzt und die darin befindlichen Heuschrecken ausgezählt. Bewährt
       * hat sich eine beprobte Grundfläche von 20 m² (z.B. Behrens & Fartmann
       * 2004; Poniatowski & Fartmann 2008; Helbing et al. 2014; Löffler &
       * Fartmann 2017). Ein 2 m² großes IQ muss folglich zehnmal zufällig
       * innerhalb der Probefläche aufgesetzt werden. Nach Ingrisch & Köhler
       * (1998) liefert diese Methode die besten Dicht-Werte bei noch
       * vertretbarem Zeitaufwand.
       */
      BOX_QUADRAT,
      /**
       * Ein Motorsauger ist für die Erfassung einiger Tiergruppen sehr gut
       * geeignet (Stewart 2002; Sanders & Entling 2011). Insbesondere für
       * Tiergruppen mit überwiegend kleinen bzw. leichten Arten, die sich gut
       * einsaugen lassen, stellte der Motorsauger eine effiziente
       * Nachweismethode dar. Als alleinige Erfassungsmethode für Heuschrecken
       * ist der Motorsauger hingegen nicht geeignet, da große und/oder mobile
       * Heuschreckenarten nicht standardisiert erfassbar sind.
       */
      SUCTION_SAMPLER, OTHER_METHOD;
   }

   /**
    * The method used for this sample.
    */
   private SampleMethod sampleMethod;

   /**
    * The date when sampling occured.
    */
   private VagueDate date;

   /**
    * Describes the position of this sample.
    */
   @NotNull
   private Locality locality;

   /**
    * The survey to which this sample belongs.
    */
   @NotNull
   private SurveyBase survey;

   /**
    * The person who did this sample.
    */
   private Person recorder;

   public SampleMethod getSampleMethod()
   {
      return sampleMethod;
   }

   public void setSampleMethod(SampleMethod sampleMethod)
   {
      this.sampleMethod = sampleMethod;
   }

   public VagueDate getDate()
   {
      return date;
   }

   public void setDate(VagueDate date)
   {
      this.date = date;
   }

   public Locality getLocality()
   {
      return locality;
   }

   public void setLocality(Locality locality)
   {
      this.locality = locality;
   }

   public SurveyBase getSurvey()
   {
      return survey;
   }

   public void setSurvey(SurveyBase survey)
   {
      this.survey = survey;
   }

   public Person getRecorder()
   {
      return recorder;
   }

   public void setRecorder(Person recorder)
   {
      this.recorder = recorder;
   }

   @Override
   public String toString()
   {
      return SampleBaseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleBaseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleBaseBeanUtil.doEquals(this, obj);
   }
}
