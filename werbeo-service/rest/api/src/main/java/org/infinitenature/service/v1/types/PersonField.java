package org.infinitenature.service.v1.types;

public enum PersonField
{
   ID, NAME
}
