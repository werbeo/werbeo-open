package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement(name = "serviceInfo")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceInfo
{
   private String version;
   private String buildDate;

   public String getVersion()
   {
      return version;
   }

   public void setVersion(String version)
   {
      this.version = version;
   }

   public String getBuildDate()
   {
      return buildDate;
   }

   public void setBuildDate(String buildDate)
   {
      this.buildDate = buildDate;
   }

   @Override
   public String toString()
   {
      return ServiceInfoBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ServiceInfoBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ServiceInfoBeanUtil.doEquals(this, obj);
   }
}
