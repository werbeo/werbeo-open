package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.infinitenature.service.v1.resources.TranslationResource;
import org.infinitenature.service.v1.types.meta.HasEnumTranslationKey;

import net.vergien.beanautoutils.annotation.Bean;

/**
 * A value for enumerated data.
 */
@Bean
@XmlAccessorType(XmlAccessType.FIELD)
public class Value implements Comparable<Value>
{
   public Value()
   {
      super();
   }

   public <T extends Enum<T>> Value(HasEnumTranslationKey<T> enumValue)
   {
      this(enumValue.name(), enumValue.getTranslationKey(),
            ((T) enumValue).ordinal());
   }

   private Value(String value, String translationKey, int ordinal)
   {
      super();
      this.value = value;
      this.translationKey = translationKey;
      this.ordinal = ordinal;
   }

   /**
    * The value itself.
    */
   private String value;
   /**
    * The key to get a translation for the value.
    *
    * @see TranslationResource#getTranslation(int, String, String)
    */
   private String translationKey;

   @XmlTransient
   private int ordinal = 0;

   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   public String getTranslationKey()
   {
      return translationKey;
   }

   public void setTranslationKey(String translationKey)
   {
      this.translationKey = translationKey;
   }

   public int getOrdinal()
   {
      return ordinal;
   }

   public void setOrdinal(int ordinal)
   {
      this.ordinal = ordinal;
   }

   @Override
   public String toString()
   {
      return ValueBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ValueBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ValueBeanUtil.doEquals(this, obj);
   }

   @Override
   public int compareTo(Value o)
   {
      return Integer.compare(this.ordinal, o.ordinal);
   }
}
