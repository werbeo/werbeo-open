package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class SampleComment extends AbstractComment
{
   public SampleComment()
   {
      super();
   }

   public SampleComment(String comment)
   {
      super(comment);
   }

   @Override
   public String toString()
   {
      return SampleCommentBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleCommentBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleCommentBeanUtil.doEquals(this, obj);
   }
}
