package org.infinitenature.service.support.xml.converter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

@Provider
public class LocalDateConverterProvider implements ParamConverterProvider
{

   @Override
   public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType,
         Annotation[] annotations)
   {
      if (rawType.equals(LocalDate.class))
      {
         return (ParamConverter<T>) new LocalDateParamConverter();
      }
      else if (rawType.equals(LocalDateTime.class))
      {
         return (ParamConverter<T>) new LocalDateTimeParamConverter();
      }
      return null;
   }

}
