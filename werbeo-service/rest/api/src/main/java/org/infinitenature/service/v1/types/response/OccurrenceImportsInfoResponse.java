package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.OccurrenceImport;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class OccurrenceImportsInfoResponse extends SliceResponse<OccurrenceImport>
{

   public OccurrenceImportsInfoResponse()
   {
      super();
   }

   public OccurrenceImportsInfoResponse(List<OccurrenceImport> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "occurrenceImports")
   @XmlElement(name = "occurrenceImport")
   @JsonProperty("occurrenceImports")
   public List<OccurrenceImport> getContent()
   {
      return content;
   }

   public void setContent(List<OccurrenceImport> content)
   {
      this.content = content;
   }

   @Override
   public String toString()
   {
      return OccurrenceImportsInfoResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceImportsInfoResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceImportsInfoResponseBeanUtil.doEquals(this, obj);
   }
}
