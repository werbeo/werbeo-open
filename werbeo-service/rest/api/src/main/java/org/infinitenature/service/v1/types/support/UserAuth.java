package org.infinitenature.service.v1.types.support;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserAuth
{

   private String email;
   private String password;

   @XmlAttribute
   public String getEmail()
   {
      return email;
   }

   public UserAuth setEmail(String email)
   {
      this.email = email;
      return this;
   }

   @XmlAttribute
   public String getPassword()
   {
      return password;
   }

   public UserAuth setPassword(String password)
   {
      this.password = password;
      return this;
   }
}
