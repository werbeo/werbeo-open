package org.infinitenature.service.v1.types.response;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TranslationResponse extends BaseResponse
{
   private Map<String, String> translations = new HashMap<String, String>();

   public TranslationResponse()
   {
      super();
   }

   public TranslationResponse(Map<String, String> translations)
   {
      super();
      this.translations = translations;
   }

   public Map<String, String> getTranslations()
   {
      return translations;
   }

   public void setTranslations(Map<String, String> translations)
   {
      this.translations = translations;
   }

}
