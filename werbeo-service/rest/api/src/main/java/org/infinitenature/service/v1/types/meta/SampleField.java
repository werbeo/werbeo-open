package org.infinitenature.service.v1.types.meta;

public enum SampleField implements EntityField<SampleField>
{
   ID("id"), //
   DATE("date"), //
   LOCATION("locality"), //
   LOCATION_MTB(null), //
   RECORDER("recorder"), //
   SURVEY("survey"), //
   LOCATION_COMMENT("locality.locationComment"), //
   LOCALITY("locality.locality"), //
   SAMPLE_METHOD("sampleMethod"), //
   BLUR("blur");

   private final String property;

   private SampleField(String property)
   {
      this.property = property;
   }

   @Override
   public String getProperty()
   {
      return property;
   }

   @Override
   public String getTranslationKeyPrefix()
   {
      return "Sample";
   }
}
