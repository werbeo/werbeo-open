package org.infinitenature.service.v1.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.response.ServiceInfoResponse;
import org.infinitenature.service.v1.types.response.UserInfoResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

/**
 * Information about the service
 *
 */
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/v1/info")
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
public interface InfoResource
{
   /**
    * Gets the current user using the service
    *
    * @RequestHeader Authorization the openID bearer authentication token
    *
    * @return
    */
   @GET
   @Path("serviceUser")
   public UserInfoResponse getServiceUser();

   @GET
   public ServiceInfoResponse getServiceInfo();

}
