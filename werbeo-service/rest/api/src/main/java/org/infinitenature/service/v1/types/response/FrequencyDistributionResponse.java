package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.FrequencyDistribution;

@XmlRootElement
public class FrequencyDistributionResponse extends BaseResponse
{
   private FrequencyDistribution frequencyDistribution;

   public FrequencyDistributionResponse()
   {
      super();
   }

   public FrequencyDistributionResponse(
         FrequencyDistribution frequencyDistribution)
   {
      super();
      this.frequencyDistribution = frequencyDistribution;
   }

   public FrequencyDistribution getFrequencyDistribution()
   {
      return frequencyDistribution;
   }

   public void setFrequencyDistribution(
         FrequencyDistribution frequencyDistribution)
   {
      this.frequencyDistribution = frequencyDistribution;
   }

}
