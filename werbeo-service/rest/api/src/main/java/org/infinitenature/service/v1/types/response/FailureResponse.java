package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class FailureResponse extends BaseResponse
{
   private String cause;

   public FailureResponse()
   {
      super();
   }

   public FailureResponse(String cause)
   {
      super();
      this.cause = cause;
   }

   public String getCause()
   {
      return cause;
   }

   public void setCause(String cause)
   {
      this.cause = cause;
   }

   @Override
   public String toString()
   {
      return FailureResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return FailureResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return FailureResponseBeanUtil.doEquals(this, obj);
   }
}
