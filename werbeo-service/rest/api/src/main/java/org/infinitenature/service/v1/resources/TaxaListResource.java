package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.TaxaListField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.TaxaListResponse;
import org.infinitenature.service.v1.types.response.TaxaListSliceResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/taxalist")
public interface TaxaListResource
{
   @GET
   public TaxaListSliceResponse find(
         @PathParam("portalId") @Min(1) int portalId,
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("ID") @QueryParam("sortField") TaxaListField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder);

   @Path("count")
   @GET
   public CountResponse count(@PathParam("portalId") @Min(1) int portalId);

   @GET
   @Path("{id}")
   public TaxaListResponse getTaxaList(
         @PathParam("portalId") @Min(1) int portalId, @PathParam("id") int id);
}
