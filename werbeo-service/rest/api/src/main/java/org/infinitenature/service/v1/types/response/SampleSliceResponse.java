package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Sample;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class SampleSliceResponse
      extends SliceResponse<Sample>
{
   public SampleSliceResponse()
   {
      super();
   }

   public SampleSliceResponse(List<Sample> content, int size)
   {
      super(content, size);
   }


   @XmlElementWrapper(name = "samples")
   @XmlElement(name = "sample")
   @JsonProperty("samples")
   public List<Sample> getContent()
   {
      return content;
   }

   public void setContent(List<Sample> content)
   {
      this.content = content;
   }

   @Override
   public String toString()
   {
      return SampleSliceResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleSliceResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleSliceResponseBeanUtil.doEquals(this, obj);
   }
}
