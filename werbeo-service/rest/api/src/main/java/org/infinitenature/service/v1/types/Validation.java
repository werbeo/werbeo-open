package org.infinitenature.service.v1.types;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlAccessorType(XmlAccessType.FIELD)
public class Validation
{
   @XmlAttribute
   private ValidationStatus status;
   private Person validator;
   private LocalDateTime validationTime;
   private String comment;

   public Validation()
   {
      super();
   }

   public Validation(ValidationStatus status)
   {
      this();
      this.status = status;
   }

   public Validation(ValidationStatus status, String comment)
   {
      this(status);
      this.comment = comment;
   }

   public ValidationStatus getStatus()
   {
      return status;
   }

   public void setStatus(ValidationStatus status)
   {
      this.status = status;
   }

   public Person getValidator()
   {
      return validator;
   }

   public void setValidator(Person validator)
   {
      this.validator = validator;
   }

   public LocalDateTime getValidationTime()
   {
      return validationTime;
   }

   public void setValidationTime(LocalDateTime valdationTime)
   {
      this.validationTime = valdationTime;
   }

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   @Override
   public String toString()
   {
      return ValidationBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ValidationBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ValidationBeanUtil.doEquals(this, obj);
   }
}
