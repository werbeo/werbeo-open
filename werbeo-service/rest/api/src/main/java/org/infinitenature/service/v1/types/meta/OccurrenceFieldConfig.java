package org.infinitenature.service.v1.types.meta;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.types.Value;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceFieldConfig implements FieldConfig<OccurrenceField>
{
   private OccurrenceField field;
   private boolean mandantory;
   private String property;
   private String translationKey;
   private List<Value> values = new ArrayList<>();

   public OccurrenceFieldConfig()
   {
      super();
   }

   public OccurrenceFieldConfig(OccurrenceField field, boolean mandantory)
   {
      super();
      this.field = field;
      this.mandantory = mandantory;
      this.property = field.getProperty();
      this.translationKey = field.getTranslationKey();

   }

   @Override
   public OccurrenceField getField()
   {
      return field;
   }

   @Override
   public void setField(OccurrenceField field)
   {
      this.field = field;
      if (field != null)
      {
         this.property = field.getProperty();
         this.translationKey = field.getTranslationKey();
      }
   }

   @Override
   public boolean isMandantory()
   {
      return mandantory;
   }

   @Override
   public void setMandantory(boolean mandantory)
   {
      this.mandantory = mandantory;
   }

   public String getProperty()
   {
      return property;
   }

   public void setProperty(String property)
   {
      this.property = property;
   }

   @Override
   public String getTranslationKey()
   {
      return translationKey;
   }

   public void setTranslationKey(String translationKey)
   {
      this.translationKey = translationKey;
   }

   @Override
   public String toString()
   {
      return OccurrenceFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceFieldConfigBeanUtil.doEquals(this, obj);
   }

   @Override
   public List<Value> getValues()
   {
      return values;
   }

   public void setValues(List<Value> values)
   {
      this.values = values;
   }
}
