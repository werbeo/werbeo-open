package org.infinitenature.service.v1.types.meta;

import java.util.List;

import org.infinitenature.service.v1.resources.TranslationResource;
import org.infinitenature.service.v1.types.Value;

public interface FieldConfig<T extends Enum & EntityField>
{
   /**
    * The name of the field.
    */
   T getField();

   void setField(T field);

   /**
    * Indicates weather a field is mandatory or not.
    *
    */
   boolean isMandantory();

   void setMandantory(boolean mandantory);

   /**
    * key for getting a translation at
    * {@link TranslationResource#getTranslation(int, String, String)}
    *
    * @return
    */
   String getTranslationKey();

   /**
    * If this field is enumerated, the allowed values are listed here.
    * <p>
    * <b>Note:</b> Depending on the configuration there might be less allowed
    * values than in the api-doc.
    */
   List<Value> getValues();
}
