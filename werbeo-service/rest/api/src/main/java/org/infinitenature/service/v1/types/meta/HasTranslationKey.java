package org.infinitenature.service.v1.types.meta;

public interface HasTranslationKey
{
   public String getTranslationKey();
}
