package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.SurveyBase;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class SurveyBaseResponse extends BaseResponse
{
   private SurveyBase survey;

   public SurveyBaseResponse()
   {
      super();
   }

   public SurveyBaseResponse(SurveyBase survey)
   {
      super();
      this.survey = survey;
   }

   public SurveyBase getSurvey()
   {
      return survey;
   }

   public void setSurvey(SurveyBase survey)
   {
      this.survey = survey;
   }

   @Override
   public String toString()
   {
      return SurveyBaseResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyBaseResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyBaseResponseBeanUtil.doEquals(this, obj);
   }
}
