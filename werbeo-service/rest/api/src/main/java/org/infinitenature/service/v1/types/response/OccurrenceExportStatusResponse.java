package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.OccurrenceExport;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class OccurrenceExportStatusResponse extends BaseResponse
{
   private OccurrenceExport status;

   public OccurrenceExportStatusResponse()
   {
      super();
   }

   public OccurrenceExportStatusResponse(OccurrenceExport status)
   {
      super();
      this.status = status;
   }

   public OccurrenceExport getStatus()
   {
      return status;
   }

   public void setStatus(OccurrenceExport status)
   {
      this.status = status;
   }


   @Override
   public String toString()
   {
      return OccurrenceExportStatusResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceExportStatusResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceExportStatusResponseBeanUtil.doEquals(this, obj);
   }
}
