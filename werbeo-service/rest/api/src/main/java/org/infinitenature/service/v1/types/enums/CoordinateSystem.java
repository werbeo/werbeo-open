package org.infinitenature.service.v1.types.enums;

   public enum CoordinateSystem
   {
      EPSG4326,
      EPSG4745,
      EPSG5678,
      EPSG31467,
      EPSG31468,
      EPSG31469,
      EPSG4258,
      EPSG25832,
      EPSG25833,
      EPSG258337,
      EPSG900913,
      EPSG5650,
      EPSG3857;
   }

