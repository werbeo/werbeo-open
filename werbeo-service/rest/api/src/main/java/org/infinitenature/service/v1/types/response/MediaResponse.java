package org.infinitenature.service.v1.types.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Document;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class MediaResponse extends BaseResponse
{
   private List<Document> documents = new ArrayList<>();

   public MediaResponse()
   {
      super();
   }

   public MediaResponse(List<Document> documents)
   {
      super();
      this.documents = documents;
   }

   public List<Document> getDocuments()
   {
      return documents;
   }

   public void setDocuments(List<Document> documents)
   {
      this.documents = documents;
   }

   @Override
   public String toString()
   {
      return MediaResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return MediaResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return MediaResponseBeanUtil.doEquals(this, obj);
   }
}
