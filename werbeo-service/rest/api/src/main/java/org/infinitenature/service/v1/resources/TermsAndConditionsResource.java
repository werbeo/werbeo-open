package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.response.TermsAndConditonsAcceptResponse;
import org.infinitenature.service.v1.types.response.TextResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/tc")
public interface TermsAndConditionsResource
{
   /**
    * Accepts the terms and conditions and privacy declaration for the current
    * user.
    *
    * @param portalId
    * @return
    */
   @Path("/accept")
   @POST
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user must be authorized to accept terms and conditions") })
   public TermsAndConditonsAcceptResponse accept(
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Return an introduction text to terms and conditions and privacy
    * declaration.
    *
    * @see TermsAndConditionsResource#getConditions(int)
    * @see TermsAndConditionsResource#getPrivacyDeclaration(int)
    *
    * @param portalId
    * @return
    */
   @Path("/acceptance")
   @GET
   public TextResponse getConditions(
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Returns the text of privacy declaration
    *
    * @param portalId
    * @return
    */
   @Path("/privacy")
   @GET
   public TextResponse getPrivacyDeclaration(
         @PathParam("portalId")
         @Min(1) int portalId);

   /**
    * Returns the text of the terms of conditions
    *
    * @param portalId
    * @return
    */
   @Path("/toc")
   @GET
   public TextResponse getToc(
         @PathParam("portalId")
         @Min(1) int portalId);
}
