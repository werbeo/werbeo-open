package org.infinitenature.service.v1.resources;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.SampleCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/samples/{uuid}/comments")
public interface SampleCommentsResource
{
   @GET
   public SampleCommentsSliceResponse getComments(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID uuid, @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("1000") @QueryParam("limit") @Min(1) int limit,
         @DefaultValue("MOD_DATE") @QueryParam("sortField") CommentField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder);

   @POST
   public SaveOrUpdateResponse saveComment(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID uuid, SampleComment sampleComment);

   @Path("count")
   @GET
   public CountResponse countComments(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID sampleId);
}
