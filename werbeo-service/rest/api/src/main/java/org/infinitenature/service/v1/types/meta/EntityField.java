package org.infinitenature.service.v1.types.meta;

public interface EntityField<T extends Enum<T>> extends HasEnumTranslationKey<T>
{
   public String getProperty();
}
