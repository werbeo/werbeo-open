package org.infinitenature.service.v1.types.response;

import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class OccurrenceExportResponse extends BaseResponse
{
   public OccurrenceExportResponse()
   {
      super();
   }
   public OccurrenceExportResponse(UUID exportId)
   {
      super();
      this.exportId = exportId;
   }

   private UUID exportId;

   public UUID getExportId()
   {
      return exportId;
   }

   public void setExportId(UUID exportId)
   {
      this.exportId = exportId;
   }

   @Override
   public String toString()
   {
      return OccurrenceExportResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceExportResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceExportResponseBeanUtil.doEquals(this, obj);
   }
}
