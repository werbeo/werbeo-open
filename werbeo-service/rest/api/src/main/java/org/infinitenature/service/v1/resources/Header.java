package org.infinitenature.service.v1.resources;

public class Header
{
   public static final String WERBEO_REQUEST_SOURCE_ID_NAME = "werbeo-request-source-id";
   public static final String WERBEO_REQUEST_SOURCE_ID_DESCRIPTION = "A request id set by the client. Used to trace requests in log files.";

}
