package org.infinitenature.service.v1.types;

public enum PortalField
{
   ID, NAME, URL
}
