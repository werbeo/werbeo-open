package org.infinitenature.service.v1.resources;

import java.time.LocalDate;

import javax.ws.rs.FormParam;

import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceFilterPOST extends OccurrenceFilter
{
   
   public OccurrenceFilterPOST()
   {
      super();
   }
   
   /**
    * The geometry in epsg:4326. By default he position of the occurrence needs
    * to be <b>within</b> this. This is controlled by the
    * {@link OccurrenceFilterPOST#wktMode} property
    */
   
   
   public OccurrenceFilterPOST(int portalId, LocalDate from, LocalDate to,
         Integer taxonId, String wkt, String mtb, Integer maxBlur,
         boolean includeChildTaxa, String owner, Integer surveyId,
         Boolean absence, String herbarium, ValidationStatus valdiationStatus,
         String validator, String occExternalKey)
   {
      super();
      this.portalId = portalId;
      this.from = from;
      this.to = to;
      this.taxonId = taxonId;
      this.wkt = wkt;
      this.mtb = mtb;
      this.maxBlur = maxBlur;
      this.includeChildTaxa = includeChildTaxa;
      this.owner = owner;
      this.surveyId = surveyId;
      this.absence = absence;
      this.herbarium = herbarium;
      this.validationStatus = valdiationStatus;
      this.validator = validator;
      this.occExternalKey = occExternalKey;
   }
   
   @FormParam("wkt")
   private String wkt;

   public String getWkt()
   {
      return wkt;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

  
}