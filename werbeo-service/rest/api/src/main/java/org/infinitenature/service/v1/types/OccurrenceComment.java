package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class OccurrenceComment extends AbstractComment
{
   public OccurrenceComment()
   {
      super();
   }

   public OccurrenceComment(String comment)
   {
      super(comment);
   }

   @Override
   public String toString()
   {
      return OccurrenceCommentBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceCommentBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceCommentBeanUtil.doEquals(this, obj);
   }
}
