package org.infinitenature.service.v1.types.response;

import org.infinitenature.service.v1.types.Person;

public class PersonResponse extends BaseResponse
{
   private Person person;

   public PersonResponse()
   {
      super();
   }

   public PersonResponse(Person person)
   {
      super();
      this.person = person;
   }

   public Person getPerson()
   {
      return person;
   }

   public void setPerson(Person person)
   {
      this.person = person;
   }

}
