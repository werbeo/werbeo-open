package org.infinitenature.service.v1.types;

import org.infinitenature.service.v1.types.support.BaseInt;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class PortalBase extends BaseInt
{
   /**
    * The name of the portal.
    */
   private String name;
   /**
    * The URL of the portal. Usually there is a website at this address.
    */
   private String url;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getUrl()
   {
      return url;
   }

   public void setUrl(String url)
   {
      this.url = url;
   }

   @Override
   public String toString()
   {
      return PortalBaseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PortalBaseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalBaseBeanUtil.doEquals(this, obj);
   }
}
