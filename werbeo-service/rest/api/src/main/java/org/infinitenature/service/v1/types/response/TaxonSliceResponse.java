package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Taxon;

@XmlRootElement
public class TaxonSliceResponse extends SliceResponse<Taxon>
{

   public TaxonSliceResponse()
   {
      super();
   }

   public TaxonSliceResponse(List<Taxon> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "taxa")
   @XmlElement(name = "taxon")
   public List<Taxon> getContent()
   {
      return content;
   }

   public void setContent(List<Taxon> content)
   {
      this.content = content;
   }
}
