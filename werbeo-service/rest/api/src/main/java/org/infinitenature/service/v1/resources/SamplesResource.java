package org.infinitenature.service.v1.resources;

import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.SampleFieldConfigResponse;
import org.infinitenature.service.v1.types.response.SampleResponse;
import org.infinitenature.service.v1.types.response.SampleSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/samples")
public interface SamplesResource
{
   @Path("/{uuid}")
   @GET
   public SampleResponse get(@PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID uuid);

   @POST
   public SaveOrUpdateResponse save(@PathParam("portalId") @Min(1) int portalId,
         Sample sample);

   @Path("/{uuid}")
   @DELETE
   public DeletedResponse delete(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID uuid);

   /**
    * Returns occurrences
    *
    * @param offset
    * @param limit
    * @param sortField
    *           the property to sort by
    * @param sortOrder
    *           the sort order
    * @param filter
    * @param inlineThumbs
    *           if set to true {@link Document} of type
    *           {@link DocumentType#IMAGE_THUMB} contains a Data-URL in the
    *           {@link Link}
    * @return
    */
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   public SampleSliceResponse find(@Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("MOD_DATE") @QueryParam("sortField") OccurrenceField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder,
         @BeanParam @Valid OccurrenceFilter filter,
         @DefaultValue("false") @QueryParam("inlineThumbs") boolean inlineThumbs);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("count")
   @GET
   public CountResponse count(@BeanParam @Valid OccurrenceFilter filter);



   /**
    * Returns the portal specific field configuration
    *
    * @param portalId
    * @return
    */
   @Path("meta")
   @GET
   public SampleFieldConfigResponse getFieldConfig(
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Returns the survey specific filed configuration
    *
    * @param portalId
    * @param surveyId
    * @return
    */
   @Path("meta/{surveyId}")
   @GET
   public SampleFieldConfigResponse getFieldConfig(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("surveyId") @Min(1) int surveyId);
}
