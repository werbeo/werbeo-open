package org.infinitenature.service.support.xml.converter;

public class StringRequest
{
   public StringRequest(String s)
   {
      this.content = s;
   }

   public String getContent()
   {
      return content;
   }

   public void setContent(String content)
   {
      this.content = content;
   }

   private String content;
}
