package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlElement;

import org.infinitenature.service.v1.types.support.MetaData;

public abstract class BaseResponse
{
   private MetaData metaData = new MetaData();

   @XmlElement(name = "metaData")
   public MetaData getMetaData()
   {
      return metaData;
   }

   public void setMetaData(MetaData metaData)
   {
      this.metaData = metaData;
   }

   public void setSourceRequestId(String sourceRequestId)
   {
      this.metaData.setSessionId(sourceRequestId);
   }
}
