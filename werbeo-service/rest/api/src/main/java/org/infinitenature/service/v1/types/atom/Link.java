/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package org.infinitenature.service.v1.types.atom;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Value object for links.
 *
 * @author Oliver Gierke
 */
@XmlType(name = "link", namespace = Link.ATOM_NAMESPACE)
@JsonIgnoreProperties("templated")
public class Link implements Serializable
{

   public static final String ATOM_NAMESPACE = "http://www.w3.org/2005/Atom";

   private String rel;

   private String href;

   public Link()
   {
      super();
   }

   public Link(String rel, String href)
   {
      super();
      this.rel = rel;
      this.href = href;
   }

   /**
    * Returns the actual URI the link is pointing to.
    *
    * @return
    */
   @XmlAttribute
   public String getHref()
   {
      return href;
   }

   /**
    * Returns the rel of the link.
    *
    * @return
    */
   @XmlAttribute
   public String getRel()
   {
      return rel;
   }

   public void setRel(String rel)
   {
      this.rel = rel;
   }

   public void setHref(String href)
   {
      this.href = href;
   }

   /*
    * (non-Javadoc)
    *
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object obj)
   {

      if (this == obj)
      {
         return true;
      }

      if (!(obj instanceof Link))
      {
         return false;
      }

      Link that = (Link) obj;

      return this.href.equals(that.href) && this.rel.equals(that.rel);
   }

   /*
    * (non-Javadoc)
    *
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode()
   {

      int result = 17;
      result += 31 * href.hashCode();
      result += 31 * rel.hashCode();
      return result;
   }

   /*
    * (non-Javadoc)
    *
    * @see java.lang.Object#toString()
    */
   @Override
   public String toString()
   {
      return String.format("<%s>;rel=\"%s\"", href, rel);
   }
}
