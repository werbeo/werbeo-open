package org.infinitenature.service.v1.types;

public enum Language
{
   /**
    * German
    */
   DEU,
   /** Latin */
   LAT,
   ENG,
   CYM,
   GLA,
   GLE
}
