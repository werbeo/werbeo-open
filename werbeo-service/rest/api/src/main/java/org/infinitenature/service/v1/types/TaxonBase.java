package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.support.BaseInt;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement(name = "taxon-base")
@Bean
@JsonIgnoreProperties(ignoreUnknown=true)
public class TaxonBase extends BaseInt
{
   /**
    * The name of the taxon.
    */
   @JsonProperty("name")
   private String name;

   private String authority;

   /**
    * The species group this taxon belongs to.
    */
   private String group;

   /**
    * The taxa list for the taxon.
    */
   private TaxaList taxaList;
   /**
    * The key in an external reference list.
    */
   private String externalKey;

   /**
    * Indicates weather this taxon is the preferred one or a synonym / common
    * name.
    */
   private boolean preferred;

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getAuthority()
   {
      return authority;
   }

   public void setAuthority(String authority)
   {
      this.authority = authority;
   }

   public String getGroup()
   {
      return group;
   }

   public void setGroup(String group)
   {
      this.group = group;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public TaxaList getTaxaList()
   {
      return taxaList;
   }

   public void setTaxaList(TaxaList taxonList)
   {
      this.taxaList = taxonList;
   }

   public boolean isPreferred()
   {
      return preferred;
   }

   public void setPreferred(boolean preferred)
   {
      this.preferred = preferred;
   }

   @Override
   public String toString()
   {
      return TaxonBaseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonBaseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonBaseBeanUtil.doEquals(this, obj);
   }
}
