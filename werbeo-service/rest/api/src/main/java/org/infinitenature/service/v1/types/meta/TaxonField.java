package org.infinitenature.service.v1.types.meta;

public enum TaxonField implements EntityField<TaxonField>
{
   ID("id"), RED_LIST_STATUS("redListStatus"), LANGUAGE("language"), NAME(
         "name"), AUTHORITY("authority"), GROUP("group");

   private final String property;

   TaxonField(String property)
   {
      this.property = property;
   }

   @Override
   public String getProperty()
   {
      return property;
   }

   @Override
   public String getTranslationKeyPrefix()
   {
      return "Taxon";
   }
}
