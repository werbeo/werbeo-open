package org.infinitenature.service.v1.types.response;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.enums.Filter;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class FilterResponse extends BaseResponse
{
   private Map<Filter, Collection<PortalBase>> filters = new HashMap<>();

   public FilterResponse()
   {
      super();
   }

   public FilterResponse(Map<Filter, Collection<PortalBase>> filters)
   {
      super();
      this.filters = filters;
   }

   public Map<Filter, Collection<PortalBase>> getFilters()
   {
      return filters;
   }

   public void setFilters(Map<Filter, Collection<PortalBase>> filters)
   {
      this.filters = filters;
   }

   @Override
   public String toString()
   {
      return FilterResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return FilterResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return FilterResponseBeanUtil.doEquals(this, obj);
   }
}
