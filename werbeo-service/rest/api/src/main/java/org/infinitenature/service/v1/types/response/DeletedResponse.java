package org.infinitenature.service.v1.types.response;

import net.vergien.beanautoutils.annotation.Bean;

import javax.xml.bind.annotation.XmlRootElement;

@Bean
@XmlRootElement
public class DeletedResponse extends BaseResponse
{
   public DeletedResponse()
   {
      super();
   }

   @Override
   public String toString()
   {
      return DeletedResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return DeletedResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return DeletedResponseBeanUtil.doEquals(this, obj);
   }
}
