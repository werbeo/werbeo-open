package org.infinitenature.service.v1.types.response;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.SampleBase.SampleMethod;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class SampleFieldConfigResponse extends BaseResponse
{

   private Set<SampleFieldConfig> fieldConfigs = new HashSet<>();

   public SampleFieldConfigResponse()
   {
      super();
   }

   public SampleFieldConfigResponse(Set<SampleFieldConfig> fieldConfigs)
   {
      super();
      this.fieldConfigs = fieldConfigs;
   }

   @XmlElementWrapper(name = "fieldConfigs")
   @XmlElement(name = "fieldConfig")
   @JsonProperty("fieldConfigs")
   public Set<SampleFieldConfig> getFieldConfigs()
   {
      return fieldConfigs;
   }

   public void setFieldConfigs(Set<SampleFieldConfig> fieldConfigs)
   {
      this.fieldConfigs = fieldConfigs;
   }

   @Override
   public String toString()
   {
      return SampleFieldConfigResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleFieldConfigResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleFieldConfigResponseBeanUtil.doEquals(this, obj);
   }
}
