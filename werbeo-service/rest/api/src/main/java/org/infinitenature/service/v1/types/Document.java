package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.atom.Link;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement(name = "document")
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class Document
{
   @XmlAttribute(name = "type")
   private DocumentType type;
   @XmlElement(name = "link")
   private Link link;
   @XmlElement
   private String caption;

   public Document()
   {
      super();
   }

   public Document(DocumentType type, Link link)
   {
      super();
      this.type = type;
      this.link = link;
   }

   public Document(DocumentType type, Link link, String caption)
   {
      super();
      this.type = type;
      this.link = link;
      this.caption = caption;
   }

   public String getCaption()
   {
      return caption;
   }

   public void setCaption(String caption)
   {
      this.caption = caption;
   }

   public DocumentType getType()
   {
      return type;
   }

   public void setType(DocumentType type)
   {
      this.type = type;
   }

   public Link getLink()
   {
      return link;
   }

   public void setLink(Link link)
   {
      this.link = link;
   }

   @Override
   public String toString()
   {
      return DocumentBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return DocumentBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return DocumentBeanUtil.doEquals(this, obj);
   }
}
