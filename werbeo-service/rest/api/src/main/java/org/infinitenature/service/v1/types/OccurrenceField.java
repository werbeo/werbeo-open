package org.infinitenature.service.v1.types;

/**
 * Fields of the {@link Occurrence} entity
 *
 */
public enum OccurrenceField
{
   /**
    * the taxon of the {@link Occurrence}
    */
   TAXON, DETERMINER, DATE, MOD_DATE, MEDIA, TIME_OF_DAY, VALIDATION_STATUS;
}
