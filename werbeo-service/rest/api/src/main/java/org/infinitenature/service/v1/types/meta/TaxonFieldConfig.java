package org.infinitenature.service.v1.types.meta;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.resources.TranslationResource;
import org.infinitenature.service.v1.types.Value;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxonFieldConfig implements FieldConfig<TaxonField>
{
   private TaxonField field;
   private boolean mandantory;
   private String property;
   private List<Value> values = new ArrayList<>();
   /**
    * key for getting a translation at
    * {@link TranslationResource#getTranslation(int, String, String)}
    */
   private String translationKey;

   public TaxonFieldConfig()
   {
      super();
   }

   public TaxonFieldConfig(TaxonField field, boolean mandantory)
   {
      super();
      this.field = field;
      this.mandantory = mandantory;
      this.property = field.getProperty();
      this.translationKey = field.getTranslationKey();
   }

   @Override
   public TaxonField getField()
   {
      return this.field;
   }

   @Override
   public void setField(TaxonField field)
   {
      this.field = field;
      if (field != null)
      {
         this.property = field.getProperty();
         this.translationKey = field.getTranslationKey();
      }
   }

   @Override
   public boolean isMandantory()
   {
      return mandantory;
   }

   @Override
   public void setMandantory(boolean mandantory)
   {
      this.mandantory = mandantory;
   }

   public String getProperty()
   {
      return property;
   }

   public void setProperty(String property)
   {
      this.property = property;
   }

   @Override
   public String getTranslationKey()
   {
      return translationKey;
   }

   public void setTranslationKey(String translationKey)
   {
      this.translationKey = translationKey;
   }

   @Override
   public List<Value> getValues()
   {
      return values;
   }

   public void setValues(List<Value> values)
   {
      this.values = values;
   }

   @Override
   public String toString()
   {
      return TaxonFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonFieldConfigBeanUtil.doEquals(this, obj);
   }
}
