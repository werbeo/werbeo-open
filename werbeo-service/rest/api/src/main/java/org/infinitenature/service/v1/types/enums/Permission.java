package org.infinitenature.service.v1.types.enums;

public enum Permission
{
   PERSONS("PERSONS"), LOCATION_POINT("LOCATION_POINT"), LOCATION_MTBQ(
         "LOCATION_MTBQ"), LOCATION_MTBQQ(
               "LOCATION_MTBQQ"), LOCATION_MTBQQQ("LOCATION_MTBQQQ");

   private final String permissionName;

   private Permission(String permissionName)
   {
      this.permissionName = permissionName;
   }

   public String getPermissionName()
   {
      return permissionName;
   }

  

}
