package org.infinitenature.service.v1.types.meta;

public enum OccurrenceField implements EntityField<OccurrenceField>
{
   ID("id"), //
   DETERMINER("determiner"), //
   RECORD_STATUS("recordStatus"), //
   AMOUNT("amount"), //
   SETTLEMENT_STATUS("settlementStatus"), //
   TAXON("taxon"), //
   AREA("coveredArea"), //
   VITALITY("vitality"), //
   SETTLEMENT_STATUS_FUKAREK("settlementStatusFukarek"), //
   BLOOMING_SPROUTS("bloomingSprouts"), //
   QUANTITY("quantity"), //
   EXTERNAL_KEY("externalKey"), //
   OBSERVERS("observers"), //
   SAMPLE("sample"), //
   HERBARIUM("herbarium"), //
   DETERMINATION_COMMENT("determinationComment"), //
   NUMERIC_AMOUNT("numericAmount"), //
   REPRODUCTION("reproduction"), //
   SEX("sex"), //
   MEDIA("documents"),
   HABITAT("habitat"), //
   LIFE_STAGE("lifeStage"),
   MAKROPTER("makropter"),
   PUBLICATION("reference"), //
   ABSENCE("absence"),
   TIME_OF_DAY("timeOfDay"), NUMERIC_AMOUNT_ACCURACY(
         "numericAmountAccuracy"), VALIDATION("validation"), REMARK("remark"), CITE_ID("citeId"), CITE_COMMENT("citeComment");

   private final String property;
   private OccurrenceField(String property)
   {
      this.property = property;
   }

   @Override
   public String getProperty()
   {
      return property;
   }

   @Override
   public String getTranslationKeyPrefix()
   {
      return "Occurrence";
   }
}
