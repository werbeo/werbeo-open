package org.infinitenature.service.v1.types.support;

import javax.validation.constraints.NotBlank;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MediaUpload
{
   /**
    * The file name - might be changed by the server
    */
   @NotBlank
   private String fileName;
   /**
    * The description of the media upload
    */
   private String description;
   /**
    * Base64 encoded file data
    */
   @NotBlank
   private String data;

   public MediaUpload()
   {
      super();
   }

   public MediaUpload(@NotBlank String fileName, String description,
         @NotBlank String data)
   {
      super();
      this.fileName = fileName;
      this.description = description;
      this.data = data;
   }

   public String getFileName()
   {
      return fileName;
   }

   public void setFileName(String fileName)
   {
      this.fileName = fileName;
   }

   public String getData()
   {
      return data;
   }

   public void setData(String data)
   {
      this.data = data;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   @Override
   public String toString()
   {
      return MediaUploadBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return MediaUploadBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return MediaUploadBeanUtil.doEquals(this, obj);
   }
}
