package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.OccurrenceExport;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class OccurrenceExportsInfoResponse extends SliceResponse<OccurrenceExport>
{

   public OccurrenceExportsInfoResponse()
   {
      super();
   }

   public OccurrenceExportsInfoResponse(List<OccurrenceExport> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "occurrenceExports")
   @XmlElement(name = "occurrenceExport")
   @JsonProperty("occurrenceExports")
   public List<OccurrenceExport> getContent()
   {
      return content;
   }

   public void setContent(List<OccurrenceExport> content)
   {
      this.content = content;
   }

   @Override
   public String toString()
   {
      return OccurrenceExportsInfoResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceExportsInfoResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceExportsInfoResponseBeanUtil.doEquals(this, obj);
   }
}
