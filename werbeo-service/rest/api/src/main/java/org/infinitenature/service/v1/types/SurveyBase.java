package org.infinitenature.service.v1.types;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.infinitenature.service.v1.types.support.BaseInt;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SurveyBase extends BaseInt
{
   /**
    * The name of the survey.
    */
   @NotBlank
   private String name;
   /**
    * The description of the survey.
    */
   @NotBlank
   private String description;
   /**
    * Indicates if the survey is a container for other surveys (true) or if it
    * contains {@link Sample}s (false).
    */
   private Boolean container;
   /**
    * The availability level of the data in this survey.
    */
   @NotNull
   private Availability availability;

   /**
    * Indicates at the home of the data is in werbeo.
    */
   @NotNull
   private Boolean werbeoOriginal;
   /**
    * Indicates if new {@link Occurrence} and {@link Sample} data is allowed for
    * this survey.
    */
   @NotNull
   private Boolean allowDataEntry;
   
   @NotNull
   private Portal portal;
   
   private List<ObfuscationPolicy> obfuscationPolicies = new ArrayList<>();

   /**
    * Tags for this survey.
    */
   private Set<String> tags = new HashSet<>();

   public SurveyBase()
   {
      super();
   }

   public SurveyBase(int id, String project, Availability availabiltiy, Person person)
   {
      setEntityId(id);
      setName(project);
      setAvailability(availabiltiy);
      // TODO: owner?
   }

   public Boolean getContainer()
   {
      return container;
   }

   public void setContainer(Boolean container)
   {
      this.container = container;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public Availability getAvailability()
   {
      return availability;
   }

   public void setAvailability(Availability availability)
   {
      this.availability = availability;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public Boolean getWerbeoOriginal()
   {
      return werbeoOriginal;
   }

   public void setWerbeoOriginal(Boolean werbeoOriginal)
   {
      this.werbeoOriginal = werbeoOriginal;
   }

   public Boolean getAllowDataEntry()
   {
      return allowDataEntry;
   }

   public void setAllowDataEntry(Boolean allowDataEntry)
   {
      this.allowDataEntry = allowDataEntry;
   }

   public Set<String> getTags()
   {
      return tags;
   }

   public void setTags(Set<String> tags)
   {
      this.tags = tags;
   }

   @Override
   public String toString()
   {
      return SurveyBaseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyBaseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyBaseBeanUtil.doEquals(this, obj);
   }

   public List<ObfuscationPolicy> getObfuscationPolicies()
   {
      return obfuscationPolicies;
   }

   public void setObfuscationPolicies(List<ObfuscationPolicy> obfuscationPolicies)
   {
      this.obfuscationPolicies = obfuscationPolicies;
   }

   public Portal getPortal()
   {
      return portal;
   }

   public void setPortal(Portal portal)
   {
      this.portal = portal;
   }
}
