package org.infinitenature.service.v1.types;

import org.infinitenature.herbariorum.entities.Institution;

public class Herbarium
{
   private String code;
   private String herbary;
   private Institution institution = new Institution();

   public String getCode()
   {
      return code;
   }
   public void setCode(String code)
   {
      this.code = code;
   }
   public String getHerbary()
   {
      return herbary;
   }
   public void setHerbary(String herbary)
   {
      this.herbary = herbary;
   }

   public Institution getInstitution()
   {
      return institution;
   }

   public void setInstitution(Institution institution)
   {
      this.institution = institution;
   }
}
