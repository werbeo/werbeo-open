package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.PortalField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.FilterResponse;
import org.infinitenature.service.v1.types.response.PortalBaseResponse;
import org.infinitenature.service.v1.types.response.PortalConfigResponse;
import org.infinitenature.service.v1.types.response.PortalSliceResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * Informations about the portals
 *
 */
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1")
public interface PortalResource
{
   /**
    * Retrieve a list of portals
    *
    * @param offset
    * @param limit
    * @param sortField
    * @param sortOrder
    * @return
    */
   @GET
   public PortalSliceResponse findPortals(
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("ID") @QueryParam("sortField") PortalField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder);

   /**
    * Count the portals.
    *
    * Used for pagenation together with
    * {@link PortalResource#findPortals(int, int, PortalField, SortOrder)}
    *
    * @return
    */
   @Path("count")
   @GET
   public CountResponse count();

   @GET
   @Path("/{portalId}")
   public PortalBaseResponse getPortal(
         @PathParam("portalId") @Min(1) int portalId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })

   /**
    * Retrieves the
    * {@link org.infinitenature.service.v1.types.PortalConfiguration} for a
    * portal.
    *
    * @param portalId
    * @return
    */
   @GET
   @Path("/{portalId}/config")
   public PortalConfigResponse getPortalConfiguration(
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Retrieves all the portals associated to this one
    *
    * @param portalId
    * @return
    */
   @GET
   @Path("/{portalId}/associated")
   public PortalSliceResponse findAssociatedPortals(
         @PathParam("portalId") @Min(1) int portalId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })

   @GET
   @Path("/{portalId}/filter")
   /**
    * Returns information about the active filters for the current user on the
    * portal
    *
    * @param portalId
    * @return
    */
   public FilterResponse getFilter(@PathParam("portalId") @Min(1) int portalId);
}
