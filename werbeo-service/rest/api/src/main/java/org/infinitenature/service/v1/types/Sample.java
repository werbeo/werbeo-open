package org.infinitenature.service.v1.types;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class Sample extends SampleBase
{
   @Valid
   private List<Occurrence> occurrences = new ArrayList<>();


   public Sample()
   {
      super();
   }
   
   public Sample(SampleBase sample)
   {
      setDate(sample.getDate());
      setLocality(sample.getLocality());
      setRecorder(sample.getRecorder());
      setSampleMethod(sample.getSampleMethod());
      setSurvey(sample.getSurvey());
      setEntityId(sample.getEntityId());
      setAllowedOperations(sample.getAllowedOperations());
      setCreatedBy(sample.getCreatedBy());
      setCreationDate(sample.getCreationDate());
      setModificationDate(sample.getModificationDate());
      setModifiedBy(sample.getModifiedBy());
      setObfuscated(sample.isObfuscated());
      setUserAlloewdToEdit(sample.isUserAlloewdToEdit());
   }

   public List<Occurrence> getOccurrences()
   {
      return occurrences;
   }

   public void setOccurrences(List<Occurrence> occurrences)
   {
      this.occurrences = occurrences;
   }

   @Override
   public String toString()
   {
      return SampleBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleBeanUtil.doEquals(this, obj);
   }
}
