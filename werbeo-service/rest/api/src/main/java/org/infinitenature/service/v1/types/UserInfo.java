package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

/**
 * Information about a user.
 */
@XmlRootElement(name = "userInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class UserInfo
{
   @XmlAttribute(name = "email")
   private String email;
   @XmlAttribute(name = "anonymous")
   private boolean anonymous;

   /**
    * The email address of the user. <b>null</b> if there is no logged in user.
    */

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   /**
    * Indicates if there is a logged in user or not.
    */

   public boolean isAnonymous()
   {
      return anonymous;
   }

   public void setAnonymous(boolean anonymous)
   {
      this.anonymous = anonymous;
   }

   @Override
   public String toString()
   {
      return UserInfoBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return UserInfoBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return UserInfoBeanUtil.doEquals(this, obj);
   }
}
