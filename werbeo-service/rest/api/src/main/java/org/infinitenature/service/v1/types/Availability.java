package org.infinitenature.service.v1.types;

import org.infinitenature.service.v1.types.meta.HasEnumTranslationKey;

public enum Availability implements HasEnumTranslationKey<Availability>
{
   FREE, RESTRICTED, EMBARGO
}
