package org.infinitenature.service.v1.types.response;

import org.infinitenature.service.v1.types.support.Token;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TokenResponse extends BaseResponse
{
   private Token token;

   public TokenResponse()
   {
      super();
   }

   public TokenResponse(Token token)
   {
      super();
      this.token = token;
   }

   public Token getToken()
   {
      return token;
   }

   public void setToken(Token token)
   {
      this.token = token;
   }

   @Override
   public String toString()
   {
      return TokenResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TokenResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TokenResponseBeanUtil.doEquals(this, obj);
   }
}
