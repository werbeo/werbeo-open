package org.infinitenature.service.v1.types.meta;

public enum SurveyField implements EntityField<SurveyField>
{
   // Enums with property null are in the core-api but not yet in the rest
   // service
   ID("id"), //
   PARENT_ID(null), //
   DESCRIPTION("description"), //
   OWNER(null), //
   NAME("name"), //
   PORTAL(null), //
   AVAILABILITY("availability"), //
   CONTAINER("container"), //
   DEPUTY_CUSTIODIANS(null), //
   WERBEO_ORIGINAL("werbeoOriginal");

   private final String property;

   private SurveyField(String property)
   {
      this.property = property;
   }

   @Override
   public String getProperty()
   {
      return property;
   }

   @Override
   public String getTranslationKeyPrefix()
   {
      return "Survey";
   }
}
