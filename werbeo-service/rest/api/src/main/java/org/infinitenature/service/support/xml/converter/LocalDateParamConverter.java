package org.infinitenature.service.support.xml.converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.ext.ParamConverter;

public class LocalDateParamConverter implements ParamConverter<LocalDate>
{

   @Override
   public LocalDate fromString(String value)
   {
      if (value == null || value.trim().isEmpty())
      {
         return null;
      }
      return LocalDate.parse(value, DateTimeFormatter.ISO_LOCAL_DATE);
   }

   @Override
   public String toString(LocalDate value)
   {
      return value == null ? ""
            : value.format(DateTimeFormatter.ISO_LOCAL_DATE);
   }

}
