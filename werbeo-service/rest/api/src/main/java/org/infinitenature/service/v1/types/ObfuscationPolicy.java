package org.infinitenature.service.v1.types;


import org.infinitenature.service.v1.types.enums.Permission;
import org.infinitenature.service.v1.types.enums.Role;

public class ObfuscationPolicy
{
   private Role role;

   private Permission permission;

   public ObfuscationPolicy()
   {
      super();
   }

   public ObfuscationPolicy(String roleName, String permission)
   {
      this.role = Role.valueOf(roleName);
      this.permission = Permission.valueOf(permission);
   }

   public ObfuscationPolicy(Role role, Permission permission)
   {
      super();
      this.role = role;
      this.permission = permission;
   }

   public Role getRole()
   {
      return role;
   }

   public void setRole(Role role)
   {
      this.role = role;
   }

   public Permission getPermission()
   {
      return permission;
   }

   public void setPermission(Permission permission)
   {
      this.permission = permission;
   }
}
