package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.PortalConfiguration;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class PortalConfigResponse extends BaseResponse
{

   private PortalConfiguration portalConfiguration;

   public PortalConfigResponse()
   {
      super();
   }

   public PortalConfigResponse(PortalConfiguration portalConfiguration)
   {
      super();
      this.portalConfiguration = portalConfiguration;
   }

   public PortalConfiguration getPortalConfiguration()
   {
      return portalConfiguration;
   }

   public void setPortalConfiguration(PortalConfiguration portalConfiguration)
   {
      this.portalConfiguration = portalConfiguration;
   }

   @Override
   public String toString()
   {
      return PortalConfigResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PortalConfigResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalConfigResponseBeanUtil.doEquals(this, obj);
   }
}
