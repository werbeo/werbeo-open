package org.infinitenature.service.v1.types;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class MTB
{
   public MTB()
   {
      super();
   }

   public MTB(String mtb)
   {
      super();
      this.mtb = mtb;
   }

   private String mtb;

   public String getMtb()
   {
      return mtb;
   }

   public void setMtb(String mtb)
   {
      this.mtb = mtb;
   }

   @Override
   public String toString()
   {
      return MTBBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return MTBBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return MTBBeanUtil.doEquals(this, obj);
   }
}
