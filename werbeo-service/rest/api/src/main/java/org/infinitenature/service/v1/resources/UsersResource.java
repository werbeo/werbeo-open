package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.enums.Role;
import org.infinitenature.service.v1.types.response.AddRoleResponse;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.UserSliceResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/users")
public interface UsersResource
{

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   public UserSliceResponse find(@PathParam("portalId") @Min(1) int portalId,
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @QueryParam("email") String email);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("count")
   @GET
   public CountResponse count(@PathParam("portalId") @Min(1) int portalId,
         @QueryParam("email") String email);

   /**
    * Add role to a certain user.
    *
    * @param portalId
    * @param userId
    *           (email)
    * @param role
    *           (E.g. APPROVED, VALIDATOR)
    * @return
    */
   @Path("{email}/roles/{role}")
   @POST
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user must be authorized to accept terms and conditions") })
   public AddRoleResponse addRole(@PathParam("portalId") @Min(1) int portalId,
         @PathParam("email") String email, @PathParam("role") Role role);

   /**
    * Remove role from a certain user.
    *
    * @param portalId
    * @param userId
    *           (email)
    * @param role
    *           (E.g. APPROVED, VALIDATOR)
    * @return
    */
   @Path("{email}/roles/{role}")
   @DELETE
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user must be authorized to accept terms and conditions") })
   public AddRoleResponse removeRole(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("email") String email, @PathParam("role") Role role);
}
