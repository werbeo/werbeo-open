package org.infinitenature.service.v1.types.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.infinitenature.service.v1.types.OccurrenceCentroid;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class OccurrenceCentroidsSliceResponse
      extends SliceResponse<OccurrenceCentroid>
{
   public OccurrenceCentroidsSliceResponse()
   {
      super();
   }

   public OccurrenceCentroidsSliceResponse(List<OccurrenceCentroid> content,
         int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "occurrenceCentroids")
   @XmlElement(name = "occurrenceCentroid")
   @JsonProperty("occurrenceCentroids")
   public List<OccurrenceCentroid> getContent()
   {
      return content;
   }

   public void setContent(List<OccurrenceCentroid> content)
   {
      this.content = content;
   }
}
