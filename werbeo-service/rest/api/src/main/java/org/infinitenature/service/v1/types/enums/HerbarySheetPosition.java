package org.infinitenature.service.v1.types.enums;

public enum HerbarySheetPosition
{
   TOP_RIGHT,
   TOP_LEFT,
   BOTTOM_LEFT,
   BOTTOM_RIGHT

}
