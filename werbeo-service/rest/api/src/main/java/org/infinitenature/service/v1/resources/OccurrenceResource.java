package org.infinitenature.service.v1.resources;

import java.io.IOException;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.HerbarySheet;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.enums.HerbarySheetPosition;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.OccurrenceCentroidsSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceFieldConfigResponse;
import org.infinitenature.service.v1.types.response.OccurrenceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceStreamResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * Resource to access data on a {@link Occurrence} level. All access needs an
 * authenticated user.
 *
 */
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Path("/v1/{portalId}/occurrences")
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
public interface OccurrenceResource
{
   /**
    * A "stream" off all {@link Occurrence}s and their changes. Every response
    * is including a continuation token, which must be passed to the service by
    * the next request.
    * <p>
    * This resource is designed for use cases where a consumer needs to
    * synchronize its data with the service.
    *
    * @param token
    *           the continuation token received from the previous response
    *
    */
   @Path("/stream")
   @GET
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   public OccurrenceStreamResponse stream(
         @PathParam("portalId") @Min(1) int portalId,
         @QueryParam("continuationToken") String token);

   /**
    * Returns occurrences
    *
    * @param offset
    * @param limit
    * @param sortField
    *           the property to sort by
    * @param sortOrder
    *           the sort order
    * @param filter
    * @param inlineThumbs
    *           if set to true {@link Document} of type
    *           {@link DocumentType#IMAGE_THUMB} contains a Data-URL in the
    *           {@link Link}
    * @return
    */
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   public OccurrenceSliceResponse find(
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("MOD_DATE") @QueryParam("sortField") OccurrenceField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder,
         @BeanParam @Valid OccurrenceFilter filter,
         @DefaultValue("false") @QueryParam("inlineThumbs") boolean inlineThumbs);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("count")
   @GET
   public CountResponse count(@BeanParam @Valid OccurrenceFilter filter);
   
   
   
   //------------------------------- find & count via POST, to allow larger parameters
   
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @POST @Consumes("application/x-www-form-urlencoded")
   public OccurrenceSliceResponse findPOST(
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("MOD_DATE") @QueryParam("sortField") OccurrenceField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder,
         @BeanParam @Valid OccurrenceFilterPOST filter,
         @DefaultValue("false") @QueryParam("inlineThumbs") boolean inlineThumbs);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("count")
   @POST @Consumes("application/x-www-form-urlencoded")
   public CountResponse countPOST(@BeanParam @Valid OccurrenceFilterPOST filter);
  
   //------------------------------- find & count via POST, to allow larger parameters
   

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("{uuid}")
   @GET
   public OccurrenceResponse get(@PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID uuid);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("{uuid}/validate")
   @POST
   public OccurrenceResponse validate(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID uuid, Validation validation);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Produces(MediaType.APPLICATION_OCTET_STREAM)
   @Path("{uuid}/herbarysheet")
   @GET
   public Response createHerbaryPage(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("uuid") UUID uuid,
         @QueryParam("position") HerbarySheetPosition position,
         @BeanParam HerbarySheet sheet) throws IOException;

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("centroids/{taxonId}")
   @GET
   /**
    * Returns the centroids of occurrences for a given taxon.
    *
    * @param portalId
    *           The id of the portal. Can be retrieved by
    *           {@link org.infinitenature.service.v1.resources.PortalResource.findPortals(int,
    *           int, PortalField, SortOrder)}
    * @param taxonId
    *           The {@link Taxon} id to filter. Can be retrieved by
    *           {@link TaxaResource#find(int, int, TaxaFilter)}
    * @return
    */
   public OccurrenceCentroidsSliceResponse findOccurrenceCentroids(
         @PathParam("portalId") @Min(1) int portalId,
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("1000") @QueryParam("limit") @Min(1) int limit,
         @PathParam("taxonId") @Min(1) int taxonId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("centroids/count/{taxonId}")
   @GET
   /**
    * Returns the number of the centroids of occurrences for a given taxon.
    *
    * @param portalId
    *           The id of the portal. Can be retrieved by
    *           {@link org.infinitenature.service.v1.resources.PortalResource.findPortals(int,
    *           int, PortalField, SortOrder)}
    * @param taxonId
    *           The {@link Taxon} id to filter. Can be retrieved by
    *           {@link TaxaResource#find(int, int, TaxaFilter)}
    * @return
    */
   public CountResponse countOccurenceCentroids(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("taxonId") @Min(1) int taxonId);

   @Path("meta")
   @GET
   /**
    * Returns the portal wide field configuration
    *
    * @param portalId
    * @return
    */
   public OccurrenceFieldConfigResponse getFieldConfig(
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Returns the survey specific field configuration
    *
    * @param portalId
    * @param surveyId
    * @return
    */
   @Path("meta/{surveyId}")
   @GET
   public OccurrenceFieldConfigResponse getFieldConfig(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("surveyId") @Min(1) int surveyId);

}
