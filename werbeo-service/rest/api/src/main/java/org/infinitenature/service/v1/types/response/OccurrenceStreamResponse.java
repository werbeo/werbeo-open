package org.infinitenature.service.v1.types.response;

import java.util.List;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Occurrence;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public class OccurrenceStreamResponse extends StreamResponse<Occurrence, UUID>
{

   public OccurrenceStreamResponse()
   {
      super();
   }

   public OccurrenceStreamResponse(List<Occurrence> content,
         List<UUID> deletedEntities, String nextToken)
   {
      super(content, deletedEntities, nextToken);
   }

   @XmlElementWrapper(name = "occurrences")
   @XmlElement(name = "occurrence")
   @JsonProperty("occurrences")
   @Override
   public List<Occurrence> getContent()
   {
      return content;
   }

   @Override
   public void setContent(List<Occurrence> content)
   {
      this.content = content;
   }


}
