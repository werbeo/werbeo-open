package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Taxon;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class TaxonResponse extends BaseResponse
{
   private Taxon taxon;

   public TaxonResponse()
   {
      super();
   }

   public TaxonResponse(Taxon taxon)
   {
      super();
      this.taxon = taxon;
   }

   public Taxon getTaxon()
   {
      return taxon;
   }

   public void setTaxon(Taxon taxon)
   {
      this.taxon = taxon;
   }

   @Override
   public String toString()
   {
      return TaxonResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonResponseBeanUtil.doEquals(this, obj);
   }
}
