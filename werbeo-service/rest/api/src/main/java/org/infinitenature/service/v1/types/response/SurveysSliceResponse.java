package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.SurveyBase;

import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement
public class SurveysSliceResponse extends SliceResponse<SurveyBase>
{
   public SurveysSliceResponse()
   {
      super();
   }

   public SurveysSliceResponse(List<SurveyBase> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "surveys")
   @XmlElement(name = "surveys")
   @JsonProperty("surveys")
   public List<SurveyBase> getContent()
   {
      return content;
   }

   public void setContent(List<SurveyBase> content)
   {
      this.content = content;
   }

}
