package org.infinitenature.service.v1.resources;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Taxon;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceFilter
{
   public enum WKTMode
   {
      /**
       * The geometry of the occurrences must be complete within the filter
       * geometry.
       */
      WITHIN,
      /**
       * The geometry of the occurrence must intersect the filter geometry.
       */
      INTERSECTS
   }

   /**
    * The portal.
    */
   @PathParam("portalId")
   @Min(1)
   protected int portalId;
   /**
    * The earliest date of an occurrence. A date without a time-zone in the
    * ISO-8601 calendar system, such as 2007-12-03.
    */
   @QueryParam("from")
   protected LocalDate from;
   /**
    * The latest date of an occurrence. A date without a time-zone in the
    * ISO-8601 calendar system, such as 2007-12-03.
    */
   @QueryParam("to")
   protected LocalDate to;
   /**
    * The {@link Taxon} id to filter. Can be retrieved by
    * {@link TaxaResource#find(int, int, TaxaFilter)}
    */
   @QueryParam("taxon")
   protected Integer taxonId;
   @QueryParam("includeChildTaxa")
   @DefaultValue("true")
   protected boolean includeChildTaxa = true;
   /**
    * The geometry in epsg:4326. By default he position of the occurrence needs
    * to be <b>within</b> this. This is controlled by the
    * {@link OccurrenceFilter#wktMode} property
    */
   @QueryParam("wkt")
   private String wkt;

   /**
    * The filter mode for the geometry filter defined by the
    * {@link OccurrenceFilter#wkt} property
    */
   @QueryParam("wktMode")
   @DefaultValue("WITHIN")
   protected WKTMode wktMode = WKTMode.WITHIN;
   /**
    * The (german) MTB the centroid of the occurrence is located at.
    */
   @QueryParam("mtb")
   protected String mtb;
   @QueryParam("maxBlur")
   @Min(1)
   protected Integer maxBlur;
   @QueryParam("owner")
   protected String owner;

   @QueryParam("surveyId")
   protected Integer surveyId;

   @QueryParam("abscence")
   protected Boolean absence;

   /**
    * The code of the herbarium where the herbarium specimen is existing
    */
   @QueryParam("herbarium")
   protected String herbarium;

   /**
    * The {@link Occurrence.ValidationStatus} of the occurrences.
    */
   @QueryParam("validationStatus")
   protected Occurrence.ValidationStatus validationStatus;
   /**
    * The username of the validator
    */
   @QueryParam("validator")
   protected String validator;

   /**
    * Filters case insesitive a substring of the external key of an occurrence
    */
   @QueryParam("occExternalKey")
   protected String occExternalKey;

   /**
    * Filters by the modification date, so that the data is modified after the
    * given point in time.
    */
   @QueryParam("modifiedSince")
   protected LocalDateTime modifiedSince;

   public OccurrenceFilter()
   {
      super();
   }

   public OccurrenceFilter(int portalId, LocalDate from, LocalDate to,
         Integer taxonId, String wkt, String mtb, Integer maxBlur,
         boolean includeChildTaxa, String owner, Integer surveyId,
         Boolean absence, String herbarium, ValidationStatus valdiationStatus,
         String validator, String occExternalKey)
   {
      super();
      this.portalId = portalId;
      this.from = from;
      this.to = to;
      this.taxonId = taxonId;
      this.wkt = wkt;
      this.mtb = mtb;
      this.maxBlur = maxBlur;
      this.includeChildTaxa = includeChildTaxa;
      this.owner = owner;
      this.surveyId = surveyId;
      this.absence = absence;
      this.herbarium = herbarium;
      this.validationStatus = valdiationStatus;
      this.validator = validator;
      this.occExternalKey = occExternalKey;
   }

   public String getOwner()
   {
      return owner;
   }

   public void setOwner(String owner)
   {
      this.owner = owner;
   }

   public int getPortalId()
   {
      return portalId;
   }

   public OccurrenceFilter setPortalId(int portalId)
   {
      this.portalId = portalId;
      return this;
   }

   public LocalDate getFrom()
   {
      return from;
   }

   public void setFrom(LocalDate from)
   {
      this.from = from;
   }

   public LocalDate getTo()
   {
      return to;
   }

   public void setTo(LocalDate to)
   {
      this.to = to;
   }

   public Integer getTaxonId()
   {
      return taxonId;
   }

   public void setTaxonId(Integer taxonId)
   {
      this.taxonId = taxonId;
   }

   public String getWkt()
   {
      return wkt;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

   public Integer getMaxBlur()
   {
      return maxBlur;
   }

   public void setMaxBlur(Integer maxBlur)
   {
      this.maxBlur = maxBlur;
   }

   public boolean isIncludeChildTaxa()
   {
      return includeChildTaxa;
   }

   public void setIncludeChildTaxa(boolean includeChildTaxa)
   {
      this.includeChildTaxa = includeChildTaxa;
   }

   public Integer getSurveyId()
   {
      return surveyId;
   }

   public void setSurveyId(Integer surveyId)
   {
      this.surveyId = surveyId;
   }

   public String getMtb()
   {
      return mtb;
   }

   public void setMtb(String mtb)
   {
      this.mtb = mtb;
   }

   public Boolean getAbsence()
   {
      return absence;
   }

   public void setAbsence(Boolean absence)
   {
      this.absence = absence;
   }

   public String getHerbarium()
   {
      return herbarium;
   }

   public void setHerbarium(String herbarium)
   {
      this.herbarium = herbarium;
   }

   public Occurrence.ValidationStatus getValidationStatus()
   {
      return validationStatus;
   }

   public OccurrenceFilter setValidationStatus(
         Occurrence.ValidationStatus validationStatus)
   {
      this.validationStatus = validationStatus;
      return this;
   }

   public String getValidator()
   {
      return validator;
   }

   public void setValidator(String validator)
   {
      this.validator = validator;
   }

   public String getOccExternalKey()
   {
      return occExternalKey;
   }

   public OccurrenceFilter setOccExternalKey(String occExternalKey)
   {
      this.occExternalKey = occExternalKey;
      return this;
   }

   public LocalDateTime getModifiedSince()
   {
      return modifiedSince;
   }

   public OccurrenceFilter setModifiedSince(LocalDateTime modifiedSince)
   {
      this.modifiedSince = modifiedSince;
      return this;
   }

   public WKTMode getWktMode()
   {
      return wktMode;
   }

   public OccurrenceFilter setWktMode(WKTMode wktMode)
   {
      this.wktMode = wktMode;
      return this;
   }

   @Override
   public String toString()
   {
      return OccurrenceFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceFilterBeanUtil.doEquals(this, obj);
   }
}