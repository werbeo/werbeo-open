package org.infinitenature.service.v1.types.response;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class SurveyFieldConfigResponse extends BaseResponse
{

   private Set<SurveyFieldConfig> fieldConfigs = new HashSet<>();

   public SurveyFieldConfigResponse()
   {
      super();
   }

   public SurveyFieldConfigResponse(Set<SurveyFieldConfig> fieldConfigs)
   {
      super();
      this.fieldConfigs = fieldConfigs;
   }

   @XmlElementWrapper(name = "fieldConfigs")
   @XmlElement(name = "fieldConfig")
   @JsonProperty("fieldConfigs")
   public Set<SurveyFieldConfig> getFieldConfigs()
   {
      return fieldConfigs;
   }

   public void setFieldConfigs(Set<SurveyFieldConfig> fieldConfigs)
   {
      this.fieldConfigs = fieldConfigs;
   }

   @Override
   public String toString()
   {
      return SurveyFieldConfigResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyFieldConfigResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyFieldConfigResponseBeanUtil.doEquals(this, obj);
   }
}
