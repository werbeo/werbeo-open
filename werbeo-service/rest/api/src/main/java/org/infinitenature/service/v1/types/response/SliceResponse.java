package org.infinitenature.service.v1.types.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;


public abstract class SliceResponse<T> extends BaseResponse

{
   private int numberOfElements;
   private int size;

   protected List<T> content = new ArrayList<>();

   public SliceResponse()
   {
      super();
   }

   public SliceResponse(List<T> content, int size)
   {
      this();
      this.size = size;
      this.content = content;
      this.numberOfElements = content.size();
   }


   /**
    * The number of all elements with the current filter.
    */
   @XmlElement(name = "size")
   public int getSize()
   {
      return size;
   }

   public void setSize(int size)
   {
      this.size = size;
   }

   /**
    * The number of elements returned by this slice.
    */
   @XmlElement
   public int getNumberOfElements()
   {
      return numberOfElements;
   }

}
