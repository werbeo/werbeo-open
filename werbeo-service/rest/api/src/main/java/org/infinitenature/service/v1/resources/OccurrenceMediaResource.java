package org.infinitenature.service.v1.resources;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.MediaResponse;
import org.infinitenature.service.v1.types.support.MediaUpload;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/occurrences/{uuid}/media")
public interface OccurrenceMediaResource
{
   /**
    * Adds media to an existing {@link Occurrence}
    */
   @POST
   @Path("image")
   public MediaResponse attachMedia(@PathParam("portalId") int portalId,
         @PathParam("uuid") UUID uuid, MediaUpload mediaUpload);

   /**
    * Deletes media from an {@link Occurrence}.
    * <p>
    * Deleting a thumbnail, eg. {@link DocumentType.IMAGE_THUMB}, also deletes
    * the actual image
    *
    * @param portalId
    * @param uuid
    * @param mediaURL
    *           The URL optaint from the {@link Occurrence}
    *           {@link Link#getHref()}
    *
    */
   @DELETE
   public DeletedResponse deleteMedia(@PathParam("portalId") int portalId,
         @PathParam("uuid") UUID uuid, String mediaURL);

}
