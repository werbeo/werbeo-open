package org.infinitenature.service.v1.types.enums;

public enum LayerName
{
   MTB,
   ORTHOFOTO_MV,
   TOPO_MV;
}
