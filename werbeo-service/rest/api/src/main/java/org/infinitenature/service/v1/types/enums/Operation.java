package org.infinitenature.service.v1.types.enums;

public enum Operation
{
   CREATE, READ, UPDATE, DELETE
}
