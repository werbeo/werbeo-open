package org.infinitenature.service.v1.types;

public class Publication
{
   private String primAuthorFirstName;
   private String primAuthorLastName;
   private int year;
   private String title;
   private String citeId;

   public String getPrimAuthorFirstName()
   {
      return primAuthorFirstName;
   }

   public void setPrimAuthorFirstName(String primAuthorFirstName)
   {
      this.primAuthorFirstName = primAuthorFirstName;
   }

   public String getPrimAuthorLastName()
   {
      return primAuthorLastName;
   }

   public void setPrimAuthorLastName(String primAuthorLastName)
   {
      this.primAuthorLastName = primAuthorLastName;
   }

   public int getYear()
   {
      return year;
   }

   public void setYear(int year)
   {
      this.year = year;
   }

   public String getTitle()
   {
      return title;
   }

   public void setTitle(String title)
   {
      this.title = title;
   }

   public String getCiteId()
   {
      return citeId;
   }

   public void setCiteId(String citeId)
   {
      this.citeId = citeId;
   }
}
