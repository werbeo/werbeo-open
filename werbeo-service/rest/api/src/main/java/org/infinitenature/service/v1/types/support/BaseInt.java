package org.infinitenature.service.v1.types.support;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.xml.bind.annotation.XmlAttribute;

@JsonIgnoreProperties(ignoreUnknown=true)
public class BaseInt extends Base<Integer>
{
   /**
    * The id of the entity.
    */
   @XmlAttribute(name = "id")
   private Integer entityId;

   public BaseInt()
   {
      super();
   }

   @Override
   public Integer getEntityId()
   {
      return entityId;
   }

   @Override
   public void setEntityId(Integer entiyId)
   {
      this.entityId = entiyId;
   }
}
