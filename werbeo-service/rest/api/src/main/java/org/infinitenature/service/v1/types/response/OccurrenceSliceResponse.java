package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Occurrence;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class OccurrenceSliceResponse
      extends SliceResponse<Occurrence>
{
   public OccurrenceSliceResponse()
   {
      super();
   }

   public OccurrenceSliceResponse(List<Occurrence> content, int size)
   {
      super(content, size);
   }


   @XmlElementWrapper(name = "occurrences")
   @XmlElement(name = "occurrence")
   @JsonProperty("occurrences")
   public List<Occurrence> getContent()
   {
      return content;
   }

   public void setContent(List<Occurrence> content)
   {
      this.content = content;
   }

   @Override
   public String toString()
   {
      return OccurrenceSliceResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceSliceResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceSliceResponseBeanUtil.doEquals(this, obj);
   }
}
