package org.infinitenature.service.v1.types.support;

import net.vergien.beanautoutils.annotation.Bean;

/**
 * Information about the request
 *
 * @author dve
 *
 */
@Bean
public class MetaData
{
   /**
    * Creation time of the response in ms.
    */
   private long creationTime;
   /**
    * The session id of the request.
    */
   private String sessionId;

   public long getCreationTime()
   {
      return creationTime;
   }

   public void setCreationTime(long creationTime)
   {
      this.creationTime = creationTime;
   }

   public String getSessionId()
   {
      return sessionId;
   }

   public void setSessionId(String sessionId)
   {
      this.sessionId = sessionId;
   }

   @Override
   public String toString()
   {
      return MetaDataBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return MetaDataBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return MetaDataBeanUtil.doEquals(this, obj);
   }
}
