package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Occurrence;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class OccurrenceResponse extends BaseResponse
{
   private Occurrence occurrence;

   public Occurrence getOccurrence()
   {
      return occurrence;
   }

   public OccurrenceResponse()
   {
      super();

   }

   public OccurrenceResponse(Occurrence occurrence)
   {
      super();
      this.occurrence = occurrence;
   }

   public void setOccurrence(Occurrence occurrence)
   {
      this.occurrence = occurrence;
   }

   @Override
   public String toString()
   {
      return OccurrenceResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceResponseBeanUtil.doEquals(this, obj);
   }
}
