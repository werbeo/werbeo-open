package org.infinitenature.service.v1.types.meta;

public interface HasEnumTranslationKey<T extends Enum<T>>
      extends HasTranslationKey
{
   @Override
   public default String getTranslationKey()
   {
      return getTranslationKeyPrefix() + "." + ((T) this).name();
   }

   public default String getTranslationKeyPrefix()
   {
      return this.getClass().getSimpleName();
   }

   public String name();
}
