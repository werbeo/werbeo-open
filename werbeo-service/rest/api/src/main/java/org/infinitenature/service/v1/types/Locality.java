package org.infinitenature.service.v1.types;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement(name = "locality")
public class Locality
{
   /**
    * The concrete position.
    */
   @NotNull
   @Valid
   private Position position;
   /**
    * The blur of the position measuring in meters.
    */
   @Min(0)
   @NotNull
   private Integer blur;
   private String locationComment;
   private String locality;


   public Position getPosition()
   {
      return position;
   }

   public void setPosition(Position position)
   {
      this.position = position;
   }

   public Integer getBlur()
   {
      return blur;
   }

   public void setBlur(Integer precision)
   {
      this.blur = precision;
   }

   public String getLocationComment()
   {
      return locationComment;
   }

   public void setLocationComment(String locationComment)
   {
      this.locationComment = locationComment;
   }

   public String getLocality()
   {
      return locality;
   }

   public void setLocality(String locality)
   {
      this.locality = locality;
   }

   @Override
   public String toString()
   {
      return LocalityBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return LocalityBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return LocalityBeanUtil.doEquals(this, obj);
   }
}
