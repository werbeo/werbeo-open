package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.SampleComment;

@XmlRootElement
public class SampleCommentsSliceResponse extends SliceResponse<SampleComment>
{

   public SampleCommentsSliceResponse()
   {
      super();
   }

   public SampleCommentsSliceResponse(List<SampleComment> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "comments")
   @XmlElement(name = "comment")
   public List<SampleComment> getContent()
   {
      return content;
   }

   public void setContent(List<SampleComment> content)
   {
      this.content = content;
   }
}
