package org.infinitenature.service.v1.types.support;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@JsonIgnoreProperties(ignoreUnknown = true)
public class Token
{
   @JsonProperty("access_token")
   private String accessToken;

   public String getAccessToken()
   {
      return accessToken;
   }

   public void setAccessToken(String accessToken)
   {
      this.accessToken = accessToken;
   }

   @Override
   public String toString()
   {
      return TokenBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TokenBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TokenBeanUtil.doEquals(this, obj);
   }
}
