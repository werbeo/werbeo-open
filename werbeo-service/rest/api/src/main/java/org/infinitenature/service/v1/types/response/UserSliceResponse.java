package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.User;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class UserSliceResponse
      extends SliceResponse<User>
{
   public UserSliceResponse()
   {
      super();
   }

   public UserSliceResponse(List<User> content, int size)
   {
      super(content, size);
   }


   @XmlElementWrapper(name = "users")
   @XmlElement(name = "user")
   @JsonProperty("users")
   public List<User> getContent()
   {
      return content;
   }

   public void setContent(List<User> content)
   {
      this.content = content;
   }

   @Override
   public String toString()
   {
      return UserSliceResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return UserSliceResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return UserSliceResponseBeanUtil.doEquals(this, obj);
   }
}
