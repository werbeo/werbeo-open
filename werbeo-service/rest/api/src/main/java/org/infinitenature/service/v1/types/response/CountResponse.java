package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CountResponse extends BaseResponse
{
   private long amount;

   public CountResponse()
   {
      super();
   }

   public CountResponse(long amount, String sourceRequestId)
   {
      super();
      this.amount = amount;
      setSourceRequestId(sourceRequestId);
   }

   public long getAmount()
   {
      return amount;
   }

   public void setAmount(long amount)
   {
      this.amount = amount;
   }

}
