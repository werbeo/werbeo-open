package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.TaxonBase;

@XmlRootElement
public class TaxonBaseSliceResponse extends SliceResponse<TaxonBase>
{

   public TaxonBaseSliceResponse()
   {
      super();
   }

   public TaxonBaseSliceResponse(List<TaxonBase> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "taxa")
   @XmlElement(name = "taxon")
   public List<TaxonBase> getContent()
   {
      return content;
   }


   public void setContent(List<TaxonBase> content)
   {
      this.content = content;
   }
}
