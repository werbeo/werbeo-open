package org.infinitenature.service.v1.types;

import java.time.LocalDateTime;
import java.util.UUID;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class OccurrenceExport
{
   private UUID id;
   private JobStatus status;
   private String fileFormat;
   private LocalDateTime creationDate;

   public UUID getId()
   {
      return id;
   }

   public void setId(UUID id)
   {
      this.id = id;
   }

   public JobStatus getStatus()
   {
      return status;
   }

   public void setStatus(JobStatus status)
   {
      this.status = status;
   }

   public String getFileFormat()
   {
      return fileFormat;
   }

   public void setFileFormat(String fileFormat)
   {
      this.fileFormat = fileFormat;
   }

   @Override
   public String toString()
   {
      return OccurrenceExportBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceExportBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceExportBeanUtil.doEquals(this, obj);
   }

   public LocalDateTime getCreationDate()
   {
      return creationDate;
   }

   public void setCreationDate(LocalDateTime creationDate)
   {
      this.creationDate = creationDate;
   }
}
