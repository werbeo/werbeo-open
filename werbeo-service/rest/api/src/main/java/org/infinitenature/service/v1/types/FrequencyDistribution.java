package org.infinitenature.service.v1.types;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;
@Bean
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class FrequencyDistribution
{
   /**
    * The gap between tow events.
    */
   private ChronoUnit gap;

   /**
    * The start date of the time range and the amount.
    */
   private List<DateEntry> data = new ArrayList<>();

   public ChronoUnit getGap()
   {
      return gap;
   }

   public void setGap(ChronoUnit gap)
   {
      this.gap = gap;
   }

   public List<DateEntry> getData()
   {
      return data;
   }

   public void setData(List<DateEntry> data)
   {
      this.data = data;
   }

   @Override
   public String toString()
   {
      return FrequencyDistributionBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return FrequencyDistributionBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return FrequencyDistributionBeanUtil.doEquals(this, obj);
   }
}
