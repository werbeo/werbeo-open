package org.infinitenature.service.v1.types;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.infinitenature.service.v1.resources.OccurrenceMediaResource;
import org.infinitenature.service.v1.types.meta.HasEnumTranslationKey;
import org.infinitenature.service.v1.types.support.BaseUUID;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class Occurrence extends BaseUUID
{
   public enum Amount implements HasEnumTranslationKey<Amount>
   {
      A_LOT, SOME, SCATTERED, FEW, ONE, 
   }

   public enum SettlementStatus
         implements HasEnumTranslationKey<SettlementStatus>
   {
      PLANTED, RESETTELD, WILD
   }

   public enum RecordStatus implements HasEnumTranslationKey<RecordStatus>
   {
      COMPLETE, IN_PROGRESS
   }

   public enum ValidationStatus
         implements HasEnumTranslationKey<ValidationStatus>
   {
      VALID, PROBABLE, UNKNOWN, IMPROBALE, INVALID;
   }

   /**
    * Covered area in square meters
    */
   public enum Area implements HasEnumTranslationKey<Area>
   {
      LESS_ONE, ONE_TO_FIVE, SIX_TO_TWENTYFIVE, TWENTYSIX_TO_FIFTY, MORE_THAN_FIFTY, MORE_THAN_HUNDRED, MORE_THAN_THOUSAND, MORE_THAN_TENTHOUSAND
   }

   public enum Vitality implements HasEnumTranslationKey<Vitality>
   {
      VITAL, KUEMMERND, ABSTERBEND, ABGESTORBEN
   }

   public enum SettlementStatusFukarek
         implements HasEnumTranslationKey<SettlementStatusFukarek>
   {
      UNBEKANNT, INDIGEN, EINGEBUERGERT, UNBESTAENDIG, SYNANTHROP, ANGESIEDELT, ANGESALBT_KULTIVIERT
   }

   public enum BloomingSprouts implements HasEnumTranslationKey<BloomingSprouts>
   {
      ONE_TO_FIVE, SIX_TO_TWENTYFIVE, TWENTYSIX, FIFTY, HUNDRED, THOUSAND, TENTHOUSAND
   }

   public enum Quantity implements HasEnumTranslationKey<Quantity>
   {
      ONE_TO_FIVE, SIX_TO_TWENTYFIVE, TWENTYSIX, FIFTY, HUNDRED, THOUSAND, TENTHOUSAND
   }

   public enum Sex implements HasEnumTranslationKey<Sex>
   {
      MALE, FEMALE, BOTH, UNKNOWN
   }

   public enum Reproduction implements HasEnumTranslationKey<Reproduction>
   {
      SURE, PLAUSIBLE, UNLIKELY, UNKNOWN
   }

   public enum LifeStage implements HasEnumTranslationKey<LifeStage>
   {
      UNKNOWN, LARVE, IMAGO, LARVE_IMAGO;
   }

   public enum Makropter implements HasEnumTranslationKey<Makropter>
   {
      UNKNOWN, YES, NO;
   }

   /**
    * Adds quantity information to the field {@link Occurrence#numericAmount}
    *
    */
   public enum NumericAmountAccuracy
         implements HasEnumTranslationKey<NumericAmountAccuracy>
   {
      COUNTED, MORETHAN, APPROXIMATE
   }
   private Amount amount;
   private SettlementStatus settlementStatus;
   @NotNull
   private RecordStatus recordStatus;
   @NotNull
   private TaxonBase taxon;
   @NotNull
   private SampleBase sample;
   @NotNull
   private Person determiner;
   private String externalKey;
   /**
    * Comment on this Occurrence. For diskussions use {@link OccurrenceComment}
    */
   private String comment;
   private LifeStage lifeStage;
   private Makropter makropter;
   private Publication reference;
   private Area coveredArea;
   private Vitality vitality;
   private SettlementStatusFukarek settlementStatusFukarek;
   private BloomingSprouts bloomingSprouts;
   private Quantity quantity;
   private String observers;
   private Herbarium herbarium;
   private String determinationComment;
   @Min(1)
   private Integer numericAmount;
   private Reproduction reproduction;
   private Sex sex;
   private String habitat;
   private String timeOfDay;
   private String remark;
   
   private String citeId;
   private String citeComment;

   /**
    * Documents saved for this occurrence. This can be e.g. images or maps.
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    *
    * @see OccurrenceMediaResource#attachMedia(int, java.util.UUID,
    *      org.infinitenature.service.v1.types.support.MediaUpload)
    */
   private List<Document> documents = new ArrayList<>();
   /**
    * Set to true if this occurrence describes the absence of an expected
    * species.
    */
   private Boolean absence;

   private Validation validation;

   private NumericAmountAccuracy numericAmountAccuracy;

   public NumericAmountAccuracy getNumericAmountAccuracy()
   {
      return numericAmountAccuracy;
   }

   public void setNumericAmountAccuracy(
         NumericAmountAccuracy numericAmountAccuracy)
   {
      this.numericAmountAccuracy = numericAmountAccuracy;
   }

   public Validation getValidation()
   {
      return validation;
   }

   public void setValidation(Validation validation)
   {
      this.validation = validation;
   }

   public Reproduction getReproduction()
   {
      return reproduction;
   }

   public void setReproduction(Reproduction reproduction)
   {
      this.reproduction = reproduction;
   }

   public Sex getSex()
   {
      return sex;
   }

   public void setSex(Sex sex)
   {
      this.sex = sex;
   }

   public Integer getNumericAmount()
   {
      return numericAmount;
   }

   public void setNumericAmount(Integer numericAmount)
   {
      this.numericAmount = numericAmount;
   }

   public Area getCoveredArea()
   {
      return coveredArea;
   }

   public void setCoveredArea(Area area)
   {
      this.coveredArea = area;
   }

   public TaxonBase getTaxon()
   {
      return taxon;
   }

   public void setTaxon(TaxonBase taxon)
   {
      this.taxon = taxon;
   }

   public SampleBase getSample()
   {
      return sample;
   }

   public void setSample(SampleBase sample)
   {
      this.sample = sample;
   }

   public Person getDeterminer()
   {
      return determiner;
   }

   public void setDeterminer(Person determiner)
   {
      this.determiner = determiner;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public LifeStage getLifeStage()
   {
      return lifeStage;
   }

   public void setLifeStage(LifeStage lifeStage)
   {
      this.lifeStage = lifeStage;
   }

   public Makropter getMakropter()
   {
      return makropter;
   }

   public void setMakropter(Makropter makropter)
   {
      this.makropter = makropter;
   }

   public Publication getReference()
   {
      return reference;
   }

   public void setReference(Publication reference)
   {
      this.reference = reference;
   }

   public Amount getAmount()
   {
      return amount;
   }

   public void setAmount(Amount amount)
   {
      this.amount = amount;
   }

   public SettlementStatus getSettlementStatus()
   {
      return settlementStatus;
   }

   public void setSettlementStatus(SettlementStatus status)
   {
      this.settlementStatus = status;
   }

   public RecordStatus getRecordStatus()
   {
      return recordStatus;
   }

   public void setRecordStatus(RecordStatus recordStatus)
   {
      this.recordStatus = recordStatus;
   }

   public String getDeterminationComment()
   {
      return determinationComment;
   }

   public void setDeterminationComment(String determinationComment)
   {
      this.determinationComment = determinationComment;
   }

   @Override
   public String toString()
   {
      return OccurrenceBeanUtil.doToString(this);
   }

//   @Override
//   public int hashCode()
//   {
//      return OccurrenceBeanUtil.doToHashCode(this);
//   }
//
//   @Override
//   public boolean equals(Object obj)
//   {
//      return OccurrenceBeanUtil.doEquals(this, obj);
//   }







   public Vitality getVitality()
   {
      return vitality;
   }

   public void setVitality(Vitality vitality)
   {
      this.vitality = vitality;
   }

   public SettlementStatusFukarek getSettlementStatusFukarek()
   {
      return settlementStatusFukarek;
   }

   public void setSettlementStatusFukarek(
         SettlementStatusFukarek settlementStatusFukarek)
   {
      this.settlementStatusFukarek = settlementStatusFukarek;
   }

   public BloomingSprouts getBloomingSprouts()
   {
      return bloomingSprouts;
   }

   public void setBloomingSprouts(BloomingSprouts bloomingSprouts)
   {
      this.bloomingSprouts = bloomingSprouts;
   }

   public Quantity getQuantity()
   {
      return quantity;
   }

   public void setQuantity(Quantity quantity)
   {
      this.quantity = quantity;
   }

   public String getObservers()
   {
      return observers;
   }

   public void setObservers(String observers)
   {
      this.observers = observers;
   }

   public Herbarium getHerbarium()
   {
      return herbarium;
   }

   public void setHerbarium(Herbarium herbarium)
   {
      this.herbarium = herbarium;
   }

   public List<Document> getDocuments()
   {
      return documents;
   }

   public void setDocuments(List<Document> documents)
   {
      this.documents = documents;
   }

   public String getHabitat()
   {
      return habitat;
   }

   public void setHabitat(String habitat)
   {
      this.habitat = habitat;
   }

   public Boolean getAbsence()
   {
      return absence;
   }

   public void setAbsence(Boolean absence)
   {
      this.absence = absence;
   }

   public String getTimeOfDay()
   {

      return timeOfDay;
   }

   public void setTimeOfDay(String timeOfDay)
   {

      this.timeOfDay = timeOfDay;
   }

   public String getRemark()
   {
      return remark;
   }

   public void setRemark(String remark)
   {
      this.remark = remark;
   }

   public String getCiteId()
   {
      return citeId;
   }

   public void setCiteId(String citeId)
   {
      this.citeId = citeId;
   }

   public String getCiteComment()
   {
      return citeComment;
   }

   public void setCiteComment(String citeComment)
   {
      this.citeComment = citeComment;
   }
}
