package org.infinitenature.service.v1.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.support.BaseInt;

@XmlRootElement(name = "taxaList")
public class TaxaList extends BaseInt
{
   private String name;

   private String description;

   private List<String> taxaGroups = new ArrayList<>();

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public List<String> getTaxaGroups()
   {
      return taxaGroups;
   }

   public void setTaxaGroups(List<String> taxaGroups)
   {
      this.taxaGroups = taxaGroups;
   }

}
