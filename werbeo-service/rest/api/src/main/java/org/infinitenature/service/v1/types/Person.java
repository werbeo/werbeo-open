package org.infinitenature.service.v1.types;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.support.BaseInt;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement(name = "person")
@Bean
public class Person extends BaseInt
{
   private String firstName;
   private String lastName;
   @Size(max = 50)
   private String externalKey;
   private String email;

   public Person()
   {
      super();
   }

   public Person(Integer id, String firstName, String lastName)
   {
      setEntityId(id);
      this.firstName = firstName;
      this.lastName = lastName;
   }

   public Person(Integer id, String firstName, String lastName, String email)
   {
      this(id, firstName, lastName);
      this.setEmail(email);
   }

   public String getFirstName()
   {
      return firstName;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   public String getExternalKey()
   {
      return externalKey;
   }

   public void setExternalKey(String externalKey)
   {
      this.externalKey = externalKey;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   @Override
   public String toString()
   {
      return PersonBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PersonBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PersonBeanUtil.doEquals(this, obj);
   }
}
