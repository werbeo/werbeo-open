package org.infinitenature.service.v1.resources;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.JobStatus;
import org.infinitenature.service.v1.types.OccurrenceImportField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceImportStatusResponse;
import org.infinitenature.service.v1.types.response.OccurrenceImportsInfoResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

@Path("/v1/{portalId}/import")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
public interface OccurrenceImportResource
{
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })

   @POST
   @Consumes("application/xml")
   @Produces("text/plain")
   public String doImportXml(@PathParam("portalId") int portalId,
         String stringRequest);

   @GET
   @Path("/{id}")
   public JobStatus getJobStatus(@PathParam("portalId") int portalId,
         @PathParam("id") UUID id);

   @POST
   @Path("/csv")
   @Consumes("application/csv")
   public String doImportCsv(@PathParam("portalId") int portalId,
         String csvContent);

   @GET
   @Path("/csv/template")
   @Produces("text/plain")
   public Response getCsvTemplate(@PathParam("portalId") int portalId);
   
   @GET
   @Path("/csv/template/help")
   @Produces("text/plain")
   public Response getCsvTemplateHelp(@PathParam("portalId") int portalId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   @Path("/csv/{importId}")
   public Response getImport(@PathParam("importId") UUID importId,
         @PathParam("portalId") @Min(1) int portalId);

   @GET
   @Path("/csv/{importId}/errors")
   public Response getErrorLog(@PathParam("importId") UUID importId,
         @PathParam("portalId") @Min(1) int portalId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   @Path("{importId}/status")
   public OccurrenceImportStatusResponse getStatus(
         @PathParam("importId") UUID importId,
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Returns the export jobs of the current user.
    *
    * @param portalId
    * @return
    */
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   public OccurrenceImportsInfoResponse findImports(
         @PathParam("portalId") @Min(1) int portalId,
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("ID") @QueryParam("sortField") OccurrenceImportField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder);

   /**
    * Deletes an export
    *
    * @param exportId
    *           of Export to be deleted
    * @return
    */
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @DELETE
   @Path("{importId}")
   public DeletedResponse delete(@PathParam("importId") UUID importId,
         @PathParam("portalId") @Min(1) int portalId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("count")
   @GET
   public CountResponse count(@PathParam("portalId") @Min(1) int portalId);

}
