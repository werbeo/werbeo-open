package org.infinitenature.service.v1.types.support;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.enums.Operation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown=true)
public abstract class Base<ID_TYPE> implements LinkAware
{
   /**
    * The id of the user who created this entity.
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    */
   private String createdBy;

   /**
    * The timestamp of the cration of this entity. The timestamp is in the
    * timezone Europe/Berlin by default.
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    */
   private LocalDateTime creationDate;
   /**
    * A collection of {@link Link}s related to this entity.
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    */
   @JsonProperty("links")
   @XmlElementWrapper(name = "links")
   @XmlElement(name = "link", namespace = Link.ATOM_NAMESPACE)
   private final List<Link> links;
   /**
    * The id of the user who changed this entity last.
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    */
   private String modifiedBy;
   /**
    * The timestamp of the last modification on this entity. The timestamp is in
    * the timezone Europe/Berlin by default.
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    */
   private LocalDateTime modificationDate;
   /**
    * Indicates weather the user who made the request is allowed to edit this
    * entity or not.
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    */
   private boolean userAlloewdToEdit;

   /**
    * Indicate weather the data is obfuscated
    * <p>
    * This entity is read only and populated by the service. It is ignored when
    * saving entities.
    */
   private boolean obfuscated = false;

   private Set<Operation> allowedOperations;

   public Base()
   {
      links = new ArrayList<>();
      allowedOperations = new HashSet<>();
   }

   public String getCreatedBy()
   {
      return createdBy;
   }

   public LocalDateTime getCreationDate()
   {
      return creationDate;
   }

   public abstract ID_TYPE getEntityId();

   @Override
   public List<Link> getLinks()
   {
      return links;
   }

   public String getModifiedBy()
   {
      return modifiedBy;
   }

   public LocalDateTime getModificationDate()
   {
      return modificationDate;
   }

   public boolean isUserAlloewdToEdit()
   {
      return userAlloewdToEdit;
   }

   public void setCreatedBy(String createdBy)
   {
      this.createdBy = createdBy;
   }

   public void setCreationDate(LocalDateTime creationDate)
   {
      this.creationDate = creationDate;
   }

   public abstract void setEntityId(ID_TYPE id);

   public void setModifiedBy(String modificatedBy)
   {
      this.modifiedBy = modificatedBy;
   }

   public void setModificationDate(LocalDateTime modificationDate)
   {
      this.modificationDate = modificationDate;
   }

   public void setUserAlloewdToEdit(boolean userAlloewdToEdit)
   {
      this.userAlloewdToEdit = userAlloewdToEdit;
   }

   public Set<Operation> getAllowedOperations()
   {
      return allowedOperations;
   }

   public void setAllowedOperations(Set<Operation> allowedOperations)
   {
      this.allowedOperations = allowedOperations;
   }

   public boolean isObfuscated()
   {
      return obfuscated;
   }

   public void setObfuscated(boolean obfuscated)
   {
      this.obfuscated = obfuscated;
   }

}
