package org.infinitenature.service.v1.types.response;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.meta.TaxonFieldConfig;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class TaxonFieldConfigResponse
{
   private Set<TaxonFieldConfig> fieldConfigs = new HashSet<>();

   public TaxonFieldConfigResponse()
   {
      super();
   }

   public TaxonFieldConfigResponse(Set<TaxonFieldConfig> fieldConfigs)
   {
      super();
      this.fieldConfigs = fieldConfigs;
   }

   @XmlElementWrapper(name = "fieldConfigs")
   @XmlElement(name = "fieldConfig")
   @JsonProperty("fieldConfigs")
   public Set<TaxonFieldConfig> getFieldConfigs()
   {
      return fieldConfigs;
   }

   public void setFieldConfigs(Set<TaxonFieldConfig> fieldConfigs)
   {
      this.fieldConfigs = fieldConfigs;
   }

   @Override
   public String toString()
   {
      return TaxonFieldConfigResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxonFieldConfigResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxonFieldConfigResponseBeanUtil.doEquals(this, obj);
   }
}
