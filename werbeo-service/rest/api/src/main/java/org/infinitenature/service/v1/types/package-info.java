@javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters({
      @javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(type = java.time.LocalDateTime.class, value = org.infinitenature.service.support.xml.adapter.LocalDateTimeAdapter.class),
      @javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter(type = java.time.LocalDate.class, value = org.infinitenature.service.support.xml.adapter.LocalDateAdapter.class) })
package org.infinitenature.service.v1.types;
