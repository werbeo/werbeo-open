package org.infinitenature.service.v1.types;

import java.util.UUID;

import org.infinitenature.service.v1.types.support.BaseInt;

public abstract class AbstractComment extends BaseInt
{
   public AbstractComment()
   {
      super();
   }

   public AbstractComment(String comment)
   {
      super();
      this.comment = comment;
   }

   /**
    * The comment to the entity formated as MarkDown text
    */
   private String comment;

   /**
    * The id of the target entity of this comment. Ignored on save, cause the
    * url is used to identify the target entity
    */
   private UUID targetId;

   public String getComment()
   {
      return comment;
   }

   public void setComment(String comment)
   {
      this.comment = comment;
   }

   public UUID getTargetId()
   {
      return targetId;
   }

   public void setTargetId(UUID targetId)
   {
      this.targetId = targetId;
   }
}
