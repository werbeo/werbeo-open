package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.PortalBase;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class PortalBaseResponse extends BaseResponse
{
   public PortalBaseResponse()
   {
      super();
   }

   public PortalBaseResponse(PortalBase portal)
   {
      super();
      this.portal = portal;
   }

   private PortalBase portal;

   public PortalBase getPortal()
   {
      return portal;
   }

   public void setPortal(PortalBase portal)
   {
      this.portal = portal;
   }

   @Override
   public String toString()
   {
      return PortalBaseResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PortalBaseResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalBaseResponseBeanUtil.doEquals(this, obj);
   }
}
