package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.OccurrenceComment;

@XmlRootElement
public class OccurrenceCommentsSliceResponse
      extends SliceResponse<OccurrenceComment>
{
   public OccurrenceCommentsSliceResponse()
   {
      super();
   }

   public OccurrenceCommentsSliceResponse(List<OccurrenceComment> content,
         int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "comments")
   @XmlElement(name = "comment")
   public List<OccurrenceComment> getContent()
   {
      return content;
   }

   public void setContent(List<OccurrenceComment> content)
   {
      this.content = content;
   }
}
