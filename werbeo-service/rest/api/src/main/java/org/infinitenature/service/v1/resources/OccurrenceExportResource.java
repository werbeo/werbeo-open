package org.infinitenature.service.v1.resources;

import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportStatusResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportsInfoResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * Resource for the export of occurrences.
 * <p>
 * The workflow will be
 * <ol>
 * <li>Select data with
 * {@link OccurrenceExportResource #find(OccurrenceFilter)}</li>
 * <li>Check with the {@link OccurrenceExportResponse#getExportId()} retrieved
 * export id if the export is finished with
 * {@link OccurrenceExportResource #getStatus(UUID)}, or wait for an email.</li>
 * <li>Download the export at {@link OccurrenceExportResource #getExport(UUID)}</li>
 * <li>Delete an export with the {@link OccurrenceExportResponse#getExportId()} retrieved
 * export id</li>
 * </ol>
 *
 * @author dve
 *
 */
@Path("/v1/{portalId}/occurrences/export")
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
public interface OccurrenceExportResource
{
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   @Path("{exportId}")
   public Response getExport(@PathParam("exportId") UUID exportId,
         @PathParam("portalId") @Min(1) int portalId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   @Path("{exportId}/status")
   public OccurrenceExportStatusResponse getStatus(
         @PathParam("exportId") UUID exportId,
         @PathParam("portalId") @Min(1) int portalId);

   /**
    * Returns the export jobs of the current user.
    *
    * @param portalId
    * @return
    */
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   public OccurrenceExportsInfoResponse findExports(
         @PathParam("portalId") @Min(1) int portalId,
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("ID") @QueryParam("sortField") OccurrenceExportField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder);

   /**
    * Requests an export
    *
    * @param filter
    * @param exportFormat
    *           the format of the export
    * @return
    */
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @POST
   @Path("/")
   public OccurrenceExportResponse create(
         @BeanParam @Valid OccurrenceFilter filter,
         @NotNull @QueryParam("format") ExportFormat exportFormat);
   
   @RequestHeaders({
      @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
      @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @POST
   @Path("/large")
   public OccurrenceExportResponse createLargeWKT(
      @BeanParam @Valid OccurrenceFilterPOST filter,
      @NotNull @QueryParam("format") ExportFormat exportFormat);

   /**
    * Deletes an export
    *
    * @param exportId of Export to be deleted
    * @return
    */
   @RequestHeaders({ @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({ @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @DELETE
   @Path("{exportId}")
   public DeletedResponse delete(@PathParam("exportId") UUID exportId,
         @PathParam("portalId") @Min(1) int portalId);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("count")
   @GET
   public CountResponse count(@PathParam("portalId") @Min(1) int portalId);

}
