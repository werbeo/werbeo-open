package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.response.FrequencyDistributionResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

/**
 * Resource to access statistics on a {@link Occurrence} level.
 *
 */
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/occurrences/stats")
public interface OccurrenceStatsResource
{
   /**
    * Resource to get how many occurrences have been created the last n-days.
    *
    * @param portalId
    * @param days
    *           how many days in the past
    * @return
    */
   @Path("/createdPerDay")
   @GET
   public FrequencyDistributionResponse getOccurrencesCreatedPerDay(
         @PathParam("portalId") @Min(1) int portalId,
         @QueryParam("days") @Min(1) int days);

   /**
    * Resource to get how many occurrences have been created the last n-weeks.
    *
    * @param portalId
    * @param weeks
    *           how many weeks in the past
    * @return
    */
   @Path("/createdPerWeek")
   @GET
   public FrequencyDistributionResponse getOccurrencesCreatedPerWeek(
         @PathParam("portalId") @Min(1) int portalId,
         @QueryParam("weeks") @Min(1) int weeks);

   /**
    * Resource to get how many occurrences have been created the last n-months.
    *
    * @param portalId
    * @param months
    *           how many months in the past
    * @return
    */
   @Path("/createdPerMonth")
   @GET
   public FrequencyDistributionResponse getOccurrencesCreatedPerMonth(
         @PathParam("portalId") @Min(1) int portalId,
         @QueryParam("months") @Min(1) int months);
}
