package org.infinitenature.service.v1.resources;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.infinitenature.service.v1.types.Language;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class TaxaFilter
{
   /**
    * The portal
    */
   @PathParam("portalId")
   @Min(1)
   private int portalId;
   /**
    * Filter by the name of the taxa
    * <p>
    * The search term is split up into parts and each part is checked to be the
    * starting part of the corresponding part of the taxa.
    * <p>
    * This search is case insensitive.
    * <p>
    * <table border="1">
    * <tr>
    * <td>Taxon</td>
    * <td>nameContains</td>
    * <td>matches</td>
    * </tr>
    * <tr>
    * <td>Abies alba</td>
    * <td>"ABIES"</td>
    * <td>true</td>
    * </tr>
    * <tr>
    * <td>Abies alba</td>
    * <td>"&nbsp;ABIES"</td>
    * <td>false</td>
    * </tr>
    * <tr>
    * <td>Abies alba</td>
    * <td>"AB AL"</td>
    * <td>true</td>
    * </tr>
    * </table>
    */
   @QueryParam("nameContains")
   private String nameContains;
   /**
    * languages of the taxa
    */
   @QueryParam("languages")
   @DefaultValue("LAT")
   private Set<Language> languages;

   /**
    * Filters by the given external keys.
    */
   @QueryParam("externalKeys")
   private Set<String> externalKeys = new HashSet<>();

   /**
    * If set to true, also synonyms of the taxa are returned.
    */
   @QueryParam("withSynonyms")
   @DefaultValue("false")
   private boolean withSynonyms;

   /**
    * only return taxa for which occurrences have changed since a given date
    */
   @QueryParam("occurrencesChangedSince")
   private LocalDate occurrencesChangedSince;

   /**
    * If set to true, only taxa wich are used for occurrences are returned.
    */
   @QueryParam("onlyUsedTaxa")
   @DefaultValue("false")
   private boolean onlyUsedTaxa;

   @QueryParam("taxaGroup")
   private String taxaGroup;

   @QueryParam("onlyTaxaAvailableForInput")
   @DefaultValue("false")
   /**
    * If set to true only taxa which are allowed to save occurrences are
    * returned.
    */
   private boolean onlyTaxaAvailableForInput;

   public TaxaFilter()
   {
      super();
   }

   public TaxaFilter(int portalId, String nameContains, Set<Language> languages,
         boolean withSynonyms, LocalDate occurrencesChangedSince,
         boolean onlyUsedTaxa, String taxaGroup, boolean onlyTaxaAvailableForInput)
   {
      super();
      this.portalId = portalId;
      this.nameContains = nameContains;
      this.languages = languages;
      this.withSynonyms = withSynonyms;
      this.occurrencesChangedSince = occurrencesChangedSince;
      this.onlyUsedTaxa = onlyUsedTaxa;
      this.taxaGroup = taxaGroup;
      this.onlyTaxaAvailableForInput = onlyTaxaAvailableForInput;
   }

   public int getPortalId()
   {
      return portalId;
   }

   public void setPortalId(int portalId)
   {
      this.portalId = portalId;
   }

   public String getNameContains()
   {
      return nameContains;
   }

   public void setNameContains(String nameContains)
   {
      this.nameContains = nameContains;
   }

   public Set<Language> getLanguages()
   {
      return languages;
   }

   public void setLanguages(Set<Language> languages)
   {
      this.languages = languages;
   }

   public boolean isWithSynonyms()
   {
      return withSynonyms;
   }

   public void setWithSynonyms(boolean withSynonyms)
   {
      this.withSynonyms = withSynonyms;
   }

   public LocalDate getOccurrencesChangedSince()
   {
      return occurrencesChangedSince;
   }

   public void setOccurrencesChangedSince(LocalDate occurrencesChangedSince)
   {
      this.occurrencesChangedSince = occurrencesChangedSince;
   }

   public boolean isOnlyUsedTaxa()
   {
      return onlyUsedTaxa;
   }

   public void setOnlyUsedTaxa(boolean onlyUsedTaxa)
   {
      this.onlyUsedTaxa = onlyUsedTaxa;
   }

   @Override
   public String toString()
   {
      return TaxaFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TaxaFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TaxaFilterBeanUtil.doEquals(this, obj);
   }

   public Set<String> getExternalKeys()
   {
      return externalKeys;
   }

   public void setExternalKeys(Set<String> externalKeys)
   {
      this.externalKeys = externalKeys;
   }

   public String getTaxaGroup()
   {
      return taxaGroup;
   }

   public void setTaxaGroup(String taxaGroup)
   {
      this.taxaGroup = taxaGroup;
   }

   public boolean isOnlyTaxaAvailableForInput()
   {
      return onlyTaxaAvailableForInput;
   }

   public void setOnlyTaxaAvailableForInput(boolean onlyTaxaAvailableForInput)
   {
      this.onlyTaxaAvailableForInput = onlyTaxaAvailableForInput;
   }

}