package org.infinitenature.service.support.xml.converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.ws.rs.ext.ParamConverter;

public class LocalDateTimeParamConverter
      implements ParamConverter<LocalDateTime>
{
   @Override
   public LocalDateTime fromString(String value)
   {
      if (value == null || value.trim().isEmpty())
      {
         return null;
      }
      return LocalDateTime.parse(value, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
   }

   @Override
   public String toString(LocalDateTime value)
   {
      return value == null ? ""
            : value.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
   }
}
