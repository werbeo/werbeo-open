package org.infinitenature.service.v1.types.response;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.MTB;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class MTBResponse extends BaseResponse
{
   private Set<MTB> coveredMTBs = new HashSet<>();

   public MTBResponse()
   {
      super();
   }

   public MTBResponse(Set<MTB> coveredMTBs)
   {
      super();
      this.coveredMTBs = coveredMTBs;
   }

   @XmlElementWrapper(name = "coveredMTB")
   @XmlElement(name = "mtb")
   public Set<MTB> getCoveredMTBs()
   {
      return coveredMTBs;
   }

   public void setCoveredMTBs(Set<MTB> coveredMTBs)
   {
      this.coveredMTBs = coveredMTBs;
   }

   @Override
   public String toString()
   {
      return MTBResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return MTBResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return MTBResponseBeanUtil.doEquals(this, obj);
   }
}
