package org.infinitenature.service.v1.types.support;

import java.util.List;
import org.infinitenature.service.v1.types.atom.Link;

public interface LinkAware
{

   List<Link> getLinks();

}
