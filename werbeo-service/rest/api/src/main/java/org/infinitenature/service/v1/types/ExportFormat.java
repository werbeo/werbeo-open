package org.infinitenature.service.v1.types;

public enum ExportFormat
{
   XML_BDE, WINART,
   /**
    * CSV format with encoding ISO-8859-1
    */
   CSV, KMZ
}
