package org.infinitenature.service.v1.types.meta;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.types.Value;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SurveyFieldConfig implements FieldConfig<SurveyField>
{
   private SurveyField field;
   private boolean mandantory;
   private String property;
   private String translationKey;
   private List<Value> values = new ArrayList<>();
   public SurveyFieldConfig()
   {
      super();
   }

   public SurveyFieldConfig(SurveyField field, boolean mandantory)
   {
      super();
      this.field = field;
      this.mandantory = mandantory;
      this.property = field.getProperty();
      this.translationKey = field.getTranslationKey();

   }

   @Override
   public SurveyField getField()
   {
      return field;
   }

   @Override
   public void setField(SurveyField field)
   {
      this.field = field;
      if (field != null)
      {
         this.property = field.getProperty();
         this.translationKey = field.getTranslationKey();
      }
   }

   @Override
   public boolean isMandantory()
   {
      return mandantory;
   }

   @Override
   public void setMandantory(boolean mandantory)
   {
      this.mandantory = mandantory;
   }

   public String getProperty()
   {
      return property;
   }

   public void setProperty(String property)
   {
      this.property = property;
   }

   @Override
   public String getTranslationKey()
   {
      return translationKey;
   }

   public void setTranslationKey(String translationKey)
   {
      this.translationKey = translationKey;
   }

   @Override
   public List<Value> getValues()
   {
      return values;
   }

   public void setValues(List<Value> values)
   {
      this.values = values;
   }

   @Override
   public String toString()
   {
      return SurveyFieldConfigBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyFieldConfigBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyFieldConfigBeanUtil.doEquals(this, obj);
   }
}
