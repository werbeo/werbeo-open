package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class TextResponse extends BaseResponse
{
   private String text;

   public TextResponse()
   {
      super();
   }

   public TextResponse(String text)
   {
      super();
      this.text = text;
   }

  

   @Override
   public String toString()
   {
      return TextResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TextResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TextResponseBeanUtil.doEquals(this, obj);
   }

   public String getText()
   {
      return text;
   }

   public void setText(String text)
   {
      this.text = text;
   }
}
