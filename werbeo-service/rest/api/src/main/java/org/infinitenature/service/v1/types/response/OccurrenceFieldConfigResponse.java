package org.infinitenature.service.v1.types.response;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class OccurrenceFieldConfigResponse extends BaseResponse
{

   private Set<OccurrenceFieldConfig> fieldConfigs = new HashSet<>();

   public OccurrenceFieldConfigResponse()
   {
      super();
   }

   public OccurrenceFieldConfigResponse(Set<OccurrenceFieldConfig> fieldConfigs)
   {
      super();
      this.fieldConfigs = fieldConfigs;
   }

   @XmlElementWrapper(name = "fieldConfigs")
   @XmlElement(name = "fieldConfig")
   @JsonProperty("fieldConfigs")
   public Set<OccurrenceFieldConfig> getFieldConfigs()
   {
      return fieldConfigs;
   }

   public void setFieldConfigs(Set<OccurrenceFieldConfig> fieldConfigs)
   {
      this.fieldConfigs = fieldConfigs;
   }

   @Override
   public String toString()
   {
      return OccurrenceFieldConfigResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceFieldConfigResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceFieldConfigResponseBeanUtil.doEquals(this, obj);
   }
}
