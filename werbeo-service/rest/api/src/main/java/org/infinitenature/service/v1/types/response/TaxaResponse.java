package org.infinitenature.service.v1.types.response;

import net.vergien.beanautoutils.annotation.Bean;
import org.infinitenature.service.v1.types.Taxon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Bean
public class TaxaResponse extends BaseResponse
{
   private List<Taxon> taxa;

   public TaxaResponse ()
   {
      super();
   }

   public TaxaResponse (List<Taxon> taxa)
   {
      super();
      this.taxa = taxa;
   }

   public List<Taxon> getTaxa()
   {
      return taxa;
   }

   public void setTaxa(List<Taxon> taxa)
   {
      this.taxa = taxa;
   }
}
