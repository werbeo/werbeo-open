package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.atom.Link;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
@XmlRootElement
public class SaveOrUpdateResponse extends BaseResponse
{
   private Object entityId;
   private Link link;

   public SaveOrUpdateResponse()
   {
      super();
   }

   public SaveOrUpdateResponse(Link entity, Object entityId)
   {
      super();
      this.link = entity;
      this.entityId = entityId;
   }

   public Link getLink()
   {
      return link;
   }

   public Object getEntityId()
   {
      return entityId;
   }

   public void setEntityId(Object entityId)
   {
      this.entityId = entityId;
   }

   public void setLink(Link link)
   {
      this.link = link;
   }

   @Override
   public String toString()
   {
      return SaveOrUpdateResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SaveOrUpdateResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SaveOrUpdateResponseBeanUtil.doEquals(this, obj);
   }

}
