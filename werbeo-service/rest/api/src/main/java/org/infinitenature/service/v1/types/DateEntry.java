package org.infinitenature.service.v1.types;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Bean
public class DateEntry
{
   private LocalDate startDate;

   private int count;

   @Override
   public boolean equals(Object obj)
   {
      return DateEntryBeanUtil.doEquals(this, obj);
   }

   public LocalDate getStartDate()
   {
      return startDate;
   }

   public int getCount()
   {
      return count;
   }

   @Override
   public int hashCode()
   {
      return DateEntryBeanUtil.doToHashCode(this);
   }

   public void setStartDate(LocalDate startDate)
   {
      this.startDate = startDate;
   }

   public void setCount(int value)
   {
      this.count = value;
   }

   @Override
   public String toString()
   {
      return DateEntryBeanUtil.doToString(this);
   }
}