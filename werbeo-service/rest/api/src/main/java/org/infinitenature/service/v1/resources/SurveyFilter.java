package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class SurveyFilter
{
   /**
    * The portal
    */
   @PathParam("portalId")
   @Min(1)
   private int portalId;
   /**
    * Filter by the name of the survey
    */
   @QueryParam("nameContains")
   private String nameContains;

   @QueryParam("name")
   private String name;
   @QueryParam("parentSurveyId")
   private Integer parentSurveyId;
   @QueryParam("container")
   private Boolean container;

   public Boolean getContainer()
   {
      return container;
   }

   public void setContainer(Boolean container)
   {
      this.container = container;
   }

   public int getPortalId()
   {
      return portalId;
   }

   public void setPortalId(int portalId)
   {
      this.portalId = portalId;
   }

   public String getNameContains()
   {
      return nameContains;
   }

   public void setNameContains(String nameContains)
   {
      this.nameContains = nameContains;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public Integer getParentSurveyId()
   {
      return parentSurveyId;
   }

   public void setParentSurveyId(Integer parentSurveyId)
   {
      this.parentSurveyId = parentSurveyId;
   }

   @Override
   public String toString()
   {
      return SurveyFilterBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SurveyFilterBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SurveyFilterBeanUtil.doEquals(this, obj);
   }
}
