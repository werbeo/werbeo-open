package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.ServiceInfo;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class ServiceInfoResponse extends BaseResponse
{
   private ServiceInfo serviceInfo;

   public ServiceInfoResponse()
   {
      super();
   }

   public ServiceInfoResponse(ServiceInfo serviceInfo)
   {
      super();
      this.serviceInfo = serviceInfo;
   }

   public ServiceInfo getServiceInfo()
   {
      return serviceInfo;
   }

   public void setServiceInfo(ServiceInfo serviceInfo)
   {
      this.serviceInfo = serviceInfo;
   }

   @Override
   public String toString()
   {
      return ServiceInfoResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return ServiceInfoResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return ServiceInfoResponseBeanUtil.doEquals(this, obj);
   }
}
