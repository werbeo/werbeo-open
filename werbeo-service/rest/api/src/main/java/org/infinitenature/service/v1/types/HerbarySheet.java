package org.infinitenature.service.v1.types;

import org.infinitenature.herbariorum.entities.Institution;

import javax.ws.rs.BeanParam;
import javax.ws.rs.QueryParam;

public class HerbarySheet
{
   @BeanParam
   private Institution institution= null;

   @QueryParam("herbary")
   private String herbary = null;

   @QueryParam("author")
   private String author = null;

   @QueryParam("synonym")
   private String synonym = null;

   @QueryParam("book")
   private String book = null;

   @QueryParam("habitat")
   private String habitat = null;

   public Institution getInstitution()
   {
      return institution;
   }

   public void setInstitution(Institution institution)
   {
      this.institution = institution;
   }

   public String getHerbary()
   {
      return herbary;
   }

   public void setHerbary(String herbary)
   {
      this.herbary = herbary;
   }

   public String getSynonym()
   {
      return synonym;
   }

   public void setSynonym(String synonym)
   {
      this.synonym = synonym;
   }

   public String getBook()
   {
      return book;
   }

   public void setBook(String book)
   {
      this.book = book;
   }

   public String getHabitat()
   {
      return habitat;
   }

   public void setHabitat(String habitat)
   {
      this.habitat = habitat;
   }

   public String getAuthor()
   {
      return author;
   }

   public void setAuthor(String author)
   {
      this.author = author;
   }
}
