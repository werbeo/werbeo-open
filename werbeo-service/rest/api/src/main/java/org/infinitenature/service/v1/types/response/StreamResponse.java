package org.infinitenature.service.v1.types.response;

import java.util.ArrayList;
import java.util.List;

public abstract class StreamResponse<T, I> extends BaseResponse
{
   protected List<T> content = new ArrayList<>();
   protected List<I> deletedEntities = new ArrayList<>();
   private String nextToken;

   public StreamResponse()
   {
      super();
   }

   public StreamResponse(List<T> content, List<I> deletedEntities,
         String nextToken)
   {
      super();
      this.content = content;
      this.deletedEntities = deletedEntities;
      this.nextToken = nextToken;
   }

   public abstract List<T> getContent();

   public abstract void setContent(List<T> content);

   public List<I> getDeletedEntities()
   {
      return deletedEntities;
   }

   public void setDeletedEntities(List<I> deletedEntities)
   {
      this.deletedEntities = deletedEntities;
   }

   public String getNextToken()
   {
      return nextToken;
   }

   public void setNextToken(String nextToken)
   {
      this.nextToken = nextToken;
   }
}
