package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.TaxaList;

@XmlRootElement
public class TaxaListResponse extends BaseResponse
{
   private TaxaList taxaList;

   public TaxaListResponse()
   {
      super();
   }

   public TaxaListResponse(TaxaList taxaList)
   {
      super();
      this.taxaList = taxaList;
   }

   public TaxaList getTaxaList()
   {
      return taxaList;
   }

   public void setTaxaList(TaxaList taxaList)
   {
      this.taxaList = taxaList;
   }

}
