package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.PersonResponse;
import org.infinitenature.service.v1.types.response.PersonSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/people")
public interface PeopleResource
{
   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   public PersonSliceResponse find(
         @Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("ID") @QueryParam("sortField") PersonField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder,
         @BeanParam PeopleFilter parameterObject);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @GET
   @Path("{id}")
   public PersonResponse getPerson(@PathParam("portalId") @Min(1) int portalId,
         @PathParam("id") int id);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @Path("count")
   @GET
   public CountResponse count(@BeanParam PeopleFilter peopleFilter);

   @RequestHeaders({
         @RequestHeader(name = "Authorization", description = "The auth token") })
   @StatusCodes({
         @ResponseCode(code = 403, condition = "The user has not enough rights") })
   @POST
   public SaveOrUpdateResponse save(@PathParam("portalId") @Min(1) int portalId,
         Person person);
}
