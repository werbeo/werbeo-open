package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.SurveySortField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.service.v1.types.response.SurveyBaseResponse;
import org.infinitenature.service.v1.types.response.SurveyFieldConfigResponse;
import org.infinitenature.service.v1.types.response.SurveysSliceResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/surveys")
public interface SurveysResource
{
   @Path("meta")
   @GET
   public SurveyFieldConfigResponse getFieldConfig(
         @PathParam("portalId") @Min(1) int portalId);

   @GET
   public SurveysSliceResponse find(@Min(0) @DefaultValue("0") @QueryParam("offset") int offset,
         @DefaultValue("10") @QueryParam("limit") int limit,
         @DefaultValue("ID") @QueryParam("sortField") SurveySortField sortField,
         @DefaultValue("ASC") @QueryParam("sortOrder") SortOrder sortOrder,
         @BeanParam SurveyFilter filter);

   @Path("count")
   @GET
   public CountResponse count(@BeanParam SurveyFilter filter);

   @POST
   public SaveOrUpdateResponse save(@PathParam("portalId") @Min(1) int portalId,
         SurveyBase survey);

   @GET
   @Path("{id}")
   public SurveyBaseResponse getSurvey(
         @PathParam("portalId") @Min(1) int portalId, @PathParam("id") int id);
}
