package org.infinitenature.service.v1.types;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.meta.HasEnumTranslationKey;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@XmlRootElement(name = "taxon")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Taxon extends TaxonBase
{
   public enum RedListStatus implements HasEnumTranslationKey<RedListStatus>
   {
      /**
       *
       *  *  nicht gefährdet
       *  nb  nicht bewertet
       *  2   stark gefährdet
       *  1   vom aussterben bedroht
       *  0  ausgestorben oder verschollen
       *  R  extrem selten
       *  3  gefährdet
       *  V  Vorwarnliste
       *  G gefährdung unbekannten ausmaßes
       *  D Daten unzureichend
       *
       */

      SAFE, NB, ZERO, ONE, TWO, THREE, V, G, D, R
   }

   private RedListStatus redListStatus;

   public void setRedListStatus(RedListStatus redListStatus)
   {
      this.redListStatus = redListStatus;
   }

   public RedListStatus getRedListStatus()
   {
      return redListStatus;
   }


   private List<Document> documents = new ArrayList<>();

   @JsonProperty("language")
   private Language language;

   public Language getLanguage()
   {
      return language;
   }

   public void setLanguage(Language language)
   {
      this.language = language;
   }

   public List<Document> getDocuments()
   {
      return documents;
   }

   public void setDocuments(List<Document> documents)
   {
      this.documents = documents;
   }
}
