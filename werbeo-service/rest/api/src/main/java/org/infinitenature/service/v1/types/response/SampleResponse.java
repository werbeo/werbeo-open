package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.Sample;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class SampleResponse extends BaseResponse
{
   private Sample sample;

   public SampleResponse()
   {
      super();
   }

   public SampleResponse(Sample sample)
   {
      super();
      this.sample = sample;
   }

   public Sample getSample()
   {
      return sample;
   }

   public void setSample(Sample sample)
   {
      this.sample = sample;
   }

   @Override
   public String toString()
   {
      return SampleResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return SampleResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return SampleResponseBeanUtil.doEquals(this, obj);
   }
}
