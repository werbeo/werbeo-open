package org.infinitenature.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.infinitenature.service.v1.types.response.TranslationResponse;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

/**
 * Resource for dealing with translations
 *
 */

@RequestHeaders(@RequestHeader(name = Header.WERBEO_REQUEST_SOURCE_ID_NAME, description = Header.WERBEO_REQUEST_SOURCE_ID_DESCRIPTION))
@Path("/v1/{portalId}/translation")
public interface TranslationResource
{
   /**
    * Returns the translation for a given key and language tag
    *
    * @param portalId
    * @param key
    * @param languageTag
    *           BCP47 language Tag
    * @return the translation
    */
   @Produces({ MediaType.TEXT_PLAIN })
   @GET
   @Path("/{languageTag}/{key}")
   public String getTranslation(@PathParam("portalId") @Min(1) int portalId,
         @PathParam("key") String key,
         @PathParam("languageTag") String languageTag);

   /**
    * Returns all the translation key value pairs.
    * <p>
    * Note: there is no guarantee that keys are stable.
    *
    * @param portalId
    * @param languageTag
    *           BCP47 language Tag - not evaluated right now, but must be
    *           supplied
    * @return
    */
   @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
   @GET
   @Path("/{languageTag}")
   public TranslationResponse getTranslations(
         @PathParam("portalId") @Min(1) int portalId,
         @PathParam("languageTag") String languageTag);
}
