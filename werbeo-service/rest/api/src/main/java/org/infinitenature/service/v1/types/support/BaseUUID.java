package org.infinitenature.service.v1.types.support;

import java.util.UUID;

import javax.xml.bind.annotation.XmlAttribute;

public abstract class BaseUUID extends Base<UUID>
{
   public BaseUUID()
   {
      super();
   }

   @XmlAttribute(name = "id")
   private UUID entityId;

   @Override
   public UUID getEntityId()
   {
      return entityId;
   }

   @Override
   public void setEntityId(UUID entiyId)
   {
      this.entityId = entiyId;
   }
}
