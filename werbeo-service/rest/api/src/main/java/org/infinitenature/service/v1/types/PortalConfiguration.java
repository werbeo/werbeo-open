package org.infinitenature.service.v1.types;

import java.util.ArrayList;
import java.util.List;

import org.infinitenature.service.v1.types.enums.CoordinateSystem;
import org.infinitenature.service.v1.types.enums.ExportPolicy;
import org.infinitenature.service.v1.types.enums.LayerName;
import org.infinitenature.service.v1.types.enums.StaticMapStyle;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class PortalConfiguration
{
   /**
    * The ID of the {@link Survey} which is the default one for online entered
    * data at this portal.
    */
   private Integer defaultDataEntrySurveyId = null;
   /**
    * The initial zoom factor for displaying maps on this portal.
    */
   private Double mapInitialZoom;
   /**
    * The initial latitude for displayoing maps on this portal.
    */
   private Double mapInitialLatitude;
   /**
    * The initial longitude for displayoing maps on this portal.
    */
   private Double mapInitialLongitude;
   /**
    * The available map layers for this portal.
    *
    * @see LayerName
    */
   private List<LayerName> mapOverlayLayers = new ArrayList<>();
   /**
    * The maximum allowed size of an upload in MB
    */
   private int maxUploadSize;
   /**
    * The styles of static maps available for this portal.
    * 
    * @see StaticMapStyle
    */
   private StaticMapStyle staticMapStyle = null;
   /**
    * Indicates if there is anonymous access allowed to this portal.
    */
   private boolean allowAnonymousAccess;
   
   /**
    * Available CoordinateSystems 
    */
   private List<CoordinateSystem> coordinateSystems = new ArrayList<>();
   
   /**
    * Export Policies depending on Role Approved User
    * */
   private List<ExportPolicy> exportPolicies = new ArrayList<>();

   public List<ExportPolicy> getExportPolicies()
   {
      return exportPolicies;
   }

   public void setExportPolicies(List<ExportPolicy> exportPolicies)
   {
      this.exportPolicies = exportPolicies;
   }

   public StaticMapStyle getStaticMapStyle()
   {
      return staticMapStyle;
   }

   public void setStaticMapStyle(StaticMapStyle staticMapStyle)
   {
      this.staticMapStyle = staticMapStyle;
   }

   public Integer getDefaultDataEntrySurveyId()
   {
      return defaultDataEntrySurveyId;
   }

   public void setDefaultDataEntrySurveyId(Integer defaultDataEntrySurveyId)
   {
      this.defaultDataEntrySurveyId = defaultDataEntrySurveyId;
   }

   public Double getMapInitialZoom()
   {
      return mapInitialZoom;
   }

   public void setMapInitialZoom(Double mapInitialZoom)
   {
      this.mapInitialZoom = mapInitialZoom;
   }

   public Double getMapInitialLatitude()
   {
      return mapInitialLatitude;
   }

   public void setMapInitialLatitude(Double mapInitialLatitude)
   {
      this.mapInitialLatitude = mapInitialLatitude;
   }

   public Double getMapInitialLongitude()
   {
      return mapInitialLongitude;
   }

   public void setMapInitialLongitude(Double mapInitialLongitude)
   {
      this.mapInitialLongitude = mapInitialLongitude;
   }

   public List<LayerName> getMapOverlayLayers()
   {
      return mapOverlayLayers;
   }

   public void setMapOverlayLayers(List<LayerName> mapOverlayLayers)
   {
      this.mapOverlayLayers = mapOverlayLayers;
   }

   public int getMaxUploadSize()
   {
      return maxUploadSize;
   }

   public void setMaxUploadSize(int maxUploadSize)
   {
      this.maxUploadSize = maxUploadSize;
   }

   public boolean isAllowAnonymousAccess()
   {
      return allowAnonymousAccess;
   }

   public void setAllowAnonymousAccess(boolean allowAnonymousAccess)
   {
      this.allowAnonymousAccess = allowAnonymousAccess;
   }

   @Override
   public String toString()
   {
      return PortalConfigurationBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PortalConfigurationBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PortalConfigurationBeanUtil.doEquals(this, obj);
   }

   public List<CoordinateSystem> getCoordinateSystems()
   {
      return coordinateSystems;
   }

   public void setCoordinateSystems(List<CoordinateSystem> coordinateSystems)
   {
      this.coordinateSystems = coordinateSystems;
   }
}
