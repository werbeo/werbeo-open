package org.infinitenature.service.v1.types;

import java.util.List;

import org.infinitenature.service.v1.types.enums.Role;

import net.vergien.beanautoutils.annotation.Bean;

@Bean
public class User
{
   private String email;
   private List<Role> roles;

   public User(String login, List<Role> roles)
   {
      this.email = login;
      this.roles = roles;
   }

   public User()
   {
      super();
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public List<Role> getRoles()
   {
      return roles;
   }

   public void setRoles(List<Role> roles)
   {
      this.roles = roles;
   }

   @Override
   public String toString()
   {
      return UserBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return UserBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return UserBeanUtil.doEquals(this, obj);
   }
}
