package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.UserInfo;

@XmlRootElement
public class UserInfoResponse extends BaseResponse
{
   private UserInfo userInfo;

   public UserInfoResponse()
   {
      super();
   }

   public UserInfoResponse(UserInfo userInfo)
   {
      super();
      this.userInfo = userInfo;
   }

   public UserInfo getUserInfo()
   {
      return userInfo;
   }

   public void setUserInfo(UserInfo userInfo)
   {
      this.userInfo = userInfo;
   }

}
