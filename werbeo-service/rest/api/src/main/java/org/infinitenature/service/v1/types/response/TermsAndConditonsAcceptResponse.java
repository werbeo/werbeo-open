package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class TermsAndConditonsAcceptResponse extends BaseResponse
{
   private boolean accepted;

   public TermsAndConditonsAcceptResponse()
   {
      super();
   }

   public TermsAndConditonsAcceptResponse(boolean accepted)
   {
      super();
      this.accepted = accepted;
   }

   public boolean isAccepted()
   {
      return accepted;
   }

   public void setAccepted(boolean accepted)
   {
      this.accepted = accepted;
   }

   @Override
   public String toString()
   {
      return TermsAndConditonsAcceptResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return TermsAndConditonsAcceptResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return TermsAndConditonsAcceptResponseBeanUtil.doEquals(this, obj);
   }
}
