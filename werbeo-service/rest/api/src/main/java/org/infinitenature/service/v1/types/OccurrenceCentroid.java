package org.infinitenature.service.v1.types;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement(name = "occurrence-centroid")
@Bean
public class OccurrenceCentroid
{
   private LocalDate date;
   /**
    * The wkt of the centroid of the {@link Occurrence}. In epsg 31468.
    */
   private String centroid;

   public LocalDate getDate()
   {
      return date;
   }

   public void setDate(LocalDate date)
   {
      this.date = date;
   }

   public String getCentroid()
   {
      return centroid;
   }

   public void setCentroid(String centroid)
   {
      this.centroid = centroid;
   }

   @Override
   public String toString()
   {
      return OccurrenceCentroidBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceCentroidBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceCentroidBeanUtil.doEquals(this, obj);
   }
}
