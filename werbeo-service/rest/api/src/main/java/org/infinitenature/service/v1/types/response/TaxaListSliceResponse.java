package org.infinitenature.service.v1.types.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.TaxaList;

@XmlRootElement
public class TaxaListSliceResponse extends SliceResponse<TaxaList>
{
   public TaxaListSliceResponse()
   {
      super();
   }

   public TaxaListSliceResponse(List<TaxaList> content, int size)
   {
      super(content, size);
   }

   @XmlElementWrapper(name = "taxaLists")
   @XmlElement(name = "taxaList")
   public List<TaxaList> getContent()
   {
      return content;
   }

   public void setContent(List<TaxaList> content)
   {
      this.content = content;
   }

}
