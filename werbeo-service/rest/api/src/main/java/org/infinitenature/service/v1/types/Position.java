package org.infinitenature.service.v1.types;

import javax.xml.bind.annotation.XmlRootElement;

import net.vergien.beanautoutils.annotation.Bean;

/**
 * Describes the position of a sample.
 * <p>
 * Positions do not have an id. They are not a stand alone entity.
 *
 */
@Bean
@XmlRootElement(name = "position")
public class Position
{
   public enum PositionType
   {
      POINT, MTB, SHAPE, SQUARE
   }

   private MTB mtb;

   private int epsg;

   private String wkt;

   private int wktEpsg;
   private PositionType type;
   private double posCenterLatitude;
   private double posCenterLongitude;

   /**
    * The german grid square in which the position is located
    */
   public MTB getMtb()
   {
      return mtb;
   }

   public void setMtb(MTB mtb)
   {
      this.mtb = mtb;
   }

   /**
    * The epsg code of the reference system the position
    */
   public int getEpsg()
   {
      return epsg;
   }

   public void setEpsg(int epsg)
   {
      this.epsg = epsg;
   }

   public PositionType getType()
   {
      return type;
   }

   public void setType(PositionType type)
   {
      this.type = type;
   }

   public double getPosCenterLatitude()
   {
      return posCenterLatitude;
   }

   public void setPosCenterLatitude(double posCenterLatitude)
   {
      this.posCenterLatitude = posCenterLatitude;
   }

   public double getPosCenterLongitude()
   {
      return posCenterLongitude;
   }

   public void setPosCenterLongitude(double posCenterLongitude)
   {
      this.posCenterLongitude = posCenterLongitude;
   }

   /**
    * The wkt of the position
    */
   public String getWkt()
   {
      return wkt;
   }

   public void setWkt(String wkt)
   {
      this.wkt = wkt;
   }

   /**
    * The epsg code of the reference system the wkt
    */
   public int getWktEpsg()
   {
      return wktEpsg;
   }

   public void setWktEpsg(int wktEpsg)
   {
      this.wktEpsg = wktEpsg;
   }

   @Override
   public String toString()
   {
      return PositionBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return PositionBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return PositionBeanUtil.doEquals(this, obj);
   }
}
