package org.infinitenature.service.v1.types.response;

import javax.xml.bind.annotation.XmlRootElement;

import org.infinitenature.service.v1.types.OccurrenceImport;

import net.vergien.beanautoutils.annotation.Bean;

@XmlRootElement
@Bean
public class OccurrenceImportStatusResponse extends BaseResponse
{
   private OccurrenceImport status;

   public OccurrenceImportStatusResponse()
   {
      super();
   }

   public OccurrenceImportStatusResponse(OccurrenceImport status)
   {
      super();
      this.status = status;
   }

   public OccurrenceImport getStatus()
   {
      return status;
   }

   public void setStatus(OccurrenceImport status)
   {
      this.status = status;
   }


   @Override
   public String toString()
   {
      return OccurrenceImportStatusResponseBeanUtil.doToString(this);
   }

   @Override
   public int hashCode()
   {
      return OccurrenceImportStatusResponseBeanUtil.doToHashCode(this);
   }

   @Override
   public boolean equals(Object obj)
   {
      return OccurrenceImportStatusResponseBeanUtil.doEquals(this, obj);
   }
}
