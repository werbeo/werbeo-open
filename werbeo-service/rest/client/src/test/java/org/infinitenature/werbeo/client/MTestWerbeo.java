package org.infinitenature.werbeo.client;

import org.infinitenature.service.v1.resources.TaxaFilter;
import org.junit.Test;

public class MTestWerbeo {

  @Test
  public void test() {
    Werbeo werbeo = new Werbeo("http://localhost:8087/api");
      System.out
            .println(werbeo.info().getServiceUser().getUserInfo().getEmail());
  }

  @Test
  public void testTaxa() {
    Werbeo werbeo = new Werbeo("http://localhost:8087/api");
    System.out
            .println(werbeo.taxa()
                  .find(0, 20, new TaxaFilter(2, "Abies ALba", null, false,
                        null, false, null, false))
                  .getContent().get(0).getName());
  }

}
