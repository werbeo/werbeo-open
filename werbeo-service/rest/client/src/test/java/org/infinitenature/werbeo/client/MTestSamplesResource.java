package org.infinitenature.werbeo.client;

import java.util.List;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Sample;
import org.junit.Test;

public class MTestSamplesResource
{
   @Test
   public void test()
   {
      Werbeo werbeo = new Werbeo("http://localhost:8087/api");

      List<Occurrence> occurrences = werbeo.occurrences()
            .find(0, 1, OccurrenceField.TAXON, SortOrder.ASC,
                  new OccurrenceFilter(1, null, null, null,null, null, 0, true,
                        null, null, false, null, null, null, null), false)
            .getContent();

      Sample sample = werbeo.samples()
            .get(1, occurrences.get(0).getSample().getEntityId()).getSample();

      System.out.println(sample);
   }
}
