package org.infinitenature.werbeo.client;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Priority;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
@Priority(Integer.MIN_VALUE)
public class ClientErrorLoggingFilter implements ClientResponseFilter
{
   class LoggingStream extends FilterOutputStream
   {

      private final StringBuilder b;
      private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      private final int maxEntitySize;

      LoggingStream(final StringBuilder b, final OutputStream inner,
            final int maxEntitySize)
      {
         super(inner);

         this.b = b;
         this.maxEntitySize = maxEntitySize;
      }

      @Override
      public void write(final int i) throws IOException
      {
         if (baos.size() <= maxEntitySize)
         {
            baos.write(i);
         }
         out.write(i);
      }
   }

   private static final int DEFAULT_MAX_ENTITY_SIZE = 8 * 1024;
   private static final String ENTITY_LOGGER_PROPERTY = ClientErrorLoggingFilter.class
         .getName() + ".entityLogger";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ClientErrorLoggingFilter.class);
   private static final String LOGGING_ID_PROPERTY = ClientErrorLoggingFilter.class
         .getName() + ".id";
   private static final String NOTIFICATION_PREFIX = "* ";
   private static final String REQUEST_PREFIX = "> ";
   private static final String RESPONSE_PREFIX = "< ";
   private final AtomicLong idCounter = new AtomicLong(0);
   private final int maxEntitySize;
   private final boolean printEntity;

   public ClientErrorLoggingFilter()
   {
      this(DEFAULT_MAX_ENTITY_SIZE, true);
   }

   public ClientErrorLoggingFilter(int maxEntitySize, boolean printEntity)
   {
      this.maxEntitySize = maxEntitySize;
      this.printEntity = printEntity;
   }

   @Override
   public void filter(ClientRequestContext requestContext,
         ClientResponseContext responseContext) throws IOException
   {
      if (responseContext.getStatusInfo().getFamily() != Family.SUCCESSFUL)
      {
         log(requestContext);
         log(requestContext, responseContext);
      }
   }

   /**
    * Get the character set from a media type.
    * <p>
    * The character set is obtained from the media type parameter "charset". If
    * the parameter is not present the {@link #UTF8} charset is utilized.
    *
    * @param m
    *           the media type.
    * @return the character set.
    */
   protected Charset getCharset(MediaType m)
   {
      String name = (m == null) ? null
            : m.getParameters().get(MediaType.CHARSET_PARAMETER);
      return (name == null) ? StandardCharsets.UTF_8 : Charset.forName(name);
   }

   private Set<Map.Entry<String, List<String>>> getSortedHeaders(
         final Set<Map.Entry<String, List<String>>> headers)
   {
      final TreeSet<Map.Entry<String, List<String>>> sortedHeaders = new TreeSet<>(
            (o1, o2) -> o1.getKey().compareToIgnoreCase(o2.getKey()));
      sortedHeaders.addAll(headers);
      return sortedHeaders;
   }

   private void log(ClientRequestContext context)
   {
      final long id = idCounter.incrementAndGet();
      context.setProperty(LOGGING_ID_PROPERTY, id);

      final StringBuilder b = new StringBuilder();

      printRequestLine(b, "Sending client request", id, context.getMethod(),
            context.getUri());
      printPrefixedHeaders(b, id, REQUEST_PREFIX, context.getStringHeaders());

      if (printEntity && context.hasEntity())
      {
         final OutputStream stream = new LoggingStream(b,
               context.getEntityStream(), maxEntitySize);
         context.setEntityStream(stream);
         context.setProperty(ENTITY_LOGGER_PROPERTY, stream);
         // not calling log(b) here - it will be called by the interceptor
      } else
      {
         LOGGER.error(b.toString());
      }
   }

   private void log(ClientRequestContext requestContext,
         ClientResponseContext responseContext) throws IOException
   {
      final Object requestId = requestContext.getProperty(LOGGING_ID_PROPERTY);
      final long id = requestId != null ? (Long) requestId
            : idCounter.incrementAndGet();

      final StringBuilder b = new StringBuilder();

      printResponseLine(b, "Client response received",
            id,
            responseContext.getStatus());
      printPrefixedHeaders(b, id, RESPONSE_PREFIX,
            responseContext.getHeaders());

      if (printEntity && responseContext.hasEntity())
      {
         responseContext.setEntityStream(
               logInboundEntity(b, responseContext.getEntityStream(),
                     getCharset(responseContext.getMediaType())));
      }

      LOGGER.error(b.toString());
   }

   protected InputStream logInboundEntity(final StringBuilder b,
         InputStream stream, final Charset charset) throws IOException
   {
      if (!stream.markSupported())
      {
         stream = new BufferedInputStream(stream);
      }
      stream.mark(maxEntitySize + 1);
      final byte[] entity = new byte[maxEntitySize + 1];
      final int entitySize = stream.read(entity);
      b.append(new String(entity, 0, Math.min(entitySize, maxEntitySize),
            charset));
      if (entitySize > maxEntitySize)
      {
         b.append("...more...");
      }
      b.append('\n');
      stream.reset();
      return stream;
   }

   private StringBuilder prefixId(final StringBuilder b, final long id)
   {
      b.append(Long.toString(id)).append(" ");
      return b;
   }

   protected void printPrefixedHeaders(final StringBuilder b, final long id,
         final String prefix, final MultivaluedMap<String, String> headers)
   {
      for (final Map.Entry<String, List<String>> headerEntry : getSortedHeaders(
            headers.entrySet()))
      {
         final List<?> val = headerEntry.getValue();
         final String header = headerEntry.getKey();

         if (val.size() == 1)
         {
            prefixId(b, id).append(prefix).append(header).append(": ")
                  .append(val.get(0)).append("\n");
         } else
         {
            final StringBuilder sb = new StringBuilder();
            boolean add = false;
            for (final Object s : val)
            {
               if (add)
               {
                  sb.append(',');
               }
               add = true;
               sb.append(s);
            }
            prefixId(b, id).append(prefix).append(header).append(": ")
                  .append(sb.toString()).append("\n");
         }
      }
   }

   protected void printRequestLine(final StringBuilder b, final String note,
         final long id, final String method, final URI uri)
   {
      prefixId(b, id).append(NOTIFICATION_PREFIX).append(note)
            .append(" on thread ").append(Thread.currentThread().getName())
            .append("\n");
      prefixId(b, id).append(REQUEST_PREFIX).append(method).append(" ")
            .append(uri.toASCIIString()).append("\n");
   }

   protected void printResponseLine(final StringBuilder b, final String note,
         final long id, final int status)
   {
      prefixId(b, id).append(NOTIFICATION_PREFIX).append(note)
            .append(" on thread ").append(Thread.currentThread().getName())
            .append("\n");
      prefixId(b, id).append(RESPONSE_PREFIX).append(Integer.toString(status))
            .append("\n");
   }
}
