package org.infinitenature.werbeo.client;

import javax.ws.rs.client.ClientRequestFilter;

import org.infinitenature.service.v1.resources.InfoResource;
import org.infinitenature.service.v1.resources.OccurrenceCommentsResource;
import org.infinitenature.service.v1.resources.OccurrenceExportResource;
import org.infinitenature.service.v1.resources.OccurrenceImportResource;
import org.infinitenature.service.v1.resources.OccurrenceMediaResource;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.resources.OccurrenceStatsResource;
import org.infinitenature.service.v1.resources.PeopleResource;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.resources.SampleCommentsResource;
import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.resources.TermsAndConditionsResource;
import org.infinitenature.service.v1.resources.TranslationResource;
import org.infinitenature.service.v1.resources.UsersResource;
import org.infinitenature.service.v1.resources.security.TokenResource;
import org.infinitenature.service.v1.types.response.TextResponse;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public class Werbeo
{

   public ResteasyClient getClient()
   {
      return client;
   }

   private final ResteasyClient client = new ResteasyClientBuilder().build();

   public ResteasyWebTarget getTarget()
   {
      return target;
   }

   private final ResteasyWebTarget target;
   private final String url;

   public Werbeo(String url)
   {
      this.url = url;
      target = client.target(url);
      client.register(new ClientErrorLoggingFilter());
   }

   public Werbeo(String url, ClientRequestFilter filter)
   {
      this(url);
      client.register(filter);

   }

   public Werbeo register(Class<?> componentClass)
   {
      client.register(componentClass);
      return this;
   }

   public Werbeo register(Object component)
   {
      client.register(component, Integer.MAX_VALUE);
      return this;
   }

   public InfoResource info()
   {
      return target.proxy(InfoResource.class);
   }

   public OccurrenceResource occurrences()
   {
      return target.proxy(OccurrenceResource.class);
   }

   public OccurrenceExportResource occurrenceExports()
   {
      return target.proxy(OccurrenceExportResource.class);
   }

   public OccurrenceImportResource occurrenceImports()
   {
      return target.proxy(OccurrenceImportResource.class);
   }

   public SamplesResource samples()
   {
      return target.proxy(SamplesResource.class);
   }

   public SampleCommentsResource sampleComments()
   {
      return target.proxy(SampleCommentsResource.class);
   }

   public OccurrenceCommentsResource occurrenceComments()
   {
      return target.proxy(OccurrenceCommentsResource.class);
   }

   public OccurrenceMediaResource occurrenceMedia()
   {
      return target.proxy(OccurrenceMediaResource.class);
   }

   public PeopleResource people()
   {
      return target.proxy(PeopleResource.class);
   }

   public PortalResource portals()
   {
      return target.proxy(PortalResource.class);
   }

   public TaxaResource taxa()
   {
      return target.proxy(TaxaResource.class);
   }

   public SurveysResource surveys()
   {
      return target.proxy(SurveysResource.class);
   }
   
   public UsersResource users()
   {
      return target.proxy(UsersResource.class);
   }

   public TokenResource token()
   {
      return target.proxy(TokenResource.class);
   }

   public TermsAndConditionsResource termsAndConditions()
   {
      return target.proxy(TermsAndConditionsResource.class);
   }

   public TextResponse privacyDeclaration()
   {
      return target.proxy(TextResponse.class);
   }

   public TranslationResource translationResource()
   {
      return target.proxy(TranslationResource.class);
   }

   public OccurrenceStatsResource occurenceStats()
   {
      return target.proxy(OccurrenceStatsResource.class);
   }

   @Override
   public String toString()
   {
      StringBuilder builder = new StringBuilder();
      builder.append("Werbeo [url=");
      builder.append(url);
      builder.append("]");
      return builder.toString();
   }
}
