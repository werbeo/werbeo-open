package org.infinitenature.werbeo.service.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.infinitenature.service.v1.types.response.BaseResponse;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AddRequestIdToResponseAspect
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(AddRequestIdToResponseAspect.class);

   @Pointcut("execution(org.infinitenature.service.v1.types.response.BaseResponse+ org.infinitenature.werbeo.service.mapper.*.*(..))")
   public void mappingMethod()
   {
      // Empty because pointcut definition
   }

   @Around("mappingMethod()")
   public Object addRequestIdToResponse(ProceedingJoinPoint pjp) throws Throwable
   {

      Object retVal = pjp.proceed();
      if (retVal instanceof BaseResponse)
      {
         BaseResponse baseRespone = (BaseResponse) retVal;
         String requestId = getRequestId(pjp.getArgs());
         LOGGER.debug("Set request ID {} to response", requestId);
         baseRespone.getMetaData().setSessionId(requestId);
      }
      return retVal;
   }

   private String getRequestId(Object[] args)
   {
      for (Object arg : args)
      {
         if (arg instanceof Context)
         {
            return ((Context) arg).getRequestId();
         }
      }
      return null;
   }
}
