package org.infinitenature.werbeo.service.config;

import org.piwik.java.tracking.PiwikTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PiwikConfig
{
   @Bean
   public PiwikTracker tracker(@Autowired InstanceConfig instanceConfig)
   {
      return new PiwikTracker(instanceConfig.getPiwik().getHostUrl());
   }
}
