package org.infinitenature.werbeo.service.mapper;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.Amount;
import org.infinitenature.service.v1.types.Occurrence.Area;
import org.infinitenature.service.v1.types.Occurrence.BloomingSprouts;
import org.infinitenature.service.v1.types.Occurrence.NumericAmountAccuracy;
import org.infinitenature.service.v1.types.Occurrence.Quantity;
import org.infinitenature.service.v1.types.Occurrence.RecordStatus;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatus;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatusFukarek;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Occurrence.Vitality;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.service.v1.types.response.OccurrenceCentroidsSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceStreamResponse;
import org.infinitenature.werbeo.common.commons.DateTimeUtils;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.StreamSlice;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.util.ContinuationTokenFactory;
import org.infintenature.mtb.MTB;
import org.infintenature.mtb.MTBHelper;
import org.mapstruct.AfterMapping;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ValueMapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class,
      TaxonBaseMapperRest.class, SampleMapperRest.class, PersonMapperRest.class,
      MediaMapperRest.class })
public abstract class OccurrenceMapperRest extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.Occurrence, OccurrenceResource>
{
   private static final ZoneId UTC = ZoneId.of("UTC");
   @Autowired
   private ContinuationTokenFactory continuationTokenFactory;

   public OccurrenceMapperRest()
   {
      super(OccurrenceResource.class);
   }

   public abstract RecordStatus map(
         org.infinitenature.werbeo.service.core.api.enity.RecordStatus recordStatus);

   public abstract org.infinitenature.werbeo.service.core.api.enity.RecordStatus recordStatusToRecordStatus(
         RecordStatus recordStatus);

   public abstract Amount map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount amount);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.Amount amountToAmount(
         Amount amount);

   @ValueMapping(source = "TWENTYSIX", target = "TWENTYSIX_TO_FIFTY")
   @ValueMapping(source = "FIFTY", target = "MORE_THAN_FIFTY")
   @ValueMapping(source = "HUNDRED", target = "MORE_THAN_HUNDRED")
   @ValueMapping(source = "THOUSAND", target = "MORE_THAN_THOUSAND")
   @ValueMapping(source = "TENTHOUSAND", target = "MORE_THAN_TENTHOUSAND")
   public abstract Area map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.Area area);

   @InheritInverseConfiguration
   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.Area areaToArea(
         Area area);

   public abstract Vitality map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.Vitality vitality);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.Vitality vitalityToVitality(
         Vitality vitality);

   public abstract SettlementStatusFukarek map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatusFukarek occurrenceStatus);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatusFukarek occurrenceStatusToOccurrenceStatus(
         SettlementStatusFukarek occurrenceStatus);

   @ValueMapping(source = "LESS_ONE", target = "ONE_TO_FIVE")
   public abstract BloomingSprouts map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.BloomingSprouts bloomingSprouts);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.BloomingSprouts bloomingSproutsToBloomingSprouts(
         BloomingSprouts bloomingSprouts);

   public abstract Quantity map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.Quantity quantity);

   public abstract ValidationStatus map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus validationStatus);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.ValidationStatus map(
         ValidationStatus validationStatus);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.Quantity quantityToQuantity(
         Quantity quantity);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.Sex sexToSex(
         Occurrence.Sex sex);

   public abstract Occurrence.Sex map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.Sex sex);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.LifeStage lifeStageToLifeStage(
         Occurrence.LifeStage lifeStage);

   public abstract Occurrence.LifeStage map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.LifeStage lifeStage);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.Makropter makropoterToMakropter(
         Occurrence.Makropter makropter);

   public abstract Occurrence.Makropter map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.Makropter makropter);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.Reproduction reproductionToReproduction(
         Occurrence.Reproduction reproduction);

   public abstract Occurrence.Reproduction map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.Reproduction reproduction);

   public abstract SettlementStatus map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus status);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.SettlementStatus settlementStatusToSettlementStatus(
         SettlementStatus status);

   public abstract OccurrenceSortField mapToApi(
         OccurrenceField occurrenceField);

   @ValueMapping(source = "APPROXIMATE", target = "APPROXIMATE")
   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence.NumericAmountAccuracy map(
         NumericAmountAccuracy numericAmountAccuracy);

   @ValueMapping(source = "APPROXIMATE", target = "APPROXIMATE")
   public abstract NumericAmountAccuracy map(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence.NumericAmountAccuracy numericAmountAccuracy);

   @Mapping(target = "documents", source = "media")
   @Mapping(target = "comment", ignore = true)
   @Mapping(target = "reference", ignore = true)
   public abstract Occurrence mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.Occurrence entity,
         @org.mapstruct.Context Context context);

   public abstract List<Occurrence> mapToApi(
         List<org.infinitenature.werbeo.service.core.api.enity.Occurrence> entities,
         @org.mapstruct.Context Context context);

   public OccurrenceSliceResponse mapToApi(
         Slice<org.infinitenature.werbeo.service.core.api.enity.Occurrence, OccurrenceSortField> entitySlice,
         Context context)

   {
      return new OccurrenceSliceResponse(
            mapToApi(entitySlice.getContent(), context), entitySlice.getSize());
   }

   @Mapping(target = "media", ignore = true)
   public abstract org.infinitenature.werbeo.service.core.api.enity.Occurrence toCoreApi(
         Occurrence occurrence, @org.mapstruct.Context Context context);

   @Mapping(target = "herbaryCode", source = "herbarium")
   @Mapping(target = "portalBasedFilters", ignore = true)
   @Mapping(target = "requestingUser", ignore = true)
   public abstract OccurrenceFilter mapCoreToApi(
         org.infinitenature.service.v1.resources.OccurrenceFilter filter);

   public OccurrenceStreamResponse mapToApi(
         StreamSlice<org.infinitenature.werbeo.service.core.api.enity.Occurrence, UUID> streamSlice,
         @org.mapstruct.Context Context context)
   {
      return new OccurrenceStreamResponse(
            mapToApi(streamSlice.getEntities(), context),
            streamSlice.getDeletedIds(),
            continuationTokenFactory.write(streamSlice.getContinuationToken()));
   }

   public OccurrenceCentroidsSliceResponse mapOccurrenceCentroidSliceToApi(
         Slice<OccurrenceCentroid, OccurrenceCentroidSortField> entitySlice,
         Context context)
   {
      return new OccurrenceCentroidsSliceResponse(
            mapOccurrenceCentroidListToApi(entitySlice.getContent(), context),
            entitySlice.getSize());
   }

   public List<org.infinitenature.service.v1.types.OccurrenceCentroid> mapOccurrenceCentroidListToApi(
         List<OccurrenceCentroid> entity, Context context)
   {
      List<org.infinitenature.service.v1.types.OccurrenceCentroid> occurrenceCentroidList = new ArrayList<>();
      for (OccurrenceCentroid occurrenceCentroidCore : entity)
      {
         occurrenceCentroidList.add(mapCoreToApi(occurrenceCentroidCore));
      }
      return occurrenceCentroidList;
   }

   @Mapping(target = "centroid", expression = "java(occurrenceCentroidCore.getPoint().toText())")
   public abstract org.infinitenature.service.v1.types.OccurrenceCentroid mapCoreToApi(
         OccurrenceCentroid occurrenceCentroidCore);

   MTB map(String mtb)
   {
      if (StringUtils.isBlank(mtb))
      {
         return null;
      }
      return MTBHelper.toMTB(mtb);
   }

   @Mapping(target = "validator.createdBy", ignore = true)
   @Mapping(target = "validator.modifiedBy", ignore = true)
   @Mapping(target = "validationTime", source = "modificationDate")
   public abstract Validation mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.Validation validation);

   @Mapping(target = "validator.createdBy", ignore = true)
   @Mapping(target = "validator.modifiedBy", ignore = true)
   @Mapping(target = "allowedOperations", ignore = true)
   @Mapping(target = "createdBy", ignore = true)
   @Mapping(target = "creationDate", ignore = true)
   @Mapping(target = "id", ignore = true)
   @Mapping(target = "modificationDate", ignore = true)
   @Mapping(target = "modifiedBy", ignore = true)
   @Mapping(target = "obfuscated", ignore = true)
   public abstract org.infinitenature.werbeo.service.core.api.enity.Validation mapToCore(
         Validation validation);

   @AfterMapping
   void modifiyValidationTimezone(@MappingTarget Validation target)
   {
      final ZoneId targetTimeZone = ZoneId.of("Europe/Berlin");
      if (target.getValidationTime() != null)
      {
         target.setValidationTime(DateTimeUtils.toZone(target.getValidationTime(),
               UTC, targetTimeZone));
      }

   }
}
