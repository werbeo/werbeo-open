package org.infinitenature.werbeo.service.mapper;

import org.infinitenature.service.v1.types.support.BaseInt;
import org.infinitenature.service.v1.types.support.BaseUUID;
import org.infinitenature.werbeo.service.core.api.enity.BaseTypeInt;
import org.infinitenature.werbeo.service.core.api.enity.BaseTypeUUID;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;
import org.mapstruct.MappingInheritanceStrategy;

@MapperConfig(mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_FROM_CONFIG, componentModel = "spring", uses = OperationsMapper.class)
public interface CentralConfigRest
{
   @Mapping(target = "links", expression = "java(createLinks(entity, context))")
   @Mapping(target = "userAlloewdToEdit", constant = "false")
   @Mapping(source = "id", target = "entityId")
   @Mapping(target = "createdBy", source = "createdBy.login")
   @Mapping(target = "modifiedBy", source = "modifiedBy.login")
   BaseUUID uuidBaseTypeToWSType(
         org.infinitenature.werbeo.service.core.api.enity.BaseTypeUUID entity,
         @org.mapstruct.Context Context context);

   @Mapping(target = "links", expression = "java(createLinks(entity, context))")
   @Mapping(target = "userAlloewdToEdit", constant = "false")
   @Mapping(source = "id", target = "entityId")
   @Mapping(target = "createdBy", source = "createdBy.login")
   @Mapping(target = "modifiedBy", source = "modifiedBy.login")
   BaseInt intBaseTypeToWSType(BaseTypeInt entity,
         @org.mapstruct.Context Context context);

   @Mapping(target = "createdBy", ignore = true) // ignored, because it's
                                                 // handled by the software
   @Mapping(target = "modifiedBy", ignore = true) // ignored, because it's
                                                  // handled by the software
   @Mapping(target = "creationDate", ignore = true) // ignored, because it's
                                                    // handled by the software
   @Mapping(target = "modificationDate", ignore = true) // ignored, because it's
                                                        // handled by the
                                                        // software
   @Mapping(target = "id", source = "entityId")
   @Mapping(target = "allowedOperations", ignore = true) // don't trust this as
                                                         // user input
   @Mapping(target = "obfuscated", ignore = true) // don't trust this as user
                                                   // input
   BaseTypeInt WSTypeToIntBaseType(BaseInt base,
         @org.mapstruct.Context Context context);

   @Mapping(target = "createdBy", ignore = true) // ignored, because it's
                                                 // handled by the software
   @Mapping(target = "modifiedBy", ignore = true) // ignored, because it's
                                                  // handled by the software
   @Mapping(target = "creationDate", ignore = true) // ignored, because it's
                                                    // handled by the software
   @Mapping(target = "modificationDate", ignore = true) // ignored, because it's
                                                        // handled by the
                                                        // software
   @Mapping(target = "id", source = "entityId")
   @Mapping(target = "allowedOperations", ignore = true) // don't trust this as
                                                         // user input
   @Mapping(target = "obfuscated", ignore = true) // don't trust this as user
                                                   // input
   BaseTypeUUID WSTypeToUUIDBaseType(BaseUUID base,
         @org.mapstruct.Context Context context);

}
