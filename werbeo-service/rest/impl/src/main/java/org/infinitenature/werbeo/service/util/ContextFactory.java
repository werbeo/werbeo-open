package org.infinitenature.werbeo.service.util;

import java.util.Collections;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.werbeo.service.core.api.enity.Group;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class ContextFactory
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ContextFactory.class);
   private static final String WERBEO_REQUEST_SOURCE_ID = "werbeo-request-source-id";
   @Autowired
   private PortalQueryPort portalQueryPort;

   public Context createAnonymousContext(HttpHeaders httpHeaders)
   {
      LOGGER.info("Create anonymous context");
      String requestId = httpHeaders.getHeaderString(WERBEO_REQUEST_SOURCE_ID);
      return new Context(Portal.NO_PORTAL, null, Collections.emptySet(),
            requestId);
   }

   public Context createContext(int portalId, HttpHeaders httpHeaders)
   {
      LOGGER.info("Create context for portal {} and user {}", portalId,
            SecurityContextHolder.getContext().getAuthentication());
      Portal portal = portalQueryPort.load(portalId);

      PortalConfiguration portalConfiguration = portalQueryPort
            .loadConfig(portalId);

      if (portal == null)
      {
         // TODO throw proper exception
         throw new EntityNotFoundException(
               "Portal with id " + portalId + " does not exist.");
      }

      String userName = SecurityContextHolder.getContext().getAuthentication()
            .getName();
      User user;
      if ("anonymousUser".equals(userName))
      {
         user = null;
      } else
      {
         user = new User();
         user.setLogin(userName);
         KeycloakAuthenticationToken token = (KeycloakAuthenticationToken) SecurityContextHolder
               .getContext().getAuthentication();
         KeycloakPrincipal<KeycloakSecurityContext> principal = (KeycloakPrincipal<KeycloakSecurityContext>) token
               .getPrincipal();
         KeycloakSecurityContext session = principal
               .getKeycloakSecurityContext();
         AccessToken accessToken = session.getToken();

         Person person = new Person(accessToken.getGivenName(),
               accessToken.getFamilyName());
         user.setPerson(person);
      }
      String requestId = httpHeaders.getHeaderString(WERBEO_REQUEST_SOURCE_ID);
      return new Context(portal, user,
            SecurityContextHolder.getContext().getAuthentication()
                  .getAuthorities().stream()
                  .map(auth -> new Group(auth.getAuthority()))
                  .collect(Collectors.toSet()),
            portalConfiguration, requestId);
   }
}
