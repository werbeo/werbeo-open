package org.infinitenature.werbeo.service.mapper;

import java.util.Set;

import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class)
public abstract class PositionMapper
{
   @Autowired
   private PositionFactory positionFactory;

   @Mapping(target = "posCenterLatitude", expression = "java(position.getPosCenter()[1])")
   @Mapping(target = "posCenterLongitude", expression = "java(position.getPosCenter()[0])")
   public abstract Position toRestApi(
         org.infinitenature.werbeo.service.core.api.enity.Position position);

   public abstract MTB toRestApi(org.infintenature.mtb.MTB mtb);

   public abstract Set<MTB> toRestApi(Set<org.infintenature.mtb.MTB> mtbs);

   public org.infinitenature.werbeo.service.core.api.enity.Position toCoreApi(
         Position position)
   {
      switch (position.getType())
      {
      case MTB:
         return positionFactory.createFromMTB(position.getMtb().getMtb());
      case POINT:
      case SHAPE:
         return positionFactory.create(position.getWkt(),
               position.getWktEpsg());

      default:
         throw new IllegalArgumentException(
               "Unknown position type: " + position.getType());
      }
   }

   protected void setPositionFactory(PositionFactory positionFactory)
   {
      this.positionFactory = positionFactory;
   }
}
