package org.infinitenature.werbeo.service.v1.resources;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.OccurrenceCommentsResource;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.OccurrenceCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceCommandsPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.OccurrenceCommentsMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class OccurrenceCommentsResourceImpl
      implements OccurrenceCommentsResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceCommentsResourceImpl.class);
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private OccurrenceCommentsMapperRest commentMapper;
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;
   @Autowired
   private OccurrenceCommandsPort occurrenceCommandsPort;

   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public OccurrenceCommentsSliceResponse getComments(@Min(1) int portalId,
         UUID uuid, @Min(0) int offset, @Min(1) int limit,
         CommentField sortField, SortOrder sortOrder)
   {
      LOGGER.info("Request comments for occurrence {} for portal {}", uuid,
            portalId);
      Context context = contextFactory.createContext(portalId, httpHeaders);
      OffsetRequest<CommentSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, commentMapper.mapToApi(sortField));

      return commentMapper.mapOccurrenceToApi(
            occurrenceQueryPort.getComments(uuid, context, offsetRequest),
            context);
   }

   @Override
   public CountResponse countComments(@Min(1) int portalId, UUID occurrenceId)
   {
      LOGGER.info("Request comments for occurrence {} for portal {}",
            occurrenceId,
            portalId);
      Context context = contextFactory.createContext(portalId, httpHeaders);

      return new CountResponse(
            occurrenceQueryPort.countComments(occurrenceId, context),
            context.getRequestId());
   }

   @Override
   public SaveOrUpdateResponse saveComment(@Min(1) int portalId, UUID uuid,
         OccurrenceComment occurrenceComment)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      int id = occurrenceCommandsPort.addComment(uuid,
            commentMapper.mapToCore(occurrenceComment, context), context);
      Link link = commentMapper.createOccurrenceCommentIdLink(context, uuid,
            id);
      return new SaveOrUpdateResponse(link, id);

   }
}
