package org.infinitenature.werbeo.service.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.DocumentType;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaxonLinksMapper
{
   private final InstanceConfig instanceConfig;
   private final DocumentTypeMapperRest documentTypeMapper;

   public TaxonLinksMapper(@Autowired InstanceConfig instanceConfig,
         @Autowired DocumentTypeMapperRest documentTypeMapper)
   {
      this.instanceConfig = instanceConfig;
      this.documentTypeMapper = documentTypeMapper;
   }

   public Collection<Document> createDocuments(TaxonBase source,
         Context context)
   {
      List<Document> documents = new ArrayList<>();
      for (DocumentType docType : source.getDocumentsAvailable())
      {
         Link link = createLink(source, context, docType);
         if (link != null)
         {
            Document doc = new Document();
            doc.setLink(link);
            doc.setType(documentTypeMapper.mapToRestApi(docType));
            documents.add(doc);
         }
      }
      return documents;
   }

   private Link createLink(TaxonBase source, Context context,
         DocumentType docType)
   {
      Link link = null;
      switch (docType)
      {
      case MAP_TAXON_VEGETWEB_STYLE:
         link = new Link();
         link.setRel("self");
         link.setHref(instanceConfig.getStaticMapsURL() + "/"
               + context.getPortal().getId() + "/vegetweb-style/"
               + source.getName() + ".png");
         break;
      case MAP_TAXON_VEGETWEB_STYLE_THUMB:
         link = new Link();
         link.setRel("self");
         link.setHref(instanceConfig.getStaticMapsURL() + "/"
               + context.getPortal().getId() + "/vegetweb-style/"
               + source.getName() + "_thumb.png");
         break;
      case MAP_TAXON_VEGETWEB_STYLE_THUMB_L:
         link = new Link();
         link.setRel("self");
         link.setHref(instanceConfig.getStaticMapsURL() + "/"
               + context.getPortal().getId() + "/vegetweb-style/"
               + source.getName() + "_thumb_l.png");
         break;
      case MAP_TAXON_FLORA_MV_STYLE:
         link = new Link();
         link.setRel("self");
         link.setHref(instanceConfig.getStaticMapsURL() + "/"
               + context.getPortal().getId() + "/"
               + source.getGroup().substring(0, 1) + "/" + source.getName()
               + ".png");
         break;
      case MAP_TAXON_FLORA_MV_STYLE_THUMB:
         link = new Link();
         link.setRel("self");
         link.setHref(instanceConfig.getStaticMapsURL() + "/"
               + context.getPortal().getId() + "/"
               + source.getGroup().substring(0, 1) + "/thumbs/thumb_"
               + source.getName() + ".png");
         break;
      default:
         break;
      }
      return link;
   }

}
