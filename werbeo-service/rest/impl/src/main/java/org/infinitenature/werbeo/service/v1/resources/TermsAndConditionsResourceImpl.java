package org.infinitenature.werbeo.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.service.v1.resources.TermsAndConditionsResource;
import org.infinitenature.service.v1.types.response.TermsAndConditonsAcceptResponse;
import org.infinitenature.service.v1.types.response.TextResponse;
import org.infinitenature.werbeo.service.core.api.ports.AcceptanceQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.PrivacyQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.RolesCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.TermsAndConditionsQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class TermsAndConditionsResourceImpl
      implements TermsAndConditionsResource
{
   @Autowired
   private ContextFactory contextFactory;

   @Autowired
   private PrivacyQueryPort privacyQueryPort;

   @Autowired
   private AcceptanceQueryPort acceptanceQueryPort;


   @Autowired
   private TermsAndConditionsQueryPort termsAndConditionsQueryPort;
   @Autowired
   private RolesCommandPort termsAndConditionsCommandPort;

   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public TermsAndConditonsAcceptResponse accept(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new TermsAndConditonsAcceptResponse(
            termsAndConditionsCommandPort.accept(context));
   }

   @Override
   public TextResponse getConditions(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new TextResponse(
            acceptanceQueryPort.getAcceptanceDeclaration(context));
   }


   @Override
   public TextResponse getPrivacyDeclaration(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new TextResponse(privacyQueryPort.getPrivacyDeclaration(context));
   }

   @Override
   public TextResponse getToc(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new TextResponse(
            termsAndConditionsQueryPort.getTC(context));
   }
}


