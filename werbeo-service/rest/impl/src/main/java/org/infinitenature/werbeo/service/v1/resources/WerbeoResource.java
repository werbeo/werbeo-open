package org.infinitenature.werbeo.service.v1.resources;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

@Service
@RequestScope
@Retention(RetentionPolicy.RUNTIME)
public @interface WerbeoResource
{

}
