package org.infinitenature.werbeo.service.mapper;

import java.util.Set;

import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SampleBase;
import org.infinitenature.service.v1.types.SampleBase.SampleMethod;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigRest.class, uses = { VagueDateMapperRest.class,
      OccurrenceMapperRest.class, SurveyMapperRest.class,
      PersonMapperRest.class, PositionMapper.class })
public abstract class SampleMapperRest extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.SampleBase, SamplesResource>
{
   public SampleMapperRest()
   {
      super(SamplesResource.class);
   }

   @Mapping(target = "locality.locationComment", source = "locality.locationComment")
   @Mapping(target = "locality.blur", source = "locality.precision")
   public abstract Sample mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.Sample entity,
         @org.mapstruct.Context Context context);

   public MTB toMTB(String string)
   {
      MTB mtb = new MTB();
      mtb.setMtb(string);
      return mtb;
   }


   @Mapping(target = "locality.locationComment", source = "locality.locationComment")
   @Mapping(target = "locality.blur", source = "locality.precision")
   public abstract SampleBase mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.SampleBase entity,
         @org.mapstruct.Context Context context);

   public abstract SampleMethod mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod sampleMethod);

   public abstract Set<SampleMethod> mapSampleMethodToApi(
         Set<org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod> sampleMethods);

   protected abstract org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod sampleMethodToSampleMethod(
         SampleMethod sampleMethod);

   protected abstract PositionType positionTypeToPositionType(
         org.infinitenature.werbeo.service.core.api.enity.Position.PositionType positionType);

   protected abstract Availability availabilityToAvailability(
         org.infinitenature.werbeo.service.core.api.enity.Survey.Availability availability);

   @Mapping(source = "locality.blur", target = "locality.precision")
   public abstract org.infinitenature.werbeo.service.core.api.enity.Sample mapToCore(
         Sample sample, @org.mapstruct.Context Context context);

   @Mapping(source = "locality.blur", target = "locality.precision")
   public abstract org.infinitenature.werbeo.service.core.api.enity.SampleBase mapToCore(
         SampleBase sample, @org.mapstruct.Context Context context);
}
