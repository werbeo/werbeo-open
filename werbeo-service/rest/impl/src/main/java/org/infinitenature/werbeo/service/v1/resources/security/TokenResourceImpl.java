package org.infinitenature.werbeo.service.v1.resources.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.infinitenature.service.v1.resources.security.TokenResource;
import org.infinitenature.service.v1.types.response.TokenResponse;
import org.infinitenature.service.v1.types.support.Token;
import org.infinitenature.service.v1.types.support.UserAuth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class TokenResourceImpl implements TokenResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TokenResourceImpl.class);
   @Value("${keycloak.realm}")
   private String realm;
   @Value("${keycloak.resource}")
   private String resource;
   @Value("${keycloak.auth-server-url}")
   private String authServerUrl;
   @Value("${keycloak.credentials.secret}")
   private String secret;

   @Override
   public TokenResponse getToken(UserAuth userAuth)
   {
      LOGGER.info("Requesting auth token for user {}", userAuth.getEmail());
      try (CloseableHttpClient httpClient = HttpClients.createDefault())
      {
         HttpPost post = new HttpPost(createURI());
         List<NameValuePair> nvps = new ArrayList<>();
         nvps.add(new BasicNameValuePair("client_id", resource));
         nvps.add(new BasicNameValuePair("client_secret", secret));
         nvps.add(new BasicNameValuePair("username", userAuth.getEmail()));
         nvps.add(new BasicNameValuePair("password", userAuth.getPassword()));
         nvps.add(new BasicNameValuePair("grant_type", "password"));
         post.setEntity(new UrlEncodedFormEntity(nvps));

         try (CloseableHttpResponse response = httpClient.execute(post);)
         {
            ObjectMapper mapper = new ObjectMapper();
            Token token = mapper.readValue(
                  EntityUtils.toString(response.getEntity()), Token.class);
            LOGGER.info("Token: {}", token);
            return new TokenResponse(token);
         }
      } catch (IOException e)
      {
         LOGGER.error("Failure getting token from keycloak", e);
         throw new IllegalAccessError();
      }

   }

   private String createURI()
   {
      return authServerUrl + "/realms/" + realm
            + "/protocol/openid-connect/token";
   }
}
