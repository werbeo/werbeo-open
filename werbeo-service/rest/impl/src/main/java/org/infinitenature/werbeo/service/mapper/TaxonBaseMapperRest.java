package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.response.TaxonBaseSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class,
      TaxaListMapperRest.class })
public abstract class TaxonBaseMapperRest
      extends RestMapper<TaxonBase, TaxaResource>
{
   @Autowired
   private TaxonLinksMapper taxonLinksMapper;

   public TaxonBaseMapperRest()
   {
      super(TaxaResource.class);
   }

   public abstract org.infinitenature.service.v1.types.TaxonBase mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.TaxonBase entity,
         @org.mapstruct.Context Context context);

   public abstract List<org.infinitenature.service.v1.types.TaxonBase> mapToApi(
         List<org.infinitenature.werbeo.service.core.api.enity.TaxonBase> entity,
         @org.mapstruct.Context Context context);

   @Mapping(target = "allowDataEntry", ignore = true)
   @Mapping(target = "taxaList", ignore = true) // not needed when saving
                                                // occurrences, and taxa
                                                // manipulation is not available
                                                // by the api
   @Mapping(target = "documentsAvailable", ignore = true)
   public abstract org.infinitenature.werbeo.service.core.api.enity.TaxonBase toCoreApi(
         org.infinitenature.service.v1.types.TaxonBase taxon);

   public TaxonBaseSliceResponse mapToApi(
         Slice<org.infinitenature.werbeo.service.core.api.enity.TaxonBase, TaxonSortField> entitySlice,
         Context context)
   {
      return new TaxonBaseSliceResponse(mapToApi(entitySlice.getContent(), context),
            entitySlice.getSize());
   }

   @AfterMapping
   public void addDocuments(
         @MappingTarget org.infinitenature.service.v1.types.Taxon target,
         Taxon source, @org.mapstruct.Context Context context)
   {
      target.getDocuments()
            .addAll(taxonLinksMapper.createDocuments(source, context));
   }
}
