package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.PeopleFilter;
import org.infinitenature.service.v1.resources.PeopleResource;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.service.v1.types.response.PersonSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.ports.PersonFilter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class })
public abstract class PersonMapperRest extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.Person, PeopleResource>
{
   @Autowired
   private LinkMapperRest linkMapper;

   public PersonMapperRest()
   {
      super(PeopleResource.class);
   }

   public abstract Person mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.Person entity,
         @org.mapstruct.Context Context context);

   public abstract List<Person> mapToApi(
         List<org.infinitenature.werbeo.service.core.api.enity.Person> entities,
         @org.mapstruct.Context Context context);

   public PersonSliceResponse mapToApi(
         Slice<org.infinitenature.werbeo.service.core.api.enity.Person, PersonSortField> entitySlice,
         @org.mapstruct.Context Context context)
   {
      return new PersonSliceResponse(
            mapToApi(entitySlice.getContent(), context), entitySlice.getSize());
   }

   public abstract org.infinitenature.werbeo.service.core.api.enity.Person mapToCore(
         Person person, @org.mapstruct.Context Context context);

   public abstract PersonSortField mapToCore(PersonField sortField);

   public abstract PersonFilter mapToCore(PeopleFilter peopleFilter);
}
