package org.infinitenature.werbeo.service.v1.resources;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.IOUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.OccurrenceImportResource;
import org.infinitenature.service.v1.types.JobStatus;
import org.infinitenature.service.v1.types.OccurrenceImportField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceImportStatusResponse;
import org.infinitenature.service.v1.types.response.OccurrenceImportsInfoResponse;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceImportSortField;
import org.infinitenature.werbeo.service.core.api.infra.query.FileQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceCSVImportQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceImportCommands;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceImportQueries;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.JobStatusMapper;
import org.infinitenature.werbeo.service.mapper.OccurrenceImportMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class OccurrenceImportResourceImpl implements OccurrenceImportResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceImportResourceImpl.class);

   @Autowired
   private OccurrenceImportCommands importCommands;
   @Autowired
   private OccurrenceImportQueries importQueries;
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private JobStatusMapper jobStatusMapper;
   @Autowired
   private FileQueries fileQueries;
   @Autowired
   private OccurrenceImportMapperRest occurrenceImportMapper;
   @Autowired
   private OccurrenceCSVImportQueryPort occurrenceImportQueryPort;

   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public String doImportXml(int portalId, String request)
   {
      return "" + importCommands
            .importXML(request, contextFactory.createContext(portalId, httpHeaders)).getId();
   }

   @Override
   public JobStatus getJobStatus(int portalId, UUID id)
   {
      return jobStatusMapper.map(importQueries.getStatus(id,
            contextFactory.createContext(portalId, httpHeaders)));
   }

   @Override
   public String doImportCsv(int portalId, String csvContent)
   {
      return "" + importCommands
            .importCSV(csvContent, contextFactory.createContext(portalId, httpHeaders)).getId();
   }

   @Override
   public Response getCsvTemplate(int portalId)
   {
      try
      {
         ResponseBuilder response = Response
               .ok((IOUtils.toByteArray(fileQueries.loadCsvTemplate(portalId))));
         response.header("Content-Disposition",
               "attachment; filename=\"template.csv\"");
         return response.build();
      } catch (Exception e)
      {
         LOGGER.error("Error retrieving csv template", e);
         return null;
      }
   }
   
   @Override
   public Response getCsvTemplateHelp(int portalId)
   {
      try
      {
         ResponseBuilder response = Response
               .ok((IOUtils.toByteArray(fileQueries.loadCsvTemplateHelp(portalId))));
         response.header("Content-Disposition",
               "attachment; filename=\"templateHelp.txt\"");
         return response.build();
      } catch (Exception e)
      {
         LOGGER.error("Error retrieving csv template help", e);
         return null;
      }
   }

   @Override
   public Response getImport(UUID importId, int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);

      TypedFile export = occurrenceImportQueryPort.load(importId, context);

      return Response.ok(export.getData(),
            occurrenceImportMapper.map(export.getFileFormat())).build();
   }

   @Override
   public Response getErrorLog(UUID importId, int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);

      TypedFile export = occurrenceImportQueryPort.loadErrorLog(importId, context);

      return Response.ok(export.getData(),
            occurrenceImportMapper.map(export.getFileFormat())).build();
   }

   @Override
   public OccurrenceImportStatusResponse getStatus(UUID exportId, int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new OccurrenceImportStatusResponse(
            occurrenceImportMapper.map(
                  occurrenceImportQueryPort.get(exportId, context)));
   }


   @Override
   public DeletedResponse delete(UUID importId, int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      importCommands.delete(importId, context);
      return new DeletedResponse();
   }

   @Override
   public OccurrenceImportsInfoResponse findImports(@Min(1) int portalId,
         @Min(0) int offset, int limit, OccurrenceImportField sortField,
         SortOrder sortOrder)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      OffsetRequest<OccurrenceImportSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder,
            occurrenceImportMapper.mapToApi(sortField));
      return occurrenceImportMapper
            .mapCoreToApi(
                  occurrenceImportQueryPort.find(null, context, offsetRequest));
   }

   @Override
   public CountResponse count(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new CountResponse(
            occurrenceImportQueryPort.count(null, context),
            context.getRequestId());
   }


}
