package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.TaxaListResource;
import org.infinitenature.service.v1.types.TaxaList;
import org.infinitenature.service.v1.types.TaxaListField;
import org.infinitenature.service.v1.types.response.TaxaListSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.Mapper;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class })
public abstract class TaxaListMapperRest extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.TaxaList, TaxaListResource>
{
   public TaxaListMapperRest()
   {
      super(TaxaListResource.class);
   }

   public TaxaListSliceResponse mapToApi(
         Slice<org.infinitenature.werbeo.service.core.api.enity.TaxaList, TaxaListSortField> entitySlice,
         @org.mapstruct.Context Context context)
   {
      return new TaxaListSliceResponse(
            mapToApi(entitySlice.getContent(), context), entitySlice.getSize());
   }

   public abstract TaxaList mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.TaxaList entity,
         @org.mapstruct.Context Context context);

   abstract List<TaxaList> mapToApi(
         List<org.infinitenature.werbeo.service.core.api.enity.TaxaList> content,
         @org.mapstruct.Context Context context);

   public abstract TaxaListSortField mapToCore(TaxaListField sortField);
}
