package org.infinitenature.werbeo.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.ext.Provider;

import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.togglz.core.manager.FeatureManager;

@Provider
@PreMatching
@Priority(Integer.MIN_VALUE)
@Component
public class ServerErrorLoggingFilter
      implements ContainerResponseFilter
{
   class LoggingStream extends FilterOutputStream
   {

      private final StringBuilder b;
      private final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      private final int maxEntitySize;

      LoggingStream(final StringBuilder b, final OutputStream inner,
            final int maxEntitySize)
      {
         super(inner);

         this.b = b;
         this.maxEntitySize = maxEntitySize;
      }

      StringBuilder getStringBuilder(final Charset charset)
      {
         // write entity to the builder
         final byte[] entity = baos.toByteArray();

         b.append(new String(entity, 0, Math.min(entity.length, maxEntitySize),
               charset));
         if (entity.length > maxEntitySize)
         {
            b.append("...more...");
         }
         b.append('\n');

         return b;
      }

      @Override
      public void write(final int i) throws IOException
      {
         if (baos.size() <= maxEntitySize)
         {
            baos.write(i);
         }
         out.write(i);
      }
   }

   private static final int DEFAULT_MAX_ENTITY_SIZE = 8 * 1024;
   static final String ENTITY_LOGGER_PROPERTY = ServerErrorLoggingFilter.class
         .getName() + ".entityLogger";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ServerErrorLoggingFilter.class);
   private static final String LOGGING_ID_PROPERTY = ServerErrorLoggingFilter.class
         .getName() + ".id";
   private static final String NOTIFICATION_PREFIX = "* ";
   private static final String REQUEST_PREFIX = "> ";
   private static final String RESPONSE_PREFIX = "< ";
   /**
    * Get the character set from a media type.
    * <p>
    * The character set is obtained from the media type parameter "charset". If
    * the parameter is not present the {@link #UTF8} charset is utilized.
    *
    * @param m
    *           the media type.
    * @return the character set.
    */
   protected static Charset getCharset(MediaType m)
   {
      String name = (m == null) ? null
            : m.getParameters().get(MediaType.CHARSET_PARAMETER);
      return (name == null) ? StandardCharsets.UTF_8 : Charset.forName(name);
   }

   private final FeatureManager featureManager;

   private final AtomicLong idCounter = new AtomicLong(0);
   private final int maxEntitySize;

   private final boolean printEntity;

   @Autowired
   public ServerErrorLoggingFilter(FeatureManager featureManager)
   {

      this(DEFAULT_MAX_ENTITY_SIZE, true, featureManager);
   }



   public ServerErrorLoggingFilter(int maxEntitySize, boolean printEntity,
         FeatureManager featureManager)
   {
      this.maxEntitySize = maxEntitySize;
      this.printEntity = printEntity;
      this.featureManager = featureManager;
   }

   @Override
   public void filter(ContainerRequestContext requestContext,
         ContainerResponseContext responseContext) throws IOException
   {
      if (featureManager.isActive(WerBeoFeatures.LOG_REQUESTS_ON_ERROR)
            && responseContext.getStatusInfo().getFamily() != Family.SUCCESSFUL)
      {
         log(requestContext);
         log(requestContext, responseContext);
      }
   }

   private Set<Map.Entry<String, List<String>>> getSortedHeaders(
         final Set<Map.Entry<String, List<String>>> headers)
   {
      final TreeSet<Map.Entry<String, List<String>>> sortedHeaders = new TreeSet<>(
            (o1, o2) -> o1.getKey().compareToIgnoreCase(o2.getKey()));
      sortedHeaders.addAll(headers);
      return sortedHeaders;
   }

   private void log(ContainerRequestContext context) throws IOException
   {
      final long id = idCounter.incrementAndGet();
      context.setProperty(LOGGING_ID_PROPERTY, id);

      final StringBuilder b = new StringBuilder();

      printRequestLine(b, "Server has received a request", id,
            context.getMethod(), context.getUriInfo().getRequestUri());
      printPrefixedHeaders(b, id, REQUEST_PREFIX, context.getHeaders());

      if (printEntity && context.hasEntity())
      {
         context.setEntityStream(logInboundEntity(b, context.getEntityStream(),
               getCharset(context.getMediaType())));
      }

      LOGGER.error(b.toString());
   }

   private void log(ContainerRequestContext requestContext,
         ContainerResponseContext responseContext)
   {
      final Object requestId = requestContext.getProperty(LOGGING_ID_PROPERTY);
      final long id = requestId != null ? (Long) requestId
            : idCounter.incrementAndGet();

      final StringBuilder b = new StringBuilder();

      printResponseLine(b, "Server responded with a response", id,
            responseContext.getStatus());
      printPrefixedHeaders(b, id, RESPONSE_PREFIX,
            responseContext.getStringHeaders());

      if (printEntity && responseContext.hasEntity())
      {
         final OutputStream stream = new LoggingStream(b,
               responseContext.getEntityStream(), maxEntitySize);
         responseContext.setEntityStream(stream);
         requestContext.setProperty(ENTITY_LOGGER_PROPERTY, stream);
         // not calling log(b) here - it will be called by the interceptor
      } else
      {
         LOGGER.error(b.toString());
      }

   }

   protected InputStream logInboundEntity(final StringBuilder b,
         InputStream stream, final Charset charset) throws IOException
   {
      if (!stream.markSupported())
      {
         stream = new BufferedInputStream(stream);
      }
      stream.mark(maxEntitySize + 1);
      final byte[] entity = new byte[maxEntitySize + 1];
      final int entitySize = stream.read(entity);
      b.append(new String(entity, 0, Math.min(entitySize, maxEntitySize),
            charset));
      if (entitySize > maxEntitySize)
      {
         b.append("...more...");
      }
      b.append('\n');
      stream.reset();
      return stream;
   }

   private StringBuilder prefixId(final StringBuilder b, final long id)
   {
      b.append(Long.toString(id)).append(" ");
      return b;
   }

   protected void printPrefixedHeaders(final StringBuilder b, final long id,
         final String prefix, final MultivaluedMap<String, String> headers)
   {
      for (final Map.Entry<String, List<String>> headerEntry : getSortedHeaders(
            headers.entrySet()))
      {
         final List<?> val = headerEntry.getValue();
         final String header = headerEntry.getKey();

         if (val.size() == 1)
         {
            prefixId(b, id).append(prefix).append(header).append(": ")
                  .append(val.get(0)).append("\n");
         } else
         {
            final StringBuilder sb = new StringBuilder();
            boolean add = false;
            for (final Object s : val)
            {
               if (add)
               {
                  sb.append(',');
               }
               add = true;
               sb.append(s);
            }
            prefixId(b, id).append(prefix).append(header).append(": ")
                  .append(sb.toString()).append("\n");
         }
      }
   }

   protected void printRequestLine(final StringBuilder b, final String note,
         final long id, final String method, final URI uri)
   {
      prefixId(b, id).append(NOTIFICATION_PREFIX).append(note)
            .append(" on thread ").append(Thread.currentThread().getName())
            .append("\n");
      prefixId(b, id).append(REQUEST_PREFIX).append(method).append(" ")
            .append(uri.toASCIIString()).append("\n");
   }

   protected void printResponseLine(final StringBuilder b, final String note,
         final long id, final int status)
   {
      prefixId(b, id).append(NOTIFICATION_PREFIX).append(note)
            .append(" on thread ").append(Thread.currentThread().getName())
            .append("\n");
      prefixId(b, id).append(RESPONSE_PREFIX).append(Integer.toString(status))
            .append("\n");
   }
}
