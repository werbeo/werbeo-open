package org.infinitenature.werbeo.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.types.PortalField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.FilterResponse;
import org.infinitenature.service.v1.types.response.PortalBaseResponse;
import org.infinitenature.service.v1.types.response.PortalConfigResponse;
import org.infinitenature.service.v1.types.response.PortalSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.PortalFilter;
import org.infinitenature.werbeo.service.core.api.ports.PortalQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.PortalMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class PortalResourceImpl implements PortalResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PortalResourceImpl.class);
   @Autowired
   private PortalMapperRest mapper;
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;
   @Autowired
   private PortalQueryPort portalQueryPort;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public PortalSliceResponse findPortals(@Min(0) int offset, int limit,
         PortalField sortField, SortOrder sortOrder)
   {
      OffsetRequest<PortalSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, mapper.mapToCore(sortField));
      // Always fetch portal list anonymous
      Context context = contextFactory.createAnonymousContext(httpHeaders);
      Slice<Portal, PortalSortField> find = portalQueryPort
            .find(new PortalFilter(), context, offsetRequest);
      return mapper.mapToApiResponse(find, context);
   }

   @Override
   public PortalSliceResponse findAssociatedPortals(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);

      Slice<Portal, PortalSortField> find = portalQueryPort
            .findAssociated(context);
      return mapper.mapToApiResponse(find, context);
   }

   @Override
   public CountResponse count()
   {
      Context context = contextFactory.createAnonymousContext(httpHeaders);
      return new CountResponse(
            portalQueryPort.count(new PortalFilter(), context),
            context.getRequestId());
   }

   @Override
   public FilterResponse getFilter(int portalId)
   {
      LOGGER.info("Requesting filters");
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return mapper.mapToApi(occurrenceQueryPort.getFilter(context), context);
   }

   @Override
   public PortalBaseResponse getPortal(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return mapper.mapToApiResponse(portalQueryPort.load(portalId), context);
   }

   @Override
   public PortalConfigResponse getPortalConfiguration(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      PortalConfiguration portalConfig = portalQueryPort.loadConfig(portalId);
      return mapper.mapToApiResponse(portalConfig);
   }
}
