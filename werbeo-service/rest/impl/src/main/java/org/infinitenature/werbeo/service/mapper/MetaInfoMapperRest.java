package org.infinitenature.werbeo.service.mapper;

import java.util.Arrays;
import java.util.Set;

import org.infinitenature.service.v1.types.Value;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.service.v1.types.meta.TaxonFieldConfig;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence;
import org.infinitenature.werbeo.service.core.api.enity.Occurrence.BloomingSprouts;
import org.infinitenature.werbeo.service.core.api.enity.Survey;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.enity.meta.TaxonConfig;
import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class, uses = { OccurrenceMapperRest.class,
      SampleMapperRest.class })
public abstract class MetaInfoMapperRest
{

   @Autowired
   private OccurrenceMapperRest occurrenceMapperRest;
   @Autowired
   private SampleMapperRest sampleMapperRest;
   @Autowired
   private TaxonMapperRest taxonMapperRest;
   @Autowired
   private SurveyMapperRest surveyMapperRest;
   public abstract Set<OccurrenceFieldConfig> mapOccurrenceToApi(
         Set<org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceFieldConfig> configuredFields,
         @Context OccurrenceConfig occurrenceConfig);

   @Mapping(target = "property", ignore = true)
   @Mapping(target = "translationKey", ignore = true)
   @Mapping(target = "values", ignore = true)
   abstract OccurrenceFieldConfig map(
         org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceFieldConfig fieldConfig,
         @Context OccurrenceConfig config);

   @AfterMapping
   void addValues(@MappingTarget OccurrenceFieldConfig target,
         org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceFieldConfig source,
         @Context OccurrenceConfig config)
   {

      if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.AMOUNT)
      {
         Arrays.stream(Occurrence.Amount.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.AREA)
      {
         Arrays.stream(Occurrence.Area.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.BLOOMING_SPROUTS)
      {
         Arrays.stream(Occurrence.BloomingSprouts.values())
               .filter(value -> value != BloomingSprouts.LESS_ONE)
               .forEach(value -> target.getValues()
                     .add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.QUANTITY)
      {
         Arrays.stream(Occurrence.Quantity.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.REPRODUCTION)
      {
         Arrays.stream(Occurrence.Reproduction.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.SETTLEMENT_STATUS)
      {
         Arrays.stream(Occurrence.SettlementStatus.values())
               .forEach(value -> target.getValues()
                     .add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.SETTLEMENT_STATUS_FUKAREK)
      {
         Arrays.stream(Occurrence.SettlementStatusFukarek.values())
               .forEach(value -> target.getValues()
                     .add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.SEX)
      {
         Arrays.stream(Occurrence.Sex.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      } else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.VITALITY)
      {
         Arrays.stream(Occurrence.Vitality.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      }
      else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.NUMERIC_AMOUNT_ACCURACY)
      {
         Arrays.stream(Occurrence.NumericAmountAccuracy.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      }
      else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.MAKROPTER)
      {
         Arrays.stream(Occurrence.Makropter.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      }
      else if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.OccurrenceField.LIFE_STAGE)
      {
         Arrays.stream(Occurrence.LifeStage.values()).forEach(value -> target
               .getValues().add(new Value(occurrenceMapperRest.map(value))));
      }
   }

   public abstract Set<SampleFieldConfig> mapSampleToApi(
         Set<org.infinitenature.werbeo.service.core.api.enity.meta.SampleFieldConfig> configuredFields,
         @Context SampleConfig sampleConfig);

   @Mapping(target = "property", ignore = true)
   @Mapping(target = "translationKey", ignore = true)
   @Mapping(target = "values", ignore = true)
   abstract SampleFieldConfig map(
         org.infinitenature.werbeo.service.core.api.enity.meta.SampleFieldConfig fieldConfig,
         @Context SampleConfig config);

   @AfterMapping
   void addValues(@MappingTarget SampleFieldConfig target,
         org.infinitenature.werbeo.service.core.api.enity.meta.SampleFieldConfig source,
         @Context SampleConfig config)
   {
      if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.SampleField.SAMPLE_METHOD)
      {
         for (org.infinitenature.werbeo.service.core.api.enity.SampleBase.SampleMethod method : config
               .getAvailableMethods())
         {
            target.getValues()
                  .add(new Value(sampleMapperRest.mapToApi(method)));
         }
      }
   }

   public abstract Set<SurveyFieldConfig> mapSurveyToApi(
         Set<org.infinitenature.werbeo.service.core.api.enity.meta.SurveyFieldConfig> fieldConfigs,
         @Context SurveyConfig config);

   @Mapping(target = "property", ignore = true)
   @Mapping(target = "translationKey", ignore = true)
   @Mapping(target = "values", ignore = true)
   abstract SurveyFieldConfig map(
         org.infinitenature.werbeo.service.core.api.enity.meta.SurveyFieldConfig fieldConfig,
         @Context SurveyConfig config);

   @AfterMapping
   void addValues(@MappingTarget SurveyFieldConfig target,
         org.infinitenature.werbeo.service.core.api.enity.meta.SurveyFieldConfig source,
         @Context SurveyConfig config)
   {
      if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.SurveyField.AVAILABILITY)
      {
         Arrays.stream(Survey.Availability.values()).forEach(value -> target
               .getValues().add(new Value(surveyMapperRest.map(value))));
      }
   }

   public abstract Set<TaxonFieldConfig> mapTaxonToApi(
         Set<org.infinitenature.werbeo.service.core.api.enity.meta.TaxonFieldConfig> configuredFields,
         @Context TaxonConfig config);

   @Mapping(target = "property", ignore = true)
   @Mapping(target = "translationKey", ignore = true)
   @Mapping(target = "values", ignore = true)
   abstract TaxonFieldConfig map(
         org.infinitenature.werbeo.service.core.api.enity.meta.TaxonFieldConfig fieldConfig,
         @Context TaxonConfig config);

   @AfterMapping
   void addValues(@MappingTarget TaxonFieldConfig target,
         org.infinitenature.werbeo.service.core.api.enity.meta.TaxonFieldConfig source,
         @Context TaxonConfig config)
   {
      if (source
            .getField() == org.infinitenature.werbeo.service.core.api.enity.meta.fields.TaxonField.RED_LIST_STATUS)
      {
         Arrays.stream(Taxon.RedListStatus.values()).forEach(value -> target
               .getValues().add(new Value(taxonMapperRest.map(value))));
      }
   }

}
