package org.infinitenature.werbeo.service.v1.resources;

import java.util.Locale;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.service.v1.resources.TranslationResource;
import org.infinitenature.service.v1.types.response.TranslationResponse;
import org.infinitenature.werbeo.service.core.api.ports.TranslationQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class TranslationResourceImpl implements TranslationResource
{
   @Autowired
   private TranslationQueryPort translationQueries;
   @Autowired
   private ContextFactory contextFactory;

   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public String getTranslation(int portalId, String key, String languageTag)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      Locale locale = Locale.forLanguageTag(languageTag);
      return translationQueries.getTranslation(context, key, locale);
   }

   @Override
   public TranslationResponse getTranslations(@Min(1) int portalId,
         String languageTag)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      Locale locale = Locale.forLanguageTag(languageTag);
      return new TranslationResponse(
            translationQueries.getTranslations(context, locale));
   }
}
