package org.infinitenature.werbeo.service.v1.resources;

import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.UsersResource;
import org.infinitenature.service.v1.types.enums.Role;
import org.infinitenature.service.v1.types.response.AddRoleResponse;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.UserSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.UserSortField;
import org.infinitenature.werbeo.service.core.api.ports.UserCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.UserQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.UserMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class UsersResourceImpl implements UsersResource
{
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private UserQueryPort userQueryPort;
   @Autowired
   private UserCommandPort userCommandPort;
   @Autowired
   private UserMapperRest userMapper;

   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public UserSliceResponse find(int portalId, int offset, int limit,
         String email)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      Slice<User, UserSortField> slice = userQueryPort.find(email, offset,
            limit, context);
      return new UserSliceResponse(
            userMapper.mapToApi(slice.getContent(), context), slice.getSize());
   }

   @Override
   public CountResponse count(int portalId, String email)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new CountResponse(userQueryPort.count(email, context),
            context.getRequestId());
   }

   @Override
   public AddRoleResponse addRole(int portalId, String email, Role role)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      userCommandPort.addRole(context, email,
            userMapper.mapToCore(role, context));
      return new AddRoleResponse();
   }

   @Override
   public AddRoleResponse removeRole(int portalId, String email, Role role)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      userCommandPort.removeRole(context, email,
            userMapper.mapToCore(role, context));
      return new AddRoleResponse();
   }

}
