package org.infinitenature.werbeo.service.config;

import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CacheConfig
{
   @Bean(name = "nameAwareKeyGenerator")
   public NameAwareKeyGenerator nameAwareKeyGenerator()
   {
      return new NameAwareKeyGenerator();
   }

   @Bean
   public CacheManager chaceManager()
   {
      return new WerbeoCacheManager();
   }

}
