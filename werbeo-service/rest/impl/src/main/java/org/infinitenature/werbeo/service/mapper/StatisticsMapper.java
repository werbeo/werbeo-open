package org.infinitenature.werbeo.service.mapper;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.stream.Collectors;

import org.infinitenature.service.v1.types.DateEntry;
import org.infinitenature.service.v1.types.FrequencyDistribution;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigRest.class)
public interface StatisticsMapper
{
   public FrequencyDistribution toRestApi(
         org.infinitenature.werbeo.service.core.api.enity.FrequencyDistribution frequencyDistribution);

   default List<DateEntry> map(SortedMap<ZonedDateTime, Integer> value)
   {
      if (value == null)
      {
         return null;
      }

      return value.entrySet().stream().map(e -> map(e))
            .collect(Collectors.toList());

   }

   @Mapping(target = "startDate", source = "key")
   @Mapping(target = "count", source = "value")
   public DateEntry map(Map.Entry<ZonedDateTime, Integer> entry);

   public default String to(ZonedDateTime dateTime)
   {
      return LocalDate.of(dateTime.getYear(), dateTime.getMonth(),
            dateTime.getDayOfMonth()).format(DateTimeFormatter.ISO_DATE);
   }
}
