package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.Language;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class,
      TaxaListMapperRest.class })
public abstract class TaxonMapperRest
      extends RestMapper<Taxon, TaxaResource>
{
   public TaxonMapperRest()
   {
      super(TaxaResource.class);
   }

   @Autowired
   private TaxonLinksMapper taxonLinksMapper;

   @Mapping(target = "documents", ignore = true)
   public abstract org.infinitenature.service.v1.types.Taxon mapToApi(
         Taxon entity,
         @org.mapstruct.Context Context context);

   public abstract List<org.infinitenature.service.v1.types.Taxon> mapToApi(
         List<Taxon> entities,
         @org.mapstruct.Context Context context);


   public abstract org.infinitenature.service.v1.types.Taxon.RedListStatus map(
         org.infinitenature.werbeo.service.core.api.enity.Taxon.RedListStatus redListStatus);

   public abstract org.infinitenature.werbeo.service.core.api.enity.Taxon.RedListStatus redListStatusToRedListStatus(
         org.infinitenature.service.v1.types.Taxon.RedListStatus redListStatus);

   @AfterMapping
   public void addDocuments(
         @MappingTarget org.infinitenature.service.v1.types.Taxon target,
         Taxon source, @org.mapstruct.Context Context context)
   {
      target.getDocuments()
            .addAll(taxonLinksMapper.createDocuments(source, context));
   }

   abstract Language mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.Language language);

}
