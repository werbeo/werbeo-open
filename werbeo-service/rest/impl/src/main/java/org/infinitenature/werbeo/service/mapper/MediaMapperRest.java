package org.infinitenature.werbeo.service.mapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.mapstruct.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class)
public abstract class MediaMapperRest
{
   private static final String RELATED = "related";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(MediaMapperRest.class);
   @Autowired
   private MediaPathUtils mediaPathUtils;

   public List<Document> map(Medium medium)
   {
      List<Document> documents = new ArrayList<>();
      switch (medium.getType())
      {
      case IMAGE_LOCAL:
         documents.add(new Document(DocumentType.IMAGE,
               new Link(RELATED, mediaPathUtils.createLocalMediumUrl(
                     medium)),
               medium.getDescription()));
         documents.add(new Document(DocumentType.IMAGE_THUMB,
               new Link(RELATED, mediaPathUtils.createLocalMediumThumpnailUrl(
                     medium)),
               medium.getDescription()));
         break;
      case AUDIO_LOCAL:
         documents.add(new Document(DocumentType.AUDIO,
               new Link(RELATED, mediaPathUtils
                     .createLocalMediumUrl(medium)),
               medium.getDescription()));
         break;
      default:
         LOGGER.error("Can't map mediaType {} to documentType",
               medium.getType());
         break;
      }
      return documents;

   }

   public List<Document> map(Collection<Medium> media)
   {
      if (media == null)
      {
         return new ArrayList<>();
      }
      return media.stream().flatMap(medium -> map(medium).stream())
            .collect(Collectors.toList());
   }

   public void setMediaPathUtils(MediaPathUtils mediaPathUtils)
   {
      this.mediaPathUtils = mediaPathUtils;
   }

}
