package org.infinitenature.werbeo.service.mapper;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.PortalField;
import org.infinitenature.service.v1.types.enums.StaticMapStyle;
import org.infinitenature.service.v1.types.response.FilterResponse;
import org.infinitenature.service.v1.types.response.PortalBaseResponse;
import org.infinitenature.service.v1.types.response.PortalConfigResponse;
import org.infinitenature.service.v1.types.response.PortalSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalSortField;
import org.infinitenature.werbeo.service.core.api.roles.Filter;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ValueMapping;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class })
public abstract class PortalMapperRest extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.Portal, PortalResource>
{

   public PortalMapperRest()
   {
      super(PortalResource.class);
   }

   @Mapping(target = "name", source = "title")
   public abstract PortalBase mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.Portal entity,
         @org.mapstruct.Context Context context);

   public abstract Collection<PortalBase> mapToApi(
         Collection<org.infinitenature.werbeo.service.core.api.enity.Portal> entities,
         @org.mapstruct.Context Context context);

   protected abstract List<PortalBase> mapToApi(List<Portal> content,
         @org.mapstruct.Context Context context);

   public abstract Map<org.infinitenature.service.v1.types.enums.Filter, Collection<PortalBase>> mapFilterToApi(
         Map<Filter, Collection<Portal>> filter,
         @org.mapstruct.Context Context context);

   public abstract org.infinitenature.service.v1.types.enums.Filter mapToApi(
         Filter filter);

   public FilterResponse mapToApi(Map<Filter, Collection<Portal>> filter,
         @org.mapstruct.Context Context context)
   {
      return new FilterResponse(mapFilterToApi(filter, context));
   }

   public PortalBaseResponse mapToApiResponse(Portal portal,
         @org.mapstruct.Context Context context)
   {
      return new PortalBaseResponse(mapToApi(portal, context));
   }

   public PortalConfigResponse mapToApiResponse(
         org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration portalConfiguration)
   {
      return new PortalConfigResponse(mapToApi(portalConfiguration));
   }

   public abstract PortalConfiguration mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration portalConfiguration);

   public abstract StaticMapStyle mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.support.StaticMapStyle staticMapStyle);

   @ValueMapping(source = "NAME", target = "TITLE")
   public abstract PortalSortField mapToCore(PortalField sortField);

   public PortalSliceResponse mapToApiResponse(
         Slice<Portal, PortalSortField> entitySlice,
         @org.mapstruct.Context Context context)
   {
      return new PortalSliceResponse(
            mapToApi(entitySlice.getContent(), context), entitySlice.getSize());
   }

}
