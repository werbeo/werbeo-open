package org.infinitenature.werbeo.service.util;

import java.util.Base64;
import java.util.Collection;

import org.apache.commons.lang3.ArrayUtils;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.werbeo.common.commons.WerbeoStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DocumentLinkInliner
{

   @Autowired
   public DocumentLinkInliner(RestTemplate restTemplate)
   {
      super();
      this.restTemplate = restTemplate;
   }

   private final RestTemplate restTemplate;

   public void inlineLinks(Collection<Document> documents,
         DocumentType... documentTypes)
   {
      for (Document document : documents)
      {
         if (ArrayUtils.contains(documentTypes, document.getType()))
         {
            inlineLink(document);
         }
      }
   }

   public Document inlineLink(Document document)
   {
      
      String href = WerbeoStringUtils
            .encodeFileAndParams(document.getLink().getHref());
      String dataUrl = createDataURL(href);
      document.getLink().setHref(dataUrl);
      return document;
   }

   protected String createDataURL(String href)
   {
      HttpEntity<String> entity = new HttpEntity<>(new HttpHeaders());

      ResponseEntity<byte[]> response = restTemplate.exchange(href,
            HttpMethod.GET, entity, byte[].class);

      MediaType mediaType = response.getHeaders().getContentType();
      byte[] content = response.getBody();

      String base64Content = new String(Base64.getEncoder().encode(content));

      return "data:" + mediaType + ";base64," + base64Content;
   }
}
