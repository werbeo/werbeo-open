package org.infinitenature.werbeo.service.mapper.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.infinitenature.service.v1.types.response.FailureResponse;
import org.infinitenature.werbeo.service.core.api.error.EntityNotFoundException;
import org.infinitenature.werbeo.service.core.api.error.NotEnoughtRightsException;
import org.infinitenature.werbeo.service.core.api.error.ValidationException;
import org.infinitenature.werbeo.service.core.api.error.WerbeoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class WerbeoExceptionMapper implements ExceptionMapper<WerbeoException>
{

   private static final Logger log = LoggerFactory
         .getLogger(WerbeoExceptionMapper.class);

   @Override
   public Response toResponse(WerbeoException exception)
   {
      log.error("An exception occurred", exception);

      Response response;
      if (exception instanceof NotEnoughtRightsException)
      {
         response = Response.status(Response.Status.FORBIDDEN)
               .entity(new FailureResponse(exception.getMessage())).build();

      } else if (exception instanceof ValidationException)
      {
         response = Response.status(Response.Status.BAD_REQUEST)
               .entity(new FailureResponse(exception.getMessage())).build();
      } else if (exception instanceof EntityNotFoundException)
      {
         response = Response.status(Response.Status.NOT_FOUND)
               .entity(new FailureResponse(exception.getMessage())).build();
      } else
      {
         response = Response.status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity(new FailureResponse(exception.getMessage())).build();
      }
      return response;
   }
}
