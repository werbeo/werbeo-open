package org.infinitenature.werbeo.service.util;

import java.text.ParseException;

import org.apache.commons.lang3.StringUtils;
import org.geotools.geometry.GeometryBuilder;
import org.geotools.geometry.iso.text.WKTParser;
import org.geotools.referencing.CRS;
import org.infinitenature.werbeo.service.common.position.PositionFactory;
import org.infinitenature.werbeo.service.common.position.impl.PositionFactoryImpl;
import org.opengis.geometry.Geometry;
import org.opengis.geometry.primitive.Point;
import org.vergien.bde.model.PositionType;

public class BDEMapper
{
   private static final PositionFactory positionFactory = new PositionFactoryImpl();

   public static org.infinitenature.werbeo.service.core.api.enity.Position map(PositionType position)
   {
      assert position != null : "Postion parameter is not allowed to be null!";
      if (StringUtils.equalsIgnoreCase("DE-MTB", position.getSrefSystem()))
      {
         return positionFactory.createFromMTB(position.getMtb());
      } else
      {
         int epsg = getEPSGCode(position.getSrefSystem());
         WKTParser wktParser = getWKTParser(epsg);
         try
         {
            Geometry geometry = wktParser.parse(position.getWkt());
            if (geometry instanceof Point)
            {
               double[] coordinates = ((Point) geometry).getDirectPosition()
                     .getCoordinate();
               return positionFactory
                     .create(coordinates[0], coordinates[1], epsg);
            } else
            {
               throw new RuntimeException(
                     "Can't build geometry, because only POINT and MTB are supported, for positionType: "
                           + position);
            }
         } catch (ParseException e)
         {
            throw new RuntimeException(
                  "Can't build geomtry for postionType: " + position, e);
         }
      }
   }

   protected static WKTParser getWKTParser(int epsg)
   {
      try
      {
         GeometryBuilder geometryBuilder = new GeometryBuilder(
               CRS.decode("EPSG:900913"));
         return new WKTParser(geometryBuilder);
      } catch (Exception e)
      {
         throw new IllegalArgumentException(
               "Can't build WKTParser for epsg: " + epsg, e);
      }
   }

   protected static int getEPSGCode(String srefSystem)
   {
      if (StringUtils.startsWithIgnoreCase(srefSystem, "EPSG-"))
      {
         try
         {
            return Integer.valueOf(srefSystem.substring(5));
         } catch (Exception e)
         {
            throw new IllegalArgumentException(
                  "Unknown epsg notation: " + srefSystem);
         }

      }
      throw new IllegalArgumentException(
            "Unknown epsg notation: " + srefSystem);
   }
}
