package org.infinitenature.werbeo.service.v1.resources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.HerbarySheet;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.service.v1.types.enums.HerbarySheetPosition;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.OccurrenceCentroidsSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceFieldConfigResponse;
import org.infinitenature.service.v1.types.response.OccurrenceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceStreamResponse;
import org.infinitenature.werbeo.service.common.logging.Logger;
import org.infinitenature.werbeo.service.common.logging.LoggerFactory;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceCentroid;
import org.infinitenature.werbeo.service.core.api.enity.meta.OccurrenceConfig;
import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceCentroidSortField;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.ports.DocumentPort;
import org.infinitenature.werbeo.service.core.api.ports.MetaInfoQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceCommandsPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceFilter;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.HerbarySheetMapperRest;
import org.infinitenature.werbeo.service.mapper.MetaInfoMapperRest;
import org.infinitenature.werbeo.service.mapper.OccurrenceMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.infinitenature.werbeo.service.util.ContinuationTokenFactory;
import org.infinitenature.werbeo.service.util.DocumentLinkInliner;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class OccurrenceResourceImpl implements OccurrenceResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceResourceImpl.class);
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;
   @Autowired
   private OccurrenceCommandsPort occurrenceCommandPort;
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private ContinuationTokenFactory continuationTokenFactory;
   @Autowired
   private OccurrenceMapperRest occurrenceMapper;
   @Autowired
   private DocumentPort documentPort;
   @Autowired
   private HerbarySheetMapperRest herbarySheetMapper;
   @Autowired
   private MetaInfoMapperRest metaInfoMapper;
   @Autowired
   private MetaInfoQueryPort metaInfoCommandPort;
   @Autowired
   private DocumentLinkInliner documentLinkInliner;

   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public OccurrenceSliceResponse find(int offset, int limit,
         OccurrenceField sortField, SortOrder sortOrder,
         org.infinitenature.service.v1.resources.OccurrenceFilter filter, boolean inlineThumbs)
   {

      Context context = contextFactory.createContext(filter.getPortalId(),
            httpHeaders);
      LOGGER.info(context,
            "find , offset {}, limit {}, sortField {}, sortOrder {}, filter {}",
            offset, limit, sortField, sortOrder, filter);
      OffsetRequest<OccurrenceSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, occurrenceMapper.mapToApi(sortField));

      OccurrenceSliceResponse response = occurrenceMapper.mapToApi(
            occurrenceQueryPort.find(occurrenceMapper.mapCoreToApi(filter),
                  context, offsetRequest),
            context);
      if (inlineThumbs)
      {
         response.getContent().stream().map(Occurrence::getDocuments)
               .forEach(documents -> documentLinkInliner.inlineLinks(documents,
                     DocumentType.IMAGE_THUMB));
      }
      return response;
   }
   
   @Override
   public OccurrenceSliceResponse findPOST(int offset, int limit,
         OccurrenceField sortField, SortOrder sortOrder,
         org.infinitenature.service.v1.resources.OccurrenceFilterPOST filter, boolean inlineThumbs)
   {
      return find(offset, limit, sortField, sortOrder, filter, inlineThumbs);
   }

   @Override
   public OccurrenceStreamResponse stream(int portalId, String token)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      ContinuationToken continuationToken = continuationTokenFactory
            .parseToken(StringUtils.isBlank(token) ? "0_0" : token);
      return occurrenceMapper.mapToApi(
            occurrenceQueryPort.find(context, continuationToken), context);
   }

   @Override
   public OccurrenceResponse get(int portalId, UUID uuid)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      org.infinitenature.werbeo.service.core.api.enity.Occurrence occurrence = occurrenceQueryPort
            .get(uuid, context);
      if (occurrence == null)
      {
         throw new NotFoundException();
      } else
      {
         return new OccurrenceResponse(
               occurrenceMapper.mapToApi(occurrence, context));
      }
   }

   @Override
   public OccurrenceResponse validate(@Min(1) int portalId, UUID uuid,
         Validation validation)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      occurrenceCommandPort.setValidationStatus(uuid,
            occurrenceMapper.mapToCore(validation), context);

      return get(portalId, uuid);
   }

   @Override
   public CountResponse count(
         org.infinitenature.service.v1.resources.OccurrenceFilter filter)
   {

      Context context = contextFactory.createContext(filter.getPortalId(), httpHeaders);
      LOGGER.info(context, "count filter {}", filter);
      long count = occurrenceQueryPort.count(occurrenceMapper.mapCoreToApi(filter),
            context);
      LOGGER.info(context, "Count result {}", count);
      return new CountResponse(
            count, context.getRequestId());
   }
   
   @Override
   public CountResponse countPOST(
         org.infinitenature.service.v1.resources.OccurrenceFilterPOST filter)
   {
      return count(filter);
   }

   @Override
   public Response createHerbaryPage(int portalId, UUID uuid,
         HerbarySheetPosition position, HerbarySheet sheet) throws IOException
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      LOGGER.info("createHerbaryPage protalId {}, UUID {}, Position {}",
            portalId, uuid, position);
      ByteArrayOutputStream stream = documentPort.createHerbaryPage(context,
            uuid, herbarySheetMapper.mapToApi(position),
            herbarySheetMapper.mapToApi(sheet));
      if (stream == null)
      {
         return Response.status(404).build();
      }
      return Response.ok(stream.toByteArray()).build();
   }

   @Override
   public OccurrenceCentroidsSliceResponse findOccurrenceCentroids(int portalId,
         int offset, int limit, int taxonId)
   {
      OffsetRequest<OccurrenceCentroidSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, SortOrder.ASC, OccurrenceCentroidSortField.DATE);

      Context context = contextFactory.createContext(portalId, httpHeaders);
      Slice<OccurrenceCentroid, OccurrenceCentroidSortField> slice = occurrenceQueryPort
            .findOccurrenceCentroids(
                  OccurrenceFilter.taxonInclChildren(taxonId), context,
                  offsetRequest);

      return occurrenceMapper.mapOccurrenceCentroidSliceToApi(slice, context);
   }

   @Override
   public CountResponse countOccurenceCentroids(int portalId, int taxonId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new CountResponse(occurrenceQueryPort.countOccurrenceCentroids(
            OccurrenceFilter.taxonInclChildren(taxonId),
            context), context.getRequestId());
   }

   @Override
   public OccurrenceFieldConfigResponse getFieldConfig(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      OccurrenceConfig occurrenceConfig = metaInfoCommandPort
            .getOccurrenceConfig(context);
      Set<OccurrenceFieldConfig> occurrenceFieldConfigs = metaInfoMapper
            .mapOccurrenceToApi(occurrenceConfig.getConfiguredFields(),
                  occurrenceConfig);
      // new
      // EnumValueTranslator().addValuesToOccurrenceFields(occurrenceFieldConfigs);
      return new OccurrenceFieldConfigResponse(occurrenceFieldConfigs);
   }

   @Override
   public OccurrenceFieldConfigResponse getFieldConfig(@Min(1) int portalId,
         @Min(1) int surveyId)
   {
      return getFieldConfig(portalId);
   }
}
