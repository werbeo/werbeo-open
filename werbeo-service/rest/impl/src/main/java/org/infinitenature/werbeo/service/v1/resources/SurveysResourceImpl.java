package org.infinitenature.werbeo.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.BeanParam;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.SurveySortField;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.service.v1.types.response.SurveyBaseResponse;
import org.infinitenature.service.v1.types.response.SurveyFieldConfigResponse;
import org.infinitenature.service.v1.types.response.SurveysSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.meta.SurveyConfig;
import org.infinitenature.werbeo.service.core.api.ports.MetaInfoQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.SurveyQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.MetaInfoMapperRest;
import org.infinitenature.werbeo.service.mapper.SurveyMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class SurveysResourceImpl implements SurveysResource
{
   @Autowired
   private SurveyQueryPort surveysQueriesPort;
   @Autowired
   private SurveyCommandPort surveyCommandPort;
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private SurveyMapperRest surveyMapper;
   @Autowired
   private MetaInfoMapperRest metaInfoMapper;
   @Autowired
   private MetaInfoQueryPort metaInfoCommandPort;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public SurveysSliceResponse find(int offset, int limit,
         SurveySortField sortField, SortOrder sortOrder, SurveyFilter filter)
   {
      OffsetRequest<org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, surveyMapper.mapToCore(sortField));

      Context context = contextFactory.createContext(filter.getPortalId(),
            httpHeaders);
      return surveyMapper.mapToApi(
            surveysQueriesPort.find(filter.getName(), filter.getNameContains(),
                  filter.getParentSurveyId(), context, offsetRequest),
            context);

   }

   @Override
   public SurveyBaseResponse getSurvey(int portalId, int id)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new SurveyBaseResponse(surveyMapper
            .mapToApi(surveysQueriesPort.get(id, context), context));
   }

   @Override
   public CountResponse count(@BeanParam SurveyFilter filter)
   {
      Context context = contextFactory.createContext(filter.getPortalId(),
            httpHeaders);
      return new CountResponse(surveysQueriesPort.count(filter.getName(),
            filter.getNameContains(), filter.getParentSurveyId(), context),
            context.getRequestId());

   }

   @Override
   public SaveOrUpdateResponse save(int portalId, SurveyBase survey)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      int id = surveyCommandPort
            .saveOrUpdate(surveyMapper.mapToCore(survey, context), context);
      Link link = surveyMapper.createIdLink(context, id);
      return new SaveOrUpdateResponse(link, id);
   }

   @Override
   public SurveyFieldConfigResponse getFieldConfig(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      SurveyConfig surveyConfig = metaInfoCommandPort.getSurveyConfig(context);
      return new SurveyFieldConfigResponse(metaInfoMapper
            .mapSurveyToApi(surveyConfig.getConfiguredFields(), surveyConfig));
   }

}
