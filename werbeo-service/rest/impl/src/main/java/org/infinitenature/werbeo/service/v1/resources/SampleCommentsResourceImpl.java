package org.infinitenature.werbeo.service.v1.resources;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.SampleCommentsResource;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.SampleCommentsSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.ports.SampleCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.SampleQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.SampleCommentMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class SampleCommentsResourceImpl implements SampleCommentsResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SampleCommentsResourceImpl.class);
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private SampleCommentMapperRest commentMapper;
   @Autowired
   private SampleQueryPort sampleQueryPort;
   @Autowired
   private SampleCommandPort sampleCommandsPort;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;
   @Override
   public SampleCommentsSliceResponse getComments(@Min(1) int portalId,
         UUID uuid, @Min(0) int offset, @Min(1) int limit,
         CommentField sortField, SortOrder sortOrder)
   {
      LOGGER.info("Request comments for sample {} for portal {}", uuid,
            portalId);
      Context context = contextFactory.createContext(portalId, httpHeaders);
      OffsetRequest<CommentSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, commentMapper.mapToApi(sortField));

      return commentMapper.mapToApi(
            sampleQueryPort.getComments(uuid, context, offsetRequest), context);
   }

   @Override
   public CountResponse countComments(@Min(1) int portalId, UUID sampleId)
   {
      LOGGER.info("Request comments for sample {} for portal {}", sampleId,
            portalId);
      Context context = contextFactory.createContext(portalId, httpHeaders);

      return new CountResponse(
            sampleQueryPort.countComments(sampleId, context),
            context.getRequestId());
   }

   @Override
   public SaveOrUpdateResponse saveComment(@Min(1) int portalId, UUID uuid,
         SampleComment sampleComment)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      int id = sampleCommandsPort.addComment(uuid,
            commentMapper.mapToCore(sampleComment, context), context);
      Link link = commentMapper.createSampleCommentIdLink(context, uuid, id);
      return new SaveOrUpdateResponse(link, id);
   }
}
