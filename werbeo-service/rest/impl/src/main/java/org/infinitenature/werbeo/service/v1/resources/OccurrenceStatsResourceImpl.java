package org.infinitenature.werbeo.service.v1.resources;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.service.v1.resources.OccurrenceStatsResource;
import org.infinitenature.service.v1.types.response.FrequencyDistributionResponse;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceStatsQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.StatisticsMapper;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class OccurrenceStatsResourceImpl implements OccurrenceStatsResource
{
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private OccurrenceStatsQueryPort occurrenceStatsQueryPort;
   @Autowired
   private StatisticsMapper mapper;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;
   @Override
   public FrequencyDistributionResponse getOccurrencesCreatedPerDay(
         @Min(1) int portalId, @Min(1) int days)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new FrequencyDistributionResponse(
            mapper.toRestApi(occurrenceStatsQueryPort
                  .getOccurrencesCreatedPerDay(context, days)));
   }

   @Override
   public FrequencyDistributionResponse getOccurrencesCreatedPerWeek(
         @Min(1) int portalId, @Min(1) int weeks)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new FrequencyDistributionResponse(
            mapper.toRestApi(occurrenceStatsQueryPort
                  .getOccurrencesCreatedPerWeek(context, weeks)));
   }

   @Override
   public FrequencyDistributionResponse getOccurrencesCreatedPerMonth(
         int portalId, int months)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new FrequencyDistributionResponse(
            mapper.toRestApi(occurrenceStatsQueryPort
                  .getOccurrencesCreatedPerMonth(context, months)));
   }

}
