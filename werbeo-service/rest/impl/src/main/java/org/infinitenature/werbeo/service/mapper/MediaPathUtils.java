package org.infinitenature.werbeo.service.mapper;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MediaPathUtils
{
   @Autowired
   private InstanceConfig instanceConfig;

   public String createLocalMediumThumpnailUrl(Medium medium)
   {
      return createLocalMediumThumpnailUrl(medium.getPath());
   }

   public String createLocalMediumThumpnailUrl(String path)
   {
      return instanceConfig.getLocalMediaURL() + "/" + "thumb-" + path;
   }

   public String createLocalMediumUrl(Medium medium)
   {
      return createLocalMediumUrl(medium.getPath());
   }

   public String createLocalMediumUrl(String path)
   {
      return instanceConfig.getLocalMediaURL() + "/" + path;
   }

   public String clearPath(String mediaURL)
   {

      String removeThumbnailPrefix = StringUtils.removeStart(mediaURL,
            instanceConfig.getLocalMediaURL() + "/thumb-");
      return StringUtils.removeStart(removeThumbnailPrefix,
            instanceConfig.getLocalMediaURL() + "/");
   }

   void setInstanceConfig(InstanceConfig instanceConfig)
   {
      this.instanceConfig = instanceConfig;
   }
}