package org.infinitenature.werbeo.service.config;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.filter.KeycloakAuthenticationProcessingFilter;
import org.keycloak.adapters.springsecurity.filter.KeycloakPreAuthActionsFilter;
import org.keycloak.adapters.springsecurity.filter.KeycloakSecurityContextRequestFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, proxyTargetClass = true)
public class SecurityConfiguration extends KeycloakWebSecurityConfigurerAdapter
{
   @Override
   protected void configure(AuthenticationManagerBuilder auth) throws Exception
   {
      auth.authenticationProvider(keycloakAuthenticationProvider());
   }

   @Override
   protected void configure(HttpSecurity http) throws Exception
   {
      http.httpBasic().disable();
      http.formLogin().disable();
      http.csrf().disable();
      http.authorizeRequests().anyRequest().permitAll();

      http.addFilterBefore(keycloakPreAuthActionsFilter(), LogoutFilter.class)
            .addFilterBefore(keycloakAuthenticationProcessingFilter(),
                  BasicAuthenticationFilter.class);
      http.exceptionHandling()
            .authenticationEntryPoint(authenticationEntryPoint());
      http.sessionManagement()
            .sessionAuthenticationStrategy(sessionAuthenticationStrategy());
   }

   @Bean
   public KeycloakConfigResolver keycloakConfigResolver()
   {
      return new KeycloakSpringBootConfigResolver();
   }

   @Bean
   @Override
   public AuthenticationManager authenticationManagerBean() throws Exception
   {
      return super.authenticationManagerBean();
   }

   @Bean
   @Override
   protected SessionAuthenticationStrategy sessionAuthenticationStrategy()
   {
      return new RegisterSessionAuthenticationStrategy(
            new SessionRegistryImpl());
   }

   @Bean
   public FilterRegistrationBean<KeycloakAuthenticationProcessingFilter> keycloakAuthenticationProcessingFilterRegistrationBean(
         KeycloakAuthenticationProcessingFilter filter)
   {
      FilterRegistrationBean<KeycloakAuthenticationProcessingFilter> registrationBean = new FilterRegistrationBean<>(
            filter);
      registrationBean.setEnabled(false);
      return registrationBean;
   }

   @Bean
   public FilterRegistrationBean<KeycloakPreAuthActionsFilter> keycloakPreAuthActionsFilterRegistrationBean(
         KeycloakPreAuthActionsFilter filter)
   {
      FilterRegistrationBean<KeycloakPreAuthActionsFilter> registrationBean = new FilterRegistrationBean<>(
            filter);
      registrationBean.setEnabled(false);
      return registrationBean;
   }

   @Bean
   public FilterRegistrationBean<KeycloakSecurityContextRequestFilter> keycloakSecurityContextRequestFilterBean(
         KeycloakSecurityContextRequestFilter filter)
   {
      FilterRegistrationBean<KeycloakSecurityContextRequestFilter> registrationBean = new FilterRegistrationBean<>(
            filter);
      registrationBean.setEnabled(false);
      return registrationBean;
   }
}
