package org.infinitenature.werbeo.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.togglz.core.manager.EnumBasedFeatureProvider;
import org.togglz.core.spi.FeatureProvider;
import org.togglz.core.user.FeatureUser;
import org.togglz.core.user.SimpleFeatureUser;
import org.togglz.core.user.UserProvider;

@Configuration
public class FeaturesConfig
{
   @Bean
   public FeatureProvider featureProvider()
   {
      return new EnumBasedFeatureProvider()
            .addFeatureEnum(WerBeoFeatures.class);
   }

   @Bean
   public UserProvider userProvider()
   {
      return new UserProvider()
      {

         @Override
         public FeatureUser getCurrentUser()
         {
            String userName = SecurityContextHolder.getContext()
                  .getAuthentication().getName();
            if ("xxx@xxx.net".equals(userName))
            {
               return new SimpleFeatureUser(userName, true);
            } else
            {
               return null;
            }
         }
      };
   }
}
