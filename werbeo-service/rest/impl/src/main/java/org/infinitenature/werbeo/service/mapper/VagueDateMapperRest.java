package org.infinitenature.werbeo.service.mapper;

import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.vaguedate.VagueDateFactory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigRest.class)
public interface VagueDateMapperRest
{
   @Mapping(target = "from", source = "startDate")
   @Mapping(target = "to", source = "endDate")
   VagueDate toRestApi(org.infinitenature.vaguedate.VagueDate apiDate);

   default org.infinitenature.vaguedate.VagueDate toCoreApi(VagueDate restDate)
   {
      switch (restDate.getType())
      {
      case DAY:
      case MONTH_IN_YEAR:
      case YEAR:
         return VagueDateFactory.create(restDate.getFrom(),
               toCoreApi(restDate.getType()));

      default:
         return VagueDateFactory.create(restDate.getFrom(), restDate.getTo(),
               toCoreApi(restDate.getType()).getStringRepresentation());
      }

   }

   VagueDate.VagueDateType toRestApi(
         org.infinitenature.vaguedate.VagueDate.Type restType);

   org.infinitenature.vaguedate.VagueDate.Type toCoreApi(
         VagueDate.VagueDateType coreType);
}
