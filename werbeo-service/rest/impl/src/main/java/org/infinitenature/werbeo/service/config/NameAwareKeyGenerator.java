package org.infinitenature.werbeo.service.config;

import java.lang.reflect.Method;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;

public class NameAwareKeyGenerator implements KeyGenerator
{
   @Override
   public Object generate(Object target, Method method, Object... params)
   {
      return SimpleKeyGenerator.generateKey(target.getClass().getSimpleName(),
            method.getName(), params);
   }
}
