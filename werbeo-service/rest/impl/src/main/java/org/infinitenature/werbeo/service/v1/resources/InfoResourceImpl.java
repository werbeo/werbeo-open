package org.infinitenature.werbeo.service.v1.resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.service.v1.resources.InfoResource;
import org.infinitenature.service.v1.types.ServiceInfo;
import org.infinitenature.service.v1.types.UserInfo;
import org.infinitenature.service.v1.types.response.ServiceInfoResponse;
import org.infinitenature.service.v1.types.response.UserInfoResponse;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

@WerbeoResource
public class InfoResourceImpl implements InfoResource
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(InfoResourceImpl.class);
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   private final ContextFactory contextFactory;
   private final ServiceInfo serviceInfo;

   @Autowired
   public InfoResourceImpl(ContextFactory contextFactory)
   {
      this.contextFactory = contextFactory;
      this.serviceInfo = loadServiceInfo();

   }

   private ServiceInfo loadServiceInfo()
   {
      ServiceInfo info = new ServiceInfo();
      String name = "version.properties";
      try (InputStream input = InfoResourceImpl.class.getClassLoader()
            .getResourceAsStream(name))
      {

         Properties prop = new Properties();

         if (input == null)
         {
            LOGGER.error("Can't find {} to load service version info", name);
         }

         prop.load(input);

         info.setBuildDate(prop.getProperty("build.date"));
         info.setVersion(prop.getProperty("version"));

      } catch (IOException ex)
      {
         LOGGER.error("Failure reading service version info from {}", name, ex);
      }
      return info;
   }

   @Override
   public UserInfoResponse getServiceUser()
   {
      String userName = SecurityContextHolder.getContext().getAuthentication()
            .getName();
      UserInfo userInfo = new UserInfo();
      if ("anonymousUser".equals(userName))
      {
         userInfo.setAnonymous(true);
      } else
      {
         userInfo.setAnonymous(false);
         userInfo.setEmail(userName);
      }
      return new UserInfoResponse(userInfo);
   }

   @Override
   public ServiceInfoResponse getServiceInfo()
   {
      Context context = contextFactory.createAnonymousContext(httpHeaders);
      ServiceInfoResponse serviceInfoResponse = new ServiceInfoResponse(
            serviceInfo);
      serviceInfoResponse.setSourceRequestId(context.getRequestId());
      return serviceInfoResponse;
   }
}
