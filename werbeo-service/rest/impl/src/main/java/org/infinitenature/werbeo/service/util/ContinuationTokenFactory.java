package org.infinitenature.werbeo.service.util;

import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.springframework.stereotype.Service;

@Service
public class ContinuationTokenFactory
{
   public ContinuationToken parseToken(String token)
   {

      return new ContinuationToken(token);
   }

   public String write(ContinuationToken token)
   {
      if (token == null)
      {
         throw new IllegalArgumentException("The token may not be null!");
      }
      return token.serialize();
   }
}
