package org.infinitenature.werbeo.service.v1.resources;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.TaxaListResource;
import org.infinitenature.service.v1.types.TaxaListField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.TaxaListResponse;
import org.infinitenature.service.v1.types.response.TaxaListSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxaListSortField;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxaListQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.TaxaListMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class TaxaListResourceImpl implements TaxaListResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(TaxaListResourceImpl.class);
   @Autowired
   private TaxaListQueryPort taxaListQueryPort;
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private TaxaListMapperRest taxaListMapper;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public TaxaListSliceResponse find(int portalId, int offset, int limit,
         TaxaListField sortField, SortOrder sortOrder)
   {
      OffsetRequest<TaxaListSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, taxaListMapper.mapToCore(sortField));

      Context context = contextFactory.createContext(portalId, httpHeaders);
      return taxaListMapper.mapToApi(taxaListQueryPort
            .find(TaxaListFilter.ALL, context, offsetRequest), context);

   }

   @Override
   public CountResponse count(int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new CountResponse(
            taxaListQueryPort.count(TaxaListFilter.ALL, context),
            context.getRequestId());
   }

   @Override
   public TaxaListResponse getTaxaList(int portalId, int id)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      org.infinitenature.werbeo.service.core.api.enity.TaxaList taxaList = taxaListQueryPort
            .get(id, context);
      if (taxaList == null)
      {
         throw new NotFoundException();
      }
      return new TaxaListResponse(taxaListMapper.mapToApi(taxaList, context));
   }

}
