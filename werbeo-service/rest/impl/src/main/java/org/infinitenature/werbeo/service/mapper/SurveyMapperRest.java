package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.service.v1.types.ObfuscationPolicy;
import org.infinitenature.service.v1.types.Portal;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.response.SurveysSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.support.SurveySortField;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class })
public abstract class SurveyMapperRest extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.Survey, SurveysResource>
{
   public SurveyMapperRest()
   {
      super(SurveysResource.class);
   }

   @Mapping(target = "portal", ignore = true) // done in afterMapping
   public abstract SurveyBase mapToApi(
         org.infinitenature.werbeo.service.core.api.enity.Survey entity,
         @org.mapstruct.Context Context context);

   public abstract List<SurveyBase> mapToApi(
         List<org.infinitenature.werbeo.service.core.api.enity.Survey> entities,
         @org.mapstruct.Context Context context);

   public abstract List<ObfuscationPolicy> mapObfuscationPoliciesToApi(
         List<org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy> obfuscationPolicies);

   public abstract List<org.infinitenature.werbeo.service.core.api.enity.ObfuscationPolicy> mapObfuscationPoliciesToCore(
         List<ObfuscationPolicy> obfuscationPolicies);

   public abstract Availability map(
         org.infinitenature.werbeo.service.core.api.enity.Survey.Availability availability);

   public SurveysSliceResponse mapToApi(
         Slice<org.infinitenature.werbeo.service.core.api.enity.Survey, SurveySortField> entitySlice,
         Context context)

   {
      return new SurveysSliceResponse(
            mapToApi(entitySlice.getContent(), context), entitySlice.getSize());
   }

   @Mapping(target = "deputyCustodians", ignore = true)
   @Mapping(target = "owner", ignore = true)
   @Mapping(target = "parentId", ignore = true)
   @Mapping(target = "portal", ignore = true) // done in afterMapping
   public abstract org.infinitenature.werbeo.service.core.api.enity.Survey mapToCore(
         SurveyBase survey, @org.mapstruct.Context Context context);

   @AfterMapping
   public void addPortal(
         @MappingTarget org.infinitenature.werbeo.service.core.api.enity.Survey survey,
         SurveyBase mappingSource,
         @org.mapstruct.Context Context context)
   {
      survey.setPortal(context.getPortal());
   }
   
   @AfterMapping
   public void addPortalToApi(
         @MappingTarget SurveyBase survey,
         org.infinitenature.werbeo.service.core.api.enity.Survey mappingSource,
         @org.mapstruct.Context Context context)
   {
      survey.setPortal(new Portal(context.getPortal().getId()));
   }


   public abstract org.infinitenature.werbeo.service.core.api.enity.Survey.Availability mapToCore(
         Availability availability);

   public abstract SurveySortField mapToCore(
         org.infinitenature.service.v1.types.SurveySortField sortField);
}
