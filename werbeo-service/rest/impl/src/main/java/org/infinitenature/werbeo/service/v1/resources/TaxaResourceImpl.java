package org.infinitenature.werbeo.service.v1.resources;

import java.util.List;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.MTBResponse;
import org.infinitenature.service.v1.types.response.TaxonBaseSliceResponse;
import org.infinitenature.service.v1.types.response.TaxonFieldConfigResponse;
import org.infinitenature.service.v1.types.response.TaxonResponse;
import org.infinitenature.service.v1.types.response.TaxonSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.Taxon;
import org.infinitenature.werbeo.service.core.api.enity.meta.TaxonConfig;
import org.infinitenature.werbeo.service.core.api.enity.support.TaxonSortField;
import org.infinitenature.werbeo.service.core.api.ports.MetaInfoQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.TaxonFilter;
import org.infinitenature.werbeo.service.core.api.ports.TaxonQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.LanguageMapperRest;
import org.infinitenature.werbeo.service.mapper.MetaInfoMapperRest;
import org.infinitenature.werbeo.service.mapper.PositionMapper;
import org.infinitenature.werbeo.service.mapper.TaxonBaseMapperRest;
import org.infinitenature.werbeo.service.mapper.TaxonMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

@WerbeoResource
public class TaxaResourceImpl implements TaxaResource
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(TaxaResourceImpl.class);
   @Autowired
   private TaxonBaseMapperRest taxonBaseMapperRest;
   @Autowired
   private LanguageMapperRest languageMapper;
   @Autowired
   private TaxonMapperRest taxonMapperRest;
   @Autowired
   private TaxonQueryPort taxonQueryPort;
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private PositionMapper positionMapper;
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;
   @Autowired
   private MetaInfoMapperRest metaInfoMapper;
   @Autowired
   private MetaInfoQueryPort metaInfoQueryPort;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   @Cacheable("taxon")
   public TaxonResponse getTaxon(int portalId, int id)
   {
      return new TaxonResponse(taxonMapperRest.mapToApi(
            taxonQueryPort.get(id, contextFactory.createContext(portalId, httpHeaders)),
            contextFactory.createContext(portalId, httpHeaders)));
   }

   @Override
   public TaxonBaseSliceResponse find(int offset, int limit, TaxaFilter taxaFilter)
   {
      OffsetRequest<TaxonSortField> offsetRequest = new OffsetRequestImpl<>(
            offset,
            limit,
            SortOrder.ASC, TaxonSortField.TAXON);
      return taxonBaseMapperRest.mapToApi(
            taxonQueryPort.findBase(
                  new TaxonFilter(taxaFilter.getNameContains(),
                        languageMapper.mapToCoreApi(taxaFilter.getLanguages()),
                        taxaFilter.isWithSynonyms(),
                        taxaFilter.getOccurrencesChangedSince(),
                        taxaFilter.isOnlyUsedTaxa(),
                        taxaFilter.getExternalKeys(),
                        taxaFilter.getTaxaGroup(),
                        taxaFilter.isOnlyTaxaAvailableForInput()),
                  contextFactory.createContext(taxaFilter.getPortalId(), httpHeaders),
                  offsetRequest),
            contextFactory.createContext(taxaFilter.getPortalId(), httpHeaders));
   }


   @Override
   public CountResponse count(TaxaFilter taxaFilter)
   {
      Context context = contextFactory.createContext(taxaFilter.getPortalId(), httpHeaders);
      return new CountResponse(taxonQueryPort.count(
            new TaxonFilter(taxaFilter.getNameContains(),
                  languageMapper.mapToCoreApi(taxaFilter.getLanguages()),
                  taxaFilter.isWithSynonyms(),
                  taxaFilter.getOccurrencesChangedSince(),
                  taxaFilter.isOnlyUsedTaxa(), taxaFilter.getExternalKeys(),
                  taxaFilter.getTaxaGroup(),
                  taxaFilter.isOnlyTaxaAvailableForInput()),
            context), context.getRequestId());

   }

   @Override
   public TaxonSliceResponse findAllSynonyms(@Min(1) int portalId, int id)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      List<Taxon> taxa = taxonQueryPort.findAllSynonyms(id, context);
      List<org.infinitenature.service.v1.types.Taxon> taxaApi = taxonMapperRest
            .mapToApi(taxa, context);

      return new TaxonSliceResponse(taxaApi, taxaApi.size());
   }

   @Override
   @Cacheable("coveredMTB") // TODO this is a dirty hack because of #570 - there
                            // is NO cache eviction right now
   public MTBResponse getCoveredMTB(@Min(1) int portalId, int id,
         boolean inclusiveChildTaxa)
   {
      LOGGER.info("Requesting mtb coverage for taxa with id {}", id);
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new MTBResponse(positionMapper.toRestApi(occurrenceQueryPort
            .getCoveredMTB(id, inclusiveChildTaxa, context)));
   }

   @Override
   public TaxonFieldConfigResponse getFieldConfig(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      TaxonConfig taxonConfig = metaInfoQueryPort.getTaxonConfig(context);
      return new TaxonFieldConfigResponse(metaInfoMapper.mapTaxonToApi(
            taxonConfig.getConfiguredFields(), taxonConfig));
   }

   @Override
   public TaxonFieldConfigResponse getFieldConfig(@Min(1) int portalId,
         @Min(1) int surveyId)
   {
      return getFieldConfig(portalId);
   }
}
