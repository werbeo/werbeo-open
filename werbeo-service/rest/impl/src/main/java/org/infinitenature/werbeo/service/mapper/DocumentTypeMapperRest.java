package org.infinitenature.werbeo.service.mapper;

import org.infinitenature.service.v1.types.DocumentType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DocumentTypeMapperRest
{
   DocumentType mapToRestApi(
         org.infinitenature.werbeo.service.core.api.enity.DocumentType documentType);
}
