package org.infinitenature.werbeo.service.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.SampleCommentsResource;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.SampleCommentsSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.SampleComment;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class)
public abstract class SampleCommentMapperRest extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.SampleComment, SampleCommentsResource>
{
   @Autowired
   private LinkMapperRest linkMapper;

   public SampleCommentMapperRest()
   {
      super(SampleCommentsResource.class);
   }

   public abstract CommentSortField mapToApi(CommentField sortField);

   public SampleCommentsSliceResponse mapToApi(
         Slice<SampleComment, CommentSortField> entitySlice, Context context)
   {
      return new SampleCommentsSliceResponse(
            mapToApi(entitySlice.getContent(), context), entitySlice.getSize());
   }

   abstract List<org.infinitenature.service.v1.types.SampleComment> mapToApi(
         List<SampleComment> content, @org.mapstruct.Context Context context);

   @Mapping(target = "links", ignore = true) // TODO create correct links of
                                             // sub-resource
   abstract org.infinitenature.service.v1.types.SampleComment mapToApi(
         SampleComment entity, @org.mapstruct.Context Context context);

   public abstract SampleComment mapToCore(
         org.infinitenature.service.v1.types.SampleComment sampleComment,
         @org.mapstruct.Context Context context);

   public Link createSampleCommentIdLink(Context context, UUID sampleId, int id)
   {
      Map<String, Object> params = new HashMap<>();
      params.put("uuid", sampleId);
      return linkMapper.mapToRestAPI(createSpringIdLink(context, id, params));

   }
}
