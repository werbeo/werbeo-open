package org.infinitenature.werbeo.service.v1;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.infinitenature.service.v1.types.response.BaseResponse;
import org.infinitenature.service.v1.types.support.MetaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class PerformanceAspect
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(PerformanceAspect.class);

   @Pointcut("execution(org.infinitenature.service.v1.types.response.BaseResponse+ org.infinitenature.werbeo.service.v1.resources.*.*(..))")
   public void restMethod()
   {
   }

   @Around("restMethod()")
   public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable
   {
      StopWatch stopWatch = new StopWatch();
      stopWatch.start();
      Object retVal = pjp.proceed();
      stopWatch.stop();
      LOGGER.info("{} took {}ms", pjp.getSignature(),
            stopWatch.getTime(TimeUnit.MILLISECONDS));
      if (retVal instanceof BaseResponse)
      {
         BaseResponse baseRespone = (BaseResponse) retVal;
         if (baseRespone.getMetaData() == null)
         {
            baseRespone.setMetaData(new MetaData());
         }
         baseRespone.getMetaData()
               .setCreationTime(stopWatch.getTime(TimeUnit.MILLISECONDS));
      }
      return retVal;
   }
}
