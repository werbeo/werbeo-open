package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enums.Role;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigRest.class, uses = { LinkMapperRest.class })
public interface UserMapperRest
{
   @Mapping(target = "email", source = "login")
   org.infinitenature.service.v1.types.User mapToApi(User coreUser,
         @org.mapstruct.Context Context context);

   List<org.infinitenature.service.v1.types.User> mapToApi(List<User> coreUsers,
         @org.mapstruct.Context Context context);

   Role mapToCore(org.infinitenature.service.v1.types.enums.Role role,
         @org.mapstruct.Context Context context);
}
