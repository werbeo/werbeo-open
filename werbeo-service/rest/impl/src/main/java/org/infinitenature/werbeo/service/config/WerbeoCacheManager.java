package org.infinitenature.werbeo.service.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.Cache;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.caffeine.CaffeineCacheManager;

import com.github.benmanes.caffeine.cache.Caffeine;

public class WerbeoCacheManager extends CaffeineCacheManager
{
   @Override
   protected Cache createCaffeineCache(String name)
   {
      if ("occ-stats".equals(name))
      {
         return new CaffeineCache("occ-stats", Caffeine.newBuilder()
               .expireAfterWrite(1, TimeUnit.MINUTES).build());
      } else
      {
         return super.createCaffeineCache(name);
      }
   }
}
