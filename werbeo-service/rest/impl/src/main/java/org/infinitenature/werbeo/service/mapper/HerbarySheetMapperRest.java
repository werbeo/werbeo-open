package org.infinitenature.werbeo.service.mapper;

import org.infinitenature.service.v1.types.HerbarySheet;
import org.infinitenature.service.v1.types.enums.HerbarySheetPosition;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigRest.class)
public abstract class HerbarySheetMapperRest
{
   public abstract org.infinitenature.herbariorum.business.Institution mapToBusiness(
         org.infinitenature.herbariorum.entities.Institution institution);

   @Mapping(target = "institution", expression = "java(mapToBusiness(herbarySheet.getInstitution()))")
   @Mapping(target = "synonym", source = "synonym")
   public abstract org.infinitenature.werbeo.service.core.api.enity.HerbarySheet mapToApi(
         HerbarySheet herbarySheet);

   public abstract org.infinitenature.werbeo.service.core.api.enums.HerbarySheetPosition mapToApi(
         HerbarySheetPosition position);
}
