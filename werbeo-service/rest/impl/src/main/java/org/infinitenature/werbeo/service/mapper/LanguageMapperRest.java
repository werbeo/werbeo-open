package org.infinitenature.werbeo.service.mapper;

import java.util.Set;

import org.infinitenature.werbeo.service.core.api.enity.Language;
import org.mapstruct.Mapper;

@Mapper(config = CentralConfigRest.class)
public interface LanguageMapperRest
{
   Language mapToCoreApi(org.infinitenature.service.v1.types.Language language);

   Set<Language> mapToCoreApi(
         Set<org.infinitenature.service.v1.types.Language> language);

   org.infinitenature.service.v1.types.Language mapToRestApi(Language language);

}
