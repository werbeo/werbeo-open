package org.infinitenature.werbeo.service.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.resources.OccurrenceCommentsResource;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.OccurrenceCommentsSliceResponse;
import org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment;
import org.infinitenature.werbeo.service.core.api.enity.support.CommentSortField;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(config = CentralConfigRest.class)
public abstract class OccurrenceCommentsMapperRest
      extends
      RestMapper<org.infinitenature.werbeo.service.core.api.enity.OccurrenceComment, OccurrenceCommentsResource>
{
   @Autowired
   private LinkMapperRest linkMapper;

   public OccurrenceCommentsMapperRest()
   {
      super(OccurrenceCommentsResource.class);
   }

   public abstract CommentSortField mapToApi(CommentField sortField);

   @Mapping(target = "links", ignore = true) // TODO create correct links of
   // sub-resource
   abstract org.infinitenature.service.v1.types.OccurrenceComment mapToApi(
         OccurrenceComment entity, @org.mapstruct.Context Context context);

   abstract List<org.infinitenature.service.v1.types.OccurrenceComment> mapOccToApi(
         List<OccurrenceComment> content,
         @org.mapstruct.Context Context context);

   public abstract OccurrenceComment mapToCore(
         org.infinitenature.service.v1.types.OccurrenceComment occurrenceComment,
         @org.mapstruct.Context Context context);

   public OccurrenceCommentsSliceResponse mapOccurrenceToApi(
         Slice<OccurrenceComment, CommentSortField> comments, Context context)
   {
      return new OccurrenceCommentsSliceResponse(
            mapOccToApi(comments.getContent(), context), comments.getSize());
   }

   public Link createOccurrenceCommentIdLink(Context context, UUID occurrenceId,
         int id)
   {
      Map<String, Object> params = new HashMap<>();
      params.put("uuid", occurrenceId);
      return linkMapper.mapToRestAPI(
            createSpringIdLink(context, id, params ));

   }
}
