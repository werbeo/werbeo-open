package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.OccurrenceImport;
import org.infinitenature.service.v1.types.OccurrenceImportField;
import org.infinitenature.service.v1.types.response.OccurrenceImportsInfoResponse;
import org.infinitenature.werbeo.service.core.api.enity.CSVImport;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceImportSortField;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigRest.class, uses = { JobStatusMapper.class })
public abstract class OccurrenceImportMapperRest
{
   public abstract FileFormat map(ExportFormat format);

   public String map(FileFormat fileFormat)
   {
      switch (fileFormat)
      {
      case XML_BDE:
         return MediaType.APPLICATION_XML_TYPE.toString();

      default:
         return MediaType.WILDCARD;

      }
   }

   @Mapping(target = "fileFormat", source = "fileFormat")
   public abstract OccurrenceImport map(CSVImport export);

   public abstract List<OccurrenceImport> map(List<CSVImport> exports);

   public abstract OccurrenceImportSortField mapToApi(
         OccurrenceImportField sortField);

   public OccurrenceImportsInfoResponse mapCoreToApi(
         Slice<CSVImport, OccurrenceImportSortField> entitySlice)
   {
      return new OccurrenceImportsInfoResponse(map(entitySlice.getContent()),
            entitySlice.getSize());
   }
}
