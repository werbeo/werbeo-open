package org.infinitenature.werbeo.service.config;

import org.infinitenature.service.support.xml.converter.LocalDateConverterProvider;
import org.infinitenature.werbeo.service.CorsFilter;
import org.infinitenature.werbeo.service.mapper.exception.WerbeoExceptionMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestEasyConfig
{
   @Bean
   public LocalDateConverterProvider localDateConverterProvider()
   {
      return new LocalDateConverterProvider();
   }

   @Bean
   public WerbeoExceptionMapper werbeoExceptionMapper()
   {
      return new WerbeoExceptionMapper();
   }

   @Bean
   public CorsFilter corsFilter()
   {
      return new CorsFilter();
   }
}
