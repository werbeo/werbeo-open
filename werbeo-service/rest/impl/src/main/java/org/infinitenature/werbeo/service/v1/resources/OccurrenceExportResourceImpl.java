package org.infinitenature.werbeo.service.v1.resources;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.OccurrenceExportResource;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceFilterPOST;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportStatusResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportsInfoResponse;
import org.infinitenature.werbeo.service.core.api.enity.TypedFile;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceExportSortField;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceExportCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceExportQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.JobStatusMapper;
import org.infinitenature.werbeo.service.mapper.OccurrenceExportMapperRest;
import org.infinitenature.werbeo.service.mapper.OccurrenceMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class OccurrenceExportResourceImpl implements OccurrenceExportResource
{
   @Autowired
   private OccurrenceMapperRest occurrenceMapper;
   @Autowired
   private OccurrenceExportMapperRest occurrenceExportMapper;
   @Autowired
   private OccurrenceExportQueryPort occurrenceExportQueryPort;
   @Autowired
   private OccurrenceExportCommandPort occurrenceExportCommandPort;
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private JobStatusMapper jobStatusMapper;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;
   @Override
   public Response getExport(UUID exportId, int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);

      TypedFile export = occurrenceExportQueryPort.load(exportId, context);

      return Response.ok(export.getData(),
            occurrenceExportMapper.map(export.getFileFormat())).build();
   }

   @Override
   public OccurrenceExportStatusResponse getStatus(UUID exportId, int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new OccurrenceExportStatusResponse(
            occurrenceExportMapper.map(
                  occurrenceExportQueryPort.get(exportId, context)));
   }

   @Override
   public OccurrenceExportResponse create(OccurrenceFilter filter,
         ExportFormat exportFormat)
   {
      Context context = contextFactory.createContext(filter.getPortalId(), httpHeaders);
      return new OccurrenceExportResponse(
            occurrenceExportCommandPort.create(occurrenceMapper.mapCoreToApi(filter),
                  occurrenceExportMapper.map(exportFormat), context));
   }
   
   @Override
   public OccurrenceExportResponse createLargeWKT(OccurrenceFilterPOST filter,
         ExportFormat exportFormat)
   {
      Context context = contextFactory.createContext(filter.getPortalId(), httpHeaders);
      return new OccurrenceExportResponse(
            occurrenceExportCommandPort.create(occurrenceMapper.mapCoreToApi(filter),
                  occurrenceExportMapper.map(exportFormat), context));
   }

   @Override
   public DeletedResponse delete(UUID exportId, int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      occurrenceExportCommandPort.delete(exportId, context);
      return new DeletedResponse();
   }

   @Override
   public OccurrenceExportsInfoResponse findExports(@Min(1) int portalId,
         @Min(0) int offset, int limit, OccurrenceExportField sortField,
         SortOrder sortOrder)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      OffsetRequest<OccurrenceExportSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder,
            occurrenceExportMapper.mapToApi(sortField));
      return occurrenceExportMapper
            .mapCoreToApi(
                  occurrenceExportQueryPort.find(null, context, offsetRequest));
   }

   @Override
   public CountResponse count(@Min(1) int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      return new CountResponse(occurrenceExportQueryPort.count(null, context),
            context.getRequestId());
   }

}
