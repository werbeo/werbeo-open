package org.infinitenature.werbeo.service.v1.resources;

import java.util.Base64;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.service.v1.resources.OccurrenceMediaResource;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.MediaResponse;
import org.infinitenature.service.v1.types.support.MediaUpload;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.infinitenature.werbeo.service.core.api.enity.Sample;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceCommandsPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.SampleCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.SampleQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.MediaMapperRest;
import org.infinitenature.werbeo.service.mapper.MediaPathUtils;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class OccurrenceMediaResourceImpl implements OccurrenceMediaResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceMediaResourceImpl.class);
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private OccurrenceCommandsPort occurenceComandsPorts;
   @Autowired
   private SampleCommandPort sampleComandsPorts;
   @Autowired
   private MediaMapperRest mediaMapper;
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;
   @Autowired
   private SampleQueryPort sampleQueryPort;
   @Autowired
   private MediaPathUtils mediaPathUtils;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;
   @Override
   public MediaResponse attachMedia(int portalId, UUID uuid,
         MediaUpload mediaUpload)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      List<Medium> media = occurenceComandsPorts.addMedia(uuid,
            new org.infinitenature.werbeo.service.core.api.ports.MediaUpload(
                  Base64.getDecoder().decode(mediaUpload.getData()), mediaUpload.getFileName(), mediaUpload.getDescription()),
            context);


      updateOccurrence(uuid, portalId);

      return new MediaResponse(mediaMapper.map(media));

   }

   @Override
   public DeletedResponse deleteMedia(int portalId, UUID occurrenceId,
         String mediaURL)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      occurenceComandsPorts.deleteMedium(occurrenceId,
            mediaPathUtils.clearPath(mediaURL), context);
      return new DeletedResponse();
   }

   private void updateOccurrence(UUID uuid, int portalId)
   {
      try
      {
         Context context = contextFactory.createContext(portalId, httpHeaders);
         UUID sampleId = occurrenceQueryPort.get(uuid, context).getSample()
               .getId();
         Sample sample = sampleQueryPort.get(sampleId, context);
         sampleComandsPorts.saveOrUpdate(sample, context);
      } catch (Exception e)
      {

      }
   }
}
