package org.infinitenature.werbeo.service.mapper;

import java.util.Set;

import org.infinitenature.service.v1.types.enums.Operation;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OperationsMapper
{
   Operation map(
         org.infinitenature.werbeo.service.core.access.Operation operation);

   Set<Operation> map(
         Set<org.infinitenature.werbeo.service.core.access.Operation> operations);
}
