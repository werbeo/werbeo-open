package org.infinitenature.werbeo.service.aspects;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.piwik.RestServiceTracker;
import org.piwik.java.tracking.PiwikRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.togglz.core.manager.FeatureManager;

@Component
@Aspect
public class RestServiceTracking
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(RestServiceTracking.class);

   @Autowired
   private InstanceConfig instanceConfig;
   @Autowired
   private RestServiceTracker restServiceTracker;
   @Autowired
   private FeatureManager featureManager;

   @Pointcut("execution(public * org.infinitenature.werbeo.service.v1.resources.*.*(..))")
   void restServiceMethod()
   {
      // just a pointcut definition
   }

   @Around("restServiceMethod()")
   public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable
   {

      StopWatch stopWatch = StopWatch.createStarted();

      LOGGER.info("REST Service request {}", pjp.getSignature().getName());
      Object result = pjp.proceed();
      stopWatch.stop();
      try
      {
         if (featureManager.isActive(WerBeoFeatures.TRACK_REST_SERVICE_CALLS))
         {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                  .getRequestAttributes()).getRequest();

            String requestURL = request.getRequestURL().toString();
            String userName = SecurityContextHolder.getContext()
                  .getAuthentication().getName();
            String queryString = request.getQueryString();
            String completeURL = StringUtils.isBlank(queryString) ? requestURL
                  : requestURL + "?" + queryString;

            PiwikRequest piwikRequest = new PiwikRequest(
                  instanceConfig.getPiwik().getSiteId(), null);
            piwikRequest.setActionUrlWithString(completeURL);
            piwikRequest.setAuthToken(instanceConfig.getPiwik().getAuthToken());
            piwikRequest.setUserId(userName);
            piwikRequest.setVisitorIp(request.getRemoteAddr());
            piwikRequest.setHeaderUserAgent(request.getHeader("User-Agent"));
            piwikRequest.setActionTime(stopWatch.getTime());

            restServiceTracker.track(piwikRequest);
         }
      } catch (Exception e)
      {
         LOGGER.error("Failure tracking request", e);
      }
      return result;
   }
}