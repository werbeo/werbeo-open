package org.infinitenature.werbeo.service.v1.resources;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;

import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.PeopleFilter;
import org.infinitenature.service.v1.resources.PeopleResource;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.PersonResponse;
import org.infinitenature.service.v1.types.response.PersonSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.werbeo.service.core.api.enity.support.PersonSortField;
import org.infinitenature.werbeo.service.core.api.ports.PeopleCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.PersonQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.PersonMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class PeopleResourceImpl implements PeopleResource
{
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private PersonMapperRest personMapper;
   @Autowired
   private PersonQueryPort peopleFacade;
   @Autowired
   private PeopleCommandPort peopleCommandPort;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;
   @Override
   public PersonResponse getPerson(int portalId, int id)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      org.infinitenature.werbeo.service.core.api.enity.Person person = peopleFacade
            .get(id, context);
      if (person == null)
      {
         throw new NotFoundException();
      }
      return new PersonResponse(personMapper.mapToApi(person, context));
   }

   @Override
   public PersonSliceResponse find(int offset, int limit, PersonField sortField,
         SortOrder sortOrder, PeopleFilter peopleFilter)
   {
      OffsetRequest<PersonSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, personMapper.mapToCore(sortField));
      Context context = contextFactory.createContext(peopleFilter.getPortalId(), httpHeaders);
      return personMapper.mapToApi(peopleFacade.find(
            personMapper.mapToCore(peopleFilter), context, offsetRequest),
            context);
   }

   @Override
   public CountResponse count(PeopleFilter peopleFilter)
   {
      Context context = contextFactory.createContext(peopleFilter.getPortalId(), httpHeaders);
      return new CountResponse(
            peopleFacade.count(personMapper.mapToCore(peopleFilter), context),
            context.getRequestId());
   }

   @Override
   public SaveOrUpdateResponse save(int portalId, Person person)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      int id = peopleCommandPort
            .saveOrUpdate(personMapper.mapToCore(person, context), context);
      Link link = personMapper.createIdLink(context, id);
      return new SaveOrUpdateResponse(link, id);
   }

}
