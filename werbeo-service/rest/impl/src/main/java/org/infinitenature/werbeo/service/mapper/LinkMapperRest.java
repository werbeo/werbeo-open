package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import org.infinitenature.service.v1.types.atom.Link;
import org.mapstruct.Mapper;
import org.springframework.hateoas.LinkRelation;

@Mapper(config = CentralConfigRest.class)
public interface LinkMapperRest
{
   Link mapToRestAPI(org.springframework.hateoas.Link springLink);

   List<Link> mapToRestAPI(List<org.springframework.hateoas.Link> springLinks);

   default String map(LinkRelation rel)
   {
      return rel.value();
   }
}
