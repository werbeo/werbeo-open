package org.infinitenature.werbeo.service.v1.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.Min;
import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.OffsetRequest;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.commons.pagination.impl.OffsetRequestImpl;
import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.SampleFieldConfigResponse;
import org.infinitenature.service.v1.types.response.SampleResponse;
import org.infinitenature.service.v1.types.response.SampleSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.werbeo.service.core.api.enity.meta.SampleConfig;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceSortField;
import org.infinitenature.werbeo.service.core.api.ports.MetaInfoQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.api.ports.SampleCommandPort;
import org.infinitenature.werbeo.service.core.api.ports.SampleQueryPort;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.mapper.MetaInfoMapperRest;
import org.infinitenature.werbeo.service.mapper.OccurrenceMapperRest;
import org.infinitenature.werbeo.service.mapper.SampleMapperRest;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.infinitenature.werbeo.service.util.DocumentLinkInliner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@WerbeoResource
public class SamplesResourceImpl implements SamplesResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SamplesResourceImpl.class);
   @Autowired
   private ContextFactory contextFactory;
   @Autowired
   private SampleQueryPort samplesFacade;
   @Autowired
   private SampleCommandPort sampleCommandPort;
   @Autowired
   private SampleMapperRest sampleMapper;
   @Autowired
   private MetaInfoMapperRest metaInfoMapper;
   @Autowired
   private MetaInfoQueryPort metaInfoCommandPort;
   @Autowired
   private OccurrenceQueryPort occurrenceQueryPort;
   @Autowired
   private OccurrenceMapperRest occurrenceMapper;
   @Autowired
   private DocumentLinkInliner documentLinkInliner;
   @javax.ws.rs.core.Context
   private HttpHeaders httpHeaders;

   @Override
   public SampleResponse get(int portalId, UUID uuid)
   {
      LOGGER.info("Request for sample {} for portal {}", uuid, portalId);
      Context context = contextFactory.createContext(portalId, httpHeaders);
      org.infinitenature.werbeo.service.core.api.enity.Sample sample = samplesFacade
            .get(uuid, context);
      for (org.infinitenature.werbeo.service.core.api.enity.Occurrence occurrence : sample
            .getOccurrences())
      {
         occurrence.setSample(null);
      }
      return new SampleResponse(sampleMapper.mapToApi(sample, context));
   }

   @Override
   public DeletedResponse delete(int portalId, UUID uuid)
   {
      LOGGER.info("Delete sample {} for portal {}", uuid, portalId);
      Context context = contextFactory.createContext(portalId, httpHeaders);
      sampleCommandPort.delete(uuid, context);
      return new DeletedResponse();
   }

   @Override
   public SampleSliceResponse find(int offset, int limit,
         OccurrenceField sortField, SortOrder sortOrder,
         org.infinitenature.service.v1.resources.OccurrenceFilter filter,
         boolean inlineThumbs)
   {

      LOGGER.info(
            "find , offset {}, limit {}, sortField {}, sortOrder {}, filter {}",
            offset, limit, sortField, sortOrder, filter);
      OffsetRequest<OccurrenceSortField> offsetRequest = new OffsetRequestImpl<>(
            offset, limit, sortOrder, occurrenceMapper.mapToApi(sortField));

      Context context = contextFactory.createContext(filter.getPortalId(),
            httpHeaders);

      OccurrenceSliceResponse occurrenceResponse = occurrenceMapper
            .mapToApi(
                  occurrenceQueryPort.find(occurrenceMapper.mapCoreToApi(filter)
                        .withGroupBySample(true), context, offsetRequest),
                  context);

      if (inlineThumbs)
      {
         occurrenceResponse.getContent().stream().map(Occurrence::getDocuments)
               .forEach(documents -> documentLinkInliner.inlineLinks(documents,
                     DocumentType.IMAGE_THUMB));
      }
      return map(occurrenceResponse, limit, context);
   }

   private SampleSliceResponse map(OccurrenceSliceResponse occurrences,
         int limit, Context context)
   {
      List<Sample> samples = new ArrayList<>();
      for (Occurrence occurrence : occurrences.getContent())
      {
         Sample sample = new Sample(occurrence.getSample());
         sample.getOccurrences()
               .addAll(occurrenceMapper.mapToApi(occurrenceQueryPort
                     .findBySampleId(sample.getEntityId(), context), context));
         samples.add(sample);
      }
      return new SampleSliceResponse(samples, limit);
   }

   @Override
   public CountResponse count(
         org.infinitenature.service.v1.resources.OccurrenceFilter filter)
   {
      LOGGER.info("count filter {}", filter);
      Context context = contextFactory.createContext(filter.getPortalId(),
            httpHeaders);
      long count = occurrenceQueryPort.count(
            occurrenceMapper.mapCoreToApi(filter).withGroupBySample(true),
            context);
      LOGGER.info("Count result {}", count);
      return new CountResponse(count, context.getRequestId());
   }

   @Override
   public SaveOrUpdateResponse save(int portalId, Sample sample)
   {
      LOGGER.info("Save sample {} for portal {}", sample, portalId);
      Context context = contextFactory.createContext(portalId, httpHeaders);

      /**
       * Nasty workaround here
       * https://git.loe.auf.uni-rostock.de/werbeo/werbeo/-/issues/1056 TODO:
       * Remove asap after App enhancement
       */

      removeDuplicatedPointsInPolygon(sample);

      UUID id = sampleCommandPort
            .saveOrUpdate(sampleMapper.mapToCore(sample, context), context);
      Link link = sampleMapper.createIdLink(context, id);
      return new SaveOrUpdateResponse(link, id);
   }

   protected void removeDuplicatedPointsInPolygon(Sample sample)
   {
      String wkt = sample.getLocality().getPosition().getWkt();
      if (StringUtils.isNotBlank(wkt) && !wkt.contains("POINT") && !wkt.contains("MULTIPOLYGON" ))
      {
         String shapeType = wkt.substring(0, wkt.indexOf("(("));
         List<String> points = new ArrayList<String>();
         String[] pointArray = wkt.replace(shapeType + "((", "")
               .replace("))", "").split(",");
         for (int i = 0; i < pointArray.length; i++)
         {
            if (i > 0 && pointArray[i].equals(pointArray[i - 1]))
            {
               // nothing to do
            } else
            {
               points.add(pointArray[i]);
            }
         }
         sample.getLocality().getPosition()
               .setWkt(shapeType + "((" + StringUtils.join(points, ",") + "))");
      }
   }

   @Override
   public SampleFieldConfigResponse getFieldConfig(int portalId)
   {
      Context context = contextFactory.createContext(portalId, httpHeaders);
      SampleConfig sampleConfig = metaInfoCommandPort.getSampleConfig(context);
      Set<SampleFieldConfig> sampleFieldConfigs = metaInfoMapper
            .mapSampleToApi(sampleConfig.getConfiguredFields(), sampleConfig);
      return new SampleFieldConfigResponse(sampleFieldConfigs);
   }

   @Override
   public SampleFieldConfigResponse getFieldConfig(@Min(1) int portalId,
         @Min(1) int surveyId)
   {
      return getFieldConfig(portalId);
   }
}
