package org.infinitenature.werbeo.service.mapper;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.infinitenature.service.v1.types.response.BaseResponse;
import org.infinitenature.service.v1.types.support.Base;
import org.infinitenature.werbeo.common.commons.DateTimeUtils;
import org.infinitenature.werbeo.service.aspects.AddRequestIdToResponseAspect;
import org.infinitenature.werbeo.service.core.api.enity.BaseType;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.mapstruct.AfterMapping;
import org.mapstruct.DecoratedWith;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.jaxrs.JaxRsLinkBuilder;

@DecoratedWith(AddRequestIdToResponseAspect.class)
public class RestMapper<ENTITY extends BaseType<?>, RESOURCE>
{
   private static final ZoneId UTC = ZoneId.of("UTC");

   @Autowired
   private LinkMapperRest linkMapper;

   private final Class<RESOURCE> resourceClass;

   public RestMapper(Class<RESOURCE> resourceClass)
   {
      this.resourceClass = resourceClass;
   }

   public List<org.infinitenature.service.v1.types.atom.Link> createLinks(
         ENTITY entity, Context context)
   {
      ArrayList<Link> springLinkList = new ArrayList<>();

      springLinkList.add(createSpringIdLink(context, entity.getId(),
            Collections.emptyMap()));

      return linkMapper.mapToRestAPI(springLinkList);
   }

   protected Link createSpringIdLink(Context context, Object entityId,
         Map<String, Object> moreParams)
   {
      Map<String, Object> params = new HashMap<>();
      params.put("portalId", context.getPortal().getId());
      params.putAll(moreParams);
      return JaxRsLinkBuilder.linkTo(resourceClass, params).slash(entityId)
            .withSelfRel();
   }

   public org.infinitenature.service.v1.types.atom.Link createIdLink(
         Context context, Object entityId)
   {
      return linkMapper.mapToRestAPI(
            createSpringIdLink(context, entityId, Collections.emptyMap()));
   }

   public void setLinkMapper(LinkMapperRest linkMapper)
   {
      this.linkMapper = linkMapper;
   }

   @AfterMapping
   void modifiyTimezone(@MappingTarget Base<?> target, BaseType<?> source,
         @org.mapstruct.Context Context context)
   {
      final ZoneId targetTimeZone = ZoneId.of("Europe/Berlin");
      if (source.getModificationDate() != null)
      {
         target.setModificationDate(
               DateTimeUtils.toZone(source.getModificationDate(), UTC, targetTimeZone));
      }
      if (source.getCreationDate() != null)
      {
         target.setCreationDate(
               DateTimeUtils.toZone(source.getModificationDate(), UTC, targetTimeZone));
      }
   }

   void addRequestId(@MappingTarget BaseResponse response,
         @org.mapstruct.Context Context context)
   {
      response.getMetaData().setSessionId(context.getRequestId());
   }
}
