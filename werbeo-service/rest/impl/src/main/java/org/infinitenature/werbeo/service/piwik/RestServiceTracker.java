package org.infinitenature.werbeo.service.piwik;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpResponse;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.piwik.java.tracking.PiwikRequest;
import org.piwik.java.tracking.PiwikTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.togglz.core.manager.FeatureManager;

@Component
public class RestServiceTracker
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(RestServiceTracker.class);
   private final PiwikTracker piwikTracker;
   private final BlockingQueue<PiwikRequest> requests = new LinkedBlockingQueue<>();
   private final FeatureManager featureManager;
   private final String authToken;

   @Autowired
   public RestServiceTracker(PiwikTracker piwikTracker,
         FeatureManager featureManager, InstanceConfig instanceConfig)
   {
      this.piwikTracker = piwikTracker;
      this.featureManager = featureManager;
      this.authToken = instanceConfig.getPiwik().getAuthToken();
   }

   public void track(PiwikRequest request)
   {
      requests.add(request);
   }

   @Scheduled(fixedDelayString = "PT10S")
   public void send()
   {
      if (featureManager.isActive(WerBeoFeatures.TRACK_REST_SERVICE_CALLS))
      {
         Collection<PiwikRequest> requestsToSend = new LinkedList<>();
         requests.drainTo(requestsToSend);
         if (!requestsToSend.isEmpty())
         {
            try
            {
               Future<HttpResponse> sendBulkRequestAsync = piwikTracker
                     .sendBulkRequestAsync(requestsToSend, authToken);
               LOGGER.info("Send {} requests to piwik", requestsToSend.size());
               HttpResponse response = sendBulkRequestAsync.get(2,
                     TimeUnit.SECONDS);
               LOGGER.info("Result: " + response.getStatusLine());
            } catch (IOException | InterruptedException | ExecutionException
                  | TimeoutException e)
            {
               LOGGER.error("Failure sending {} request to piwik",
                     requestsToSend.size(), e);
            }
         }
      }
   }
}
