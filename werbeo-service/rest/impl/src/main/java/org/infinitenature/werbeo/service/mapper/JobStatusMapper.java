package org.infinitenature.werbeo.service.mapper;

import org.infinitenature.werbeo.service.core.api.enity.JobStatus;
import org.mapstruct.Mapper;

@Mapper(config = CentralConfigRest.class)
public interface JobStatusMapper
{
   public org.infinitenature.service.v1.types.JobStatus map(
         JobStatus status);

   public JobStatus map(org.infinitenature.service.v1.types.JobStatus jobStatus);
}
