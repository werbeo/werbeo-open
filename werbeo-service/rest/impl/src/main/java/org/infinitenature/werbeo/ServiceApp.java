package org.infinitenature.werbeo;

import org.infinitenature.werbeo.service.indicia.jpa.repositories.OccurrenceRepository;
import org.infinitenature.werbeo.service.indicia.jpa.repositories.QueryDslJpaEnhancedRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

@EntityScan(basePackageClasses = { ServiceApp.class,
      Jsr310JpaConverters.class })
@SpringBootApplication
@EnableCaching
@EnableAspectJAutoProxy
@EnableAsync
@EnableScheduling
@EnableJpaRepositories(repositoryBaseClass = QueryDslJpaEnhancedRepositoryImpl.class, basePackageClasses = OccurrenceRepository.class)
public class ServiceApp
{
   public static void main(String[] args)
   {
      SpringApplication.run(ServiceApp.class, args);
   }

   @Bean
   public RestTemplate restTemplate()
   {
      return new RestTemplate();
   }

   @Bean
   public JaxbAnnotationModule jacksonJaxb()
   {
      return new JaxbAnnotationModule();
   }

}
