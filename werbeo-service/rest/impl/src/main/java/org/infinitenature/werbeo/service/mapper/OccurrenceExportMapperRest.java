package org.infinitenature.werbeo.service.mapper;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.infinitenature.commons.pagination.Slice;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.OccurrenceExport;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.service.v1.types.response.OccurrenceExportsInfoResponse;
import org.infinitenature.werbeo.service.core.api.enity.Export;
import org.infinitenature.werbeo.service.core.api.enity.FileFormat;
import org.infinitenature.werbeo.service.core.api.enity.support.OccurrenceExportSortField;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = CentralConfigRest.class, uses = { JobStatusMapper.class })
public abstract class OccurrenceExportMapperRest
{
   public abstract FileFormat map(ExportFormat format);

   public String map(FileFormat fileFormat)
   {
      switch (fileFormat)
      {
      case XML_BDE:
         return MediaType.APPLICATION_XML_TYPE.toString();

      default:
         return MediaType.WILDCARD;

      }
   }

   @Mapping(target = "fileFormat", source = "fileFormat")
   public abstract OccurrenceExport map(Export export);

   public abstract List<OccurrenceExport> map(List<Export> exports);

   public abstract OccurrenceExportSortField mapToApi(
         OccurrenceExportField sortField);

   public OccurrenceExportsInfoResponse mapCoreToApi(
         Slice<Export, OccurrenceExportSortField> entitySlice)
   {
      return new OccurrenceExportsInfoResponse(map(entitySlice.getContent()),
            entitySlice.getSize());
   }
}
