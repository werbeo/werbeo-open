package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.List;

import javax.annotation.PostConstruct;

import org.infinitenature.service.v1.resources.UsersResource;
import org.infinitenature.service.v1.types.enums.Role;
import org.infinitenature.service.v1.types.response.AddRoleResponse;
import org.infinitenature.service.v1.types.response.UserSliceResponse;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ITestRESTUsersResource extends AbstractITestREST
{
   private UsersResource resourceUT;

   @PostConstruct
   @Override
   protected void setUp()
   {
      super.setUp();
      resourceUT = client.users();
   }

   @Test
   @DisplayName("Find by email")
   void test001() throws Exception
   {
      UserSliceResponse response = resourceUT.find(PortalTestUtil.FLORA_MV_ID,
            0, 10, UserTestUtil.LOGIN_CTOMALAA);

      assertAll(
            () -> assertThat("wrong number of users returned",
                  response.getContent(), hasSize(1)),
            () -> assertThat("wrong user name",
                  response.getContent().get(0).getEmail(),
                  is("ctomalaa@ucoz.com")),
            () -> assertThat("wrong number of roles",
                  response.getContent().get(0).getRoles(), hasSize(4)),
            () -> assertThat("wrong roles",
                  response.getContent().get(0).getRoles(), containsInAnyOrder(
                        Role.ADMIN, Role.VALIDATOR, Role.ACCEPTED,
                        Role.APPROVED)));
   }

   @Test
   @DisplayName("Find all")
   void test002() throws Exception
   {
      UserSliceResponse response = resourceUT.find(PortalTestUtil.FLORA_MV_ID,
            0, 10, "");

      assertAll(
            () -> assertThat("wrong number of users returned",
                  response.getContent(), hasSize(5)),
            () -> assertThat("wrong user name",
                  response.getContent().get(0).getEmail(),
                  is("awalters8@wunderground.com")),
            () -> assertThat("wrong number of roles",
                  response.getContent().get(0).getRoles(), hasSize(1)),
            () -> assertThat("wrong roles",
                  response.getContent().get(0).getRoles(),
                  containsInAnyOrder(Role.ACCEPTED)));
   }

   @Test
   @DisplayName("modify rules")
   void test003() throws Exception
   {
      String email = "awalters8@wunderground.com";
      List<Role> existingRoles = resourceUT
            .find(PortalTestUtil.FLORA_MV_ID, 0, 1, email).getContent().get(0)
            .getRoles();

      assertThat("precondition is not met", existingRoles, hasSize(1));

      Role roleToRemove = existingRoles.get(0);
      AddRoleResponse response = resourceUT.removeRole(
            PortalTestUtil.FLORA_MV_ID, email, roleToRemove);

      List<Role> rolesAfterRemoveMV = resourceUT
            .find(PortalTestUtil.FLORA_MV_ID, 0, 1, email).getContent().get(0)
            .getRoles();
      List<Role> rolesAfterRemoveBB = resourceUT
            .find(PortalTestUtil.FLORA_BB_ID, 0, 1, email).getContent().get(0)
            .getRoles();
      resourceUT.addRole(PortalTestUtil.FLORA_MV_ID, email, roleToRemove);

      List<Role> rolesAfterAdd = resourceUT
            .find(PortalTestUtil.FLORA_MV_ID, 0, 1, email).getContent().get(0)
            .getRoles();
      assertAll(
            () -> assertThat("role not removed", rolesAfterRemoveMV,
                  hasSize(0)),
            () -> assertThat("role removed in wrong portal", rolesAfterRemoveBB,
                  hasSize(1)),
            () -> assertThat("role not added", rolesAfterAdd,
                  is(existingRoles)));
   }
}
