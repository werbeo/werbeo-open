package org.infinitenature.werbeo.service.v1.resource;

import static io.restassured.RestAssured.given;
import static io.restassured.matcher.RestAssuredMatchers.containsPath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ws.rs.BadRequestException;

import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.Language;
import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.meta.TaxonFieldConfig;
import org.infinitenature.service.v1.types.response.TaxonBaseSliceResponse;
import org.infinitenature.service.v1.types.response.TaxonSliceResponse;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;

class ITestRESTTaxonResource extends RestApiTest
{
   private TaxaResource taxaResourceUT;

   private static final Set<Language> latinOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.LAT)));

   private static final Set<Language> germanOnly = Collections
         .unmodifiableSet(new HashSet<>(Arrays.asList(Language.DEU)));


   @PostConstruct
   public void setUp()
   {
      taxaResourceUT = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA).taxa();
   }

   @Test
   @DisplayName("find - filter by external key")
   void test001() {
      TaxaFilter filter = new TaxaFilter();
      filter.setPortalId(PortalTestUtil.FLORA_MV_ID);
      filter.getExternalKeys().add("1");
      filter.setLanguages(latinOnly);

      TaxonBaseSliceResponse response = taxaResourceUT.find(0, 10, filter);
      List<TaxonBase> content = response.getContent();
      TaxonBase abiesAlba = content.get(0);

      assertAll(() -> assertThat(content, hasSize(1)),
            () -> assertThat(abiesAlba.getName(), is("Abies alba")),
            () -> assertThat(abiesAlba.getTaxaList().getName(),
                  is("GermanSL 1.5.1")));

   }

   @Test
   @DisplayName("find - name abies, only taxa allowed for input")
   void test002()
   {
      TaxaFilter filter = new TaxaFilter();
      filter.setPortalId(PortalTestUtil.FLORA_MV_ID);
      filter.setOnlyTaxaAvailableForInput(true);
      filter.setLanguages(latinOnly);
      filter.setNameContains("Abies");

      TaxonBaseSliceResponse response = taxaResourceUT.find(0, 20, filter);

      List<String> taxonNames = response.getContent().stream()
            .map(TaxonBase::getName).collect(Collectors.toList());
      assertAll(() -> assertThat(response.getContent(), hasSize(4)),
            () -> assertThat(taxonNames,
                  containsInAnyOrder("Abies nordmanniana", "Abies grandis",
                        "Abies concolor", "Abies alba")));

   }

   @Test
   void testFind_All()
   {
      TaxonBaseSliceResponse response = taxaResourceUT.find(0, 10, new TaxaFilter(
            PortalTestUtil.FLORA_MV_ID, "", latinOnly, false, null, false, null, false));
      List<TaxonBase> taxa = response.getContent();

      assertThat(taxa.size(), is(10));
   }

   @Test
   void testFind_PortalSep()
   {
      TaxonBaseSliceResponse response = taxaResourceUT.find(0, 10,
            new TaxaFilter(PortalTestUtil.FLORA_MV_ID, "Chorthippus", latinOnly,
                  false, null, false, null, false));
      List<TaxonBase> taxa = response.getContent();

      assertThat(taxa.size(), is(0));
   }

   @Test
   void testFind_PortalSep_Chorthippus()
   {
      List<TaxonBase> chorthippus = taxaResourceUT.find(0,
            20,
            new TaxaFilter(PortalTestUtil.HEUSCHRECKEN_D_ID, "Chorthippus",
                  latinOnly, false, null, false, null, false)).getContent();
      assertAll(() -> assertThat(chorthippus.size(), is(
            8)),
            () -> assertThat(chorthippus.get(0).getName(),
                  is("Chorthippus albomarginatus")));

      List<TaxonBase> abies = taxaResourceUT
            .find(0, 10, new TaxaFilter(PortalTestUtil.HEUSCHRECKEN_D_ID,
                  "abies", latinOnly, false, null, false, null, false))
            .getContent();

      assertThat(abies, hasSize(0));
   }

   @Test
   void testFind_AbiesAlba()
   {
      List<TaxonBase> taxa = taxaResourceUT
            .find(0, 10,
                  new TaxaFilter(PortalTestUtil.DEMO_ID, "Abies alba",
                        latinOnly, false, null, false, null, false))
            .getContent();

      assertThat(taxa.size(), is(1));
      assertThat(taxa.get(0).getEntityId(), is(49403));
      assertThat(taxa.get(0).getGroup(), is("Spermatophyta"));
   }

   @Test
   void testFind_AbiesAlba_WrongGroup()
   {
      List<TaxonBase> taxa = taxaResourceUT
            .find(0, 10, new TaxaFilter(PortalTestUtil.DEMO_ID, "Abies alba",
                  latinOnly, false, null, false, "Algen", false))
            .getContent();

      assertThat(taxa.size(), is(0));
   }

   @Test
   void testFind_WrongGroup_Filter()
   {
      assertThrows(BadRequestException.class, () ->
      {
         taxaResourceUT
            .find(0, 10, new TaxaFilter(PortalTestUtil.FLORA_MV_ID, "",
                  latinOnly, false, null, false, "Algen", false))
            .getContent();
      });
   }

   @Test
   void testFind_AbiesAlba_RightGroup()
   {
      List<TaxonBase> taxa = taxaResourceUT
            .find(0, 10,
                  new TaxaFilter(PortalTestUtil.DEMO_ID, "Abies alba",
                        latinOnly, false, null, false, "Spermatophyta", false))
            .getContent();
      System.out.println(taxa.get(0).getGroup());
      assertThat(taxa.size(), is(1));
   }

   @Test
   void testGet_AbiesGrandis()
   {
      Taxon taxon = taxaResourceUT.getTaxon(PortalTestUtil.DEMO_ID,
            49407).getTaxon();
      assertThat(taxon.getEntityId(), is(49407));
      assertThat(taxon.getName(), is("Abies grandis"));
   }


   @Test
   void testFind_Abies()
   {
      List<TaxonBase> taxa = taxaResourceUT
            .find(0, 10,
                  new TaxaFilter(PortalTestUtil.DEMO_ID, "Abies", latinOnly,
                        false, null, false, null, false))
            .getContent();

      assertThat(taxa.size(), is(5));
   }

   @Test
   void testFind_InclSynonyms()
   {
      List<TaxonBase> taxa = taxaResourceUT
            .find(0, 10,
                  new TaxaFilter(PortalTestUtil.DEMO_ID, "Conyza canadensis",
                        latinOnly,
                        true, null, false, null, false))
            .getContent();

      assertAll(() -> assertThat(taxa.size(), is(1)),
            () -> assertThat(taxa.get(0).isPreferred(), is(false)),
            () -> assertThat(taxa.get(0).getName(), is("Conyza canadensis")));
   }

   @Test
   void testCount_withoutSynonyms()
   {
      long count = taxaResourceUT.count(new TaxaFilter(PortalTestUtil.DEMO_ID,
            "Conyza canadensis", latinOnly, false, null, false, null, false))
            .getAmount();

      assertThat(count, is(0l));
   }

   @Test
   public void testCount_AbiesAlba()
   {

      assertThat(
            taxaResourceUT.count(new TaxaFilter(PortalTestUtil.DEMO_ID,
                  "Abies alba", latinOnly, false, null, false, null, false)).getAmount(),
            is(1l));
   }

   @Test
   void testCount_Abies()
   {
      assertThat(taxaResourceUT.count(new TaxaFilter(PortalTestUtil.DEMO_ID,
            "Abies", latinOnly, false, null, false, null, false)).getAmount(), is(5l));
   }

   @Test
   void testCount_ExistingTaxa()
   {
      assertThat(
            taxaResourceUT.count(new TaxaFilter(PortalTestUtil.FLORA_MV_ID, "",
                  latinOnly, false, null, true, null, false)).getAmount(),
            is(3496l));
   }

   @Test
   void testCount_All_Filtered()
   {
      assertThat(taxaResourceUT.count(new TaxaFilter(PortalTestUtil.FLORA_MV_ID,
            "", latinOnly, false, null, false, null, false)).getAmount(),
            is(8657l));
   }

   @Test
   void testCount_All()
   {
      assertThat(taxaResourceUT.count(new TaxaFilter(PortalTestUtil.DEMO_ID, "",
            latinOnly, false, null, false, null, false)).getAmount(),
            is(14481l));
   }

   @Test
   void testLinks_JSON()
   {
      given().accept("application/json")
            .filters(new RequestLoggingFilter(), new ResponseLoggingFilter())
            .get(createServerUrl() + "/v1/3/taxa?limit=1").then()
            .body("taxon[0].links[0].rel", is("self"))
            .body("taxon[0]", not(containsPath("link")));

   }

   @Test
   void testLinks_XML()
   {
      given().accept("application/xml")
            .filters(new RequestLoggingFilter(), new ResponseLoggingFilter())
            .get(createServerUrl() + "/v1/3/taxa?limit=1").then().assertThat()
            .body("taxonSliceResponse.taxa.size()", is(1))
            .body("taxonSliceResponse.taxa.taxon[0].links.link[0].@rel", // .links[0].rel.text()",
                  is("self"))
            .and().assertThat().body("taxonSliceResponse.taxa.taxon[0]",
                  not(containsPath("link")));

   }

   @Test
   void testFindAllSynonyms()
   {
      TaxonSliceResponse response = taxaResourceUT.findAllSynonyms(4, 11935);

      List<Taxon> taxa = response.getContent();
      taxa.sort((a, b) -> a.getEntityId() - b.getEntityId());

      assertAll(() -> assertThat(taxa, hasSize(2)),
            () -> assertThat(taxa.get(0).getName(), is("Oxalis acetosella")),
            () -> assertThat(taxa.get(0).isPreferred(), is(
                  true)),
            () -> assertThat(taxa.get(1).getName(), is("Wald-Sauerklee")),
            () -> assertThat(taxa.get(1).isPreferred(), is(false)));
   }

   void testFindTaxaWithChangedOcurrencesOrSamples_since2018()
   {
      TaxonBaseSliceResponse response = taxaResourceUT.find(0, 20,
            new TaxaFilter(PortalTestUtil.FLORA_MV_ID, "", null, false,
                  LocalDate.of(2018, 07, 01), false, null, false));
      List<TaxonBase> taxa = response.getContent();

      assertThat(taxa.size(), is(12));
   }

   @Test
   void testFindTaxaWithChangedOcurrencesOrSamples_since2017()
   {
      TaxonBaseSliceResponse response = taxaResourceUT.find(0, 20,
            new TaxaFilter(PortalTestUtil.FLORA_MV_ID, "", null, false,
                  LocalDate.of(2017, 07, 01), false, null, false));
      List<TaxonBase> taxa = response.getContent();

      assertThat(taxa.size(), is(20));
   }

   @Test
   void testGetMTBCoverage()
   {
      Set<MTB> coveredMTBs = taxaResourceUT.getCoveredMTB(
            PortalTestUtil.FLORA_MV_ID, 19265, true)
            .getCoveredMTBs();

      assertThat(coveredMTBs.size(), is(36));
   }

   @Test
   void testGetMTBCoverage_withOutChildren()
   {
      Set<MTB> coveredMTBs = taxaResourceUT
            .getCoveredMTB(PortalTestUtil.FLORA_MV_ID, 1, false)
            .getCoveredMTBs();

      assertThat(coveredMTBs.size(), is(0));
   }

   @Test
   void testGetFieldConfig()
   {
      Set<TaxonFieldConfig> fieldConfigs = taxaResourceUT
            .getFieldConfig(PortalTestUtil.FLORA_MV_ID).getFieldConfigs();
      assertThat(fieldConfigs, hasSize(greaterThan(0)));

      for (TaxonFieldConfig surveyFieldConfig : fieldConfigs)
      {
         assertThat(surveyFieldConfig.getField(), is(not(nullValue())));
         assertThat(
               "The translationKey for the field "
                     + surveyFieldConfig.getField() + " may not be empty",
               surveyFieldConfig.getTranslationKey(),
               is(not(isEmptyOrNullString())));
         assertThat(
               "The property for the field " + surveyFieldConfig.getField()
                     + " may not be empty",
               surveyFieldConfig.getProperty(), is(not(isEmptyOrNullString())));
      }
   }
}
