package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.PeopleFilter;
import org.infinitenature.service.v1.resources.PeopleResource;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.service.v1.types.response.PersonSliceResponse;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.Test;

public class ITestRESTPeopleResource extends RestApiTest
{
   private PeopleResource peopleResourceUT;

   @PostConstruct
   public void setUp()
   {
      disableFeature(WerBeoFeatures.BUILD_OCCURRENCE_INDEX);
      peopleResourceUT = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA).people();
   }

   @Test
   public void testFind_sortASC()
   {
      PersonSliceResponse peopleResponse = peopleResourceUT.find(
            0, 5, PersonField.NAME, SortOrder.ASC,
            new PeopleFilter(PortalTestUtil.FLORA_MV_ID, null, null, null));

      List<Person> people = peopleResponse.getContent();

      assertAll(() -> assertThat(people.size(), is(5)),
            () -> assertThat(people.get(0).getLastName(), is("Abrahami")),
            () -> assertThat(people.get(1).getLastName(), is("Absolem")));
   }

   @Test
   public void testFind_sortDESC()
   {
      PersonSliceResponse peopleResponse = peopleResourceUT.find(
            0, 5, PersonField.NAME, SortOrder.DESC, new PeopleFilter(PortalTestUtil.FLORA_MV_ID, null, null, null));

      List<Person> people = peopleResponse.getContent();

      assertAll(() -> assertThat(people.size(), is(5)),
            () -> assertThat(people.get(0).getLastName(), is("admin")),
            () -> assertThat(people.get(1).getLastName(), is("Yeoman")));
   }

   @Test
   void testSave()
   {
      final String email = UUID.randomUUID().toString() + "hogen@freeze.com";
      Person person = new Person();
      person.setFirstName("Hogen");
      person.setLastName("Freeze");
      person.setExternalKey("externalKey");
      person.setEmail(email);
      int id = (Integer) peopleResourceUT
            .save(PortalTestUtil.FLORA_MV_ID, person).getEntityId();
      Person personDB = peopleResourceUT
            .getPerson(PortalTestUtil.FLORA_MV_ID, id).getPerson();
      assertAll(() -> assertThat(personDB.getFirstName(), is("Hogen")),
            () -> assertThat(personDB.getLastName(), is("Freeze")),
            () -> assertThat(personDB.getEmail(), is(email)),
            () -> assertThat(personDB.getExternalKey(), is("externalKey")));
   }

   @Test
   public void testFindValidators()
   {
      PeopleFilter personFilter = new PeopleFilter(PortalTestUtil.FLORA_MV_ID,
            true);
      PersonSliceResponse result = peopleResourceUT.find(0, 10, PersonField.NAME, SortOrder.ASC,
            personFilter);

      assertAll(() -> assertThat(result.getContent(), hasSize(1)),
            () -> assertThat(result.getContent().get(0).getFirstName(),
                  is("Cthrine")),
            () -> assertThat(result.getContent().get(0).getLastName(),
                  is("Tomala")));
   }

   @Test
   void testFilterByLastName()
   {
      PeopleFilter peopleFilter = new PeopleFilter(PortalTestUtil.FLORA_MV_ID)
            .setLastName("Yeoman");

      PersonSliceResponse result = peopleResourceUT.find(0, 10,
            PersonField.NAME, SortOrder.ASC, peopleFilter);

      assertAll(() -> assertThat(result.getContent(), hasSize(1)),
            () -> assertThat(result.getContent().get(0).getFirstName(),
                  is("Dara")),
            () -> assertThat(result.getContent().get(0).getLastName(),
                  is("Yeoman")));
   }

   @Test
   void testFilterByLastNameCaseDiffer()
   {
      PeopleFilter peopleFilter = new PeopleFilter(PortalTestUtil.FLORA_MV_ID)
            .setLastName("yeoman");

      PersonSliceResponse result = peopleResourceUT.find(0, 10,
            PersonField.NAME, SortOrder.ASC, peopleFilter);

      assertAll(() -> assertThat(result.getContent(), hasSize(1)),
            () -> assertThat(result.getContent().get(0).getFirstName(),
                  is("Dara")),
            () -> assertThat(result.getContent().get(0).getLastName(),
                  is("Yeoman")));
   }

   @Test
   void testFilterByFirstName()
   {
      PeopleFilter peopleFilter = new PeopleFilter(PortalTestUtil.FLORA_MV_ID)
            .setFirstName("Dara");

      PersonSliceResponse result = peopleResourceUT.find(0, 10,
            PersonField.NAME, SortOrder.ASC, peopleFilter);

      assertAll(() -> assertThat(result.getContent(), hasSize(1)),
            () -> assertThat(result.getContent().get(0).getFirstName(),
                  is("Dara")),
            () -> assertThat(result.getContent().get(0).getLastName(),
                  is("Yeoman")));
   }

   @Test
   void testFilterByFirstNameCaseDiffer()
   {
      PeopleFilter peopleFilter = new PeopleFilter(PortalTestUtil.FLORA_MV_ID)
            .setFirstName("dara");

      PersonSliceResponse result = peopleResourceUT.find(0, 10,
            PersonField.NAME, SortOrder.ASC, peopleFilter);

      assertAll(() -> assertThat(result.getContent(), hasSize(1)),
            () -> assertThat(result.getContent().get(0).getFirstName(),
                  is("Dara")),
            () -> assertThat(result.getContent().get(0).getLastName(),
                  is("Yeoman")));
   }

   @Test
   void testFilterByExternalKey()
   {
      PeopleFilter peopleFilter = new PeopleFilter(PortalTestUtil.FLORA_MV_ID)
            .setExternalKey("p1");

      PersonSliceResponse result = peopleResourceUT.find(0, 10,
            PersonField.NAME, SortOrder.ASC, peopleFilter);

   }
}
