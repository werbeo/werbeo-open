package org.infinitenature.werbeo.service.mapper;

import java.util.Collections;
import java.util.UUID;

import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.adapters.PortalQueriesAdapter;
import org.infinitenature.werbeo.service.core.api.enity.Person;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Import(org.infinitenature.werbeo.ServiceApp.class)
public abstract class IntegrationTest
{
   protected User getRandomUser()
   {
      String uuid = "Daba" + UUID.randomUUID().toString(); // sorts somewhere in
                                                           // the middle
      Person person = new Person(uuid, uuid);
      User user = new User();
      user.setLogin(uuid + "@" + uuid + ".com");
      user.setPerson(person);
      return user;
   }
   protected User getUser(String login)
   {
      switch (login)
      {
      case UserTestUtil.USER_LOGIN_A:
         Person person = new Person("Cthrine", "Tomala");
         User user = new User();
         user.setLogin(UserTestUtil.USER_LOGIN_A);
         user.setPerson(person);
         return user;
      case UserTestUtil.USER_LOGIN_B:
         Person personB = new Person("Sarge", "Ellins");
         User userB = new User();
         userB.setLogin(UserTestUtil.USER_LOGIN_B);
         userB.setPerson(personB);
         return userB;
      default:
         throw new IllegalArgumentException("Unknown user login " + login);
      }

   }

   @Autowired
   private PortalQueriesAdapter portalQueriesAdapter;

   protected Context createContext(int portalId, User user)
   {
      return new Context(portalQueriesAdapter.load(portalId), user,
            Collections.emptySet(), "test");
   }
}
