package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.infinitenature.service.v1.resources.OccurrenceMediaResource;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.response.MediaResponse;
import org.infinitenature.service.v1.types.support.MediaUpload;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.junit.jupiter.api.Test;

public class ITestRESTOccurrcenceMediaResource extends AbstractITestREST
{
   private OccurrenceMediaResource occurrenceMediaResourceUT;

   @PostConstruct
   @Override
   protected void setUp()
   {
      super.setUp();
      occurrenceMediaResourceUT = client.occurrenceMedia();
   }

   @Test
   public void testAttachImageJPEG() throws IOException
   {
      String fileName = "nettle-4215279_1920.jpg";
      String description = "A pricture";

      List<Document> documents = uploadMedia(fileName, description);
      assertThat(documents.size(), is(2));

      Document image = documents.stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE).findFirst()
            .get();
      Document thumbnail = documents.stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE_THUMB)
            .findFirst().get();

      assertThat(image.getLink().getHref(),
            endsWith(fileName));
      assertThat(thumbnail.getLink().getHref(),
            endsWith(fileName));
      assertThat(thumbnail.getLink().getHref(), containsString("thumb"));
   }

   @Test
   public void testAttachImagePNG() throws IOException
   {
      String fileName = "nettle-4215279_1920.png";
      String description = "A pricture";

      List<Document> documents = uploadMedia(fileName, description);
      assertThat(documents.size(), is(2));

      Document image = documents.stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE).findFirst()
            .get();
      Document thumbnail = documents.stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE_THUMB)
            .findFirst().get();

      assertThat(image.getLink().getHref(),
            endsWith(fileName));
      assertThat(thumbnail.getLink().getHref(),
            endsWith(fileName));
      assertThat(thumbnail.getLink().getHref(), containsString("thumb"));
   }

   @Test
   public void testAttachAudioWAV() throws IOException
   {
      String fileName = "456440__inspectorj__bird-whistling-robin-single-13.wav";
      String description = "A sound";

      List<Document> documents = uploadMedia(fileName, description);

      assertThat(documents.size(), is(1));

      Document audio = documents.stream()
            .filter(doc -> doc.getType() == DocumentType.AUDIO).findFirst()
            .get();

      assertThat(audio.getLink().getHref(), endsWith(fileName));
      assertThat(audio.getCaption(), is(description));
   }

   @Test
   public void testAttachAudioMP3() throws IOException
   {
      String fileName = "456440__inspectorj__bird-whistling-robin-single-13.mp3";
      String description = "A sound";

      List<Document> documents = uploadMedia(fileName, description);

      assertThat(documents.size(), is(1));

      Document audio = documents.stream()
            .filter(doc -> doc.getType() == DocumentType.AUDIO).findFirst()
            .get();

      assertThat(audio.getLink().getHref(), endsWith(fileName));
      assertThat(audio.getCaption(), is(description));
   }

   private List<Document> uploadMedia(String fileName, String description)
         throws IOException
   {
      UUID sampleID = UUID.randomUUID();

      Sample sample = createCompleSample();

      sample.setEntityId(sampleID);

      UUID savedID = UUID.fromString((String) client.samples()
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(savedID, is(sampleID));

      Sample loaded = client.samples().get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      UUID occurrenceId = loaded.getOccurrences().get(0).getEntityId();

      byte[] data = IOUtils
            .toByteArray(getClass().getResourceAsStream("/" + fileName));
      String base64Encoded = Base64.getEncoder().encodeToString(data);

      MediaResponse response = occurrenceMediaResourceUT.attachMedia(
            PortalTestUtil.DEMO_ID, occurrenceId,
            new MediaUpload(fileName, description, base64Encoded));

      List<Document> documents = occurrenceResource
            .get(PortalTestUtil.DEMO_ID, occurrenceId).getOccurrence()
            .getDocuments();
      return documents;
   }
}
