package org.infinitenature.werbeo.service.support;

import java.io.IOException;
import java.util.UUID;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.infinitenature.service.v1.resources.security.TokenResource;
import org.infinitenature.service.v1.types.support.UserAuth;
import org.infinitenature.werbeo.ServiceApp;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.repository.FeatureState;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(ServiceApp.class)
abstract public class RestApiTest
{
   public static final String AUTHORIZATION_HEADER = "Authorization";
   public static final String WERBEO_REQUEST_SOURCE_ID = "werbeo-request-source-id";

   private String serverHost = "localhost";
   private String serverProto = "http";

   @Autowired
   private Environment environment;

   @Autowired
   private TokenResource tokenResurce;

   @Autowired
   private FeatureManager fm;

   public void disableFeature(WerBeoFeatures werbeoFeature)
   {
      fm.setFeatureState(new FeatureState(werbeoFeature, false));
   }

   public void enableFeature(WerBeoFeatures werBeoFeatures)
   {
      fm.setFeatureState(new FeatureState(werBeoFeatures, true));
   }
   public String createServerUrl()
   {
      return serverProto + "://" + serverHost + ":" + getServerPort() + "/api";
   }

   public String getServerPort()
   {
      return environment.getProperty("local.server.port");
   }

   protected Werbeo createClient()
   {
      return new Werbeo(createServerUrl());
   }

   protected Werbeo createClient(String email, String password)
   {
      UserAuth userAuth = new UserAuth();
      userAuth.setEmail(email);
      userAuth.setPassword(password);

      String token = tokenResurce.getToken(userAuth).getToken()
            .getAccessToken();

      return new Werbeo(createServerUrl(), new ClientRequestFilter()
      {
         @Override
         public void filter(ClientRequestContext requestContext)
               throws IOException
         {
            requestContext.getHeaders().add(AUTHORIZATION_HEADER,
                  "Bearer " + token);
            requestContext.getHeaders().add(WERBEO_REQUEST_SOURCE_ID,
                  "i-test-" + UUID.randomUUID());
         }
      });
   }


}
