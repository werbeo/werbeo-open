package org.infinitenature.werbeo.service.v1.resource;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(properties = {
      "werbeo.service.use-taxon-cache=true" }, webEnvironment = WebEnvironment.RANDOM_PORT)
public class ITestRESTTaxonResourceWithCache extends ITestRESTTaxonResource
{
   // run the tests with active taxon cache
}
