package org.infinitenature.werbeo.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

class TestRestMapper
{

   private RestMapper<TaxonBase, TaxaResource> mapperUT;

   @BeforeEach
   void setUp() throws Exception
   {
      mapperUT = new RestMapper<>(TaxaResource.class);
      mapperUT.setLinkMapper(new LinkMapperRestImpl());
   }

   @Test
   void test()
   {
      HttpServletRequest request = mock(HttpServletRequest.class);
      when(request.getRequestURI())
            .thenReturn("http://api.test/bla");
      when(request.getContextPath()).thenReturn("/bla");
      when(request.getServletPath()).thenReturn("/");
      ServletRequestAttributes sra = new ServletRequestAttributes(request);
      RequestContextHolder.setRequestAttributes(sra);
      TaxonBase taxon = new TaxonBase();
      taxon.setId(22);
      Context context = new Context(new Portal(33), User.INTERNAL_SUPER_USER,
            Collections.emptySet(), "test");
      List<Link> links = mapperUT.createLinks(taxon, context);

      assertThat(links.get(0).getHref(), is("/bla/v1/33/taxa/22"));
   }

}
