package org.infinitenature.werbeo.service.v1.resource;

import java.time.LocalDate;
import java.util.Collections;
import java.util.UUID;

import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.RecordStatus;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.VagueDate.VagueDateType;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.infinitenature.werbeo.service.support.RestApiTest;

public abstract class AbstractITestREST extends RestApiTest
{

   protected OccurrenceResource occurrenceResource;
   protected TaxaResource taxaResource;
   protected Werbeo client;

   public AbstractITestREST()
   {
      super();
   }

   protected void setUp()
   {
      disableFeature(WerBeoFeatures.BUILD_OCCURRENCE_INDEX);
      client = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA);
      occurrenceResource = client.occurrences();
      taxaResource = client.taxa();
   }

   protected Sample createSample()
   {
      Sample sample = new Sample();

      UUID sampleID = UUID.randomUUID();

      sample.setEntityId(sampleID);

      Position position = new Position();
      position.setType(PositionType.MTB);
      position.setMtb(new MTB("1943/4"));

      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(position);

      sample.getLocality().setBlur(4);

      VagueDate date = createTodayMinus(5);
      sample.setDate(date);

      SurveyBase survey = createDemoSurvey();
      sample.setSurvey(survey);

      Person person = createPerson();
      sample.setRecorder(person);
      return sample;
   }

   protected Occurrence occurrence(Person person, TaxonBase taxon)
   {
      Occurrence occurrence = new Occurrence();
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setDeterminer(person);
      occurrence.setTaxon(taxon);
      return occurrence;
   }

   protected Sample createCompleSample()
   {
      Sample sample = new Sample();

      Position position = new Position();
      position.setType(PositionType.MTB);
      position.setMtb(new MTB("1943/4"));

      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(position);

      sample.getLocality().setBlur(4);

      VagueDate date = createTodayMinus(5);
      sample.setDate(date);

      SurveyBase survey = createDemoSurvey();
      sample.setSurvey(survey);

      Person person = createPerson();
      sample.setRecorder(person);

      Occurrence occurrence = new Occurrence();
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setDeterminer(person);

      TaxonBase urtica = utritca();
      occurrence.setTaxon(urtica);
      sample.getOccurrences().add(occurrence);
      return sample;
   }

   protected VagueDate createTodayMinus(int days)
   {
      VagueDate date = new VagueDate();
      date.setFrom(LocalDate.now().minusDays(days));
      date.setType(VagueDateType.DAY);
      return date;
   }

   protected SurveyBase createDemoSurvey()
   {
      SurveyBase survey = new SurveyBase();
      survey.setEntityId(1);
      return survey;
   }

   protected Person createPerson()
   {
      Person person = new Person();
      person.setEntityId(4);
      return person;
   }

   protected Person createPersonWithoutId()
   {
      Person person = new Person();
      person.setFirstName("firstName");
      person.setLastName("LastName");
      person.setEmail("mail@domain.com");
      return person;
   }

   protected TaxonBase chorthippusPullus()
   {
      TaxonBase chorthippusPullus = taxaResource
            .find(0, 1,
                  new TaxaFilter(PortalTestUtil.HEUSCHRECKEN_D_ID,
                        "Chorthippus pullus",
                        Collections.emptySet(), false, null, false, null, false))
            .getContent().get(0);

      return chorthippusPullus;
   }

   protected TaxonBase abiesAlba()
   {
      TaxonBase abiesAlba = taxaResource
            .find(0, 1,
                  new TaxaFilter(PortalTestUtil.DEMO_ID, "Abies alba",
                        Collections.emptySet(), false, null, false, null, false))
            .getContent().get(0);

      return abiesAlba;
   }

   protected TaxonBase utritca()
   {
      TaxonBase urtica = new TaxonBase();
      urtica.setEntityId(34364);
      return urtica;
   }

}