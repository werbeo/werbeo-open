package org.infinitenature.werbeo.service.support;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.infinitenature.service.v1.types.support.UserAuth;
import org.infinitenature.werbeo.client.Werbeo;

public class WerbeoClientFactory
{
   public static final String AUTHORIZATION_HEADER = "Authorization";

   private String serverHost = "localhost";
   private String serverProto = "http";
   private String serverPort = "8087";

   public String createServerUrl()
   {
      return serverProto + "://" + serverHost + ":" + serverPort + "/api";
   }

   public Werbeo createClient()
   {
      return new Werbeo(createServerUrl());
   }

   public Werbeo createClient(String email, String password)
   {
      UserAuth userAuth = new UserAuth();
      userAuth.setEmail(email);
      userAuth.setPassword(password);

      String token = createClient().token().getToken(userAuth).getToken()
            .getAccessToken();

      return new Werbeo(createServerUrl(), new ClientRequestFilter()
      {
         @Override
         public void filter(ClientRequestContext requestContext)
               throws IOException
         {
            requestContext.getHeaders().add(AUTHORIZATION_HEADER,
                  "Bearer " + token);
         }
      });
   }

   public String getServerHost()
   {
      return serverHost;
   }

   public void setServerHost(String serverHost)
   {
      this.serverHost = serverHost;
   }

   public String getServerProto()
   {
      return serverProto;
   }

   public void setServerProto(String serverProto)
   {
      this.serverProto = serverProto;
   }

   public String getServerPort()
   {
      return serverPort;
   }

   public void setServerPort(String serverPort)
   {
      this.serverPort = serverPort;
   }

}
