package org.infinitenature.werbeo.service.v1.resource.performance;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.apache.commons.lang3.time.StopWatch;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.support.WerbeoClientFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;

public class MTestRESTOccurrencePerformance
{
   private static final OccurrenceField SORT_FIELD = OccurrenceField.MOD_DATE;
   private static final String PASSWORD = UserTestUtil.PASSWORD_CTOMALAA;
   private static final String LOGIN = UserTestUtil.LOGIN_CTOMALAA;
   private static final int WEBSITE_ID = PortalTestUtil.FLORA_MV_ID;
   private static final String LAST_REQUEST_TOOK = "last request took: {}ms";
   private static final Logger LOGGER = LoggerFactory
         .getLogger(MTestRESTOccurrencePerformance.class);

   private final static int OFFSET_OFFSET = 0;

   public static void setLoggingLevel(Level level)
   {
      ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory
            .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
      root.setLevel(level);
   }

   @BeforeAll
   public static void setinit()
   {
      setLoggingLevel(Level.INFO);

   }

   public OccurrenceResource createResource()
   {
      WerbeoClientFactory clientFacotry = new WerbeoClientFactory();
      return clientFacotry.createClient(LOGIN, PASSWORD).occurrences();
   }

   @BeforeEach
   public void warmUp()
   {
      OccurrenceResource occurrenceResourceUT = createResource();
      LOGGER.info("Start warming up");
      int i = 0;
      while (i < 5)
      {
         occurrenceResourceUT.find(0, 50, SORT_FIELD, SortOrder.ASC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null,
                     null,null, null, true, null, null, false, null, null, null, null), false);
         OccurrenceSliceResponse find = occurrenceResourceUT.find(100, 50, OccurrenceField.DATE,
               SortOrder.DESC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null,null, null,
                     true,
                     null, null, false, null, null, null, null), false);
         assertThat(find.getContent().size(), is(50));
         i++;
      }
      LOGGER.info("Stop warming up");
   }

   /**
    * The occurrence table access the data in this way:
    * <ol>
    * <li>load 40 items</li>
    * <li>on scrolling load the next 10</li>
    * <li>on jumping load 90</li>
    * </ol>
    *
    * Sorting has these affects:
    *
    * <ol>
    * <li>load 90 items</li>
    * <li>on the begin or end load 50 items</li>
    * </ol>
    */
   @Test
   public void testFind()
   {
      OccurrenceResource occurrenceResourceUT = createResource();
      int i = 0;
      while (i < 5)
      {
         LOGGER.info("Iteration {}", i);
         i++;
         StopWatch holeRound = new StopWatch();
         holeRound.start();
         StopWatch stopWatch = new StopWatch();
         stopWatch.start();

         Integer maxBlur = null;
         // Initial load
         occurrenceResourceUT.find(0 + OFFSET_OFFSET, 40, SORT_FIELD,
               SortOrder.ASC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null,null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());
         stopWatch.reset();
         stopWatch.start();
         // scroll
         occurrenceResourceUT.find(40 + OFFSET_OFFSET, 10, SORT_FIELD,
               SortOrder.ASC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null,null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());
         stopWatch.reset();
         stopWatch.start();

         occurrenceResourceUT.find(50 + OFFSET_OFFSET, 10, SORT_FIELD,
               SortOrder.ASC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null,null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());
         stopWatch.reset();
         stopWatch.start();

         // jump
         occurrenceResourceUT.find(150 + OFFSET_OFFSET, 90, SORT_FIELD,
               SortOrder.ASC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null, null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());
         stopWatch.reset();
         stopWatch.start();

         // jump back
         occurrenceResourceUT.find(50 + OFFSET_OFFSET, 90, SORT_FIELD,
               SortOrder.ASC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null, null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());
         stopWatch.reset();
         stopWatch.start();

         // change sort order
         occurrenceResourceUT.find(50 + OFFSET_OFFSET, 90, SORT_FIELD,
               SortOrder.DESC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null, null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());
         stopWatch.reset();
         stopWatch.start();

         // go to the beginning
         occurrenceResourceUT.find(0 + OFFSET_OFFSET, 90, SORT_FIELD,
               SortOrder.DESC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null, null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());
         stopWatch.reset();
         stopWatch.start();

         // change sort
         occurrenceResourceUT.find(0 + OFFSET_OFFSET, 50, OccurrenceField.DATE,
               SortOrder.ASC,
               new OccurrenceFilter(WEBSITE_ID, null, null, null, null,null, maxBlur,
                     true,
                     null, null, false, null, null, null, null), false);
         stopWatch.stop();
         LOGGER.info(LAST_REQUEST_TOOK, stopWatch.getTime());

         holeRound.stop();
         LOGGER.info("hole round took {}ms", holeRound.getTime());
      }
   }
}
