package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.NotFoundException;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.PortalField;
import org.infinitenature.service.v1.types.enums.Filter;
import org.infinitenature.service.v1.types.response.FilterResponse;
import org.infinitenature.service.v1.types.response.PortalBaseResponse;
import org.infinitenature.service.v1.types.response.PortalConfigResponse;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.Test;

public class ITestRESTPortalResource extends RestApiTest
{
   private PortalResource portalResourceUT;

   @PostConstruct
   public void setUp()
   {
      portalResourceUT = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA).portals();
   }

   @Test
   public void testLoadPortal()
   {
      PortalBaseResponse portalBaseResponse = portalResourceUT.getPortal(1);

      assertThat(portalBaseResponse.getPortal().getName(),
            is("Demonstration Website"));
   }

   @Test
   public void testLoadPortalNoExisting()
   {
      assertThrows(NotFoundException.class,
            () -> portalResourceUT.getPortal(77));


   }

   @Test
   public void testLoadFilter()
   {
      FilterResponse filterResponse = portalResourceUT
            .getFilter(PortalTestUtil.REST_SRV_ID);
      Map<Filter, Collection<PortalBase>> filters = filterResponse.getFilters();

      assertThat(filters.size(), is(1));

      assertThat(filters.get(Filter.ONLY_OWN_DATA).size(), is(1));
   }

   @Test
   public void testLoadFilterAll()
   {
      FilterResponse filterResponse = createClient(UserTestUtil.LOGIN_NO_TC,
            UserTestUtil.PASSWORD_NO_TC).portals()
                  .getFilter(PortalTestUtil.REST_SRV_ID);

      Map<Filter, Collection<PortalBase>> filters = filterResponse.getFilters();

      assertThat(filters.size(), is(1));

      assertThat(filters.get(Filter.ONLY_OWN_DATA).size(), is(3));
   }

   @Test
   public void testLoadConfig_FloraMV()
   {
      PortalConfigResponse response = portalResourceUT
            .getPortalConfiguration(PortalTestUtil.FLORA_MV_ID);

      assertThat(
            response.getPortalConfiguration().getDefaultDataEntrySurveyId(),
            is(7));
      assertThat(response.getPortalConfiguration().getMaxUploadSize(), is(3));
   }

   @Test
   public void testLoadConfig_FloraMV_anon()
   {
      PortalConfigResponse response = createClient().portals()
            .getPortalConfiguration(PortalTestUtil.FLORA_MV_ID);

      assertThat(
            response.getPortalConfiguration().getDefaultDataEntrySurveyId(),
            is(7));
   }

   @Test
   public void testLoadConfig_NotExisting()
   {
      assertThrows(NotFoundException.class,
            () -> portalResourceUT.getPortalConfiguration(77));
   }

   @Test
   public void testCount()
   {
      long count = portalResourceUT.count().getAmount();

      assertThat(count, is(11L));
   }

   @Test
   public void testFind()
   {
      List<PortalBase> portals = portalResourceUT
            .findPortals(0, 5, PortalField.ID, SortOrder.ASC).getContent();

      assertThat(portals, hasSize(5));

      assertThat(portals.get(3).getEntityId(), is(4));
      assertThat(portals.get(3).getName(), is("flora-mv"));
   }
}