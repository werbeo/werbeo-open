package org.infinitenature.werbeo.service.v1.resources;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.infinitenature.service.v1.resources.InfoResource;
import org.infinitenature.service.v1.types.ServiceInfo;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.enity.support.PortalConfiguration;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.infinitenature.werbeo.service.util.ContextFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestInfoResourceImpl
{
   private InfoResource resourceUT;

   @BeforeEach
   void setUp()
   {
      ContextFactory contextFacotry = mock(ContextFactory.class);
      Context context = new Context(new Portal(), User.INTERNAL_SUPER_USER,
            Collections.emptySet(), new PortalConfiguration(), "requestID");
      when(contextFacotry.createAnonymousContext(any())).thenReturn(context);
      resourceUT = new InfoResourceImpl(contextFacotry);
   }

   @Test
   void testGetServiceInfo()
   {
      ServiceInfo serviceInfo = resourceUT.getServiceInfo().getServiceInfo();

      assertThat(serviceInfo.getBuildDate(), is(not(emptyOrNullString())));
      assertThat(serviceInfo.getVersion(), is(not(emptyOrNullString())));
   }

}
