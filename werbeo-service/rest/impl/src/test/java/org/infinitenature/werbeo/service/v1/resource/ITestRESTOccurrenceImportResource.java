package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceImportResource;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.JobStatus;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.Test;

public class ITestRESTOccurrenceImportResource extends RestApiTest
{
   private OccurrenceImportResource importResourceUT;
   private Werbeo werbeo;

   @PostConstruct
   public void setUp()
   {
      werbeo = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA);
      importResourceUT = werbeo.occurrenceImports();

   }
   
   @Test
   public void testImport() throws Exception
   {

      String response = importResourceUT.doImportXml(PortalTestUtil.DEMO_ID,
            FileUtils.readFileToString(
                  new File("src/test/resources/NewFile.xml"), "utf-8"));
      UUID importId = UUID.fromString(response);

      int triesToDo = 120_000;

      int msToWait = triesToDo;
      while (msToWait > 0)
      {

         JobStatus status = importResourceUT
               .getJobStatus(PortalTestUtil.DEMO_ID, importId);
         assertThat(status, is(not(JobStatus.FAILURE)));
         if (status != JobStatus.INITIALIZED && status != JobStatus.RUNNING)
         {
            break;
         }
         Thread.sleep(10_000);
         triesToDo = msToWait - 10_000;
      }
      Occurrence occurrence = werbeo.occurrences()
            .get(PortalTestUtil.DEMO_ID,
                  UUID.fromString("12345678-1234-1234-1234-123456789023"))
            .getOccurrence();

      assertThat(occurrence.getExternalKey(), is("ID-34306"));

      assertThat(occurrence.getSample().getLocality().getLocality(),
            is("A position description"));

      assertThat(werbeo.occurrenceComments()
            .countComments(PortalTestUtil.DEMO_ID,
                  UUID.fromString("12345678-1234-1234-1234-123456789023"))
            .getAmount(), is(1l));

      List<OccurrenceComment> occurrenceComments = werbeo.occurrenceComments()
            .getComments(PortalTestUtil.DEMO_ID,
                  UUID.fromString("12345678-1234-1234-1234-123456789023"), 0,
                  100, CommentField.ID, SortOrder.ASC)
            .getContent();
      assertThat(occurrenceComments, hasSize(1));
      assertThat(occurrenceComments.get(0).getComment(),
            is("Ein Kommentar an der Beobachtung"));

      assertThat(werbeo.sampleComments()
            .countComments(PortalTestUtil.DEMO_ID,
                  UUID.fromString("12345678-1234-1234-1234-123456789022"))
            .getAmount(), is(2l));

      List<SampleComment> sampleComments = werbeo.sampleComments()
            .getComments(PortalTestUtil.DEMO_ID,
                  UUID.fromString("12345678-1234-1234-1234-123456789022"), 0,
                  100, CommentField.MOD_DATE, SortOrder.ASC)
            .getContent();

      assertThat(sampleComments.size(), is(2));
      assertThat(sampleComments.get(0).getComment(),
            is("Precision is guessed."));
      assertThat(sampleComments.get(1).getComment(), is("Ein Kommentar"));

      occurrence = werbeo.occurrences()
            .get(PortalTestUtil.DEMO_ID,
                  UUID.fromString("12345678-1234-1234-1234-123456789013"))
            .getOccurrence();
      assertThat(occurrence.getSample().getCreatedBy(),
            is("newuser@mymail.de"));
   }
   
   
//   @Test
//   void testImportCSV() throws Exception
//   {
//      werbeo.occurrenceImports().doImportCsv(10,
//            FileUtils.readFileToString(
//                  new File("src/test/resources/daten_sachsen.csv"),
//                  Charset.forName("UTF-8")));
//      
//      
//      while(true)
//      {
//         Thread.sleep(60000);
//      }
//   }
}
