package org.infinitenature.werbeo.service.v1.resource;

import static org.exparity.hamcrest.date.LocalDateTimeMatchers.within;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.infinitenature.werbe.test.support.SampleTestUtil.UNKNOWN_SAMPLE_ID;
import static org.infinitenature.werbe.test.support.SampleTestUtil.VALID_SAMPLE_ID_0;
import static org.infinitenature.werbe.test.support.SampleTestUtil.VALID_SAMPLE_ID_1;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.types.CommentField;
import org.infinitenature.service.v1.types.Herbarium;
import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.Area;
import org.infinitenature.service.v1.types.Occurrence.BloomingSprouts;
import org.infinitenature.service.v1.types.Occurrence.NumericAmountAccuracy;
import org.infinitenature.service.v1.types.Occurrence.Quantity;
import org.infinitenature.service.v1.types.Occurrence.RecordStatus;
import org.infinitenature.service.v1.types.Occurrence.Reproduction;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatus;
import org.infinitenature.service.v1.types.Occurrence.Sex;
import org.infinitenature.service.v1.types.Occurrence.Vitality;
import org.infinitenature.service.v1.types.OccurrenceComment;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SampleBase.SampleMethod;
import org.infinitenature.service.v1.types.SampleComment;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.VagueDate.VagueDateType;
import org.infinitenature.service.v1.types.enums.Operation;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.SampleSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ITestRESTSampleResource extends AbstractITestREST
{
   private SamplesResource samplesResourceUT;

   @Override
   @PostConstruct
   public void setUp()
   {
      super.setUp();
      samplesResourceUT = client.samples();
   }

   @Test
   void testGet_media()
   {
      Sample sample = samplesResourceUT.get(PortalTestUtil.FLORA_MV_ID, UUID.fromString("91b746ba-e45a-40f2-8473-9dc1e3060961")).getSample();
      Occurrence urtica = sample.getOccurrences().stream()
            .filter(o -> o.getEntityId().equals(
                  UUID.fromString("4285b9e6-59c8-4c4c-af36-14762592384f")))
            .findFirst().get();

      assertThat(urtica.getDocuments().size(), is(2));
   }

   @Test
   void testGet_anon_MV()
   {
      SamplesResource anon = createClient().samples();

      Sample sample = anon.get(PortalTestUtil.FLORA_MV_ID, VALID_SAMPLE_ID_0)
            .getSample();
      Occurrence occurrence = sample.getOccurrences().get(0);

      assertAll(
            () -> assertThat("The sample must be obfuscated",
                  sample.isObfuscated(), is(true)),
            () -> assertThat("The sample must have 9 occurrences",
                  sample.getOccurrences(), hasSize(9)),
            () -> assertThat(
                  "The occurrence must have a sample",
                  occurrence.getSample(), is(nullValue())));

   }

   @Test
   void testGet_anon_BB()
   {
      SamplesResource anon = createClient().samples();
      assertThrows(ForbiddenException.class,
            () -> anon.get(PortalTestUtil.FLORA_BB_ID, VALID_SAMPLE_ID_0));
   }

   @Test
   void testGet_anon_invalid()
   {
      SamplesResource anon = createClient().samples();

      assertThrows(NotFoundException.class,
            () -> anon.get(PortalTestUtil.FLORA_MV_ID, UNKNOWN_SAMPLE_ID));
   }

   @Test
   void testGet()
   {
      Sample sample = samplesResourceUT
            .get(PortalTestUtil.FLORA_MV_ID, VALID_SAMPLE_ID_0).getSample();

      VagueDate date = sample.getDate();
      assertThat(sample.getEntityId(), is(VALID_SAMPLE_ID_0));

      assertThat(sample.getCreatedBy(), is(UserTestUtil.LOGIN_CTOMALAA));

      assertThat(sample.getCreationDate(),
            is(LocalDateTime.of(2018, 4, 13, 12, 04, 24)));


      assertThat(date.getFrom(), is(LocalDate.of(2009, 12, 1)));
      assertThat(date.getTo(), is(LocalDate.of(2009, 12, 31)));
      assertThat(date.getType(), is(VagueDateType.MONTH_IN_YEAR));

      Position position = sample.getLocality().getPosition();
      assertThat(position.getType(), is(Position.PositionType.MTB));
      assertThat(position.getMtb().getMtb(), is("1644/1"));
      assertThat(position.getEpsg(), is(4314));
      assertThat(position.getWkt(), is(
            "POLYGON((13.0 54.35,13.083333333333334 54.35,13.083333333333334 54.4,13.0 54.4,13.0 54.35))"));

      assertThat(sample.getLocality().getBlur(), is(200));

      Person recorder = sample.getRecorder();
      assertThat(recorder.getFirstName(), is("Antoine"));
      assertThat(recorder.getLastName(), is("Dudin"));
      assertThat(recorder.getEntityId(), is(42));

      SurveyBase survey = sample.getSurvey();
      assertThat(survey.getName(), is("Rasterkartierung"));

      assertThat(sample.getOccurrences(), hasSize(9));

      Occurrence occurrence = sample.getOccurrences().stream()
            .filter(occ -> occ.getEntityId().equals(
                  UUID.fromString("b9425285-89e2-4cfe-a136-f291c8794e61")))
            .findFirst().get();

      assertThat(occurrence.getTaxon().getName(), is("Nasturtium"));
      assertThat(occurrence.getDeterminer().getFirstName(), is("Garey"));
      assertThat(occurrence.getDeterminer().getLastName(), is("Rosendorf"));
      assertThat(occurrence.getRecordStatus(), is(RecordStatus.COMPLETE));
   }

   @Test
   void testFind()
   {
      SampleSliceResponse response = samplesResourceUT.find(0, 10,
            OccurrenceField.DETERMINER, SortOrder.ASC,
            new OccurrenceFilter(4, null, null, null, null, null, null, true, null,
                  null, false, null, null, null, null), false);

      response.getContent().forEach(sample -> System.out.println(sample.getEntityId()));

      long count = samplesResourceUT.count(
            new OccurrenceFilter(4, null, null, null, null, null, null, true, null,
                  null, false, null, null, null, null)).getAmount();
      System.out.println("Count: " + count);
   }

   @Test
   void testToString()
   {
      Sample sample = samplesResourceUT.get(4, VALID_SAMPLE_ID_0).getSample();

      assertThat(sample.toString(), is(not(nullValue())));
   }

   @Test
   void testHashCode()
   {
      Sample sampleA = samplesResourceUT.get(4, VALID_SAMPLE_ID_0).getSample();

      assertThat(sampleA.hashCode(), is(not(nullValue())));
   }

   @Test
   @Disabled("because of https://git.loe.auf.uni-rostock.de/werbeo/werbeo/commit/651576e2c4be7be346c459b776138eb8306d1fed#e09c1623b29f16df3a8d0119421a05da4559c083_248_237")
   void testEquals_differentInstances()
   {
      Sample sampleA = samplesResourceUT.get(4, VALID_SAMPLE_ID_0).getSample();
      Sample sampleAagain = samplesResourceUT.get(4, VALID_SAMPLE_ID_0)
            .getSample();

      assertThat(sampleA.equals(sampleAagain), is(true));
      assertThat(sampleAagain.equals(sampleA), is(true));
   }

   @Test
   void testEquals_differentSamples()
   {
      Sample sampleA = samplesResourceUT.get(4, VALID_SAMPLE_ID_0).getSample();
      Sample sampleB = samplesResourceUT.get(4, VALID_SAMPLE_ID_1).getSample();

      assertThat(sampleA.equals(sampleB), is(false));
      assertThat(sampleB.equals(sampleA), is(false));
   }

   @Test
   void testEdit_changeTaxa()
   {

      Sample sample = createSample();

      sample.getOccurrences().add(occurrence(createPerson(), utritca()));

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      loaded.getOccurrences().get(0).setTaxon(abiesAlba());
      samplesResourceUT.save(PortalTestUtil.DEMO_ID, loaded);

      loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertThat(loaded.getOccurrences().get(0).getTaxon().getName(),
            is("Abies alba"));

   }

   @Test
   void testEdit_changePosition()
   {

      Sample sample = createSample();

      sample.getOccurrences().add(occurrence(createPerson(), utritca()));

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertThat(loaded.getLocality().getPosition().getType(),
            is(PositionType.MTB));

      Position position = new Position();
      position.setType(PositionType.POINT);
      position.setWkt("POINT (4530300.0 5979200.0)");
      position.setWktEpsg(31468);
      loaded.getLocality().setPosition(position);
      loaded.getLocality().setBlur(50);

      samplesResourceUT.save(PortalTestUtil.DEMO_ID, loaded);

      Sample changed = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertAll(() -> assertThat(changed.getLocality().getBlur(), is(50)),
            () -> assertThat(changed.getLocality().getPosition().getType(),
                  is(PositionType.POINT)),
            () -> assertThat(changed.getLocality().getPosition().getWkt(),
                  is("POINT(4530300.0 5979200.0)")),
            () -> assertThat(changed.getLocality().getPosition().getWktEpsg(),
                  is(31468)));

   }

   @Test
   void testEdit_removeOccurrence()
   {
      Sample sample = createSample();

      sample.getOccurrences().add(occurrence(createPerson(), utritca()));

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(savedID, is(sample.getEntityId()));

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertThat(loaded.getOccurrences().size(), is(1));

      // add 2nd taxon
      loaded.getOccurrences().add(occurrence(createPerson(), abiesAlba()));
      samplesResourceUT.save(PortalTestUtil.DEMO_ID, loaded);
      loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();
      assertThat(loaded.getOccurrences().size(), is(2));

      // delete one taxon

      loaded.getOccurrences().remove(0);

      samplesResourceUT.save(PortalTestUtil.DEMO_ID, loaded);
      loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();
      assertThat(loaded.getOccurrences().size(), is(1));

   }

   @Test
   @Disabled("no portal dependent sampleMethods implementied yet")
   void testSave_InvalidSampleMethod()
   {
      UUID sampleID = UUID.randomUUID();

      Sample sample = createSample();
      sample.setEntityId(sampleID);
      Occurrence occurrence = occurrence(createPerson(), chorthippusPullus());
      occurrence.setDeterminationComment("Determination comment");
      occurrence.setNumericAmount(55);
      occurrence.setReproduction(Reproduction.UNKNOWN);
      occurrence.setSex(Sex.UNKNOWN);

      sample.getOccurrences().add(occurrence);
      sample.setSampleMethod(SampleMethod.QUADRAT);

      BadRequestException assertThrows = assertThrows(BadRequestException.class,
            () -> UUID.fromString((String) samplesResourceUT
                  .save(PortalTestUtil.HEUSCHRECKEN_D_ID, sample)
                  .getEntityId()));
   }

   @Test
   void testSave_OnlyOccIdIsSet()
   {
      UUID occId = UUID.randomUUID();

      Sample sample = createCompleSample();
      sample.getOccurrences().get(0).setEntityId(occId);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertAll(() -> assertThat(loaded.getEntityId(), is(not(nullValue()))),
            () -> assertThat(loaded.getOccurrences().get(0).getEntityId(),
            is(occId)));
   }

   @Test
   void testSave_ChorthippusPullus()
   {
      UUID sampleID = UUID.randomUUID();

      Sample sample = createSample();
      sample.getSurvey().setEntityId(26);
      sample.setEntityId(sampleID);
      Occurrence occurrence = occurrence(createPerson(), chorthippusPullus());
      occurrence.setDeterminationComment("Determination comment");
      occurrence.setNumericAmount(55);
      occurrence.setReproduction(Reproduction.UNKNOWN);
      occurrence.setSex(Sex.UNKNOWN);

      sample.getOccurrences().add(occurrence);
      sample.setSampleMethod(SampleMethod.FIELD_OBSERVATION);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.HEUSCHRECKEN_D_ID, sample).getEntityId());

      assertThat(savedID, is(sampleID));

      Sample loaded = samplesResourceUT
            .get(PortalTestUtil.HEUSCHRECKEN_D_ID, savedID).getSample();
      assertThat(loaded.getSampleMethod(), is(SampleMethod.FIELD_OBSERVATION));

      Occurrence chortippus = loaded.getOccurrences().iterator().next();

      assertThat(chortippus.getDeterminationComment(),
            is("Determination comment"));
      assertThat(chortippus.getTaxon().getName(), is("Chorthippus pullus"));
      assertThat(chortippus.getNumericAmount(), is(55));
      assertThat(chortippus.getSex(), is(Sex.UNKNOWN));
      assertThat(chortippus.getReproduction(), is(Reproduction.UNKNOWN));
   }

   @Test
   void testSave_ChorthippusPullus_wrongPortal()
   {
      UUID sampleID = UUID.randomUUID();

      Sample sample = createSample();
      sample.setEntityId(sampleID);
      Occurrence occurrence = occurrence(createPerson(), chorthippusPullus());
      sample.getOccurrences().add(occurrence);

      BadRequestException badRequestException = assertThrows(
            BadRequestException.class, () -> samplesResourceUT
                  .save(PortalTestUtil.FLORA_MV_ID, sample).getEntityId());

      String response = badRequestException.getResponse().readEntity(String.class);
      assertThat(response, is(
            "{\"cause\":\"Failure validating sample\\n\\noccurrences[0].taxon - werbeo.taxonnotactiveonportal.message\",\"metaData\":{\"creationTime\":0,\"sessionId\":null}}"));

   }

   @Test
   void testOccurrenceComment()
   {
      Sample sample = createCompleSample();
      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      UUID occurrenceId = loaded.getOccurrences().get(0).getEntityId();


      assertThat(client.occurrenceComments()
            .countComments(PortalTestUtil.DEMO_ID, occurrenceId)
            .getAmount(),
            is(0l));

      OccurrenceComment occurrenceComment = new OccurrenceComment(
            "This is a occurrence comment");

      SaveOrUpdateResponse response = client.occurrenceComments().saveComment(
            PortalTestUtil.DEMO_ID, occurrenceId, occurrenceComment);

      List<OccurrenceComment> comments = client
            .occurrenceComments().getComments(PortalTestUtil.DEMO_ID,
                  occurrenceId, 0, 10, CommentField.ID,
                  SortOrder.ASC)
            .getContent();

      assertThat(comments.size(), is(1));
      assertThat(comments.get(0).getComment(),
            is("This is a occurrence comment"));
      assertThat(comments.get(0).getTargetId(), is(occurrenceId));
   }

   @Test
   void testSampleComment()
   {
      Sample sample = createCompleSample();
      UUID sampleId = UUID.fromString(
            (String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(
            client.sampleComments()
                  .countComments(PortalTestUtil.DEMO_ID, sampleId)
                  .getAmount(),
            is(0l));

      SampleComment sampleComment = new SampleComment(
            "This is a sample comment");

      SaveOrUpdateResponse response = client.sampleComments()
            .saveComment(PortalTestUtil.DEMO_ID, sampleId, sampleComment);

      List<SampleComment> comments = client.sampleComments()
            .getComments(PortalTestUtil.DEMO_ID, sampleId, 0,
                  10,
                  CommentField.ID, SortOrder.ASC)
            .getContent();

      assertThat(comments.size(), is(1));
      assertThat(comments.get(0).getComment(), is("This is a sample comment"));
      assertThat(comments.get(0).getTargetId(), is(sampleId));
   }

   @Test
   void testSave_mtb()
   {
      UUID sampleID = UUID.randomUUID();

      Sample sample = createCompleSample();

      sample.getOccurrences().get(0).setNumericAmount(22);
      sample.getOccurrences().get(0)
            .setNumericAmountAccuracy(NumericAmountAccuracy.APPROXIMATE);
      sample.setEntityId(sampleID);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(savedID, is(sampleID));

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertAll(
            () -> assertThat(loaded.getLocality().getPosition().getType(),
                  is(PositionType.MTB)),
            () -> assertThat(
                  loaded.getLocality().getPosition().getMtb().getMtb(),
                  is("1943/4")),
            () -> assertThat(loaded.getLocality().getPosition().getEpsg(),
                  is(4314)),
            () -> assertThat(loaded.getOccurrences().get(0).getNumericAmount(),
                  is(22)),
            () -> assertThat(
                  loaded.getOccurrences().get(0).getNumericAmountAccuracy(),
                  is(NumericAmountAccuracy.APPROXIMATE)));
   }

   @Test
   void testSave_MissingNumericAmountAccuracy()
   {
      Sample sample = createCompleSample();

      sample.getOccurrences().get(0).setNumericAmount(22);

      assertThrows(BadRequestException.class,
            () -> samplesResourceUT.save(PortalTestUtil.DEMO_ID, sample));
   }

   @Test
   void testSave_SyncQuantiy()
   {
      Sample sample = createCompleSample();

      sample.getOccurrences().get(0).setQuantity(Quantity.ONE_TO_FIVE);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertAll(
            () -> assertThat(loaded.getOccurrences().get(0).getQuantity(),
                  is(Quantity.ONE_TO_FIVE)),
            () -> assertThat(loaded.getOccurrences().get(0).getNumericAmount(),
                  is(3)),
            () -> assertThat(
                  loaded.getOccurrences().get(0).getNumericAmountAccuracy(),
                  is(NumericAmountAccuracy.APPROXIMATE)));

   }

   @Test
   void testDelete()
   {
      UUID sampleID = UUID.randomUUID();

      Sample sample = createCompleSample();

      sample.setEntityId(sampleID);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(savedID, is(sampleID));

      DeletedResponse deletedResponse = samplesResourceUT.delete(PortalTestUtil.DEMO_ID, savedID);

      assertThrows(NotFoundException.class,
            () -> samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID));
   }

   @Test
   void testSave_MissingBlur()
   {
      Sample sample = new Sample();

      UUID sampleID = UUID.randomUUID();

      sample.setEntityId(sampleID);

      Position position = new Position();
      position.setWkt("POINT (4575000 6015000)");
      position.setWktEpsg(31468);
      position.setType(PositionType.POINT);

      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(position);

      VagueDate date = createTodayMinus(5);
      sample.setDate(date);

      SurveyBase survey = createDemoSurvey();
      sample.setSurvey(survey);

      Person person = createPerson();
      sample.setRecorder(person);

      Occurrence occurrence = new Occurrence();
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setSettlementStatus(SettlementStatus.WILD);
      occurrence.setDeterminer(person);

      TaxonBase urtica = utritca();
      occurrence.setTaxon(urtica);
      sample.getOccurrences().add(occurrence);

      assertThrows(BadRequestException.class, () -> samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());
   }

   @Test
   void testSave_point()
   {
      LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Berlin"));

      Sample sample = new Sample();

      UUID sampleID = UUID.randomUUID();

      sample.setEntityId(sampleID);

      Position position = new Position();
      position.setWkt("POINT (4575000 6015000)");
      position.setWktEpsg(31468);
      position.setType(PositionType.POINT);

      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(position);

      sample.getLocality().setBlur(4);

      VagueDate date = createTodayMinus(5);
      sample.setDate(date);

      SurveyBase survey = createDemoSurvey();
      sample.setSurvey(survey);

      Person person = createPerson();
      sample.setRecorder(person);

      Occurrence occurrence = new Occurrence();
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setDeterminer(person);

      TaxonBase urtica = utritca();
      occurrence.setTaxon(urtica);
      sample.getOccurrences().add(occurrence);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(savedID, is(sampleID));

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertThat(loaded.getLocality().getPosition().getWktEpsg(), is(31468));
      assertThat(loaded.getLocality().getPosition().getWkt(),
            is("POINT(4575000.0 6015000.0)"));
      assertThat(loaded.getCreationDate(),
            is(within(30, ChronoUnit.SECONDS, now)));
      assertThat(loaded.getModificationDate(),
            is(within(30, ChronoUnit.SECONDS, now)));
   }

   @Test
   void testSave_point_Complete_Attributes()
   {
      Sample sample = new Sample();

      UUID sampleID = UUID.randomUUID();

      sample.setEntityId(sampleID);

      Position position = new Position();
      position.setWkt("POINT (4575000 6015000)");
      position.setWktEpsg(31468);
      position.setType(PositionType.POINT);

      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(position);

      sample.getLocality().setLocality("fundort");
      sample.getLocality().setBlur(4);

      VagueDate date = createTodayMinus(5);
      sample.setDate(date);

      SurveyBase survey = createDemoSurvey();
      sample.setSurvey(survey);

      Person person = createPerson();
      sample.setRecorder(createPersonWithoutId());

      Occurrence occurrence = new Occurrence();
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setDeterminer(createPersonWithoutId());

      occurrence.setVitality(Vitality.VITAL);
      occurrence.setBloomingSprouts(BloomingSprouts.TENTHOUSAND);
      occurrence.setQuantity(Quantity.ONE_TO_FIVE);
      occurrence.setObservers("ich Du er Sie es");
      // TODO: activate, after indicia-docker-build was repaired
//      occurrence.setHabitat("habitat");

      Herbarium herbarium = new Herbarium();
      herbarium.setCode("code");
      herbarium.setHerbary("herbary");
      occurrence.setHerbarium(herbarium);

      TaxonBase urtica = utritca();
      occurrence.setTaxon(urtica);
      occurrence.setCoveredArea(Area.TWENTYSIX_TO_FIFTY);
      sample.getOccurrences().add(occurrence);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(savedID, is(sampleID));

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertThat(loaded.getLocality().getPosition().getWktEpsg(), is(31468));
      assertThat(loaded.getLocality().getPosition().getWkt(),
            is("POINT(4575000.0 6015000.0)"));

      assertThat(loaded.getLocality().getBlur(), is(4));
      assertThat(loaded.getLocality().getLocality(), is("fundort"));


      assertThat(loaded.getOccurrences().get(0).getCoveredArea(), is(Area.TWENTYSIX_TO_FIFTY));
      assertThat(loaded.getOccurrences().get(0).getVitality(),
            is(Vitality.VITAL));
      // TODO: activate, after indicia-docker-build was repaired
      // assertThat(loaded.getOccurrences().get(0).getHabitat(), is("habitat"));
      assertThat(loaded.getOccurrences().get(0).getDeterminer().getEmail(),
            is("mail@domain.com"));

      assertThat(loaded.getRecorder().getEmail(),
            is("mail@domain.com"));

      assertTrue(loaded.getOccurrences().get(0).getDeterminer().getEntityId() > 0);
      assertTrue(loaded.getRecorder().getEntityId() > 0);

      assertThat(loaded.getOccurrences().get(0).getBloomingSprouts(),
            is(BloomingSprouts.TENTHOUSAND));
      assertThat(loaded.getOccurrences().get(0).getQuantity(),
            is(Quantity.ONE_TO_FIVE));
      assertThat(loaded.getOccurrences().get(0).getObservers(),
            is("ich Du er Sie es"));
      assertThat(loaded.getOccurrences().get(0).getHerbarium().getCode(),
            is("code"));
      assertThat(loaded.getOccurrences().get(0).getHerbarium().getHerbary(),
            is("herbary"));
   }

   @Test
   void testSave_polygon()
   {
      Sample sample = new Sample();

      UUID sampleID = UUID.randomUUID();

      sample.setEntityId(sampleID);

      Position position = new Position();
      position.setWkt(GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326);
      position.setWktEpsg(4326);
      position.setType(PositionType.SHAPE);

      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(position);

      sample.getLocality().setBlur(4);

      VagueDate date = createTodayMinus(5);
      sample.setDate(date);

      SurveyBase survey = createDemoSurvey();
      sample.setSurvey(survey);

      Person person = createPerson();
      sample.setRecorder(person);

      Occurrence occurrence = new Occurrence();
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setDeterminer(person);

      TaxonBase urtica = utritca();
      occurrence.setTaxon(urtica);
      sample.getOccurrences().add(occurrence);

      UUID savedID = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      assertThat(savedID, is(sampleID));

      Sample loaded = samplesResourceUT.get(PortalTestUtil.DEMO_ID, savedID)
            .getSample();

      assertThat(loaded.getLocality().getPosition().getWktEpsg(), is(4326));
      assertThat(loaded.getLocality().getPosition().getWkt(), is("POLYGON ((13.33920478820801 54.1302092867478, 13.336973190307617 54.12870049692955, 13.342466354370119 54.12739283464121, 13.344526290893555 54.129002259288896, 13.33920478820801 54.1302092867478))"));

   }

   @Test
   void testUserRights_loadAsAdmin()
   {
      Sample sample = createCompleSample();

      UUID sampleId = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      Sample loadedAsAdmin = samplesResourceUT
            .get(PortalTestUtil.DEMO_ID, sampleId).getSample();

      assertThat(loadedAsAdmin.getAllowedOperations(), containsInAnyOrder(
            Operation.DELETE, Operation.READ, Operation.UPDATE));
   }

   @Test
   void testUserRights_loadAsNonAdmin()
   {
      Sample sample = createCompleSample();

      UUID sampleId = UUID.fromString((String) samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());

      Sample loadedAsNonAdmin = createClient(UserTestUtil.LOGIN_SELLINSF,
            UserTestUtil.PASSWORD_SELLINSF).samples()
                  .get(PortalTestUtil.DEMO_ID, sampleId).getSample();

      assertThat(loadedAsNonAdmin.getAllowedOperations(),
            containsInAnyOrder(Operation.READ));
   }

   @Test
   @DisplayName("Find samples within an area")
   void testCountWKT()
   {
      CountResponse response = samplesResourceUT.count(
            new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null, null,
                  GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326, null, null, true,
                  null, null, false, null, null, null, null));

      assertThat(response.getAmount(), is(0l));
   }

   @Test
   @DisplayName("Invalid wkt for area filter")
   void testCountInvalidWKT()
   {
      assertThrows(BadRequestException.class,
            () -> samplesResourceUT
            .count(new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                  null, GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326.substring(1),
                        null, null, true, null, null, false, null, null, null, null)));
   }


   @Test
   void testSave_MissingTaxonId()
   {
      Sample sample = new Sample();

      UUID sampleID = UUID.randomUUID();

      sample.setEntityId(sampleID);

      Position position = new Position();
      position.setWkt("POINT (4575000 6015000)");
      position.setWktEpsg(31468);
      position.setType(PositionType.POINT);

      Locality locality = new Locality();
      sample.setLocality(locality);
      sample.getLocality().setPosition(position);
      locality.setBlur(100);

      VagueDate date = createTodayMinus(5);
      sample.setDate(date);

      SurveyBase survey = createDemoSurvey();
      sample.setSurvey(survey);

      Person person = createPerson();
      sample.setRecorder(person);

      Occurrence occurrence = new Occurrence();
      occurrence.setRecordStatus(RecordStatus.COMPLETE);
      occurrence.setSettlementStatus(SettlementStatus.WILD);
      occurrence.setDeterminer(person);

      TaxonBase noTaxonId = new TaxonBase();

      occurrence.setTaxon(noTaxonId);
      sample.getOccurrences().add(occurrence);

      assertThrows(BadRequestException.class, () -> samplesResourceUT
            .save(PortalTestUtil.DEMO_ID, sample).getEntityId());
   }

   @Test
   public void testSampleMethods()
   {
      for (SampleFieldConfig config : samplesResourceUT
            .getFieldConfig(PortalTestUtil.HEUSCHRECKEN_D_ID).getFieldConfigs())
      {
         if (config.getField().equals(SampleField.SAMPLE_METHOD))
         {
            assertEquals(7, config.getValues().size());
         }
      }
   }
}
