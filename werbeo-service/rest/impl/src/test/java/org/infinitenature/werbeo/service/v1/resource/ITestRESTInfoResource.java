package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import javax.annotation.PostConstruct;

import org.infinitenature.service.v1.resources.InfoResource;
import org.infinitenature.service.v1.types.UserInfo;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.Test;

public class ITestRESTInfoResource extends RestApiTest
{
   private InfoResource infoResourceAuthUT;
   private InfoResource infoResourceAnonUT;

   @PostConstruct
   public void setUp()
   {
      infoResourceAnonUT = createClient().info();
      infoResourceAuthUT = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA).info();
   }

   @Test
   public void testGetServiceUser_Anon()
   {
      UserInfo userInfo = infoResourceAnonUT.getServiceUser().getUserInfo();

      assertThat(userInfo.isAnonymous(), is(true));
      assertThat(userInfo.getEmail(), is(nullValue()));
   }

   @Test
   public void testGetServiceUser_Auth()
   {
      UserInfo userInfo = infoResourceAuthUT.getServiceUser().getUserInfo();

      assertThat(userInfo.isAnonymous(), is(false));
      assertThat(userInfo.getEmail(), is(UserTestUtil.LOGIN_CTOMALAA));
   }

}
