package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.exparity.hamcrest.date.LocalDateTimeMatchers;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ItestRESTOccurrenceValidationResource extends AbstractITestREST
{

   private OccurrenceResource occurrenceValidationResourceUT;

   @PostConstruct
   @Override
   protected void setUp()
   {
      super.setUp();
      occurrenceValidationResourceUT = client.occurrences();
   }

   @Test
   @DisplayName("Default no validation")
   void test_001()
   {
      UUID sampleID = UUID.randomUUID();
      UUID occurrenceID = UUID.randomUUID();
      Sample sample = createCompleSample();

      sample.setEntityId(sampleID);
      sample.getOccurrences().get(0).setEntityId(occurrenceID);

      client.samples().save(PortalTestUtil.DEMO_ID, sample);

      Sample loaded = client.samples().get(PortalTestUtil.DEMO_ID, sampleID)
            .getSample();

      assertThat(loaded.getOccurrences().get(0).getValidation(),
            is(nullValue()));
   }

   @Test
   @DisplayName("validate")
   void test_002()
   {
      UUID sampleID = UUID.randomUUID();
      UUID occurrenceID = UUID.randomUUID();
      Sample sample = createCompleSample();

      sample.setEntityId(sampleID);
      sample.getOccurrences().get(0).setEntityId(occurrenceID);

      client.samples().save(PortalTestUtil.DEMO_ID, sample);

      occurrenceValidationResourceUT.validate(PortalTestUtil.DEMO_ID,
            occurrenceID,
            new Validation(ValidationStatus.INVALID, "A comment"));

      Sample loaded = client.samples().get(PortalTestUtil.DEMO_ID, sampleID)
            .getSample();

      List<Occurrence> loadedOccurrences = client.occurrences()
            .find(0, 10, OccurrenceField.VALIDATION_STATUS, SortOrder.ASC,
                  new OccurrenceFilter()
                        .setValidationStatus(ValidationStatus.INVALID)
                        .setPortalId(PortalTestUtil.DEMO_ID),
                  false)
            .getContent();

      Validation validationFromGetSample = loaded.getOccurrences().get(0)
            .getValidation();
      assertAll(
            () -> assertThat(loadedOccurrences, hasSize(1)),
            () -> assertThat(validationFromGetSample.getStatus(),
                  is(ValidationStatus.INVALID)),
            () -> assertThat(validationFromGetSample.getComment(),
                  is("A comment")),
            () -> assertThat(validationFromGetSample.getValidator().getEmail(),
                  is(UserTestUtil.LOGIN_CTOMALAA)),
            () -> assertThat(
                  validationFromGetSample.getValidator().getFirstName(),
                  is("Cthrine")),
            () -> assertThat(
                  validationFromGetSample.getValidator().getLastName(),
                  is("Tomala")),
            () -> assertThat(validationFromGetSample.getValidationTime(),
                  LocalDateTimeMatchers.within(1l, ChronoUnit.MINUTES,
                        LocalDateTime.now())));
   }

}
