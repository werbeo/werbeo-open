package org.infinitenature.werbeo.service.mapper;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestMediaPathUtils
{
   private MediaPathUtils mediaPathUtilsUT;

   @BeforeEach
   void setUp() throws Exception
   {
      mediaPathUtilsUT = new MediaPathUtils();
      InstanceConfig instanceConfig = new InstanceConfig();
      instanceConfig.setLocalMediaURL("https://local.indicia.loe.de/media");
      mediaPathUtilsUT.setInstanceConfig(instanceConfig);
   }

   @Test
   @DisplayName("Clear local image")
   void test003()
   {
      assertThat(
            mediaPathUtilsUT
                  .clearPath("https://local.indicia.loe.de/media/path.png"),
            is("path.png"));
   }

   @Test
   @DisplayName("Clear local image thumb")
   void test004()
   {
      assertThat(
            mediaPathUtilsUT.clearPath(
                  "https://local.indicia.loe.de/media/thumb-path.png"),
            is("path.png"));
   }

   @Test
   @DisplayName("Clear external path")
   void test005()
   {
      assertThat(
            mediaPathUtilsUT.clearPath("https://www.external.com/resource"),
            is("https://www.external.com/resource"));
   }
}
