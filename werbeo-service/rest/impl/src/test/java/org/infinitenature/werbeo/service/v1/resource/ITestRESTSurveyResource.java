package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.WebApplicationException;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.service.v1.types.ObfuscationPolicy;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.enums.Permission;
import org.infinitenature.service.v1.types.enums.Role;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.SurveyTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.service.config.WerBeoFeatures;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ITestRESTSurveyResource extends AbstractITestREST
{

   private SurveysResource surveysResourceUT;
   private SamplesResource samplesResource;

   @Override
   @PostConstruct
   public void setUp()
   {
      super.setUp();

      surveysResourceUT = client.surveys();
      samplesResource = client.samples();
   }

   @Test
   public void testSave()
   {

      SurveyBase survey = new SurveyBase();
      survey.setName("My survey to save");
      survey.setDescription("My test survey description");
      survey.setAvailability(Availability.FREE);
      survey.setWerbeoOriginal(true);
      survey.setAllowDataEntry(true);
      survey.setContainer(true);

      SaveOrUpdateResponse response = surveysResourceUT
            .save(PortalTestUtil.DEMO_ID, survey);

      assertThat(response.getLink().getRel(), is("self"));
   }

   @Test
   public void testSaveMissingAvailability()
   {

      SurveyBase survey = new SurveyBase();
      survey.setName("My survey to save");
      survey.setDescription("My test survey description");

      WebApplicationException exception = assertThrows(
            WebApplicationException.class,
            () -> surveysResourceUT.save(PortalTestUtil.DEMO_ID, survey));

      String body = exception.getResponse().readEntity(String.class);

      assertThat(body, containsString(
            "{\"constraintType\":\"PROPERTY\",\"path\":\"availability\",\"message\":\"javax.validation.constraints.NotNull.message\",\"value\":\"\"}"));

   }

   @Test
   public void testSaveEmptyDescription()
   {

      SurveyBase survey = new SurveyBase();
      survey.setName("My survey to save");
      survey.setDescription("  ");
      survey.setAvailability(Availability.FREE);

      BadRequestException exception = assertThrows(BadRequestException.class,
            () -> surveysResourceUT.save(PortalTestUtil.DEMO_ID, survey));

      String body = exception.getResponse().readEntity(String.class);

      assertThat(body, containsString(
            "{\"constraintType\":\"PROPERTY\",\"path\":\"description\",\"message\""));

   }

   @Test
   public void testRetrive()
   {
      SurveyBase survey = new SurveyBase();
      survey.setName("My survey to save");
      survey.setDescription("My test survey description");
      survey.setAvailability(Availability.FREE);
      survey.setWerbeoOriginal(true);
      survey.setAllowDataEntry(false);
      ObfuscationPolicy pol1 = new ObfuscationPolicy("ACCEPTED", "PERSONS");
      ObfuscationPolicy pol2 = new ObfuscationPolicy("ADMIN", "LOCATION_POINT");
      survey.getObfuscationPolicies().add(pol1);
      survey.getObfuscationPolicies().add(pol2);
      survey.getTags().add("MULTIBASE");
      survey.getTags().add("2nd TAG");

      SaveOrUpdateResponse response = surveysResourceUT
            .save(PortalTestUtil.DEMO_ID, survey);

      SurveyBase retrived = surveysResourceUT
            .getSurvey(PortalTestUtil.DEMO_ID, (int) response.getEntityId())
            .getSurvey();

      assertAll(
            () -> assertThat(retrived.getAvailability(), is(Availability.FREE)),
            () -> assertThat(retrived.getName(), is("My survey to save")),
            () -> assertThat(retrived.getDescription(),
                  is("My test survey description")),
            () -> assertThat(retrived.getObfuscationPolicies().size(), is(2)),
            () -> assertThat(retrived.getWerbeoOriginal(), is(true)),
            () -> assertThat(retrived.getAllowDataEntry(), is(false)),
            () -> assertThat(retrived.getTags(),
                  containsInAnyOrder("MULTIBASE", "2nd TAG")));
   }

   @Test
   public void testUpdate()
   {
      SurveyBase survey = new SurveyBase();
      survey.setName("My survey to save");
      survey.setDescription("My test survey description");
      survey.setAvailability(Availability.FREE);
      survey.setWerbeoOriginal(true);
      survey.setAllowDataEntry(true);
      survey.getTags().add("MULTIBASE");
      survey.getTags().add("2nd TAG");

      SaveOrUpdateResponse createionResponse = surveysResourceUT
            .save(PortalTestUtil.DEMO_ID, survey);

      SurveyBase retrived = surveysResourceUT.getSurvey(PortalTestUtil.DEMO_ID,
            (int) createionResponse.getEntityId()).getSurvey();

      retrived.setAvailability(Availability.RESTRICTED);
      retrived.setName("My edited Survey");
      retrived.setDescription("My test survey description - edited");
      retrived.setWerbeoOriginal(false);
      retrived.setAllowDataEntry(false);
      retrived.setContainer(true);
      retrived.getTags().remove("MULTIBASE");
      retrived.getTags().add("BLA");

      SaveOrUpdateResponse updateResponse = surveysResourceUT
            .save(PortalTestUtil.DEMO_ID, retrived);

      SurveyBase updated = surveysResourceUT.getSurvey(PortalTestUtil.DEMO_ID,
            (int) updateResponse.getEntityId()).getSurvey();

      assertAll(
            () -> assertThat(updated.getEntityId(),
                  is(createionResponse.getEntityId())),
            () -> assertThat(updated.getAvailability(),
                  is(Availability.RESTRICTED)),
            () -> assertThat(updated.getName(), is("My edited Survey")),
            () -> assertThat(updated.getDescription(),
                  is("My test survey description - edited")),
            () -> assertThat(updated.getWerbeoOriginal(), is(false)),
            () -> assertThat(updated.getAllowDataEntry(), is(false)),
            () -> assertThat(updated.getContainer(), is(true)),
            () -> assertThat(retrived.getTags(),
                  containsInAnyOrder("BLA", "2nd TAG")));
   }

   @Test
   @DisplayName("Try to update an survey without rights")
   void testUpdate002()
   {
      SurveysResource notAdminClient = createClient(UserTestUtil.LOGIN_SELLINSF,
            UserTestUtil.PASSWORD_SELLINSF).surveys();

      SurveyBase floraMVOnline = notAdminClient
            .getSurvey(PortalTestUtil.FLORA_MV_ID,
                  SurveyTestUtil.SURVEY_ID_DE_MV_ONLINE)
            .getSurvey();

      floraMVOnline
            .setDescription("Try to update something I'm not allowed to");

      assertThrows(ForbiddenException.class, () -> notAdminClient
            .save(PortalTestUtil.FLORA_MV_ID, floraMVOnline));
   }

   @Test
   void testGetFieldConfig()
   {
      Set<SurveyFieldConfig> fieldConfigs = surveysResourceUT
            .getFieldConfig(PortalTestUtil.FLORA_MV_ID).getFieldConfigs();
      assertThat(fieldConfigs, hasSize(greaterThan(0)));

      for (SurveyFieldConfig surveyFieldConfig : fieldConfigs)
      {
         assertThat(surveyFieldConfig.getField(), is(not(nullValue())));
         assertThat(
               "The translationKey for the field "
                     + surveyFieldConfig.getField() + " may not be empty",
               surveyFieldConfig.getTranslationKey(),
               is(not(isEmptyOrNullString())));
         assertThat(
               "The property for the field " + surveyFieldConfig.getField()
                     + " may not be empty",
               surveyFieldConfig.getProperty(), is(not(isEmptyOrNullString())));
      }
   }

   @Test
   @DisplayName("save with obfusication policies")
   void testUpdate003() throws Exception
   {
      enableFeature(WerBeoFeatures.BUILD_OCCURRENCE_INDEX);
      enableFeature(WerBeoFeatures.UPDATE_INDEX_ON_SAVE);

      SurveyBase survey = new SurveyBase();
      survey.setName("A new survey");
      survey.setDescription("For testing only");
      survey.setAvailability(Availability.FREE);
      survey.getObfuscationPolicies()
            .add(new ObfuscationPolicy(Role.ACCEPTED, Permission.PERSONS));
      SaveOrUpdateResponse response = surveysResourceUT
            .save(PortalTestUtil.DEMO_ID, survey);

      assertThat(response.getLink().getRel(), is("self"));

      Sample sample = createCompleSample();
      sample.getSurvey().setEntityId((Integer) response.getEntityId());

      samplesResource.save(PortalTestUtil.DEMO_ID, sample);

      TimeUnit.SECONDS.sleep(3);

      Werbeo nonAdmin = createClient(UserTestUtil.LOGIN_SELLINSF,
            UserTestUtil.PASSWORD_SELLINSF);

      OccurrenceFilter filter = new OccurrenceFilter();
      filter.setPortalId(PortalTestUtil.DEMO_ID);
      filter.setSurveyId((Integer) response.getEntityId());

      OccurrenceSliceResponse occResponse = nonAdmin.occurrences().find(0, 10,
            OccurrenceField.MOD_DATE, SortOrder.ASC, filter, false);

      assertThat("Names should not be obfusicated",
            occResponse.getContent().get(0).getDeterminer().getFirstName(),
            is("Haleigh"));

      survey.setObfuscationPolicies(Collections.emptyList());
      survey.setEntityId((Integer) response.getEntityId());

      response = surveysResourceUT.save(PortalTestUtil.DEMO_ID, survey);

      TimeUnit.SECONDS.sleep(3);

      occResponse = nonAdmin.occurrences().find(0, 10, OccurrenceField.MOD_DATE,
            SortOrder.ASC, filter, false);

      assertThat("Names must be obfusicated",
            occResponse.getContent().get(0).getDeterminer().getFirstName(),
            is("Hidden"));
   }

   @Test
   @DisplayName("reenable data entry - #1072")
   void testUpdate004()
   {
      SurveyBase survey = new SurveyBase();
      survey.setName("My survey to save");
      survey.setDescription("My test survey description");
      survey.setAvailability(Availability.FREE);

      survey.setAllowDataEntry(false);

      SaveOrUpdateResponse createionResponse = surveysResourceUT
            .save(PortalTestUtil.DEMO_ID, survey);

      SurveyBase retrived = surveysResourceUT.getSurvey(PortalTestUtil.DEMO_ID,
            (int) createionResponse.getEntityId()).getSurvey();

      assertThat(retrived.getAllowDataEntry(), is(false));

      retrived.setAllowDataEntry(true);

      SaveOrUpdateResponse updateResponse = surveysResourceUT
            .save(PortalTestUtil.DEMO_ID, retrived);

      SurveyBase updated = surveysResourceUT.getSurvey(PortalTestUtil.DEMO_ID,
            (int) updateResponse.getEntityId()).getSurvey();

      assertAll(() -> assertThat(updated.getAllowDataEntry(), is(true)));
   }
}
