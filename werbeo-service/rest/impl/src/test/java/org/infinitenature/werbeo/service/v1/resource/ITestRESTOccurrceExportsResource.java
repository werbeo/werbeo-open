package org.infinitenature.werbeo.service.v1.resource;

import org.apache.commons.io.FileUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceExportResource;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.JobStatus;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportsInfoResponse;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.infra.commands.OccurrenceExportCommands;
import org.infinitenature.werbeo.service.core.api.infra.query.FieldConfigQueries;
import org.infinitenature.werbeo.service.core.api.ports.OccurrenceQueryPort;
import org.infinitenature.werbeo.service.core.impl.export.ExportFacade;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ITestRESTOccurrceExportsResource extends RestApiTest
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(ITestRESTOccurrceExportsResource.class);
   private OccurrenceExportResource occurrenceExportResourceUT;

   @BeforeEach
   public void setUp()
   {
      occurrenceExportResourceUT = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA).occurrenceExports();

   }

   @Test
   public void testGetWinArt() throws InterruptedException
   {
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
            null, null, 5930, null, null, null, true, null, null, false, null, null, null, null);

      OccurrenceExportResponse response = occurrenceExportResourceUT
            .create(filter, ExportFormat.WINART);
      JobStatus status = null;
      int polls = 10;
      while (polls > 0)
      {
         polls--;
         try
         {
            status = occurrenceExportResourceUT
                  .getStatus(response.getExportId(), PortalTestUtil.FLORA_MV_ID)
                  .getStatus().getStatus();

            LOGGER.info("Export status: {}", status);
            if (status == JobStatus.FINISHED)
            {
               break;
            }
         } catch (Exception e)
         {
            LOGGER.info(e.getMessage());
         }
         Thread.sleep(TimeUnit.SECONDS.toMillis(5));
      }
      assertThat(status, is(JobStatus.FINISHED));

      // Download the result

      Response downloadResponse = occurrenceExportResourceUT
            .getExport(response.getExportId(), PortalTestUtil.FLORA_MV_ID);
      InputStream downloadStream = (InputStream) downloadResponse.getEntity();
      System.out.println(downloadResponse);
   }

   @Test
   public void testGetCsv() throws InterruptedException
   {
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
            null, null, 5930, null, null, null, true, null, null, false, null, null, null, null);

      OccurrenceExportResponse response = occurrenceExportResourceUT
            .create(filter, ExportFormat.CSV);
      JobStatus status = null;
      int polls = 1000;
      while (polls > 0)
      {
         polls--;
         try
         {
            status = occurrenceExportResourceUT
                  .getStatus(response.getExportId(), PortalTestUtil.FLORA_MV_ID)
                  .getStatus().getStatus();

            LOGGER.info("Export status: {}", status);
            if (status == JobStatus.FINISHED)
            {
               break;
            }
         } catch (Exception e)
         {
            LOGGER.info(e.getMessage());
         }
         Thread.sleep(TimeUnit.SECONDS.toMillis(5));
      }
      assertThat(status, is(JobStatus.FINISHED));

      // Download the result

      Response downloadResponse = occurrenceExportResourceUT
            .getExport(response.getExportId(), PortalTestUtil.FLORA_MV_ID);
      InputStream inputStream = (InputStream) downloadResponse.getEntity();
      StringBuilder sb = new StringBuilder();

      try (BufferedReader br = new BufferedReader(
            new InputStreamReader(inputStream, "ISO-8859-1")))
      {
         String line;
         while ((line = br.readLine()) != null)
         {
            sb.append(line);
            sb.append('\n');
         }
      } catch (UnsupportedEncodingException e)
      {
         e.printStackTrace();
      } catch (IOException e)
      {
         e.printStackTrace();
      }

      String result = sb.toString();
      System.out.println(result);
      assertThat(result.contains("UUID"), is(true));
      assertThat(result.contains("Taxon"), is(true));
      assertThat(result.contains("Bestimmer"), is(true));
      assertThat(result.contains("Mitbeobachter"), is(true));
      assertThat(result.contains("Vorkommens-Status"), is(true));
      assertThat(result.contains("Anzahl"), is(true));
      assertThat(result.contains("Habitat"), is(true));
      assertThat(result.contains("Fundort"), is(true));
      assertThat(result.contains("Breite"), is(true));
      assertThat(result.contains("Koordinatensystem EPSG"), is(true));
      assertThat(result.contains("Ungenauigkeit der Ortsangabe in m"),
            is(true));
      assertThat(result.contains("Herbarbeleg"), is(true));

   }

   @Test
   public void testFind_noExports()
   {
      OccurrenceExportsInfoResponse response = occurrenceExportResourceUT
            .findExports(PortalTestUtil.FLORA_BB_ID, 0, 100,
                  OccurrenceExportField.ID, SortOrder.ASC);

      assertThat(response.getContent().size(), is(0));
   }

   @Test
   public void testFind() throws InterruptedException
   {
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
            null, null, 5930, null, null, null, true, null, null, false, null, null, null, null);

      OccurrenceExportResponse exportResponse = occurrenceExportResourceUT
            .create(filter, ExportFormat.WINART);

      Thread.sleep(TimeUnit.SECONDS.toMillis(1));
      OccurrenceExportsInfoResponse response = occurrenceExportResourceUT
            .findExports(PortalTestUtil.FLORA_MV_ID, 0, 100,
                  OccurrenceExportField.ID, SortOrder.ASC);

      assertThat(response.getContent().size(), is(not((0))));
   }

   @Test
   public void testDeleteCsv() throws InterruptedException
   {
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
            null, null, 5930, null, null, null, true, null, null, false, null, null, null, null);

      OccurrenceExportResponse response = occurrenceExportResourceUT
            .create(filter, ExportFormat.CSV);

      JobStatus status = null;
      int polls = 1000;
      while (polls > 0)
      {
         polls--;
         try
         {
            status = occurrenceExportResourceUT
                  .getStatus(response.getExportId(), PortalTestUtil.FLORA_MV_ID)
                  .getStatus().getStatus();

            LOGGER.info("Export status: {}", status);
            if (status == JobStatus.FINISHED)
            {
               break;
            }
         } catch (Exception e)
         {
            LOGGER.info(e.getMessage());
         }
         Thread.sleep(TimeUnit.SECONDS.toMillis(5));
      }
      assertThat(status, is(JobStatus.FINISHED));

      DeletedResponse deletedResponse = occurrenceExportResourceUT
            .delete(response.getExportId(), PortalTestUtil.FLORA_MV_ID);

      NotFoundException thrown = assertThrows(
            NotFoundException.class, () -> {
               occurrenceExportResourceUT
                     .getStatus(response.getExportId(),
                           PortalTestUtil.FLORA_MV_ID);
            }, "Expected getStatus to throw, but it didn't");

      assertTrue(thrown.getMessage().contains("HTTP 404 Not Found"));

      ByteArrayInputStream arrayInputStream = (ByteArrayInputStream) thrown
            .getResponse().getEntity();
      Scanner scanner = new Scanner(arrayInputStream);
      scanner.useDelimiter("\\Z");
      String data = "";
      if (scanner.hasNext())
      {
         data = scanner.next();
      }
      assertTrue(data.contains(response.getExportId().toString()));
   }

   @Test
   public void testDeleteCsv_wrong_user() throws InterruptedException
   {
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
            null, null, 5930, null, null, null, true, null, null, false, null, null, null, null);

      OccurrenceExportResponse response = occurrenceExportResourceUT
            .create(filter, ExportFormat.CSV);

      JobStatus status = null;
      int polls = 1000;
      while (polls > 0)
      {
         polls--;
         try
         {
            status = occurrenceExportResourceUT
                  .getStatus(response.getExportId(), PortalTestUtil.FLORA_MV_ID)
                  .getStatus().getStatus();

            LOGGER.info("Export status: {}", status);
            if (status == JobStatus.FINISHED)
            {
               break;
            }
         } catch (Exception e)
         {
            LOGGER.info(e.getMessage());
         }
         Thread.sleep(TimeUnit.SECONDS.toMillis(5));
      }
      assertThat(status, is(JobStatus.FINISHED));

      OccurrenceExportResource occurrenceExportResourceWrongUser = createClient(
            UserTestUtil.LOGIN_SELLINSF, UserTestUtil.PASSWORD_SELLINSF)
            .occurrenceExports();

      NotFoundException thrownByWrongUser = assertThrows(
            NotFoundException.class, () -> {
               DeletedResponse deletedResponse = occurrenceExportResourceWrongUser
                     .delete(response.getExportId(),
                           PortalTestUtil.FLORA_MV_ID);
            }, "Expected delete to throw, but it didn't");

      assertTrue(thrownByWrongUser.getMessage().contains("HTTP 404 Not Found"));

      ByteArrayInputStream arrayInputStream = (ByteArrayInputStream) thrownByWrongUser
            .getResponse().getEntity();
      Scanner scanner = new Scanner(arrayInputStream);
      scanner.useDelimiter("\\Z");
      String data = "";
      if (scanner.hasNext())
      {
         data = scanner.next();
      }
      assertTrue(data.contains(response.getExportId().toString()));

      JobStatus statusStillThere = occurrenceExportResourceUT
            .getStatus(response.getExportId(), PortalTestUtil.FLORA_MV_ID)
            .getStatus().getStatus();

      assertThat(statusStillThere, is(JobStatus.FINISHED));

      // clean up after test
      DeletedResponse deletedResponse = occurrenceExportResourceUT
            .delete(response.getExportId(), PortalTestUtil.FLORA_MV_ID);
   }

   @Test
   public void testDeleteCsv_not_existing_exportId() throws InterruptedException
   {
      UUID notExistingExportId = UUID.randomUUID();
      NotFoundException thrown = assertThrows(
            NotFoundException.class, () -> {
               occurrenceExportResourceUT
                     .getStatus(notExistingExportId,
                           PortalTestUtil.FLORA_MV_ID);
            }, "Expected getStatus to throw, but it didn't");

      assertTrue(thrown.getMessage().contains("HTTP 404 Not Found"));

      ByteArrayInputStream arrayInputStream = (ByteArrayInputStream) thrown
            .getResponse().getEntity();
      Scanner scanner = new Scanner(arrayInputStream);
      scanner.useDelimiter("\\Z");
      String data = "";
      if (scanner.hasNext())
      {
         data = scanner.next();
      }
      assertTrue(data.contains(notExistingExportId.toString()));
   }

   @Test
   public void testInit(@TempDir File temporaryFolder)
   {
      try
      {
         File runningFolder = new File(
               temporaryFolder.getPath() + "/exports/RUNNING");
         File finishedFolder = new File(
               temporaryFolder.getPath() + "/exports/FINISHED");

         File runningFile = new File(runningFolder, "delete.csv");
         FileUtils.writeStringToFile(runningFile, "content", "utf-8");
         File finishedFile = new File(finishedFolder, "keep.csv");
         FileUtils.writeStringToFile(finishedFile, "content", "utf-8");

         OccurrenceQueryPort occurrenceQueries = mock(OccurrenceQueryPort.class);
         InstanceConfig instanceConfig = mock(InstanceConfig.class);
         when(instanceConfig.getDir()).thenReturn(temporaryFolder.getPath());
         List<OccurrenceExportCommands> occurrenceExports = Arrays
               .asList(mock(OccurrenceExportCommands.class),
                     mock(OccurrenceExportCommands.class));
         FieldConfigQueries fieldConfigQueries = mock(FieldConfigQueries.class);

         ExportFacade exportFacade = new ExportFacade(occurrenceQueries,
               instanceConfig, occurrenceExports, fieldConfigQueries);

         assertTrue(runningFile.exists());
         assertTrue(finishedFile.exists());

         exportFacade.init();

         assertTrue(!runningFile.exists());
         assertTrue(finishedFile.exists());

      } catch (IOException e)
      {

      }

   }

}
