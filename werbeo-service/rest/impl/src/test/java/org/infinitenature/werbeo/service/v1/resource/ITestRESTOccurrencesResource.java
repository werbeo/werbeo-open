package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceFilter.WKTMode;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.HerbarySheet;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.enums.HerbarySheetPosition;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.OccurrenceCentroidsSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceStreamResponse;
import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.service.indicia.http.exception.EntityNotFoundException;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ITestRESTOccurrencesResource extends RestApiTest
{
   private OccurrenceResource occurrenceResourceUT;
   private HerbarySheet sheet;

   @PostConstruct
   public void setUp()
   {
      occurrenceResourceUT = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA).occurrences();

      sheet = new HerbarySheet();
      sheet.setAuthor("Carl von Linné");
      sheet.setBook("Rothmaler");
      sheet.setSynonym("Birke");
      sheet.setHabitat("Moor");
      sheet.setHerbary("Elfis Herbarium");
   }

   @Test
   void testCount_anon()
   {
      OccurrenceResource anon = createClient().occurrences();
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_BB_ID, null, null,
            null, null, null, null, true, null, null, false, null, null, null, null);
      assertThrows(ForbiddenException.class, () -> anon.count(filter));
   }

   @Test
   void testCount_anon_FloraMV()
   {
      OccurrenceResource anon = createClient().occurrences();

      CountResponse response = anon
            .count(new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                  null, null, null, null, true, null, null, false, null, null,
                  null, null));

      assertThat(response.getAmount(), is(7425L));
   }

   @Test
   void testCount()
   {
      CountResponse response = occurrenceResourceUT.count(
            new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null, null,
                  null, null, null, true, null,
                  null, false, null, null, null, null));

      assertThat(response.getAmount(), is(7425L));
      assertThat(response.getMetaData().getSessionId(), startsWith("i-test-"));
   }

   @Test
   @DisplayName("Count by mtb when position is point - #943")
   void testCountByMTB()
   {
      CountResponse response = occurrenceResourceUT.count(new OccurrenceFilter(
            PortalTestUtil.HEUSCHRECKEN_D_ID, null, null, null, null, "1643",
            null, true, null, null, false, null, null, null, null));

      assertThat(response.getAmount(), is(2L));
   }

   @Test
   @DisplayName("Count by external key - exact match")
   void testCountExternalKey001()
   {
      CountResponse response = occurrenceResourceUT
            .count(new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                  null, null, null, null, true, null, null, false, null, null,
                  null, "Flora-MV-Global-ID: 1234"));

      assertThat(response.getAmount(), is(1L));
   }

   @Test
   @DisplayName("Count by external key - case sesitive no match")
   void testCountExternalKey002()
   {
      CountResponse response = occurrenceResourceUT.count(new OccurrenceFilter(
            PortalTestUtil.FLORA_MV_ID, null, null, null, null, null, null,
            true, null, null, false, null, null, null, "GLOBAL"));

      assertThat(response.getAmount(), is(0L));
   }

   @Test
   @DisplayName("Count by external key - case sesitive match")
   void testCountExternalKey003()
   {
      CountResponse response = occurrenceResourceUT.count(new OccurrenceFilter(
            PortalTestUtil.FLORA_MV_ID, null, null, null, null, null, null,
            true, null, null, false, null, null, null, "Global"));

      assertThat(response.getAmount(), is(2L));
   }

   @Test
   @DisplayName("Count by modified since - no data because date in the future")
   void testCountModifiedSince()
   {
      CountResponse response = occurrenceResourceUT.count(new OccurrenceFilter()
            .setModifiedSince(LocalDateTime.now().plus(1, ChronoUnit.DAYS))
            .setPortalId(PortalTestUtil.FLORA_MV_ID));

      assertThat(response.getAmount(), is(0L));
      assertThat(response.getMetaData().getSessionId(),
            startsWith("i-test-"));
   }

   @Test
   void testCount_invalidBlur()
   {
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
            null, null, null, null, null, -2, true, null, null, false, null,
            null, null, null);
      assertThrows(BadRequestException.class,
            () -> occurrenceResourceUT.count(filter));
   }

   @Test
   void testCount_invalidPortal()
   {
      OccurrenceFilter filter = new OccurrenceFilter(0, null, null, null, null,
            null, null, true, null, null, false, null, null, null, null);
      assertThrows(BadRequestException.class,
            () -> occurrenceResourceUT.count(filter));
   }

   @Test
   void testStream_first()
   {
      OccurrenceStreamResponse response = occurrenceResourceUT
            .stream(PortalTestUtil.FLORA_MV_ID, null);

      assertThat(response.getContent().size(), is(150));
      assertThat(response.getNextToken(), startsWith("154_"));
   }

   @Test
   void testStream_noChanges()
   {
      OccurrenceStreamResponse response = occurrenceResourceUT
            .stream(PortalTestUtil.FLORA_MV_ID, "7378_2523607814000");

      assertThat(response.getContent().size(), is(0));
      assertThat(response.getNextToken(), startsWith("7378_"));
   }

   @Test
   void testFind()
   {
      OccurrenceSliceResponse response = occurrenceResourceUT.find(0, 5,
            OccurrenceField.DETERMINER, SortOrder.ASC,
            new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null, null,
                  null, null, null, true, null, null, false, null, null, null,
                  null),
            false);

      assertThat(response.getContent().size(), is(5));

      response.getContent()
            .forEach(occurrence ->
            {
               assertThat(occurrence.isObfuscated(), is(false));
               assertThat(occurrence.getEntityId(), is(not(nullValue())));
                     assertThat(occurrence.getTaxon().getTaxaList().getName(),
                                 is("GermanSL 1.5.1"));
            });
   }

   @Test
   void testCountPolygonWithin()
   {
      CountResponse response = occurrenceResourceUT.count(
            new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null, null,
                  GeomTestUtil.WKT_INERSECTS_1944_1_AND_2, null, null, true,
                  null, null, false, null, null, null, null)
                        .setWktMode(WKTMode.WITHIN));

      assertThat(response.getAmount(), is(0l));

   }

   @Test
   void testCountPolygonIntersects()
   {
      CountResponse response = occurrenceResourceUT
            .count(new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                  null, GeomTestUtil.WKT_INERSECTS_1944_1_AND_2, null, null,
                  true, null, null, false, null, null, null, null)
                        .setWktMode(WKTMode.INTERSECTS));

      assertThat(response.getAmount(), is(287l));

   }

   @Test
   void testCountVALID()
   {
      CountResponse response = occurrenceResourceUT.count(new OccurrenceFilter(
            PortalTestUtil.FLORA_MV_ID, null, null, null, null, null, null,
            true, null, null, false, null, ValidationStatus.VALID, null, null));

      assertThat(response.getAmount(), is(1l));
   }

   @Test
   void testCountByValidator()
   {
      CountResponse response = occurrenceResourceUT.count(new OccurrenceFilter(
            PortalTestUtil.FLORA_MV_ID, null, null, null, null, null, null,
            true, null, null, false, null, null, UserTestUtil.LOGIN_CTOMALAA, null));

      assertThat(response.getAmount(), is(2l));
   }

   @Test
   void testCountByValidatorAndStatus()
   {
      CountResponse response = occurrenceResourceUT
            .count(new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                  null, null, null, null, true, null, null, false, null,
                  ValidationStatus.INVALID, UserTestUtil.LOGIN_CTOMALAA, null));

      assertThat(response.getAmount(), is(1l));
   }

   @Test
   void testFind_not_approved_user()
   {
      OccurrenceResource notApprovedResource = createClient(
            UserTestUtil.LOGIN_NOT_APPROVED, UserTestUtil.PASSWORD_NOT_APPROVED)
            .occurrences();

      OccurrenceSliceResponse response = notApprovedResource
            .find(0, 5, OccurrenceField.DETERMINER, SortOrder.ASC,
                  new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                        null, null, null, null,
                        true, null, null, null, null, null, null, null),
                  false);

      assertThat(response.getContent().size(), is(5));
   }

   @Test
   void testFind_not_approved_user_BB()
   {
      OccurrenceResource notApprovedResource = createClient(
            UserTestUtil.LOGIN_NOT_APPROVED, UserTestUtil.PASSWORD_NOT_APPROVED)
                  .occurrences();

      OccurrenceSliceResponse response = notApprovedResource.find(0, 5,
            OccurrenceField.DETERMINER, SortOrder.ASC,
            new OccurrenceFilter(PortalTestUtil.FLORA_BB_ID, null, null, null,
                  null, null, null, true, null, null, null, null, null, null, null),
            false);

      assertThat(response.getContent().size(), is(0));
   }

   @Test
   void testFindObfuscated_policy_PERSONS_FOR_APPROVED_USERS_approved()
   {
      OccurrenceResource noAdminResource = createClient(
            UserTestUtil.LOGIN_SELLINSF, UserTestUtil.PASSWORD_SELLINSF)
                  .occurrences();
      OccurrenceSliceResponse response = noAdminResource.find(0, 5,
            OccurrenceField.DETERMINER, SortOrder.ASC,
            new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
                  null, null, null, null, null, null, true, null, null, false,
                  null, null, null, null),
            false);

      assertThat(response.getContent().size(), is(5));

      response.getContent()
            .forEach(occurrence ->
            {
               assertThat(occurrence.isObfuscated(), is(true));
               assertThat(occurrence.getEntityId(), is(not(nullValue())));
               assertThat(occurrence.getDeterminer().getEntityId(),
                     is(Integer.MIN_VALUE));
            });
   }

   @Test
   @DisplayName("Test for approved user on portal with remove persons")
   void testFindObfuscated_no_policy_PERSONS_FOR_APPROVED_USERS_approved()
   {
      OccurrenceResource noAdminResource = createClient(
            UserTestUtil.LOGIN_SELLINSF, UserTestUtil.PASSWORD_SELLINSF)
                  .occurrences();
      OccurrenceSliceResponse response = noAdminResource.find(0,
            8,
            OccurrenceField.DETERMINER, SortOrder.ASC,
            new OccurrenceFilter(PortalTestUtil.HEUSCHRECKEN_D_ID,
                  null, null, null, null, null, null, true, null, null, null,
                  null, null, null, null),
            false);


      response.getContent()
            .forEach(occurrence ->
            {
               assertThat(occurrence.isObfuscated(), is(true));
               assertThat(occurrence.getEntityId(), is(not(nullValue())));
               assertThat(occurrence.getDeterminer().getEntityId(),
                     is(Integer.MIN_VALUE));
            });
   }

   @Test
   @Disabled("Rare usecase, works local, returns wired results in ci")
   void testFind_sortASC()
   {
      OccurrenceSliceResponse response = occurrenceResourceUT.find(0, 10,
            OccurrenceField.MOD_DATE, SortOrder.ASC,
            new OccurrenceFilter(4, null, null, null, null, null, null, true, null,
                  null, false, null, null, null, null),
            false);

      System.out.println(response.getContent());
      UUID uuid = response.getContent().get(0).getSample().getEntityId();

      assertThat(uuid,
            is(UUID.fromString("bdec3695-98ca-483f-9bb2-260891ffb1ec")));

   }

   @Test
   void testFind_sortDESC()
   {
      OccurrenceSliceResponse response = occurrenceResourceUT.find(0, 1,
            OccurrenceField.MOD_DATE, SortOrder.DESC,
            new OccurrenceFilter(4, null, null, null, null, null, null, true, null,
                  null, false, null, null, null, null),
            false);

      Occurrence occurrence = response.getContent().get(0);
      assertThat("Wrong sample uuid",
            occurrence.getSample().getEntityId(),
            is(UUID.fromString("bdec3695-98ca-483f-9bb2-260891ffb1ec")));
            assertThat("Wrong werbeo original",
                  occurrence.getSample().getSurvey().getWerbeoOriginal(),
            is(Boolean.TRUE));

   }

   @Test
   void testFind_invalidOffset()
   {
      OccurrenceFilter filter = new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID,
            null, null, null, null, null, null, true, null, null, false, null,
            null, null, null);
      assertThrows(BadRequestException.class,
            () -> occurrenceResourceUT.find(-99, 1, OccurrenceField.MOD_DATE,
                  SortOrder.DESC, filter, false));
   }

   @Test
   void testFind_FloraBB()
   {
      OccurrenceSliceResponse response = occurrenceResourceUT.find(0, 5,
            OccurrenceField.DETERMINER, SortOrder.ASC,
            new OccurrenceFilter(PortalTestUtil.FLORA_BB_ID, null, null, null,
                  null, null, null, true, null, null, null, null, null,
                  null, null),
            false);

      assertThat(response.getContent().size(), is(0));
      assertThat(response.getMetaData().getSessionId(), startsWith("i-test-"));
   }

   @Test
   void testCount_inclChildTaxa()
   {

      CountResponse response = occurrenceResourceUT
            .count(new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                  49401, null, null, null, true, null, null, false, null,
                  null,
                  null, null));

      assertThat(response.getAmount(), is(6L));

   }

   @Test
   void testCount_withoutChildTaxa()
   {

      CountResponse response = occurrenceResourceUT
            .count(new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                  49401, null, null, null, false, null, null, false, null,
                  null,
                  null, null));

      assertThat(response.getAmount(), is(1L));

   }

   @Test
   void testCreateHerbaryPage_known_UUID_BottomRight() throws IOException
   {
      try (Response response = occurrenceResourceUT.createHerbaryPage(4,
            UUID.fromString("bd715b7f-3cdc-4f6f-b5e9-6982bfd4efe7"),
            HerbarySheetPosition.BOTTOM_RIGHT, sheet);)
      {
         assertThat(response.getStatus(),
               is(Response.Status.OK.getStatusCode()));
         assertThat(response.readEntity(byte[].class).length > 0, is(true));
      }
   }

   @Test
   void testCreateHerbaryPage_known_UUID_BottomLeft() throws IOException
   {
      try (Response response = occurrenceResourceUT.createHerbaryPage(4,
            UUID.fromString("bd715b7f-3cdc-4f6f-b5e9-6982bfd4efe7"),
            HerbarySheetPosition.BOTTOM_LEFT, sheet);)
      {
         assertThat(response.getStatus(),
               is(Response.Status.OK.getStatusCode()));
         assertThat(response.readEntity(byte[].class).length > 0, is(true));
      }
   }

   @Test
   void testCreateHerbaryPage_known_UUID_TopRight() throws IOException
   {
      try (Response response = occurrenceResourceUT.createHerbaryPage(4,
            UUID.fromString("bd715b7f-3cdc-4f6f-b5e9-6982bfd4efe7"),
            HerbarySheetPosition.TOP_RIGHT, sheet);)
      {

         assertThat(response.getStatus(),
               is(Response.Status.OK.getStatusCode()));
         assertThat(response.readEntity(byte[].class).length > 0, is(true));
      }
   }

   @Test
   void testCreateHerbaryPage_known_UUID_TopLeft() throws IOException
   {
      try (Response response = occurrenceResourceUT.createHerbaryPage(4,
            UUID.fromString("bd715b7f-3cdc-4f6f-b5e9-6982bfd4efe7"),
            HerbarySheetPosition.TOP_LEFT, sheet);)
      {
         assertThat(response.getStatus(),
               is(Response.Status.OK.getStatusCode()));
         assertThat(response.readEntity(byte[].class).length > 0, is(true));
      }
   }

   @Test
   void testCreateHerbaryPage_unknown_UUID()
         throws EntityNotFoundException, IOException
   {
      try (Response response = occurrenceResourceUT.createHerbaryPage(
            PortalTestUtil.FLORA_MV_ID,
            UUID.randomUUID(), HerbarySheetPosition.TOP_LEFT, sheet);)
      {
         assertAll(() -> assertThat(response.getStatus(), is(500)),
               // assertThat(response.getStatus(), is(406));
               () -> assertThat(response.readEntity(byte[].class).length == 0,
                     is(true)));
      }
   }

   @Test
   void testGetOccurrenceCentroids()
   {
      // 34427 => Betula pubescens
      OccurrenceCentroidsSliceResponse response = occurrenceResourceUT
            .findOccurrenceCentroids(PortalTestUtil.FLORA_MV_ID, 0, 10, 34427);

      assertAll(() -> assertThat(response.getContent().size(), is(3)),
            () -> assertThat(response.getContent().get(0).getCentroid(),
                  is("POINT (4546025.019482303 6027264.896216604)")));
   }

   @Test
   void testFindHerbariumFilter()
   {
      OccurrenceSliceResponse response = occurrenceResourceUT.find(0, 5,
            OccurrenceField.DETERMINER, SortOrder.ASC,
            new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null, null,
                  null, null, null, true, null, null, false, "GFW", null, null, null),
            false);

      assertThat(response.getContent().size(), is(1));

      assertThat(response.getContent().get(0).getHerbarium().getHerbary(),
            is("1234"));
      assertThat(response.getContent().get(0).getHerbarium().getCode(),
            is("GFW"));
   }

   @Test
   void testFindWithMedia()
   {
      OccurrenceSliceResponse response = occurrenceResourceUT
            .find(0, 5, OccurrenceField.DETERMINER, SortOrder.ASC,
                  new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                        34376, null, null, null, true, null, null, false,
                        null,
                        null,
                        null, null),
                  false);

      Occurrence occurrence = response.getContent().get(0);

      assertThat(occurrence.getDocuments().size(), is(2));

      Document image = occurrence.getDocuments().stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE).findFirst()
            .get();
      Document imageThumb = occurrence.getDocuments().stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE_THUMB)
            .findFirst().get();

      assertThat(image.getLink().getHref(),
            endsWith("/indicia/upload/1581080334677.jpg"));
      assertThat(imageThumb.getLink().getHref(),
            endsWith("/indicia/upload/thumb-1581080334677.jpg"));
   }

   @Test
   void testFindWithMediaInlineThumb()
   {
      OccurrenceSliceResponse response = occurrenceResourceUT
            .find(0, 5, OccurrenceField.DETERMINER, SortOrder.ASC,
                  new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                        34376, null, null, null, true, null, null, false,
                        null,
                        null, null, null),
                  true);

      Occurrence occurrence = response.getContent().get(0);

      assertThat(occurrence.getDocuments().size(), is(2));

      Document image = occurrence.getDocuments().stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE).findFirst()
            .get();
      Document imageThumb = occurrence.getDocuments().stream()
            .filter(doc -> doc.getType() == DocumentType.IMAGE_THUMB)
            .findFirst().get();

      assertThat(image.getLink().getHref(),
            endsWith("/indicia/upload/1581080334677.jpg"));
      assertThat(imageThumb.getLink().getHref(),
            startsWith("data:image/jpeg;base64,/9j/4AA"));
   }
}
