package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

import org.infinitenature.service.v1.resources.TranslationResource;
import org.infinitenature.service.v1.types.meta.OccurrenceField;
import org.infinitenature.service.v1.types.meta.SampleField;
import org.infinitenature.service.v1.types.meta.SurveyField;
import org.infinitenature.service.v1.types.meta.TaxonField;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ITestRESTTranslationResource extends RestApiTest
{
   private TranslationResource translationResource;

   @BeforeEach
   void setUp()
   {
      translationResource = createClient().translationResource();
   }

   @Test
   void testGetTranslation()
   {
      String translation = translationResource.getTranslation(
            PortalTestUtil.FLORA_MV_ID, "message.export.csv", "DE");
      assertThat(translation, is("csv Export"));
   }

   @Test
   void testGetTaxonFieldTranslations()
   {
      for (TaxonField taxonField : TaxonField.values())
      {
         String translation = translationResource.getTranslation(
               PortalTestUtil.FLORA_MV_ID, taxonField.getTranslationKey(),
               "DE");
         assertThat("Translation for " + taxonField + " must be present",
               translation, allOf(not(startsWith("!")), not(endsWith("!"))));
      }
   }

   @Test
   void testGetSampleFieldTranslations()
   {
      for (SampleField sampleField : SampleField.values())
      {
         String translation = translationResource.getTranslation(
               PortalTestUtil.FLORA_MV_ID, sampleField.getTranslationKey(),
               "DE");
         assertThat("Translation for " + sampleField + " must be present",
               translation, allOf(not(startsWith("!")), not(endsWith("!"))));
      }
   }

   @Test
   void testGetSurveyFieldTranslations()
   {
      for (SurveyField surveyField : SurveyField.values())
      {
         String translation = translationResource.getTranslation(
               PortalTestUtil.FLORA_MV_ID, surveyField.getTranslationKey(),
               "DE");
         assertThat("Translation for " + surveyField + " must be present",
               translation, allOf(not(startsWith("!")), not(endsWith("!"))));
      }
   }

   @Test
   void testGetOccurrenceFieldTranslations()
   {
      for (OccurrenceField occurrenceField : OccurrenceField.values())
      {
         String translation = translationResource.getTranslation(
               PortalTestUtil.FLORA_MV_ID, occurrenceField.getTranslationKey(),
               "DE");
         assertThat("Translation for " + occurrenceField + " must be present",
               translation, allOf(not(startsWith("!")), not(endsWith("!"))));
      }
   }
}
