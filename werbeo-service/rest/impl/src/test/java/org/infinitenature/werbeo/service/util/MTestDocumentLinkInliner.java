package org.infinitenature.werbeo.service.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

class MTestDocumentLinkInliner
{

   @BeforeEach
   void setUp() throws Exception
   {
   }

   @Test
   void test()
   {
      DocumentLinkInliner documentLinkInliner = new DocumentLinkInliner(
            new RestTemplate());

      System.out.println(documentLinkInliner.createDataURL(
            "https://loe3.loe.auf.uni-rostock.de/indicia/upload/thumb-1571817911black.png"));
   }

}
