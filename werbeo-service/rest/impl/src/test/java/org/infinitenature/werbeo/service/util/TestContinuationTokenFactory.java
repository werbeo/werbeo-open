package org.infinitenature.werbeo.service.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.infinitenature.werbeo.service.core.api.enity.support.ContinuationToken;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestContinuationTokenFactory
{
   private ContinuationTokenFactory continationTokenFactoryUT = new ContinuationTokenFactory();

   @Test
   @DisplayName("try to parse empty string")
   void testParseToken001()
   {
      assertThrows(IllegalArgumentException.class, () ->
      {
         continationTokenFactoryUT.parseToken("");
      });
   }

   @Test
   @DisplayName("try to parse null string")
   void testParseToken002()
   {
      assertThrows(IllegalArgumentException.class, () ->
      {
         continationTokenFactoryUT.parseToken(null);
      });
   }

   @Test
   @DisplayName("try to parse non numeric token")
   void testParseToken003()
   {
      assertThrows(IllegalArgumentException.class, () ->
      {
         continationTokenFactoryUT.parseToken("abc");
      });
   }

   @Test
   @DisplayName("try to parse non numeric token part")
   void testParseToken004()
   {
      assertThrows(IllegalArgumentException.class, () ->
      {
         continationTokenFactoryUT.parseToken("abc_001");
      });
   }

   @Test
   @DisplayName("parse valid token")
   void testParseToken005()
   {
      assertThat(continationTokenFactoryUT.parseToken("1_1"),
            is(new ContinuationToken(1, 1)));
   }

   @Test
   @DisplayName("try to parse wrong delemiter")
   void testParseToken006()
   {
      assertThrows(IllegalArgumentException.class, () ->
      {
         continationTokenFactoryUT.parseToken("1-001");
      });
   }

   @Test
   @DisplayName("write first token")
   void testWriteToken001()
   {
      assertThat(continationTokenFactoryUT.write(ContinuationToken.FIRST),
            is("0_0"));
   }

   @Test
   @DisplayName("write last token")
   void testWriteToken002()
   {
      assertThat(
            continationTokenFactoryUT.write(
                  new ContinuationToken(Integer.MAX_VALUE, Long.MAX_VALUE)),
            is("2147483647_9223372036854775807"));
   }

   @Test
   @DisplayName("try to write null token")
   void testWriteToken003()
   {
      assertThrows(IllegalArgumentException.class, () ->
      {
         continationTokenFactoryUT.write(null);
      });
   }
}
