package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

import javax.ws.rs.ForbiddenException;

import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.types.response.TermsAndConditonsAcceptResponse;
import org.infinitenature.werbe.test.support.*;
import org.infinitenature.werbeo.service.support.RestApiTest;
import org.junit.jupiter.api.*;;

public class ITestRESTTermsAndConditionsResource extends RestApiTest
{

   @Test
   public void testGetTermsAndConditions()
   {
      String tcMV = createClient(UserTestUtil.LOGIN_CTOMALAA,
            UserTestUtil.PASSWORD_CTOMALAA).termsAndConditions()
                  .getConditions(PortalTestUtil.FLORA_MV_ID)
                  .getText();
      assertThat(tcMV, startsWith("Bestimmungen"));
   }

   @Test
   public void testGetTermsAndConditions_notYetAccepted()
   {
      String tcMV = createClient(UserTestUtil.LOGIN_NO_TC,
            UserTestUtil.PASSWORD_NO_TC).termsAndConditions()
                  .getConditions(PortalTestUtil.FLORA_MV_ID)
                  .getText();
      assertThat(tcMV, startsWith("Bestimmungen"));
   }

   @Test
   public void testGetTermsAndConditions_annon()
   {
      String tcMV = createClient().termsAndConditions()
            .getConditions(PortalTestUtil.FLORA_MV_ID)
            .getText();
      assertThat(tcMV, startsWith("Bestimmungen"));
   }

   @Test
   public void testAcceptTermsAndConditions_alreadyAccepted()
   {
      TermsAndConditonsAcceptResponse tc = createClient(
            UserTestUtil.LOGIN_CTOMALAA, UserTestUtil.PASSWORD_CTOMALAA)
                  .termsAndConditions().accept(PortalTestUtil.FLORA_MV_ID);
      assertThat(tc.isAccepted(), is(true));
   }

   @Test
   @Disabled
   public void testAcceptTermsAndConditions_notYetAccepted()
   {
      try
      {
         createClient(UserTestUtil.LOGIN_ACCEPT_TC,
               UserTestUtil.PASSWORD_ACCEPT_TC).occurrences().count(
               new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                     null, null, null, null, false, null, null, false, null, null, null, null));
         fail("Should be forbidden, because terms and conditions are not accepted yet");
      } catch (ForbiddenException e)
      {
         // this should happen
      }

      TermsAndConditonsAcceptResponse tc = createClient(
            UserTestUtil.LOGIN_ACCEPT_TC, UserTestUtil.PASSWORD_ACCEPT_TC)
            .termsAndConditions().accept(PortalTestUtil.FLORA_MV_ID);
      assertThat(tc.isAccepted(), is(true));

      try
      {
         createClient(UserTestUtil.LOGIN_ACCEPT_TC,
               UserTestUtil.PASSWORD_ACCEPT_TC).occurrences().count(
               new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null,
                     null, null, null, null, false, null, null, false, null, null, null, null));
         fail("Should be forbidden, because privacy is not accepted yet");
      } catch (ForbiddenException e)
      {
         // this should happen
      }

    

      createClient(UserTestUtil.LOGIN_ACCEPT_TC,
            UserTestUtil.PASSWORD_ACCEPT_TC).occurrences().count(
            new OccurrenceFilter(PortalTestUtil.FLORA_MV_ID, null, null, null,
                  null, null, null, false, null, null, false, null, null, null, null));
   }

   @Test
   public void testAcceptTermsAndConditions_annon()
   {
      assertThrows(ForbiddenException.class, () -> createClient()
            .termsAndConditions().accept(PortalTestUtil.FLORA_MV_ID));
   }
}
