package org.infinitenature.werbeo.service.v1.resource.securitiy;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.infinitenature.service.v1.resources.security.TokenResource;
import org.infinitenature.service.v1.types.support.UserAuth;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.ServiceApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Import(ServiceApp.class)
public class TestTokenResource
{

   @Autowired
   private TokenResource tokenResurceUT;

   @Test
   public void test()
   {
      UserAuth userAuth = new UserAuth();
      userAuth.setEmail(UserTestUtil.LOGIN_CTOMALAA);
      userAuth.setPassword(UserTestUtil.PASSWORD_CTOMALAA);

      assertThat(tokenResurceUT.getToken(userAuth).getToken().getAccessToken(),
            is(not(nullValue())));

   }

}
