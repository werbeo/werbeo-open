package org.infinitenature.werbeo.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import java.util.Collection;
import java.util.HashSet;

import org.infinitenature.service.v1.types.Document;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.DocumentType;
import org.infinitenature.werbeo.service.core.api.enity.Portal;
import org.infinitenature.werbeo.service.core.api.enity.TaxonBase;
import org.infinitenature.werbeo.service.core.api.enity.User;
import org.infinitenature.werbeo.service.core.api.support.Context;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestTaxonLinksMapper
{
   @Test
   @DisplayName("no links")
   void test001()
   {
      TaxonLinksMapper taxonLinksMapper = new TaxonLinksMapper(
            new InstanceConfig(), new DocumentTypeMapperRestImpl());

      Collection<Document> links = taxonLinksMapper.createDocuments(
            new TaxonBase(),
            new Context(new Portal(1), new User(), new HashSet<>(), "test"));
      assertThat(links, is(emptyCollectionOf(Document.class)));
   }

   @Test
   @DisplayName("only thumbnail")
   void test002()
   {
      TaxonLinksMapper taxonLinksMapper = new TaxonLinksMapper(
            new InstanceConfig(), new DocumentTypeMapperRestImpl());

      TaxonBase taxon = new TaxonBase();
      taxon.setName("A Taxon");
      taxon.getDocumentsAvailable()
            .add(DocumentType.MAP_TAXON_VEGETWEB_STYLE_THUMB);
      Collection<Document> links = taxonLinksMapper.createDocuments(taxon,
            new Context(new Portal(1), new User(), new HashSet<>(), "test"));

      assertThat(links, hasSize(1));
      Document thumbLink = links.iterator().next();
      assertThat(thumbLink.getType(), is(
            org.infinitenature.service.v1.types.DocumentType.MAP_TAXON_VEGETWEB_STYLE_THUMB));
      assertThat(thumbLink.getLink().getRel(), is("self"));
      assertThat(thumbLink.getLink().getHref(), is(
            "https://loe3.loe.auf.uni-rostock.de/maps/1/vegetweb-style/A Taxon_thumb.png"));
   }

   @Test
   @DisplayName("only map")
   void test003()
   {
      TaxonLinksMapper taxonLinksMapper = new TaxonLinksMapper(
            new InstanceConfig(), new DocumentTypeMapperRestImpl());

      TaxonBase taxon = new TaxonBase();
      taxon.setName("A Taxon");
      taxon.getDocumentsAvailable().add(DocumentType.MAP_TAXON_VEGETWEB_STYLE);
      Collection<Document> links = taxonLinksMapper.createDocuments(taxon,
            new Context(new Portal(1), new User(), new HashSet<>(), "test"));

      assertThat(links, hasSize(1));
      Document thumbLink = links.iterator().next();
      assertThat(thumbLink.getType(), is(
            org.infinitenature.service.v1.types.DocumentType.MAP_TAXON_VEGETWEB_STYLE));
      assertThat(thumbLink.getLink().getRel(), is("self"));
      assertThat(thumbLink.getLink().getHref(), is(
            "https://loe3.loe.auf.uni-rostock.de/maps/1/vegetweb-style/A Taxon.png"));
   }
}
