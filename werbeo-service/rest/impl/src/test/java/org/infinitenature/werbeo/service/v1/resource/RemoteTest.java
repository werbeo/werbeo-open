package org.infinitenature.werbeo.service.v1.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.apache.commons.lang3.StringUtils;
import org.geotools.geometry.GeometryBuilder;
import org.geotools.geometry.iso.text.WKTParser;
import org.geotools.referencing.CRS;
import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.resources.security.TokenResource;
import org.infinitenature.service.v1.types.Occurrence.RecordStatus;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.VagueDate.VagueDateType;
import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.TaxonBase;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.support.UserAuth;
import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbe.test.support.PortalTestUtil;
import org.infinitenature.werbe.test.support.UserTestUtil;
import org.infinitenature.werbeo.client.Werbeo;
import org.infinitenature.werbeo.common.commons.coordinate.impl.GeometryHelper;
import org.junit.jupiter.api.Test;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

public class RemoteTest
{
   private SamplesResource samplesResourceUT;
   private TaxaResource taxaResourceUT;
   private TokenResource tokenResurceUT;
   protected Werbeo client;

   public void init()
   {
      tokenResurceUT = new Werbeo("https://api.test.infinitenature.org/api").token();
      client = createClient(UserTestUtil.LOGIN_HOGEN,
            UserTestUtil.PASSWORD_HOGEN);
      samplesResourceUT = client.samples();
      taxaResourceUT = client.taxa();
      
   }
   
   protected Werbeo createClient(String email, String password)
   {
      UserAuth userAuth = new UserAuth();
      userAuth.setEmail(email);
      userAuth.setPassword(password);

      String token = tokenResurceUT.getToken(userAuth).getToken()
            .getAccessToken();

      return new Werbeo("https://api.test.infinitenature.org/api", new ClientRequestFilter()
      {
         @Override
         public void filter(ClientRequestContext requestContext)
               throws IOException
         {
            requestContext.getHeaders().add("Authorization",
                  "Bearer " + token);
         }
      });
   }

   @Test
   public void testSaveMulti_Square()
   {
      init();

         Sample sample = new Sample();

         UUID sampleID = UUID.randomUUID();

         sample.setEntityId(sampleID);

         Position position = new Position();
         position.setWkt(GeomTestUtil.WKT_MULTIPOLYGON_SQUARES_ERROR_5650);
         position.setWktEpsg(5650);
         position.setType(PositionType.SHAPE);

         Locality locality = new Locality();
         sample.setLocality(locality);
         sample.getLocality().setPosition(position);

         sample.getLocality().setBlur(4);

         VagueDate date = createTodayMinus(5);
         sample.setDate(date);

         SurveyBase survey = createDemoSurvey();
         sample.setSurvey(survey);

         Person person = createPerson();
         sample.setRecorder(person);

         Occurrence occurrence = new Occurrence();
         occurrence.setRecordStatus(RecordStatus.COMPLETE);
         occurrence.setDeterminer(person);

         TaxonBase urtica = utritca();
         occurrence.setTaxon(urtica);
         sample.getOccurrences().add(occurrence);

      
         samplesResourceUT.save(5, sample);
         

//         assertThat(loaded.getLocality().getPosition().getWktEpsg(), is(5650));
//         assertThat(loaded.getLocality().getPosition().getWkt(), is("POLYGON ((13.33920478820801 54.1302092867478, 13.336973190307617 54.12870049692955, 13.342466354370119 54.12739283464121, 13.344526290893555 54.129002259288896, 13.33920478820801 54.1302092867478))"));
         System.out.println();
      
      
      

   }
   
   
   @Test
   public void testSaveMulti_Brace()
   {
      init();
      
      GeometryHelper geometryHelper = new GeometryHelper();
      geometryHelper.getCenter(GeomTestUtil.WKT_MULTIPOLYGON_BRACE_ERROR_5650, 5650);

         Sample sample = new Sample();

         UUID sampleID = UUID.randomUUID();

         sample.setEntityId(sampleID);

         Position position = new Position();
         position.setWkt(GeomTestUtil.WKT_MULTIPOLYGON_BRACE_ERROR_5650);
         position.setWktEpsg(5650);
         position.setType(PositionType.SHAPE);

         Locality locality = new Locality();
         sample.setLocality(locality);
         sample.getLocality().setPosition(position);

         sample.getLocality().setBlur(4);

         VagueDate date = createTodayMinus(5);
         sample.setDate(date);

         SurveyBase survey = createDemoSurvey();
         sample.setSurvey(survey);

         Person person = createPerson();
         sample.setRecorder(person);

         Occurrence occurrence = new Occurrence();
         occurrence.setRecordStatus(RecordStatus.COMPLETE);
         occurrence.setDeterminer(person);

         TaxonBase urtica = utritca();
         occurrence.setTaxon(urtica);
         sample.getOccurrences().add(occurrence);

      
         samplesResourceUT.save(5, sample);
         

//         assertThat(loaded.getLocality().getPosition().getWktEpsg(), is(5650));
//         assertThat(loaded.getLocality().getPosition().getWkt(), is("POLYGON ((13.33920478820801 54.1302092867478, 13.336973190307617 54.12870049692955, 13.342466354370119 54.12739283464121, 13.344526290893555 54.129002259288896, 13.33920478820801 54.1302092867478))"));
         System.out.println();
      
      
      

   }
   
   protected TaxonBase utritca()
   {
      TaxonBase urtica = new TaxonBase();
      urtica.setEntityId(34364);
      return urtica;
   }
   
   protected VagueDate createTodayMinus(int days)
   {
      VagueDate date = new VagueDate();
      date.setFrom(LocalDate.now().minusDays(days));
      date.setType(VagueDateType.DAY);
      return date;
   }

   protected SurveyBase createDemoSurvey()
   {
      SurveyBase survey = new SurveyBase();
      survey.setEntityId(26922);
      return survey;
   }

   protected Person createPerson()
   {
      Person person = new Person();
      person.setEntityId(4);
      return person;
   }

}
