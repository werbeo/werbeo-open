package org.infinitenature.werbeo.service.mapper;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.List;

import org.infinitenature.service.v1.types.Document;
import org.infinitenature.werbeo.service.config.InstanceConfig;
import org.infinitenature.werbeo.service.core.api.enity.MediaType;
import org.infinitenature.werbeo.service.core.api.enity.Medium;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestMediaMapperRest
{
   private MediaMapperRest mapperUT;

   @BeforeEach
   void setUp() throws Exception
   {
      mapperUT = new MediaMapperRestImpl();
      InstanceConfig instanceConfig = new InstanceConfig();
      instanceConfig.setLocalMediaURL("https://local.indicia.loe.de/media");
      MediaPathUtils mediaPathUtils = new MediaPathUtils();
      mediaPathUtils.setInstanceConfig(instanceConfig);
      mapperUT.setMediaPathUtils(mediaPathUtils);
   }

   @Test
   @DisplayName("Map local image")
   void test001()
   {
      Medium medium = new Medium("path.png", "local image",
            MediaType.IMAGE_LOCAL);
      List<Document> documents = mapperUT.map(medium);

      assertAll(() -> assertThat(documents, hasSize(2)),
            () -> assertThat(documents.get(0).getLink().getHref(),
                  is("https://local.indicia.loe.de/media/path.png")),
            () -> assertThat(documents.get(1).getLink()
                  .getHref(),
                  is("https://local.indicia.loe.de/media/thumb-path.png")));
   }

   @Test
   @DisplayName("Map local audio")
   void test002()
   {
      Medium medium = new Medium("path.mp3",
            "local audio",
            MediaType.AUDIO_LOCAL);
      List<Document> documents = mapperUT.map(medium);
      assertAll(() -> assertThat(documents, hasSize(1)),
            () -> assertThat(documents.get(0).getLink().getHref(),
                  is("https://local.indicia.loe.de/media/path.mp3")));
   }

}
