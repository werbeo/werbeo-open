package org.infinitenature.werbeo.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.werbe.test.support.GeomTestUtil;
import org.infinitenature.werbeo.service.common.position.impl.PositionFactoryImpl;
import org.infintenature.mtb.MTBHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class TestPositionMapper
{
   private PositionMapper positionMapperUT;

   @BeforeEach
   void setUp() throws Exception
   {
      PositionMapperImpl positionMapper = new PositionMapperImpl();
      positionMapper.setPositionFactory(new PositionFactoryImpl());
      positionMapperUT = positionMapper;
   }

   @Test
   @DisplayName("Map shape position to core api")
   void testToCoreApi()
   {
      Position position = new Position();
      position.setWkt(GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326);
      position.setWktEpsg(4326);
      position.setType(PositionType.SHAPE);

      org.infinitenature.werbeo.service.core.api.enity.Position coreApiPosition = positionMapperUT
            .toCoreApi(position);

      assertThat(coreApiPosition.getMtb(), is(MTBHelper.toMTB("1846/313")));
      assertThat(coreApiPosition.getType(), is(
            org.infinitenature.werbeo.service.core.api.enity.Position.PositionType.SHAPE));
      assertThat(coreApiPosition.getEpsg(), is(4326));
      assertThat(coreApiPosition.getWktEpsg(), is(4326));
      assertThat(coreApiPosition.getWkt(),
            is(GeomTestUtil.WKT_KIESHOFER_MOOR_APROX_4326));
   }

   @Test
   @DisplayName("center longitude, latitude to rest api")
   void testToRestApi()
   {
      org.infinitenature.werbeo.service.core.api.enity.Position position = new org.infinitenature.werbeo.service.core.api.enity.Position();
      position.setPosCenter(new double[]{11.0,53.0});

      Position positionRest = positionMapperUT.toRestApi(position);

      assertThat(positionRest.getPosCenterLatitude(), is(53.0));
      assertThat(positionRest.getPosCenterLongitude(), is(11.0));
   }

}
