package org.infinitenature.service.v1.resources.mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.Min;
import javax.ws.rs.NotFoundException;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.SurveyFilter;
import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.SurveySortField;
import org.infinitenature.service.v1.types.meta.SurveyFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;
import org.infinitenature.service.v1.types.response.SurveyBaseResponse;
import org.infinitenature.service.v1.types.response.SurveyFieldConfigResponse;
import org.infinitenature.service.v1.types.response.SurveysSliceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SurveyResourceMock implements SurveysResource
{
   private static final Logger LOGGER = LoggerFactory
         .getLogger(SurveyResourceMock.class);
   private MockData data;
   public SurveyResourceMock(MockData data)
   {
      this.data = data;
   }

   @Override
   public SaveOrUpdateResponse save(@Min(1) int portalId, SurveyBase survey)
   {
      LOGGER.info("Saved {}", survey);
      return null;
   }

   @Override
   public SurveyBaseResponse getSurvey(@Min(1) int portalId, int id)
   {
      Optional<SurveyBase> optional = data.getSurveys().stream()
            .filter(s -> s.getEntityId().equals(id)).findAny();
      if (optional.isPresent())
      {
         return new SurveyBaseResponse(optional.get());
      } else
      {
         throw new NotFoundException();
      }

   }

   @Override
   public SurveysSliceResponse find(@Min(0) int offset, int limit,
         SurveySortField sortField, SortOrder sortOrder, SurveyFilter filter)
   {
      return new SurveysSliceResponse(Collections.emptyList(), 0);
   }

   @Override
   public CountResponse count(SurveyFilter filter)
   {
      return new CountResponse(0l, "mock-data");
   }

   @Override
   public SurveyFieldConfigResponse getFieldConfig(@Min(1) int portalId)
   {
      return new SurveyFieldConfigResponse(Arrays
            .asList(
                  org.infinitenature.service.v1.types.meta.SurveyField.values())
            .stream().map(field -> new SurveyFieldConfig(field, true))
            .collect(Collectors.toSet()));
   }
}
