package org.infinitenature.service.v1.resources.mock;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceExportResource;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceFilterPOST;
import org.infinitenature.service.v1.types.ExportFormat;
import org.infinitenature.service.v1.types.OccurrenceExportField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportStatusResponse;
import org.infinitenature.service.v1.types.response.OccurrenceExportsInfoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.util.UUID;

public class OccurrenceExportResourceMock implements OccurrenceExportResource
{

   private static final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceExportResourceMock.class);
   private final MockData data;

   public OccurrenceExportResourceMock(MockData data)
   {
      super();
      this.data = data;
   }

   @Override
   public OccurrenceExportStatusResponse getStatus(UUID exportId,
         @Min(1) int portalId)
   {
      return null;
   }

   @Override
   public Response getExport(UUID exportId, @Min(1) int portalId)
   {
      return null;
   }

   @Override
   public OccurrenceExportsInfoResponse findExports(@Min(1) int portalId,
         @Min(0) int offset, int limit, OccurrenceExportField sortField,
         SortOrder sortOrder)
   {
      return null;
   }

   @Override
   public OccurrenceExportResponse create(OccurrenceFilter filter,
         @NotNull ExportFormat exportFormat)
   {
      UUID id = UUID.randomUUID();
      LOGGER.info("Create new export with ID {}, Filter {} for {}", id, filter,
            exportFormat);
      return new OccurrenceExportResponse(id);
   }
   
   @Override
   public OccurrenceExportResponse createLargeWKT(OccurrenceFilterPOST filter,
         @NotNull ExportFormat exportFormat)
   {
      UUID id = UUID.randomUUID();
      LOGGER.info("Create new export with ID {}, Filter {} for {}", id, filter,
            exportFormat);
      return new OccurrenceExportResponse(id);
   }

   @Override
   public DeletedResponse delete(UUID exportId, @Min(1) int portalId)
   {
      return null;
   }

   @Override
   public CountResponse count(@Min(1) int portalId)
   {
      return null;
   }
}
