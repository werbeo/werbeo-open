package org.infinitenature.service.v1.resources.mock;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.OccurrenceFilterPOST;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.types.HerbarySheet;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.service.v1.types.enums.HerbarySheetPosition;
import org.infinitenature.service.v1.types.meta.OccurrenceFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.OccurrenceCentroidsSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceFieldConfigResponse;
import org.infinitenature.service.v1.types.response.OccurrenceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceSliceResponse;
import org.infinitenature.service.v1.types.response.OccurrenceStreamResponse;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformer;
import org.infinitenature.werbeo.common.commons.coordinate.CoordinateTransformerFactory;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OccurrenceResourceMock implements OccurrenceResource
{

   private final Logger LOGGER = LoggerFactory
         .getLogger(OccurrenceResourceMock.class);
   private final MockData data;

   private final CoordinateTransformerFactory coordinateTransformerFactory = new CoordinateTransformerFactory();

   public OccurrenceResourceMock(MockData data)
   {
      this.data = data;
   }

   @Override
   public CountResponse count(OccurrenceFilter filter)
   {
      // TODO Auto-generated method stub

      return new CountResponse(data.getOccurrences().stream()
            .filter(occ -> matches(occ, filter)).count(), "mock-data");
   }

   @Override
   public OccurrenceResponse get(int portalId, UUID uuid)
   {
      Optional<Occurrence> occurrence = data.getOccurrences().stream()
            .filter(occ -> occ.getEntityId().equals(uuid)).findFirst();

      OccurrenceResponse occurrenceResponse = new OccurrenceResponse(
            occurrence.orElseThrow(() -> new NotFoundException()));
      LOGGER.info("response: {}", occurrenceResponse);
      return occurrenceResponse;
   }

   @Override
   public Response createHerbaryPage(int portalId, UUID uuid,
         HerbarySheetPosition position, HerbarySheet sheet)
   {
      return null;
   }

   @Override
   public OccurrenceCentroidsSliceResponse findOccurrenceCentroids(
         @Min(1) int portalId, @Min(0) int offset, int limit,
         int taxonMeaningId)
   {
      return null;
   }

   @Override
   public CountResponse countOccurenceCentroids(@Min(1) int portalId,
         @Min(1) int taxonId)
   {
      return null;
   }

   @Override
   public OccurrenceSliceResponse find(int offset, int limit,
         OccurrenceField sortField, SortOrder sortOrder,
         OccurrenceFilter filter, boolean inlineThumbs)
   {
      LOGGER.info(
            " offset={}, limit={}, OccurrenceField sortField={}, sortOrder={}, filter={}",
            offset, limit, sortField, sortOrder, filter);
      List<Occurrence> toReturn = data.getOccurrences().stream()
            .filter(occ -> matches(occ, filter))
            .sorted((Occurrence o1, Occurrence o2) -> compare(o1, o2, sortField,
                  sortOrder))
            .skip(offset).limit(limit).collect(Collectors.toList());

      OccurrenceSliceResponse occurrenceSliceResponse = new OccurrenceSliceResponse(
            toReturn, data.getOccurrences().size());
      LOGGER.info("response: {}", occurrenceSliceResponse);
      return occurrenceSliceResponse;
   }

   private int compare(Occurrence o1, Occurrence o2, OccurrenceField sortField,
         SortOrder sortOrder)
   {
      switch (sortField)
      {
      case MOD_DATE:
         return sortOrder == SortOrder.ASC
               ? o1.getModificationDate().compareTo(o2.getModificationDate())
               : o2.getModificationDate().compareTo(o1.getModificationDate());
      default:
         return 0;
      }
   }

   private boolean matches(Occurrence occ, OccurrenceFilter filter)
   {
      if (filter.getTaxonId() != null
            && occ.getTaxon().getEntityId() != filter.getTaxonId())
      {
         return false;
      }
      if (StringUtils.isNotBlank(filter.getMtb())
            && !occ.getSample().getLocality().getPosition().getMtb().getMtb()
                  .startsWith(filter.getMtb()))
      {
         return false;
      }
      if (StringUtils.isNoneBlank(filter.getWkt()))
      {
         String occWkt = occ.getSample().getLocality().getPosition().getWkt();
         CoordinateTransformer coordinateTransformer = coordinateTransformerFactory
               .getCoordinateTransformer(
                     occ.getSample().getLocality().getPosition().getWktEpsg(),
                     4326);
         String occWkt4326 = coordinateTransformer.convert(occWkt);
         WKTReader wktReader = new WKTReader(new GeometryFactory());

         try
         {
            Geometry sampleGeom = wktReader.read(occWkt4326);
            Geometry filterGeom = wktReader.read(filter.getWkt());
            if (!sampleGeom.within(filterGeom))
            {
               return false;
            }
         } catch (ParseException e)
         {
            LOGGER.error("Failure comparing geom", e);
         }
      }
      return true;
   }

   @Override
   public OccurrenceStreamResponse stream(int portalId, String token)
   {
      return null;
   }

   @Override
   public OccurrenceFieldConfigResponse getFieldConfig(@Min(1) int portalId)
   {
      return new OccurrenceFieldConfigResponse(Arrays.asList(
            org.infinitenature.service.v1.types.meta.OccurrenceField.values())
            .stream()
            .filter(field -> !field.equals(
                  org.infinitenature.service.v1.types.meta.OccurrenceField.AMOUNT))
            .map(field -> new OccurrenceFieldConfig(field, true))
            .collect(Collectors.toSet()));
   }

   @Override
   public OccurrenceFieldConfigResponse getFieldConfig(@Min(1) int portalId,
         @Min(1) int surveyId)
   {
      return new OccurrenceFieldConfigResponse(Arrays.asList(
            org.infinitenature.service.v1.types.meta.OccurrenceField.values())
            .stream()
            .filter(field -> !field.equals(
                  org.infinitenature.service.v1.types.meta.OccurrenceField.AMOUNT))
            .map(field -> new OccurrenceFieldConfig(field, true))
            .collect(Collectors.toSet()));
   }

   @Override
   public OccurrenceResponse validate(@Min(1) int portalId, UUID uuid,
         Validation validationStatus)
   {
      return get(portalId, uuid);
   }

   @Override
   public OccurrenceSliceResponse findPOST(@Min(0) int offset, int limit,
         OccurrenceField sortField, SortOrder sortOrder,
         @Valid OccurrenceFilterPOST filter, boolean inlineThumbs)
   {
      LOGGER.info(
            " offset={}, limit={}, OccurrenceField sortField={}, sortOrder={}, filter={}",
            offset, limit, sortField, sortOrder, filter);
      List<Occurrence> toReturn = data.getOccurrences().stream()
            .filter(occ -> matches(occ, filter))
            .sorted((Occurrence o1, Occurrence o2) -> compare(o1, o2, sortField,
                  sortOrder))
            .skip(offset).limit(limit).collect(Collectors.toList());

      OccurrenceSliceResponse occurrenceSliceResponse = new OccurrenceSliceResponse(
            toReturn, data.getOccurrences().size());
      LOGGER.info("response: {}", occurrenceSliceResponse);
      return occurrenceSliceResponse;
   }

   @Override
   public CountResponse countPOST(@Valid OccurrenceFilterPOST filter)
   {
   // TODO Auto-generated method stub

      return new CountResponse(data.getOccurrences().stream()
            .filter(occ -> matches(occ, filter)).count(), "mock-data");
   }
}
