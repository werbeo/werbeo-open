package org.infinitenature.service.v1.resources.mock;

import java.util.List;

import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SampleBase;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SampleMapper
{
   SampleMapper INSTANCE = Mappers.getMapper(SampleMapper.class);
   public SampleBase toBase(Sample sample);

   public List<SampleBase> toBase(List<Sample> samples);
}
