package org.infinitenature.service.v1.resources.mock;

import org.infinitenature.service.v1.resources.OccurrenceExportResource;
import org.infinitenature.service.v1.resources.OccurrenceResource;
import org.infinitenature.service.v1.resources.PeopleResource;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.resources.SurveysResource;
import org.infinitenature.service.v1.resources.TaxaResource;

public class MockFactory
{
   private MockData data = new MockData();
   private OccurrenceResource occurrenceResource = new OccurrenceResourceMock(
         data);
   private TaxaResource taxaResource = new TaxaResourceMock(data);
   private SurveysResource surveysResource = new SurveyResourceMock(data);
   private SamplesResource samplesResource = new SampleResourceMock(data);
   private OccurrenceExportResource occurrenceExportResource = new OccurrenceExportResourceMock(
         data);
   private PortalResource portalResource = new PortalResourceMock(data);
   private PeopleResource peopleResource = new PeopleResourceMock(data);
   public OccurrenceResource getOccurrenceResource()
   {
      return occurrenceResource;
   }

   public TaxaResource getTaxaResource()
   {
      return taxaResource;
   }

   public SurveysResource getSurveysResource()
   {
      return surveysResource;
   }

   public MockData getData()
   {
      return data;
   }

   public SamplesResource getSamplesResource()
   {
      return samplesResource;
   }

   public OccurrenceExportResource getOccurrenceExportResource()
   {
      return occurrenceExportResource;
   }

   public PortalResource getPortalResource()
   {
      return portalResource;
   }

   public PeopleResource getPeopleResource()
   {
      return peopleResource;
   }
}
