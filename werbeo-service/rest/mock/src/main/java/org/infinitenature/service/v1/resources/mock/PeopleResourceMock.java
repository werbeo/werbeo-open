package org.infinitenature.service.v1.resources.mock;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.validation.constraints.Min;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.PeopleFilter;
import org.infinitenature.service.v1.resources.PeopleResource;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.PersonField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.PersonResponse;
import org.infinitenature.service.v1.types.response.PersonSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;

public class PeopleResourceMock implements PeopleResource
{
   private final MockData data;

   public PeopleResourceMock(MockData data)
   {
      this.data = data;
   }

   @Override
   public PersonSliceResponse find(@Min(0) int offset, int limit,
         PersonField sortField, SortOrder sortOrder,
         PeopleFilter parameterObject)
   {
      return new PersonSliceResponse(filteredStream(parameterObject)
            .skip(offset).limit(limit).collect(Collectors.toList()), 0);
   }

   @Override
   public PersonResponse getPerson(@Min(1) int portalId, int id)
   {
      return null;
   }

   @Override
   public CountResponse count(PeopleFilter peopleFilter)
   {
      return new CountResponse(filteredStream(peopleFilter).count(),
            "mock-data");
   }

   @Override
   public SaveOrUpdateResponse save(@Min(1) int portalId, Person person)
   {
      return null;
   }

   Stream<Person> filteredStream(PeopleFilter filter)
   {
      Stream<Person> stream = data.getPeople().stream();
      if (StringUtils.isNotBlank(filter.getNameContains()))
      {
         stream = stream.filter(
               person -> StringUtils.containsIgnoreCase(person.getFirstName(),
                     filter.getNameContains())
                     || StringUtils.containsIgnoreCase(person.getLastName(),
                           filter.getNameContains()));
      }
      return stream;
   }

}
