package org.infinitenature.service.v1.resources.mock;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Min;
import javax.ws.rs.NotFoundException;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.PortalResource;
import org.infinitenature.service.v1.types.PortalBase;
import org.infinitenature.service.v1.types.PortalField;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.FilterResponse;
import org.infinitenature.service.v1.types.response.PortalBaseResponse;
import org.infinitenature.service.v1.types.response.PortalConfigResponse;
import org.infinitenature.service.v1.types.response.PortalSliceResponse;

public class PortalResourceMock implements PortalResource
{
   private final MockData data;

   public PortalResourceMock(MockData data)
   {
      super();
      this.data = data;
   }

   @Override
   public PortalBaseResponse getPortal(@Min(1) int portalId)
   {
      return new PortalBaseResponse(
            data.getPortals().stream().filter(p -> p.getEntityId() == portalId)
                  .findFirst().orElseThrow(() -> new NotFoundException()));
   }

   @Override
   public PortalConfigResponse getPortalConfiguration(@Min(1) int portalId)
   {
      return new PortalConfigResponse(data.getPortals().stream()
            .filter(p -> p.getEntityId() == portalId).map(p -> p.getConfig())
            .findFirst().orElseThrow(() -> new NotFoundException()));
   }

   @Override
   public FilterResponse getFilter(@Min(1) int portalId)
   {
      return null;
   }

   @Override
   public CountResponse count()
   {
      return new CountResponse(data.getPortals().size(), "mock-data");
   }

   @Override
   public PortalSliceResponse findPortals(@Min(0) int offset, int limit,
         PortalField sortField, SortOrder sortOrder)
   {
      List<PortalBase> content = data.getPortals().stream().skip(offset)
            .limit(limit).collect(Collectors.toList());
      return new PortalSliceResponse(content, content.size());
   }

   @Override
   public PortalSliceResponse findAssociatedPortals(@Min(1) int portalId)
   {
      return null;
   }
}
