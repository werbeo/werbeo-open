package org.infinitenature.service.v1.resources.mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Min;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.resources.TaxaFilter;
import org.infinitenature.service.v1.resources.TaxaResource;
import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.MTBResponse;
import org.infinitenature.service.v1.types.response.TaxonBaseSliceResponse;
import org.infinitenature.service.v1.types.response.TaxonFieldConfigResponse;
import org.infinitenature.service.v1.types.response.TaxonResponse;
import org.infinitenature.service.v1.types.response.TaxonSliceResponse;

public class TaxaResourceMock implements TaxaResource
{
   private final MockData data;

   public TaxaResourceMock(MockData data)
   {
      super();
      this.data = data;
   }

   @Override
   public TaxonResponse getTaxon(int portalId, int id)
   {
      return new TaxonResponse(data.getTaxa().stream()
            .filter(t -> t.getEntityId() == id).findFirst().get());
   }

   @Override
   public TaxonBaseSliceResponse find(int offset, int limit,
         TaxaFilter parameterObject)
   {
      List<Taxon> filtered = data.getTaxa().stream()
            .filter(t -> StringUtils.containsIgnoreCase(t.getName(),
                  parameterObject.getNameContains()))
            .collect(Collectors.toList());
      return new TaxonBaseSliceResponse(new ArrayList<>(filtered), filtered.size());
   }

   @Override
   public CountResponse count(TaxaFilter parameterObject)
   {
      return new CountResponse(data.getTaxa().size(), "mock-data");
   }

   @Override
   public TaxonSliceResponse findAllSynonyms(@Min(1) int portalId, int id)
   {
      return new TaxonSliceResponse();
   }

   @Override
   public MTBResponse getCoveredMTB(@Min(1) int portalId, int id,
         boolean inclusiveChildTaxa)
   {
      return new MTBResponse(
            new HashSet<>(Arrays.asList(new MTB("1943"), new MTB("1942"))));
   }

   @Override
   public TaxonFieldConfigResponse getFieldConfig(@Min(1) int portalId)
   {
      return new TaxonFieldConfigResponse();
   }

   @Override
   public TaxonFieldConfigResponse getFieldConfig(@Min(1) int portalId,
         @Min(1) int surveyId)
   {
      return null;
   }
}
