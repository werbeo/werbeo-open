package org.infinitenature.service.v1.resources.mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.constraints.Min;

import org.infinitenature.commons.pagination.SortOrder;
import org.infinitenature.service.v1.resources.OccurrenceFilter;
import org.infinitenature.service.v1.resources.SamplesResource;
import org.infinitenature.service.v1.types.OccurrenceField;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.meta.SampleFieldConfig;
import org.infinitenature.service.v1.types.response.CountResponse;
import org.infinitenature.service.v1.types.response.DeletedResponse;
import org.infinitenature.service.v1.types.response.SampleFieldConfigResponse;
import org.infinitenature.service.v1.types.response.SampleResponse;
import org.infinitenature.service.v1.types.response.SampleSliceResponse;
import org.infinitenature.service.v1.types.response.SaveOrUpdateResponse;

public class SampleResourceMock implements SamplesResource
{
   private final MockData data;

   public SampleResourceMock(MockData data)
   {
      super();
      this.data = data;
   }

   @Override
   public SaveOrUpdateResponse save(@Min(1) int portalId, Sample sample)
   {
      return null;
   }

   @Override
   public SampleFieldConfigResponse getFieldConfig(@Min(1) int portalId)
   {
      return new SampleFieldConfigResponse(Arrays
            .asList(
                  org.infinitenature.service.v1.types.meta.SampleField.values())
            .stream().map(field -> new SampleFieldConfig(field, true))
            .collect(Collectors.toSet()));
   }

   @Override
   public SampleFieldConfigResponse getFieldConfig(@Min(1) int portalId,
         @Min(1) int surveyId)
   {
      return getFieldConfig(portalId);
   }
   @Override
   public SampleResponse get(@Min(1) int portalId, UUID uuid)
   {
      return null;
   }

   @Override
   public DeletedResponse delete(@Min(1) int portalId, UUID uuid)
   {
      return new DeletedResponse();
   }

   @Override
   public SampleSliceResponse find(@Min(0) int offset, int limit,
         OccurrenceField sortField, SortOrder sortOrder,
         OccurrenceFilter filter, boolean inlineThumbs)
   {
      // TODO Auto-generated method stub
      return null;
   }

   @Override
   public CountResponse count(OccurrenceFilter filter)
   {
      // TODO Auto-generated method stub
      return null;
   }
}
