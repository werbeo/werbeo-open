package org.infinitenature.service.v1.resources.mock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.infinitenature.service.v1.types.Availability;
import org.infinitenature.service.v1.types.Document;
import org.infinitenature.service.v1.types.DocumentType;
import org.infinitenature.service.v1.types.Locality;
import org.infinitenature.service.v1.types.MTB;
import org.infinitenature.service.v1.types.ObfuscationPolicy;
import org.infinitenature.service.v1.types.Occurrence;
import org.infinitenature.service.v1.types.Occurrence.Amount;
import org.infinitenature.service.v1.types.Occurrence.Area;
import org.infinitenature.service.v1.types.Occurrence.BloomingSprouts;
import org.infinitenature.service.v1.types.Occurrence.Quantity;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatus;
import org.infinitenature.service.v1.types.Occurrence.SettlementStatusFukarek;
import org.infinitenature.service.v1.types.Occurrence.ValidationStatus;
import org.infinitenature.service.v1.types.Person;
import org.infinitenature.service.v1.types.Portal;
import org.infinitenature.service.v1.types.PortalConfiguration;
import org.infinitenature.service.v1.types.Position;
import org.infinitenature.service.v1.types.Position.PositionType;
import org.infinitenature.service.v1.types.Sample;
import org.infinitenature.service.v1.types.SurveyBase;
import org.infinitenature.service.v1.types.Taxon;
import org.infinitenature.service.v1.types.VagueDate;
import org.infinitenature.service.v1.types.VagueDate.VagueDateType;
import org.infinitenature.service.v1.types.Validation;
import org.infinitenature.service.v1.types.atom.Link;
import org.infinitenature.service.v1.types.enums.StaticMapStyle;
import org.infintenature.mtb.MTBHelper;

public class MockData
{

   private static final int NO_OF_SAMPLES = 500;
   private static final String[] mtbs = { "1943", "1944" };
   private static final String[] mtbqs = { "", "1", "2", "3", "4", "21", "23",
         "11", "33" };

   private RandomEnum<Availability> randomAvailability = new RandomEnum<>(
         Availability.class);
   private RandomEnum<Quantity> randomQuantiy = new RandomEnum<>(
         Quantity.class);
   private RandomEnum<Amount> randomAmount = new RandomEnum<>(Amount.class);
   private RandomEnum<SettlementStatus> randomSettlementStatus = new RandomEnum<>(
         SettlementStatus.class);
   private RandomEnum<Area> randomArea = new RandomEnum<>(Area.class);
   private RandomEnum<SettlementStatusFukarek> randomSettlementStatusFukarek = new RandomEnum<>(
         SettlementStatusFukarek.class);
   private RandomEnum<BloomingSprouts> randomBloomingSprouts = new RandomEnum<>(
         BloomingSprouts.class);
   private final List<Sample> samples = new ArrayList<>();
   private final List<Taxon> taxa = fillTaxa();
   private final List<Portal> portals = fillPortal();
   private final List<SurveyBase> surveys = fillSurveys();
   private final List<Person> people = fillPeople();

   public MockData()
   {

      for (int i = 1; i <= NO_OF_SAMPLES; i++)
      {
         Sample sample = createSample(i);
         occurrence(i, sample);
         if (i % 2 == 0)
         {
            occurrence(i + NO_OF_SAMPLES, sample);
         }
         samples.add(sample);
      }
   }

   private List<Person> fillPeople()
   {
      List<Person> people = new ArrayList<>();
      int id = 1;
      for (String lastname : new String[] { "Meier", "Müller", "Schulz" })
      {
         for(String firstName : new String[] {"Adam", "Eva"}) {
            Person person = new Person(id, firstName, lastname);
            person.setEmail(firstName.charAt(0) + lastname.charAt(0) + id
                  + "@somemail.org");
            people.add(person);
            id++;
         }
      }
      return people;
   }

   private List<SurveyBase> fillSurveys()
   {
      List<SurveyBase> surveys = new ArrayList<>();

      for (int i = 1; i <= 20; i++)
      {
         SurveyBase survey = new SurveyBase(i, "Projekt - " + i,
               randomAvailability.random(), null);
         survey.setDescription("Eine Beschreibung für das Projekt " + i);
         survey.getObfuscationPolicies().add(new ObfuscationPolicy("APPROVED","LOCATION_MTBQQ"));
         survey.getObfuscationPolicies().add(new ObfuscationPolicy("APPROVED","PERSONS"));
         survey.setWerbeoOriginal(i % 2 == 0);
         survey.setAllowDataEntry(i % 2 == 0);
         surveys.add(survey);
      }
      return surveys;
   }

   private Sample createSample(int i)
   {
      Sample sample = new Sample();

      VagueDate date = new VagueDate();
      date.setFrom(LocalDate.now());
      date.setTo(LocalDate.now());
      date.setType(VagueDateType.DAY);
      sample.setDate(date);

      sample.setModificationDate(LocalDateTime.now());

      sample.setSurvey(surveys.get(i % surveys.size()));

      sample.setLocality(createLocality(i));
      return sample;
   }

   private Locality createLocality(int i)
   {

      org.infintenature.mtb.MTB coreMTB = createMTB(i);

      Position position = new Position();
      position.setEpsg(org.infintenature.mtb.MTB.getEpsgNumber());
      position.setType(PositionType.MTB);
      MTB mtb = new MTB();
      mtb.setMtb(coreMTB.getMtb());
      position.setPosCenterLatitude(coreMTB.getCenter()[1].toDouble());
      position.setPosCenterLongitude(coreMTB.getCenter()[0].toDouble());
      position.setMtb(mtb);
      position.setWkt(coreMTB.toWkt());
      position.setWktEpsg(org.infintenature.mtb.MTB.getEpsgNumber());
      Locality locality = new Locality();
      locality.setBlur(1234);
      locality.setPosition(position);
      return locality;
   }

   private org.infintenature.mtb.MTB createMTB(int i)
   {
      String mtbString = mtbs[i % mtbs.length];
      String mtbQString = mtbqs[i % mtbqs.length];
      org.infintenature.mtb.MTB coreMTB = StringUtils.isBlank(mtbQString)
            ? MTBHelper.toMTB(mtbString)
            : MTBHelper.toMTB(mtbString + "/" + mtbQString);
      return coreMTB;
   }

   private List<Portal> fillPortal()
   {
      List<Portal> portals = new ArrayList<>();
      Portal floraMv = new Portal();
      floraMv.setEntityId(1);
      floraMv.setName("Flora-MV");
      floraMv.setConfig(new PortalConfiguration());
      floraMv.getConfig().setStaticMapStyle(StaticMapStyle.FLORA_MV);
      setMapOptions(floraMv.getConfig());
      portals.add(floraMv);
      Portal heuschrecken = new Portal();
      heuschrecken.setEntityId(2);
      heuschrecken.setName("Heuschrecken Deutschland");
      heuschrecken.setConfig(new PortalConfiguration());
      heuschrecken.getConfig().setStaticMapStyle(StaticMapStyle.VEGETWEB);
      setMapOptions(heuschrecken.getConfig());
      portals.add(heuschrecken);
      return portals;
   }

   private void setMapOptions(PortalConfiguration config)
   {
      config.setMapInitialZoom(8.0);
      config.setMapInitialLatitude(53.9);
      config.setMapInitialLongitude(12.25);
   }

   private Occurrence occurrence(int i, Sample sample)
   {
      Occurrence occurrence = new Occurrence();
      occurrence.setEntityId(UUID.randomUUID());

      occurrence.setTaxon(taxa.get(i % taxa.size()));
      occurrence.setModificationDate(LocalDateTime.now().minusMinutes(i + 1));

      occurrence.setDeterminer(new Person(22, "Hans", "Maier"));
      occurrence.setQuantity(randomQuantiy.random());
      occurrence.setCoveredArea(randomArea.random());
      occurrence.setSettlementStatus(randomSettlementStatus.random());
      occurrence
            .setSettlementStatusFukarek(randomSettlementStatusFukarek.random());
      occurrence.setBloomingSprouts(randomBloomingSprouts.random());
      occurrence.setAmount(randomAmount.random());
      occurrence.setExternalKey("External-" + i);
      occurrence.setSample(SampleMapper.INSTANCE.toBase(sample));
      occurrence.getDocuments()
            .add(new Document(DocumentType.IMAGE,
                  new Link("self", "https://picsum.photos/800/600"),
                  "Ein Bild von " + occurrence.getTaxon().getName()));
      occurrence.setValidation(validation(i));
      sample.getOccurrences().add(occurrence);
      return occurrence;
   }

   private Validation validation(int i)
   {
      if (i % 6 == 5)
      {
         return null;
      }
      else
      {
         Validation validation = new Validation();
         validation.setStatus(ValidationStatus.values()[i % 5]);
         validation.setValidationTime(LocalDateTime.now().minusDays(5));
         validation
               .setValidator(people.get(i % (people.size() - 1)));
         validation.setComment(
               "Ein Kommentar zur Validierung. Dieses ist *sehr* wichtig");
         return validation;
      }

   }

   private List<Taxon> fillTaxa()
   {
      List<Taxon> taxa = new ArrayList<>();

      Taxon taxon = new Taxon();
      taxon.setName("Blaue Blume");
      taxon.setGroup("GL");
      taxon.setEntityId(1);

      Taxon urtica = new Taxon();
      urtica.setName("Urtica dioica");
      urtica.setGroup("GL");
      urtica.setEntityId(18980);
      urtica.getDocuments()
            .add(new Document(DocumentType.MAP_TAXON_FLORA_MV_STYLE, new Link(
                  "self",
                  "https://loe3.loe.auf.uni-rostock.de/maps/5/S/Urtica%20dioica.png")));
      urtica.getDocuments().add(new Document(
            DocumentType.MAP_TAXON_FLORA_MV_STYLE_THUMB, new Link("self",
                  "https://loe3.loe.auf.uni-rostock.de/maps/5/S/thumbs/thumb_Urtica%20dioica.png")));
      urtica.getDocuments()
            .add(new Document(DocumentType.MAP_TAXON_VEGETWEB_STYLE, new Link(
                  "self",
                  "https://loe3.loe.auf.uni-rostock.de/maps/5/vegetweb-style/Urtica%20dioica.png")));
      urtica.getDocuments().add(new Document(
            DocumentType.MAP_TAXON_VEGETWEB_STYLE_THUMB, new Link("self",
                  "https://loe3.loe.auf.uni-rostock.de/maps/5/vegetweb-style/Urtica%20dioica_thumb.png")));
      urtica.getDocuments().add(new Document(
            DocumentType.MAP_TAXON_VEGETWEB_STYLE_THUMB_L, new Link("self",
                  "https://loe3.loe.auf.uni-rostock.de/maps/5/vegetweb-style/Urtica%20dioica_thumb_l.png")));
      taxa.add(taxon);
      taxa.add(urtica);
      return taxa;
   }

   public List<Occurrence> getOccurrences()
   {
      return samples.stream().flatMap(s -> s.getOccurrences().stream())
            .collect(Collectors.toList());
   }

   public List<Sample> getSamples()
   {
      return samples;
   }

   public List<SurveyBase> getSurveys()
   {
      return surveys;
   }
   public List<Taxon> getTaxa()
   {
      return taxa;
   }

   public List<Portal> getPortals()
   {
      return portals;
   }

   public List<Person> getPeople()
   {
      return people;
   }
}
